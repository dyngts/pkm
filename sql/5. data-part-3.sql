set search_path to pkm_ui;

--==============================
--BISA
--==============================


-- detail transaksi salemba
insert into transaksi_obat_salemba (jenis, tgl, deskripsi, username_pegawai) values
('jenis1', '2013-07-10', 'tidak ada deskripsi khusus','kasir'),
('jenis2', '2013-07-10', 'tidak ada deskripsi khusus','kasir');

-- detail transaksi salemba
insert into detail_transaksi_salemba (id_transaksi_salemba, nama_obat, jml_satuan) values
(1, 'Yusimox', 80),
(2, 'Ulcumaag', 80);
	
-- jadwal mingguan
insert into jadwal_mingguan (hari, waktu_awal, waktu_akhir, id_layanan) values
('1', '08:00:00', '10:00:00', 1),
('2', '08:00:00', '10:00:00', 1),
('3', '08:00:00', '10:00:00', 1);

INSERT INTO jadwal_mingguan VALUES (35, 1, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (36, 2, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (37, 4, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (38, 6, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (39, 3, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (40, 5, '08:00:00', '11:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (41, 1, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (42, 2, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (43, 3, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (44, 4, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (45, 5, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (46, 6, '13:00:00', '17:00:00', 1);
INSERT INTO jadwal_mingguan VALUES (47, 1, '13:00:00', '17:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (48, 2, '13:00:00', '17:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (49, 5, '13:00:00', '17:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (50, 6, '13:00:00', '17:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (51, 1, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (52, 2, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (53, 3, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (54, 4, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (55, 5, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (56, 6, '08:00:00', '11:00:00', 2);
INSERT INTO jadwal_mingguan VALUES (57, 3, '08:00:00', '11:00:00', 3);
INSERT INTO jadwal_mingguan VALUES (58, 5, '08:00:00', '11:00:00', 3);
	
-- dokter mingguan
INSERT INTO dokter_mingguan VALUES (35, 'P10011');
INSERT INTO dokter_mingguan VALUES (36, 'P10011');
INSERT INTO dokter_mingguan VALUES (37, 'P10011');
INSERT INTO dokter_mingguan VALUES (38, 'P10011');
INSERT INTO dokter_mingguan VALUES (39, 'P10008');
INSERT INTO dokter_mingguan VALUES (40, 'P10008');
INSERT INTO dokter_mingguan VALUES (41, 'P10008');
INSERT INTO dokter_mingguan VALUES (42, 'P10008');
INSERT INTO dokter_mingguan VALUES (43, 'P10008');
INSERT INTO dokter_mingguan VALUES (44, 'P10011');
INSERT INTO dokter_mingguan VALUES (45, 'P10008');
INSERT INTO dokter_mingguan VALUES (46, 'P10011');
INSERT INTO dokter_mingguan VALUES (47, 'P10009');
INSERT INTO dokter_mingguan VALUES (48, 'P10009');
INSERT INTO dokter_mingguan VALUES (49, 'P10009');
INSERT INTO dokter_mingguan VALUES (50, 'P10009');
INSERT INTO dokter_mingguan VALUES (51, 'P10006');
INSERT INTO dokter_mingguan VALUES (52, 'P10006');
INSERT INTO dokter_mingguan VALUES (53, 'P10006');
INSERT INTO dokter_mingguan VALUES (54, 'P10006');
INSERT INTO dokter_mingguan VALUES (55, 'P10006');
INSERT INTO dokter_mingguan VALUES (56, 'P10006');
INSERT INTO dokter_mingguan VALUES (57, 'P10011');
INSERT INTO dokter_mingguan VALUES (58, 'P10011');


-- jadwal harian
insert into jadwal_harian (id_mingguan, tanggal, waktu_aktual_awal, waktu_aktual_akhir) values
(1, '2013-07-25', '08:00:00', '10:00:00'),
(2, '2013-07-25', '10:00:00', '12:00:00'),
(3, '2013-07-25', '13:00:00', '15:00:00');

-- dokter harian


-- registrasi pengobatan
insert into registrasi_pengobatan (status, no_urutan, id_pasien, jadwal_harian, tanggal, nomor_petugas) values
(1, 10, 'A12345', 1, '2013-07-25', 'P10002');
	
-- artikel
insert into artikel (isi, nomor_penulis, judul) values 
('Isi dari judul satu', 'P10002', 'Judul 1');

--rekam medis
insert into rekam_medis (id_rekam_medis,waktu,tanggal,keluhan_utama,anamnesa,keterangan_alergi,username_perawat,username_dokter ) values 
('A12345','06:00:00','2013-08-20','Sakit','-','Tidak ada','suster','boyke'),
('A34567','06:06:00','2013-08-20','Sakit','-','Tidak ada','suster','boyke'),
('B23456','06:07:00','2013-08-20','Ah oh','-','Tidak ada','suster','boyke'),
('A12345','06:08:00','2013-08-20','Sakit gigi','-','Tidak ada','suster','drgigi');

insert into resep values
('A12345','06:00:00','2013-08-20',18,0),
('A34567','06:06:00','2013-08-20',19,0),
('B23456','06:07:00','2013-08-20',20,0),
('A12345','06:08:00','2013-08-20',21,0);

insert into detail_resep values
(18,'Tidak Ada Obat', 0,0, FALSE),
(19,'Ulcumaag',2,'3x1', FALSE),
(19,'Acifar 400mg tablet',2,'3x3', FALSE),
(20,'Itamol tab',4,'3x1', FALSE),
(21,'Bisoprolol 5mg',4,'3x2', FALSE);
