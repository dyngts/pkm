alter user postgres set DateStyle to 'European'; --agar format date 'dd-mm-yy'
create database pkm;
create schema pkm_ui;
set search_path to pkm_ui;

-- create domain
-- create domain idobat as serial;
-- create domain idsupplier as serial;
-- create domain idtransaksisalemba as serial;
-- create domain idlogobatkeluar as serial;
-- create domain idlogobatmasuk as serial;
-- create domain idresep as serial;
-- create domain idjadwalmingguan as serial;
-- create domain idartikel as serial;
-- create domain idregpengobatan as serial;
-- create domain idotorisasi as serial;
-- create domain idkritiksaran as serial;
-- create domain nopelayanansalemba as serial;

create domain kodediagnosis as varchar(6);
create domain nokuitansi as char(10);

create domain idpasien as char(6);
create domain nopegawai as char(6);
create domain idtindakan as varchar(5);

create domain namaobat as varchar(50);
create domain namapegawai as varchar(50);
create domain namapasien as varchar(50);
create domain username as varchar(30);
create domain password as varchar(33);

create domain emailaddress as varchar(50);
create domain nomortelepon as varchar(20);
create domain gender as char(1);
create domain goldarah as varchar(2);
create domain tempatlahir as varchar(30);
create domain kewarganegaraan as varchar(20);
create domain statuspernikahan as varchar(20);
create domain agama as varchar(20);
create domain foto as varchar(255);
create domain hari as int;
create domain klinik as varchar(30);

create domain nofaktur as varchar(20);
create domain nosuratjalan as char(10);