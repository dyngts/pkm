set search_path to pkm_ui;

-- *********************************************
-- table with no dependent
-- *********************************************

-- supplier
-- id > serial
create table supplier (
	id serial not null primary key,
	nama varchar(50) not null,
	no_telepon varchar(15),
	alamat text not null,
	kota varchar(20) not null,
	contact_person varchar(30),
	email varchar(30),
	status boolean not null default true,
	unique (nama, kota)
);

-- otorisasi pegawai
-- id > serial
create table otorisasi (
	otorisasi_id serial not null primary key,
  	jabatan varchar(30) not null unique,
	auth_registrasi int not null default 2,
	auth_pelayanan int not null default 2,
	auth_apotek int not null default 2
);

-- ldap_dummy
create table ldap_dummy (
	ldap_username username primary key,
	ldap_password password
);

-- layanan PKM UI 
-- id < serial
create table layanan (
    id serial primary key,
		nama varchar(255) not null,
		deskripsi text not null,
		foto foto,
    gate_antrian boolean not null default false
);

-- daftar tindakan
create table tindakan (
    id idtindakan primary key,
    jenis_pelayanan varchar(20) not null,
    nama varchar(50) not null,
    tarif_umum int default 0,
    tarif_karyawan int default 0,
    tarif_mahasiswa int default 0,
    ketentuan varchar(255)
);

-- daftar diagnosis
create table diagnosis (
    kode kodediagnosis primary key,
    jenis_penyakit varchar(80) not null,
    keterangan text 
);

-- laporan pelayanan salemba
create table laporan_pelayanan_salemba (
    nomor serial primary key,
    tanggal date not null,
    nama_pasien namapasien not null,
    jenis_kelamin gender not null,
    umur int not null,
    diagnosa text not null,
    tindakan text not null,
    jasa_dokter int not null,
    jasa_tindakan int not null,
    poli_umum_netto int not null,
    poli_umum_jp_pkm int not null,
    pendapatan_poli_umum int not null,
    dokter_pemeriksa namapegawai not null 
);

-- *********************************************
-- table with dependent(s)
-- *********************************************

-- pengguna
-- foreign ke otorisasi dan pegawai
create table pengguna (
	username username primary key,
	password password,
	id_otorisasi int not null,
	foreign key (id_otorisasi) references otorisasi
	on update cascade on delete cascade
);


-- pegawai
-- id_otorisasi: serial
create table pegawai (
	nomor nopegawai primary key,
	nama namapegawai not null,
	jenis_kelamin gender not null,
	email emailaddress not null,
	alamat text,
	no_telepon nomortelepon,
	foto foto,
	username username, 
	id_otorisasi int not null,
	status_aktif boolean not null default true,
	foreign key (id_otorisasi) references otorisasi
	on update cascade on delete cascade,
	foreign key (username) references pengguna
	on update cascade on delete cascade	
);

-- pasien
-- id > idpasien
create table pasien (
  id idpasien primary key,
  nama namapasien not null,
  jenis_kelamin gender not null,
  agama agama not null,
  gol_darah goldarah not null,
  tempat_lahir tempatlahir not null,
  tanggal_lahir date not null,
  status_pernikahan statuspernikahan not null,
  alamat text not null,
  jenis_pasien integer not null,
  lembaga_fakultas varchar(50),
  kewarganegaraan kewarganegaraan,
	foto foto,
	no_telepon varchar(15) not null,
	no_identitas varchar(25) not null,
	waktu_pendaftaran timestamp not null default now(),
	status_verifikasi boolean default false,
	username username,
	password password,
	jurusan varchar(50),
	tahun_masuk int,
	nomor_dokter_gigi nopegawai,
	foreign key (jenis_pasien) references otorisasi(otorisasi_id),
	foreign key (nomor_dokter_gigi) references pegawai (nomor)
	on delete cascade on update cascade
);

-- obat
-- id_supplier : serial
-- id > serial
create table obat (
	nama namaobat not null primary key,
	id serial unique not null,
	kategori varchar(50) not null,
	jenis varchar(50) not null,
	status_pemakaian boolean not null default true,
	sediaan varchar(30) not null,
	kemasan varchar(30) not null,
	satuan_per_kemasan int default 0,
	total_satuan int default 0,
	komposisi text,
	harga_satuan int,
	harga_per_kemasan int,
	id_supplier int,
	foreign key(id_supplier) references supplier
	on delete cascade on update cascade
);

-- stok obat
create table stok_obat (
	nama_obat namaobat not null,
	tgl_kadaluarsa date not null,
	jml_satuan int not null,
	status_kadaluarsa boolean not null default false,

	primary key(nama_obat, tgl_kadaluarsa),
	foreign key(nama_obat) references obat
	on delete cascade on update cascade
);

-- log obat masuk
-- id > serial
create table log_obat_masuk (
	id serial primary key,
	tgl date not null,
	jam time without time zone not null,
	deskripsi text not null,
	flag_faktur boolean not null default true,
	no_faktur nofaktur,
	scanned_faktur foto,
	username_penerima username not null,

	foreign key(username_penerima) references pengguna (username)
	on delete cascade on update cascade
);

-- detail log obat masuk
-- id_log_obat_masuk: serial
create table penerimaan_obat (
	id_log_obat_masuk int not null,
	nama_obat namaobat not null,
	jumlah int not null,
	total_stok int not null,

	primary key(id_log_obat_masuk, nama_obat),
	foreign key(nama_obat) references obat
	on delete restrict on update cascade,
	foreign key(id_log_obat_masuk) references log_obat_masuk
	on delete cascade on update cascade
);

-- log obat keluar
-- id > serial
create table log_obat_keluar (
	id serial primary key,
	deskripsi text,
	tgl date not null,
	jam time without time zone not null,
	jenis varchar(20) not null,
	username_pemberi username not null,
	no_surat_jalan nosuratjalan unique,
	penerima varchar(30),
	tgl_penerimaan date,
	id_resep int,
	
	foreign key(username_pemberi) references pengguna (username),
	foreign key(id_resep) references resep (id)
	on delete cascade on update cascade
);

-- detail log obat keluar
-- id_log_obat_keluar: serial
create table pengeluaran_obat (
	id_log_obat_keluar int not null,
	nama_obat namaobat not null,
	jumlah int not null,
	sisa_obat int not null,

	primary key(id_log_obat_keluar, nama_obat),
	foreign key(nama_obat) references obat
	on delete restrict on update cascade,
	foreign key(id_log_obat_keluar) references log_obat_keluar
	on delete cascade on update cascade
);

-- log crud obat
create table log_update_obat (
	waktu timestamp without time zone not null default now() primary key,
	deskripsi text not null,
	nama_obat namaobat not null,
	username_updater username not null,

	foreign key(nama_obat) references obat
	on delete restrict on update cascade,
	foreign key(username_updater) references pengguna (username)
	on delete cascade on update cascade
);

-- penyimpanan transaksi obat di Salemba
-- id > serial
create table transaksi_obat_salemba (
	id serial primary key,
	jenis varchar(6) not null,
	tgl date not null,
	jam time without time zone not null,
	deskripsi text not null,
	username_pegawai username not null,

	foreign key (username_pegawai) references pengguna (username)
	on update cascade on delete cascade
);

-- detail transaksi obat di Salemba
-- id_transaksi_salemba: serial	
create table detail_transaksi_salemba (
	id_transaksi_salemba int not null,
	nama_obat namaobat not null,
	jml_satuan int not null,
	
	primary key(id_transaksi_salemba, nama_obat),
	foreign key(id_transaksi_salemba) references transaksi_obat_salemba
	on delete restrict on update cascade,
	foreign key(nama_obat) references obat
	on delete restrict on update cascade
);

-- jadwal mingguan
-- id > serial
create table jadwal_mingguan (
  id serial primary key,
  hari hari not null,
  waktu_awal time without time zone not null,
  waktu_akhir time without time zone not null,
  id_layanan int not null,

	foreign key (id_layanan) references layanan
	on delete cascade on update cascade
);

-- dokter pada jadwal mingguan
-- id_jadwal_mingguan: serial
create table dokter_mingguan (
  id_jadwal_mingguan int not null,
	nomor_dokter nopegawai not null,
	
	primary key(id_jadwal_mingguan, nomor_dokter),
	foreign key (nomor_dokter) references pegawai (nomor)
	on delete cascade on update cascade,
	foreign key (id_jadwal_mingguan) references jadwal_mingguan
	on delete cascade on update cascade

);

-- jadwal harian
-- id_mingguan: serial
create table jadwal_harian (
  id_mingguan int not null,
  tanggal date not null,
	waktu_aktual_awal time without time zone not null,
	waktu_aktual_akhir time without time zone not null,
	
	primary key(id_mingguan, tanggal),
	foreign key(id_mingguan) references jadwal_mingguan
	on delete cascade on update cascade
);

-- dokter yang bertugas pada hari tersebut
create table dokter_harian (
  id_harian int not null,
  tanggal date not null,
	nomor_dokter nopegawai not null,
	
	primary key(id_harian, nomor_dokter, tanggal),
	foreign key (nomor_dokter) references pegawai (nomor)
	on delete cascade on update cascade,
	foreign key (id_harian, tanggal) references jadwal_harian
	on delete cascade on update cascade
);

-- tabel registrasi pengobatan pasien
-- id > serial
create table registrasi_pengobatan (
  id serial primary key,
  status int not null,
  no_urutan int,
  id_pasien idpasien not null,
  jadwal_harian int not null,
  tanggal date not null,
  waktu_reg_awal time without time zone default now(),
  waktu_reg_akhir time without time zone,
	respon_kepuasan int,
	nomor_petugas nopegawai not null,
	
	foreign key(id_pasien) references pasien
    on delete cascade on update cascade,
	foreign key(nomor_petugas) references pegawai (nomor)
    on delete cascade on update cascade,
    foreign key(jadwal_harian, tanggal) references jadwal_harian (id_mingguan, tanggal)
    on delete cascade on update cascade
);

-- artikel
-- id  > serial
create table artikel (
    id serial primary key,
    isi text not null,
    nomor_penulis nopegawai not null,
    status varchar(10) default 'draft',
    judul varchar(255) not null,
    tgl_create timestamp without time zone not null default now(),
		tgl_naik timestamp without time zone,
		tgl_turun timestamp without time zone,
    foto foto,
    tag text,

	foreign key(nomor_penulis) references pegawai (nomor)
	on delete cascade on update cascade
);

-- kritik dan saran 
-- id > serial
create table kritik_saran (
  id serial not null,
  isi text not null,
  subjek varchar(50),
  id_pasien idpasien not null,
  waktu timestamp without time zone not null,

	foreign key (id_pasien) references pasien
	on update cascade on delete cascade
);

-- rekam medis
-- id_reg: serial
create table rekam_medis (
  id_rekam_medis idpasien not null,
  waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
	keluhan_utama text not null, 
  anamnesa text not null,
	keterangan_alergi text,
  username_perawat username not null,
  username_dokter username not null,

  primary key (id_rekam_medis, waktu, tanggal),
  foreign key (id_rekam_medis) references pasien
	on delete cascade on update cascade,
	foreign key (username_perawat) references pengguna (username) 
	on delete cascade on update cascade,
	foreign key (username_dokter) references pengguna (username) 
	on delete cascade on update cascade
);

-- catatan tindakan terhadap pasien
create table catatan_tindakan (
    id_rekam_medis idpasien not null,
    id_tindakan idtindakan not null,
    waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
	keterangan_tindakan text,

    primary key (id_rekam_medis, id_tindakan, waktu, tanggal),
    foreign key (id_rekam_medis, waktu, tanggal) references rekam_medis 
	on delete cascade on update cascade,
    foreign key (id_tindakan) references tindakan 
	on delete cascade on update cascade
);

-- catatan diagnosis pasien
create table catatan_diagnosis (
    id_rekam_medis idpasien not null,
    kode_diagnosis kodediagnosis not null,
	waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
    status_diagnosa varchar(4) not null,
	keterangan_diagnosa text,
	
    primary key (id_rekam_medis, kode_diagnosis, waktu, tanggal),
    foreign key (id_rekam_medis, waktu, tanggal) references rekam_medis 
	on delete cascade on update cascade,
    foreign key (kode_diagnosis) references diagnosis
	on delete cascade on update cascade
);

-- hasil pemeriksaan pasien
create table hasil_pemeriksaan (
    id_rekam_medis idpasien not null,
    waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
    tekanan_darah varchar(10) not null,
    frekuensi_suhu varchar(5) not null,
    frekuensi_nadi varchar(5) not null,
    frekuensi_pernapasan varchar(5) not null,
    hasil_lab varchar(100),
    rontgen varchar(100),

    PRIMARY KEY (id_rekam_medis, waktu, tanggal),
    FOREIGN KEY (id_rekam_medis, waktu, tanggal) REFERENCES rekam_medis
	on delete cascade on update cascade 
);

-- hasil rontgen pasien
create table hasil_rontgen (
    id_rekam_medis idpasien not null,
    waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
	rontgen foto not null,	

    primary key (id_rekam_medis, waktu, tanggal, rontgen),
    foreign key (id_rekam_medis, waktu, tanggal) references hasil_pemeriksaan 
	on delete cascade on update cascade 
);

-- resep
-- id > serial
create table resep (
    id_rekam_medis idpasien not null,
    waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
	id serial primary key,
    status_proses int not null,

    foreign key (id_rekam_medis, waktu, tanggal) references rekam_medis
	on delete cascade on update cascade
);

-- detail obat pada resep
-- id_resep: serial
create table detail_resep (
	id_resep int not null,
	nama_obat namaobat not null,
    jumlah int not null,
    dosis varchar(10) not null,
    status boolean not null default false,
	
    primary key (id_resep, nama_obat),
    foreign key (nama_obat) references obat (nama) 
    on delete cascade on update cascade
);

create table tagihan (
	status boolean not null default false,
	id_rekam_medis idpasien not null,
	waktu time without time zone NOT NULL,
	tanggal date NOT NULL, 
	username_dokter username not null, 
	jumlah_pembayaran int not null default 0,

    primary key (id_rekam_medis, waktu, tanggal), 
	foreign key (id_rekam_medis, waktu, tanggal) references rekam_medis
    on delete cascade on update cascade,
	foreign key (username_dokter) references pengguna (username)
    on delete cascade on update cascade
);

create table pembayaran (
    no_kuitansi serial PRIMARY KEY,
    status_pembayaran boolean NOT NULL,
	jumlah_pembayaran integer NOT NULL,
    username_keuangan character varying(30) NOT NULL,
    id_rekam_medis character(6) NOT NULL,
	waktu_tagih time NOT NULL,
	waktu time without time zone NOT NULL,
	tanggal date NOT NULL,
	foreign key (id_rekam_medis, waktu_tagih, tanggal) references rekam_medis
    on delete cascade on update cascade,
    --FOREIGN KEY (id_rekam_medis, waktu, tanggal) REFERENCES tagihan (id_rekam_medis, waktu, tanggal) ON DELETE RESTRICT ON UPDATE RESTRICT,
    foreign key (username_keuangan) references pengguna (username) on delete RESTRICT on update restrict 
);

create table resep_tebus (
	id_resep integer,
	status_tebus boolean,
	primary key(id_resep, status_tebus),
	foreign key (id_resep) references resep(id) 
);

create table stok_awal_bulan( 
	nama_obat namaobat not null, 
	bulan char(2), 
	tahun char(4), 
	jumlah_stok int, 
	jumlah_penerimaan int, 
	jumlah_pemakaian int, 
	jumlah_sisa int, 
	primary key (nama_obat, bulan, tahun), 
	foreign key (nama_obat) references obat 
	on update cascade on delete cascade 
	); 
