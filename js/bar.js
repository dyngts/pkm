/**
* Animated Graph Tutorial for Smashing Magazine
* July 2011
*
* Author: Derek Mack
* derekmack.com
* @derek_mack
*
* Example 3 - Animated Bar Chart via jQuery
*/

// Wait for the DOM to load everything, just to be safe
$(document).ready(function() {

   // Create our graph from the data table and specify a container to put the graph in
   createGraph('#data-table', '.chart');

   // Here be graphs
   function createGraph(data, container) {

      // Declare some common variables and container elements
      var bars = [];
      var figureContainer = $('<div id="figure"></div>');
      var legendContainer = $('<div class="legend_box"></div>');
      var graphContainer = $('<div class="graph"></div>');
      var barContainer = $('<div class="bars"></div>');
      var data = $(data);
      var container = $(container);
      var chartData;
      var chartYMax;
      var columnGroups;
      var totalGroup;
      var totalBar;
      var groupWidth;
      var barWidth;
      var left = 0;
      var red;
      var green;
      var blue;

      // Timer variables
      var barTimer;
      var graphTimer;

      // Create table data object
      var tableData = {

         // Get numerical data from table cells
         chartData: function() {
            var chartData = [];
            data.find('tbody td').each(function() {
               chartData.push($(this).text());
            });

            return chartData;
         },

         // Get heading data from table caption
         chartHeading: function() {
            var chartHeading = data.find('caption').text();

            return chartHeading;
         },

         // Get legend data from table body
         chartLegend: function() {
            var chartLegend = [];

            // Find th elements in table body - that will tell us what items go in the main legend
            data.find('tbody th').each(function() {
               chartLegend.push($(this).text());
            });

            return chartLegend;
         },

         // Get number of legend data from table body
         chartLegendSize: function() {
            var chartLegend = 0;

            // Find th elements in table body - that will tell us what items go in the main legend
            data.find('tbody th').each(function() {
               chartLegend += 1;
            });

            return chartLegend;
         },

         // Get highest value for y-axis scale
         chartYMax: function() {
            var chartData = this.chartData();

            // Round off the value
            var chartYMax = Math.ceil(Math.max.apply(Math, chartData))+5; //@
            
            return chartYMax;
         },

         // Get y-axis data from table cells
         yLegend: function() {
            var chartYMax = this.chartYMax();
            var yLegend = [];

            // Number of divisions on the y-axis
            var yAxisMarkings = 5;

            // Add required number of y-axis markings in order from 0 - max
            for (var i = 0; i < yAxisMarkings; i++) {
               yLegend.unshift(((chartYMax * i) / (yAxisMarkings - 1)));
               // yLegend.unshift(((chartYMax * i) / (yAxisMarkings - 1)) / 1000); //Original
            }

            return yLegend;
         },

         // Get x-axis data from table header
         xLegend: function() {
            var xLegend = [];

            // Find th elements in table header - that will tell us what items go in the x-axis legend
            data.find('thead th').each(function() {
               xLegend.push($(this).text());
            });

            return xLegend;
         },

         // Get number of x-axis data from table header
         xLegendSize: function() {
            var xLegend = 0;

            // Find th elements in table header - that will tell us what items go in the x-axis legend
            data.find('thead th').each(function() {
               xLegend += 1;
            });

            return xLegend;
         },

         // Sort data into groups based on number of columns
         columnGroups: function() {
            var columnGroups = [];

            // Get number of columns from first row of table body
            var columns = data.find('tbody tr:eq(0) td').length;
               for (var i = 0; i < columns; i++) {
                  columnGroups[i] = [];
                  data.find('tbody tr').each(function() {
                     columnGroups[i].push($(this).find('td').eq(i).text());
                  });
               }
            return columnGroups;
         }
      }

      // Useful variables for accessing table data
      chartData = tableData.chartData();
      chartYMax = tableData.chartYMax();
      columnGroups = tableData.columnGroups();


      // BARWIDTH
      totalGroup = tableData.xLegendSize();
      totalBar = tableData.chartLegendSize();
      groupWidth = 700/(totalGroup+1);
      barWidth = groupWidth/(totalBar+0.2);

      // Construct the graph
      // Loop through column groups, adding bars as we go
      $.each(columnGroups, function(i) {

      var chartLegend = tableData.chartLegend();

         // Create bar group container
         var barGroup = $('<div class="bar-group"></div>');
         
         // Add bars inside each column
         for (var j = 0, k = columnGroups[i].length; j < k; j++) {
         
            // Create bar object to store properties (label, height, code etc.) and add it to array
            // Set the height later in displayGraph() to allow for left-to-right sequential display
            var barObj = {};
            barObj.label = this[j];
            barObj.height = Math.floor(barObj.label / chartYMax * 100) + '%';
            barObj.bar = $('<div class="bar fig' + j + '"><div class="barlabelbox"><div class="barlabel">' + chartLegend[j] + '<br/>' + barObj.label + '</div></div></div>')
            .appendTo(barGroup);
            bars.push(barObj);
         }
         // Add bar groups to graph
         barGroup.appendTo(barContainer);
      });

      // Add heading to graph
      var chartHeading = tableData.chartHeading();
      var heading = $('<h4>' + chartHeading + '</h4>');
      heading.appendTo(figureContainer);

      // Add legend to graph
      var chartLegend = tableData.chartLegend();
      var legendList = $('<ul class="legend"></ul>');
      $.each(chartLegend, function(i) {
         var listItem = $('<li><span class="icon fig' + i + '"></span>' + this + '</li>')
         .appendTo(legendList);
      });
      legendList.appendTo(legendContainer);
      legendContainer.appendTo(figureContainer);

      // Add x-axis to graph
      var xLegend = tableData.xLegend();
      var xAxisList = $('<ul class="x-axis"></ul>');
      $.each(xLegend, function(i) {
         var listItem = $('<li><span>' + this + '</span></li>')
         .appendTo(xAxisList);
      });
      xAxisList.appendTo(graphContainer);

      // Add y-axis to graph
      var yLegend = tableData.yLegend();
      var yAxisList = $('<ul class="y-axis"></ul>');
      $.each(yLegend, function(i) {
         var listItem = $('<li><span>' + this + '</span></li>')
         .appendTo(yAxisList);
      });
      yAxisList.appendTo(graphContainer);

      // Add bars to graph
      barContainer.appendTo(graphContainer);

      // Add graph to graph container
      graphContainer.appendTo(figureContainer);

      // Add graph container to main container
      figureContainer.appendTo(container);

   //    // Set individual height of bars
   //    function displayGraph(bars, i) {

   //       // Changed the way we loop because of issues with $.each not resetting properly
   //       if (i < bars.length) {

   //          // Animate height using jQuery animate() function
   //          $(bars[i].bar).animate({
   //             height: bars[i].height
   //          }, 800);

   //          // Wait the specified time then run the displayGraph() function again for the next bar
   //          barTimer = setTimeout(function() {
   //             i++;
   //             displayGraph(bars, i);
   //          }, 100);
   //       }
   //    }

   //    // Reset graph settings and prepare for display
   //    function resetGraph() {

   //       // Stop all animations and set bar height to 0
   //       $.each(bars, function(i) {
   //          $(bars[i].bar).stop().css('height', 0);
   //       });

   //       // Clear timers
   //       clearTimeout(barTimer);
   //       clearTimeout(graphTimer);

   //       // Restart timer
   //       graphTimer = setTimeout(function() {
   //          displayGraph(bars, 0);
   //       }, 200);
   //    }


// Set individual height of bars
function displayGraph(bars, i) {
      // Set bar-group width via CSS
      // $('.fig'+[i]+' .barlabelbox .barlabel').css('left', -20);

      // Set bar-group width via CSS
      $('.bar-group').css('width', groupWidth);

      // Set bar width via CSS
      $('.bar').css('width', barWidth);

      // Set legend width via CSS
      $('.x-axis li').css('width', groupWidth);

      //
      $('.bar.fig'+[i]).css('left', left);
      left += (barWidth + 2);
      
      // i*Math.floor(Math.random()*16)
      // Algorithm 1
      // red = (150-(i*32))%255;
      // green = 255-((i*64)%255);
      // blue = (i)%255;
      // $('.fig'+[i]).css('background-color', 'rgb('+red+','+green+','+blue+')');

      //Algorithm 2
      //   red = Math.floor(Math.random()*256);
      //   green = Math.floor(Math.random()*256);
      //   blue = Math.floor(Math.random()*256);

      // // mix the color
      //     red = Math.floor((red + 255) / 2);
      //     green = Math.floor((green + 255) / 2);
      //     blue = Math.floor((blue + 255) / 2);

      //   $('.fig'+[i]).css('background-color', 'rgb('+red+','+green+','+blue+')');

      // //Algorithm 3
      // var gap = 256 / totalBar;

      // red = Math.floor((red + 255) / 2);
      // green = Math.floor((green + 255) / 2);
      // blue = Math.floor((blue + 255) / 2);

      // $('.fig'+[i]).css('background-color', 'rgb('+red+','+green+','+blue+')');

      // Algorithm 4
      red = (i*77)%360; //Math.floor(Math.random()*360);
      green = Math.floor(Math.random()*30)+70;
      blue = 70//Math.floor(Math.random()*30)+50;

      $('.fig'+[i]).css('background-color', 'hsl('+red+','+green+'%,'+blue+'%)');

   // Changed the way we loop because of issues with $.each not resetting properly
   if (i < bars.length) {
      // Add transition properties and set height via CSS
      $(bars[i].bar).css({
         'height': bars[i].height, 
         '-moz-transition': 'height 0.8s ease-in-out',
         '-webkit-transition': 'height 0.8s ease-in-out',
         '-o-transition': 'height 0.8s ease-in-out',
         '-ms-transition': 'height 0.8s ease-in-out',
         'transition': 'height 0.8s ease-in-out'
       });


      // Wait the specified time, then run the displayGraph() function again for the next bar
      barTimer = setTimeout(function() {
         i++;
         displayGraph(bars, i);
      }, 100);
   }
}
// Reset graph settings and prepare for display
function resetGraph() {

   left = 0;

   // Set bar height to 0 and clear all transitions
   $.each(bars, function(i) {
      $(bars[i].bar).stop().css({
         'height': 0, 
         '-moz-transition': 'none',
         '-webkit-transition': 'none',
         '-o-transition': 'none',
         '-ms-transition': 'none',
         'transition': 'none' 
       });
   });

   // Clear timers
   clearTimeout(barTimer);
   clearTimeout(graphTimer);

   // Restart timer
   graphTimer = setTimeout(function() {
      displayGraph(bars, 0);
   }, 200);
}

      // Helper functions
      // Call resetGraph() when button is clicked to start graph over
      $('#reset-graph-button').click(function() {
         resetGraph();
         return false;
      });

      // Finally, display graph via reset function
      resetGraph();
   }

});