<?php
	 CHANGELOG
/**
 * Ubah nama kolom "id" di table otorisasi jadi "otorisasi_id"
 * Ubah type data jenis pasien dari character[1] jadi integer
 * Ubah nama kolom "no_telp" di table pegawai jadi "no_telepon"
 */

	 DOKUMENTASI
// ==================
	 MODEL - User_model
// ==================
/**
 * Model untuk keperluan CRUD user (pegawai/pasien).
 */

/**
 * Memeriksa apakah pengguna (pasien/pegawai) telah diverifikasi
 * @param  [string] $username [username pengguna]
 * @return [boolean]           [true jika sudah diverifikasi, false jika tidak]
 */
function is_verified($username)

/**
 * Mengambil username pengguna berdasarkan id pengguna
 * @param  [string] $user_id [id pengguna yang dicari]
 * @return [string] $username [username pengguna]
 */
function get_username_by_id($user_id)

/**
 * Mengambil id pengguna berdasarkan username pengguna
 * @param  [string] $user_id [username pengguna yang dicari]
 * @return [string]          [id pengguna]
 */
function get_id_by_username($user_id)

/**
 * Mengambil data pengguna berdasarkan username
 * @param  [string] $username [username pengguna]
 * @return [array]           [array berupa data pengguna]
 */
function get_user_by_username($username)

/**
 * Mengambil data pengguna berdasarkan id
 * @param  [string] $id [id pengguna]
 * @return [array]           [array berupa data pengguna]
 */
function get_user_by_id($id)

/**
 * Mengambil id user
 * @return String id user
 */
function get_my_id() {
	return $this->session->userdata('id');
}

function get_my_name() {
    return $this->session->userdata('name');
}

function get_my_role() {
    return $this->session->userdata('role');
}

// ==================
	 MODEL - authentication_model
// ==================
/**
 * Class untuk keperluan login dan
 * Otentikasi user dan set session user
 *
 * yang disimpen di session cuma 
 * 'id','nama','role'
 *
 */


/**
 * Mengambil id user
 * @return String id user
 */
function get_my_id() 
function get_my_name() 
function get_my_role() 
function get_my_role_name()


// boolean
function is_pasien()
function is_pegawaipkm()
function is_pelayanan()
function is_loket()
function is_loggedin()
function is_perawat()
function is_dokter()
function is_kasir()
function is_adm_keu()
function is_apoteker()
function is_logistik()
function is_pj()
function is_sekretariat()
function is_admin()


;?>