<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doktermod extends CI_Model {
	function __construct() {
			parent:: __construct();
	}

	public function search_rekmed($idpasien){
		$this->db->select('*'); 
        $this->db->from(SCHEMA.'pasien');
        $this->db->where('id',$idpasien);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
        	$row = $query->row();
        	$id_data = array ('idrekam' => $row->id);
        	$this->session->set_userdata($id_data);
        	return true;
        }
        else return false;
	}

	function getPerawat(){
		$query = $this->db->query("SELECT * FROM ".SCHEMA."PEGAWAI join ".SCHEMA." OTORISASI on PEGAWAI.id_otorisasi=otorisasi.otorisasi_id WHERE jabatan LIKE 'Perawat%'");
		return $query->result_array();
	}

	function getDokter(){
		$query = $this->db->query("SELECT * FROM ".SCHEMA."PEGAWAI join ".SCHEMA." OTORISASI on PEGAWAI.id_otorisasi=otorisasi.otorisasi_id WHERE jabatan LIKE 'Dokter%' OR jabatan LIKE 'Koordinator Pelayanan%' OR jabatan LIKE 'Penanggung%'");
		return $query->result_array();
	}

	function getIdPasien(){
		$this->db->select('*'); 
        $this->db->from(SCHEMA.'pasien');
        $this->db->where('status_verifikasi',"TRUE");
        $query = $this->db->get();
        if($query->num_rows() > 0) {
        	$array = $query->result_array();
        	return $array;
        }
	}

	public function getIdTindakan($data) {
		$this->db->select('*');
		$this->db->from(SCHEMA.'tindakan');
		$this->db->where('nama',$data);
		$query = $this->db->get();
		$row = $query->row();
		$idtindakan = $row->id;
		return $idtindakan;
	}

	public function getUsername($namapegawai) {
		$querypegawai = $this->db->query("SELECT * FROM ".SCHEMA."PEGAWAI WHERE nama LIKE '%".$namapegawai."%'");
		$row = $querypegawai->row();
		$usernamepegawai = $row->username;
		return $usernamepegawai;
	}

	public function rekmed_entry($data) {
		$this->db->insert(SCHEMA.'rekam_medis',$data);
		return TRUE;
	}

	public function pemeriksaan_insert($dota) {
		$this->db->insert(SCHEMA.'hasil_pemeriksaan',$dota);
		return true;
	}

	public function diagnosa_insert($dota) {
		$this->db->insert(SCHEMA.'catatan_diagnosis',$dota);
		return true;
	}

    public function tindakan_insert($data) {
        $this->db->insert(SCHEMA.'catatan_tindakan',$data);
		return true;
    }

    public function resep_insert($data){
    	$this->db->insert(SCHEMA.'resep',$data);
		return true;
    }
	
	function detailresep($data){
		$this->db->insert(SCHEMA.'detail_resep',$data);
		return true;
	}
	
	function rontgen_insert($data){
		$this->db->insert(SCHEMA.'hasil_rontgen',$data);
		return true;
	}
	
	function resep_tebus($data){
		$this->db->insert(SCHEMA.'resep_tebus',$data);
	}

    public function getDataObat(){
    	$this->db->select('*');
		$this->db->from(SCHEMA.'obat');
		$this->db->order_by("nama", "asc"); 
		$query = $this->db->get();
		$arrayq = $query->result_array();
		return $arrayq;
    }

    function retrieve_rekam_medis($idpasien, $namapasien){
    	$query = $this->db->query("SELECT * FROM ".SCHEMA." pasien WHERE upper(NAMA) LIKE '%".$namapasien."%' OR lower(NAMA) LIKE '%".$namapasien."%' OR NAMA LIKE '%".$namapasien."%' OR upper(id) LIKE '%".$idpasien."%' OR lower(id) LIKE '%".$idpasien."%' OR id LIKE '%".$idpasien."%' "); 
    	$array = $query->result_array();
    	return $array;
    }

	public function search_rekmed_retrieve($idpasien){
		$this->db->select('*');
		$this->db->from(SCHEMA.'pasien');
		$this->db->where('id',$idpasien);
		$querypasien = $this->db->get();
        $queryrekam = $this->db->query("SELECT * from ((".SCHEMA."rekam_medis natural join ".SCHEMA."catatan_diagnosis natural join ".SCHEMA."catatan_tindakan) as asd join ".SCHEMA."tindakan as t on asd.id_tindakan = t.id )as zxc join ".SCHEMA."diagnosis as d on zxc.kode_diagnosis = d.kode where zxc.id_rekam_medis ='".$idpasien."' ");
          
        if($queryrekam->num_rows() > 0 && $querypasien->num_rows() > 0) {
        	$rowrekam = $queryrekam->row();
        	$rowpasien = $querypasien->row();
        	$id_data = array ( 
			   'idrekam' => $rowpasien->id,
			   'namapasien' =>$rowpasien->nama,
			   'tempatlahir' =>$rowpasien->tempat_lahir,
			   'tanggallahir' =>$rowpasien->tanggal_lahir,
			   'jeniskelamin' =>$rowpasien->jenis_kelamin,
			   'bangsa' => 'Indonesia',
			   'lembaga' =>$rowpasien->lembaga_fakultas,
			   'kawin' =>$rowpasien->status_pernikahan,
			   'agama' =>$rowpasien->agama,
			   'alamat' =>$rowpasien->alamat,
			   'gol_darah' =>$rowpasien->gol_darah
			  );
	    	$this->session->set_userdata($id_data);
	    	return $queryrekam;
        }
        else if ($queryrekam->num_rows() == 0 && $querypasien->num_rows() > 0) {
        	$rowpasien = $querypasien->row();
            	$id_data = array ( 
				   'idrekam' => $rowpasien->id,
				   'namapasien' =>$rowpasien->nama,
				   'tempatlahir' =>$rowpasien->tempat_lahir,
				   'tanggallahir' =>$rowpasien->tanggal_lahir,
				   'jeniskelamin' =>$rowpasien->jenis_kelamin,
				   'bangsa' => 'Indonesia',
				   'lembaga' =>$rowpasien->lembaga_fakultas,
				   'kawin' =>$rowpasien->status_pernikahan,
				   'agama' =>$rowpasien->agama,
				   'alamat' =>$rowpasien->alamat,
				   'gol_darah' =>$rowpasien->gol_darah
				  );
            $this->session->set_userdata($id_data);
        	return "Pasien ini belum pernah berobat di PKM UI";
        }
	}
	
	public function retrieveTindakan() {
		$this->db->order_by("nama", "asc"); 
		$query = $this->db->get(SCHEMA.'tindakan');
		return $query;
	}
	
	function getDiagnosa(){
		$this->db->select('*');
		$this->db->from(SCHEMA.'diagnosis');
		$query = $this->db->get();
		$arrayquery = $query->result_array();
		return $arrayquery;
	}

	function getDataDiagnosa($idrekmed,$tanggal){
		$query = $this->db->query("SELECT * from (".SCHEMA."catatan_diagnosis as cd join ".SCHEMA."diagnosis as d on cd.kode_diagnosis=d.kode) where tanggal = '".$tanggal."' AND id_rekam_medis='".$idrekmed."'");
		$diagarray = $query->result_array();
		return $diagarray;
	}

	function search_tindakan($id){
		$this->db->select('*');
		$this->db->from(SCHEMA.'tindakan');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->num_rows();
		return $result;
	}

	function search_obat($id) {
		$this->db->select('*');
		$this->db->from(SCHEMA.'obat');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->num_rows();
		return $result;
	}
	function getRekMed($idrm, $tanggal,$waktu) {
		$sql = "SELECT * FROM ".SCHEMA." rekam_medis WHERE id_rekam_medis = ? AND tanggal = ? AND waktu=?"; 
		$query = $this->db->query($sql, array($idrm, $tanggal, $waktu));
		if($query->num_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	function getIDResep($idrm, $tanggal,$waktu) {
		$sql = "SELECT id FROM ".SCHEMA." resep WHERE id_rekam_medis = ? AND tanggal = ? AND waktu=?"; 
		$query = $this->db->query($sql, array($idrm, $tanggal, $waktu));
		if($query->num_rows() > 0){
			$row = $query->row();
			$idresep = $row->id;
			return $idresep;
		}
		else {
			return null;
		}
	}
	
	function getNamaObat($idobat){
		$this->db->select('nama');
		$this->db->from(SCHEMA.'obat');
		$this->db->where('id', $idobat);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$row = $query->row();
			$namaobat = $row->nama;
			return $namaobat;
		}
		else return 'Tidak ada';
	}
	

}