<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kasirmod extends CI_Model {
	function __construct() {
			parent:: __construct();
	}

	public function buat_tagihan($dota){
		$this->db->insert(SCHEMA.'tagihan',$dota);
		return TRUE;
	}

	public function gettagihan() {
		$this->db->select('*');
		$this->db->from(SCHEMA.'tagihan');
		$query = $this->db->get();
		return $query;
	}

	public function getDataTagihan() {
		$this->db->select('*');
		$this->db->from(SCHEMA.'tagihan');
		$this->db->where('');
	}

	function getdatapasien($id){
		$this->db->select('*');
		$this->db->from(SCHEMA.'pasien');
		$this->db->where('id', $id);
		$datapasien = $this->db->get();
		return $datapasien;
	}
	
	function getNamaDokter($idrekmed,$tanggal) {
		$this->db->select('username_dokter');
		$this->db->from(SCHEMA.'rekam_medis');
		$this->db->where('tanggal', $tanggal);
		$this->db->where('id_rekam_medis', $idrekmed);
		$query = $this->db->get();
		$row = $query->row();
		$namadokter = $row->username_dokter;
		return $namadokter;
	}

	function getNamebyUsername($username) {
		$this->db->select('nama');
		$this->db->from(SCHEMA.'pegawai');
		$this->db->where('username', $username);
		$query = $this->db->get();
		$row = $query->row();
		$namadokter = $row->nama;
		return $namadokter;
	}

	function getbiayaobat($idrekmed, $tanggal) {
		//$timenow = date("H:i");and waktu < '".$timenow."'
		$query = $this->db->query("SELECT * FROM (SELECT * from ((SELECT * FROM (".SCHEMA." resep as co join ".SCHEMA." resep_tebus on co.id=resep_tebus.id_resep) where status_tebus = 'FALSE') as tr join ".SCHEMA." detail_resep as dr on tr.id=dr.id_resep) where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' ) as tmp join ".SCHEMA." obat as o on tmp.nama_obat=o.nama");
		return $query;
	}

	function bayar($datapembayaran, $idresep) {
		$this->db->insert(SCHEMA.'pembayaran ',$datapembayaran);
		$this->db->query("update ".SCHEMA." tagihan set status = 'TRUE' where id_rekam_medis = '".$datapembayaran['id_rekam_medis']."'");
		$this->db->query("update ".SCHEMA." resep_tebus set status_tebus = 'TRUE' where id_resep='".$idresep."'");
		return TRUE;
	}

	function getLatestNoKuitansi(){
		$this->db->select_max('no_kuitansi');
		$this->db->from(SCHEMA.'pembayaran');
		$query = $this->db->get();
		$row = $query->row();
		$nokuitansi = $row->no_kuitansi;
		return $nokuitansi;
	}
	/*====================================================================================================
	==========================Start of Model buat Laporan Harian=========================================
	=======================================================================================================*/
	function getUsernameDokter($tanggal, $idrm, $time){
		$sql = "SELECT username_dokter FROM ".SCHEMA." rekam_medis WHERE tanggal = ? AND id_rekam_medis = ?   AND waktu=?"; 
		$query = $this->db->query($sql, array($tanggal, $idrm, $time));
		if($query->num_rows() > 0){
			$row = $query->row();
			$usernamedokter = $row->username_dokter;
			return $usernamedokter;
		}
	}
	
	function getLaporanHarian($tanggal) {
		$query = $this->db->query("SELECT * FROM (SELECT * from ".SCHEMA." PEMBAYARAN order by no_kuitansi asc)as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id where tp.tanggal = '".$tanggal."' ORDER BY no_kuitansi asc");
		/*$query = $this->db->query("SELECT * FROM (SELECT pe.id_rekam_medis, no_kuitansi, username_keuangan, pe.waktu as waktu_bayar, pe.tanggal, username_dokter, pe.jumlah_pembayaran, t.waktu as waktu_rekam_medis from (tagihan as t join pembayaran as pe on t.id_rekam_medis = pe.id_rekam_medis AND t.tanggal=pe.tanggal AND t.status=pe.status_pembayaran))as tp join pasien as p on tp.id_rekam_medis = p.id where tp.tanggal = '".$tanggal."'ORDER BY no_kuitansi asc");*/
		return $query;	
	}
	
	function gethargaobat($idrekmed, $tanggal, $waktu) {
		$query = $this->db->query("SELECT * FROM (SELECT * from ((SELECT * FROM (".SCHEMA." resep as co join ".SCHEMA." resep_tebus on co.id=resep_tebus.id_resep) where status_tebus = 'TRUE') as tr join ".SCHEMA." detail_resep as dr on tr.id=dr.id_resep) where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' AND waktu='".$waktu."' ) as tmp join ".SCHEMA." obat as o on tmp.nama_obat=o.nama ");
		return $query;
	}
	
	function getbiayatindakan($idrekmed, $tanggal, $waktu) {
		$query = $this->db->query("SELECT * from (".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id)  where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' and waktu='".$waktu."'");
		return $query;
	}


	function getbiayatindakanumum($idrekmed, $tanggal,$waktu) {
		$query = $this->db->query("SELECT * from (".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id)  where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Poli Umum' OR jenis_pelayanan = '-') AND waktu='".$waktu."'  ");
		$dataumum = $query->result_array();
		return $dataumum;
	}

	function getbiayatindakangigi($idrekmed, $tanggal, $waktu) {
		$query = $this->db->query("SELECT * from (".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id)  where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Poli Gigi' OR jenis_pelayanan = '-') AND waktu='".$waktu."' ");
		$datagigi = $query->result_array();
		return $datagigi;
	}

	function getKonsultasi($idrekmed,$tanggal,$waktu) {
		$query = $this->db->query("SELECT * from (".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id)  where id_rekam_medis = '".$idrekmed."' and tanggal = '".$tanggal."' AND nama = 'Konsultasi/Pemeriksaan Fisik (PF)' AND waktu='".$waktu."'");
		if($query->num_rows() > 0 ) {
			return true;
		}
		return false;
	}

	/*====================================================================================================
	==========================End of Model buat Laporan Harian============================================
	=======================================================================================================*/

	/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<><<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

	/*====================================================================================================
	==========================Start of Model buat Laporan Bulanan ========================================
	=======================================================================================================*/

	function getPoliUmumHarian($tanggal) {
		$query = $this->db->query("SELECT * from ((".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id) as asd join ".SCHEMA." pasien as p on asd.id_rekam_medis = p.id)  where tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Poli Umum' OR jenis_pelayanan = 'Laboratorium' OR jenis_pelayanan = 'Estetika Medis' OR jenis_pelayanan = '-') ");
		$dataumum = $query->result_array();
		return $dataumum;
	}

	function getPoliGigiHarian($tanggal) {
		$query = $this->db->query("SELECT * from ((".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id) as asd join ".SCHEMA." pasien as p on asd.id_rekam_medis = p.id)  where tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Poli Gigi' OR jenis_pelayanan = '-') ");
		$datagigi = $query->result_array();
		return $datagigi;
	}

	function getFarmasiHarian($tanggal){
		$query = $this->db->query("SELECT * FROM ".SCHEMA." PASIEN AS P JOIN(SELECT * FROM ".SCHEMA." OBAT as o join (SELECT * FROM ".SCHEMA." RESEP as r JOIN ".SCHEMA." DETAIL_RESEP as dr ON r.id=dr.id_resep)as temp on o.nama=temp.nama_obat) AS ASD on p.id = asd.id_rekam_medis where tanggal = '".$tanggal."' ");
		$dataobat = $query->result_array();
		return $dataobat;
	}

	function getRadiologiHarian($tanggal){
		$query = $this->db->query("SELECT * from ((".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id) as asd join ".SCHEMA." pasien as p on asd.id_rekam_medis = p.id)  where tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Radiologi' OR jenis_pelayanan = '-') ");
		$dataobat = $query->result_array();
		return $dataobat;
	}

	function getAmbulansHarian($tanggal){
		$query = $this->db->query("SELECT * from ((".SCHEMA." catatan_tindakan as ct join ".SCHEMA." tindakan as t on ct.id_tindakan = t.id) as asd join ".SCHEMA." pasien as p on asd.id_rekam_medis = p.id)  where tanggal = '".$tanggal."' AND (jenis_pelayanan = 'Ambulans' OR jenis_pelayanan = '-') ");
		$dataobat = $query->result_array();
		return $dataobat;
	}
	/*====================================================================================================
	==========================End of Model buat Laporan Bulanan============================================
	=======================================================================================================*/


	
	/*====================================================================================================
	==========================Start of Model buat Salemba=================================================
	=======================================================================================================*/
	function insert_salemba($data){
		$this->db->insert(SCHEMA.'laporan_pelayanan_salemba', $data);
		return TRUE;
	}

	function get_salemba($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$thn = $tahun;
		$query = $this->db->query("SELECT * FROM ".SCHEMA." laporan_pelayanan_salemba where extract(MONTH FROM tanggal) = '$numbulan' and extract(YEAR FROM tanggal) = '$thn'");
		return $query;
	}

	/*====================================================================================================
	==========================End of Model buat Salemba=================================================
	=======================================================================================================*/
	
	function getlaporanbulanan($bulan, $tahun) {
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT tanggal FROM ".SCHEMA." pembayaran WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '$tahun' GROUP BY TANGGAL ORDER BY tanggal asc");
		return $query;
	}

	function getLaporanBulananUmum($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from (SELECT waktu_tagih,tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin,jumlah_pembayaran FROM ".SCHEMA." pembayaran as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."' ) as asd JOIN (SELECT * from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)='".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."' and jenis_pelayanan='Poli Umum') as ddd on asd.id_rekam_medis=ddd.id_rekam_medis and asd.tanggal = ddd.tanggal and asd.tanggal = ddd.tanggal and asd.waktu_tagih=ddd.waktu order by asd.tanggal asc");
		return $query;
	}

	function getLaporanBulananGigi($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from (SELECT waktu_tagih,tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin,jumlah_pembayaran FROM ".SCHEMA." pembayaran as tp join ".SCHEMA."pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."' ) as asd join (select * from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)='".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."'  and jenis_pelayanan='Poli Gigi') as ddd on asd.id_rekam_medis=ddd.id_rekam_medis and asd.tanggal = ddd.tanggal and asd.waktu_tagih=ddd.waktu order by asd.tanggal asc");
		return $query;
	}

	function getLaporanBulananEstetika($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from ((SELECT waktu_tagih,tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin,jumlah_pembayaran, no_kuitansi FROM ".SCHEMA." pembayaran as tp join ".SCHEMA."pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."') as asd join (SELECT id,jenis_pelayanan,nama,tarif_umum,tarif_karyawan,tarif_mahasiswa,ketentuan, id_rekam_medis as idrekmed, tanggal as tang, waktu from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)='".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."' and jenis_pelayanan='Estetika Medis') as ddd on asd.id_rekam_medis =ddd.idrekmed and asd.tanggal = ddd.tang and asd.waktu_tagih=ddd.waktu) as temp join ".SCHEMA." resep as resp on temp.id_rekam_medis=resp.id_rekam_medis AND temp.tanggal=resp.tanggal order by temp.tanggal asc");
		return $query;
	}

	function getLaporanBulananFarmasi($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from (SELECT * from ".SCHEMA." pembayaran) as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."'  AND (jenis_pasien = '2' OR jenis_pasien='3')");
		return $query;
	}

	function getLaporanBulananLab($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from (SELECT tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin,jumlah_pembayaran, no_kuitansi FROM ".SCHEMA." pembayaran as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '$tahun' ) as asd JOIN (SELECT * from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)='".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '$tahun' and jenis_pelayanan='Laboratorium') as ddd on asd.id_rekam_medis=ddd.id_rekam_medis and asd.tanggal = ddd.tanggal order by asd.tanggal asc");
		return $query;
	}

	function getLaporanBulananRadio($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT * from (SELECT tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin,jumlah_pembayaran FROM ".SCHEMA." pembayaran as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '$tahun' ) as asd JOIN (SELECT * from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)='".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '$tahun'  and jenis_pelayanan='Radiologi') as ddd on asd.id_rekam_medis=ddd.id_rekam_medis and asd.tanggal = ddd.tanggal order by asd.tanggal asc");
		return $query;
	}

	function getLaporanBulananAmbulans($bulan, $tahun){
		switch($bulan) {
			case 'Januari': $numbulan = 1;break;
			case 'Februari': $numbulan = 2;break;
			case 'Maret': $numbulan = 3;break;
			case 'April': $numbulan = 4;break;
			case 'Mei': $numbulan = 5;break;
			case 'Juni': $numbulan = 6;break;
			case 'Juli': $numbulan = 7;break;
			case 'Agustus': $numbulan = 8;break;
			case 'September': $numbulan = 9;break;
			case 'Oktober': $numbulan = 10;break;
			case 'November': $numbulan = 11;break;
			case 'Desember': $numbulan = 12;break;
		}
		$query = $this->db->query("SELECT distinct tanggal_lahir,jenis_pasien,nama_pasien,ddd.tanggal,jenis_kelamin,ddd.id_rekam_medis, nama, tarif_mahasiswa, tarif_karyawan, tarif_umum FROM(SELECT tanggal_lahir,jenis_pasien,p.nama as nama_pasien, tanggal,tp.id_rekam_medis,jenis_kelamin FROM ".SCHEMA." pembayaran as tp join ".SCHEMA." pasien as p on tp.id_rekam_medis = p.id WHERE EXTRACT(MONTH FROM tanggal) = '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."')as asd join (SELECT * from ".SCHEMA." tindakan as t join ".SCHEMA." catatan_tindakan as ct on t.id = ct.id_tindakan where extract(month from tanggal)= '".$numbulan."' and EXTRACT(YEAR FROM tanggal) = '".$tahun."'  and jenis_pelayanan='Ambulans') as ddd on asd.id_rekam_medis=ddd.id_rekam_medis and asd.tanggal = ddd.tanggal order by ddd.tanggal asc");
		return $query;
	}

	function searchrekmed($idrm, $tanggal) {
		$sql = "SELECT * FROM ".SCHEMA." rekam_medis WHERE id_rekam_medis = ? AND tanggal = ?"; 
		$query = $this->db->query($sql, array($idrm, $tanggal));
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row;
		}
		else {
			return null;
		}
	}

	function getObatEstetika($idresep,$idrm, $tanggal) {
		$sql = "SELECT * FROM (SELECT * FROM (".SCHEMA." detail_resep as dr join ".SCHEMA." RESEP as resp on dr.id_resep = resp.id) where resp.id = ? AND id_rekam_medis = ? AND tanggal = ?) as asd join ".SCHEMA." obat on asd.nama_obat = obat.nama";
		$query = $this->db->query($sql, array($idresep,$idrm, $tanggal));
		if($query->num_rows() > 0){
			$arrayquery = $query->result_array();
			return $arrayquery;
		}
	}

}