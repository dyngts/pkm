<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk mengambil daftar calon pasien.
 */
class Calon_pasien extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * Memeriksa apakan user adalah pasien
     * @return boolean TRUE jika user adalah pasien, FALSE jika bukan
     */
    public function is_pasien() {
        return $this->session->userdata('is_pasien');
    }

    /**
     * Mengambil semua calon pasien dalam database
     * @return query daftar calon pasien
     */
    public function get_calon_pasien(){
        
        $this->load->database();
        // Urutkan berdasarkan id secara ascending
        $this->db->order_by("id", "asc"); 
        // Ambil semua calon pasien
        $query = $this->db->get('registrasi_pasien_sementara');

        return $query;
        
    }

    /**
     * Mengambil calon pasien dalam database pada urutan tertentu
     * @param  integer $limit jumlah calon pasien yang diambil
     * @param  integer $start baris pertama dalam database yang diambil
     * @return query        daftar calon pasien mulai baris ke $start sebanyak $limit baris
     */
    public function get_calon_pasien_limited($limit, $start){
        
        $this->load->database();
        // Tentukan batasan yang akan diambil
        $this->db->limit($limit, $start);
        // Urutkan berdasarkan id secara ascending
        $this->db->order_by("id", "asc"); 
        // Ambil dari database
        $query = $this->db->get('registrasi_pasien_sementara');
        
        return $query;
    }

} ?>