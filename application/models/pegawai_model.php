<?php
class pegawai_model extends CI_Model{
	
    function __construct(){

        parent::__construct();
    }


    public function is_firsttime() {
        return $this->session->userdata('is_firsttime');
    }

    public function is_unique($str) {
    	$this->db->where('username',$str);
    	$query = $this->db->get('pegawai');

    	if ($query->num_rows > 0 ) {

            return false;
        }

        else return true;
    }

    public function is_only_me($str,$id) {
    	$this->db->where('username',$str);
    	$this->db->where('id',$id);
    	$query = $this->db->get('pegawai');

    	if ($query->num_rows == 1 ) {

            return true;
        }
        else {
	    	$this->db->where('username',$str);
	    	$query = $this->db->get('pegawai');
	    	if ($query->num_rows == 0 ) {
	    		return true;
	    	}
	    	else {
	    		return false;
	    	}
        }
    }

	public function firsttime()
	{

		$query = $this->daftarpegawaimodel();

        if ($query->num_rows > 0) {

	        // Set data berikut
	        $data = array(
	                'is_firsttime' => false,
	                );

	        // Set session data sesuai dengan data di atas
	        $this->session->set_userdata($data);
            return false;
        }

        else 
        {
	        // Set data berikut
	        $data = array(
	                'is_firsttime' => true,
	                );

	        // Set session data sesuai dengan data di atas
	        $this->session->set_userdata($data);

	        return true;
        }
	}
    
    public function insert($data){
		$this->db->insert('pegawai', $data); 
	
		}
	public function registrasi_sukses (){
		$query = $this->db->query ('Select * FROM pegawai');
		$row = $query->last_row();
		
	}
		function get_my_id() {
		return $this->session->userdata('id');
	}
	
	public function daftarpegawaimodel (){
		$this->load->database();
        $this->db->order_by('id', 'asc'); 
        $query = $this->db->get('pegawai');
        return $query;
	}
	
	public function getprofilepegawai ($id){
		// controller manggil model. di model select bla bla from pegawai
		$query = $this->db->query ("SELECT * FROM pegawai where id='".$id."'");
		return $query->result();
	}
	
	public function updatepegawai($id, $data){
		
		$this->db->where('id', $id);
        $this->db->update('pegawai', $data);
	}
	
	public function getperanpegawai($id){
		$peran= $this->db->query ("SELECT peran FROM pegawai WHERE id='".$id."' ");
		return $peran;
	}
	public function deletepegawai ($id){
		$this->db->where('id', $id);
		$this->db->delete('pegawai'); 
	}
	public function getnama ($id){
		$this->load->database();
		$this->db->select('nama');
		$this->db->where('id', $id);
		$query = $this->db->get('pegawai');
		
		foreach ($query->result() as $row) {
			return $row->nama;
		}
		//return $query;
	} 
	public function insertimage($image,$id){
		$this->db->query('update pegawai set Foto ='.$image.' where id = '.$id.''); 
		 
	}
	function count_all(){
        return $this->db->count_all('pegawai');
    }
	// get persons with paging
    function get_paged_list($limit, $offset){
		$this->load->database();
		$this->db->limit( $limit, $offset);
        $this->db->order_by('id','asc');
        $query = $this->db->get('pegawai');
		return $query;
    }
	
	function get_last_id(){
		$last_id = $this->db->query ("SELECT last_id_pegawai FROM last_id");
		return $last_id;
	}
	
	function update_id($id_update){
		$this->db->query('UPDATE last_id set last_id_pegawai='.$id_update.'');
	}

	function update_sufix($int){
		$this->db->query('UPDATE pegawai set sufix='.$int.'');
	}
	function get_sufix(){
		$suf = $this->db->query('select max(sufix) from pegawai');
		foreach ($suf->result() as $row) {
			return $row->sufix;
		}
	}
	
	function get_name($id) {
    $this->load->database();
    $this->db->where('id', $id);
    $query = $this->db->get('pegawai');
    $row = $query->row(); 

    $nama = $row->nama;

    return $nama;
	}
	
	
	function get_user_role($id) {

    $this->load->database();
    $this->db->where('id', $id);
    $query = $this->db->get('pegawai');
    $row = $query->row(); 

    $role = $row->peran;

    return $role;
	}

	public function updatepassword($id)
	{

		$newpassword = $this->input->post('password');
		$data = array(
					'password_pegawai'=>md5($newpassword),
				);

		
		$this->db->where('id', $id);
		$this->db->update('pegawai', $data); 
	}
}
?>