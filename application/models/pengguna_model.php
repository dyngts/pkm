<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna_model extends MY_Model {


	public function __construct()
	{
		parent::__construct();
		
	}

	public function login($username, $password) {
		$nama_tabel = 'propensi.pengguna';
		
		// variabel yang dicek
		$where = array(
			'username'=>$username,
			'password'=>$password);

		// retrieve user yang memenuhi kondisi username dan password yang telah diisi
		$query = $this->db->query("SELECT p.username, p.password, p.nip, p.flag_admin
								   FROM propensi.pengguna p
								   WHERE p.username = '$username'
								   		 AND p.password = '$password'");
		
		return $query->first_row('array');
	}

	public function _ambil_semua_pengguna($mulai = 0) {

		$query = $this->db->query("SELECT username
								   FROM propensi.apoteker
								   LIMIT 10
								   OFFSET $mulai
								   ");

		$query1 = $this->db->query("SELECT username
								   FROM propensi.apoteker
								   ");

		$hasil = array();
		$hasil['apotekers'] = $query->result();
		$hasil['jumlah_baris'] = $query1->num_rows();

		return $hasil;
	}

	public function _ambil_pengguna($username) {

		$this->db->select()->from('propensi.pengguna')->where('username',$username);
		return $this->db->get()->row();
	}

	public function _ambil_kustom($atribut, $nilai) {
		$this->db->select($atribut)->from('propensi.pengguna')->where($atribut, $nilai);
		return $this->db->get()->row();
	} 

	public function _cek_ada_pengguna($username) {

		$query = $this->db->query("SELECT a.username
								   FROM propensi.pengguna p, propensi.apoteker a
								   WHERE p.username = a.username
								   		 AND p.username = '$username'");

		$hasil = $query->row();

		if(count($hasil) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function _cek_pengguna($username) {

		$query = $this->db->query("SELECT p.username
								   FROM propensi.pengguna p
								   WHERE p.username = '$username'
								  ");

		$hasil = $query->row();

		if(count($hasil) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function _tambah_pengguna($data) {
		$this->db->set($data);
		$this->db->insert('propensi.apoteker');
	}

	public function _ubah_pengguna($data, $id = NULL) {
		$this->db->set($data);
		$this->db->where('username',$id);
		$this->db->update('propensi.pengguna');
	}

	public function _hapus_pengguna($username) {
		
		//if($username = NULL) return FALSE;

		$this->db->where('username',$username);
		$this->db->delete('propensi.pengguna');
	}

	public function _ambil_daftar_flag_admin() {
		$this->db->select('flag_admin')->from('propensi.pengguna');
		$query = $this->db->get();
		return $query;
	}

}

/* End of file pengguna_model.php */
/* Location: ./application/models/pengguna_model.php */