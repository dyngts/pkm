<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CRUDJadwalDokter_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	function get_doctor_name(){
		$this->db->select('id, nama');
		$this->db->where("peran LIKE 'Dokter%'");
		$this->db->order_by ('nama', 'asc');
		return $query = $this->db->get('pegawai');
	}
	
	function add_doctor_schedule($data_schedule) {
		$nama= $this->input->post('nama');
		$hari= $this->input->post('hari');
		$waktuawal=$this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktuakhir=$this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$query=$this->db->query("select * from jadwal_mingguan where id_pegawai='$nama' AND hari='$hari' AND waktuawal='$waktuawal' AND waktuakhir='$waktuakhir'");
		if($query->num_rows()>0){
			return "Jadwal tersebut sudah ada!";
		}
		else if($waktuawal>$waktuakhir){
			return "Jadwal waktu salah. Waktu mulai lebih dari waktu selesai!";
		}
		else{
			$this->db->insert('jadwal_mingguan', $data_schedule);
		}
	}
	
	
	function get_doctor_table($poli) {
		$this->db->select('jadwalmingguanid, id_pegawai, nama, hari, waktuawal, waktuakhir');
		$this->db->from('jadwal_mingguan');
		$this->db->join('pegawai', 'jadwal_mingguan.id_pegawai=pegawai.id', 'inner');
        $this->db->where('klinik', $poli);
		$this->db->order_by ("hari ASC, waktuawal ASC, waktuakhir ASC");
		return $query = $this->db->get();
    }
	
	function delete_schedule($jadwalmingguanid) {
        $this->db->where('jadwalmingguanid', $jadwalmingguanid);
        $this->db->delete('jadwal_mingguan');
    }
	
	function update_schedule($data, $jadwalmingguanid) {
		$nama= $this->input->post('nama');
		$hari= $this->input->post('hari');
		$waktuawal=$this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktuakhir=$this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$sql=$this->db->query("select * from jadwal_mingguan where id_pegawai='$nama' AND hari='$hari' AND waktuawal='$waktuawal' AND waktuakhir='$waktuakhir'");
		if($sql->num_rows()>0){
			return "Jadwal tersebut sudah ada!";
		}
		else if($waktuawal>$waktuakhir){
			return "Jadwal waktu salah. Waktu mulai lebih dari waktu selesai!";
		}
		else{
			$this->db->where('jadwalmingguanid', $jadwalmingguanid);
			$this->db->update('jadwal_mingguan', $data);
		}
    }
	
	public function getscheduleupdate($jadwalmingguanid){
		$this->db->select('jadwalmingguanid, id_pegawai, hari, waktuawal, waktuakhir');
		$this->db->where('jadwalmingguanid', $jadwalmingguanid);
		return $query = $this->db->get('jadwal_mingguan');
	}

	function get_id($hari) {
		$this->db->from('jadwal_mingguan');
		$this->db->where('hari', $hari);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_jadwal_harian($poli) {
		$this->load->helper('date');
		date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
		$this->db->from('jadwal_harian');
		//$this->db->join('jadwal_mingguan', 'jadwal_mingguan.jadwalmingguanid=jadwal_harian.jadwalid');
		$this->db->where('tanggal', $today);
		$this->db->where('klinik', $poli);
		//$this->db->select('jadwal_mingguan.hari, jadwalid, tanggal, waktuawal, waktuakhir, waktuaktualawal, waktuaktualakhir, jadwal_mingguan.id_pegawai as pegawai, jadwal_harian.id_pegawai as pengganti');
		
		$query = $this->db->get();
		return $query->result();
	}
	function get_harian($jadwal, $tanggal) {
		$this->db->from('jadwal_harian');
		$this->db->where('jadwalid', $jadwal);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get();
		return $query->result();
	}
	function is_harian_kosong() {
		date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
		$this->db->from('jadwal_harian');
		$this->db->where('tanggal', $today);
		$query = $this->db->get();

		date_default_timezone_set('Asia/Jakarta');
		$today = getdate();
		$this->db->from('jadwal_mingguan');
		$this->db->where('hari', $today['wday']);
		$id_minggu = $this->db->get();
		if ($id_minggu->num_rows() == $query->num_rows()) {
			return false;
		} else {
			return true;
		}
	}
	function update_harian($data,$jadwal, $tanggal) {
		//$this->load->database();
		//$this->db->from('jadwal_harian');
		$this->db->where('jadwalid', $jadwal);
		$this->db->where('tanggal', $tanggal);
		$this->db->update('jadwal_harian', $data);
	}
	function add_harian($data) {
		$this->db->from('jadwal_harian');
		$this->db->where('jadwalid', $data['jadwalid']);
		$this->db->where('tanggal', $data['tanggal']);
		$this->db->where('id_pegawai', $data['id_pegawai']);
		$this->db->where('hari', $data['hari']);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$this->db->insert('jadwal_harian', $data);
		}
	}
	
	function generate_jadwal_harian() {
		date_default_timezone_set('Asia/Jakarta');
		$today = getdate();
		$tanggal = date("Y-m-d");
		$id_minggu = $this->get_id($today['wday']);
		foreach ($id_minggu as $row) {
			$data = array(
				'jadwalid' => $row->jadwalmingguanid,
				'tanggal' => $tanggal,
				'waktuaktualawal' => $row->waktuawal,
				'waktuaktualakhir' => $row->waktuakhir,
				'id_pegawai' => $row->id_pegawai,
				'hari' => $row->hari,
				'klinik' => $row->klinik
			);
			$this->add_harian($data);
		}
	}
}
?>