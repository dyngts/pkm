<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
	class Search_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		
		public function get_results($search_term){
			$this->db->select('
					pegawai.nomor AS id, 
				  otorisasi.otorisasi_id AS id_peran, 
				  otorisasi.jabatan AS peran, 
				  pegawai.nama AS nama
				');
			$this->db->from(SCHEMA.'pegawai');
			$this->db->join(SCHEMA.'otorisasi','otorisasi.otorisasi_id = pegawai.id_otorisasi');
			$this->db->where("(lower(pegawai.nama) LIKE lower('%$search_term%') OR lower(pegawai.username) LIKE lower('%$search_term%') OR pegawai.nomor LIKE '%$search_term%') AND pegawai.status_aktif = true");
			$this->db->order_by("pegawai.nomor", "asc");
			return $query = $this->db->get();
		}
		
		public function get_results_pasien($search_term){
			$this->db->select('
					pasien.id AS id, 
				  otorisasi.otorisasi_id AS id_peran, 
				  otorisasi.jabatan AS peran, 
				  pasien.nama AS nama
				');
			$this->db->from(SCHEMA.'pasien');
			$this->db->join(SCHEMA.'otorisasi','otorisasi.otorisasi_id = pasien.jenis_pasien');
			$this->db->where("lower(nama) LIKE lower('%$search_term%') OR lower(username) LIKE lower('%$search_term%') OR id LIKE '%$search_term%'");			
			$this->db->order_by("id", "asc");
			return $query = $this->db->get();
		}
		
		public function get_results_article($search_term){
			$this->db->select('
			  artikel.id, 
			  artikel.isi, 
			  artikel.nomor_penulis, 
			  artikel.status, 
			  artikel.judul, 
			  artikel.tgl_naik, 
			  artikel.foto, 
			  artikel.tag,
  			pegawai.nama');
			$this->db->from(SCHEMA.'artikel, '.SCHEMA.'pegawai');
			$this->db->where("(artikel.nomor_penulis=pegawai.nomor AND status='terbit') AND (lower(judul) LIKE lower('%$search_term%'))");			
			$this->db->order_by('id', 'desc');
			return $query = $this->db->get();
		}
		
		public function get_results_article_limited($search_term, $limit, $start){
			$this->db->limit($limit, $start);
			$this->db->select('
			  artikel.id, 
			  artikel.isi, 
			  artikel.nomor_penulis, 
			  artikel.status, 
			  artikel.judul, 
			  artikel.tgl_naik, 
			  artikel.foto, 
			  artikel.tag,
  			pegawai.nama');
			$this->db->from(SCHEMA.'artikel, '.SCHEMA.'pegawai');
			$this->db->where("(artikel.nomor_penulis = pegawai.nomor AND status='terbit') AND (lower(judul) LIKE lower('%$search_term%'))");			
			$this->db->order_by('id', 'desc');
			return $query = $this->db->get();
		}
		
		public function get_results_limited($search_term, $limit, $start){
			$this->db->limit($limit, $start);
			$this->db->select('
					pegawai.nomor AS id, 
				  otorisasi.otorisasi_id AS id_peran, 
				  otorisasi.jabatan AS peran, 
				  pegawai.nama AS nama
				');
			$this->db->from(SCHEMA.'pegawai');
			$this->db->join(SCHEMA.'otorisasi','otorisasi.otorisasi_id = pegawai.id_otorisasi');
			$this->db->where("(lower(pegawai.nama) LIKE lower('%$search_term%') OR lower(pegawai.username) LIKE lower('%$search_term%') OR pegawai.nomor LIKE '%$search_term%') AND pegawai.status_aktif = true");
			$this->db->order_by("pegawai.nomor", "asc");
			return $query = $this->db->get();
		}
		
		public function get_results_pasien_limited($search_term, $limit, $start){
			$this->db->limit($limit, $start);
			$this->db->select('
					pasien.id AS id, 
				  otorisasi.otorisasi_id AS id_peran, 
				  otorisasi.jabatan AS peran, 
				  pasien.nama AS nama
				');
			$this->db->from(SCHEMA.'pasien');
			$this->db->join(SCHEMA.'otorisasi','otorisasi.otorisasi_id = pasien.jenis_pasien');
			$this->db->where("lower(nama) LIKE lower('%$search_term%') OR lower(username) LIKE lower('%$search_term%') OR id LIKE '%$search_term%'");			
			$this->db->order_by("id", "asc");
			return $query = $this->db->get();
		}
	}
?>