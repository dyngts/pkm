<?php
class laporan_model extends CI_Model{
	

    function __construct()
    {

        parent::__construct();
    }

    public function get_first_year(/*$jenis*/) 
    {
        $this->db->order_by("id_reg", "asc");
        //$this->db->order_by("poli", "desc");
        $query = $this->db->get('registrasi_pengobatan');
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
                    
            return substr($source, 6, 4);
        }          
    }

    public function get_last_year(/*$jenis*/) 
    {
        $this->db->order_by("id_reg", "desc");
        //$this->db->order_by("poli", "desc");
        $query = $this->db->get('registrasi_pengobatan');

        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
                    
            return substr($source, 6, 4);
        }   
    }
	
	public function get_umur()
	{
		$this->load->database();

		$query = $this->db->get('laporan_dummy');
		
		return $query;
	}
	
	public function get_gigi($bulan, $tahun)
	{
		$this->load->database();

		//$query = $this->db->get('laporan_dummy');
		$query = $this->db->query("select *
			from
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpb  natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpl natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msb natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msl natural full outer join
			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpl natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksl natural full outer join




			(select tamu.peran as fakultas, count(*) as tamu_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpl natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsl");
		
		return $query;
	}
	
	public function get_umum($bulan, $tahun)
	{
		$this->load->database();

		//$query = $this->db->get('laporan_dummy');

		$query = $this->db->query(
			"select *
			from
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpb  natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpl natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msb natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msl natural full outer join
			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpl natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksl natural full outer join




			(select tamu.peran as fakultas, count(*) as tamu_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpl natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsl");

		return $query;
	}
}
