<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class jadwal_model extends MY_Model{
    function __construct(){
        parent::__construct();
    }
	
	function get_doctor_name(){
		$this->db->select('id, nama');
		$this->db->where("peran LIKE 'Dokter%'");
		$this->db->order_by ('nama', 'asc');
		return $query = $this->db->get('pegawai');
	}
	
	function add_doctor_schedule($data_schedule) {
		$nama= $this->input->post('nama');
		$hari= $this->input->post('hari');
		$waktuawal=$this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktuakhir=$this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$query=$this->db->query("select * from jadwal_mingguan where id_pegawai='$nama' AND hari='$hari' AND waktuawal='$waktuawal' AND waktuakhir='$waktuakhir'");
		if($query->num_rows()>0){
			return "Jadwal tersebut sudah ada!";
		}
		else if($waktuawal>$waktuakhir){
			return "Jadwal waktu salah. Waktu mulai lebih dari waktu selesai!";
		}
		else{
			$this->db->insert('jadwal_mingguan', $data_schedule);
		}
	}
	
	function get_doctor_table($poli) {
		$this->db->select('jadwalmingguanid, id_pegawai, nama, hari, waktuawal, waktuakhir');
		$this->db->from('jadwal_mingguan');
		$this->db->join('pegawai', 'jadwal_mingguan.id_pegawai=pegawai.id', 'inner');
        $this->db->where('klinik', $poli);
		$this->db->order_by ("hari ASC, waktuawal ASC, waktuakhir ASC");
		return $query = $this->db->get();
    }
	
	function delete_schedule($jadwalmingguanid) {
        $this->db->where('jadwalmingguanid', $jadwalmingguanid);
        $this->db->delete('jadwal_mingguan');
    }
	
	function update_schedule($data, $jadwalmingguanid) {
		$nama= $this->input->post('nama');
		$hari= $this->input->post('hari');
		$waktuawal=$this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktuakhir=$this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$sql=$this->db->query("select * from jadwal_mingguan where id_pegawai='$nama' AND hari='$hari' AND waktuawal='$waktuawal' AND waktuakhir='$waktuakhir'");
		if($sql->num_rows() > 0){
			return "Jadwal tersebut sudah ada!";
		}
		else if($waktuawal>$waktuakhir){
			return "Jadwal waktu salah. Waktu mulai lebih dari waktu selesai!";
		}
		else{
			$this->db->where('jadwalmingguanid', $jadwalmingguanid);
			$this->db->update('jadwal_mingguan', $data);
		}
    }
	
	public function getscheduleupdate($jadwalmingguanid){
		$this->db->select('jadwalmingguanid, id_pegawai, hari, waktuawal, waktuakhir');
		$this->db->where('jadwalmingguanid', $jadwalmingguanid);
		return $query = $this->db->get('jadwal_mingguan');
	}

	function get_id($hari) {
		$this->db->from('jadwal_mingguan');
		$this->db->where('hari', $hari);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_harian1($poli) {
		$this->load->helper('date');
		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $today = mdate($datestring, $time);
		$this->db->from('jadwal_harian');
		//$this->db->join('jadwal_mingguan', 'jadwal_mingguan.jadwalmingguanid=jadwal_harian.jadwalid');
		$this->db->where('tanggal', $today);
		$this->db->where('klinik', $poli);
		//$this->db->select('jadwal_mingguan.hari, jadwalid, tanggal, waktuawal, waktuakhir, waktuaktualawal, waktuaktualakhir, jadwal_mingguan.id_pegawai as pegawai, jadwal_harian.id_pegawai as pengganti');
		
		$query = $this->db->get();
		return $query->result();
	}

	function get_jadwal_harian($jadwal, $tanggal) {
		$this->db->from('jadwal_harian');
		$this->db->where('jadwalid', $jadwal);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get();
		return $query->result();
	}

	function is_harian_kosong() {
		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $today = mdate($datestring, $time);

		$this->db->from(SCHEMA.'jadwal_harian');
		$this->db->where('tanggal', $today);
		$query = $this->db->get();

		date_default_timezone_set('Asia/Jakarta');
		$today = getdate();
		$this->db->from(SCHEMA.'jadwal_mingguan');
		$this->db->where('hari', $today['wday']);
		$id_minggu = $this->db->get();
		if ($id_minggu->num_rows() == $query->num_rows()) {
			return false;
		} else {
			return true;
		}
	}

	function update_harian($data,$jadwal, $tanggal) {
		//$this->load->database();
		//$this->db->from('jadwal_harian');
		$this->db->where('jadwalid', $jadwal);
		$this->db->where('tanggal', $tanggal);
		$this->db->update('jadwal_harian', $data);
	}
	function add_harian($data) {
		$this->db->from('jadwal_harian');
		$this->db->where('id_mingguan', $data['id']);
		$this->db->where('tanggal', $data['tanggal']);
		$this->db->where('id_pegawai', $data['id_pegawai']);
		$this->db->where('hari', $data['hari']);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			$this->db->insert('jadwal_harian', $data);
		}
	}
	
	function o_generate_jadwal_harian() {
		date_default_timezone_set('Asia/Jakarta');
		$today = getdate();
		$tanggal = date("Y-m-d");
		$id_minggu = $this->get_id($today['wday']);
		foreach ($id_minggu as $row) {
			$data = array(
				'jadwalid' => $row->jadwalmingguanid,
				'tanggal' => $tanggal,
				'waktuaktualawal' => $row->waktuawal,
				'waktuaktualakhir' => $row->waktuakhir,
				'id_pegawai' => $row->id_pegawai,
				'hari' => $row->hari,
				'klinik' => $row->klinik
			);
			$this->add_harian($data);
		}
	}
	






	function is_harian_set()
	{
		//kalo iya, return true
		//kalo ga, periksa ...

		//kalo ada, set per poli, ada apa ga
		$jadwal_mingguan_on_hari_ini = $this->get_jadwal_mingguan_on_hari_ini();
		//ambil harian pada hari ini
		$jadwal_hari_ini = $this->get_jadwal_hari_ini();
		return count($jadwal_hari_ini) >= count($jadwal_mingguan_on_hari_ini);
	}

	//
	function get_jadwal_hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $today = mdate($datestring, $time);
		
		$data['const'] = array(
			"jadwal_harian.tanggal = '".$today."'",
			);

		return $this->get_jadwal_praktik_harian($data);
	}

	function get_jadwal_mingguan_on_hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $today = getdate();
		
		$data['const'] = array(
			"hari = '".$today['wday']."'",
			);

		return $this->get_jadwal_praktik_mingguan($data);
	}

	//periksa apakah jadwal mingguan untuk hari ini ada
	function is_today_mingguan_set()
	{
		//kalo ga ada, return tue
		if ($this->get_jadwal_mingguan_on_hari_ini() == NULL) return FALSE;
		else return TRUE;
	}

	function generate_jadwal_harian() {
		//untuk setiap jadwal mingguan untuk hari ini
		//buat jadwal hariannya

		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);
		
		$jadwal_mingguan = $this->get_jadwal_mingguan_on_hari_ini();
		if ($jadwal_mingguan == NULL) return FALSE;

		foreach ($jadwal_mingguan as $key) {

			$data1 = array(
				'id_mingguan' => $key['id'],
				'waktu_aktual_awal' => $key['waktu_awal'],
				'waktu_aktual_akhir' => $key['waktu_akhir'],
				'tanggal' => $tanggal,
				);

			$data2 = array(
				'id_harian' => $key['id'],
				'tanggal' => $tanggal,
				'nomor_dokter' => $key['nomor_dokter'],
				);

			$sukses = $this->insert_harian($data1,$data2);

		}
		return TRUE;
	}

	function insert_harian($data1_in,$data2_in)
	{
		$jadwal_harian = $this->get_jadwal_harian_by_id($data1_in['id_mingguan'],$data1_in['tanggal']);
		if ($jadwal_harian != NULL) return FALSE;

		$data1['base_table'] = SCHEMA."jadwal_harian";
		$data1['data_in'] = $data1_in;
		$a = $this->insert($data1);

		$data2['base_table'] = SCHEMA."dokter_harian";
		$data2['data_in'] = $data2_in;
		$b = $this->insert($data2);

		return $a && $b;
	}

	function get_jadwal_harian_by_id($id_mingguan,$tanggal)
	{
		$data['single'] = true;
		$data['const'] = array(
				'id_mingguan = '.$id_mingguan,
				"jadwal_harian.tanggal = '".$tanggal."'",
			);
		return $this->get_jadwal_praktik_harian($data);
	}

	function insert_jadwal($data)
	{
		return $this->insert($data);
	}

	function update_jadwal($data)
	{
		return $this->update($data);
	}

	function delete_jadwal($data)
	{
		return $this->delete($data);
	}

	// get jadwal mingguan by id
	function get_by_id($id)
	{
		if($id == NULL) return NULL;
		else 
			$data['base_table'] = SCHEMA."jadwal_mingguan";
			$data['join'] = array(
				array(
					'table' => SCHEMA."dokter_mingguan",
					'const' => 'id = id_jadwal_mingguan',
					)
				);
			$data['const'] = array(
				'id = '.$id,
				);
			$data['single'] = true;
			return $this->get_data($data);
	}

	function get_umum_harian()
	{
		$poli = "1";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		return $this->get_jadwal_praktik_harian($data);
	}

	function get_gigi_harian()
	{
		$poli = "2";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		return $this->get_jadwal_praktik_harian($data);
	}

	function get_estetika_harian()
	{
		$poli = "3";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		return $this->get_jadwal_praktik_harian($data);
	}

	function get_umum_mingguan()
	{
		$poli = "1";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		$data['order_by'] = array(array('attrb' => 'hari', 'type' => "asc"));
		return $this->get_jadwal_praktik_mingguan($data);
	}

	function get_gigi_mingguan()
	{
		$poli = "2";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		$data['order_by'] = array(array('attrb' => 'hari', 'type' => "asc"));
		return $this->get_jadwal_praktik_mingguan($data);
	}

	function get_estetika_mingguan()
	{
		$poli = "3";
		$data['const'] = array(
			'id_layanan = '.$poli,
			);
		$data['order_by'] = array(array('attrb' => 'hari', 'type' => "asc"));
		return $this->get_jadwal_praktik_mingguan($data);
	}

	function get_gate_status_umum()
	{
		$data['const'] = array(
			'id_layanan = 1',
			);
		return $this->get_gate_status($data);
	}

	function get_gate_status_gigi()
	{
		$data['const'] = array(
			'id_layanan = 2',
			);
		return $this->get_gate_status($data);
	}

	private function get_gate_status($data)
	{
		return $this->get($data);
	}

	// get semua
	private function get_jadwal_praktik_harian($data)
	{
		if (!isset($data['tanggal']))
		{
			date_default_timezone_set('Asia/Jakarta');
	    $datestring = "%Y-%m-%d";
	    $time = time();
	    $tanggal = mdate($datestring, $time);
		}
		else $tanggal = $data['tanggal'];
		
		$data['const'][] = 
			"jadwal_harian.tanggal = '".$tanggal."'";

		$data['select'] = array( 
		  'dokter_harian.nomor_dokter',
		  'jadwal_harian.id_mingguan', 
		  'jadwal_harian.tanggal', 
		  'jadwal_harian.waktu_aktual_awal', 
		  'jadwal_harian.waktu_aktual_akhir', 
		  'layanan.gate_antrian', 
		  'layanan.nama as l_nama',
		  'jadwal_mingguan.id_layanan', 
		  'jadwal_mingguan.hari', 
		  'pegawai.nomor', 
		  'pegawai.nama as p_nama',
		  );
		$data['base_table'] = SCHEMA."jadwal_harian";
		$data['join'] = array(
			array(
				'table' => SCHEMA."dokter_harian",
				'const' => 'id_harian = id_mingguan',
				),
			array(
				'table' => SCHEMA."pegawai",
				'const' => 'nomor_dokter = nomor',
				),
			array(
				'table' => SCHEMA."jadwal_mingguan",
				'const' => 'id_mingguan = id',
				),
			array(
				'table' => SCHEMA."layanan",
				'const' => 'id_layanan = layanan.id',
				)
			);
		return $this->get_datas($data);
	}

	//get semua
	private function get_jadwal_praktik_mingguan($data)
	{
		$data['base_table'] = SCHEMA."jadwal_mingguan";
		$data['join'] = array(
			array(
				'table' => SCHEMA."dokter_mingguan",
				'const' => 'id = id_jadwal_mingguan',
				),
			array(
				'table' => SCHEMA."pegawai",
				'const' => 'nomor_dokter = nomor',
				)
			);
		return $this->get_datas($data);
	}

	function get_last_jadwal_mingguan_id()
	{
		$data['base_table'] = SCHEMA."jadwal_mingguan";
		$data['order_by'] = array(array('attrb' => 'id', 'type' => "desc"));
		$query = $this->get_data($data);

		return $query['id'];
	}

	function get_jadwal_konflik($data)
	{
		return $this->get_datas($data);
	}
}
?>