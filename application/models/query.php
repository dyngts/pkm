<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Class User Model manages database manipulation
 * have been registered, retrieve, edit, delete user's profile
 * checked for validate user, available username,
 */

class Query extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
	}

	protected function insert($data) 
	{
		$data['query_type'] = "C";
		return $this->run_query($data);
	}

	protected function update($data) 
	{
		$data['query_type'] = "U";
		return $this->run_query($data);
	}

	protected function delete($data) 
	{
		$data['query_type'] = "D";
		return $this->run_query($data);
	}

	protected function get($data)
	{
		return $this->get_data($data);
	}

	protected function get_data($data)
	{
		return $this->get_datas($data);
	}

	protected function get_datas($data) 
	{
		$data['query_type'] = "R";
		return $this->run_query($data);
	}

	private function run_query($data)
	{
		if(isset($data['data_in']))
		{
			$this->db->set($data['data_in']);
		}

		if(isset($data['select']))
		{
			$this->db->select($data['attrb']);
		}

		if(isset($data['join']))
		{
			foreach ($data['join'] as $join) {
				if(!isset($join['type'])) $join['type'] = "asc";
				$this->db->join($join['table'],$join['const'],$join['type']);
			}
		}

		if(isset($data['const']))
		{
			foreach ($data['const'] as $where) {
				$this->db->where($where);
			}
		}

		if(isset($data['order_by']))
		{
			foreach ($data['order_by'] as $order) {
				$this->db->order_by($order['attrb'],$order['type']);
			}
		}

		if(!isset($data['query_type']))
		{
			$data['query_type'] = "R";
		}
		
		if($data['query_type'] == "R")
		{
			$query = $this->db->get($data['base_table']);

			if ($query->num_rows() > 0) {
				if (isset($data['single'])) {
					if (isset($data['obj'])) return $query->row();
					else return $query->row_array();
				}
				elseif (isset($data['obj'])) return $query->result();
				else return $query->result_array();
			}
			else return NULL;
		}
		elseif($data['query_type'] == "C")
		{
			$this->db->insert($data['base_table']);
			return TRUE;
		}
		elseif($data['query_type'] == "U")
		{
			$this->db->update($data['base_table']);
			return TRUE;
		}
		elseif($data['query_type'] == "D")
		{
			$this->db->delete($data['base_table']);
			return TRUE;
		}
	}
}