<?php
/* Author: Jorge Torres
 * Description: Pegawai model class
 */
class artikel_model extends CI_Model{
	

    function __construct(){
        parent::__construct();
    }

    function add_artikel($data){
    	/*
		$tag_list = explode(",", $data['tag']);
		$this->load->model('artikel_tag_model');
		$last = $this->get_idmax();
		foreach ($last->result() as $last_id):
			$id_tag = $last_id->max;
		endforeach;
		foreach ($tag_list as $tag) {
			$this->artikel_tag_model->add_tag($id_tag,$tag);
		} */
		
		$this->db->insert(SCHEMA.'artikel',$data);
	}
	/*
	function read_artikel($id){
		
        $this->load->database();
		$query = $this->db->query ("Select judul, timestamp, isi,artikel_id FROM artikel WHERE artikel_id='".$id."'");
		return $query;
	} */
	function daftar_artikel(){
		$this->db->select ('
		  artikel.id, 
		  artikel.isi, 
		  artikel.nomor_penulis, 
		  artikel.status, 
		  artikel.judul, 
		  artikel.tgl_create, 
		  artikel.tgl_naik, 
		  artikel.tgl_turun, 
		  artikel.foto, 
		  artikel.tag, 
		  pegawai.nomor, 
		  pegawai.username, 
		  pegawai.nama
		  ');
		$this->db->from (SCHEMA.'artikel,'.SCHEMA.'pegawai,'.SCHEMA.'pengguna');
		$this->db->where('artikel.nomor_penulis = pegawai.nomor AND pegawai.username = pengguna.username');
		$this->db->order_by('artikel.tgl_create ASC');
		return $this->db->get();
	}
	function daftar_artikel_terbit(){
		$this->db->select ('
		  artikel.id, 
		  artikel.isi, 
		  artikel.nomor_penulis, 
		  artikel.status, 
		  artikel.judul, 
		  artikel.tgl_create, 
		  artikel.tgl_naik, 
		  artikel.tgl_turun, 
		  artikel.foto, 
		  artikel.tag, 
		  pegawai.nomor, 
		  pegawai.username, 
		  pegawai.nama
		  ');
		$this->db->from (SCHEMA.'artikel,'.SCHEMA.'pegawai,'.SCHEMA.'pengguna');
		$this->db->where("artikel.status = 'terbit' AND artikel.nomor_penulis = pegawai.nomor AND pegawai.username = pengguna.username");
		$this->db->order_by('artikel.tgl_create ASC');
		return $this->db->get();
	}
	function set_status($id, $status){
		$this->db->query ("UPDATE ".SCHEMA."artikel set status='".$status."' WHERE id='".$id."'");
	}
	function update_artikel($id,$data){
		//ubah data tag dabit
		/*
		$tag_list = explode(",", $data['tag']);
		$this->load->model('artikel_tag_model');
		foreach ($tag_list as $tag) {
			$this->artikel_tag_model->add_tag($id,$tag);
		} */
		$this->db->where('id', $id);
        $this->db->update(SCHEMA.'artikel', $data);
	}
	function get_id ($id){
		//$this->load->database();
		//$this->db->from('artikel');
		//$this->db->where('timestamp', $timestamp);
		//$query = $this->db->get();
		$id = $this->db->query ('SELECT artikel.id FROM '.SCHEMA.'artikel where id='.$id);
		//return $query->result();
		return $id;
	}
	
	function delete_artikel ($id) {
		$this->db->where('id', $id);
		$this->db->delete(SCHEMA.'artikel'); 
	}
	function get_artikel ($id){
		$this->db->select ('
		  artikel.id as artikel_id, 
		  artikel.isi, 
		  artikel.nomor_penulis, 
		  artikel.status, 
		  artikel.judul, 
		  artikel.tgl_create, 
		  artikel.tgl_naik as timestamp, 
		  artikel.tgl_turun, 
		  artikel.foto, 
		  artikel.tag, 
		  pegawai.nomor, 
		  pegawai.username, 
		  pegawai.nama
		  ');
		$this->db->from (SCHEMA.'artikel,'.SCHEMA.'pegawai,'.SCHEMA.'pengguna');
		$this->db->where('artikel.nomor_penulis = pegawai.nomor AND artikel.id = '.$id);
		$this->db->order_by('artikel.tgl_create ASC');
		return $this->db->get();
	}

	function get_paged_list($limit, $offset){
		$this->db->limit( $limit, $offset);
		$this->db->select ('
		  artikel.id, 
		  artikel.isi, 
		  artikel.nomor_penulis, 
		  artikel.status, 
		  artikel.judul, 
		  artikel.tgl_create, 
		  artikel.tgl_naik, 
		  artikel.tgl_turun, 
		  artikel.foto, 
		  artikel.tag, 
		  pegawai.nomor, 
		  pegawai.username, 
		  pegawai.nama
		  ');
		$this->db->from (SCHEMA.'artikel,'.SCHEMA.'pegawai,'.SCHEMA.'pengguna');
		$this->db->where('artikel.nomor_penulis = pegawai.nomor AND pegawai.username = pengguna.username');
		$this->db->order_by('artikel.tgl_create ASC');
		return $this->db->get();
   }

	function get_paged_list_terbit($limit, $offset){
		$this->db->limit( $limit, $offset);
		$this->db->select ('
		  artikel.id, 
		  artikel.isi, 
		  artikel.nomor_penulis, 
		  artikel.status, 
		  artikel.judul, 
		  artikel.tgl_create, 
		  artikel.tgl_naik, 
		  artikel.tgl_turun, 
		  artikel.foto, 
		  artikel.tag, 
		  pegawai.nomor, 
		  pegawai.username, 
		  pegawai.nama
		  ');
		$this->db->from (SCHEMA.'artikel,'.SCHEMA.'pegawai,'.SCHEMA.'pengguna');
		$this->db->where("artikel.status = 'terbit' AND artikel.nomor_penulis = pegawai.nomor AND pegawai.username = pengguna.username");
		$this->db->order_by('artikel.tgl_naik desc');
		return $this->db->get();
   }

   function update_tgl($id,$data)
   {
   	$this->db->where('id', $id);
   	$this->db->update(SCHEMA.'artikel',$data);
   }
}
?>