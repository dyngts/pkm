<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class kritiksaran_model extends CI_Model{
		function __construct(){
			parent::__construct();
		}
			
		 public function add_kritiksaran($data){
			$this->db->insert(SCHEMA.'kritik_saran',$data); 
		}

		function get_kritiksaran() {
		$this->db->select('kritik_saran.id AS kritik_id,
										  kritik_saran.isi, 
										  kritik_saran.subjek, 
										  kritik_saran.id_pasien, 
										  kritik_saran.waktu');
		$this->db->order_by ("id", "asc");
		return $query = $this->db->get(SCHEMA.'kritik_saran');
		}
		
		public function delete_kritiksaran ($id_kritik) {
		$this->db->where('id', $id_kritik);
		$this->db->delete(SCHEMA.'kritik_saran'); 
		}
		
		function get_kritiksaran_limited($limit, $start) {
		$this->db->limit($limit, $start);
		$this->db->select('*');
		$this->db->order_by ("id", "asc");
		return $query = $this->db->get(SCHEMA.'kritik_saran');
		}
	}

?>