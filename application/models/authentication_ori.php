<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan login.
 * Otentikasi user dan set session user
 */
class authentication extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pasien
     * @return boolean mengembalikan true jika user adalah pasien, false jika bukan
     */
    public function is_pasien() {
        return $this->session->userdata('is_pasien');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pasien
     * @return boolean mengembalikan true jika user adalah pasien, false jika bukan
     */
    public function is_pegawaipkm() {
        return $this->session->userdata('is_pegawai');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah koordinator pelayanan
     * @return boolean mengembalikan true jika user adalah koordinator pelayanan, false jika bukan
     */
    public function is_pelayanan() {
        return $this->session->userdata('is_pelayanan');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah petugas administrasi loket
     * @return boolean mengembalikan true jika user adalah petugas administrasi loket, false jika bukan
     */
    public function is_loket() {
        return $this->session->userdata('is_loket');
    }

    /**
     * Fungsi untuk memeriksa apakah user telah berhasil login ke dalam sistem
     * @retutidakolean mengembalikan true jika user telah berhasil login ke dalam sistem, false jika tidak
     */
    public function is_loggedin() {
        return $this->session->userdata('is_loggedin');
    }

    /**
     * Fungsi untuk memeriksa apakah user belum menjadi pasien
     * @return boolean mengembalikan true jika user belum menjadi pasien, false jika bukan
     */
    public function is_baru() {
        return $this->session->userdata('is_baru');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah perawat
     * @return boolean mengembalikan true jika user adalah perawat, false jika bukan
     */
    public function is_perawat() {
        return $this->session->userdata('is_perawat');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_dokter() {
        return $this->session->userdata('is_dokter');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_kasir() {
        return $this->session->userdata('is_kasir');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_adm_keu() {
        return $this->session->userdata('is_adm_keu');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_pj() {
        return $this->session->userdata('is_pj');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_apoteker() {
        return $this->session->userdata('is_apoteker');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_logistik() {
        return $this->session->userdata('is_logistik');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah pegawai
     * @return boolean mengembalikan true jika user adalah pegawai, false jika bukan
     */
    public function is_sekretariat() {
        return $this->session->userdata('is_sekretariat');
    }

    /**
     * Fungsi untuk memeriksa apakah user sedang menunggu form pendaftarannya diverifikasi (user menggunakan akun LDAP)
     * @return boolean mengembalikan true jika user sedang menunggu form pendaftarannya diverifikasi, false jika bukan
     */
    public function is_mendaftar() {
        return $this->session->userdata('is_mendaftar');
    }

    /**
     * Fungsi untuk memeriksa apakah user adalah civitas akademika UI (login menggunakan akun LDAP)
     * @return boolean mengembalikan true jika user adalah civitas akademika UI (login menggunakan akun LDAP), false jika bukan
     */
    public function is_civitas() {
        return $this->session->userdata('is_civitas');
    }

    /**
     * Fungsi untuk memeriksa apakah user sedang menunggu form pendaftarannya diverifikasi
     * @return boolean mengembalikan true jika user sedang menunggu form pendaftarannya diverifikasi, false jika bukan
     */
    public function is_calon_pasien() {
        return $this->session->userdata('is_calon_pasien');
    }

    /**
     * Fungsi untuk memeriksa apakah user sedang menunggu form pendaftarannya diverifikasi
     * @return boolean mengembalikan true jika user sedang menunggu form pendaftarannya diverifikasi, false jika bukan
     */
    public function is_admin() {
        return $this->session->userdata('is_admin');
    }
    

    /**
     * Fungsi untuk melakukan validasi username dan password terhadap database
     * @return boolean mengembalikan true jika username dan password cocok, false jika tidak
     */
    public function validate() {
        
        // ambil input dari user
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        
        $this->load->database();
        
        //periksa apakah user adalah masyarakat umum
        if ($this->is_umum($username,$password)) {
            $this->set_session_data_pasien($is_civitas = false, $username);
            return true;
        }
        //jika bukan, periksa apakah user adalah pegawai PKM
        elseif ($this->is_pegawai($username,$password)) {
            $this->set_session_data_pegawai($username);
            return true;
        }
        //jika bukan, periksa apakah user adalah civitas UI (dummy)
        elseif ($this->is_LDAP($username,$password)) {
            $this->set_session_data_pasien($is_civitas = true, $username);
            return true;
        }
        //jika bukan, periksa apakah user adalah civitas UI (true)
        // elseif ($this->ldap_auth($username,$password)) {
        //      $this->set_session_data_pasien($is_civitas = true, $username);
        //      return true;
        // }
        else return false;
    }

    /**
     * Memeriksa apakah input username dan password cocok dengan database (dummy) LDAP
     * @param  string  $username username yang dimasukkan user
     * @param  string  $password password yang dimasukkan user
     * @return boolean           mengembalikan TRUE jika username dan password cocok
     */
    public function is_LDAP($username, $password) {
        $this->load->database();

        // Query. Atribut yang akan dicocokkan nilainya
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        
        // Jalankan query
        $query = $this->db->get('akun');

        // Periksa apakah cocok
        if ($query->num_rows == 1) {

            return true;
        }

        else return false;
        
    }

    /**
     * Memeriksa apakah input username dan password cocok dengan database pasien umum
     * @param  string  $username username yang dimasukkan user
     * @param  string  $password password yang dimasukkan user
     * @return boolean           mengembalikan TRUE jika username dan password cocok
     */
    public function is_umum($username,$password) {
        $this->load->database();

        // Query. Atribut yang akan dicocokkan nilainya
        $this->db->where('id', $username);
        $this->db->where('password', md5($password));
        
        // Jalankan query
        $query = $this->db->get('pasien');

        // Periksa apakah cocok
        if($query->num_rows == 1) {
            return true;
        } else return false;
    }

    /**
     * Memeriksa apakah input username dan password cocok dengan database pegawai
     * @param  string  $username username yang dimasukkan user
     * @param  string  $password password yang dimasukkan user
     * @return boolean           mengembalikan TRUE jika username dan password cocok
     */
    public function is_pegawai($username,$password) {
        $this->load->database();

        // Query. Atribut yang akan dicocokkan nilainya
        $this->db->where('username', $username);
        $this->db->where('password_pegawai', md5($password));
        
        // Jalankan query
        $query = $this->db->get('pegawai');

        // Periksa apakah cocok
        if($query->num_rows == 1) {
            return true;
        } else return false;

    }

    /**
     * Melakukan pengesetan session untuk user yang login menggunakan akun LDAP (id, role, nama)
     * @param boolean $is_civitas TRUE jika user login dengan akun LDAP
     * @param string $username   username
     */
    public function set_session_data_pasien ($is_civitas, $username) {

        $this->load->database();

        // Query. Atribut yang akan digunakan
        $this->db->where('username', $username);
        // Jalankan query
        $query = $this->db->get('pasien');

        // Periksa apakah user telah terdaftar sebagai pasien
        if ($query->num_rows() > 0) {

          // Jika benar, set data berikut
          $row = $query->row();
          $data = array(
                  'id' => $row->id,
                  'nama' => $row->nama,
                  'role' => $row->peran,
                  'username' => $username,              
                  'is_baru' => false,
                  'is_loggedin' => true,
                  'is_pelayanan' => false,
                  'is_pegawai' => false,
                  'is_loket' => false,
                  'is_perawat' => false,
                  'is_pasien' => true,
                  'is_civitas' => $is_civitas,
                  );
        }

        // Jika tidak
        else {

            $this->db->where('username', $username);
            $query = $this->db->get('registrasi_pasien_sementara');

            // Periksa apakah sedang menunggu verifikasi pendaftarannya
            if ($query->num_rows() > 0) {

              $row = $query->row();

              // Jika benar, set data berikut
              $data = array(
                'id' => $row->id,
                'is_baru' => false,
                'is_loggedin' => true,
                'is_mendaftar' => true,
                'username' => $username
                );
            }


            // Jika tidak, set data berikut
            else {
              $data = array(               
                'is_baru' => true,
                'is_loggedin' => true,
                'username' => $username
                );
            }

        } 

        // Set session data sesuai dengan data di atas
        $this->session->set_userdata($data);
    }

    /**
     * Melakukan pengesetan session untuk user yang login menggunakan akun pegawai
     * @param boolean $is_civitas TRUE jika user login dengan akun pegawai
     * @param string $username   username
     */
    public function set_session_data_pegawai($username) {

        $this->load->database();

        // Query. Atribut yang akan digunakan
        $this->db->where('username', $username);
        // Jalankan query
        $query = $this->db->get('pegawai');

        // Default value
        $is_pegawai = true;
        $is_pelayanan = false;
        $is_loket = false;
        $is_perawat = false;
        $is_kasir = false;
        $is_dokter = false;
        $is_adm_keu = false;
        $is_pj = false;
        $is_apoteker = false;
        $is_logistik = false;
        $is_sekretariat = false;
        $is_loggedin = true;
        $is_pasien = false;
        $is_baru = false;
        $is_admin = false;

        $row = $query->row();

        // Periksa apakah user adalah koordinator pelayanan
        if ($row->peran == "Koordinator Pelayanan") {
            $is_pelayanan = true;
        }

        // Periksa apakah user adalah perawat
        elseif ($row->peran == "Perawat Gigi" || $row->peran == "Perawat Umum") {
            $is_perawat = true;
        }

        // Periksa apakah user adalah petugas administrasi loket
        elseif ($row->peran == "Administrasi Loket") {
            $is_loket = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Penanggung Jawab PKM") {
            $is_pj = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Koordinator Logistik") {
            $is_logistik = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Koord Administrasi&Keuangan") {
            $is_adm_keu = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Kesekretariatan") {
            $is_sekretariat = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Staf Keuangan") {
            $is_kasir = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Dokter Umum" || $row->peran == "Dokter Gigi" || $row->peran == "Dokter Estetika Medis") {
            $is_dokter = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Apoteker") {
            $is_apoteker = true;
        }

        // Periksa apakah user adalah 
        elseif ($row->peran == "Asisten Apoteker") {
            $is_apoteker = true;
        }

        elseif ($row->peran == "Admin") {
            // Default value
            $is_admin = true;
            $is_pelayanan = true;
            $is_loket = true;
            $is_perawat = true;
            $is_kasir = true;
            $is_dokter = true;
            $is_adm_keu = true;
            $is_pj = true;
            $is_apoteker = true;
            $is_logistik = true;
            $is_sekretariat = true;
        }
        // Jika bukan keempatnyanya, set data sebagai pasien
        else {
            $is_pasien = true;
        }

        // Set data berikut
        $data = array(
                'id' => $row->id,
                'nama' => $row->nama,
                'role' => $row->peran,
                'is_loggedin' => $is_loggedin,
                'is_pelayanan' => $is_pelayanan,
                'is_loket' => $is_loket,
                'is_perawat' => $is_perawat,
                'is_pegawai' => $is_pegawai,
                'is_baru' => $is_baru,
                'is_pasien' => $is_pasien,
                'is_dokter' => $is_dokter,
                'is_kasir' => $is_kasir,
                'is_adm_keu' => $is_adm_keu,
                'is_pj' => $is_pj,
                'is_apoteker' => $is_apoteker,
                'is_logistik' => $is_logistik,
                'is_sekretariat' => $is_sekretariat,
                'is_admin' => $is_admin,
                );

        // Set session data sesuai dengan data di atas
        $this->session->set_userdata($data);
    }

    /**
     * Melakukan update session data, jika pasien melakukan update/mengubah profil
     * @param  String $id id user
     */
    public function update_session_data_pasien($id) {
        $this->load->database();

        // Query. Atribut yang akan digunakan
        $this->db->where('id', $id);
        // Jalankan query
        $query = $this->db->get('pasien');

        // Periksa apakah user terdaftar
        if ($query->num_rows() > 0) {

          // Set data berikut
          $row = $query->row();
          $data = array(
                  'id' => $row->id,
                  'nama' => $row->nama,
                  'role' => $row->peran,
                  'username' => $row->username,              
                  'is_baru' => false,
                  'is_loggedin' => true,
                  'is_pelayanan' => false,
                  'is_loket' => false,
                  'is_pegawai' => false,
                  'is_perawat' => false,
                  'is_pasien' => true
                  );
        }

        // Set session data sesuai dengan data di atas
        $this->session->set_userdata($data);
    }

    /**
     * Memeriksa apakah user terdaftar dalam database LDAP UI
     * @param  String $login  username
     * @param  String $passwd password
     * @return boolean         TRUE jika terdaftar, FALSE jika tidak
     */ 
    public function ldap_auth ($login, $passwd)
    {
        // Periksa koneksi ke server LDAP
        $ds = ldap_connect ('152.118.39.37');
        $dn = $this->authorize ($ds, $login);

        if($dn != "" && $passwd != "")
        {

            if (authenticate($ds, $dn, $passwd) != null)
            {
                return $true;
            }

            else return false;

        }

        return false;
    }

    /**
     * Memerika apakah user terdaftar dalam database LDAP UI
     * @param  resource $ds    //lihat dokumetasi php
     * @param  String $login username
     * @return String        $idLengkap
     */
    public function authorize($ds, $login)
    {
        if ($ds) { 

            $r=ldap_bind($ds);         

            $sr=ldap_search($ds, "o=Universitas Indonesia, c=ID", "(&(uid=$login)(hasAccessTo=makara.cso.ui.ac.id))");  
            $info = ldap_get_entries($ds, $sr);
            $idLengkap = "";
            for ($i=0; $i<$info["count"]; $i++) {
                $idLengkap = $info[$i]["dn"];
            }
            return $idLengkap;
            
        } else {
            echo "<h4>Unable to connect to LDAP server</h4>";
        }
    }

    /**
     * Memeriksa pakah username dan password cocok
     * @param  resource $ds    //lihat dokumetasi php
     * @param  string $login    username
     * @param  string $password 
     * @return boolen           TRUE jika cocok, FALSE jika tidak
     */
    public function authenticate($ds, $login, $password)
    {
        if ($ds) { 

            $r = ldap_bind($ds, $login, $password);

            return $r;
        } else {
            echo "<h4>Unable to connect to LDAP server</h4>";
        }
    }

}
?>
