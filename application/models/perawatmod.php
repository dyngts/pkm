<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perawatmod extends CI_Model {
	function __construct() {
			parent:: __construct();
	}

	function getPolaPenyakit($bulan, $tahun){
		$thn = $tahun;
		$query = $this->db->query("SELECT jenis_penyakit, kode_diagnosis FROM ".SCHEMA." catatan_diagnosis a JOIN ".SCHEMA." diagnosis b on a.kode_diagnosis = b.kode
								   where EXTRACT(MONTH from a.tanggal)= '".$bulan."' and EXTRACT(YEAR from a.tanggal) = '".$thn."'
								   GROUP BY jenis_penyakit, a.kode_diagnosis
								   order by jenis_penyakit asc");
		return $query;
	}

	function getDokter(){
		$query = $this->db->query("SELECT username_dokter FROM ".SCHEMA." rekam_medis GROUP BY username_dokter");
		return $query->result_array();
	}
	
	function getNamaDokter($username){
		$query = $this->db->query("SELECT nama FROM ".SCHEMA." pegawai where username ='".$username."'");
		return $query->row();
	}

	function getKinerja($usernamedokter, $bulan, $tahun){
		$query = $this->db->query("SELECT count(1) from (SELECT * FROM ".SCHEMA." rekam_medis where username_dokter='".$usernamedokter."' AND EXTRACT(MONTH from tanggal)='".$bulan."' AND EXTRACT(YEAR from tanggal)='".$tahun."') as asd");
		return $query->row();
	}

	function getKasusBaru ($kodepenyakit, $bulan, $tahun){
		$kode = $kodepenyakit;
		$query = $this->db->query(" SELECT count(*) as kasus_baru, kode_diagnosis
									FROM ".SCHEMA." catatan_diagnosis a
									WHERE a.status_diagnosa = 'baru' AND EXTRACT(MONTH FROM a.tanggal)= '".$bulan."' AND a.kode_diagnosis = '".$kode."' and EXTRACT(YEAR from a.tanggal) = '$tahun'
									GROUP BY kode_diagnosis");
		
		return $query;
	}

	function getKasusLama ($kodepenyakit, $bulan, $tahun){
		$kode = $kodepenyakit;
		$thn = $tahun;
		$query = $this->db->query(" SELECT count(*) as kasus_lama, kode_diagnosis	
									FROM ".SCHEMA." catatan_diagnosis a 
									WHERE a.status_diagnosa = 'lama' AND EXTRACT(MONTH FROM a.tanggal)= '".$bulan."' AND a.kode_diagnosis = '".$kode."' and EXTRACT(YEAR from a.tanggal) = '".$tahun."'
									GROUP BY kode_diagnosis");
		return $query;
	}

	function getJenisTindakan($bulan, $tahun){
		$thn = $tahun;
		$query = $this->db->query("SELECT nama, id_tindakan, id_rekam_medis
								   FROM ".SCHEMA." catatan_tindakan a, ".SCHEMA."tindakan b
								   WHERE EXTRACT(MONTH from a.tanggal)= '".$bulan."' and a.id_tindakan = b.id and b.jenis_pelayanan = 'Poli Gigi' and EXTRACT(YEAR from a.tanggal) = '".$thn."'
								   GROUP BY nama, id_tindakan, id_rekam_medis
								   ORDER BY nama asc");		
		return $query;
		//panggilnya berarti nama
	}
	
	function getMahasiswa ($bulan, $tahun){
		$thn = $tahun;
		$query = $this->db->query("SELECT x.nama, x.id_tindakan, count (*) as mahasiswa
							FROM ".SCHEMA." pasien p,
								(SELECT nama, id_tindakan, id_rekam_medis
	 							 FROM ".SCHEMA." catatan_tindakan a, ".SCHEMA." tindakan b
	 							 WHERE EXTRACT(MONTH from a.tanggal)= '".$bulan."' and a.id_tindakan = b.id and b.jenis_pelayanan = 'Poli Gigi' and EXTRACT(YEAR from a.tanggal) = '".$thn."'
	 							 GROUP BY nama, id_tindakan, id_rekam_medis	ORDER BY nama asc) x
							WHERE p.id = x.id_rekam_medis and jenis_pasien = '1'
							GROUP BY x.nama, x.id_tindakan");
		return $query;
	}
	
	function getKaryawan ($bulan, $tahun){
		$thn = $tahun;
		$query = $this->db->query("SELECT x.nama, x.id_tindakan, count (*) as karyawan
							FROM ".SCHEMA." pasien p,
								(SELECT nama, id_tindakan, id_rekam_medis FROM ".SCHEMA." catatan_tindakan a, ".SCHEMA." tindakan b
	 							 WHERE EXTRACT(MONTH from a.tanggal)= '".$bulan."' and a.id_tindakan = b.id and b.jenis_pelayanan = 'Poli Gigi' and EXTRACT(YEAR from a.tanggal) = ".$thn."
	 							 GROUP BY nama, id_tindakan, id_rekam_medis	 							 ORDER BY nama asc) x
							WHERE p.id = x.id_rekam_medis and jenis_pasien = '2'
							GROUP BY x.nama, x.id_tindakan");
		return $query;
	}
	
	function getTamu ($bulan, $tahun){
		$thn = $tahun;
		$query = $this->db->query("SELECT x.nama, x.id_tindakan, count (*) as tamu
							FROM ".SCHEMA." pasien p,
								(SELECT nama, id_tindakan, id_rekam_medis FROM ".SCHEMA." catatan_tindakan a, ".SCHEMA." tindakan b
	 							 WHERE EXTRACT(MONTH from a.tanggal)= '".$bulan."' and a.id_tindakan = b.id and b.jenis_pelayanan = 'Poli Gigi' and EXTRACT(YEAR from a.tanggal) = ".$thn."
	 							 GROUP BY nama, id_tindakan, id_rekam_medis	 							 ORDER BY nama asc) x
							WHERE p.id = x.id_rekam_medis and jenis_pasien = '3'
							GROUP BY x.nama, x.id_tindakan");
		return $query;
	}

	function getLaporanPenyakitUmur($bulan, $tahun) {
		$query = $this->db->query("SELECT kode_diagnosis, jenis_penyakit, jenis_kelamin, CASE 
										WHEN usia BETWEEN 0 AND 4 THEN '0-4' 
										WHEN usia BETWEEN 5 AND 9 THEN '5-9' 
										WHEN usia BETWEEN 10 AND 14 THEN '10-14' 
										WHEN usia BETWEEN 15 AND 19 THEN '15-19'
										WHEN usia BETWEEN 20 AND 44 THEN '20-44'
										WHEN usia BETWEEN 45 AND 54 THEN '45-54'
										WHEN usia BETWEEN 55 AND 59 THEN '55-59'
										WHEN usia > 60 THEN '> 60'
									  END as kategori, count(*) as jumlah
									FROM 
										(SELECT DISTINCT cd.kode_diagnosis, cd.tanggal, p.id, d.jenis_penyakit, p.jenis_kelamin, extract(year from age(tanggal_lahir)) as usia
										FROM ".SCHEMA." diagnosis d, ".SCHEMA." catatan_diagnosis cd , ".SCHEMA." pasien p
										WHERE 
										  d.kode = cd.kode_diagnosis AND
										  cd.id_rekam_medis = p.id) as x
									WHERE
										EXTRACT(MONTH from tanggal)= '".$bulan."' AND EXTRACT(YEAR from tanggal)= '".$tahun."'
									GROUP BY
										jenis_penyakit, jenis_kelamin, usia, kode_diagnosis");
		return $query;
	}

	function getLaporanPenyakitKasus($bulan,$tahun) {
		$query = $this->db->query("SELECT kode_diagnosis, jenis_penyakit, jenis_kelamin, status_diagnosa, count(*) as jumlah
				FROM 
					(SELECT DISTINCT cd.kode_diagnosis, cd.tanggal, p.id, d.jenis_penyakit, p.jenis_kelamin, cd.status_diagnosa
					FROM ".SCHEMA." diagnosis d, ".SCHEMA." catatan_diagnosis cd , ".SCHEMA." pasien p
					WHERE 
					  d.kode = cd.kode_diagnosis AND
					  cd.id_rekam_medis = p.id) as x
				WHERE
					EXTRACT(MONTH from tanggal)= '".$bulan."' AND EXTRACT(YEAR from tanggal)= '".$tahun."'
				GROUP BY
					jenis_penyakit, jenis_kelamin, status_diagnosa, kode_diagnosis");
		return $query;
	}
}