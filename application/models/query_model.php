<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Query_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

	function get_akun() {

    $query = $this->db->get('akun');
    return $query->result();
	}

	function get_pasien_simple() {
	
	$query = $this->db->query('select id,username,nama,peran,pasword from pasien');
	//$this->db->select('id,username,nama,peran,password');
    //$query = $this->db->get('pasien');
    return $query->result();
	}

	function get_user_additional_data($id) {

	$role = $this->get_user_role($id);
    $this->db->from('pasien');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->result();
    
	}
}
?>
				   
