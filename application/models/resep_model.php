<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resep_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function _ambil_daftar_resep($mulai = 0) {
		$query1 = $this->db->query("SELECT DISTINCT rm.id_rekam_medis, rm.tanggal, rm.waktu, p.nama as nama_pasien, rm.username_dokter as ur_dokter, rm.id as id_resep
								   FROM ($this->t_resep natural join $this->t_rekam_medis) as rm, $this->t_pasien p
								   WHERE rm.status_proses = 0
								   		 AND rm.id_rekam_medis = p.id
								   ORDER BY rm.tanggal DESC, rm.waktu DESC
								   LIMIT 10
								   OFFSET $mulai
							");

		$query2 = $this->db->query("SELECT DISTINCT rm.id_rekam_medis, rm.tanggal, rm.waktu, p.nama as nama_pasien, rm.username_dokter as ur_dokter
								   FROM ($this->t_resep natural join $this->t_rekam_medis) as rm, $this->t_pasien p
								   WHERE rm.status_proses = 0
								   		 AND rm.id_rekam_medis = p.id
								   ORDER BY rm.tanggal DESC, rm.waktu DESC
							");

		$hasil = array();

		$hasil['reseps'] = $query1->result('array');
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_dokter_pasien($id = '') {
		$query = $this->db->query("SELECT DISTINCT r.id_rekam_medis, r.tanggal, dr.jumlah, r.waktu, p.id, p.nama, m.username_dokter as dokter, o.jenis, o.sediaan, dr.nama_obat as nama_obat, o.harga_satuan, o.komposisi, r.id as id_resep, p.nama as nama_pasien
								   FROM $this->t_resep r, $this->t_rekam_medis m, $this->t_pasien p, $this->t_detail_resep dr, $this->t_obat o, $this->t_stok_obat so
								   WHERE r.id_rekam_medis = m.id_rekam_medis
								   		 AND r.tanggal = m.tanggal
								   		 AND r.waktu = m.waktu
								   		 AND dr.nama_obat = o.nama
								   		 AND m.id_rekam_medis = p.id
								   		 AND r.id = dr.id_resep
								   		 AND r.id = '$id'
								   ORDER BY dr.nama_obat	   
				");	

		$hasil = array();
		$hasil['daftar_obats'] = $query;
		$hasil['data_resep'] = $query->row();

		return $hasil;
	}

	public function _ambil_obat_tertentu_total($nama_obat) {
		$query = $this->db->query("SELECT o.id, o.nama, so.jml_satuan, SUM(so.jml_satuan) as total
								   FROM $this->t_obat as o LEFT OUTER JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat
								   WHERE o.nama = '$nama_obat'
								   GROUP BY o.id, o.nama, so.jml_satuan
								   ORDER BY o.nama ASC
				");

		return $query->first_row();
	}

	public function _ambil_log_keluar() {
		$query = $this->db->query("SELECT *
								   FROM $this->t_log_obat_keluar lo
								   ORDER BY lo.tgl DESC, lo.jam DESC");

		return $query->first_row();
	}

	public function _ambil_baris_tertentus($nama = '') {
		$query = $this->db->query("SELECT *
								   FROM $this->t_stok_obat k
								   WHERE nama_obat = '$nama'
								   		 AND status_kadaluarsa = '0'
								   ORDER BY tgl_kadaluarsa asc");
		$kad_obats = $query->result();
		$kumpulan_kad_obat = array();
		$i = 0;

		foreach ($kad_obats as $kad_obat) {
			$kumpulan_kad_obat[$i++] = $kad_obat;
		}

		return $kumpulan_kad_obat;
	}

}

/* End of file resep_model.php */
/* Location: ./application/models/resep_model.php */