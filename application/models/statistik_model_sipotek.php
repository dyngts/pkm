<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik_model_sipotek extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}


	public function _ambil_statistik_umum($mulai = 0, $search) {
		

		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_umum'];
			$tgl_akhir = $search['tanggal_akhir_umum'];

			$query1 = $this->db->query("SELECT nama_obat,
				SUM(case l.jenis when 'Event' then jumlah else 0 end) AS event,
				SUM(case l.jenis when 'Resep' then jumlah else 0 end) AS resep,
				SUM(case l.jenis when 'Salemba' then jumlah else 0 end) AS salemba
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				GROUP BY nama_obat
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT nama_obat,
				SUM(case l.jenis when 'Event' then jumlah else 0 end) AS event,
				SUM(case l.jenis when 'Resep' then jumlah else 0 end) AS resep,
				SUM(case l.jenis when 'Salemba' then jumlah else 0 end) AS salemba
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				GROUP BY nama_obat
				");
		}
		else {
			$query1 = $this->db->query("SELECT nama_obat,
				SUM(case l.jenis when 'Event' then jumlah else 0 end) AS event,
				SUM(case l.jenis when 'Resep' then jumlah else 0 end) AS resep,
				SUM(case l.jenis when 'Salemba' then jumlah else 0 end) AS salemba
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				GROUP BY nama_obat
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT nama_obat,
				SUM(case l.jenis when 'Event' then jumlah else 0 end) AS event,
				SUM(case l.jenis when 'Resep' then jumlah else 0 end) AS resep,
				SUM(case l.jenis when 'Salemba' then jumlah else 0 end) AS salemba
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				GROUP BY nama_obat
				");
		}


		$daftar_statistik = $query1->result('array');
		
		$hasil = array();

		$hasil['daftar_statistik'] = $daftar_statistik;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_top_10_umum_json($tgl_mulai, $tgl_akhir) {

		if($tgl_mulai != '' && $tgl_akhir != '') {
			$query = $this->db->query("SELECT nama_obat, SUM(p.jumlah) AS jumlah_keluar
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				GROUP BY nama_obat
				ORDER BY jumlah_keluar DESC
				LIMIT 10
				");
		}
		else {
			$query = $this->db->query("SELECT nama_obat, SUM(p.jumlah) AS jumlah_keluar
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				GROUP BY nama_obat
				ORDER BY jumlah_keluar DESC
				LIMIT 10
				");
		}

		

		$hasil = $query->result();
		
		$obat_umum = array();
		$obat_umum[] = array('Nama Obat', 'Jumlah Unit');

		if($query->num_rows() > 0) {
			foreach ($hasil as $obat) {
				$obat_umum[] = array("$obat->nama_obat", (int) $obat->jumlah_keluar);
			}
		}
		else {
			$obat_umum[] = array('', 0);
		}
		

		echo json_encode($obat_umum);
	}

	public function _ambil_top_10_pasien_json($id_pasien, $tgl_mulai, $tgl_akhir) {

		if($tgl_mulai != '' && $tgl_akhir != '') {
			$query = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			AND ps.id = '$id_pasien'
			AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			GROUP BY nama_obat
			LIMIT 10
			");
		}
		else {
			$query = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			AND ps.id = '$id_pasien'
			GROUP BY nama_obat
			LIMIT 10
			");
		}
		

		$hasil = $query->result();
		
		$obat_umum = array();
		$obat_umum[] = array('Nama Obat', 'Jumlah Unit');

		if($query->num_rows() > 0) {
			foreach ($hasil as $obat) {
				$obat_umum[] = array("$obat->nama_obat", (int) $obat->total_pemakaian);
			}
		}
		else {
		 	$obat_umum[] = array('', 0);
		}

		

		echo json_encode($obat_umum);
	}

	public function _ambil_statistik_pasien($mulai = 0) {
		$query1 = $this->db->query("SELECT DISTINCT ps.id, ps.nama
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			ORDER BY ps.nama ASC
			LIMIT 10
			OFFSET $mulai
			");

		$query2 = $this->db->query("SELECT DISTINCT ps.id, ps.nama
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			ORDER BY ps.nama ASC
			");


		$hasil = array();
		$hasil['list_namas'] = $query1->result();
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_nama_pasien($id_pasien = '') {
		$query = $this->db->query("SELECT ps.nama
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			AND ps.id = '$id_pasien'
			");

		$pasien = $query->row_array();

		return $pasien['nama'];
	}

	public function _detil_statistik_pasien($id_pasien = '', $search, $mulai = 0) {
		
		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_pasien'];
			$tgl_akhir = $search['tanggal_akhir_pasien'];

			$query1 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			AND ps.id = '$id_pasien'
			AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			GROUP BY nama_obat
			LIMIT 10
			OFFSET $mulai
			");

		$query2 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND jenis = 'Resep'
			AND ps.id = '$id_pasien'
			AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			GROUP BY nama_obat
			");
		}
		else {
			$query1 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND ps.id = '$id_pasien'
			AND jenis = 'Resep'
			GROUP BY nama_obat
			LIMIT 10
			OFFSET $mulai
			");

		$query2 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pasien ps, $this->t_rekam_medis rm, $this->t_resep r, $this->t_pengeluaran_obat p
			WHERE l.id = p.id_log_obat_keluar
			AND r.id = l.id_resep
			AND rm.id_rekam_medis = ps.id
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND ps.id = '$id_pasien'
			AND jenis = 'Resep'
			GROUP BY nama_obat
			");
		}

		$hasil = array();
		$hasil['daftar_obats'] = $query1->result();
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_top_10_dokter_json($username_dokter, $tgl_mulai, $tgl_akhir) {


		if($tgl_mulai != '' && $tgl_akhir != '') {
			$query = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
			WHERE l.id = p.id_log_obat_keluar
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND username_dokter = '$username_dokter'
			AND jenis = 'Resep'
			AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			GROUP BY nama_obat
			LIMIT 10
			");
		}
		else {
			$query = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
			FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
			WHERE l.id = p.id_log_obat_keluar
			AND r.id_rekam_medis = rm.id_rekam_medis
			AND username_dokter = '$username_dokter'
			AND jenis = 'Resep'
			GROUP BY nama_obat
			LIMIT 10
			");
		}
		

		$hasil = $query->result();
		
		$obat_umum = array();
		$obat_umum[] = array('Nama Obat', 'Jumlah Unit');

		if($query->num_rows() > 0) {
			foreach ($hasil as $obat) {
				$obat_umum[] = array("$obat->nama_obat", (int) $obat->total_pemakaian);
			}
		}
		else {
			$obat_umum[] = array('', 0);
		}

		echo json_encode($obat_umum);
	}


	public function _ambil_statistik_dokter($mulai = 0) {
		$query1 = $this->db->query("SELECT DISTINCT rm.username_dokter
									FROM $this->t_log_obat_keluar l, $this->t_rekam_medis rm, $this->t_resep r
									WHERE jenis = 'Resep'
										  AND l.id_resep = r.id
										  AND r.id_rekam_medis = rm.id_rekam_medis
									ORDER BY rm.username_dokter ASC
									LIMIT 10
									OFFSET $mulai
									");

		$query2 = $this->db->query("SELECT DISTINCT rm.username_dokter
									FROM $this->t_log_obat_keluar l, $this->t_rekam_medis rm, $this->t_resep r
									WHERE jenis = 'Resep'
										  AND l.id_resep = r.id
										  AND r.id_rekam_medis = rm.id_rekam_medis
									ORDER BY rm.username_dokter ASC
									");

		$hasil = array();

		$hasil['list_namas'] = $query1;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_nama_dokter($username = '') {
		$query = $this->db->query("SELECT user_dokter
								   FROM propensi.log_obat_keluar l, propensi.rekam_medis r
								   WHERE l.id_rekam_medis = r.id_rekam_medis
								   		 AND username_pegawai = '$username'
			");

		$dokter = $query->row_array();

		return $dokter['nama_dokter'];
	}

	public function _detil_statistik_dokter($username_dokter = '', $search, $mulai = 0) {
		
		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_dokter'];
			$tgl_akhir = $search['tanggal_akhir_dokter'];

			$query1 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
				WHERE l.id = p.id_log_obat_keluar
				AND r.id_rekam_medis = rm.id_rekam_medis
				AND username_dokter = '$username_dokter'
				AND jenis = 'Resep'
				AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				GROUP BY nama_obat
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
				WHERE l.id = p.id_log_obat_keluar
				AND r.id_rekam_medis = rm.id_rekam_medis
				AND username_dokter = '$username_dokter'
				AND jenis = 'Resep'
				AND (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				GROUP BY nama_obat
				");
		}
		else {
			$query1 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
				WHERE l.id = p.id_log_obat_keluar
				AND r.id_rekam_medis = rm.id_rekam_medis
				AND username_dokter = '$username_dokter'
				AND jenis = 'Resep'
				GROUP BY nama_obat
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT nama_obat, SUM(jumlah) as total_pemakaian 
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_resep r, $this->t_rekam_medis rm
				WHERE l.id = p.id_log_obat_keluar
				AND r.id_rekam_medis = rm.id_rekam_medis
				AND username_dokter = '$username_dokter'
				AND jenis = 'Resep'
				GROUP BY nama_obat
				");
		}

		$hasil = array();
		$hasil['daftar_obats'] = $query1->result();
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

}

/* End of file statistik_model.php */
/* Location: ./application/models/statistik_model.php */