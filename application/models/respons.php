<?php
/* Author: Jorge Torres
 * Description: Pegawai model class
 */
class respons extends CI_Model{
	

  function __construct(){

      parent::__construct();
  }
  public function get_data($id)
  {
		$query = $this->db->query(
			"select
			  id, 
			  id_pasien, 
			  jadwal_harian, 
			  tanggal, 
			  respon_kepuasan
		 	from
			  ".SCHEMA."registrasi_pengobatan
			where
				id_pasien = '".$id."' AND
				respon_kepuasan IS NULL");
		return $query->result();
	}
	
	public function get_idreg($id_pasien, $tanggal){
		$query = $this->db->query("select id from ".SCHEMA."registrasi_pengobatan where id_pasien = '".$id_pasien."' and tanggal = '".$tanggal."'");
		return $query;
		
	}
	
	public function set_respons($value, $idregistrasi)
	{
		$this->db->set('respon_kepuasan',$value);
		$this->db->where('id', $idregistrasi);
		$this->db->update(SCHEMA."registrasi_pengobatan");
	}
}
