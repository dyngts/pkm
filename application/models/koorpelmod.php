<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Koorpelmod extends CI_Model {
	function __construct() {
			parent:: __construct();
	}

	public function create_tindakan($dota) {	
		$this->db->insert(SCHEMA.'tindakan',$dota);
		return TRUE;
	}


	public function search_tindakan($coty) {
		$query = $this->db->query("SELECT * FROM ".SCHEMA." TINDAKAN WHERE upper(NAMA) LIKE '%".$coty."%' OR lower(NAMA) LIKE '%".$coty."%' OR NAMA LIKE '%".$coty."%'");
		return $query;
	}

	public function search_diagnosis($coty) {
		$query = $this->db->query("SELECT * FROM ".SCHEMA." DIAGNOSIS WHERE upper(jenis_penyakit) LIKE '%".$coty."%' OR lower(jenis_penyakit) LIKE '%".$coty."%' OR jenis_penyakit LIKE '%".$coty."%'");
		return $query;
	}

	public function retrieveTindakan($num, $offset) {
		$this->db->order_by("nama", "asc"); 
		$query = $this->db->get(SCHEMA.'tindakan',$num, $offset);
		return $query;
	}
	
	function retrieveDiagnosis($num, $offset){
		$this->db->order_by("jenis_penyakit", "asc"); 
		$query = $this->db->get(SCHEMA.'diagnosis',$num, $offset);
		return $query;
	}


	public function searchbyId($idtindakan) {
		$this->db->select('*');
		$this->db->from(SCHEMA.'tindakan');
		$this->db->where('id',$idtindakan);
		$query = $this->db->get();
		return $query;
	}

	public function searchbyKode($kodediagnosis){
		$this->db->select('*');
		$this->db->from(SCHEMA.'diagnosis');
		$this->db->where('kode',$kodediagnosis);
		$query = $this->db->get();
		return $query;
	}

	public function updatetindakan($cotba) {
		$this->db->where('id', $cotba['id']);
		$this->db->update(SCHEMA.'tindakan', $cotba); 
		return TRUE;
	}

	public function deletetindakan($cotba) {
		$this->db->where('id', $cotba['id']);
		$this->db->delete(SCHEMA.'tindakan', $cotba); 
		return TRUE;
	}

	public function create_diagnosis($data){
		$this->db->insert(SCHEMA.'diagnosis',$data);
		return TRUE;
	}

	public function updatediagnosis($arraydata){
		$this->db->where('kode', $arraydata['kode']);
		$this->db->update(SCHEMA.'diagnosis', $arraydata); 
		return TRUE;
	}

	public function deletediagnosis($arraydata){
		$this->db->where('kode', $arraydata['kode']);
		$this->db->delete(SCHEMA.'diagnosis', $arraydata); 
		return TRUE;
	}

	/*function getcatatan($data) {
		$this->db->select('*');
		$this->db->from('catatan_tindakan');
		$this->db->where('id_tindakan', $data);
		$hasil = $this->db->get();
		if($hasil->num_rows() > 0) {
			RETURN TRUE;
		}
		else RETURN FALSE;
	}*/
}