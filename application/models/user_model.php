<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan mengambil data seorang user
 */
class User_model extends MY_Model{

  function __construct() {
      parent::__construct();
  }

  function insert_pegawai($data) {
    $table = 'pegawai';
    return $this->insert($table,$data);
  }

  function insert_pasien($data) {
    $table = 'pasien';
    return $this->insert($table,$data);
  }

  function update_pegawai($data) {
    $table = 'pegawai';
    return $this->update($table,$data);
  }

  function update_pasien($data) {
    $table = 'pasien';
    return $this->update($table,$data);
  }

  function insert($table,$data) {
    $table_auth = "pengguna";
    if ($table == "pasien") {
      // $data['data_general']['id'] = $this->create_id($data['data_general']['jenis_pasien'], "false");
      if ($data['data_general']['id'] == FALSE) return FALSE;
      else {
        $this->db->insert(SCHEMA.$table_auth,$data['data_auth']);
        $this->db->insert(SCHEMA.$table,$data['data_general']);
        return TRUE;
      }
    }
    elseif ($table == "pegawai") {
      $jenis_user = $data['data_general']['id_otorisasi'];
      $status_verifikasi = "false";
      $data['data_general']['nomor'] = $this->create_id($jenis_user, $status_verifikasi);
      if ($data['data_general']['nomor'] == FALSE) return FALSE;
      else {
        $this->db->insert(SCHEMA.$table_auth,$data['data_auth']);
        $this->db->insert(SCHEMA.$table,$data['data_general']);
      return TRUE;
      }
    }
    else return FALSE;
  }

  function verify($table,$data) {

    date_default_timezone_set('Asia/Jakarta');
    $date = new DateTime();
    $time = $date->format('Y-m-d H:i:s.u');

    $table_auth = "pengguna";
    if ($data != NULL) {
      if ($table == "pegawai") $jenis_user = 4;
      else $jenis_user = $data['jenis_pasien'];
      $status_verifikasi = "TRUE" ;

        $id = $this->create_id($jenis_user,$status_verifikasi);

      if ($jenis_user <= 3)
       $username = $id;
      else $username = $data['username'];

      if ($table == "pasien") {
        $dataupdate = array(
          'id' => $id,
          'status_verifikasi' => $status_verifikasi,
          'waktu_pendaftaran' => $time,
          'username' => $username,
        );
        $newpassword = $this->random_string(6);
        $data_in = array(
          'id' => $id,
          'username' => $username,
          'password' => $newpassword,
        );
      }
      elseif ($table == "pegawai") {
        $dataupdate = array(
          'nomor' => $id,
          'status_aktif' => $status_verifikasi,
        );
        $newpassword = $this->random_string(6);
        $data_in = array(
          'id' => $id,
          'username' => $username,
          'password' => $newpassword,
        );
      }
      else return FALSE;

      if ($this->is_verified($data['username'])) return FALSE;
      else {
        $this->db->where('username',$data['username']);
        $this->db->update(SCHEMA.$table,$dataupdate);
        if ($table == "pasien") {
          $this->db->where('username',$data['username']);
          $this->db->update(SCHEMA.$table_auth,array('username' => $username,)); 
        }
        $this->updatepassword($data_in);
        return $data_in;
      }
      return TRUE;
    }
    else return FALSE;
  }

  /**
   * Memeriksa apakah pengguna (pasien/pegawai) telah diverifikasi
   * @param  [string] $username [username pengguna]
   * @return [boolean]           [true jika sudah diverifikasi, false jika tidak]
   */
  function is_verified($username) {
    $table1 = "pasien";
    $table2 = "pegawai";

    $this->db->where('username',$username);
    $this->db->where('status_verifikasi',"true");
    $query = $this->db->get(SCHEMA.$table1);
    if ($query->num_rows() > 0) {
      return TRUE;
    }
    else {
      $this->db->where('username',$username);
      $this->db->where('status_aktif',"true");
      $query = $this->db->get(SCHEMA.$table2);
      if ($query->num_rows() > 0) {
        return TRUE;
      }
      else return FALSE;
    }
  }

  function update($table,$data) {
    $table_auth = "pengguna";
    if ($table == "pegawai") {
      
      $this->db->where('username',$data['data_id']['username']);
      $this->db->update(SCHEMA.$table_auth,$data['data_auth']);
      
      $this->db->where('nomor',$data['data_id']['id']);
      $this->db->update(SCHEMA.$table,$data['data_general']);
      
      return TRUE;
    }
    elseif ($table == "pasien") {

      $this->db->where('username',$data['data_id']['username']);
      $this->db->update(SCHEMA.$table_auth,$data['data_auth']);

      $this->db->where('id',$data['data_id']['id']);
      $this->db->update(SCHEMA.$table,$data['data_general']);

      return TRUE;
    }
    else return FALSE;
  }

  function del($id) {
    $userdata = $this->get_user_by_id($id);
    if($userdata != NULL) return $this->delele($userdata);
    else return false;
  }

  function delete($userdata) {
    if ($userdata['otorisasi_id'] <= 3) {
      return $this->delete_user('pasien', $userdata);
    }
    elseif ($userdata['otorisasi_id'] > 3) {
      return $this->delete_user('pegawai', $userdata);
    }
    else return FALSE;
  }

  private function delete_user($table,$data_in) {
    $table_auth = "pengguna";
    if ($table == "pegawai") {
      $data['status_aktif'] = "false";
      $data['nomor'] = $this->create_id($data_in['otorisasi_id'], 'false');
      $data['username'] = $data_in['username'];
      $this->db->where('nomor',$data_in['nomor']);
      $this->db->update(SCHEMA.$table,$data);

      $data2['username'] = $data['username'];
      $this->db->where('username',$data_in['username']);
      $this->db->update(SCHEMA.$table_auth,$data2);

      return TRUE;
    }
    elseif ($table == "pasien") {
      $verified = $data_in['status_verifikasi'];
      if ($verified == 't') {

        $data['status_verifikasi'] = "false";
        $data['id'] = $this->create_id($data_in['otorisasi_id'], 'false');
        $data['username'] = $data_in['id'];
        $this->db->where('id',$data_in['id']);
        $this->db->update(SCHEMA.$table,$data);

        $data2['username'] = $data['username'];
        $this->db->where('username',$data_in['username']);
        $this->db->update(SCHEMA.$table_auth,$data2);

        return TRUE;
      }
      else {        
        return $this->delete_permanently($table,$data_in);
      }
    }
    else return FALSE;
  }

  private function delete_permanently($table,$data) {
    $table_auth = "pengguna";
    if ($table == "pegawai") {
      $this->db->delete(SCHEMA.$table, array('nomor' => $data['nomor']));
      $this->db->delete(SCHEMA.$table_auth, array('username' => $data['username']));
      return TRUE;
    }
    elseif ($table == "pasien") {
      $this->db->delete(SCHEMA.$table, array('id' => $data['id']));
      $this->db->delete(SCHEMA.$table_auth, array('username' => $data['username']));
      return TRUE;
    }
    return FALSE;
  }

  function is_unique_username($username) {
    $table = "pengguna";
    $column = "username";
    $str = $username;
    return $this->is_unique($table,$column,$str);
  }

  function is_unique($table,$column,$str) {
    $this->db->where($column,$str);
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0) {
      return FALSE;     
    }
    else {
      return TRUE;
    }
  }

  /**
   * [create_id description]
   * @param  varchar[1] $tipe              [description]
   * @param  [type] $status_verifikasi [description]
   * @return [type]                    [description]
   */
  function create_id($jenis_user,$status_verifikasi) {

    if ($status_verifikasi == "false") {
      if ($jenis_user > 3) $prefix = "X"; else $prefix = "Y";
    } 
    elseif ($jenis_user == 1) $prefix = "M";
    elseif ($jenis_user == 2) $prefix = "K";
    elseif ($jenis_user == 3) $prefix = "U";
    elseif ($jenis_user > 3) $prefix = "P";
    else $prefix = "Z";

    if ($jenis_user <= 3) {
      $last_id = $this->get_last_id_pasien($jenis_user,$status_verifikasi);
      $last_id = substr($last_id, -5);
      $last_id++;
      $new_id = $prefix.$last_id;
    }
    elseif ($jenis_user > 3) {
      $last_id = $this->get_last_id_pegawai($status_verifikasi);
      $last_id = substr($last_id, -5);      
      $last_id++;
      $new_id = $prefix.$last_id;
    }  
    else return FALSE;
    return $new_id;
  }

  function get_last_id_pasien($jenis_pasien,$status_verifikasi) {
    $table = "pasien";
    if ($status_verifikasi == 'TRUE')
      $this->db->where('jenis_pasien',$jenis_pasien);
    $this->db->where('status_verifikasi',$status_verifikasi);
    $this->db->order_by('id','desc');
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0) {
      $result = $query->row_array();
      $last_id = $result['id'];            
    }
    else {
      $last_id = "Y10000";
    }
    return $last_id;
  }

  function get_last_id_pegawai($status_aktif) {
    $table = "pegawai";
    if ($status_aktif == 't')
      $this->db->where('status_aktif',$status_aktif);
    $this->db->order_by('nomor', 'desc');
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0) {
      $result = $query->row_array();
      $last_id = $result['nomor'];  
    }
    else {
      $last_id = "X10000";
    }
    return $last_id;
  }

  function get_user_name_by_id($id) {
    $username = $this->get_username_by_id($id);
    $role = $this->get_user_name_by_username($username);
    return $role;
  }

  function get_name($id) {
    return $this->get_user_name_by_id($id);
  }

  function get_user_username_by_id($id) {
    return $this->get_username_by_id($id);
  }

  /**
   * Mengambil username pengguna berdasarkan id pengguna
   * @param  [string] $user_id [id pengguna yang dicari]
   * @return [string] $username [username pengguna]
   */
  function get_username_by_id($user_id) {
    $table1 = "pasien";
    $table2 = "pegawai";
    $attributes_in1 = array('id' => $user_id,);
    $attributes_in2 = array('nomor' => $user_id,);
    $attributes_out1 = array('username',);
    $attributes_out2 = array('username');

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $username = $result[$attributes_out1[0]];
    }
    if ($result == NULL) {
      $result = $this->get_attributes_by_attributes_on_table($table2,$attributes_in2,$attributes_out2);
      $username = $result[$attributes_out2[0]];
    }
    if ($result == NULL) return "";
    else return $username;
  }

  function get_id_by_username($username) {
    $table1 = "pasien";
    $table2 = "pegawai";
    $attributes_in1 = array('username' => $username,);
    $attributes_in2 = array('username' => $username,);
    $attributes_out1 = array('id',);
    $attributes_out2 = array('nomor');

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $id = $result[$attributes_out1[0]];
    }
    if ($result == NULL) {
      $result = $this->get_attributes_by_attributes_on_table($table2,$attributes_in2,$attributes_out2);
      $id = $result[$attributes_out2[0]];
    }
    if ($result == NULL) return "";
    else return $id;
  }

  function get_user_name_by_username($username) {
    $table1 = "pasien";
    $table2 = "pegawai";
    $attributes_in1 = array('username' => $username,);
    $attributes_in2 = array('username' => $username,);
    $attributes_out1 = array('nama',);
    $attributes_out2 = array('nama',);

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $id = $result[$attributes_out1[0]];
    }
    if ($result == NULL) {
      $result = $this->get_attributes_by_attributes_on_table($table2,$attributes_in2,$attributes_out2);
      $id = $result[$attributes_out2[0]];
    }
    if ($result == NULL) return "";
    else return $id;
  }

  function get_user_role_by_username($username) {
    $table1 = "pengguna";
    $attributes_in1 = array('username' => $username);
    $attributes_out1 = array('id_otorisasi',);

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $role = $result[$attributes_out1[0]];
    }
    if ($result == NULL) return "";
    else return $role;
  }

  function get_user_role_name_by_username($username) {
    $role = $this->get_user_role_by_username($username);

    $table1 = "otorisasi";
    $attributes_in1 = array('otorisasi_id' => $role,);
    $attributes_out1 = array('jabatan',);

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $rolename = $result[$attributes_out1[0]];
    }
    if ($result == NULL) return "";
    else return $rolename;
  }

  function get_user_role($id) {
    return $this->get_user_role_by_id($id);
  }

  function get_user_role_by_id($id) {
    $username = $this->get_username_by_id($id);
    $role = $this->get_user_role_by_username($username);
    return $role;
  }

  function get_user_role_name_by_id($id) {
    $username = $this->get_username_by_id($id);
    $rolename = $this->get_user_role_name_by_username($username);
    return $rolename;
  }

  function get_user_password_by_id($id) {
    $username = $this->get_username_by_id($id);

    $table1 = "pengguna";
    $attributes_in1 = array('username' => $username,);
    $attributes_out1 = array('password');

    $result = $this->get_attributes_by_attributes_on_table($table1,$attributes_in1,$attributes_out1);
    if ($result != NULL) {
      $password = $result[$attributes_out1[0]];
    }
    return $password;
  }

  /**
   * Mengambil data pengguna berdasarkan username
   * @param  [string] $username [username pengguna]
   * @return [array]           [array berupa data pengguna]
   */
  function get_user_by_username($username) {
    $id = $this->get_id_by_username($username);
    $userdata = $this->get_user_by_id($id);
    return $userdata;
  }

  function get_userdata($id) {
    return $this->get_user_by_id($id);
  }

  /**
   * Mengambil data pengguna berdasarkan id
   * @param  [string] $id [id pengguna]
   * @return [array]           [array berupa data pengguna]
   */
  function get_user_by_id($id) {
    $table0 = SCHEMA."otorisasi";
    $table1 = SCHEMA."pasien";
    $table2 = SCHEMA."pegawai";
    $username = $this->get_username_by_id($id);
    if ($username == "") return NULL;

    $this->db->where('username',$username);
    $this->db->join($table2,$table2.'.id_otorisasi = '.$table0.'.otorisasi_id');
    $query = $this->db->get($table0);
    if ($query->num_rows() > 0) {
      $userdata = $query->row_array();
      return $userdata;
    }
    else {
      $this->db->where('username',$username);
      $this->db->join($table1,$table1.'.jenis_pasien = '.$table0.'.otorisasi_id');
      $query = $this->db->get($table0);
      if ($query->num_rows() > 0) {
        $userdata = $query->row_array();
        return $userdata;
      }
      else return NULL;
    }
  }

  function get_all_pasien() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = true;
    $data['tipe'] = "all";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "";
    $data['order_type'] = "";

    return $this->get_all($data);
  }

  function get_role_list() {
    $table = 'otorisasi';
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0)
      return $query->result_array();
    else return NULL;
  }

  function get_limited_pasien($num_of_row,$offset) {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = true;
    $data['tipe'] = "lim";

    $data['num_of_row'] = $num_of_row;
    $data['offset'] = $offset;

    $data['attr_order'] = "waktu_pendaftaran";
    $data['order_type'] = "desc";

    return $this->get_all($data);
  }

  function get_num_of_pasien() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = true;
    $data['tipe'] = "num";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "waktu_pendaftaran";
    $data['order_type'] = "desc";

    return $this->get_all($data);
  }

  function get_all_calon_pasien() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = false;
    $data['tipe'] = "all";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "";
    $data['order_type'] = "";

    return $this->get_all($data);
  }

  function get_limited_calon_pasien($num_of_row,$offset) {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = false;
    $data['tipe'] = "lim";

    $data['num_of_row'] = $num_of_row;
    $data['offset'] = $offset;

    $data['attr_order'] = "waktu_pendaftaran";
    $data['order_type'] = "desc";

    return $this->get_all($data);
  }

  function get_num_of_calon_pasien() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pasien";
    $data['attr1'] = "jenis_pasien";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pasien";
    $data['verified'] = false;
    $data['tipe'] = "num";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "waktu_pendaftaran";
    $data['order_type'] = "desc";

    return $this->get_all($data);
  }

  function get_all_pegawai() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pegawai";
    $data['attr1'] = "id_otorisasi";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pegawai";
    $data['verified'] = true;
    $data['tipe'] = "all";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "";
    $data['order_type'] = "";

    return $this->get_all($data);
  }

  function get_limited_pegawai($num_of_row,$offset) {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pegawai";
    $data['attr1'] = "id_otorisasi";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pegawai";
    $data['verified'] = true;
    $data['tipe'] = "lim";

    $data['num_of_row'] = $num_of_row;
    $data['offset'] = $offset;

    $data['attr_order'] = "id_otorisasi";
    $data['order_type'] = "asc";

    return $this->get_all($data);
  }

  function get_num_of_pegawai() {
    $data['table0'] = SCHEMA."otorisasi";
    $data['table1'] = SCHEMA."pegawai";
    $data['attr1'] = "id_otorisasi";
    $data['attr2'] = "otorisasi_id";
    $data['jenis_user'] = "pegawai";
    $data['verified'] = true;
    $data['tipe'] = "num";

    $data['num_of_row'] = "";
    $data['offset'] = "";

    $data['attr_order'] = "";
    $data['order_type'] = "";

    return $this->get_all($data);
  }

  function get_all($data) {

    $table0 = $data['table0'];
    $table1 = $data['table1'];
    $attr1 = $data['attr1'];
    $attr2 = $data['attr2'];

    $this->db->join($table1,$table1.'.'.$attr1.' = '.$table0.'.'.$attr2);

    if ($data['jenis_user'] == "pasien")
    {
      if ($data['verified'] == true)
      {
        $this->db->where('status_verifikasi', 'TRUE');
      }
      elseif ($data['verified'] == false)
      {
        $this->db->where('status_verifikasi', 'FALSE');
      }
      $this->db->where('otorisasi_id <=', 3);
    }
    elseif ($data['jenis_user'] == "pegawai")
    {
      if ($data['verified'] == true)
      {
        $this->db->where('status_aktif', 'TRUE');
      }
      elseif ($data['verified'] == false)
      {
        $this->db->where('status_aktif', 'FALSE');
      }
      $this->db->where('otorisasi_id >', 3);
    }

    if ($data['tipe'] == "lim") 
    {
      $this->db->limit($data['num_of_row'], $data['offset']);
      $this->db->order_by($data['attr_order'], $data['order_type']);
    }
    $query = $this->db->get($table0);

    //tipe
    if ($data['tipe'] == "all") 
    {  
      if ($query->num_rows() > 0) 
      {
        $userdata = $query->result_array();
        return $userdata;
      }
      else return NULL;
    }
    elseif ($data['tipe'] == "num") 
    {
      return $query->num_rows();
    }
    elseif ($data['tipe'] == "lim") 
    {
      if ($query->num_rows() > 0) {
        $userdata = $query->result();
        return $userdata;
      }
      else return NULL;
    }
    else return NULL;
  }

  function get_all_doctor()
  {
    $table1 = SCHEMA."pegawai";
    $table2 = SCHEMA."otorisasi";

    $data['base_table'] = $table1;

    $data['join'] = array(
      array('table' => $table2, 'const' => $table1.".id_otorisasi"." = ".$table2.".otorisasi_id", )
      );
    $data['const'] = array(
      'otorisasi_id >= 8 AND otorisasi_id <= 10',
      );

    return $this->get_datas($data);
  }

  /**
   * Mengambil data-data pengguna berdasarkan atribut tertentu
   * @param  [string] $table      [nama table]
   * @param  [array] $attributes [attribut(s) yang discocokkan]
   * @return [array]             [data-datanya]
   */
  function get_data_by_attributes_on_table($table,$attributes) {
    $this->db->where($attributes);
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0)
      return $query->row_array();
    else return NULL;
  }

  /**
   * Mengambil atribut pengguna berdasarkan atribut tertentu
   * @param  [string] $table      [nama table]
   * @param  [array] $attributes_in [attribut(s) yang dicocokkan]
   * @param  [array] $attributes_out [attribut(s) yang diambil]
   * @return [array]             [data-datanya]
   */
  function get_attributes_by_attributes_on_table($table,$attributes_in,$attributes_out) {
    $this->db->select($attributes_out);
    $this->db->where($attributes_in);
    $query = $this->db->get(SCHEMA.$table);
    if ($query->num_rows() > 0)
      return $query->row_array();
    else return NULL;
  }

  /**
   * Mengambil seluruh atribut seorang user
   * @param  String $id id user
   * @return query     data lengkap seorang user
   */
  /*function get_userdata($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('pasien');
    return $query->row_array();
  }*/

  /**
   * Mengambil nama user
   * @param  String $id id user
   * @return String     nama lengkap user
   */
  /*function get_name($id) {

    $this->load->database();
    $this->db->where('id', $id);
    $query = $this->db->get('pasien');
    $row = $query->row(); 

    $nama = $row->nama;

    return $nama;
  }*/

  /**
   * Mengambil role/peran user
   * @param  String $id id user
   * @return String     peran user
   */
  /*function get_user_role($id) {

    $this->load->database();
    $this->db->where('id', $id);
    $query = $this->db->get('pasien');
    $row = $query->row(); 

    $role = $row->peran;

    return $role;
  }*/

  /**
   * Mengambil id user
   * @return String id user
   */
  function get_my_id() {
    return $this->session->userdata('id');
  }

  function get_my_username() {
    return $this->session->userdata('username');
  }

  function get_my_name() {
    return $this->session->userdata('name');
  }

  function get_my_role() {
    return $this->session->userdata('role');
  }

  function get_my_role_name() {
    $id = $this->get_my_id();
    $rolename = $this->get_user_role_name_by_id($id);
    return $rolename;
  }

  function get_my_data() {
    $id = $this->get_my_id();
    $data = $this->user_model->get_user_by_id($id);
    return $data;
  }

  /**
   * Mengambil seluruh atribut seorang calon pasien
   * @param  String $id id calon pasien
   * @return query     data lengkap seorang calon pasien
   */
  function get_calon_pasien_data($id) {

    return $this->get_userdata($id);
  }

  /**
   * Mengambil role/peran calon pasien
   * @param  String $id id calon pasien
   * @return String     peran calon pasien
   */
  function get_calon_pasien_role($id) {
    return $this->get_user_role_by_id($id);
  }

  function updatepassword($data_in)
  {
    $table = SCHEMA.'pengguna';

    $id = $data_in['id'];
    $username = $data_in['username'];
    $newpassword = $data_in['password'];

    $data = array(
        'password'=>md5($newpassword),
      );
    
    $this->db->where('username', $username);
    $this->db->update($table, $data); 
  }

  public function random_string($length) 
  {
    $key = "";
    $keys = range('a', 'z');
    $keys0 = array('c','r','q','f','x','v','z');
    $keys1 = array('a','i','u','e','o');
    $keys2 = array_diff($keys, array_merge($keys0,$keys1));
    
    for($i=0; $i < $length; $i++) {
        if ($i%2 == 1) {
          $key = $key.$keys1[array_rand($keys1)];
        }
        else {
          $key = $key.$keys2[array_rand($keys2)];
        }
    }
    return $key;
  }

  public function set_last_url($url)
  {
    $data = array('last_url' => $url) ;
    $this->session->set_userdata($data);
  }

  public function get_last_url()
  {
    return $this->session->userdata('last_url');
  }

}
?>
           
