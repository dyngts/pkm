<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Class User Model manages database manipulation
 * have been registered, retrieve, edit, delete user's profile
 * checked for validate user, available username,
 */

class antrian_model extends MY_Model {
	function __construct() {
        parent::__construct();
    }

	function get_all_antrian() {
        $this->load->database();
        
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
        $this->db->select('
          registrasi_pengobatan.id, 
          registrasi_pengobatan.status, 
          registrasi_pengobatan.no_urutan, 
          registrasi_pengobatan.id_pasien, 
          registrasi_pengobatan.jadwal_harian, 
          registrasi_pengobatan.waktu_reg_awal, 
          registrasi_pengobatan.waktu_reg_akhir, 
          registrasi_pengobatan.respon_kepuasan, 
          registrasi_pengobatan.nomor_petugas, 
          jadwal_harian.id_mingguan, 
          jadwal_harian.tanggal, 
          jadwal_harian.waktu_aktual_awal, 
          jadwal_harian.waktu_aktual_akhir, 
          jadwal_mingguan.id_layanan, 
          jadwal_mingguan.hari,
          pasien.nama
          ');
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'pasien', 'pasien.id=registrasi_pengobatan.id_pasien');
        $this->db->join(SCHEMA.'jadwal_harian', 'jadwal_harian.id_mingguan=registrasi_pengobatan.jadwal_harian AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        $this->db->join(SCHEMA.'jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.id');
        $this->db->where('jadwal_harian.tanggal', $today);
        //$this->db->order_by("status desc, id asc");
        $this->db->order_by("status desc, no_urutan asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_no_antrian() {
        $this->load->database();
        $this->load->helper('date');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);

        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->where("(status!=1 AND status!=3 AND status!=2)");
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '1');
        $query = $this->db->get();
        $data[0] = $query->num_rows();
        
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->where("(status!=1 AND status!=3 AND status!=2)");
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '2');
        $query = $this->db->get();
        $data[1] = $query->num_rows();

        return $data;
    }
    function get_total_antrian() {
        $this->load->database();
        $this->load->helper('date');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);

        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '1');
        $query = $this->db->get();
        $data[0] = $query->num_rows();
        
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '2');
        $query = $this->db->get();
        $data[1] = $query->num_rows();

        return $data;
    }

    function get_last_antrian() {
        $this->load->database();
        $this->load->helper('date');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);

        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        
        $this->db->where("(status!=2)");
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '1');
        $query = $this->db->get();
        $data[0] = $query->num_rows()+1;
        
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        //$this->db->join('jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.jadwalmingguanid');
        //$this->db->join('klinik', 'jadwal_mingguan.klinik=klinik.tipe');
        $this->db->where("(status!=2)");
        $this->db->where('registrasi_pengobatan.tanggal', $today);
        $this->db->join(SCHEMA.'jadwal_mingguan', 'registrasi_pengobatan.jadwal_harian = jadwal_mingguan.id');
$this->db->where('jadwal_mingguan.id_layanan', '2');
        $query = $this->db->get();
        $data[1] = $query->num_rows()+1;

        return $data;
    }
    function update_antrian($data, $id) {
        $this->load->database();
    	$this->db->where('id', $id);
    	$this->db->update(SCHEMA.'registrasi_pengobatan', $data);
        return true;
    }
    function add_antrian($data) {
        $this->load->database();

        $this->db->from(SCHEMA.'pasien');
        $this->db->where('id', $data['id_pasien']);
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            $this->db->from(SCHEMA.'registrasi_pengobatan');
            $this->db->where('tanggal', $data['tanggal']);
            $this->db->where('id_pasien', $data['id_pasien']);
            $this->db->where("(status!=1 AND status!=6)");
            $query = $this->db->get();
            if ($query->num_rows()>0) {
                return $msg="
                  <div class=\"row\">
                    <div class=\"message message-red\">
                      <p class=\"p_message\">Pasien sudah terdaftar dalam registrasi berobat!</p>
                    </div>
                  </div>";
            } else {
                //pastiin $data lengkkap
                $this->db->insert(SCHEMA.'registrasi_pengobatan',$data);
                return $msg="
                  <div class=\"row\">
                    <div class=\"message message-green\">
                      <p class=\"p_message\">Pasien berhasil ditambahkan ke dalam antrean!</p>
                    </div>
                  </div>";
            }
        } else {
            return $msg="
                  <div class=\"row\">
                    <div class=\"message message-red\">
                      <p class=\"p_message\">Pasien tidak terdaftar!</p>
                    </div>
                  </div>";;
        }
    }

    function terdaftar_umum()
    {
        $this->load->database();
        $this->load->model('user_model');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
        $this->db->select('registrasi_pengobatan.id');
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_mingguan','jadwal_harian = jadwal_mingguan.id');
        $this->db->join(SCHEMA.'layanan','jadwal_harian = jadwal_mingguan.id');
        $this->db->where('layanan.id', '1');
        $this->db->where('tanggal', $today);
        $this->db->where('id_pasien', $this->user_model->get_my_id());
        $this->db->where("(status!=1 AND status!=6)");
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            $result = $query->row_array();
            return $result['id'];
        } else {
            return "";
        }
    }

    function terdaftar_gigi()
    {
        $this->load->database();
        $this->load->model('user_model');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
        $this->db->select('registrasi_pengobatan.id');
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_mingguan','jadwal_harian = jadwal_mingguan.id');
        $this->db->join(SCHEMA.'layanan','jadwal_harian = jadwal_mingguan.id');
        $this->db->where('layanan.id', '2');
        $this->db->where('tanggal', $today);
        $this->db->where('id_pasien', $this->user_model->get_my_id());
        $this->db->where("(status!=1 AND status!=6)");
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            $result = $query->row_array();
            return $result['id'];
        } else {
            return "";
        }
    }


    function is_terdaftar() {
        $this->load->database();
        $this->load->model('user_model');
        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d";
        $time = time();
        $today = mdate($datestring, $time);
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->where('tanggal', $today);
        $this->db->where('id_pasien', $this->user_model->get_my_id());
        $this->db->where("(status!=1 AND status!=6)");
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return true;
        } else {
            return false;
        }
    }
    function is_terdaftar_gigi() {
        $this->load->database();
        $this->load->model('user_model');
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        //where tgl skrg
        $this->db->where('id_pasien', $this->user_model->get_my_id());
        $this->db->where('poli', 'Gigi');
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            return true;
        } else {
            return false;
        }
    }

    function delete_antrian($user) {
        $this->load->database();
        $this->db->delete(SCHEMA.'registrasi_pengobatan', $user);
    }

    function get_jml_antrian() {
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        $string = "Y-m-d";
        $time = time();
        $tanggal = date($string, $time);
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        $this->db->join(SCHEMA.'jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.id');
        $this->db->where('id_layanan', '1');
        $this->db->where('registrasi_pengobatan.tanggal', $tanggal);
        $this->db->where("(status!=2)");
        $query = $this->db->get();
        $data[0] = $query->num_rows();
        /*
        $data[0] = 8;
        */
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        $this->db->join(SCHEMA.'jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.id');
        $this->db->where('id_layanan', '2');
        $this->db->where('registrasi_pengobatan.tanggal', $tanggal);
        $this->db->where("(status!=1)");
        $query = $this->db->get();
        $data[1] = $query->num_rows();
        /*
        $data[1] = 7;
        */
        return $data;
    }
    function get_jml_pengantre() {
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        $string = "%Y-%m-%d";
        $time = time();
        $tanggal = mdate($string, $time);
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        $this->db->join(SCHEMA.'jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.id');
        $this->db->where('id_layanan', '1');
        $this->db->where('registrasi_pengobatan.tanggal', $tanggal);
        $this->db->where("(status=3)");
        $query = $this->db->get();
        $data[0] = $query->num_rows();
        /*
        $data[0] = 8;
        */
        $this->db->from(SCHEMA.'registrasi_pengobatan');
        $this->db->join(SCHEMA.'jadwal_harian', 'registrasi_pengobatan.jadwal_harian=jadwal_harian.id_mingguan AND registrasi_pengobatan.tanggal=jadwal_harian.tanggal');
        $this->db->join(SCHEMA.'jadwal_mingguan', 'jadwal_harian.id_mingguan=jadwal_mingguan.id');
        $this->db->where('id_layanan', '2');
        $this->db->where('registrasi_pengobatan.tanggal', $tanggal);
        $this->db->where("(status=3)");
        $query = $this->db->get();
        $data[1] = $query->num_rows();
        /*
        $data[1] = 7;
        */
        return $data;
    }

    function estimasi_waktu() {
        $this->load->database();
        $data = $this->get_jml_pengantre();
        $waktu[0] = 15*$data[0];
        if ($waktu[0] > 59 && $waktu[0]%60 != 0) {
           $waktu[0] = "Estimasi: ".floor($waktu[0]/60)." Jam ".($waktu[0]%60)." Menit.";
        } else if ($waktu[0] > 59 && $waktu[0]%60 == 0) {
            $waktu[0] = "Estimasi: ".($waktu[0]/60)." Jam.";
        } else $waktu[0] = "Estimasi: ".($waktu[0]%60)." Menit.";
 
        $waktu[1] = 30*$data[1];
        if ($waktu[1] > 59 && $waktu[1]%60 != 0) {
           $waktu[1] = "Estimasi: ".floor($waktu[1]/60)." Jam ".($waktu[1]%60)." Menit.";
        } else if ($waktu[1] > 59 && $waktu[1]%60 == 0) {
            $waktu[1] = "Estimasi: ".($waktu[1]/60)." Jam.";
        } else $waktu[1] = "Estimasi: ".($waktu[1]%60)." Menit.";
    
        return $waktu;
    }

    function ekspor_data_berobat()
    {
        $this->load->database();

        $this->db->from('antrian');
        $this->db->where('status', '3');
        $query = $this->db->get();

        date_default_timezone_set('Asia/Jakarta');
        $datestring = "%Y-%m-%d %h:%m:%s";
        $time = time();
        $today = mdate($datestring, $time);
        if (isset($query)) {
            foreach ($query as $row) {
                $record = array('time_stamp' => $today,
                                'id_pasien' => $row->id_pasien,
                                'poli' => $row->poli
                );
                $this->db->insert('registrasi_pengobatan', $record);
            }
            $this->db->delete('antrian');
            return true;
        } else return false;
    }

	public function get_regob($id) {
		$this->load->database();
		$this->db->select('id_pasien');
		$this->db->from(SCHEMA.'registrasi_pengobatan');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query;
	}

  function get_urutan_umum()
  {
    $no_urut = $this->get_last_antrian_umum();
    return ++$no_urut;
  }

  function get_last_antrian_umum()
  {
    $tipe_poli = '1';

    $data['const'] = array(
      'id_layanan ='.$tipe_poli,
      );
       
    return $this->get_last_antrian_on_poli($data);
  }

  function get_urutan_gigi()
  {
    $no_urut = $this->get_last_antrian_gigi();
    return ++$no_urut;
  }

  function get_last_antrian_gigi()
  {
    $tipe_poli = '2';

    $data['const'] = array(
      'id_layanan ='.$tipe_poli,
      );

    return $this->get_last_antrian_on_poli($data);
  }

  function get_last_antrian_on_poli($data)
  {
    date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);

    $data['const'] = $data['const'];
    $data['const'][] = "tanggal = '".$tanggal."'";

    $data['base_table'] = SCHEMA.'registrasi_pengobatan';
    $data['join'] = array(
      array(
        'table' => SCHEMA.'jadwal_mingguan',
        'const' => 'jadwal_harian = jadwal_mingguan.id',
        )
      );
    $data['order_by'] = array(
      array(
        'attrb' => "no_urutan",
        'type' => "desc",
        )
      );

    $result = $this->get_data($data);
    $no_urut = $result['no_urutan'];
    if($no_urut == NULL) $no_urut = "0";
    return $no_urut;
  }



















  function get_umum_harian()
  {
    $poli = "1";
    $data['const'] = array(
      'id_layanan = '.$poli,
      );
    return $this->get_registrasi_harian($data);
  }

  function get_gigi_harian()
  {
    $poli = "2";
    $data['const'] = array(
      'id_layanan = '.$poli,
      );
    return $this->get_registrasi_harian($data);
  }

  function get_estetika_harian()
  {
    $poli = "3";
    $data['const'] = array(
      'id_layanan = '.$poli,
      );
    return $this->get_registrasi_harian($data);
  }

  // get semua
  private function get_registrasi_harian($data)
  {
    $data['select'] = array(
  'registrasi_pengobatan.id as reg_id', 
  'registrasi_pengobatan.status', 
  'registrasi_pengobatan.no_urutan', 
  'registrasi_pengobatan.id_pasien', 
  'registrasi_pengobatan.jadwal_harian', 
  'registrasi_pengobatan.waktu_reg_awal', 
  'registrasi_pengobatan.waktu_reg_akhir', 
  'registrasi_pengobatan.respon_kepuasan', 
  'registrasi_pengobatan.nomor_petugas', 
  'jadwal_harian.id_mingguan', 
  'jadwal_harian.tanggal', 
  'jadwal_harian.waktu_aktual_awal', 
  'jadwal_harian.waktu_aktual_akhir', 
  'layanan.gate_antrian', 
  'layanan.nama as l_nama',
  'jadwal_mingguan.id_layanan', 
  'jadwal_mingguan.hari', 
  'pegawai.nomor', 
  'pegawai.nama as p_nama',
      );
    $data['base_table'] = SCHEMA."jadwal_harian";
    $data['join'] = array(
      array(
        'table' => SCHEMA."dokter_harian",
        'const' => 'id_harian = id_mingguan',
        ),
      array(
        'table' => SCHEMA."pegawai",
        'const' => 'username_dokter = nomor',
        ),
      array(
        'table' => SCHEMA."registrasi_pengobatan",
        'const' => 'jadwal_harian = id_mingguan',
        ),
      array(
        'table' => SCHEMA."jadwal_mingguan",
        'const' => 'id_mingguan = jadwal_mingguan.id',
        ),
      array(
        'table' => SCHEMA."layanan",
        'const' => 'id_layanan = layanan.id',
        ),
      );
    return $this->get_datas($data);
  }


    public function get_hide_selesai()
    {
        $hideselesai = $this->session->userdata('hideselesai') && TRUE;
        return $hideselesai;
    }
}
