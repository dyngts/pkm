<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Class User Model manages database manipulation
 * have been registered, retrieve, edit, delete user's profile
 * checked for validate user, available username,
 */

class klinik_model extends CI_Model {
	function __construct() {
        parent::__construct();
        $this->load->model('jadwal_model');
    }

	function update_status($poli)
    {
        $this->load->database();
        $data = array('status' => 'false');
        $this->db->where('tipe', $poli);
        $this->db->update('klinik', $data);
    }

    /*function update_status_umum()
    {
        $this->load->database();
        $data = array('poli_umum' => 'false');
        $this->db->update('status_antrian', $data);
    }*/

    function batal_tutup($poli)
    {
        $this->load->database();
        $data = array('gate_antrian' => 'true');
        $this->db->where('id', $poli);
        $this->db->update(SCHEMA.'layanan', $data);
    }
    
    function get_status($poli)
    {
        $this->load->database();
        $this->db->where('id', $poli);
        $query = $this->db->get(SCHEMA.'layanan');
        if ($query->num_rows() == 1)
        {
            $result = $query->row_array();
            return $result['gate_antrian'];
        }
        return FALSE;
    }

    function set_open($poli)
    {
        $this->load->database();
        $data = array('gate_antrian' => 'true');
        $this->db->where('id', $poli);
        $this->db->update(SCHEMA.'layanan', $data);
    }
    function set_all_open()
    {
        $this->load->database();
        $data = array('gate_antrian' => 'true');
        $this->db->update(SCHEMA.'layanan', $data);
    }
    function set_close($poli)
    {
        $this->load->database();
        $data = array('gate_antrian' => 'false');
        $this->db->where('id', $poli);
        $this->db->update(SCHEMA.'layanan', $data);
    }
    function is_tutup() {
        $this->load->database();
        $this->db->from(SCHEMA.'layanan');
        $this->db->where('gate_antrian', 'true');
        $query = $this->db->get();
        $data = $query->num_rows();
        if($data > '0') { 
            return false;
        } else return true;
    }
}