<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk mengambil daftar pasien dan penghapusan pasien.
 */
class Pasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    /**
     * Mengambil semua pasien dalam database
     * @return query daftar pasien
     */
    public function get_all_pasien(){
        
        $this->load->database();
        // Urutkan berdasarkan waktu pendaftaran secara descending
        $this->db->order_by("waktu_daftar", "desc");
        // Ambil dari database
        $query = $this->db->get('pasien');

        return $query;
    }

    /**
     * Mengambil pasien dalam database pada urutan tertentu
     * @param  integer $limit jumlah pasien yang diambil
     * @param  integer $start baris pertama dalam database yang diambil
     * @return query        daftar pasien mulai baris ke $start sebanyak $limit baris
     */
    public function get_all_pasien_limited($limit, $start){
        
        $this->load->database();
        // Tentukan batasan yang akan diambil
        $this->db->limit($limit, $start);
        // Urutkan berdasarkan waktu pendaftaran secara descending
        $this->db->order_by("waktu_daftar", "desc");
        // Ambil dari database 
        $query = $this->db->get('pasien');
        return $query;
    }
    
    /**
     * Menghapus pasien darti database
     * @param  String $id id pasien yang akan dihapus
     */
    public function deletepasien($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('pasien');
    }
}
?>