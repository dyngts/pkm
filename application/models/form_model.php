<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan registrasi pasien dan pengubahan password
 * Digunakan untuk mengambil masukan pada form dan mengirimkan ke model terkait
 */
class Form_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    /**
     * Mengolah masukan user pada form registrasi pasien
     * Membuat calon pasien
     * @return boolean return TRUE setelah dijalankan
     */
    public function process() {

    	// Ambil id pasien terakhir dan siapkan id untuk pasien baru
		$this->load->database();
		$query = $this->db->get('last_id');
		$row = $query->row();		
		$id = $row->last_id_calon_pasien;
		$id++; // buat id baru dengan melakukan increment pada id terakhir
		
		// Siapkan data untuk keperluan update id terakhir pada tabel
		$data = array(
	       	'last_id_calon_pasien' => $id
	        );

		$id = "".$id;

		// Siapkan dan set data untuk session user
        $data1 = array(
                'id' => $id,
                'is_mendaftar' => true,
                'is_baru' => false,
                'is_calon_pasien' => true
                );
        $this->session->set_userdata($data1);

        // Update id terakhir pada tabel dengan data yang telah disiapkan
		$this->db->where('id', 1);
		$this->db->update('last_id', $data); 

		// Periksa apakan user telah melakukan login (via LDAP).
		// Jika ya, set sesssion data
		if($this->authentication->is_loggedin()) {
			$username = $this->session->userdata('username');
		}
		// Jika tidak buat username baru (selanjutnya tidak digunakan)
		else $username = random_string('alnum', 6);

		// Ambil data yang dimasukkan atau yang dipilih user
		$tahun_daftar = (integer) date("Y");
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$golongan_darah = $this->input->post('golongan_darah');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$agama = $this->input->post('agama');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$peran = $this->input->post('peran');
		$alamat = $this->input->post('alamat');
		$npm = $this->input->post('npm');
		$jurusan = $this->input->post('jurusan');
		$fakultas = $this->input->post('fakultas');
		$no_identitas = $this->input->post('no_identitas');
		$pekerjaan = $this->input->post('pekerjaan');
		$lembaga = $this->input->post('lembaga');
		$semester_daftar = $this->input->post('semester');

		// Siapkan data
		$data = array(
				'id'=>$id,
				'username'=>$username,
				'nama'=>$nama,
				'jeniskelamin'=>$jenis_kelamin,
				'goldarah'=>$golongan_darah,
				'kewarganegaraan'=>$kewarganegaraan,
				'tempatlahir'=>$tempat_lahir,
				'tanggallahir'=>$tanggal_lahir,
				'agama'=>$agama,
				'alamat'=>$alamat,
				'statuspernikahan'=>$status_pernikahan,
				'peran'=>$peran,
				'no_telepon'=>$no_telp,
				'npm'=>$npm,
				'jurusan'=>$jurusan,
				'fakultas'=>$fakultas,
				'tahun_daftar'=>$tahun_daftar,
				'no_identitas'=>$no_identitas,
				'pekerjaan'=>$pekerjaan,
				'lembaga'=>$lembaga,
				'semester_daftar'=>$semester_daftar
		);
		// Masukkan ke dalam database
		$this->db->insert('registrasi_pasien_sementara',$data);
		return true;
	}

	/**
	 * Melakukan update data pasien
	 * @param  String $id id pasien
	 * @return boolean     TRUE jika telah dijalankan
	 */
	public function update($id){
		$this->load->database();

		// Ambil input yang dimasukkan atau dipilih user
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$golongan_darah = $this->input->post('golongan_darah');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$agama = $this->input->post('agama');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$alamat = $this->input->post('alamat');

		$semester_daftar = $this->input->post('semester');
		$tahun_daftar = $this->input->post('tahun_daftar');
		$npm = $this->input->post('npm');
		$jurusan = $this->input->post('jurusan');
		$fakultas = $this->input->post('fakultas');

		$no_identitas = $this->input->post('no_identitas');
		$pekerjaan = $this->input->post('pekerjaan');
		$password = $this->input->post('password');

		$lembaga = $this->input->post('lembaga');
		$peran = $this->input->post('peran');

		// Siapkan data
		$data = array(
				'nama'=>$nama,
				'jeniskelamin'=>$jenis_kelamin,
				'goldarah'=>$golongan_darah,
				'kewarganegaraan'=>$kewarganegaraan,
				'tempatlahir'=>$tempat_lahir,
				'tanggallahir'=>$tanggal_lahir,
				'agama'=>$agama,
				'alamat'=>$alamat,
				'statuspernikahan'=>$status_pernikahan,
				'no_telepon'=>$no_telp,
		);
		
		// Jika peran adalah mahasiswa, update data mahasiswa
		if ($peran =="Mahasiswa") {

			$data2 = array(
					'jurusan'=>$jurusan
			);

			$this->db->where('id', $id);
			$this->db->update('pasien', $data2); 
		}

		// Jika peran adalah karyawan, update data karyawan
		elseif ($peran =="Karyawan") {
			
			$data2 = array(
					'lembaga'=>$lembaga
			);
			
			$this->db->where('id', $id);
			$this->db->update('pasien', $data2); 
		}

		// Jika peran adalah masyarakat umum, update data masyarakat umum
		elseif ($peran =="Masyarakat Umum") {
			
			$data2 = array(
					'no_identitas'=>$no_identitas,
					'password'=>$password,
					'pekerjaan'=>$pekerjaan
			);
			
			$this->db->where('id', $id);
			$this->db->update('pasien', $data2); 
		}

		// Update database pasien dengan data baru
		$this->db->where('id', $id);
		$this->db->update('pasien', $data); 

		return true;
	}

	/**
	 * Mengolah masukan atau pilihan staf administrasi loket
	 * Membuat akun calon pasien
	 * @return String id pasien yang baru dibuat
	 */
    public function create() {

    	// Ambil id pasien terakhir dan siapkan id untuk pasien baru
		$this->load->database();
		$query = $this->db->get('last_id');
		$row = $query->row();		
		$id = $row->last_id_calon_pasien;
		$id++; // buat id baru dengan melakukan increment pada id terakhir
		
		// Siapkan data untuk keperluan update id terakhir pada tabel 
		$data = array(
       	'last_id_calon_pasien' => $id
        );

		$id = "".$id;

		// Siapkan dan set data untuk session user
        $data1 = array(
                'id' => $id,
                'is_calon_pasien' => true
                );

        // Buat session dengan data di atas
        $this->session->set_userdata($data1);

        // Update id terakhir pada tabel dengan data yang telah disiapkan
		$this->db->where('id', 1);
		$this->db->update('last_id', $data); 

		// Periksa apakah user telah login
		// Jika ya, set username
		if($this->authentication->is_loggedin()) {
			$username = $this->session->userdata('username');
		}
		// Jika tidak gunakan id sebagai username
		else $username = $id;

		// Ambil masukan atau pilihan user
		$tahun_daftar = (integer) date("Y");
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$golongan_darah = $this->input->post('golongan_darah');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$agama = $this->input->post('agama');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$peran = $this->input->post('peran');
		$alamat = $this->input->post('alamat');
		$npm = $this->input->post('npm');
		$jurusan = $this->input->post('jurusan');
		$fakultas = $this->input->post('fakultas');
		$no_identitas = $this->input->post('no_identitas');
		$pekerjaan = $this->input->post('pekerjaan');
		$lembaga = $this->input->post('lembaga');
		$semester_daftar = $this->input->post('semester');

		// Masukkan data di atas ke dalam array
		$data = array(
				'id'=>$id,
				'username'=>$username,
				'nama'=>$nama,
				'jeniskelamin'=>$jenis_kelamin,
				'goldarah'=>$golongan_darah,
				'kewarganegaraan'=>$kewarganegaraan,
				'tempatlahir'=>$tempat_lahir,
				'tanggallahir'=>$tanggal_lahir,
				'agama'=>$agama,
				'alamat'=>$alamat,
				'statuspernikahan'=>$status_pernikahan,
				'peran'=>$peran,
				'no_telepon'=>$no_telp,
				'npm'=>$npm,
				'jurusan'=>$jurusan,
				'fakultas'=>$fakultas,
				'tahun_daftar'=>$tahun_daftar,
				'no_identitas'=>$no_identitas,
				'pekerjaan'=>$pekerjaan,
				'lembaga'=>$lembaga,
				'semester_daftar'=>$semester_daftar
		);

		// Buat akun calon pasien baru sesuai data di atas
		$this->db->insert('registrasi_pasien_sementara',$data);
		return $id;
	}

	/**
	 * Melakukan verifikasi akun calon pasien
	 * Mengambil data calon pasien dan membuat akun pasien berdasarkan data yang diambil
	 * @param  String $id id calon pasien yang akan diverifikasi
	 * @return boolean     TRUE setelah dijalankan
	 */
	public function verifikasi_akun($id){

		// Ambil data calon pasien
   		$this->load->model('user_model');
   		$query = $this->user_model->get_calon_pasien_data($id);

   		// Buat password
		$generated_password = random_string('alnum', 6);

		// Simpan hasil query ke variabel baru
   		foreach ($query as $row) 
   		{
   			//data umum pasien
            $my_id = $row->id;
            $my_role = $row->peran;

            $my_username = $row->username;
            $my_nama = $row->nama;
            $my_sex = $row->jeniskelamin;
            $my_blood = $row->goldarah;
            $my_nation = $row->kewarganegaraan;
            $my_birthplace = $row->tempatlahir;
            $my_birthday = $row->tanggallahir;
            $my_religion = $row->agama;
            $my_address = $row->alamat;
            $my_marital = $row->statuspernikahan;
            $my_phone = $row->no_telepon;

            //data mahasiswa
            $my_npm = $row->npm;
            $my_jurusan = $row->jurusan;
            $my_fakultas = $row->fakultas;
            $my_tahundaftar = $row->tahun_daftar;
            $my_semesterdaftar = $row->semester_daftar;

            //data karyawan
            $my_lembaga = $row->lembaga;
            
            //data masyarakat umum
            $my_pekerjaan = $row->pekerjaan;
            $my_password = $generated_password;
            $my_identitas = $row->no_identitas;

        }

        // ambil waktu sekarang
		$datestring = "%Y %m %d - %h:%i %a";
		$time = time();
		$date = mdate($datestring, $time);

		// Jika peran calon pasien adalah karyawan
		if ($my_role == "Mahasiswa")
		{
			// ambil id terakhir yang dikeluarkan untuk peran mahasiswa
			$last_id = $this->get_last_id_pasien_mahasiswa();
			$last_id++;
			$my_id = $last_id; // buat id baru

			// Jika calon pasien telah menunggah foto saat melakukan registrasi,
			// rename foto dengan id baru
			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}

			// update tabel last_id
			$data = array(
           		'last_id_pasien_mahasiswa' => $last_id
            );
			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}
		
		// Jika peran calon pasien adalah karyawan
		if ($my_role == "Karyawan")
		{
			// ambil id terakhir yang dikeluarkan untuk peran karyawan
			$last_id = $this->get_last_id_pasien_karyawan();
			$last_id++;
			$my_id = $last_id; // buat id baru
	
			// Jika calon pasien telah menunggah foto saat melakukan registrasi,
			// rename foto dengan id baru	
			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}

			// update tabel last_id
			$data = array(
				
           		'last_id_pasien_karyawan' => $last_id
            );
			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}
		
		// Jika peran calon pasien adalah karyawan
		if ($my_role == "Masyarakat Umum")
		{
			// ambil id terakhir yang dikeluarkan untuk peran masyarakat umum
			$last_id = $this->get_last_id_pasien_masyarakat();
			$last_id++;
			$my_id = $last_id;

        	$my_username = $my_id; // set username sesuai dengan id yang didapat

        	// Jika calon pasien telah menunggah foto saat melakukan registrasi,
			// rename foto dengan id baru	
			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}
			
			// update tabel last_id
			$data = array(
				
           		'last_id_pasien_masyarakat' => $last_id
            );
			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}


		//array data seorang pasien
		$data_pasien = array(

			//array data general
			'id' => $my_id,
			'username' => $my_username,
			'nama'=>$my_nama,
			'jeniskelamin'=>$my_sex,
			'goldarah'=>$my_blood,
			'kewarganegaraan'=>$my_nation,
			'tempatlahir'=> $my_birthplace,
			'tanggallahir'=> $my_birthday,
			'agama'=>$my_religion,
			'alamat'=>$my_address,
			'statuspernikahan'=>$my_marital,
			'peran' => $my_role,
			'no_telepon'=>$my_phone,
			'waktu_daftar' => $date,

			//array data mahasiswa
            'npm' => $my_npm,
            'jurusan' => $my_jurusan,
            'fakultas' => $my_fakultas,
            'tahun_daftar' => $my_tahundaftar,
            'semester_daftar' => $my_semesterdaftar,

			//array data karyawan
            'lembaga' => $my_lembaga,

			//array data masyarakat umum
            'pekerjaan' => $my_pekerjaan,
            'password' => $my_password,
            'no_identitas' => $my_identitas,

          );

		//tambahkan ke tabel pasien
		$this->db->insert('pasien', $data_pasien); 

        //hapus dari tabel sementara
    	$this->delete_calon_pasien($id);

		return true;
	}

	/**
	 * Melakukan verifikasi akun calon pasien (oleh petugas administrasi loket)
	 * @param  String $id id calon pasien
	 * @return String     id pasien baru
	 */
	public function verifikasi_sekarang($id){

		//ambil dari tabel pendaftar
   		$this->load->model('user_model');
   		$query = $this->user_model->get_calon_pasien_data($id);
		$generated_password = random_string('alnum', 6);

   		foreach ($query as $row) 
   		{
   			//data umum pasien
            $my_id = $row->id;
            $my_role = $row->peran;

            $my_username = $row->username;
            $my_nama = $row->nama;
            $my_sex = $row->jeniskelamin;
            $my_blood = $row->goldarah;
            $my_nation = $row->kewarganegaraan;
            $my_birthplace = $row->tempatlahir;
            $my_birthday = $row->tanggallahir;
            $my_religion = $row->agama;
            $my_address = $row->alamat;
            $my_marital = $row->statuspernikahan;
            $my_phone = $row->no_telepon;

            //data mahasiswa
            $my_npm = $row->npm;
            $my_jurusan = $row->jurusan;
            $my_fakultas = $row->fakultas;
            $my_tahundaftar = $row->tahun_daftar;
            $my_semesterdaftar = $row->semester_daftar;

            //data karyawan
            $my_lembaga = $row->lembaga;
            
            //data masyarakat umum
            $my_pekerjaan = $row->pekerjaan;
            $my_password = $generated_password;
            $my_identitas = $row->no_identitas;

        }

        //ambil waktu sekarang
		$datestring = "%Y %m %d - %h:%i %a";
		$time = time();
		$date = mdate($datestring, $time);

		//id handler
		$this->load->database();
		
		if ($my_role == "Mahasiswa")
		{
			$last_id = $this->get_last_id_pasien_mahasiswa();
			$last_id++;
			$my_id = $last_id;


			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}

			$data = array(

           		'last_id_pasien_mahasiswa' => $last_id
            );

			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}
		
		if ($my_role == "Karyawan")
		{
			$last_id = $this->get_last_id_pasien_karyawan();
			$last_id++;
			$my_id = $last_id;
			

			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}

			$data = array(
				
           		'last_id_pasien_karyawan' => $last_id
            );

			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}
		
		if ($my_role == "Masyarakat Umum")
		{
			$last_id = $this->get_last_id_pasien_masyarakat();
			$last_id++;
			$my_id = $last_id;

			$my_username = $my_id;
			

			if (file_exists('./foto/pasien/'.$id.'.jpg')) {
				rename('./foto/pasien/'.$id.'.jpg', './foto/pasien/'.$my_id.'.jpg');
			}
			
			$data = array(
				
           		'last_id_pasien_masyarakat' => $last_id
            );

			$this->db->where('id', 1);
			$this->db->update('last_id', $data); 
		}


		//array data umum pasien
		$data_pasien = array(

			'id' => $my_id,
			'username' => $my_username,
			'nama'=>$my_nama,
			'jeniskelamin'=>$my_sex,
			'goldarah'=>$my_blood,
			'kewarganegaraan'=>$my_nation,
			'tempatlahir'=> $my_birthplace,
			'tanggallahir'=> $my_birthday,
			'agama'=>$my_religion,
			'alamat'=>$my_address,
			'statuspernikahan'=>$my_marital,
			'peran' => $my_role,
			'no_telepon'=>$my_phone,
			'waktu_daftar' => $date,

		//array data mahasiswa
            'npm' => $my_npm,
            'jurusan' => $my_jurusan,
            'fakultas' => $my_fakultas,
            'tahun_daftar' => $my_tahundaftar,
            'semester_daftar' => $my_semesterdaftar,
		
		//array data karyawan
            'lembaga' => $my_lembaga,

		//array data masyarakat umum
            'pekerjaan' => $my_pekerjaan,
            'password' => $my_password,
            'no_identitas' => $my_identitas,
		);


		//tambahkan ke tabel pasien
		$this->db->insert('pasien', $data_pasien); 

		return $my_id;
	}

	public function delete_calon_pasien($id) {

		$this->db->where('id', $id);
		$this->db->delete('registrasi_pasien_sementara'); 
	}

	public function get_last_calon_pasien_id() {

	$this->load->database();
	$query = $this->db->get('last_id');
	$row = $query->row(); 
	
	$id = $row->last_id_calon_pasien;

	return $id;
	}

	public function get_last_id_pasien_mahasiswa() {

	$this->load->database();
	$query = $this->db->get('last_id');
	$row = $query->row(); 
	
	$id = $row->last_id_pasien_mahasiswa;

	return $id;
	}

	public function get_last_id_pasien_karyawan() {

	$this->load->database();
	$query = $this->db->get('last_id');
	$row = $query->row(); 
	
	$id = $row->last_id_pasien_karyawan;

	return $id;
	}

	public function get_last_id_pasien_masyarakat() {

	$this->load->database();
	$query = $this->db->get('last_id');
	$row = $query->row(); 
	
	$id = $row->last_id_pasien_masyarakat;

	return $id;
	}

	public function get_userdata($id) {

    $this->load->database();
    $this->db->where('id', $id);
    $query = $this->db->get('pasien');
    return $query->result();
	} 



	public function updatecalonpasien($id){
		$this->load->database();

			$nama = $this->input->post('nama');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$golongan_darah = $this->input->post('golongan_darah');
			$kewarganegaraan = $this->input->post('kewarganegaraan');
			$tempat_lahir = $this->input->post('tempat_lahir');
			$tanggal_lahir = $this->input->post('demo4');
			$agama = $this->input->post('agama');
			$no_telp = $this->input->post('no_telp');
			$alamat = $this->input->post('alamat');
			$status_pernikahan = $this->input->post('status_pernikahan');
			$alamat = $this->input->post('alamat');

			$semester_daftar = $this->input->post('semester');
			$tahun_daftar = $this->input->post('tahun_daftar');
			$npm = $this->input->post('npm');
			$jurusan = $this->input->post('jurusan');
			$fakultas = $this->input->post('fakultas');

			$no_identitas = $this->input->post('no_identitas');
			$pekerjaan = $this->input->post('pekerjaan');

			$lembaga = $this->input->post('lembaga');
			$peran = $this->input->post('peran');

			$data = array(
					'nama'=>$nama,
					'jeniskelamin'=>$jenis_kelamin,
					'goldarah'=>$golongan_darah,
					'kewarganegaraan'=>$kewarganegaraan,
					'tempatlahir'=>$tempat_lahir,
					'tanggallahir'=>$tanggal_lahir,
					'agama'=>$agama,
					'alamat'=>$alamat,
					'statuspernikahan'=>$status_pernikahan,
					'no_telepon'=>$no_telp,
			);

			if ($peran =="Mahasiswa") {

				$data2 = array(
						'jurusan'=>$jurusan,
						'semester_daftar' =>$semester_daftar,
				);

				$this->db->where('id', $id);
				$this->db->update('registrasi_pasien_sementara', $data2); 
			}

			elseif ($peran =="Karyawan") {
				
				$data2 = array(
						'lembaga'=>$lembaga
				);
				
				$this->db->where('id', $id);
				$this->db->update('registrasi_pasien_sementara', $data2); 
			}

			elseif ($peran =="Masyarakat Umum") {
				
				$data2 = array(
						'no_identitas'=>$no_identitas,
						'pekerjaan'=>$pekerjaan
				);
				
				$this->db->where('id', $id);
				$this->db->update('registrasi_pasien_sementara', $data2); 
			}

		$this->db->where('id', $id);
		$this->db->update('registrasi_pasien_sementara', $data); 


		return true;
	}

	public function updatepassword($id)
	{
		$table = SCHEMA.'pengguna';
		$username = $this->user_model->get_username_by_id($id);
		$newpassword = $this->input->post('password');
		$data = array(
					'password'=>md5($newpassword),
				);

		
		$this->db->where('username', $username);
		$this->db->update($table, $data); 
	}
}
?>
				   
