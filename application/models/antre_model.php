<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Class User Model manages database manipulation
 * have been registered, retrieve, edit, delete user's profile
 * checked for validate user, available username,
 */

class antre_model extends MY_Model {
	function __construct() {
        parent::__construct();
    }

	function get_all_antrian() {
        $this->load->database();

        $this->db->from('antrian');
        $this->db->join('pasien', 'pasien.id=antrian.id_pasien');
        $this->db->order_by("status", "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function update_status($stat, $id) {
        $this->load->database();
        $data = array('status' => $stat);

    	$this->db->where('id_antri', $id);
    	$this->db->update('antrian', $data);
        return true;
    }
    function add_antrian($data) {
        $this->load->database();
        $this->db->insert('antrian',$data);
    }
    function delete_antrian_by_idpasien($id_pasien) {
		$this->load->database();
    	$this->db->where('id_pasien', $id_pasien);
        $this->db->delete('antrian');
    }
}