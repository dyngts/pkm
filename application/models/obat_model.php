<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Obat_model extends MY_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->skema = SCHEMA;
	}

	function get_delivered_stok_this_month($month, $year) { 
        $query = "select nama_obat, sum(jumlah) as jumlah_penerimaan_stok 
                  from $this->t_log_obat_masuk, $this->t_penerimaan_obat 
                  where log_obat_masuk.id = id_log_obat_masuk and
                  extract(year from log_obat_masuk.tgl) = '$year' and
                  extract(month from log_obat_masuk.tgl) = '$month' 
                  group by nama_obat 
                  order by nama_obat"; 
        $result = $this->db->query($query); 
        return $result->result(); 
    } 

	public function _ambil_log_salemba() {
		$query = $this->db->query("SELECT *
								   FROM $this->t_transaksi_obat_salemba
								   ORDER BY tgl DESC, jam DESC"
								   );

		return $query->first_row();
	}

	public function _ambil_detil_obat($id, $mulai = 0) {
		$query = $this->db->query("SELECT DISTINCT o_so.id as id_obat, o_so.nama as nama_obat, o_so.kategori, o_so.jenis, o_so.tgl_kadaluarsa, o_so.jml_satuan, o_so.status_kadaluarsa::int, s.nama as nama_supplier
								   FROM ($this->t_obat as o JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so 
								   LEFT OUTER JOIN $this->t_supplier as s ON o_so.id_supplier = s.id
								   WHERE o_so.id = '$id'
								   ORDER BY o_so.kategori, o_so.jenis, o_so.nama
								   LIMIT 10 
								   OFFSET $mulai");

		$query2 = $this->db->query("SELECT DISTINCT o_so.id as id_obat, o_so.nama as nama_obat, o_so.kategori, o_so.jenis, o_so.tgl_kadaluarsa, o_so.jml_satuan, o_so.status_kadaluarsa::int, s.nama as nama_supplier
								   FROM ($this->t_obat as o JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so 
								   LEFT OUTER JOIN $this->t_supplier as s ON o_so.id = s.id
								   WHERE o_so.id_supplier = s.id
								   AND o_so.id = '$id'
								   ORDER BY o_so.kategori, o_so.jenis, o_so.nama
								   ");

		$hasil = array();

		$hasil['obats'] = $query;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_data_obat_umum($id) {
		$query = $this->db->query("SELECT DISTINCT o_so.id as id_obat, o_so.nama as nama_obat, o_so.kategori, o_so.jenis, o_so.kemasan, o_so.sediaan, o_so.harga_per_kemasan, o_so.harga_satuan, o_so.total_satuan, o_so.satuan_per_kemasan, o_so.komposisi,  s.nama as nama_supplier
								   FROM $this->t_obat as o_so
								   LEFT OUTER JOIN $this->t_supplier as s ON o_so.id_supplier = s.id
								   WHERE o_so.id = '$id'
								   ");

		return $query->first_row();
	}

	public function _ambil_log_masuk_terbaru() {
		$query = $this->db->query("SELECT *
								   FROM $this->t_log_obat_masuk
								   ORDER BY tgl DESC, jam DESC
								   LIMIT 1");

		return $query->first_row();
	}

	public function _ambil_total_terbaru($nama_obat = '') {
		$query = $this->db->query("SELECT SUM(jml_satuan) as total
								   FROM $this->t_stok_obat
								   WHERE nama_obat = '$nama_obat'
								   GROUP BY nama_obat ");

		return $query->first_row();
	}

	public function _tampil_daftar_pilihan($jenis) {
		$query = $this->db->query(" SELECT id, nama
			FROM $this->t_obat
			WHERE jenis = '$jenis'
			ORDER BY nama ");

		$hasil = $query->result();

		//echo "<html><head><title>Test Parah</title><head><body><select>";

		foreach ($hasil as $nama) {
			echo "<option value='<?php echo $nama->nama; ?>'>".$nama->nama."</option>";
		}

		//echo "</select></body></html>";

	}


	public function _daftar_pilihan_jenis($kategori) {

		if($kategori == 'Obat-Alkes-dan-BMHP-Gigi') {
			$kat_asli = str_replace('-', ', ', $kategori);
		}
		else {
			$kat_asli = str_replace('-',' ', $kategori);
		}

		$query = $this->db->query(" SELECT DISTINCT jenis
			FROM $this->t_obat
			WHERE kategori = '$kat_asli'
			ORDER BY jenis ");

		$hasil = $query->result();


		if($kat_asli != '') {
			//echo "<html><head><title>Test Parah</title><head><body><select>";
			echo "<option value=''> Pilih Jenis Obat </option>";
			
			foreach ($hasil as $jenis_obat) {
				$nama_jenis = '';
				$temp = explode(' ', $jenis_obat->jenis);

				for($i = 0; $i < count($temp); ++$i) {
					if($i > 0) {
						$nama_jenis = $nama_jenis.'-'.$temp[$i];
					}
					else {
						$nama_jenis = $temp[$i];
					}
					
				}

				echo "<option value='$nama_jenis'>".$jenis_obat->jenis."</option>";
			}	
		}
		else {
			echo "<option value=''> Pilih Kategori Dulu </option>";
		}

		//echo "</select></body></html>";
	}

	public function _daftar_pilihan_nama($jenis) {
		
		$jenis_spasi = str_replace('-', ' ', $jenis);

		$query = $this->db->query("SELECT id, nama
			FROM $this->t_obat
			WHERE jenis = '$jenis_spasi'
			ORDER BY nama ");

		$hasil = $query->result();

		//echo "<html><head><title>Test Parah</title><head><body><select>";
		if($jenis != '' ) {
			echo "<option value=''> Pilih Nama Obat </option>";
			foreach ($hasil as $nama) {
				echo "<option value='$nama->nama'>".$nama->nama."</option>";
			}
		}
		else {
			echo "<option value=''> Pilih Jenis Obat Dulu </option>";
		}

		//echo "</select></body></html>";

	}

	public function _data_jenis() {
		$query = $this->db->query("SELECT DISTINCT jenis
								   FROM $this->t_obat
								   WHERE jenis != '' order by jenis asc
								   ");



		$hasil = $query->result();
		echo "<option value=''> Pilih Jenis Obat </option>";
		foreach ($hasil as $obat) {
			$temp = explode(' ', $obat->jenis);
			$jenis_strip = '';
			for($i = 0; $i < count($temp); ++$i) {
				if($i > 0) {
					$jenis_strip = $jenis_strip.'-'.$temp[$i];
				}
				else {
					$jenis_strip = $temp[$i];
				}
			}
			echo "<option value='$jenis_strip'>".$obat->jenis."</option>";
		}
	}


	public function _ambil_baris_tertentu_kadaluarsa_obat($id = '') {

		$query = $this->db->query("SELECT ok.id as id_obat, ok.kategori, ok.komposisi, ok.jenis, ok.status_pemakaian::int, ok.nama as nama_obat, ok.sediaan, ok.kemasan, ok.satuan_per_kemasan, ok.harga_satuan, ok.harga_per_kemasan, ok.total_satuan, ok.id_supplier, s.nama as nama_supplier, s.id as id_supplier
		FROM $this->t_obat as ok 
		LEFT OUTER JOIN $this->t_supplier s 
		ON ok.id_supplier = s.id
		WHERE ok.id = '$id'			
				 ");
		return $query->row();

		// $query = $this->db->query("SELECT ok.id, ok.tgl_kadaluarsa, ok.kategori, ok.komposisi, ok.jenis, ok.status_pemakaian, ok.nama, ok.sediaan, ok.harga_satuan, ok.total_obat_satuan, ok.jml_satuan, s.nama, s.id
		// FROM (obat natural join kadaluarsa_obat) as ok 
		// LEFT OUTER JOIN supplier s 
		// ON ok.id = s.id			
		// WHERE
		// 		  ok.id = '$id'
		// 		  AND ok.tgl_kadaluarsa = '$tgl_kadaluarsa'
		// 		 ");
		// return $query->row();
	}

	public function _join_id_kadaluarsa_obat($mulai = 0, $search) {
		$query = ''; $query2 = '';

		if(count($search) > 0) {

			$nilai = $search['nilai_obat_cari'];

			$query = $this->db->query("SELECT o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.kemasan, o_so.nama as nama_obat, SUM(o_so.jml_satuan) as total_stok
				FROM ($this->t_obat as o LEFT OUTER JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so
				WHERE (o_so.kategori ~* '$nilai' OR o_so.jenis ~* '$nilai' OR o_so.nama ~* '$nilai' OR o_so.kemasan ~* '$nilai' OR o_so.sediaan ~* '$nilai' OR o_so.komposisi ~* '$nilai')
				GROUP BY o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.nama
				ORDER BY o_so.kategori ASC, o_so.jenis ASC, o_so.nama ASC
				LIMIT 10 
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.kemasan, o_so.nama as nama_obat, SUM(o_so.jml_satuan) as total_stok
				FROM ($this->t_obat as o LEFT OUTER JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so
				WHERE (o_so.kategori ~* '$nilai' OR o_so.jenis ~* '$nilai' OR o_so.nama ~* '$nilai' OR o_so.kemasan ~* '$nilai' OR o_so.sediaan ~* '$nilai' OR o_so.komposisi ~* '$nilai')
				GROUP BY o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.nama
				ORDER BY o_so.kategori ASC, o_so.jenis ASC, o_so.nama ASC
				");
		}
		else {
			$query = $this->db->query("SELECT o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.kemasan, o_so.nama as nama_obat, SUM(o_so.jml_satuan) as total_stok
				FROM ($this->t_obat as o LEFT OUTER JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so
				GROUP BY o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.nama
				ORDER BY o_so.kategori ASC, o_so.jenis ASC, o_so.nama ASC
				LIMIT 10 
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.kemasan, o_so.nama as nama_obat, SUM(o_so.jml_satuan) as total_stok
				FROM ($this->t_obat as o LEFT OUTER JOIN $this->t_stok_obat as so ON o.nama = so.nama_obat) as o_so
				GROUP BY o_so.id, o_so.jenis, o_so.kategori, o_so.sediaan, o_so.nama
				ORDER BY o_so.kategori ASC, o_so.jenis ASC, o_so.nama ASC
				");
		}

		

		$hasil = array();

		$hasil['obats'] = $query;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _join_id_kadaluarsa_obat_notif($mulai = 0) {
		$query = $this->db->query("SELECT ok.id, ok.kategori, ok.komposisi, ok.jenis, ok.status_pemakaian, ok.nama as nama_obat, ok.sediaan, ok.harga_satuan, ok.total_satuan, ok.harga_per_kemasan as harga_per_kemasan, ok.kemasan as kemasan, s.nama as nama_supplier, s.id as id_supplier
		FROM ($this->t_obat natural join $this->t_log_obat_masuk) as ok 
		LEFT OUTER JOIN $this->t_supplier s 
		ON ok.id_supplier = s.id
			ORDER BY tgl DESC
			LIMIT 10 
			OFFSET $mulai
			 ");

		$query2 = $this->db->query("SELECT ok.id, ok.kategori, ok.komposisi, ok.jenis, ok.status_pemakaian, ok.nama as nama_obat, ok.sediaan, ok.harga_satuan, ok.total_satuan, ok.harga_per_kemasan as harga_per_kemasan, ok.kemasan as kemasan, s.nama as nama_supplier, s.id as id_supplier
		FROM ($this->t_obat natural join $this->t_log_obat_masuk) as ok 
		LEFT OUTER JOIN $this->t_supplier s 
		ON ok.id_supplier = s.id
			ORDER BY tgl DESC
			 ");
		$hasil = array();

		$hasil['obats'] = $query;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_supplier_obat($mulai = 0) {
		$query = $this->db->query("SELECT *
			FROM $this->t_obat, $this->t_supplier
			WHERE supplier.id = obat.id
			LIMIT 10
			OFFSET $mulai");
		return $query;
	}

	public function _jumlah_supplier_obat() {
		$query = $this->db->query("SELECT *
			FROM $this->t_obat, $this->t_supplier
			WHERE supplier.id = obat.id");
		return $query->num_rows();
	}


	public function _ambil_daftar_menjelang_kadaluarsa($mulai = 0) {

		$sixmonths = date("Y-m-d", strtotime("+180 days")); 
		$now = date("Y-m-d"); 

		$query1 = $this->db->query("SELECT * 
									FROM $this->t_stok_obat 
									WHERE tgl_kadaluarsa > '$now' 
									AND tgl_kadaluarsa <= '$sixmonths' 
									AND status_kadaluarsa = '0'
									ORDER BY tgl_kadaluarsa ASC
									LIMIT 10
									OFFSET $mulai ");

		$query2 = $this->db->query("SELECT * 
									FROM $this->t_stok_obat 
									WHERE tgl_kadaluarsa > '$now' 
									AND tgl_kadaluarsa <= '$sixmonths' 
									AND status_kadaluarsa = '0'
									ORDER BY tgl_kadaluarsa ASC ");
 
		// $query1 = $this->db->query("SELECT * 
		// 	FROM $this->t_obat o, $this->t_stok_obat so 
		// 	WHERE o.nama = so.nama_obat 
		// 		AND tgl_kadaluarsa <= (SELECT current_date + integer '180') 
		// 	EXCEPT
		// 	(SELECT *
		// 	FROM $this->t_obat o, $this->t_stok_obat so 
		// 	WHERE o.nama = so.nama_obat 
		// 	AND tgl_kadaluarsa <= (SELECT current_date))  
		// 	ORDER BY tgl_kadaluarsa ASC
		// 	LIMIT 10
		// 	OFFSET $mulai ");

		// $query2 = $this->db->query("SELECT * 
		// 	FROM $this->t_obat o, $this->t_stok_obat so 
		// 	WHERE o.nama = so.nama_obat 
		// 		AND tgl_kadaluarsa <= (SELECT current_date + integer '180') 
		// 	EXCEPT
		// 	(SELECT *
		// 	FROM $this->t_obat o, $this->t_stok_obat so 
		// 	WHERE o.nama = so.nama_obat 
		// 	AND tgl_kadaluarsa <= (SELECT current_date))  
		// 	ORDER BY tgl_kadaluarsa ASC 
		// 	");

		$hasil = array();
		$hasil['jumlah_baris'] = $query2->num_rows();
		$hasil['kumpulan'] = $query1;

		return $hasil;
	}

	public function _ambil_daftar_sudah_kadaluarsa($mulai = 0) {
		$query1 = $this->db->query("SELECT *
			FROM $this->t_obat o, $this->t_stok_obat so
			WHERE o.nama = so.nama_obat 
				  AND (so.tgl_kadaluarsa <= (select current_date)) 
				  AND so.status_kadaluarsa = '1'
			ORDER BY so.nama_obat, so.tgl_kadaluarsa asc
			LIMIT 10
			OFFSET $mulai
		");

		$query2 = $this->db->query("SELECT *
			FROM $this->t_obat o, $this->t_stok_obat so
			WHERE o.nama = so.nama_obat 
				  AND (so.tgl_kadaluarsa <= (select current_date)) 
				  AND so.status_kadaluarsa = '1'
			ORDER BY so.nama_obat, so.tgl_kadaluarsa asc
		");

		$hasil = array();
		$hasil['kumpulan'] = $query1->result();
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_semua_obat_kadaluarsa() {
		$query = $this->db->query("SELECT so.nama_obat, so.tgl_kadaluarsa, so.jml_satuan, o.total_satuan
			FROM $this->t_obat o, $this->t_stok_obat so 
			WHERE o.nama = so.nama_obat 
				  AND (so.tgl_kadaluarsa <= (select current_date))
				  AND so.status_kadaluarsa = '0'
			ORDER BY so.nama_obat, so.tgl_kadaluarsa asc
		");

		return $query->result();
	}

	public function _ambil_daftar_log_masuk($mulai = 0, $search) {
		

		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_masuk'];
			$tgl_akhir = $search['tanggal_akhir_masuk'];
			$query = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_masuk l
				WHERE (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				ORDER BY tgl asc, jam asc
				LIMIT 10
				OFFSET $mulai");

			$query1 = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_masuk l
				WHERE (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				ORDER BY tgl asc, jam asc
				");
		} else {
			$query = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_masuk l
				ORDER BY tgl desc, jam desc
				LIMIT 10
				OFFSET $mulai");

			$query1 = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_masuk l
				ORDER BY tgl desc, jam desc
				");
		}

		$data = array();
		$data['daftar_log'] = $query->result();
		$data['jumlah_log'] = $query1->num_rows();
		return $data;
	}

	public function searchterm_handler($field,$searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata($field, $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata($field))
		{
			$searchterm = $this->session->userdata($field);
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

	public function _ambil_detil_log($id, $jenis = '') {

		$query = '';

		if($jenis == 'masuk') {
			$query = $this->db->query("SELECT *
				FROM $this->t_log_obat_masuk l, $this->t_penerimaan_obat p
				WHERE l.id = p.id_log_obat_masuk
				AND l.id = '$id'
				");
		}
		else if($jenis == 'keluar') {
			$query = $this->db->query("SELECT *
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				AND l.id = '$id'
				");
		}
		

		$datas = array();
		$datas['data_log'] = $query->first_row();
		$datas['data_obat'] = $query;

		return $datas;

	}

	public function _detil_log_surat_jalan($id) {

		
			$query = $this->db->query("SELECT *
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p, $this->t_obat o
				WHERE l.id = p.id_log_obat_keluar
				AND p.nama_obat = o.nama
				AND l.id = '$id'
				AND l.jenis != 'Resep'
				");

		$datas = array();
		$datas['data_log'] = $query->first_row();
		$datas['data_obat'] = $query;

		return $datas;

	}

	public function _jumlah_daftar_log_masuk() {

		$query = $this->db->query('SELECT l.tgl_pembuatan, nama, jenis, sediaan, jumlah, jam
			FROM log_obat_masuk l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND k.id = o.id');

		return $query->num_rows();
	}

	public function _ambil_daftar_log_keluar($mulai = 0, $search) {

		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_keluar'];
			$tgl_akhir = $search['tanggal_akhir_keluar'];
			$query = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_keluar l
				WHERE (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				ORDER BY tgl asc, jam asc
				LIMIT 10
				OFFSET $mulai");

			$query1 = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_keluar l
				WHERE (l.tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				ORDER BY tgl asc, jam asc
				");
		} else {
			$query = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_keluar l
				ORDER BY tgl desc, jam desc
				LIMIT 10
				OFFSET $mulai");

			$query1 = $this->db->query("SELECT *, to_char(tgl,'DD-MM-YYYY') as tanggal
				FROM $this->t_log_obat_keluar l
				ORDER BY tgl desc, jam desc
				");
		}

		$data = array();
		$data['daftar_log'] = $query->result();
		$data['jumlah_log'] = $query1->num_rows();
		return $data;
	}

	public function _jumlah_daftar_log_keluar() {
		$query = $this->db->query("SELECT *
			FROM log_obat_keluar l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND l.id = k.id
				  AND k.id = o.id
			");
		return $query->num_rows();
	}

	public function _ambil_log_masuk_query($tgl_mulai = '', $tgl_akhir = '', $mulai = 0) {
		$query = $this->db->query("SELECT *
			FROM log_obat_masuk l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND l.id = k.id
				  AND k.id = o.id
				  AND (l.tgl_pembuatan BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			ORDER BY tgl_pembuatan desc, jam desc
			LIMIT 10
			OFFSET $mulai");

		return $query;
	}

	public function _jumlah_log_masuk_query($tgl_mulai = '', $tgl_akhir = '') {
		$query = $this->db->query("SELECT *
			FROM log_obat_masuk l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND l.id = k.id
				  AND k.id = o.id
				  AND (l.tgl_pembuatan BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			");

		return $query->num_rows();
	}

	public function _ambil_log_keluar_query($tgl_mulai = '', $tgl_akhir = '', $mulai = 0) {
		$query = $this->db->query("SELECT *
			FROM log_obat_keluar l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND l.id = k.id
				  AND k.id = o.id
				  AND (l.tgl_transaksi BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			ORDER BY tgl_transaksi desc, jam desc
			LIMIT 10
			OFFSET $mulai");

		return $query;
	}

	public function _jumlah_log_keluar_query($tgl_mulai = '', $tgl_akhir = '') {
		$query = $this->db->query("SELECT *
			FROM log_obat_keluar l, kadaluarsa_obat k, obat o
			WHERE l.id = k.id
				  AND l.tgl_kadaluarsa = k.tgl_kadaluarsa
				  AND l.id = k.id
				  AND k.id = o.id
				  AND (l.tgl_transaksi BETWEEN '$tgl_mulai' AND '$tgl_akhir')
			");

		return $query->num_rows();
	}
	
	public function _ambil_daftar_supplier() {
		$query = $this->db->query("SELECT supplier.id, supplier.nama 
								   FROM $this->t_supplier 
								   order by nama asc");
		$data_dropdown = array();
		$data_dropdown[''] = 'Pilih Supplier';

		foreach($query->result() as $data) {
			$data_dropdown[$data->id] = $data->nama;
		}

		return $data_dropdown;
	}

	public function _ambil_daftar_jenis_obat($kategori = '') {
		$query = $this->db->query("SELECT distinct jenis
								   FROM $this->t_obat
								   WHERE kategori = '$kategori' order by jenis asc");
		$data_jenis_obat = array();

		$data_jenis_obat[''] = 'Pilih Jenis Obat';
		//$data_jenis_obat['Tidak Ada'] = 'Tidak Ada';

		foreach($query->result() as $data) {
			$temp = explode(' ', $data->jenis);
			$jenis_strip = '';
			for($i = 0;$i < count($temp); ++$i) {
				if($i > 0) {
					$jenis_strip = $jenis_strip.'-'.$temp[$i];
				}
				else {
					$jenis_strip = $temp[$i];
				}
			}
			$data_jenis_obat[$jenis_strip] = $data->jenis;
		}

		return $data_jenis_obat;
	}

	public function _ambil_daftar_jenis_obat_obj($kategori) {
		$query = $this->db->query("SELECT distinct jenis
								   FROM $this->t_obat
								   WHERE kategori = '$kategori' order by jenis asc");

		return $query->result();
	}

	public function _ambil_daftar_nama_obat_obj($nama_obat = '') {
		$query = $this->db->query("SELECT distinct nama
								   FROM $this->t_obat
								   WHERE nama = '$nama_obat'
								   ORDER BY nama ASC");
		$daftar_nama = array();

		$daftar_nama[''] = 'Pilih Nama Obat';

		foreach ($query->result() as $nama) {
			$daftar_nama[$nama->nama] = $nama->nama;
		}

		return $daftar_nama;
	}

	public function _ambil_daftar_jenis_obat_modifikasi() {
		$query = $this->db->query("SELECT id, jenis
								   FROM $this->t_obat
								   WHERE jenis != '' order by jenis asc");
		$data_jenis_obat = array();
		$data_jenis_obat[''] = 'Pilih Jenis Obat';
		foreach($query->result() as $data) {
			$temp = explode(' ', $data->jenis);
			$jenis_strip = '';
			for($i = 0;$i < count($temp); ++$i) {
				if($i > 0) {
					$jenis_strip = $jenis_strip.'-'.$temp[$i];
				}
				else {
					$jenis_strip = $temp[$i];
				}
			}

			$data_jenis_obat[$jenis_strip] = $data->jenis;
		}

		return $data_jenis_obat;
	}

	public function _ambil_daftar_nama() {
		$query = $this->db->query("SELECT id, nama
								   FROM $this->t_obat 
								   order by nama asc");

		$data_nama = array();
		foreach ($query->result() as $data) {
			$data_nama[$data->id] = $data->nama;
		}

		return $data_nama;
	}

	public function _ambil_daftar_sediaan() {
		$query = $this->db->query("SELECT id, sediaan
								   FROM $this->t_obat 
								   order by sediaan asc");
		$data_sediaan_obat = array();
		$data_sediaan_obat[''] = 'Pilih Sediaan Obat';

		foreach($query->result() as $data) {
			$data_sediaan_obat[$data->sediaan] = $data->sediaan;
		}
		return $data_sediaan_obat;
	}


	public function _ambil_daftar_kategori_obat($pilihan) {
		$query = $this->db->query("SELECT id, kategori
								   FROM $this->t_obat");

		$data_kategori_obat = array();

		foreach ($query->result() as $data) {
			$data_kategori_obat[$data->kategori] = $data->kategori;
		}

		return $data_kategori_obat;
	}

	public function _ambil_daftar_log_salemba($mulai = 0, $search) {

		if(count($search) > 0) {
			$tgl_mulai = $search['tanggal_awal_salemba'];
			$tgl_akhir = $search['tanggal_akhir_salemba'];

			$query1 = $this->db->query("SELECT *
				FROM $this->t_transaksi_obat_salemba
				WHERE (tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir') 
				ORDER BY tgl ASC, jam ASC
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT *
				FROM $this->t_transaksi_obat_salemba
				WHERE (tgl BETWEEN '$tgl_mulai' AND '$tgl_akhir')
				ORDER BY tgl ASC, jam ASC
				");
		}
		else {
			$query1 = $this->db->query("SELECT *
				FROM $this->t_transaksi_obat_salemba
				ORDER BY tgl DESC, jam DESC
				LIMIT 10
				OFFSET $mulai
				");

			$query2 = $this->db->query("SELECT *
				FROM $this->t_transaksi_obat_salemba
				ORDER BY tgl DESC, jam DESC
				");
		}

		

		$hasil = array();
		$hasil['daftar_log'] = $query1->result();
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_detil_salemba($id) {

		$query = $this->db->query("SELECT *
								   FROM $this->t_transaksi_obat_salemba s, $this->t_detail_transaksi_salemba d
								   WHERE s.id = d.id_transaksi_salemba
								   AND s.id = '$id'
								   ");

		$data = array();
		$data['data_log'] = $query->first_row();
		$data['data_obat'] = $query;

		return $data;
	}

}

/* End of file obat_model.php */
/* Location: ./application/models/obat_model.php */