<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik kunjungan
 */
class Statistik_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /**
     * Mengambil data statistik kepuasan pada periode tertentu
     * @param  String $tahun     tahun statistik yang ingin ditampilkan
     * @param  String $namalevel nama level yang nantinya ditampilkan (bebas)
     * @param  integer $levelpuas level kepuasan
     * @return query            
     */
    public function get_statistik_kepuasan($tahun, $namalevel, $levelpuas){
        $query = $this->db->query(
        "select * 
        from
            (select extract (month from tanggal) as bulan, count(*) as $namalevel
            from (select tanggal, nilai from pasien, RESPON_KEPUASAN, REGISTRASI_PENGOBATAN
            where RESPON_KEPUASAN.ID_registrasi = REGISTRASI_PENGOBATAN.ID_reg and RESPON_KEPUASAN.ID_pasien = PASIEN.ID) as wawa where extract (year from tanggal) ='$tahun' and nilai = '$levelpuas'
            group by Bulan) as one order by bulan asc;");
        return $query->result();
    }

    /**
     * Mengambil data statistik kunjungan pada tahun tertentu berdasarkan bulan
     * @param  String $tahun tahun statistik yang ingin ditampilkan
     * @return query        
     */
    public function get_statistik_periode($tahun) {

        $query = $this->db->query("select jp.bulan, COALESCE(jumlah.jumlah_pengunjung,0)
        from jumlah_pengunjung jp LEFT OUTER JOIN (select extract (month from tanggal) as Bulan, count(*) as jumlah_pengunjung
        from registrasi_pengobatan where extract (year from tanggal) = '".$tahun."' and registrasi_pengobatan.status=6
        group by Bulan) as jumlah
        on jp.bulan = jumlah.Bulan");

        return $query->result();
    }

    /**
     * Mengambil data statistik kunjungan pada tahun dan bulan tertentu berdasarkan fakultas
     * @param  String $tahun tahun statistik yang ingin ditampilkan
     * @param  String $bulan bulan statistik yang ingin ditampilkan
     * @return query        
     */
    public function get_statistik_fakultas($tahun, $bulan) {
        $query = $this->db->query("select jpf.fakultas, COALESCE (jumlah.jumlah_pengunjung,0)
            from jumlah_pengunjung_fakultas jpf LEFT OUTER JOIN (SELECT fakultas, count (*) as jumlah_pengunjung
            from registrasi_pengobatan, pasien
            where extract (year from tanggal) ='".$tahun."' and extract (month from tanggal) = '".$bulan."'
            and registrasi_pengobatan.id_pasien = pasien.id and registrasi_pengobatan.status=6
            group by fakultas) as jumlah
            on jpf.fakultas= jumlah.fakultas");
        
        return $query->result();
    }

    /**
     * Mengambil tahun pada tanggal dimana kunjungan pertama dibuat
     * @return String tahun
     */
    public function get_first_year() 
    {
        // Urutkan berdasarkan id_reg secara ascending
        $this->db->order_by("id", "asc");
        $query = $this->db->get(SCHEMA.'registrasi_pengobatan');

        // Periksa apakah ada registrasi pengobatan dalam database
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;

            // Jika ada, ambil tahun
            date_default_timezone_set('Asia/Jakarta');
            $datestring = "%Y";
            $time = strtotime($source);
            $year = mdate($datestring, $time);

            return $year;
        }           
    }


    /**
     * Mengambil tahun pada tanggal dimana kunjungan paling akhir dibuat
     * @return String tahun
     */
    public function get_last_year() 
    {
        // Urutkan berdasarkan id_reg secara descending
        $this->db->order_by("id", "desc");
        $query = $this->db->get(SCHEMA.'registrasi_pengobatan');

        // Periksa apakah ada registrasi pengobatan dalam database
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;

            // Jika ada, ambil tahun
            date_default_timezone_set('Asia/Jakarta');
            $datestring = "%Y";
            $time = strtotime($source);
            $year = mdate($datestring, $time);

            return $year;
        }   
    }

    public function get_visit_on_year($year)
    {
        $data = array();
        for ($ii = 1; $ii <= 12; $ii++)
        {
            $data[$ii] = $this->get_visit_on_month($ii,$year);
        }
        return $data;
    }

    public function get_visit_on_month($month,$year)
    {
        $table = 'registrasi_pengobatan';
        $query = $this->db->query("SELECT COUNT(*) FROM ".SCHEMA.$table." WHERE extract (year from tanggal) = '$year' AND extract (month from tanggal) = '$month'");
        $result = $query->row();
        return $result->count;
    }

    public function get_fakultas_visit($month,$year)
    {
                                
        $listfakultas = array("",
            "Fakultas Kedokteran",
            "Fakultas Kedokteran Gigi",
            "Fakultas Matematika dan Ilmu Pengetahuan Alam",
            "Fakultas Teknik",
            "Fakultas Hukum",
            "Fakultas Ekonomi",
            "Fakultas Ilmu Pengetahuan Budaya",
            "Fakultas Psikologi",
            "Fakultas Kesehatan Masyarakat",
            "Fakultas Ilmu Komputer",
            "Fakultas Ilmu Keperawatan",
            "Fakultas Farmasi",
            "Program Vokasi",
            "Program Pascasarjana",
            );

        $listfak = array("",
            "FK",
            "FKG",
            "FMIPA",
            "FT",
            "FH",
            "FE",
            "FIB",
            "FPsi",
            "FKM",
            "FKom",
            "FIK",
            "FF",
            "Vokasi",
            "Pasca",
            );

        $data = array();
        for ($ii = 1; $ii < count($listfakultas); $ii++)
        {
            $data[$ii][0] = $listfak[$ii];
            $data[$ii][1] = $this->get_fakultas_visit_on_month($listfakultas[$ii],$month,$year);
        }
        return $data;
    }

    public function get_fakultas_visit_on_month($fakultas,$month,$year)
    {
        $table = 'registrasi_pengobatan';
        $tablejoin = 'pasien';
        $query = $this->db->query("SELECT COUNT(*) FROM ".SCHEMA.$table." JOIN ".SCHEMA.$tablejoin." ON id_pasien = ".$tablejoin.".id WHERE lembaga_fakultas = '$fakultas' AND extract (year from tanggal) = '$year' AND extract (month from tanggal) = '$month'");
        $result = $query->row();
        return $result->count;
    }

    public function get_kepuasan_on_year($year)
    {
        $datayear = array();
        for ($ii = 1; $ii <= 12; $ii++)
        {
            $datayear[$ii] = $this->get_kepuasan_on_month($ii,$year);
        }
        return $datayear;
    }

    public function get_kepuasan_on_month($month,$year)
    {
        $datamonth = array();
        for ($type = 1; $type <= 4; $type++)
        {
            $datamonth[$type] = $this->get_kepuasan_by_type($type,$month,$year);
        }
        return $datamonth;
    }

    public function get_kepuasan_by_type($type,$month,$year)
    {
        $table = 'registrasi_pengobatan';
        $query = $this->db->query("SELECT COUNT(*) FROM ".SCHEMA.$table." WHERE respon_kepuasan = '$type' AND extract (year from tanggal) = '$year' AND extract (month from tanggal) = '$month'");
        $result = $query->row();
        return $result->count;
    }


}
?>
