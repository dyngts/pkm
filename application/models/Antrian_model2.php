<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');
/*
* Class User Model manages database manipulation
* have been registered, retrieve, edit, delete user's profile
* checked for validate user, available username,
*/

class Antrian_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_all_antrian() {
		$this->load->database();

		$this->db->from('sitrasi.antrian');
		$this->db->join('sitrasi.pasien', 'sitrasi.pasien.id=sitrasi.antrian.id_pasien');
		$this->db->order_by("status", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	function update_status($stat, $id) {
		$this->load->database();
		$data = array('status' => $stat);

		$this->db->where('id_antri', $id);
		$this->db->update('sitrasi.antrian', $data);
		return true;
	}
	function add_antrian($data) {
		$this->load->database();
		$this->db->insert('sitrasi.antrian',$data);
	}
}