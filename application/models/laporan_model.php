<?php
/**
 * Class untuk keperluan laporan
 */
class laporan_model extends CI_Model{
	

    function __construct()
    {

        parent::__construct();
    }
	
	function getlistlembaga()
	{
		$table = SCHEMA.'pasien';
		$this->db->select($table.'.lembaga_fakultas');
		$this->db->where($table.'.jenis_pasien != 3');
		$this->db->distinct();
		$query = $this->db->get($table);
		$result = $query->result_array();
		$list = array();
		foreach ($result as $key) {
			$list[] = $key['lembaga_fakultas'];
		}
			$list[] = 'Lain-lain';
		return $list;
	}

	
	function getlistlembagaon($bulan,$tahun)
	{
		$table1 = SCHEMA.'registrasi_pengobatan';
		$table2 = SCHEMA.'pasien';

		$this->db->select($table2.'.lembaga_fakultas');

		$this->db->join($table2, $table1.'.id_pasien = '.$table2.'.id');

		$this->db->where($table2.'.jenis_pasien != 3');
		$this->db->where('extract(month from '.$table1.'.tanggal) = '.$bulan.' and extract(year from '.$table1.'.tanggal) = '.$tahun);

		$this->db->distinct();

		$query = $this->db->get($table1);
		$result = $query->result_array();
		$list = array();
		foreach ($result as $key) {
			$list[] = $key['lembaga_fakultas'];
		}
			$list[] = 'Lain-lain';
		return $list;
	}

	/**
	 * Mengambil data untuk keperluan pembuatan laporan kunjungan berdasarkan umur
	 * @return query
	 */
	public function get_umur($bulan, $tahun)
	{

		$agerange = array(
									array('0','5',),
									array('5','10',),
									array('10','15',),
									array('15','20',),
									array('20','45',),
									array('45','55',),
									array('55','60',),
									array('60','1000',),
								);

		$result = array();
		$i = 0;

		foreach ($agerange as $range) {
			$poliumum = '1';
			$poligigi = '2';

			$totalumum = $this->get_umur_range($poliumum, $bulan, $tahun, $range[0], $range[1]);
			$totalgigi = $this->get_umur_range($poligigi, $bulan, $tahun, $range[0], $range[1]);

			$result[$i][0] = $totalumum;
			$result[$i][1] = $totalgigi;
			
			$i++;
		}
		
		return $result;
	}

	function get_umur_range($jenispoli, $bulan, $tahun, $minrange, $maxrange)
	{
		$table1 = SCHEMA.'registrasi_pengobatan';
		$table2 = SCHEMA.'pasien';
		$table3 = SCHEMA.'jadwal_harian';
		$table4 = SCHEMA.'jadwal_mingguan';
		$statusselesai = '6';

		$this->db->select($table2.'.id as id_pasien, '.$table1.'.status, '.$table1.'.waktu_reg_akhir');

		$this->db->from($table1);

		$this->db->join($table2, $table1.'.id_pasien = '.$table2.'.id');
		$this->db->join($table3, $table1.'.jadwal_harian = '.$table3.'.id_mingguan');
		$this->db->join($table4, $table4.'.id = '.$table3.'.id_mingguan');

		$this->db->where($table4.'.id_layanan = '.$jenispoli);
		$this->db->where('extract(year from age('.$table2.'.tanggal_lahir)) >=  '.$minrange.' and extract(year from age('.$table2.'.tanggal_lahir)) <  '.$maxrange);
		$this->db->where($table1.'.status = '.$statusselesai);
		$this->db->where('extract(month from '.$table1.'.tanggal) = '.$bulan.' and extract(year from '.$table1.'.tanggal) = '.$tahun);

		$this->db->distinct();
		$query = $this->db->get();
		$result = $query->result_array();

		return count($result);
	}

	public function getlaporanpoli($jenispoli, $bulan, $tahun)
	{
		$listlembaga = $this->getlistlembagaon($bulan,$tahun);
		$listfakultas = array(
				'Fakultas Kedokteran',
				'Fakultas Kedokteran Gigi',
				'Fakultas Matematika dan Ilmu Pengetahuan Alam',
				'Fakultas Teknik',
				'Fakultas Hukum',
				'Fakultas Ekonomi',
				'Fakultas Ilmu Pengetahuan Budaya',
				'Fakultas Psikologi',
				'Fakultas Kesehatan Masyarakat',
				'Fakultas Ilmu Komputer',
				'Fakultas Ilmu Keperawatan',
				'Fakultas Farmasi',
				'Program Vokasi',
				'Program Pascasarjana',
			); //n not used

				$jammulaipagi = "'".date('H:i:s', strtotime('00:01'))."'";
				$jamselesaipagi = "'".date('H:i:s', strtotime('13:00'))."'";
				$jammulaisiang = "'".date('H:i:s', strtotime('13:30'))."'";
				$jamselesaisiang = "'".date('H:i:s', strtotime('23:59'))."'";

		for ($i=0; $i < count($listlembaga) ; $i++) { 

			for ($jenispasien = 1; $jenispasien <= 3 ; $jenispasien++) { 


				$result[$i]['lembaga'] = $listlembaga[$i];
				$result[$i][$jenispasien]['pagi'] = $this->getpoli($jenispoli, $jenispasien, $bulan, $tahun, $jammulaipagi, $jamselesaipagi, $listlembaga[$i]);
				$result[$i][$jenispasien]['sore'] = $this->getpoli($jenispoli, $jenispasien, $bulan, $tahun, $jammulaisiang, $jamselesaisiang, $listlembaga[$i]);

			}
		}
			
		return $result;
	}

	public function getpoli($jenispoli, $jenispasien, $bulan, $tahun, $jammulai, $jamselesai, $fakultas)
	{
		$table1 = SCHEMA.'registrasi_pengobatan';
		$table2 = SCHEMA.'pasien';
		$table3 = SCHEMA.'jadwal_harian';
		$table4 = SCHEMA.'jadwal_mingguan';
		$statusselesai = '6';

		$this->db->select($table2.'.id as id_pasien, '.$table1.'.id as reg_id, '.$table1.'.waktu_reg_akhir');

		$this->db->from($table1);

		$this->db->join($table2, $table1.'.id_pasien = '.$table2.'.id');
		$this->db->join($table3, $table1.'.jadwal_harian = '.$table3.'.id_mingguan');
		$this->db->join($table4, $table4.'.id = '.$table3.'.id_mingguan');

		$this->db->where($table4.'.id_layanan = '.$jenispoli);
		$this->db->where($table2.'.jenis_pasien = '.$jenispasien);
		$this->db->where($table1.'.waktu_reg_akhir >= '.$jammulai.' AND '.$table1.'.waktu_reg_akhir <= '.$jamselesai);
		$this->db->where($table1.'.status = '.$statusselesai);
		$this->db->where('extract(month from '.$table1.'.tanggal) = '.$bulan.' and extract (year from '.$table1.'.tanggal) = '.$tahun);

		if ($fakultas != 'Lain-lain' || $jenispasien != 3)
		$this->db->where($table2.".lembaga_fakultas = '".$fakultas."'");
		
		$this->db->distinct();
		$query = $this->db->get();
		$result = $query->result_array();

		$baru = 0;
		$lama = 0;

		foreach ($result as $key) {
			$id_pasien = $key['id_pasien'];
			$firstcoming = $this->getFirstComing($id_pasien);
			if ($firstcoming['id'] != $key['reg_id']) $lama++;
			else $baru++;
		}

		return array('baru' => $baru, 'lama' => $lama);
	}

	function getFirstComing($id_pasien)
	{
		$table1 = SCHEMA.'registrasi_pengobatan';
		$statusselesai = '6';

		$this->db->from($table1);

		$this->db->where($table1.".id_pasien = '".$id_pasien."'");
		$this->db->where($table1.'.status = '.$statusselesai);

		$this->db->order_by($table1.".id asc");

		$query = $this->db->get();
		return $query->row_array();
	}

	public function getListBerobatPasien($id_pasien)
	{
		$table1 = SCHEMA.'registrasi_pengobatan';
		$statusselesai = '6';

		$this->db->from($table1);

		$this->db->where($table1.".id_pasien = '".$id_pasien."'");
		$this->db->where($table1.'.status = '.$statusselesai);

		$query = $this->db->get();
		return $query->result_array();
	}
	
	/**
	 * Mengambil data untuk keperluan pembuatan laporan kunjungan poli gigi pada periode tertentu
	 * @param  integer $bulan bulan yang dipilih
	 * @param  integer $tahun tahun yang dipilih
	 * @return query        
	 */
	public function get_gigi($bulan, $tahun)
	{
		$this->load->database();

		$query = $this->db->query("select *
			from
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpb  natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpl natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msb natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msl natural full outer join
			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpl natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksl natural full outer join




			(select tamu.peran as fakultas, count(*) as tamu_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpl natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Gigi') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Gigi' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsl");
		
		return $query;
	}
	
	/**
	 * Mengambil data untuk keperluan pembuatan laporan kunjungan poli umum pada periode tertentu
	 * @param  integer $bulan bulan yang dipilih
	 * @param  integer $tahun tahun yang dipilih
	 * @return query        
	 */
	public function get_umum($bulan, $tahun)
	{
		return $this->db->get(SCHEMA.'registrasi_pengobatan');
	}
	
	/**
	 * Mengambil data untuk keperluan pembuatan laporan kunjungan poli umum pada periode tertentu
	 * @param  integer $bulan bulan yang dipilih
	 * @param  integer $tahun tahun yang dipilih
	 * @return query        
	 */
	public function get_umum2($bulan, $tahun)
	{
		$this->load->database();

		$query = $this->db->query(
			"select *
			from
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpb  natural full outer join

			(select mahasiswa.fakultas as fakultas, count(*) as mhs_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as mpl natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_baru

			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msb natural full outer join
			(select mahasiswa.fakultas as fakultas, count(*) as mhs_sore_lama

			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select fakultas, id from pasien where peran ='Mahasiswa' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as mahasiswa
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = mahasiswa.id
			group by mahasiswa.fakultas) as msl natural full outer join

			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as kpl natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksb natural full outer join


			(select karyawan.lembaga as fakultas, count(*) as kar_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select lembaga, id from pasien where peran ='Karyawan' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar) <'".$bulan."') as karyawan
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = karyawan.id
			group by karyawan.lembaga) as ksl natural full outer join




			(select tamu.peran as fakultas, count(*) as tamu_pagi_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_pagi_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir < '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tpl natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_baru
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract(month from waktu_daftar) ='".$bulan."' and extract (year from waktu_daftar)='".$tahun."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsb natural full outer join

			(select tamu.peran as fakultas, count(*) as tamu_sore_lama
			from registrasi_pengobatan,
			(select jadwalid, tanggal
				from jadwal_harian
				where klinik='Umum') as klinik,
			(select peran, id from pasien where peran ='Masyarakat Umum' and extract (year from waktu_daftar)<='".$tahun."' and extract(month from waktu_daftar)<'".$bulan."') as tamu
			where klinik.jadwalid=registrasi_pengobatan.jadwal and klinik.tanggal=registrasi_pengobatan.tanggal and registrasi_pengobatan.wakturegakhir > '12:00' and registrasi_pengobatan.status= 6 and registrasi_pengobatan.id_pasien = tamu.id
			group by tamu.peran) as tsl");

		return $query;
	}

	/**
	 * Mengambil tahun pada tanggal dimana kunjungan pertama dibuat
	 * @return String tahun
	 */
    public function get_first_year() 
    {
    	// Ambil daftar registrasi pengobatan diurutjan berdasarkan id_reg secara ascending
        $this->db->order_by("id", "asc");
        $query = $this->db->get(SCHEMA.'registrasi_pengobatan');

        // Periksa apakah ada registrasi pengobatan dalam database
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
            
            // Jika ada, ambil tahun
            return substr($source, 6, 4);
        }           
    }

	/**
	 * Mengambil tahun pada tanggal dimana kunjungan paling akhir dibuat
	 * @return String tahun
	 */
    public function get_last_year() 
    {
    	// Ambil daftar registrasi pengobatan diurutjan berdasarkan id_reg secara descending
        $this->db->order_by("id", "desc");
        $query = $this->db->get(SCHEMA.'registrasi_pengobatan');

        // Periksa apakah ada registrasi pengobatan dalam database
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
            
            // Jika ada, ambil tahun
            return substr($source, 6, 4);
        }   
    }
}
