<?php

class Model_laporan extends MY_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	/**
	 * function get_pemakaian_obat
	 * @param the type of used
	 * @param the category of the drugs
	 * @param the month
	 * @param the year
	 */
	function get_pemakaian_obat ($condition, $month, $year) {
		/*
			category to choose:
			"Obat Umum", "Obat Alkes dan BHMP Gigi", "Obat Emergency", "Alkes Umum"
		*/

		$this->db->select('nama_obat');
		$this->db->select_sum('jumlah');
		$this->db->from($this->t_log_obat_keluar);
		$this->db->join($this->t_pengeluaran_obat, 'log_obat_keluar.id = pengeluaran_obat.id_log_obat_keluar');
		$this->db->where('extract(year from log_obat_keluar.tgl) =', "$year");
		$this->db->where('extract(month from log_obat_keluar.tgl) =', "$month");
		$this->db->join($this->t_obat, 'pengeluaran_obat.nama_obat = obat.nama');	
		$this->db->where("obat.kategori", "$condition");	
		$this->db->group_by('nama_obat');
		$this->db->order_by('nama_obat');
		$this->db->limit(10);
		$result = $this->db->get();
		return $result->result();

	}



	/**
	 * function get_pengeluaran_obat
	 * @param the type of pengeluaran haha
	 * @param the month
	 * @param the year
	 */
	function get_pengeluaran_obat ($month, $year) {
		/*
			type to choose:
			"Event", "Surat Jalan"
		*/

		// $this->db->select('nama_obat');
		// $this->db->select_sum('jumlah');
		// $this->db->from('log_obat_keluar');
		// $this->db->join('pengeluaran_obat', 'log_obat_keluar.id = pengeluaran_obat.id_log_obat_keluar');
		// $this->db->where('extract(year from log_obat_keluar.tgl) =', "$year");
		// $this->db->where('extract(month from log_obat_keluar.tgl) =', "$month");
		// $this->db->where("log_obat_keluar.jenis", "$condition");	
		// $this->db->group_by('nama_obat');
		// $this->db->order_by('nama_obat');
		// $result = $this->db->get();
		// return $result->result();

		// $query = $this->db->query("SELECT nama_obat, SUM(jumlah) as jumlah
		// 				  FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
		// 				  WHERE l.id = p.id_log_obat_keluar
		// 				  AND extract(year from l.tgl) = '$year'
		// 				  AND extract(month from l.tgl) = '$month'
		// 				  AND l.jenis = '$condition'
		// 				  GROUP BY nama_obat
		// 				  ORDER BY nama_obat
		// 				");

			$query = $this->db->query("SELECT nama_obat,
				SUM(case l.jenis when 'Event' then jumlah else 0 end) AS event,
				SUM(case l.jenis when 'Resep' then jumlah else 0 end) AS resep,
				SUM(case l.jenis when 'Salemba' then jumlah else 0 end) AS salemba
				FROM $this->t_log_obat_keluar l, $this->t_pengeluaran_obat p
				WHERE l.id = p.id_log_obat_keluar
				AND extract(year from l.tgl) = '$year'
				AND extract(month from l.tgl) = '$month'
				GROUP BY nama_obat
				ORDER BY nama_obat
				");

		return $query->result();
	}

	/**
	 * function get_pengeluaran_resep
	 * @param the month
	 * @param the year
	 */
	function get_pengeluaran_resep($month, $year) {
		$this->db->select('nama_obat');
		$this->db->select_sum('jumlah');
		$this->db->from($this->t_resep);
		$this->db->join($this->t_detail_resep, 'resep.id = detail_resep.id_resep');
		$this->db->where('extract(year from resep.waktu) =', "$year");
		$this->db->where('extract(month from resep.waktu) =', "$month");
		// has been proccessed
		$this->db->where_not('resep.status_proses', 0);
		$this->db->where('detail_resep.status', 'true');
		$this->db->group_by('nama_obat');
		$this->db->order_by('nama_obat');
		$query = $this->db->get();
		return $query->result();		
	}

	/** 
     * function get_pemakaian_depok 
     * @param the month 
     * @param the year 
     */
    function get_pemakaian_depok($month, $year, $purpose = '') {


    	if($purpose == 'CATAT') {
    		$query = "select nama_obat, sum(jumlah) as jumlah_pemakaian_depok 
                  from $this->t_log_obat_keluar, $this->t_pengeluaran_obat, $this->t_obat 
                  where log_obat_keluar.id = id_log_obat_keluar and
                  nama = nama_obat and
                  extract(year from log_obat_keluar.tgl) = '$year' and
                  extract(month from log_obat_keluar.tgl) = '$month' 
                  group by nama_obat 
                  order by nama_obat"; 
    	}
    	else {
    		$query = "select nama_obat, temp.jumlah_pemakaian 
                   from (select distinct jumlah_pemakaian from $this->t_stok_awal_bulan limit 10) as temp, $this->t_stok_awal_bulan 
                   where temp.jumlah_pemakaian = stok_awal_bulan.jumlah_pemakaian and
                   stok_awal_bulan.tahun = '$year' and
                   stok_awal_bulan.bulan = '$month'
                   order by nama_obat desc 
                   "; 
    	}

        $result = $this->db->query($query); 
        return $result->result(); 
    } 



	/**
	 * function get_pemakaian_salemba
	 * @param the month
	 * @param the year
	 */
	function get_pemakaian_salemba($month, $year) {
		$this->db->select('nama_obat');
		$this->db->select_sum('jumlah');
		$this->db->from($this->t_transaksi_obat_salemba);
		$this->db->join($this->t_detail_transaksi_salemba, 'transaksi_obat_salemba.id = obat_log_salemba.id_transaksi_salemba');
		$this->db->where('extract(year from transaksi_obat_salemba.tgl) =', "$year");
		$this->db->where('extract(month from transaksi_obat_salemba.tgl) =', "$month");
		$this->db->where('transaksi_obat_salemba.jenis', 'Keluar');
		$this->db->group_by('nama_obat');
		$this->db->order_by('nama_obat');
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * function get_stok
	 * @param the month
	 * @param the year
	 */
	function get_stok($year, $month) {
		// $this->db->select('nama_obat, total_stok, jumlah');
		// $this->db->select_min('tgl');
		// $this->db->from($this->t_log_obat_masuk);
		// $this->db->join($this->t_penerimaan_obat, 'log_obat_masuk.id = penerimaan_obat.id_log_obat_masuk');
		// $this->db->where('extract(year from log_obat_masuk.tgl) =', "$year");
		// $this->db->where('extract(month from log_obat_masuk.tgl) =', "$month");
		// $this->db->group_by('nama_obat, total_stok');
		// $this->db->order_by('nama_obat');
		// $query = $this->db->get();
		// return $query->result();

		$query2 = "select nama_obat, jumlah_stok
				   from $this->t_stok_awal_bulan
				   where extract(year from stok_awal_bulan.tahun) = '$year' and
				   extract(month from stok_awal_bulan.bulan) = '$month'
				   order by nama_obat
				   ";

		$result = $this->db->query($query);
		return $result->result();
	}

	function get_stok_by_category($category, $month, $year) {
		// $this->db->select('nama_obat, total_stok, sediaan');
		// $this->db->select_min('tgl');
		// $this->db->from($this->t_log_obat_masuk);
		// $this->db->join($this->t_penerimaan_obat, 'log_obat_masuk.id = penerimaan_obat.id_log_obat_masuk');
		// $this->db->join($this->t_obat, 'penerimaan_obat.nama_obat = obat.nama');
		// $this->db->where('extract(year from log_obat_masuk.tgl) =', "$year");
		// $this->db->where('extract(month from log_obat_masuk.tgl) =', "$month");
		// $this->db->where("obat.kategori", "$category");	
		// $this->db->group_by('nama_obat, total_stok, sediaan');
		// $this->db->order_by('nama_obat');
		// $query = $this->db->get();
		// return $query->result();




		// query2: asumsi ada tabel stok_awal_bulan
		// atribut: nama_obat, bulan, tahun, jumlah_stok, jumlah_pemakaian

		// create table stok_per_bulan(
		// 	nama_obat namaobat not null,
		// 	bulan char(2),
		// 	tahun char(4),
		// 	jumlah_stok int,
		// 	jumlah_pemakaian int,
		//  jumlah_sisa int
		// );
		// -- keterangan: jumlah stok di update tiap tanggal 1 per bulan, jumlah pemakaian di update setiap hari jam 12 malam

		$query = "select nama_obat, jumlah_stok
				   from $this->t_stok_awal_bulan, $this->t_obat
				   where extract(year from stok_awal_bulan.tahun) = '$year' and
				   extract(month from stok_awal_bulan.bulan) = '$month' and
				   kategori = '$category'
				   order by nama_obat
				   ";

		$result = $this->db->query($query);
		return $result->result();

	}

	function get_pemakaian_depok_by_category($category, $month, $year) {
		$this->db->select('nama_obat');
		$this->db->select_sum('jumlah');
		$this->db->from($this->t_log_obat_keluar);
		$this->db->join($this->t_pengeluaran_obat, 'log_obat_keluar.id = pengeluaran_obat.id_log_obat_keluar');
		$this->db->join($this->t_obat, 'pengeluaran_obat.nama_obat = obat.nama');
		$this->db->where('extract(year from log_obat_keluar.tgl) =', "$year");
		$this->db->where('extract(month from log_obat_keluar.tgl) =', "$month");
		$this->db->where("obat.kategori", "$category");
		$this->db->group_by('nama_obat');
		$this->db->order_by('nama_obat');
		$query = $this->db->get();
		return $query->result();
	}

	function get_obat_datang_sisa($category, $month, $year) {
		$this->db->select('nama_obat, total_satuan');
		$this->db->select_sum('jumlah');
		$this->db->from($this->t_log_obat_masuk);
		$this->db->join($this->t_penerimaan_obat, 'log_obat_masuk.id = penerimaan_obat.id_log_obat_masuk');
		$this->db->join($this->t_obat, 'penerimaan_obat.nama_obat = obat.nama');
		$this->db->where('extract(year from log_obat_masuk.tgl) =', "$year");
		$this->db->where('extract(month from log_obat_masuk.tgl) =', "$month");
		$this->db->where("obat.kategori", "$category");	
		$this->db->group_by('nama_obat, total_satuan');
		$this->db->order_by('nama_obat');
		$query = $this->db->get();
		return $query->result();
	}

	function get_stok_pemakaian_sisa_by_category($category, $month, $year) {

		  // // query1: jika stok diambil dari penerimaan obat pada awal bulan 
    //     $query1 = "select temp.nama_obat, total_stok, jumlah 
    //               from  
    //                 (select nama_obat, min(tgl) as tgl 
    //                  from log_obat_masuk, penerimaan_obat 
    //                  where id = id_log_obat_masuk 
    //                  group by nama_obat 
    //                  order by nama_obat) as temp, log_obat_masuk, penerimaan_obat, obat 
    //               where log_obat_masuk.id = id_log_obat_masuk and
    //               kategori = '$category' and
    //               temp.nama_obat = penerimaan_obat.nama_obat and
    //               temp.tgl = log_obat_masuk.tgl and
    //               extract(year from log_obat_masuk.tgl) = '$year' and
    //               extract(month from log_obat_masuk.tgl) = '$month' 
    //               "; 
  
        // query2: asumsi ada tabel stok_awal_bulan 
        // atribut: nama_obat, bulan, tahun, jumlah_stok, jumlah_pemakaian, jumlah_sisa 
  
        $query2 = "select nama_obat, jumlah_stok, jumlah_penerimaan, jumlah_pemakaian, jumlah_sisa, sediaan 
                   from $this->t_stok_awal_bulan, $this->t_obat 
                   where nama = nama_obat and
                   stok_awal_bulan.tahun = '$year' and
                   stok_awal_bulan.bulan = '$month' and
                   kategori = '$category'
                   order by nama_obat 
                   "; 
  
        $result = $this->db->query($query2); 
        return $result->result(); 

	}

	 /** 
     * function get_stok 
     * @param the month 
     * @param the year 
     */
    function get_stok_pemakaian_sisa($year, $month, $purpose = '') { 


    	if($purpose == 'CATAT') {
    	// query1: jika stok diambil dari penerimaan obat pada awal bulan 
        $query = "select temp.nama_obat, total_stok, jumlah 
                  from  
                    (select nama_obat, min(tgl) as tgl 
                     from $this->t_log_obat_masuk, $this->t_penerimaan_obat 
                     where id = id_log_obat_masuk 
                     group by nama_obat 
                     order by nama_obat) as temp, $this->t_log_obat_masuk, $this->t_penerimaan_obat 
                  where id = id_log_obat_masuk and
                  temp.nama_obat = penerimaan_obat.nama_obat and
                  temp.tgl = log_obat_masuk.tgl and
                  extract(year from log_obat_masuk.tgl) = '$year' and
                  extract(month from log_obat_masuk.tgl) = '$month' 
                  "; 
    	}
    	else {
    		$query = "select nama_obat, jumlah_stok, jumlah_pemakaian, jumlah_penerimaan, jumlah_sisa, sediaan 
                   from $this->t_stok_awal_bulan, $this->t_obat 
                   where nama = nama_obat and
                   stok_awal_bulan.tahun = '$year' and
                   stok_awal_bulan.bulan = '$month'
                   order by nama_obat 
                   "; 
    	}

       
  
        // query2: asumsi ada tabel stok_awal_bulan 
        // atribut: nama_obat, bulan, tahun, jumlah_stok 
  
        // create table stok_per_bulan( 
        //  nama_obat namaobat not null, 
        //  bulan char(2), 
        //  tahun char(4), 
        //  jumlah_stok int, 
        //  jumlah_penerimaan int, 
        //  jumlah_pemakaian int, 
        //  jumlah_sisa int, 
        //  primary key (nama_obat, bulan, tahun), 
        //  foreign key (nama_obat) references obat 
        //  on update cascade, on delete cascade 
        // ); 
        // -- keterangan: jumlah stok di update tiap tanggal 1 per bulan, jumlah pemakaian di update setiap hari jam 12 malam 
  
        
  
        $result = $this->db->query($query); 
        return $result->result(); 
  
    } 

	function get_pemakaian_salemba_by_category($category, $month, $year) {
		// $this->db->select('nama_obat');
		// $this->db->select_sum('jumlah');
		// $this->db->from($this->t_transaksi_obat_salemba);
		// $this->db->join($this->t_detail_transaksi_salemba, 'transaksi_obat_salemba.id = obat_log_salemba.id_transaksi_salemba');
		// $this->db->where("obat.kategori", "$category");
		// $this->db->where('extract(year from transaksi_obat_salemba.tgl) =', "$year");
		// $this->db->where('extract(month from transaksi_obat_salemba.tgl) =', "$month");
		// $this->db->where('transaksi_obat_salemba.jenis', 'Keluar');
		// $this->db->group_by('nama_obat');
		// $this->db->order_by('nama_obat');
		// $query = $this->db->get();
		// return $query->result();

		$query = "select nama_obat, sum(jml_satuan) as jumlah_pemakaian_salemba_category
				  from $this->t_transaksi_obat_salemba, $this->t_detail_transaksi_salemba, $this->t_obat
				  where transaksi_obat_salemba.id = id_transaksi_salemba and
				  obat.jenis = 'Keluar' and
				  nama = nama_obat and
				  kategori = '$category' and
				  extract(year from transaksi_obat_salemba.tgl) = '$year' and
				  extract(month from transaksi_obat_salemba.tgl) = '$month' 
				  group by nama_obat
				  order by nama_obat";

		$result = $this->db->query($query);
		return $result->result();

		
	}

	function get_delivered_stok_this_month($month, $year) { 
        $query = "select nama_obat, sum(jumlah) as jumlah_penerimaan_stok 
                  from $this->t_log_obat_masuk, $this->t_penerimaan_obat 
                  where log_obat_masuk.id = id_log_obat_masuk and
                  extract(year from log_obat_masuk.tgl) = '$year' and
                  extract(month from log_obat_masuk.tgl) = '$month' 
                  group by nama_obat 
                  order by nama_obat"; 
        $result = $this->db->query($query); 
        return $result->result(); 
    } 
  
    function get_delivered_stok_this_month_by_category($category, $month, $year) { 
        $query = "select nama_obat, sum(jumlah) as jumlah_penerimaan_stok_category 
                  from $this->t_log_obat_masuk, $this->t_penerimaan_obat, $this->t_obat 
                  where log_obat_masuk.id = id_log_obat_masuk and
                  nama = nama_obat and
                  kategori = '$category' and
                  extract(year from log_obat_masuk.tgl) = '$year' and
                  extract(month from log_obat_masuk.tgl) = '$month' 
                  group by nama_obat 
                  order by nama_obat"; 
        $result = $this->db->query($query); 
        return $result->result(); 
    }

    function get_jumlah_stok_bulan_tahun_tertentu($month, $year) {
    	$query = "select temp.nama_obat, total_stok, jumlah 
                  from  
                    (select nama_obat, min(tgl) as tgl 
                     from $this->t_log_obat_masuk, $this->t_penerimaan_obat 
                     where id = id_log_obat_masuk 
                     group by nama_obat 
                     order by nama_obat) as temp, log_obat_masuk, penerimaan_obat 
                  where id = id_log_obat_masuk and
                  temp.nama_obat = penerimaan_obat.nama_obat and
                  temp.tgl = log_obat_masuk.tgl and
                  extract(year from log_obat_masuk.tgl) = '$year' and
                  extract(month from log_obat_masuk.tgl) = '$month' 
                  ";

        $hasil = $this->db->query($query);

        return $hasil->result();
    }

    

}