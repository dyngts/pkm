<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surat_jalan_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function _cek_no_surat_jalan($no) {
		$no_length = strlen($no);
		if($no_length > 0) {
			$query = $this->db->query("SELECT DISTINCT *
								   FROM $this->t_log_obat_keluar
								   WHERE no_surat_jalan = '$no'
								   ");

			if($query->num_rows() > 0) {
				return 1;
			}
		}
		
		return 0;
	}

	public function _ambil_log_keluar_terbaru() {
		$query = $this->db->query("SELECT *
								   FROM $this->t_log_obat_keluar
								   ORDER BY tgl DESC, jam DESC
								   ");

		return $query->first_row();
	}

	public function _cek_total_obat($nama_obat) {
		$query = $this->db->query("SELECT SUM(jml_satuan) as total
								   FROM $this->t_stok_obat
								   WHERE nama_obat = '$nama_obat'
								   AND status_kadaluarsa = '0'
								   GROUP BY nama_obat");

		if($query->num_rows() > 0) {
			$obat = $query->first_row();
			return $obat->total;
		}

		return 0;
	}

	public function _jumlah_baris_tertentus($nama) {
		$query = $this->db->query("SELECT *
								   FROM $this->t_stok_obat k
								   WHERE nama = '$nama'
								   		 AND jml_satuan > 0
								   ORDER BY tgl_kadaluarsa asc");
		return $query->num_rows();
	}

	public function _ambil_baris_tertentus($nama = '') {
		$query = $this->db->query("SELECT *
								   FROM $this->t_stok_obat k
								   WHERE nama_obat = '$nama'
								   		 AND status_kadaluarsa = '0'
								   ORDER BY tgl_kadaluarsa asc");
		$kad_obats = $query->result();
		$kumpulan_kad_obat = array();
		$i = 0;

		foreach ($kad_obats as $kad_obat) {
			$kumpulan_kad_obat[$i++] = $kad_obat;
		}

		return $kumpulan_kad_obat;
	}

	public function _ambil_daftar_nama_obat() {
		$query = $this->db->query("SELECT id, nama
								   FROM $this->t_obat order by nama asc");

		$data_nama_obats = array();
		foreach ($query->result() as $data) {
			$data_nama_obats[$data->id] = $data->nama;
		}

		return $data_nama_obats;

	}

	public function _ambil_daftar_jenis_obat() {
		$query = $this->db->query("SELECT id, jenis
								   FROM $this->t_obat
								   WHERE jenis != '' order by jenis asc");
		/*$data_jenis_obat = array();

		foreach($query->result() as $data) {
			$data_jenis_obat[$data->jenis] = $data->jenis;
		}
		*/

		return $data_jenis_obat;
	}
}

/* End of file surat_jalan_model.php */
/* Location: ./application/models/surat_jalan_model.php */