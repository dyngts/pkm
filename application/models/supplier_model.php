<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function _ambil_semua_supplier($mulai = 0) {

		$query1 = $this->db->query("SELECT id, nama, alamat, no_telepon, status::int
									FROM $this->t_supplier
									ORDER BY nama ASC
									LIMIT 10
									OFFSET $mulai
			");

		$query2 = $this->db->query("SELECT id, nama, alamat, no_telepon, status::int
									FROM $this->t_supplier
									ORDER BY nama ASC
			");

		$hasil = array();
		$hasil['daftar_supplier'] = $query1;
		$hasil['jumlah_baris'] = $query2->num_rows();

		return $hasil;
	}

	public function _ambil_baris_supplier_obat_tertentu($id, $limit, $mulai) {
		$query = $this->db->query("SELECT *
						  FROM $this->t_supplier, $this->t_obat
						  WHERE supplier.id = obat.id
						  		AND supplier.id = $id order by jenis asc
						  LIMIT $limit OFFSET $mulai ");
		return $query;
	}

	public function _jumlah_baris_supplier_obat_tertentu($id) {
		$query = $this->db->query("SELECT *
						  FROM $this->t_supplier, $this->t_obat
						  WHERE supplier.id = obat.id
						  		AND supplier.id = $id
						  ");

		return $query->num_rows();
	}

}

/* End of file supplier_model.php */
/* Location: ./application/models/supplier_model.php */