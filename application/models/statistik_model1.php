<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_statistik_kepuasan($tahun, $namalevel, $levelpuas){
        $query = $this->db->query(
        "select * 
        from
            (select extract (month from tanggal) as bulan, count(*) as $namalevel
            from (select time_stamp, nilai from sitrasi.pasien, sitrasi.RESPONS_KEPUASAN, sitrasi.REGISTRASI_PENGOBATAN
            where RESPONS_KEPUASAN.ID_registrasi = REGISTRASI_PENGOBATAN.ID_reg and RESPONS_KEPUASAN.ID_pasien = PASIEN.ID) as wawa where extract (year from tanggal) ='$tahun' and nilai = '$levelpuas'
            group by Bulan) as one order by bulan asc;");
        return $query->result();
    }

    public function get_statistik_periode($tahun) {

        $query = $this->db->query("select jp.bulan, COALESCE(jumlah.jumlah_pengunjung,0)
        from sitrasi.jumlah_pengunjung jp LEFT OUTER JOIN (select extract (month from tanggal) as Bulan, count(*) as jumlah_pengunjung
        from sitrasi.registrasi_pengobatan where extract (year from tanggal) = '".$tahun."' and registrasi_pengobatan.status=6
        group by Bulan) as jumlah
        on jp.bulan = jumlah.Bulan");

        return $query->result();
    }

    public function get_statistik_fakultas($tahun, $bulan) {
        $query = $this->db->query("select jpf.fakultas, COALESCE (jumlah.jumlah_pengunjung,0)
            from sitrasi.jumlah_pengunjung_fakultas jpf LEFT OUTER JOIN (SELECT fakultas, count (*) as jumlah_pengunjung
            from sitrasi.registrasi_pengobatan, sitrasi.pasien
            where extract (year from tanggal) ='".$tahun."' and extract (month from tanggal) = '".$bulan."'
            and registrasi_pengobatan.id_pasien = pasien.id and registrasi_pengobatan.status=6
            group by fakultas) as jumlah
            on jpf.fakultas= jumlah.fakultas");
        
        return $query->result();
    }

    public function get_first_year(/*$jenis*/) 
    {
        $this->db->order_by("id_reg", "asc");
        //$this->db->order_by("poli", "desc");
        $query = $this->db->get('sitrasi.registrasi_pengobatan');
        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
                    
            return substr($source, 6, 4);
        }           
    }

    public function get_last_year(/*$jenis*/) 
    {
        $this->db->order_by("id_reg", "desc");
        //$this->db->order_by("poli", "desc");
        $query = $this->db->get('sitrasi.registrasi_pengobatan');

        if ($query->num_rows() > 0 ) {
            $row = $query->row();
            $source = $row->tanggal;
                    
            return substr($source, 6, 4);
        }   
    }
}
?>
