<html>
<head>
    <title>SIPOTEK</title>
    <link href="<?php echo base_url('css/foundation.css'); ?>
    " rel="stylesheet" />
    <link href="<?php echo base_url('css/foundation.min.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/normalize.css'); ?>
    " rel="stylesheet" />
    <link href="<?php echo base_url('css/bootstrap-responsive.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/bootstrap.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/bootstrap.min.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/style.css'); ?>
    " rel="stylesheet"  />
    <link href="<?php echo base_url('css/datepicker.css'); ?>
    " rel="stylesheet"  />

    <script type="text/javascript" src="<?php echo site_url('js/bootstrap.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('js/jquery-1.7.1.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('js/jquery-1.7.1.min.js'); ?>"></script>
     <script type="text/javascript" src="<?php echo site_url('js/bootstrap-datepicker.js'); ?>"></script>
    
</head>
<body class="sipotek">
    <div id="wrap2">
        <div class="fixed" style="background-color: #43474c">
            <nav class="top-bar" style="background-color: #323a45; margin-bottom: 1px">
                <ul class="title-area">
                    <!-- Title Area -->
                    <!--    <li class="name">
                    <a href="#">
                        <img src="img/logo.png" alt="SIPOTEK" width="181" height="45" border="0" title="SIPOTEK" />
                    </a>
                </li>
                -->
                <li class="name">
                    <h1>
                        <a href="#">
                            <img src="<?php echo site_url('img/logo.png') ?>
                            " alt="SIPOTEK" width="181" height="45" border="0" title="SIPOTEK" />
                        </a>
                    </h1>
                </li>
                <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                <li class="toggle-topbar menu-icon">
                    <a href="#">
                        <span>Menu</span>
                    </a>
                </li>
            </ul>
            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <div class="right" style="margin-right: 3%"> <i class="icon-user icon-white" style="margin-right: 3px"></i>
                    <a href="#" style="color: #a3a3a3; font-size: 12px">
                        <?php echo $this->session->userdata('username'); ?></a>
                    <a href="<?php echo site_url('home_controller/logout'); ?>
                        " style="margin-left: 10px">
                        <button class="btn btn-danger" type="button" style="width: 81px; font-size: 12px"> <i class="icon-off icon-white"></i>
                            Logout
                        </button>
                    </a>

                </div>
            </section>
        </nav>
        <nav class="top-bar">
            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="divider hide-for-small"></li>
                    <li>
                        <a href="<?php echo site_url('home_controller/welcome') ?>">Home</a>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'obat') { ?> active  <?php } ?>has-dropdown">
                        <a href="#">Obat</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?php echo site_url('obat_controller/tambah_masuk'); ?>">Entri Obat Masuk</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('obat_controller/tambah'); ?>">Tambahkan Obat</a>
                            </li>
                            <li class="divider"></li>
                             <li>
                                <a href="<?php echo site_url('obat_controller/kumpulan_obat_detil'); ?>">Lihat Daftar Obat Per Tanggal Kadaluarsa</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('obat_controller/kumpulan_obat_menjelang_kadaluarsa'); ?>">Lihat Daftar Kadaluarsa Obat</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('obat_controller/kumpulan_log_obat_masuk'); ?>">Lihat Log Obat Masuk</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('obat_controller/kumpulan_log_obat_keluar'); ?>">Lihat Log Obat Keluar</a>
                            </li>
                             <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('obat_controller/tambah_log_salemba'); ?>">Catat Log Salemba</a>
                            </li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'resep') { ?> active <?php } ?>">
                        <a href="<?php echo site_url('resep_controller/kumpulan'); ?>">Resep Obat</a>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'supplier') { ?> active <?php } ?> has-dropdown">
                        <a href="#">Supplier</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?php echo site_url('supplier_controller/tambah'); ?>">Tambahkan Data Supplier</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('supplier_controller/kumpulan'); ?>">Lihat Data Supplier</a>
                            </li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'surat_jalan') { ?> active <?php } ?>has-dropdown">
                        <a href="#">Surat Jalan</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?php echo site_url('surat_jalan_controller/tambah') ?>">Buat Surat Jalan</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('surat_jalan_controller/kumpulan') ?>">Lihat Surat Jalan</a>
                            </li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'faktur') { ?> active <?php } ?>">
                        <a href="<?php echo site_url('faktur_controller/index'); ?>">Faktur</a>
                    </li>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'statistik') { ?> active <?php } ?> has-dropdown">
                        <a href="#">Statistik</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?php echo site_url('statistik_controller/statistik_umum') ?>">Statistik Obat</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('statistik_controller/statistik_dokter') ?>">Statistik Obat Oleh Dokter</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('statistik_controller/statistik_pasien') ?>">Statistik Obat Oleh Pasien</a>
                            </li>
                        </ul>
                    </li>
                    <?php if($this->session->userdata('flag_admin') == TRUE) {  ?>
                    <li class="divider"></li>
                    <li class="<?php if($nama_aktif == 'pengguna') { ?> active <?php } ?> has-dropdown">
                        <a href="#">Apoteker</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?php echo site_url('admin/pengguna_controller/tambah') ?>">Tambah Apoteker</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('admin/pengguna_controller/kumpulan') ?>">Lihat Daftar Apoteker</a>
                            </li>
                        </ul>
                    </li>
                    <?php }  ?>
                </ul>
            </section>
        </nav>
    </div>
    <div id="user-area">
        <div id="header-login">
            <p style="color: #F5FFF5; margin-top: 5px; font-size: 16px">
                <img src="<?php echo site_url('img/user-icon.png') ?>" alt="" width="20" height="20" title="" style="float: left; margin: 3px 20px 0 30px"/>
                Pengguna
                <p></div>
                <div id="middle-login2" style="background-color: ">
                    <div style="padding: 5px 10% 0 30px">
                        <dl>
                            <dt>
                                <i class="icon-user icon-white" style=""></i>
                                Nama
                            </dt>
                            <dd style="margin-left: 11%">
                                <?php echo $this->session->userdata('username'); ?></dd>
                        </dl>
                    </div>
                    <div style="padding: 0 10% 0 30px">
                        <dl>
                            <dt>
                                <i class="icon-tags icon-white" style=""></i>
                                Username
                            </dt>
                            <dd style="margin-left: 11%">
                                <?php echo $this->session->userdata('username'); ?></dd>
                        </dl>
                    </div>
                    <div style="padding: 0 10% 0 30px">
                        <dl>
                            <dt>
                                <i class="icon-barcode icon-white" style=""></i>
                                Kode Pegawai
                            </dt>
                            <dd style="margin-left: 11%">
                                <?php echo $this->session->userdata('nip'); ?></dd>
                        </dl>
                    </div>
                    <div style="padding: 0 10% 0 30px">
                        <dl>
                            <dt>
                                <i class="icon-briefcase icon-white" style=""></i>
                                Role
                            </dt>
                            <?php 
                                    if($this->
                            session->userdata('flag_admin') == TRUE) {
                                    ?>
                            <dd style="margin-left: 11%">
                                <?php echo "Kepala Apoteker"; ?></dd>
                            <?php } else { ?>
                            <dd style="margin-left: 11%">
                                <?php echo "Apoteker"; } ?></dd>
                        </div>

                    </div>
                </div>