<!-- FORM BEGIN -->
<div class="container">
    <div class="profil">
        <fieldset>
        <h2>FORM PEGAWAI</h2>
		<?php 
		foreach ($row as $r) :
			$id = $r->id;
			$username = $r->username;
			$password = $r->password_pegawai;
			$nama = $r->nama;
			$jenisKelamin = $r->jeniskelamin;
			$golDarah = $r->goldarah;
			$kewarga = $r->kewarganegaraan;
			$tmpLahir = $r->tempatlahir;
			$tglLahir = $r->tanggallahir;
			$agama = $r->agama;
			$alamat = $r->alamat;
			$status = $r->statuspernikahan;
			$peran = $r->peran;
			$noTelp = $r->no_telepon;
		endforeach;

    if (isset($msg)) {
      echo $msg;
    }
    echo "<div class=\"row\" style=\"text-align: right\">";
    echo "<a href=".site_url("pegawai_controller/editpassword/$id")."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a></div>"; 
		//$page='editpegawaisukses';
        echo form_open_multipart('pegawai_controller/updatepegawai/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
		//echo form_open('pegawai_controller/updatepegawai'.'/'.$id); 
		 //echo form_open_multipart('upload/do_upload'.$page,$id);
		?>
		
        <div class="row">
            <div class="label">
                <label>ID Pegawai<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
            </div>
            <div class="input">
            <p class="p_form">:</p>
                <input type="text" name="id" value="<?php echo set_value('id',$id);?>" class="input_form" id="name_form" disabled="disabled">
            </div>
        </div>
            <!--
            <div class="row">
                          <div class="label">
                            <label>Password <strong class="required">*</strong><br/><em class="english">Password</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="password" name="password" value="<?php echo set_value('password',$password);?>" class="input_form" id="password">
                          

                          <?php 
                           if ($this->session->userdata('id') == $id)
                              echo "<input type=\"button\" value=\"Lihat Password\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('password').type = 'text'\" onmouseup=\"document.getElementById('password').type = 'password'\">"; 
                              echo form_error('password','<p class="message-error">','</p>'); ?>
                              </div>
                          <div class="note">
                            <span><label>Masukkan password baru.</label></span>
                          </div>
                       </div> -->

        <div class="row">
            <div class="label">
                <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
            </div>
            <div class="input">
                <p class="p_form">:</p> 
                <input type="text" name="nama" value="<?php echo set_value('nama',$nama);?>" class="input_form" id="name_form" > </input>
            </div>
            <?php echo form_error('nama','<p class="message-error">','</p>'); ?>
            <div class="note">
                <span><label>Masukkan nama lengkap sesuai dengan kartu identitas.</label></span>
            </div>
        </div>
                   
                  <div class="row">
                    <div class="label">
                      <label>Username<strong class="required">*</strong><br/><em class="english">Username</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" style="text-transform: lowercase" name="username" value="<?php echo set_value('username',$username);?>" class="input_form" id="name_form">
                    </div>
                     <?php echo form_error('username','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan username yang Anda inginkan.</label></span>
                    </div>
                  </div>

        <div class="row">
            <div class="label">
                <label>No telp<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
            </div>
            <div class="input">
                <p class="p_form">:</p>
                <input type="text" name="no_telepon" value="<?php echo set_value('no_telepon',$noTelp);?>" class="input_form" id="name_form">
            </div>
            <?php echo form_error('no_telepon','<p class="message-error">','</p>'); ?>
            <div class="note">
                <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
            </div>
        </div>

        <div class="row">
            <div class="label">
                <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
            </div>
            <div class="input">
                <p class="p_form">:</p> 
                <textarea name ="alamat" class="input_form address_form" value ="<?php echo set_value('alamat',$alamat);?>"><?php echo $alamat;?></textarea>
            </div>
            <?php echo form_error('alamat','<p class="message-error">','</p>'); ?>
            <div class="note">
                <span><label>Masukkan alamat sesuai dengan kartu identitas</label></span>
            </div>
        </div>
		
		
        <div class="row">
			<div class="label">
				<label>Tempat Lahir<strong class="required">*</strong><br/><em class="english">Place of birth</em></label>
			</div>
			<div class="input">
				<p class="p_form" id="p_birthday">:</p> 
				<div id="place" class="input">
					<input type="text" id="birth_place_form" class="input_form" value="<?php echo set_value('tempatLahir',$tmpLahir);?>" name="tempatLahir">
				</div>
			</div>
            <?php echo form_error('tempatLahir','<p style="background-color: #FFDDDD;
    border-color: #CC9999;
    padding: 7px;
    padding-left: 7px;
    text-align: left;
    font-size: 8pt;
    line-height: 8pt;
    margin-left: 210px;
    margin-right: 160px;
    text-align: left;">','</p>'); ?>
			<div class="note">
				<span><label>Masukkan tempat lahir.</label></span>
			</div>
		</div>
					
        <div class="row">
			<div class="label">
				<label>Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Date of birth</em></label>
		</div>
			<p class="p_form" id="p_birthday">:</p> 
			<div class="input "> 
				<div  id="place" class="input">
                    <input name ="tglLahir"type="text" id="demo3" class="input_form" value="<?php echo set_value('demo3'); ?>" name="demo3" style="display:none">
                    <input name ="tglLahir" value = "<?php echo $tglLahir ?>"type="text" id="demo4" class="input_form_disabled" value="<?php echo set_value('demo4',$tglLahir); ?>" name="demo4" readonly="readonly">
                    <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                    <script src="<?php echo base_url('js/datetimepicker_css.js');?>" type="text/javascript"></script>
                    <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo3','yyyyMMdd','','','','','past')" style="cursor:pointer"/>
				</div>
			</div>
            <?php echo form_error('tglLahir','<p class="message-error">','</p>'); ?>
			<div class="note">
				<span><label>Masukkan tanggal lahir.</label></span>
			</div>
		</div>
		
		
        <div class="row">
            <div class="label">
                <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
            </div>
            <div class="input">
                <p class="p_form">:</p> 
				<?php if ($jenisKelamin == "Laki-Laki") {?>
                    <input type="radio" value="Laki-Laki" name="sex" class="input_form sex_form" checked ="checked" <?php echo set_radio('sex', $jenisKelamin);?> > Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                    <input type="radio" value="Perempuan" name="sex" class="input_form sex_form" <?php echo set_radio('sex',$jenisKelamin); ?> > Perempuan
					<?php } 
					else {?>
					<input type="radio" value="Laki-Laki" name="sex" class="input_form sex_form" > Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                    <input type="radio" value="Perempuan" name="sex" class="input_form sex_form" checked ="checked" <?php echo set_radio('sex', $jenisKelamin);?>> Perempuan
				<?php }?>
            </div>
            <?php echo form_error('sex','<p class="message-error">','</p>'); ?>
            <div class="note">
                <span><label>Pilih jenis kelamin.</label></span>
            </div>
        </div>

                    <div class="row">
                        <div class="label">
                            <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="agama" size="1" class="dropdown_form">
                                <option value="Islam" <?php if ($agama == "Islam") $one = TRUE; else $one = false ;  echo set_select('agama', 'Islam', $one); ?>>Islam</option>
                                <option value="Kristen" <?php if ($agama == "Kristen") $two = TRUE; else $two = false ;  echo set_select('agama', 'Kristen', $two); ?>>Kristen</option>
                                <option value="Katolik" <?php if ($agama == "Katolik") $three = TRUE; else $three = false ;  echo set_select('agama', 'Katolik', $three); ?>>Katolik</option>
                                <option value="Hindu" <?php if ($agama == "Hindu") $four = TRUE; else $four = false ;  echo set_select('agama', 'Hindu', $four); ?>>Hindu</option>
                                <option value="Budha" <?php if ($agama == "Budha") $five = TRUE; else $five = false ;  echo set_select('agama', 'Budha', $five); ?>>Budha</option>
                                <option value="Kong Hu Cu" <?php if ($agama == "Kong Hu Cu") $six = TRUE; else $six = false ;  echo set_select('agama', 'Kong Hu Cu', $six); ?>>Kong Hu Cu</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih agama Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="status_pernikahan" size="1" class="dropdown_form">
                                <option value="Belum Menikah" <?php if ($status == "Belum Menikah") $nope = TRUE ; else $nope = false; echo set_select('status_pernikahan', 'Belum Menikah', $nope); ?>>Belum menikah</option>
                                <option value="Menikah" <?php if ($status == "Menikah") $yep = TRUE ; else $yep = false; echo set_select('status_pernikahan', 'Menikah', $yep); ?>>Menikah</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih status pernikahan Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

        <div class="row">
            <div class="label">
                <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
            </div>
            <div class="input">
            <p class="p_form">:</p> 
            <input type="text" name="kewarga" value="<?php echo set_value('kewarga',$kewarga);?>"class="input_form">
            </div>
             <?php echo form_error('kewarga','<p class="message-error">','</p>'); ?>
			<div class="note">
				<span><label>Pilih kewarganegaraan</label></span>
			</div>
        </div>

        <div class="row">
            <div class="label">
                <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
            </div>
        <div class="input">
            <p class="p_form">:</p> 
            <?php $pilihan2 = array('Tidak Tahu'=>'Tidak Tahu', 'A'=>'A', 'B'=>'B', 'AB'=>'AB', 'O'=>'O'); 
            echo form_dropdown('golDarah', $pilihan2, $golDarah); ?>
              <!--  <select name ="golDarah"size="1" class="dropdown_form">
					       <option  value="Tidak Tahu" <?php echo set_select('golDarah', 'Tidak Tahu'); ?> >Tidak Tahu</option>
                          <option  value="A" <?php echo set_select('golDarah', 'A'); ?>>A</option>
                          <option  value="B" <?php echo set_select('golDarah', 'B'); ?>>B</option>
                          <option  value="AB" <?php echo set_select('golDarah', 'AB'); ?>>AB</option>
                          <option  value="O" <?php echo set_select('golDarah', 'O'); ?>>O</option>
                </select>-->
        </div>
        <?php echo form_error('golDarah','<p class="message-error">','</p>'); ?>
            <div class="note">
                <span><label>Pilih golongan darah.</label></span>
            </div>
        </div>

        <div class="row">
                        <div class="label">
                          <label>Peran Pegawai<strong class="required">*</strong><br/><em class="english">Role</em></label>
                        </div>
                         <div class="input">
                            <p class="p_form">:</p> 
                            <select name ="peran" size=1 class="" id="pegawaiPkm_form" disabled="disabled">
                              <option value="<?php echo $peran;?>"><?php echo $peran;?></option>

                            </select>
                        </div>
                        <div class="note">
                          <span><label>Pilih peran pegawai.</label></span>
                        </div>
                      </div>
					<!--ini jika authentication pegawai, maka form ini dimunculkan -->
					 
					  
					  
                      <div class="row">
                        <div class="label">
                          <label>Foto<br/><em class="english">Photograph</em></label>
                        </div>
                        <div class="input">
                          <input type="file" name="foto" value="upload" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                          <span><label>Upload foto Anda.</label></span>
                        </div>
                        <div class="image">
                          <img src="<?php base_url();?>image/4.png" alt="foto profil">
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label><strong class="required">*</strong>Wajib diisi.</label>
                        </div>
                      </div>

                      <!-- WHITESPACE -->
                      <div class="whitespace"></div>

                      <div class="row">
                        <div class="row" id="submit">
                          <a href="<?php echo site_url("site");?>"><input type="button" value="batal" class="button orange submit_button"></a>
                          <?php echo anchor("pegawai_controller/updatepegawai/".$id,'<input type="submit" value="submit" class="button green submit_button">');?>
                        </div>
                      </div>
                    </fieldset>
                  </form>
              </div>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
