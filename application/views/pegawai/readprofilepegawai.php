            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PEGAWAI</h2>
                  <div>
                    <div class="row">
                      <div class="label">
                        <label>Nama Lengkap<br/><em class="english">Full Name</em></label>
                      </div>
                        <p class="p_form">: dr.Chaerunnisa</p>
                    </div>
                    <div class="row">
                        <div class="label">
                          <label>Alamat<br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: Jalan Kelapa Sawit No.3</p>
                        </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Jenis Kelamin<br/><em class="english">Sex</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: Perempuan</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Agama<br/><em class="english">Religion</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Islam</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Status Pernikahan<br/><em class="english">Marital Status</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Sudah Menikah</p>
                      </div>
                    </div>
        
                    <div class="row">
                      <div class="label">
                        <label>Kewarganegaraan <br/><em class="english">Nationality</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Indonesia</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: A</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Peran Pegawai<br/><em class="english">Role</em></label>
                      </div>
                     <div class="input">
                      <p class="p_form">: Koordinator Pelayanan</p>
                    </div>
                    </div>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <a href="<?php echo base_url(); ?>.'index.php/pegawai_controller/editpegawai"><input type="button" value="Ubah Data" class="button orange submit_button"></a>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="http://localhost/ci/image/3.png" alt="foto profil">
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>