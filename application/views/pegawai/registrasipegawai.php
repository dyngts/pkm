            <!-- FORM BEGIN -->
            <div class="container">
              <!--<form action="http://localhost/ci/index.php/registrasipegawaisuccess"> -->
        		<?php //echo validation_errors();?>
        		<?php //echo form_open('pegawai_controller/add_pegawai'); ?>
        		<?php //echo form_open_multipart('upload/do_upload');?>
        		<?php echo form_open_multipart('pegawai_controller/add_pegawai', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
        		<?php //echo form_open('site/registrasipasien/umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
            

                <fieldset>
                  <div class="table_box">
                    <h2>FORM PEGAWAI</h2>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="nama" value="<?php echo set_value('nama');?>" class="input_form" id="name_form">
                    </div>
					           <?php echo form_error('nama','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan nama lengkap sesuai dengan kartu identitas.</label></span>
                    </div>
                  </div>
                   
                  <div class="row">
                    <div class="label">
                      <label>Username<strong class="required">*</strong><br/><em class="english">Username</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" style="text-transform: lowercase" name="username" value="<?php echo set_value('username');?>" class="input_form" id="name_form">
                    </div>
                     <?php echo form_error('username','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan username yang Anda inginkan.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea name="alamat" class="input_form address_form"><?php echo set_value('alamat');?></textarea>
                    </div>
					           <?php echo form_error('alamat','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan alamat sesuai dengan kartu identitas</label></span>
                    </div>
                  </div>

                
                    <div class="row">
						<div class="label">
							<label>Tempat Lahir<strong class="required">*</strong><br/><em class="english">Place of birth</em></label>
						</div>
						<div class="input">
							<p class="p_form" id="p_birthday">:</p> 
							<div id="place" class="input">
							<input type="text" id="birth_place_form" class="input_form" value="<?php echo set_value('tempatLahir');?>" name="tempatLahir">
							</div>
						</div>
            <?php echo form_error('tempatLahir','<p class="message-error">','</p>'); ?>
						<div class="note">
							<span><label>Masukkan tempat lahir.</label></span>
						</div>
					</div>
					
                    <div class="row">
						<div class="label">
							<label>Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Date of birth</em></label>
						</div>
						<p class="p_form" id="p_birthday">:</p> 
						<div class="input "> 
							<div  id="place" class="input">
                                    <input name ="tglLahir" type="text" id="demo3" class="input_form" value="<?php echo set_value('tglLahir'); ?>" name="demo3" style="display:none">
                                    <input name ="tglLahir" type="text" id="demo4" class="input_form_disabled" value="<?php echo set_value('tglLahir'); ?>" name="demo4" readonly="readonly">
                                    <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                                    <script src="<?php echo base_url('js/datetimepicker_css.js');?>" type="text/javascript"></script>
                                    <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo3','yyyyMMdd','','','','','past')" style="cursor:pointer"/>
							</div>
						</div>
            <?php echo form_error('tglLahir','<p class="message-error">','</p>'); ?>
						<div class="note">
                        <span><label>Masukkan tanggal lahir.</label></span>
						</div>
					</div>

                  <div class="row">
                    <div class="label">
                      <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                    </div>
                   <div class="input">
                      <p class="p_form">:</p> 
                      <input type="radio" value="Laki-laki" name="sex" class="input_form sex_form" <?php echo set_radio('sex', 'Laki-laki'); ?> > Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                      <input type="radio" value="Perempuan" name="sex" class="input_form sex_form" <?php echo set_radio('sex','Perempuan'); ?> > Perempuan
                    </div>
					           <?php echo form_error('sex','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih jenis kelamin.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select name="agama" size=1 class="" id="blood_form">
					  <option  value="">Pilih Agama</option>
                      <option  value="Islam"<?php echo set_select('agama', 'Islam'); ?>>Islam</option>
                      <option  value="Katolik"<?php echo set_select('agama', 'Katolik'); ?>>Katolik</option>
                      <option  value="Pretestan" <?php echo set_select('agama', 'Protestan'); ?> >Protestan</option>
                      <option  value="Hindu" <?php echo set_select('agama', 'Hindu'); ?>>Hindu</option>
                      <option  value="Buddha" <?php echo set_select('agama', 'Buddha'); ?>>Buddha</option>
                      </select>
                    </div>
					         <?php echo form_error('agama','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih Agama.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
					  
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select name="status" size="1" class="dropdown_form">
					  <option value="">Pilih Status</option>
                        <option value="Belum Menikah" <?php echo set_select('status', 'Belum Menikah'); ?>>Belum menikah</option>
                        <option value="Menikah"<?php echo set_select('status', 'Menikah'); ?>>Menikah</option>
                      </select>
                    </div>
					           <?php echo form_error('status','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih status pernikahan</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
					  
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="kewarga" value="<?php echo set_value('kewarga'); ?>" class="input_form">
                    </div>
					           <?php echo form_error('kewarga','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih kewarganegaraan</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select name="golDarah" size="1" class="dropdown_form">
						  
						              <option  value="Tidak Tahu" <?php echo set_select('golDarah', 'Tidak Tahu'); ?> >Tidak Tahu</option>
                          <option  value="A" <?php echo set_select('golDarah', 'A'); ?>>A</option>
                          <option  value="B" <?php echo set_select('golDarah', 'B'); ?>>B</option>
                          <option  value="AB" <?php echo set_select('golDarah', 'AB'); ?>>AB</option>
                          <option  value="O" <?php echo set_select('golDarah', 'O'); ?>>O</option>
                      </select>
                    </div>
					           <?php echo form_error('golDarah','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih golongan darah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Peran Pegawai<strong class="required">*</strong><br/><em class="english">Role</em></label>
                    </div>
                     <div class="input">
                        <p class="p_form">:</p> 
                        <select name="peran" size=1 class="" id="pegawaiPkm_form">
						  <option value="">Pilih Salah Satu</option>
                          <option value="Penanggung Jawab PKM" <?php echo set_select('peran', 'Penanggung Jawab PKM')?> >Penanggung Jawab PKM</option>
                          <option value="Koordinator Pelayanan" <?php echo set_select('peran', 'Koordinator Pelayanan')?> >Koordinator Pelayanan</option>
                          <option value="Dokter Umum" <?php echo set_select('peran', 'Dokter Umum')?> >Dokter Umum</option>
                          <option value="Dokter Gigi" <?php echo set_select('peran', 'Dokter Gigi')?> >Dokter Gigi</option>
                          <option value="Dokter Estetika Medis" <?php echo set_select('peran', 'Dokter Estetika Medis')?> >Dokter Estetika Medis</option>                          
                          <option value="Koordinator Logistik" <?php echo set_select('peran', 'Koordinator Logistik')?> >Koordinator Logistik</option>
                          <option value="Koord Administrasi&Keuangan" <?php echo set_select('peran', 'Koord Administrasi&Keuangan')?> >Koordinator Administrasi dan Keuangan</option>
                          <option value="Perawat Gigi" <?php echo set_select('peran', 'Perawat Gigi')?> >Perawat Gigi</option>
                          <option value="Perawat Umum" <?php echo set_select('peran', 'Perawat Umum')?> >Perawat Umum</option>
                          <option value="Kesekretariatan" <?php echo set_select('peran', 'Kesekretariatan')?> >Kesekretariatan</option>
                          <option value="Staf Keuangan" <?php echo set_select('peran', 'Staf Keuangan')?> > Staf Keuangan</option>
                          <option value="Apoteker" <?php echo set_select('peran', 'Apoteker')?> > Apoteker </option>
                          <option value="Asisten Apoteker" <?php echo set_select('peran', 'Asisten Apoteker')?> >Asisten Apoteker</option>
                          <option value="Administrasi Loket" <?php echo set_select('peran', 'Administrasi Loket')?> >Administrasi Loket</option>
                        </select>
                    </div>
					           <?php echo form_error('peran','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Pilih peran pegawai.</label></span>
                    </div>
                  </div>

				   <div class="row">
                    <div class="label">
                      <label>Nomor Telepon<strong class="required">*</strong><br/><em class="english">Telephone</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="notelepon" value="<?php echo set_value('notelepon');?>" class="input_form" id="name_form">
                    </div>
					           <?php echo form_error('notelepon','<p class="message-error">','</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan Nomor Telepon.</label></span>
                    </div>
                  </div>
				  
                  <div class="row">
                    <div class="label">
                      <label>Foto<br><em class="english">Photograph</em></label>
                    </div>
					<p class="p_form">:</p> 
                    <div class="input">
                      <input type="file" name="foto" value="upload" id="name_form"> 
                    </div>
                    <div class="note">
                      <span><label>Upload foto Anda.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>
				
                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row" id="submit">
						 <?php echo anchor("site/home/",'<input type="button" value="back" class="button orange submit_button">');?>
						<?php echo anchor("pegawai_controller/pegawailist/",'<input type="submit" value="submit" class="button green submit_button ">');?>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
			
            <!-- FORM END -->
			
            <!-- WHITESPACE -->
            <div class="whitespace"></div>