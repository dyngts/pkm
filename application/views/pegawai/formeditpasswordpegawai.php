            <!-- FORM BEGIN -->
            <div class="container">

            <?php echo validation_errors();?>
            <?php echo $msg;?>
            <?php echo form_open_multipart('pegawai_controller/editpassword/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              
                <fieldset>
                  <h2>Edit Password Pegawai PKM UI</h2>
		<?php echo "<h3>Buat password baru untuk ".$id." - ".$this->pegawai_model->get_name($id)."</h3>"?>
                <?php                                        
                    if (true) echo "
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Password lama<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"password\" name=\"oldpassword\" value=\"".set_value('password')."\" class=\"input_form\" id=\"password\">
                            
                            <input type=\"button\" value=\"Lihat Password\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('password').type = 'text'\" onmouseup=\"document.getElementById('password').type = 'password'\">
                        
                        </div>
                        <div class=\"note\">
                            <span><label>Ubah password dengan mengetik secara langsung.</label></span>
                        </div>
                    </div>";                                     
                    echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Password baru<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"password\" name=\"password\" value=\"".set_value('password')."\" class=\"input_form\" id=\"password\">
                            
                            <input type=\"button\" value=\"Lihat Password\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('password').type = 'text'\" onmouseup=\"document.getElementById('password').type = 'password'\">
                        
                        </div>
                        <div class=\"note\">
                            <span><label>Ubah password dengan mengetik secara langsung.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label><strong class=\"required\">*</strong>Wajib diisi.</label>
                        </div>
                    </div>
                    <!-- WHITESPACE -->
                    <div class=\"whitespace\"></div>
                    "
                ;?>

                    <div class="row">
                        <div class="row" id="submit">
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
