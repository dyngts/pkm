            <!-- BEGIN -->
            <div class="container">
			<!--
            <script type="text/javascript">
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_pegawailist').deleteRow(i);
                  }
                }
            </script> -->

		
              <div class="row">
                <a href="<?php echo site_url('pegawai_controller/registrasipegawai');?>">
                  <input type="button" value="Buat Akun Pegawai Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Pegawai PKM</h2>
		
				  
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama Pegawai</th>
                        <th class="even-col">Peran Pegawai</th>
                        <th class="last">Tindakan </th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <td class="last" colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_pegawailist">
                     
						
						<?php 
						$rownum =$page_num;
						foreach ($row as $r) :
						$rownum++;
						$ro = $r->id;?>
						
							<tr class="inner-table">
							<td><?php echo $rownum; ?></td>
							<td><?php echo anchor("pegawai_controller/profilepegawai/".$r->id,$r->id);?></td>
							<td class="text-left"><?php echo anchor("pegawai_controller/profilepegawai/".$r->id,$r->nama);?></td>
							<td class="text-left"><?php echo $r->peran; ?></td>
							<td class="text-left"> <div class="row" class="table_button_group">
							<?php echo form_open ("pegawai_controller/deletepegawai/".$r->id,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?> 
							<?php echo anchor("pegawai_controller/editpegawai/".$r->id,'<input type="button" value="Ubah" class="button green table_button">');?>
              <?php if (substr ($r->peran, 0,-5) != 'Dokter' ){ ?>
							       <input type="submit" value="Hapus" class="button red table_button">
              <?php } echo form_close();?>
              </div> 
              </td> 
              </tr>
					<?php endforeach; ?>
                    </tbody>
                  </table>
                  <?php echo $pagination;?>
                </div>
              </div>
         </div>
			
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>