            <!-- BEGIN -->
            <div class="container">
              <?php 
						
				foreach ($row as $r) :
						
						$id = $r->id;
						$username = $r->username;
						$password = $r->password_pegawai;
						$nama = $r->nama;
						$jenisKelamin = $r->jeniskelamin;
						$golDarah = $r->goldarah;
						$kewarga = $r->kewarganegaraan;
						$tmpLahir = $r->tempatlahir;
						$tglLahir = $r->tanggallahir;
						$agama = $r->agama;
						$alamat = $r->alamat;
						$status = $r->statuspernikahan;
						$peran = $r->peran;
						$noTelp = $r->no_telepon;
						$foto = $r->Foto;
						endforeach;
						?>
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PEGAWAI</h2>
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Perubahan data berhasil disimpan! </p>
                    </div>
                  </div>
                  <div>
                    <div class="row">
                      <div class="label">
                        <label>Peran<br/><em class="english">Role</em></label>
                      </div>
                        <p class="p_form">: <?php echo $peran; ?></p>
                    </div>
					
                    <div class="row">
                      <div class="label">
                        <label>Nama Lengkap<br/><em class="english">Full Name</em></label>
                      </div>
                        <p class="p_form">: <?php echo $nama; ?> </p>
                    </div>
                    <div class="row">
                        <div class="label">
                          <label>Alamat<br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: <?php echo $alamat ;?> </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                          <label> Tempat, Tanggal Lahir<br/><em class="english">Place and date of birth </em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: <?php echo $tempatlahir.", ".$tanggallahir; ?></p>
                        </div>
                    </div>
                    <div class="row">
                      <div class="label">
                        <label>Jenis Kelamin<br/><em class="english">Sex</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: <?php echo $jeniskelamin; ?> </p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Agama<br/><em class="english">Religion</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $agama; ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Status Pernikahan<br/><em class="english">Marital Status</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $statuspernikahan;?> </p>
                      </div>
                    </div>
        
                    <div class="row">
                      <div class="label">
                        <label>Kewarganegaraan <br/><em class="english">Nationality</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $kewarganegaraan;?> </p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $goldarah; ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>No. Telepon<br/><em class="english">Telephone</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: <?php echo $no_telepon; ?></p>
                      </div>
                    </div>
					<div class="row">
						<?php echo anchor("pegawai_controller/editpegawai/".$id,'<input type="button" value="Ubah Data" class="button orange submit_button">');?>                
                    </div>

                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div> 
                <div class="rightside">
                  <div class="image">
					<?php if (empty($foto)) {?>
						<img src="<?php echo base_url();?>image/3.png" alt="foto profil">
					<?php} 
					else {?>
						<img alt="foto profil" src="<?php echo base_url('foto/'.$foto);?>">
					<?php } ?>
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>

