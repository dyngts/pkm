<div class="container">
	<!--Body content-->
	<div class="page-header" style="margin: 150px 0 0 0">
		<h1>Ubah Data Supplier</h1>
	</div>
	<?php

	$telepons = $supplier->no_telepon;
	$temp = explode('-', $telepons);
	?>
	<div id="tableForm" style="margin: 15px 0 0 0">
		
		<?php echo form_open('supplier_controller/ubah/' . $supplier->id, array('class' => 'form-horizontal', 'name' => 'ubah', 'id' => 'ubah')); ?>
		<p><b style="color:red; font-size: 16px">* Harus Diisi</b></p>
		<div class="control-group">
			<label class="control-label" for="inputEmail">ID Supplier</label>
			<div class="controls">
				<input type="text" placeholder="Id Supplier" name="id_supplier" id="id_supplier" value="<?php echo $supplier->id; ?>" style="background-color: #EAEDF1; height: 30px" readonly>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputEmail">Nama Supplier<b style="color:red; font-size: 15px">*</b></label>
			<div class="controls">
				<input type="text" placeholder="Nama Supplier" name="supplier" id="supplier" value="<?php echo $supplier->nama; ?>" style="background-color: #EAEDF1; height: 30px">
				<div style="color:red;margin-left:3%;"><?php echo form_error('supplier'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Alamat<b style="color:red; font-size: 15px">*</b></label>
			<div class="controls">
				<input type="text" id="inputPassword" placeholder=" Alamat Supplier" name="alamat" id="alamat" value="<?php echo $supplier->alamat; ?>" style="background-color: #EAEDF1; height: 30px">
				<div style="color:red;margin-left:3%;"><?php echo form_error('alamat'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Nomor Telepon</label>
			<div class="controls">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input type="text" id="inputPassword" placeholder="" name="kode_area" id="kode_area" style="background-color: #EAEDF1; height: 30px; width:30px" value="<?php echo set_value('kode_area', $temp[0]); ?>" maxlength="4"></td>
						<td>-</td>
						<td><input type="text" id="inputPassword" placeholder="Nomor Telepon" name="telepon" id="telepon" style="background-color: #EAEDF1; height: 30px;" value="<?php echo set_value('telepon', $temp[1]); ?>" maxlength="10"></td>
					</tr>
				</table>
				<div style="color:red;margin-left:3%;"><?php echo form_error('telepon'); ?></div>
				<div style="color:red;margin-left:3%;"><?php echo form_error('kode_area'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Kota<b style="color:red; font-size: 15px">*</b></label>
			<div class="controls">
				<input type="text" id="inputPassword" placeholder="Kota" name="kota" id="kota" value="<?php echo $supplier->kota; ?>" style="background-color: #EAEDF1; height: 30px">
				<div style="color:red;margin-left:3%;"><?php echo form_error('kota'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Nama Kontak yang Dapat Dihubungi</label>
			<div class="controls">
				<input type="text" id="inputPassword" placeholder="Nama Kontak" name="kontak" id="kontak" value="<?php echo $supplier->contact_person; ?>" style="background-color: #EAEDF1; height: 30px">
				<div style="color:red;margin-left:3%;"><?php echo form_error('kontak'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Email</label>
			<div class="controls">
				<input type="email" id="inputPassword" placeholder="Email" name="email" id="email" value="<?php echo $supplier->email; ?>" style="background-color: #EAEDF1; height: 30px">
				<div style="color:red;margin-left:3%;"><?php echo form_error('email'); ?></div>  
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Status</label>
			<div class="controls">
				<select id="status" name="status">
					<option value="aktif" <?php if($supplier->status == 1) { ?>selected="selected" <?php } ?>>Aktif</option>
					<option value="tidak" <?php if($supplier->status == 0) { ?>selected="selected" <?php } ?>>Tidak Aktif</option>
				</select>
			</div>
		</div>
		<div class="form-actions" style="background-color: #D2D1D1">
			<input type="submit" class="button green" title="simpan" value="Simpan" id="simpan" name="simpan" />
			<input type="submit" class="button green" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" />
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
</div>