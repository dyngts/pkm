<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Daftar Data Supplier</h1>
                </div>
                <?php if($this->session->userdata('notif_mess')) { ?>
                <div class="message message-green">
                  <p class="p_message"><?php echo $this->session->userdata('notif_mess'); ?> </p>
              </div>
              <?php 
              $this->session->unset_userdata('notif_mess');
                }
                
              ?>
                <div style="margin: 20px 0 0 0">
                    <table class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <thead>
                                <th class="even-col">No</th>
                                <th class="">Nama Supplier</th>
                                <th class="even-col">Alamat Supplier</th>
                                <th class="">Nomor Telepon</th>
                                <th class="even-col">Status</th>
                                <th colspan="3"> Tindakan</th>
                            </thead>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 0;
                            foreach ($suppliers->result() as $supplier) {
                           
                            ?>
                            <tr>
                                <td class="text-right"><?php echo ++$mulai; ?></td>
                                <td class="text-left"><?php echo $supplier->nama; ?></td>
                                <td class="text-left"><?php echo $supplier->alamat; ?></td>
                                <td class="text-right"><?php echo $supplier->no_telepon; ?></td>
                                <td><?php echo ($supplier->status == 1) ? 'Aktif' : 'Tidak Aktif'; ?></td>
                                <td><a href="<?php echo site_url('supplier_controller/detil/'.$supplier->id); ?>" style="color: #51aded"><i class="icon-info-sign"></i> Detail</a></td>
                                <td><a href="<?php echo site_url('supplier_controller/ubah/'.$supplier->id); ?>" style="color: #51aded"><i class="icon-refresh"></i> Ubah</a></td>
                                <td><a href="<?php echo site_url('supplier_controller/ganti_status/'.$supplier->id); ?>" style="color: #51aded" onClick="return confirm( 'Yakin ingin mengubah status supplier ini?' )"><?php if($supplier->status == 1) { ?> Deaktivasi <?php } else { ?> Aktifkan <?php } ?></a></td>
                            </tr>				
                            <?php }  ?>
                        </tbody>
                    </table>
                    <?php echo $pagination_links; ?>
                </div>
                </div>
            </div>	