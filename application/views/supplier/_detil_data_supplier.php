<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Detil Data Supplier</h1>
                </div>
                 <a class="button green" href="<?php echo site_url('supplier_controller/kumpulan'); ?>" title="simpan" value="<< Kembali ke Daftar Supplier" id="simpan" name="simpan" ><< Kembali ke Daftar Supplier</a>
                <div style="margin: 20px 0 0 0">
                    <table>
                    	<tbody>
                    		<tr>
                    			<td>Nama Supplier</td>
                    			<td><?php echo $supplier->nama; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Alamat Supplier</td>
                    			<td><?php echo $supplier->alamat; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Nomor Telepon</td>
                    			<td><?php echo $supplier->no_telepon; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Kota</td>
                    			<td><?php echo $supplier->kota; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Kontak yang Dapat Dihubungi</td>
                    			<td><?php echo $supplier->contact_person; ?></td>
                    		</tr>
                    		<tr>
                    			<td>E-mail</td>
                    			<td><?php echo $supplier->email; ?></td>
                    		</tr>
                    		<tr>
                    			<td>Status</td>
                    			<td><?php echo ($supplier->status == 't') ? 'Aktif' : 'Tidak Aktif'  ?></td>
                    		</tr>
                    	</tbody>
                    </table>
<!--
                    <div class="control-group">
                        <label class="control-label" for="">Nama Supplier : <?php echo $supplier->nama_supplier; ?></label>
                    </div>
                    <p>Alamat Supplier : <?php echo $supplier->alamat; ?></p>
                    <p>Nomor Telepon : <?php echo $supplier->no_telp; ?></p>
                    <p>Kota : <?php echo $supplier->kota; ?></p>
                    <p>Kontak yang Dapat dihubungi : <?php echo $supplier->contact_person; ?></p>
                    <p>E-mail : <?php echo $supplier->email; ?></p>
                    <p>Status : <?php echo ($supplier->status == 1) ? 'Aktif' : 'Tidak Aktif'  ?></p>-->
                    <table class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th style="text-align: center">No</th>
                                <th style="text-align: center">Jenis Obat</th>
                                <th style="text-align: center">Nama Obat</th>
                                <th style="text-align: center">Satuan</th>
                                <th style="text-align: center">Harga per Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php 
                            foreach ($supplier_obats->result() as $supplier_obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $supplier_obat->jenis; ?></td>
                                <td><?php echo $supplier_obat->nama; ?></td>
                                <td style="text-align: center"><?php echo $supplier_obat->sediaan; ?></td>
                                <td><?php echo $supplier_obat->harga_satuan; ?></td>
                            </tr>			
                            <?php }  ?>
                        </tbody>
                       
                    </table>
                     <?php echo $pagination_links; ?>
                </div>
            </div>
        </div>