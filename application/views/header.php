<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $title; ?></title>
	<head>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery-ui.css" />
		<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	</head>
	<body>
		<div id = 'wrapper'>
			<header>
				<div id="banner">
					<div id="top">
						<div id="logo"><img src="<?php echo base_url(); ?>images/UI.png" width="120" height="120" /></div>
						<div id="pkm">Sistem Informasi Pelayanan PKM<br> Universitas Indonesia</div>
						<div id="pkm2">Veritas, Probitas, Iustitia</div>
					</div>      	
					
				</div>
				<div id="session">
						<?php echo $session ?>
						<?php echo $role ?>
						<?php echo $name ?>
					</div>
					<?php echo $logout ?>
			</header>
			<div id = 'pagecontent'>