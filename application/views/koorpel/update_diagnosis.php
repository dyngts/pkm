<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin mengubah tindakan ini ?");
	if (agree)
	return true ;
	else
	return false ;
}
</script>
<?php 
	$access = 0;

	if ($this->authentication->is_pelayanan()) {
		$access = 1;
	}
	else if ($this->authentication->is_adm_keu()) {
		$access = 2;
	}
	else if($this->authentication->is_pj()){
		$access = 3;
	}
	
	function setreadonly($access) {
		if($access != 1) {
			echo 'readonly="readonly"';
		}
	}
	
	function setreadonlyharga($access) {
		if($access != 2) {
			echo 'readonly="readonly"';
		}
	}
?>
<center>
	<div class='midtindakan'>
		<h2> Deskripsi Diagnosis </h2>
	<?php $attribute = array('onsubmit'=>"return confirmPost()");
	echo form_open_multipart("koorpel/form_updatediagnosis", $attribute); ?>	
	<ul id ='rekammedis'>
		<fieldset>
		<li><label><b style="color:red;">*Harus diisi</b></label></li>
		<li><label>Id Tindakan<b style="color:red;">*</b>:</label>  <input class="input_form" type='text' name='kodediagnosis' value='<?php echo $row->kode;?>' readonly="readonly" /></li>
		<li><label>Nama Tindakan Baru<b style="color:red;">*</b>:</label> <input class="input_form" type='text' name='namadiagnosis' value='<?php echo $row->jenis_penyakit;?>' <?php setreadonly($access); ?>/><?php echo form_error('namadiagnosis'); ?></li>
		
		<li><label>Keterangan:</label> <textarea class="input_form address_form" rows="8" name='keterangan' <?php if($access==3){echo"";} else{setreadonly($access);} ?>><?php echo $row->keterangan;?></textarea></li>
		</form>
		<div class="row">
		  <?php echo form_open_multipart('simpel/diagnosis', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
		  <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
		  </form>
			<input type = 'submit' value="Submit" class='button red submit_button'/>
		</div>
		</fieldset>
	</ul>
	<?php echo form_close(); ?>
	</div>
</center>