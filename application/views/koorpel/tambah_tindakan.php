<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin membuat tindakan ini ?");
	if (agree)
	return true ;
	else
	return false ;
}	
$(document).ready(function() { $(".pilihpoli").select2(); });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2.css" />
<center>
	<?php $attribute = array('onsubmit'=>"return confirmPost()");echo form_open("koorpel/form_addtindakan",$attribute); ?>
	<?php echo validation_errors(); ?>
	<div class='midtindakan'>
		<h2> Deskripsi Tindakan </h2>
	<ul id ='rekammedis'>
		<fieldset>
		<li><label><b style="color:red;">*Harus diisi</b></label></li>
		<li><label>Id Tindakan<b style="color:red;">*</b>:</label> <input type='text' name='idtindakan'/><?php echo form_error('id'); ?>
		<div class="helper" style="color:red;">ID Tindakan haruslah sepanjang 3 karakter.</div>	
		</li><br>	
		<li><label>Nama Tindakan<b style="color:red;">*</b>: </label><input type='text' name='namatindakan'/><?php echo form_error('namatindakan'); ?></li><br>
		
		<li><label>Poli<b style="color:red;">*</b>: </label><select class='pilihpoli' name ='pilihpoli' id='poli' style="width:270px;">
				<option value="Umum"> Umum</option>
				<option value='Gigi'> Gigi</option>
				<option value='Estetika Medis'> Estetika Medis</option>
				<option value='Radiologi'> Radiologi</option>
				<option value='Laboratorium'> Laboratorium</option>
				<option value='Ambulans'> Ambulans</option>
				</select>
				
				</li><br>
		
		<li><label>Harga Tindakan Untuk Umum<b style="color:red;">*</b>: </label><input type='text' name='hargaumum' /><?php echo form_error('hargaumum'); ?></li><br>
		
		<li><label>Harga Tindakan Untuk Karyawan<b style="color:red;">*</b>: </label><input type='text' name='hargakaryawan'/><?php echo form_error('hargakaryawan'); ?></li><br>
		
		<li><label>Harga Tindakan Untuk Mahasiswa<b style="color:red;">*</b>: </label><input type='text' name='hargamahasiswa' value='0'/><?php echo form_error('hargamahasiswa'); ?></li><br>
		
		<li><label>Ketentuan: </label><textarea rows="4" cols="30" name="anamnesa"></textarea></li><br>
		<a href='<?php echo base_url();?>simpel/tindakan' class='button red' style="width:250px">Kembali</a>
		<input type = 'submit' class='button orange'/>
		</fieldset>
	</ul>
	</div>
	<?php echo form_close(); ?>
</center