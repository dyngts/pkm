<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin mengubah tindakan ini ?");
	if (agree)
	return true ;
	else
	return false ;
}

$(document).ready(function() { $(".pilihpoli").select2(); });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2.css" />
<?php 
	$access = 0;

	if ($this->authentication->is_pelayanan()) {
		$access = 1;
	}
	else if ($this->authentication->is_adm_keu()) {
		$access = 2;
	}
	else if($this->authentication->is_pj()){
		$access = 3;
	}
	
	function setreadonly($access) {
		if($access != 1) {
			echo 'readonly="readonly"';
		}
	}
	
	function setreadonlyharga($access) {
		if($access != 2) {
			echo 'readonly="readonly"';
		}
	}
?>

<center>
	<div class='midtindakan'>
		<h2> Deskripsi Tindakan </h2>
	<?php $attribute = array('onsubmit'=>"return confirmPost()");
	echo form_open("koorpel/form_updatetindakan", $attribute); ?>	
	<ul id ='rekammedis'>
		<fieldset>
		<li><label><b style="color:red;">*Harus diisi</b></label></li>
		<li><label>Id Tindakan<b style="color:red;">*</b>:</label>  <input class="input_form" type='text' name='idtindakan' value='<?php echo $row->id;?>' readonly="readonly" /></li>
		<li><label>Nama Tindakan Baru<b style="color:red;">*</b>:</label> <input class="input_form" <?php if($access==3){echo"";} else{setreadonly($access);} ?>type='text' name='namatindakan' value='<?php echo $row->nama;?>'  /><?php echo form_error('namatindakan'); ?></li>
		<li><label>Poli Pelayanan<b style="color:red;">*</b>:</label>
		<select class='pilihpoli'name='poli' id='poli' style="width:270px;">
			<option value="Umum" <?php if($row->jenis_pelayanan == 'Umum') {echo "selected='selected'";}?> >Umum</option>
			<option value="Gigi" <?php if($row->jenis_pelayanan == 'Gigi') {echo "selected='selected'";}?> >Gigi</option>
			<option value="Estetika" <?php if($row->jenis_pelayanan == 'Estetika') {echo "selected='selected'";}?> >Estetika</option>
			<option value="Laboratorium" <?php if($row->jenis_pelayanan == 'Laboratorium') {echo "selected='selected'";}?> >Laboratorium</option>
			<option value="Radiologi" <?php if($row->jenis_pelayanan == 'Radiologi') {echo "selected='selected'";}?> >Radiologi</option>
			<option value="Ambulans" <?php if($row->jenis_pelayanan == 'Ambulans') {echo "selected='selected'";}?> >Ambulans</option>
		</select>
		</li>
		<li><label>Harga Baru Tindakan untuk Mahasiswa<b style="color:red;">*</b> :</label>  <input class="input_form" type='text' name='hargatindakanm'value='<?php echo $row->tarif_mahasiswa;?>' <?php if($access==3){echo"";} else{setreadonlyharga($access);} ?>/><?php echo form_error('hargatindakanm'); ?></li>
		<li><label>Harga Baru Tindakan untuk Karyawan<b style="color:red;">*</b> :</label>  <input class="input_form" type='text' name='hargatindakank'value='<?php echo $row->tarif_karyawan;?>' <?php if($access==3){echo"";} else{setreadonlyharga($access);} ?>/><?php echo form_error('hargatindakank'); ?></li>
		<li><label>Harga Baru Tindakan untuk Umum<b style="color:red;">*</b> :</label>  <input class="input_form" type='text' name='hargatindakanu'value='<?php echo $row->tarif_umum;?>' <?php if($access==3){echo"";} else{setreadonlyharga($access);} ?>/><?php echo form_error('hargatindakanu'); ?></li>
		<li><label>Ketentuan:</label> <input class="input_form" type='text' name='ketentuan' value='<?php echo $row->ketentuan;?>' <?php setreadonly($access); ?> /></li>
		</form>
		<div class="row">
		  <?php echo form_open_multipart('simpel/tindakan', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
		  <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
		  </form>
			<input type = 'submit' value="Submit" class='button red submit_button'/>
		</div>
	</fieldset>
	</ul>
	</div>
	<?php echo form_close(); ?>
</center>