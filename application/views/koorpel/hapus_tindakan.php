<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin menghapus tindakan ini ?");
	if (agree)
	return true ;
	else
	return false ;
}	
</script>

<center>
	<div class="midtindakan">
		<h2> Deskripsi Tindakan </h2>
	<?php 
	$attribute = array('onsubmit'=>"return confirmPost()");
	echo form_open("koorpel/form_deletetindakan/".$row->id."",$attribute); ?>
		<ul id ='rekammedis'>
			<fieldset>
			<li><label>Id Tindakan:</label><input type='text' name='idtindakan' value="<?php echo $row->id;?>" readonly="readonly"/></li><br>
			<li><label>Nama Tindakan Baru:</label> <input type='text' name='namatindakan' value="<?php echo $row->nama;?>" readonly="readonly"/></li><br>
			<li><label>Poli Pelayanan:</label> <input type='text' name='poli' value="<?php echo $row->jenis_pelayanan;?>" readonly="readonly"/></li><br>
			<li><label>Harga Baru Tindakan untuk Mahasiswa :</label><input type='text' name='hargaumum' value="<?php echo $row->tarif_mahasiswa;?>" readonly="readonly"/> </li><br>
			<li><label>Harga Baru Tindakan untuk Karyawan :</label><input type='text' name='hargakaryawan' value="<?php echo $row->tarif_karyawan;?>" readonly="readonly"/></li><br>
			<li><label>Harga Baru Tindakan untuk Umum :</label><input type='text' name='hargamahasiswa' value="<?php echo $row->tarif_umum;?>" readonly="readonly" /> </li><br>
			<li><label>Ketentuan:</label> <textarea rows="4" cols="30" name="anamnesa" value="<?php echo $row->ketentuan;?>" readonly="readonly"></textarea></li><br>
			</fieldset>
			<a href='<?php echo base_url();?>simpel/tindakan' class='button orange' style="width:250px">Kembali</a>
			<input type = 'submit' class='button red' value='Hapus Tindakan'/>
		</ul>
	<?php echo form_close(); ?>

	</div>
</center