<div id="searchdiv">
	<?php echo form_open("koorpel/searchdiagnosis"); ?>
	<p><input type='text' name='searchquery' placeholder='Cari Diagnosis' id='search_box' class='input_form'/>
	<input type='submit' value='Cari Diagnosis' class='button red'/>
	<a href='<?php echo base_url();?>simpel/tindakan'><button class='button orange'>Kembali</button></a>
	<?php echo form_close(); ?>
</div>
<div id="tambahtindakan">
	<?php echo form_open("koorpel/addDiagnosis"); ?>
	<input type='submit' value='Tambah Diagnosis Baru' class='button green'/>
	<?php echo form_close(); ?>
</div>	
<br>
<center>
	<div id = 'tabel_diagnosis'>
		<div id="kurung"><h2 class="text-left">Daftar Diagnosis yang ada: </h2></div>
	        <?php if($query != null) {
	        	echo '<div class="tabel_pasien"><table><tr><th>
	                Nama Tindakan
	            </th>
	            <th>
	                Keterangan
	            </th>
	            <th>
	                Yang dapat anda lakukan
	            </th>
	        </tr>';


	        	foreach  ($query as $row) {
				   echo '<tr>';
				   echo '<td>';
				   echo $row['jenis_penyakit'];
				   echo '</td>';
				   echo '<td>';
				   echo $row['keterangan'];
				   echo '</td>';
				   echo '<td>'; 
				   echo anchor("koorpel/updatediagnosis/".$row['kode']." ","<button class='button orange'>Update</button>");
				   echo " ";
				   echo anchor("koorpel/deletediagnosis/".$row['kode']." ","<button class='button red'>Delete</button>");
				   echo '</td>';
				   echo '</tr>';
				}
				echo '</div>';
            }

          	else echo '<h2>Tidak ada tindakan dengan nama seperti itu</h2>';	
	        ?>
	    </table>
	    
	
	</div>
	<br>
	<?php echo form_open("koorpel/searchdiagnosis"); ?>
	<p><input type='text' name='searchquery' placeholder='Cari Diagnosis' id='search_box' class='input_form' style='float:none;'/>
	<input type='submit' value='Cari Diagnosis' class='button green'/>
	<a href='<?php echo base_url();?>simpel/diagnosis' class='button orange'>Kembali</a>
	<?php echo form_close(); ?>
	</center>