<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin menghapus tindakan ini ?");
	if (agree)
	return true ;
	else
	return false ;
}	
</script>

<center>
	<div class="middiagnosis">
		<h2> Deskripsi Diagnosis </h2>
	<?php 
	$attribute = array('onsubmit'=>"return confirmPost()");
	echo form_open("koorpel/form_deletediagnosis/".$row->kode."",$attribute); ?>
	<ul id ='rekammedis'>
		<fieldset>
		<li><label><b style="color:red;">*Harus diisi</b></label></li>
		<li><label>Id Diagnosis<b style="color:red;">*</b>:</label>  <input type='text' name='kodediagnosis' value='<?php echo $row->kode;?>' readonly="readonly" /></li><br>
		<li><label>Nama Diagnosis Baru<b style="color:red;">*</b>:</label> <input type='text' name='namadiagnosis' readonly="readonly" value='<?php echo $row->jenis_penyakit;?>'/><?php echo form_error('namadiagnosis'); ?></li><br>
		
		<li><label>Keterangan:</label> <textarea rows="8" name='keterangan' readonly="readonly"><?php echo $row->keterangan;?></textarea></li><br>
		</fieldset>
		<a href='<?php echo base_url();?>simpel/diagnosis' class='button orange' style="width:250px">Kembali</a>
		<input type = 'submit' class='button red' value='Hapus'/>
	</ul>
	<?php echo form_close(); ?>
	</div>
</center