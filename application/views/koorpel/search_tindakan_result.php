	<?php echo form_open("koorpel/searchtindakan"); ?>
	<p><input type='text' name='searchquery' placeholder='Cari Tindakan' id='search_box' class='input_form' style='float:none;'/>
	<input type='submit' value='Cari Tindakan' class='button red'/>
	<a href='<?php echo base_url();?>simpel/tindakan'><button class='button orange'>Kembali</button></a>
	<?php echo form_close(); ?>
	<div id="tambahtindakan">
		<?php echo form_open("koorpel/addTindakan"); ?>
		<input type='submit' value='Tambah Tindakan Baru' class='button green'/>
		<?php echo form_close(); ?>
	</div>	
	<center>

	<div id = 'tabel_tindakan'>
	        <?php if($query != null) {
	        	echo '<div class="tabel_pasien" ><table><tr>

	        		<th class="first">
	                Nama Tindakan
	            </th>
	            <th class="even-col">
	                Jenis Pelayanan
	            </th>
	            <th class="first">
	                Harga Tindakan <br/>untuk Umum
	            </th>
	            <th class="even-col">
	                Harga Tindakan <br/>untuk Karyawan
	            </th>
	            <th class="first">
	                Harga Tindakan <br/>untuk Mahasiswa
	            </th>
	            <th class="even-col">
	                Tindakan
	            </th>
	        </tr>';


	        	foreach  ($query as $row) {
				   echo '<tr>';
				   echo '<td class="text-left">';
				   echo $row['nama'];
				   echo '</td>';
				   echo '<td class="text-left">';
				   echo $row['jenis_pelayanan'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_umum'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_karyawan'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_mahasiswa'];
				   echo '</td>';
				   echo '<td>'; 
				   echo anchor("koorpel/updatetindakan/".$row['id']." ","<button class='button orange table_button'>Update</button>");
				   echo " ";
				   echo anchor("koorpel/deletetindakan/".$row['id']." ","<button class='button red table_button'>Delete</button>");
				   echo '</td>';
				   echo '</tr>';
				}
				echo '</div>';
            }

          	else echo '<h2>Tidak ada tindakan dengan nama seperti itu</h2>';	
	        ?>
	    </table>
	    
	
	</div>
	</center>