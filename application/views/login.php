	<div id='formbox'>
		<center>
		<?php echo form_open("site/login"); ?>
		<div class='form-1'>
			<p class="field">
		        <input type="text" name="username" placeholder="Username">
		        <i class="icon-user icon-large">
		        	<img width="25px" height="25px" src="<?php echo base_url(); ?>images/user-icon.png">
		        </i>
		    </p>
		        <p class="field">
		        <input type="password" name="password" placeholder="Password">
		        <i class="icon-lock icon-large">
		        	<img width="25px" height="25px" src="<?php echo base_url(); ?>images/lock-icon.png">
		        </i>
		    </p>        
		    <p class="submit">
		        <button type="submit" name="submit">
		        	<i class="icon-arrow-right icon-large">
		        		<img width="65px" height="65px" src="<?php echo base_url(); ?>images/arrow_only.png">
		        	</i>
		        </button>
		    </p>
		 </div>
		<?php echo form_close(); ?>
		<?php echo validation_errors(); ?>
		<?php echo form_error('Username'); ?>
		<?php echo form_error('Password'); ?>
		</center>
	</div>