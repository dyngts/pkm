<div class="container">


<h2>GENERATE LAPORAN</h2>

<?php
$kategori = preg_replace('/^\s+|\n|\r|\s+$/m', '', form_dropdown('kategori', $kategori, '', 'id="kategori"'));
?>

<form class="form" id="laporan_form" method = "post" onsubmit="return cek_inputs()" action = "<?php echo base_url('index.php/laporan_controller/proses_laporan'); ?>">
	<label>Jenis Laporan</label>
	<?php echo form_dropdown('jenis_laporan', $jenis_laporan, '', 'id="jenis_laporan"'); ?>
	<div class="isi">
		
	</div>
	<label>Bulan</label>
	<?php echo form_dropdown('month', $month, '', 'id="month"'); ?>

	<label>Tahun</label>
	<?php echo form_dropdown('year', $year, '', 'id="year"'); ?>

    <br/>
    <input name="submit" type="submit" value="Buat Laporan" class="button green" />
</form>



<script type="text/javascript">
	function cek_inputs() {

		var jenis_laporan = document.getElementById('jenis_laporan');
		var month = document.getElementById('month');
		var year = document.getElementById('year');
		var kategori = document.getElementById('kategori');



		if(jenis_laporan.value == '') {
			alert('Jenis laporan harus dipilih');
			return false;
		}

		if(kategori != null) {
			if(kategori.value == '' && jenis_laporan.value == 'laporan_stok_obat') {
				alert('Kategori obat harus dipilih');
				return false;
			}
		}
		

		if(month.value == '') {
			alert('Bulan harus dipilih');
			return false;
		}

		if(year.value == '') {
			alert('Tahun harus dipilih');
			return false;
		}

	}
</script>

<script type="text/javascript">
	$(function() {
		$("#jenis_laporan").change(function(){
			var jenis_laporan = document.getElementById("jenis_laporan").value;
			if(jenis_laporan == "laporan_stok_obat") {
				$('.isi').html('<id id="form_kat"><label>Kategori Obat</label><?php echo $kategori; ?></id>');
			}
			else {
				$('.isi').html('');
			}
		});
	});

</script>

