<center>
	<h2>Laporan Tindakan Pasien Poli Gigi PKM UI Bulan <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>
	<br>
	<h3>Daftar Tindakan yang diberikan :</h3>
	<br>
	                <table class='tabel_laporan'>
	                    <tr>
	                        <th>
	                            No
	                        </th>
	                        <th >
	                            Jenis Tindakan
	                        </th>
	                        <th>
	                            Mahasiswa
	                        </th>
	                        <th>
	                            Dosen/Karyawan
	                        </th>
	                        <th>
	                        	Tamu
	                        </th>
							<th>
	                        	Jumlah
	                        </th>
	                    </tr>
	                    	                    
	                    <?php 	
	                    	$i = 1;
							if ($laporanTindakanGigi != null){
								foreach ($laporanTindakanGigi as $row) {
									echo "<tr>";
									echo "<td>"."$i"."</td>";	                    		
									echo "<td>".$row['nama']."</td>";	                    		
									$jmlmahasiswa = 0;
									$jmlkaryawan = 0;
									$jmltamu = 0;
									$total = 0;
									$query2 = $this->perawatmod->getMahasiswa($bulan, $tahun);
									$query3 = $this->perawatmod->getKaryawan($bulan, $tahun);
									$query4 = $this->perawatmod->getTamu($bulan, $tahun);
									$array2 = $query2->result_array();
									$array3 = $query3->result_array();
									$array4 = $query4->result_array();

									foreach ($array2 as $data_mahasiswa) {
										if($row['id_tindakan'] == $data_mahasiswa['id_tindakan']){
												$jmlmahasiswa = $data_mahasiswa['mahasiswa'];                    			
										}
									}

									foreach ($array3 as $data_karyawan) {
										if($row['id_tindakan'] == $data_karyawan['id_tindakan']){
												$jmlkaryawan = $data_karyawan['karyawan'];	                    				
										}	
									}
									
									foreach ($array4 as $data_tamu) {
										if($row['id_tindakan'] == $data_tamu['id_tindakan']){
												$jmltamu = $data_tamu['tamu'];	                    				
										}	
									}

									$total = $jmlmahasiswa + $jmlkaryawan + $jmltamu;
									echo "<td>".$jmlmahasiswa."</td>";
									echo "<td>".$jmlkaryawan."</td>";
									echo "<td>".$jmltamu."</td>";
									echo "<td>".$total."</td>";
									echo "</tr>";
									$i++;
								}
								echo "</table>";
							}
							
							else {
								echo "</table>";
								echo "<h2>Tidak ada data pada periode ini</h2>";
							}
						?>
	<br><br>
	<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/perawat/bulanan_tindakan_poli_gigi_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/tindakan_poligigi_pdf'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/print_poligigi'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
</center>