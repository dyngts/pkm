<center>
	<h2>Laporan Pola Penyakit PKM UI Bulan <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>
	<br>
	<h3>Daftar Penyakit yang ditangani :</h3>
	<br>
	                <table class='tabel_laporan'>
	                    <tr>
	                        <th>
	                            No
	                        </th>
	                        <th>
	                            Jenis Penyakit
	                        </th>
	                        <th>
	                            Jumlah Kasus Baru
	                        </th>
	                        <th>
	                            Jumlah Kasus Lama
	                        </th>
	                        <th>
	                        	Jumlah Total
	                        </th>
	                    </tr>
	                    	                    
	                    <?php 	
								$i = 1; 
							if ($laporanPolaPenyakit != null){
								foreach ($laporanPolaPenyakit as $row) {
									if($row['jenis_penyakit'] != "Sehat"){
										echo "<tr>";
										echo "<td>"."$i"."</td>";	                    		
										echo "<td>".$row['jenis_penyakit']."</td>";	                    		
										$jmlkasusbaru = 0;
										$jmlkasuslama = 0;
										$total = 0;
										$query2 = $this->perawatmod->getKasusBaru($row['kode_diagnosis'], $bulan, $tahun);
										$query3 = $this->perawatmod->getKasusLama($row['kode_diagnosis'], $bulan, $tahun);
										$array2 = $query2->result_array();
										$array3 = $query3->result_array();

										foreach ($array2 as $data_baru) {
											if($row['kode_diagnosis'] == $data_baru['kode_diagnosis']){
													$jmlkasusbaru = $data_baru['kasus_baru'];                    			
											}
										}

										foreach ($array3 as $data_lama) {
											if($row['kode_diagnosis'] == $data_lama['kode_diagnosis']){
													$jmlkasuslama = $data_lama['kasus_lama'];	                    				
											}	
										}

										$total = $jmlkasusbaru + $jmlkasuslama;
										echo "<td>".$jmlkasusbaru."</td>";
										echo "<td>".$jmlkasuslama."</td>";
										echo "<td>".$total."</td>";
										echo "</tr>";
										$i++;
									}
								}
							echo "</table>";
							}
							
							else {
								echo "</table>";
								echo "<h2>Tidak ada data pada periode ini</h2>";
							}
	                    ?>             	               	
	                </table>
	<br>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/perawat/bulanan_pola_penyakit_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/pola_penyakit_pdf'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/print_polapenyakit'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
</center>
