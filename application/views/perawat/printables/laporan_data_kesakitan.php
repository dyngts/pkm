<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
	<center>
	<br>
	<h4>Laporan Bulanan Data Kesakitan PKM UI Bulan <?php echo $bulan; ?></h4>
	<div id ='laporansakit'>
	
			<div class="tabel_print" >
	                <table align = "center">
	                    <tr>
	                        <td rowspan = "3">NO</td>
	                        <td rowspan = "3">KODE ICD 10</td>
	                        <td rowspan = "3">Jenis Penyakit</td>
	                        <td colspan = "16">JUMLAH KASUS BARU MENURUT GOLONGAN UMUR</td>
	                        <td colspan = "3" rowspan = "2">KASUS BARU</td>
	                        <td colspan = "3" rowspan = "2">KASUS LAMA</td>
	                        <td rowspan = "3">TOTAL KASUS</td>

	                    <tr id = "umur_gender">
	                        <td colspan = "2">0-4 th</td>
	                        <td colspan = "2">5-9 th</td>
	                        <td colspan = "2">10-14 th</td>
	                        <td colspan = "2">15-19 th</td>
	                        <td colspan = "2">20-44 th</td>
	                        <td colspan = "2">45-54 th</td>
	                        <td colspan = "2">55-59 th</td>
	                        <td colspan = "2">60 th keatas</td>
	                    </tr>

	                    <tr id ="umur_gender">
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>
	                        <td>L</td>
	                        <td>P</td>

	                        <td>L</td>
	                        <td>P</td>
	                        <td>JML</td>

	                        <td>L</td>
	                        <td>P</td>
	                        <td>JML</td>
	                    </tr>               
	                   </tr>
	                   
	                   <?php 
	                   		if($laporanDataKesakitan == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    	}

	                   		$i = 1;
	                   		foreach ($laporanDataKesakitan as $row){
	                   			echo "<tr>";
	                   			echo "<td>".$i."</td>";
	                   			echo "<td>".$row['kode_diagnosis']."</td>";
	                   			echo "<td>".$row['jenis_penyakit']."</td>";

	                   			$query = $this->perawatmod->getDataPasienKasusBaru($row['kode_diagnosis'], $bulan, $tahun);
	                   			$array = $query->result_array();
	                   			
	                    		$perempuanGol1 = 0;
	                    		$perempuanGol2 = 0;
	                    		$perempuanGol3 = 0;
	                    		$perempuanGol4 = 0;
	                    		$perempuanGol5 = 0;
	                    		$perempuanGol6 = 0;
	                    		$perempuanGol7 = 0;
	                    		$perempuanGol8 = 0;
	                    		$lakiGol1 = 0;
	                    		$lakiGol2 = 0;
	                    		$lakiGol3 = 0;
	                    		$lakiGol4 = 0;
	                    		$lakiGol5 = 0;
	                    		$lakiGol6 = 0;
	                    		$lakiGol7 = 0;
	                    		$lakiGol8 = 0;

	                    		$query2 = $this->perawatmod->getPasienKasusBaruP($row['kode_diagnosis'], $bulan, $tahun);
	                   			$query3 = $this->perawatmod->getPasienKasusBaruL($row['kode_diagnosis'], $bulan, $tahun);
	                   			$query4 = $this->perawatmod->getPasienKasusLamaP($row['kode_diagnosis'], $bulan, $tahun);
	                   			$query5 = $this->perawatmod->getPasienKasusLamaL($row['kode_diagnosis'], $bulan, $tahun);
	                   			$array2 = $query2->result_array();
	                    		$array3 = $query3->result_array();
	                    		$array4 = $query4->result_array();
	                    		$array5 = $query5->result_array();

	                    		$baruPerempuan = 0;
	                    		$baruLaki = 0;
	                    		$lamaPerempuan = 0;
	                    		$lamaLaki = 0;
	                   			$totalBaru = 0;
	                   			$totalLama = 0;
	                   			$totalKasus = 0;

	                   			foreach ($array as $dota) {
	                   				
	                   				list($year, $month, $date) = explode("-", $dota['tanggal_lahir']);
	                   				$age = date("Y") - $year;
	                   				$bulan = date("m") - $month;
	                   				$hari = date("d") - $date;

	                   				if($hari < 0 || $bulan < 0){
	                   					$age = $age - 1;
	                   				}

	                   				if ($age > 0 && $age <= 4){
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol1 = $perempuanGol1 + 1;
	                   					}
	                   					else{
	                   						$lakiGol1 = $lakiGol1 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 5 && $age <= 9) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol2 = $perempuanGol2 + 1;
	                   					}
	                   					else{
	                   						$lakiGol2 = $lakiGol2 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 10 && $age <= 14) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol3 = $perempuanGol3 + 1;
	                   					}
	                   					else{
	                   						$lakiGol3 = $lakiGol3 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 15 && $age <= 19) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol4 = $perempuanGol4 + 1;
	                   					}
	                   					else{
	                   						$lakiGol4 = $lakiGol4 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 20 && $age <= 44) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol5 = $perempuanGol5 + 1;
	                   					}
	                   					else{
	                   						$lakiGol5 = $lakiGol5 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 45 && $age <= 54) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol6 = $perempuanGol6 + 1;
	                   					}
	                   					else{
	                   						$lakiGol6 = $lakiGol6 + 1;
	                   					}
	                   				}
	                   				elseif ($age >= 55 && $age <= 59) {
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol7 = $perempuanGol7 + 1;
	                   					}
	                   					else{
	                   						$lakiGol7 = $lakiGol7 + 1;
	                   					}
	                   				}
	                   				else{
	                   					if ($dota['jenis_kelamin'] == 'Perempuan'){
	                   						$perempuanGol8 = $perempuanGol8 + 1;
	                   					}
	                   					else{
	                   						$lakiGol8 = $lakiGol8 + 1;
	                   					}
	                   				}	
	                   			}
                                     			                 			
	                   			foreach ($array2 as $data2) {
	                   				if($row['kode_diagnosis'] = $data2['kode_diagnosis']){
	                   					$baruPerempuan = $data2['perempuan'];
	                   				}
	                   			}

	                   			foreach ($array3 as $data3) {
	                   				if($row['kode_diagnosis'] = $data3['kode_diagnosis']){
	                   					$baruLaki = $data3['laki_laki'];
	                   				}
	                   			}

	                   			foreach ($array4 as $data4) {
	                   				if($row['kode_diagnosis'] = $data4['kode_diagnosis']){
	                   					$lamaPerempuan = $data4['perempuan'];
	                   				}
	                   			}

	                   			foreach ($array5 as $data5) {
	                   				if($row['kode_diagnosis'] = $data5['kode_diagnosis']){
	                   					$lamaLaki = $data5['laki_laki'];
	                   				}
	                   			}

	                   			$totalBaru = $baruPerempuan + $baruLaki;
	                   			$totalLama = $lamaPerempuan + $lamaLaki;
	                   			$totalKasus = $totalLama + $totalBaru;

	                   			echo "<td>".$lakiGol1."</td>";
	                   			echo "<td>".$perempuanGol1."</td>";
	                   			echo "<td>".$lakiGol2."</td>";
	                   			echo "<td>".$perempuanGol2."</td>";
	                   			echo "<td>".$lakiGol3."</td>";
	                   			echo "<td>".$perempuanGol3."</td>";
	                   			echo "<td>".$lakiGol4."</td>";
	                   			echo "<td>".$perempuanGol4."</td>";
	                   			echo "<td>".$lakiGol5."</td>";
	                   			echo "<td>".$perempuanGol5."</td>";
	                   			echo "<td>".$lakiGol6."</td>";
	                   			echo "<td>".$perempuanGol6."</td>";
	                   			echo "<td>".$lakiGol7."</td>";
	                   			echo "<td>".$perempuanGol7."</td>";
	                   			echo "<td>".$lakiGol8."</td>";
	                   			echo "<td>".$perempuanGol8."</td>";
	                   			echo "<td>".$baruLaki."</td>";
	                   			echo "<td>".$baruPerempuan."</td>";
	                   			echo "<td>".$totalBaru."</td>";
	                   			echo "<td>".$lamaLaki."</td>";
	                   			echo "<td>".$lamaPerempuan."</td>";
	                   			echo "<td>".$totalLama."</td>";
	                   			echo "<td>".$totalKasus."</td";
	                   			echo "</tr>";
	                   			$i++;
	                   		}
	                   ?>
	                                   	
	                </table>
	            </div>
	</div>
	
<div class = 'signature' id='signaturekesakitan'>
Depok,  <br>
Koordinator Administrasi dan Keuangan
<br><br><br><br><br><br>
drg. Marisa Aristiawati H, MKM
<br>NIP 198003062010122002
</div>
<br>
</div>
</center>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>