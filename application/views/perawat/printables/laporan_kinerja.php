<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
	<center>
	<h2>Laporan Data Kinerja Dokter PKM UI Bulan <?echo $bulan; echo " ".$tahun;?> </h2>
	<br>
	<br>
			<div class="tabel_print">
	                <table>
					<tr>
				        <th>
				            No
				        </th>
				        <th>
				            Nama Dokter
				        </th>
				        <th>
				            Jumlah Pasien yang ditangani
				        </th>
				    </tr>
				    <?php 	
			        	$i = 1; 
			        	foreach ($dokter as $row) {
							$getnama = $this->perawatmod->getNamaDokter($row['username_dokter']);
							$namadokter = $getnama->nama;
			        		$pasienditangani = $this->perawatmod->getKinerja($row['username_dokter'], $bulan, $tahun);
			        		echo "<tr>";
			        		echo "<td>"."$i"."</td>";	                    		
			        		echo "<td>".$namadokter."</td>";	 
			        		echo "<td>".$pasienditangani->count."</td>";	 
			        		echo "</tr>";
			        		$i++;
			        	}
				    ?>
				</table>
	            </div>
<br>
<div class = 'signature' id='signaturekesakitan'>
Depok,  <br>
Koordinator Administrasi dan Keuangan
<br><br><br><br><br><br>
drg. Marisa Aristiawati H, MKM
<br>NIP 198003062010122002
</div>
<br>
</center>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>