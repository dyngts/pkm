<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
	<center>
	<h4>Laporan Pola Penyakit PKM UI Bulan <?php echo $bulan; ?></h4>
	<br>
	<br>
			<div class="tabel_print" >
	                <table align = 'center'>
	                    <tr>
	                        <td>
	                            No
	                        </td>
	                        <td >
	                            Jenis Penyakit
	                        </td>
	                        <td>
	                            Jumlah Kasus Baru
	                        </td>
	                        <td>
	                            Jumlah Kasus Lama
	                        </td>
	                        <td>
	                        	Jumlah Total
	                        </td>
	                    </tr>
	                    	                    
	                    <?php 	
								$i = 1; 
							if ($laporanPolaPenyakit != null){
								foreach ($laporanPolaPenyakit as $row) {
									if($row['jenis_penyakit'] != "Sehat"){
										echo "<tr>";
										echo "<td>"."$i"."</td>";	                    		
										echo "<td>".$row['jenis_penyakit']."</td>";	                    		
										$jmlkasusbaru = 0;
										$jmlkasuslama = 0;
										$total = 0;
										$query2 = $this->perawatmod->getKasusBaru($row['kode_diagnosis'], $bulan, $tahun);
										$query3 = $this->perawatmod->getKasusLama($row['kode_diagnosis'], $bulan, $tahun);
										$array2 = $query2->result_array();
										$array3 = $query3->result_array();

										foreach ($array2 as $data_baru) {
											if($row['kode_diagnosis'] == $data_baru['kode_diagnosis']){
													$jmlkasusbaru = $data_baru['kasus_baru'];                    			
											}
										}

										foreach ($array3 as $data_lama) {
											if($row['kode_diagnosis'] == $data_lama['kode_diagnosis']){
													$jmlkasuslama = $data_lama['kasus_lama'];	                    				
											}	
										}

										$total = $jmlkasusbaru + $jmlkasuslama;
										echo "<td>".$jmlkasusbaru."</td>";
										echo "<td>".$jmlkasuslama."</td>";
										echo "<td>".$total."</td>";
										echo "</tr>";
										$i++;
									}
								}
							echo "</table>";
							}
							
							else {
								echo "</table>";
								echo "<h2>Tidak ada data pada periode ini</h2>";
							}
	                    ?>             	               	
	                </table>
	            </div>
<br>
<div class = 'signature' id='signaturekesakitan'>
Depok,  <br>
Koordinator Administrasi dan Keuangan
<br><br><br><br><br><br>
drg. Marisa Aristiawati H, MKM
<br>NIP 198003062010122002
</div>
<br>
</center>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>