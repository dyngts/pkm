<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
	<center>

	<h4>Laporan Tindakan Pasien Poli Gigi PKM UI Bulan <?php echo $bulan; ?></h4>
	<div id ='laporansakit'>
	<br>
	
			<div class="tabel_print" >
	                <table align = 'center'>
	                    <tr>
	                        <td>
	                            No
	                        </td>
	                        <td >
	                            Jenis Tindakan
	                        </td>
	                        <td>
	                            Mahasiswa
	                        </td>
	                        <td>
	                            Dosen/Karyawan
	                        </td>
	                        <td>
	                        	Tamu
	                        </td>
							<td>
	                        	Jumlah
	                        </td>
	                    </tr>
	                    	                    
	                    <?php 	
	                    	$i = 1;
	                    	foreach ($laporanTindakanGigi as $row) {
	                    		echo "<tr>";
	                    		echo "<td>"."$i"."</td>";	                    		
	                    		echo "<td>".$row['nama']."</td>";	                    		
	                    		$jmlmahasiswa = 0;
	                    		$jmlkaryawan = 0;
								$jmltamu = 0;
	                    		$total = 0;
	                    		$query2 = $this->perawatmod->getMahasiswa($bulan, $tahun);
	                    		$query3 = $this->perawatmod->getKaryawan($bulan, $tahun);
	                    		$query4 = $this->perawatmod->getTamu($bulan, $tahun);
								$array2 = $query2->result_array();
	                    		$array3 = $query3->result_array();
								$array4 = $query4->result_array();

	                    		foreach ($array2 as $data_mahasiswa) {
	                    			if($row['id_tindakan'] == $data_mahasiswa['id_tindakan']){
	                    					$jmlmahasiswa = $data_mahasiswa['mahasiswa'];                    			
	                    			}
	                    		}

	                    		foreach ($array3 as $data_karyawan) {
	                    			if($row['id_tindakan'] == $data_karyawan['id_tindakan']){
	                    					$jmlkaryawan = $data_karyawan['karyawan'];	                    				
	                    			}	
	                    		}
								
								foreach ($array4 as $data_tamu) {
	                    			if($row['id_tindakan'] == $data_tamu['id_tindakan']){
	                    					$jmltamu = $data_tamu['tamu'];	                    				
	                    			}	
	                    		}

	                    		$total = $jmlmahasiswa + $jmlkaryawan + $jmltamu;
	                    		echo "<td>".$jmlmahasiswa."</td>";
	                    		echo "<td>".$jmlkaryawan."</td>";
								echo "<td>".$jmltamu."</td>";
	                    		echo "<td>".$total."</td>";
	                    		echo "</tr>";
	                    		$i++;
	                    	}
	                    ?>             	               	
	                </table>
	            </div>
	</div>
	<br>

<div class = 'signature' id='signaturekesakitan'>
Depok,  <br>
Koordinator Administrasi dan Keuangan
<br><br><br><br><br><br>
drg. Marisa Aristiawati H, MKM
<br>NIP 198003062010122002
</div>
<br>
</div>
</center>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>