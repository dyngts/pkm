<center>
	<h2>Laporan Data Penyakit PKM UI Bulan <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>
	<div id ='laporansakit'>
	<br>
	
			<div >
	                <table class = "tabel_pasien">
	                    <tr>
	                        <th class="first" rowspan = "3">NO URUT</th>
	                        <th class="even-col" rowspan = "3">KODE ICD 10</th>
	                        <th rowspan = "3">JENIS PENYAKIT</th>
	                    
	                        <th class="even-col" colspan = "16">JUMLAH KASUS BARU MENURUT GOLONGAN UMUR</th>
	                        <th class="last" rowspan = "3">TOTAL KASUS</td>

	                    <tr id = "umur_gender">
	                        <th colspan = "2">0-4 th</th>
	                        <th class="even-col" colspan = "2">5-9 th</th>
	                        <th colspan = "2">10-14 th</th>
	                        <th class="even-col" colspan = "2">15-19 th</th>
	                        <th colspan = "2">20-44 th</th>
	                        <th class="even-col" colspan = "2">45-54 th</th>
	                        <th colspan = "2">55-59 th</th>
	                        <th class="even-col" colspan = "2"> >=60 th</th>
	                    </tr>

	                    <tr id ="umur_gender">
	                        <th>L</th>
	                        <th>P</th>
	                        <th class="even-col">L</th>
	                        <th class="even-col">P</th>
	                        <th>L</th>
	                        <th>P</th>
	                        <th class="even-col">L</th>
	                        <th class="even-col">P</th>
	                        <th>L</th>
	                        <th>P</th>
	                        <th class="even-col">L</th>
	                        <th class="even-col">P</th>
	                        <th>L</th>
	                        <th>P</th>
	                        <th class="even-col">L</th>
	                        <th class="even-col">P</th>
	                    </tr>               
	                   </tr>
	                   <?php
	                   	$no = 1;


	                   	foreach ($datakesakitanstatus as $datastatus) {
	                   		$status1 = 0;
	                   		$fstatus1 = 0;
	                   		$status2 = 0;
	                   		$fstatus2 = 0;
	                   		$totalstatus1 = 0;
	                   		$totalstatus2 = 0;

	                   		if($datastatus['status_diagnosa'] = 'baru') {
	                   			if($datastatus['jenis_kelamin'] == 'P'){
	                   				$fstatus1 = $datastatus['jumlah'];
	                   			}
	                   			else if ($datastatus['jenis_kelamin'] == 'L'){ 
	                   				$status1 = $datastatus['jumlah'];
	                   			}
	                   		}

	                   		elseif($datastatus['status_diagnosa'] = 'lama') {
	                   			if($datastatus['jenis_kelamin'] == 'P'){
	                   				$fstatus2 = $datastatus['jumlah'];
	                   			}
	                   			else if ($datastatus['jenis_kelamin'] == 'L'){ 
	                   				$status2 = $datastatus['jumlah'];
	                   			}
	                   		}
	                   	}
	                   	
	                   	foreach ($datakesakitan as $data) {
                   		 	$count1 = 0;
		                   	$count2  = 0;
		                   	$count3  = 0;
		                   	$count4  = 0;
		                   	$count5  = 0;
		                   	$count6  = 0;
		                   	$count7  = 0;
		                   	$count8 = 0;
		                   	$fcount1 = 0;
		                   	$fcount2  = 0;
		                   	$fcount3  = 0;
		                   	$fcount4  = 0;
		                   	$fcount5  = 0;
		                   	$fcount6  = 0;
		                   	$fcount7  = 0;
		                   	$fcount8 = 0;
	                   		echo "<tr>";
	                   		echo "<td>".$no."</td>";
	                   		echo "<td>".$data['kode_diagnosis']."</td>";
	                   		echo "<td>".$data['jenis_penyakit']."</td>";
	                   		
	                   		if($data['kategori'] == '0-4'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount1 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count1 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '5-9'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount2 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count2 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '10-14'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount3 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count3 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '15-19'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount4 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count4 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '20-44'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount5 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count5 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '45-54'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount6 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count6 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '55-59'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$fcount7 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count7 = $data['jumlah'];
	                   			}
	                   		}

	                   		elseif($data['kategori'] == '>60'){
	                   			if($data['jenis_kelamin'] == 'P'){
	                   				$count8 = $data['jumlah'];
	                   			}
	                   			else if ($data['jenis_kelamin'] == 'L'){ 
	                   				$count8 = $data['jumlah'];
	                   			}
	                   		}
	                   		$total = $count1+$count2+$count3+$count4+$count5+$count6+$count7+$count8+$fcount1+$fcount2+$fcount3+$fcount4+$fcount5+$fcount6+$fcount7+$fcount8;
	                   		echo "<td>".$count1."</td>";
	                   		echo "<td>".$fcount1."</td>";
	                   		echo "<td>".$count2."</td>";
	                   		echo "<td>".$fcount2."</td>";
	                   		echo "<td>".$count3."</td>";
	                   		echo "<td>".$fcount3."</td>";
	                   		echo "<td>".$count4."</td>";
	                   		echo "<td>".$fcount4."</td>";
	                   		echo "<td>".$count5."</td>";
	                   		echo "<td>".$fcount5."</td>";
	                   		echo "<td>".$count6."</td>";
	                   		echo "<td>".$fcount6."</td>";
	                   		echo "<td>".$count7."</td>";
	                   		echo "<td>".$fcount7."</td>";
	                   		echo "<td>".$count8."</td>";
	                   		echo "<td>".$fcount8."</td>";
	                   		echo "<td>".$total."</td>";
	                   		$no++;
	                   		echo "</tr>";
	                   	}

	                   ?>           	
	                </table>
	                <?php if($datakesakitanstatus == null) {
	                    	echo "<h1>Tidak ada data pada bulan ini</h1>";
	                    }?>
	            </div>
	</div>
	<br>
	<div class='gridperawat'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/perawat/bulanan_data_kesakitan_xls'; ?>';">Unduh dalam format excel</button>		
	</div>
</center>
<br>