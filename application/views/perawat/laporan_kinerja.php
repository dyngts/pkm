<center>
	<h2>Laporan Data Kinerja Dokter PKM UI Bulan <?echo $bulan; echo " ".$tahun;?> </h2>
	<table class='tabel_laporan'>
		<tr>
	        <th>
	            No
	        </th>
	        <th>
	            Nama Dokter
	        </th>
	        <th>
	            Jumlah Pasien yang ditangani
	        </th>
	    </tr>
	    <?php 	
        	$i = 1; 
        	foreach ($dokter as $row) {
				$getnama = $this->perawatmod->getNamaDokter($row['username_dokter']);
				$namadokter = $getnama->nama;
        		$pasienditangani = $this->perawatmod->getKinerja($row['username_dokter'], $bulan, $tahun);
        		echo "<tr>";
        		echo "<td>"."$i"."</td>";	                    		
        		echo "<td>".$namadokter."</td>";	 
        		echo "<td>".$pasienditangani->count."</td>";	 
        		echo "</tr>";
        		$i++;
        	}
	    ?>
	</table>
	<br><br>
	<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/perawat/laporan_kinerja_xls'; ?>';">Unduh dalam format excel</button>		
	&nbsp;&nbsp;
	<!--
	<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/laporan_kinerja_pdf'; ?>'" class='button red'>Unduh sebagai pdf</button>
	&nbsp;&nbsp;
	<button onClick="location.href='<?php echo base_url() . 'index.php/perawat/print_laporankinerja'; ?>'" class='button orange'>Lihat Detail Print</button>
	-->
</center>	