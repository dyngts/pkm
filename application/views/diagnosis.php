<div id='menudiagnosis'>
	<div id="searchdiv">
		<?php echo form_open("koorpel/searchdiagnosis"); ?>
		<p><input type='text' name='searchquery' placeholder='Cari Diagnosis' id='search_box' class='input_form'/>
		<input type='submit' value='Cari Diagnosis' class='button red'/>
		<a href='<?php echo base_url();?>simpel/tindakan'><button class='button orange'>Kembali</button></a>
		<?php echo form_close(); ?>
	</div>
	<div id="tambahtindakan">
		<?php echo form_open("koorpel/addDiagnosis"); ?>
		<input type='submit' value='Tambah Diagnosis Baru' class='button green'/>
		<?php echo form_close(); ?>
	</div>	

<br>
	<center>
	<div id = 'tabel_tindakan'>
	<div class="tabel_pasien" id='tabel_tindakan'>
		<div id="kurung"><h2 class="text-left">Daftar Diagnosis yang ada: </h2></div>
	    <table>
	        <tr>
	            <th class="first">
	                Nama Diagnosis
	            </th>
	            <th class="even-col">
	                Keterangan
	            </th>
	            <th class="first">
	                Tindakan
	            </th>
	        </tr>
	        <?php
                foreach ($query as $row) {
				   echo '<tr>';
				   echo '<td class="text-left">';
				   echo $row['jenis_penyakit'];
				   echo '</td>';
				   echo '<td class="text-left">';
				   echo $row['keterangan'];
				   echo '</td>';
				   echo '<td style="width:120px;">'; 
				   echo anchor("koorpel/updatediagnosis/".$row['kode']." ","<button class='button orange table_button'>Update</button>");
				   echo " ";
				   echo anchor("koorpel/deletediagnosis/".$row['kode']." ","<button class='button red table_button'>Delete</button>");
				   echo '</td>';
				   echo '</tr>';
				}
	        ?>
	    </table>	
	</div>
	</div>
	<br>
	<div class="halaman">Halaman : <?php echo $halaman;?></div>
	</center>
</div>