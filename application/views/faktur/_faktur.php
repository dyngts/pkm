<div class="container">
            <!--Body content-->
            <div class="page-header" style="margin: 150px 5% 0 0">
                <h1>Daftar Faktur</h1>
            </div>
            <div id="tableForm" style="margin: 20px 5% 0 0">
		  <?php echo $error;?>
		  <?php echo validation_errors(); ?>
                <?php echo form_open_multipart('faktur_controller/do_upload', array('class' => 'form-horizontal', 'name' => 'buat', 'id' => 'buat')); ?>

                <div class="control-group">
                        <label class="control-label" for="inputPassword" style="float: left; margin-right: 15px">
                            Nama Supplier <b style="color:red; font-size: 20px">*</b>
                        </label>
                        <div class="controls">
                            <?php echo form_dropdown('id_supplier', $daftar_supplier) ?>
                        </div>
                    </div>
                <?php if($jumlah_baris != 0) { ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <td style="text-align: center">No</td>
                            <td style="text-align: center">Tanggal Upload</td>
                            <td style="text-align: center">Nama Faktur</td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            foreach ($fakturs->result() as $faktur) { 
                        ?>
                        <tr>
                            <td style="text-align: center"><?php echo ++$mulai; ?></td>
                            <td style="text-align: center"><?php echo $faktur->tgl_upload ?></td>
                            <td style="text-align: center">Faktur <?php echo $faktur->nama_faktur; ?></td>
                            <td style="text-align: center"><a href="<?php echo site_url('index.php/faktur_controller/hapus/'.$faktur->id_faktur); ?>" style="color: #51aded" onClick="return confirm( 'Yakin ingin menghapus Faktur ini?' )"><i class="icon-remove-circle"></i> Hapus</a></td>
                            <td style="text-align: center"><a href="<?php echo site_url('index.php/faktur_controller/preview/'.$faktur->id_faktur); ?>" style="color: #51aded"><i class="icon-eye-open"></i> Preview</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php echo $pagination_links; ?>
                </table>
                <?php } else { ?>
                <h3>Tidak ada data faktur yang tersedia</h3>
                <?php } ?>
                

                <div class="form-actions" style="background-color: #D2D1D1">
                    <div class="control-group" style="">
                        	<label class="text-center control-label" for="inputEmail" style=""><b>Unggah Faktur (.jpg,.png,.gif,</br>max.size 200kb)</b></label>

                        <div class="controls" id="upload" style="">
                            <input class="span2" type="file" name="userfile" type="text" style="width:100%">
                        </div>
                        <input type="submit" class="btn btn-primary" style="margin-left: 10%" title="upload" value="Upload" name="upload" /> 
                    </div>
                </div>

              <?php echo form_close(); ?>
            </div>
        </div>
        </div>
                </div>