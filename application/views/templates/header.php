<?php 


?>
<!DOCTYPE html>
<html>
  <head>
    <link href="<?php echo base_url(); ?>style/homepage3.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/form3.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/eror.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/dropdown.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/table3.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/bar.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/select2.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('js/jquery-1.9.1.min.js');?>" ></script>
    <script src="<?php echo base_url('js/select2.min.js');?>" ></script>
    <title><?php echo $title; ?></title>

  <?php 
    $livequeuestatus = FALSE && ! $this->authentication->is_loggedin();
   ?>
  
<?php if($livequeuestatus) echo"
  <script type=\"text/javascript\">
    function loadXMLDoc()
    {
      var xmlhttp;
      if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      }
      else
      {// code for IE6, IE5
        xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
      }
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
          document.getElementById(\"myDiv\").innerHTML=xmlhttp.responseText;
        }
      }
      xmlhttp.open(\"GET\",\"".site_url('site/infonotifikasi')."\",true);
      xmlhttp.send();
    }
    </script>";

  ?>

  </head>
            <?php
                //$this->load->model('antrian_model');
                if ($this->session->userdata('flush')) 
                {
                  $jmllast = $this->antrian_model->get_jml_antrian();
                  $jmll = $jmllast[0];
                  $data = array(
                    'clicked' => true,
                    'jml_last' => $jmll,
                    );
                }
                else 
                {
                  $data = array(
                    'clicked' => true,
                    );
                }
                
              $this->session->set_userdata($data);
            ?>
  <body>
    <!-- BODY -->
    <div class="body">

      <!-- HEADER -->
      <div class="header">
        <div class="header_bigbox">
          <div class="header_box">
            <div class="logonheader">
              <div class="logo-bg">
                <div class="logoui"></div>
              </div>
              <a href="<?php echo site_url(); ?>">
              <div class="header_pkm">
                <!-- 
                <h1 class="pkm">Pusat Kesehatan Mahasiswa <em class="ui">Universitas Indonesia</em></h1>
                <h2 class="sitrasi">Sistem Informasi PKM UI</h2>
                -->
                <h2 style="padding-top:10px;margin:0;"><span class="sitrasi" style="color:#AAA;font-size:14px;">Sistem Informasi</span></h2><h1 class="pkm" style="padding-top:4px;">Pusat Kesehatan Mahasiswa</h1><em class="ui" style="padding-top:10px;">Universitas Indonesia</em>
              </div>
              </a>
              <div class="searchlogin">
                <?php if ( ! $this->authentication->is_loggedin()) { ?>
                  <div class="user-info loggedout">
                    <a style="float:right;<?php if($title == "Login") echo 'visibility:hidden' ?>" href="<?php echo site_url('auth'); ?>">
                      <button class="button red search_button">Log In</button>
                    </a>
                  </div>
                <?php 
                  }
                  else {
                ?>
                <div class="user-detail">
                  
                      <a href="<?php echo site_url("akun/detail"); ?>">
                      <span class="bold" style="color:#FFF;text-decoration: underline;width:150px;height:30px;overflow: hidden;">
                        <?php echo character_limiter($this->authentication->get_my_name(), 20); ?>
                      </span>
                      </a>
                  <br/>
                  <span style="color:#AAA"><?php echo $this->authentication->get_my_role_name(); ?></span>
                </div>
              <div class="user-info loggedin">
                <a style="float:right; margin-right:0px;" href="<?php echo site_url('auth/logout'); ?>">
                  <button class="button red search_button">Log Out</button>
                </a>
              </div>
              <?php } ?>
              <div class="search-box">
                <!-- SEARCH -->
                <form action="http://beta.pkm.ui.ac.id/pkm/index.php/site/execute_search" method="post" class="search">
                  <input type="text" name="search" id="search_box" placeholder="<?php if($this->authentication->is_pelayanan()) echo "cari pegawai PKM..." ; else if($this->authentication->is_loket() || $this->authentication->is_perawat()) echo "cari pasien..."; else echo "cari artikel...";?>" class="input_form" value="">
                  <input type="submit" value="cari" class="button yellow search_button">
                </form>
              </div>
              </div>
              <div class="greenline">
                <!-- 
                <div class="greenline-dark"></div>
                <div class="greenline-light"></div>
                 -->
              </div>
            </div>
          </div>
        </div>
        <?php if (FALSE && ENABLE_SITE_MENU) { ?>
        <div class="header_box">
          <div class="header_menu_main">
            <?php if (ENABLE_SITE_MENU) { ?>
            <div class="site-menu">
              <ul class="menu">
                <!-- <li id="sitrasi" class="menu_button"><a href="<?php echo base_url(); ?>index.php/"><h3>SITRASI PKM</h3></a></li> -->
                <li class="menu_button"><a href="<?php echo base_url(); ?>index.php/"><h3>Beranda</h3></a></li>
                <li class="menu_button"><a href="<?php echo base_url(); ?>index.php/site/index"><h3>Berita</h3></a></li>
                <?php if (ENABLE_STATISTIK_KUNJUNGAN) {?>
                <li class="menu_button"><a href="<?php echo base_url(); ?>index.php/statistik"><h3>Statistik</h3></a>
                  <!-- DROPDOWN
                  <ul id="dropper" class="drop">
                  <div id="dropline1">
                    <a href=""><li class="submenu" id="sem"><h3 class="submenuh">Beranda</h3></a></li>
                    <a href=""><li class="submenu" id="roa"><h3 class="submenuh">Berita</h3></a></li>
                    <a href=""><li class="submenu" id="exp"><h3 class="submenuh">Statistik</h3></a></li>
                    <a href=""><li class="submenu" id="ent"><h3 class="submenuh">Jadwal Praktik Dokter</h3></a></li>
                  </div>
                  </ul>
                  -->       
                </li>
                <?php }; ?>
                <li class="menu_button"><a href="<?php echo base_url(); ?>index.php/site/schedule"><h3>Jadwal Praktik Dokter</h3></a>
                  <!-- DROPDOWN
                  <ul id="dropper" class="drop">
                  <div id="dropline2">
                    <a href=""><li class="submenu"><h3 class="submenuh">Web Development</h3></a></li>
                    <a href=""><li class="submenu"><h3 class="submenuh">Robotics</h3></a></li>
                    <a href=""><li class="submenu"><h3 class="submenuh">Mobile Games</h3></a></li>
                    <a href=""><li class="submenu"><h3 class="submenuh">Programming</h3></a></li>
                  </div>
                  </ul> 
                  -->     
                </li>
                <li id="contact_us" class="menu_button"><a href="<?php echo base_url(); ?>index.php/site/about"><h3>Tentang PKM</h3></a></li>
                <?php if (ENABLE_FAQ) {?>
                <li id="faq" class="menu_button"><a href="<?php echo base_url(); ?>index.php/site/faq"><h3>FAQ</h3></a></li>
                <?php }; ?>
              </ul>
            </div>
            <div>
              <!-- SEARCH -->
              <form action="<?php echo base_url(); ?>index.php/site/execute_search" method="post" class="search">
                <input type="text" name="search" id="search_box" placeholder="<?php if($this->authentication->is_pelayanan()) echo "cari pegawai PKM..." ; else if($this->authentication->is_loket() || $this->authentication->is_perawat()) echo "cari pasien..."; else echo "cari artikel...";?>" class="input_form"/>
                <input type="submit" value="cari" class="button green search_button"/>
              </form>
            </div>
            <?php } ?>

          </div>
        </div>
        <?php } ?>


        <div class="body_box">

          <div class="row_top"  id="myDiv">
              <?php if($livequeuestatus) echo"
              <script type=\"text/javascript\">
                window.setInterval(function() {
                  loadXMLDoc()
                }, 0);
              </script>"
                ?>
          </div>