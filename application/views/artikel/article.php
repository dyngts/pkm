            <!-- FORM BEGIN -->
            <div class="container">
              <!--<form onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ?\n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');respon();"> -->
               <?php echo form_open_multipart('artikel_controller/add_article', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
      
         <?php //echo form_open('artikel_controller/add_article'); ?>

         <!-- Place TinyMCE -->
            <script type="text/javascript" src="<?php echo base_url('js/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
            <script type="text/javascript">
            tinymce.init({
                selector: "textarea"
             });
            </script>
          <style type="text/css" media="screen">
            .mce-tinymce.mce-container.mce-panel {
              margin-left : 210px;
            }
          </style>
         <fieldset>
        
                    <?php if(isset($error) && $error != NULL) {
                        echo '<div><div class="row"><div class="message message-red">'.$error.'</div></div>';
                    }?>
          
                  <div class="row">
                    <div class="label">
                      <label>Judul<strong class="required">*</strong></br><em class="english">Title</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="judul" value="<?php echo set_value('judul');?>" class="input_form" id="name_form">
                    </div>
                    <?php echo form_error('judul', '<p class="message-error">', '</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan judul artikel.</label></span>
                    </div>
                  </div>

                  <!-- <div class="row">
                    <div class="label">
                      <label>Tag<strong class="required">*</strong></br><em class="english">Tag</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="tag" value="<?php echo set_value('tag');?>" class="input_form" id="name_form">
                    </div>
                  <?php echo form_error('tag', '<p class="message-error">', '</p>'); ?>
                    <div class="note">
                      <span><label>Masukkan tag atau kata kunci.</label></span>
                    </div>
                  </div> -->

                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea name="teks" class="input_form content_form"><?php echo set_value('teks');?></textarea>
                    </div>
                  <?php echo form_error('teks', '<p class="message-error">', '</p>'); ?>
                    <div class="note">
                      <span><label>Tuliskan isi artikel.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Gambar<br/><em class="english">Image</em></label>
                    </div>
                    <div class="input">
            <p class="p_form">:</p> 
            <input type="file" name="foto" value="upload" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Upload gambar.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <?php //echo anchor("artikel_controller/add_article/",'<input name ="status" type="button" value="Simpan sebagai draft" class="button orange">');?> 
                      <input name ="status" type="submit" value="Simpan" class="button green">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>

            