            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> 
			  <!--
            <script type="text/javascript">
                function toggle(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Jadikan Draft') {
                      document.getElementById(b).value = 'Terbitkan';
                    }
                    else {
                    document.getElementById(b).value = 'Jadikan Draft';
                    }
                  }
                }
				/*
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_articlelist').deleteRow(i);
                  }
                }
                function showconfirm()
                {
                  confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                } */
            </script> -->

              <div class="row">
                <a href="<?php echo base_url(); ?>index.php/artikel_controller/article">
                  <input type="button" value="Buat Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Artikel / Informasi</h2>
                  <br>


				          <br>
				          <div class="message message-yellow">
                    <p class="p_message">Klik pada judul artikel untuk membacanya </p>
				          </div>
				          <br>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Tanggal Dibuat</th>
                        <th>Judul Artikel / Informasi</th>
                        <th class="even-col">Status</th>  
                        <th>Penulis</th>
						<th class="even-col"> Tindakan </th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <td class="last" colspan=6></td>
                      </tr>
                    </tfoot>
                    <tbody>
 						<?php 
						$rownum =$page_num;
						foreach ($row as $r) :
						$rownum++;
						?>
							<tr class="inner-table">
							<td><?php echo $rownum; ?></td>
							<td><?php echo date ('d-M-Y H:i', strtotime($r->tgl_create)); ?></td>
							<td class="text-left"><?php echo anchor("artikel_controller/readarticle/".$r->id,$r->judul); ?></td>
							<td><?php echo $r->status; ?></td>
							<td class="text-left"> <?php echo $r->nama; ?> </td>
              <?php if ($this->authentication->is_pj() || $this->authentication->is_pelayanan() || $this->authentication->is_loket()) { ?>
              <td class="text-left"> <div class="row" class="table_button_group">
                <?php echo form_open ("artikel_controller/deletearticle/".$r->id,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
                <input type="submit" value="Hapus" class="button red table_button">
                <?php echo anchor("artikel_controller/editarticle/".$r->id,'<input type="button" value="Ubah" class="button orange table_button">');?>
                <?php 
                  if ($r->status == 'draft'){
                    echo anchor("artikel_controller/ubah_status/".$r->id.'/draft','<input type="button" value="Terbitkan" class="button green table_button">');
                  }
                  if ($r->status == 'sudah terbit'){
                    echo anchor("artikel_controller/ubah_status/".$r->id.'/'.$r->status,'<input type="button" value="Jadikan Draft" class="button green table_button">');
                  }
                ?>
                <?php echo form_close(); ?>
                </div>
              </td> <?php } ?>
							</tr>
					<?php endforeach; ?>
                  </tbody>
                  </table>
                </div>
                <?php echo $pagination; ?>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>