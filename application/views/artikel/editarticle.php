            <!-- FORM BEGIN -->
            <div class="container">

         <!-- Place TinyMCE -->
            <script type="text/javascript" src="<?php echo base_url('js/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
            <script type="text/javascript">
            tinymce.init({
                selector: "textarea"
             });
            </script>
            
      <!--<form action="<?php echo base_url(); ?>index.php/site/news" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ?\n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');respon();"> -->
                <h2>EDIT ARTIKEL</h2>
        <?php 
            foreach ($row -> result() as $r) :
              $isi = $r->isi;
              $tag = $r->tag;
              $judul = $r->judul;
              $id = $r->artikel_id;
              $timestamp = $r->timestamp;
              //$status = $r->status;
            endforeach;
        echo form_open_multipart('artikel_controller/update_article/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); 
        ?>
            <?php if(isset($error) && $error != NULL) foreach ($error as $key) {
                echo '<div><div class="row"><div class="message message-red">'.$key.'</div></div>';
            }?>
              
        <style type="text/css" media="screen">
          .mce-tinymce.mce-container.mce-panel {
            margin-left : 210px;
          }
        </style>
        <fieldset>
                  <div class="row">
                    <div class="label">
                      <label>Judul<strong class="required">*</strong></br><em class="english">Title</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="judul" value="<?php echo $judul; ?>" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Masukkan judul artikel.</label></span>
                    </div>
                  </div>

                  <!-- <div class="row">
                    <div class="label">
                      <label>Tag<strong class="required">*</strong></br><em class="english">Tag</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="tag" value="<?php echo $tag; ?>" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Masukkan kata kunci.</label></span>
                    </div>
                  </div> -->

                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea name="teks" class="input_form content_form"><?php echo $isi; ?></textarea>
            </div>
                    <div class="note">
                      <span><label>Tulis isi artikel</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Gambar<br/><em class="english">Image</em></label>
                    </div>
                    <div>
            <p class="p_form">:</p> 
            <input type="file" name="foto" value="Pilih File"  id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Upload gambar.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <input type="submit" value="Simpan" class="button green">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>