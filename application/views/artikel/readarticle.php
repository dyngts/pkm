            <div class="container">
				<?php foreach ($row->result() as $r) :
					$judul = $r->judul;
					$timestamp = $r->timestamp;
					$isi = $r->isi;
					$id = $r->artikel_id;
					$foto = $r->foto;
					$status = $r->status;
					endforeach;
				?>
					<article title="artikel" id="n3"  class="complete_news">
					<h1><?php echo $judul; ?></h1>
					<time datetime="2013-04-15"><?php echo $timestamp; ?></time>
					<div class="image">
					<?php if (!empty ($foto)) { ?>
						
						<img alt="foto artikel" src="<?php echo base_url('foto/artikel/'.$foto);?> " >
					<?php } ?>
					</div>
					<p ><?php echo $isi; ?></p>
					  
					<div class="whitespace"></div>
					</article>
                      <!-- ARTICLE AND NEWS END -->
			<?php if($this->authentication->is_pelayanan() || $this->authentication->is_loket()) {?>
					<div class="row table_button_group">
                         <?php echo form_open ("artikel_controller/deletearticle/".$r->artikel_id,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
						 <?php echo anchor("artikel_controller/editarticle/".$r->artikel_id,'<input type="button" value="Ubah" class="button orange table_button">');?>
						 <input type="submit" value="Hapus" class="button red table_button">
						 <?php  if ($status=='terbit'){
							echo anchor("artikel_controller/ubah_status/".$id.'/terbit','<input type="button" value="Jadikan Draft" class="button green table_button">');
						 }
						 else{
							echo anchor("artikel_controller/ubah_status/".$id.'/draft','<input type="button" value="Terbitkan" class="button green table_button">');
							} 
						 
					echo "</div>";
					} ?>
					
            </div> 
              <!-- MAIN CONTENT 1 END -->

              <!-- WHITESPACE -->
              <div class="whitespace"></div>