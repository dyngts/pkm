  <div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Resep Obat - <?php echo $data_resep->id_resep; ?></h1>
                </div>
                <div style="margin: 20px 0 0 0">
                <?php echo validation_errors(); ?>
		  <?php  
			$temp = explode('/', $data_resep->tanggal);
			$tgl_asli = '';
				for($i = 0; $i < count($temp); ++$i) {
					if($i > 0) {
						$tgl_asli = $tgl_asli.'-'.$temp[$i];
					}
					else {
						$tgl_asli = $temp[$i];
					}
				}
		  ?> 

    <link href="<?php echo base_url(); ?>style/dy.css" rel="stylesheet" type="text/css">
          
                <?php echo form_open('resep_controller/proses_resep/'.$data_resep->id_resep, array('class' => 'form-horizontal', 'name' => 'keluarkan', 'id' => 'keluarkan')); ?>
                    <table style="width: 60%">
                        <tbody>
                            <tr>
                                <td class="detil1">ID Rekam Medis Resep</td>
				                <td class="detil2"><?php echo $data_resep->id_rekam_medis; ?></td>
                            </tr>
                            <tr>
                                <td class="detil1">Tanggal Pembuatan Resep</td>
				                <td class="detil2"><?php echo $data_resep->tanggal; ?></td>
                            </tr>
                            <tr>
                                <td class="detil1">Jam Pembuatan Resep</td>
				                <td class="detil2"><?php echo $data_resep->waktu; ?></td>
                            </tr>
                            <tr>
                                <td class="detil1">Nama Pasien</td>
				                <td class="detil2"><?php echo $data_resep->nama; ?></td>
                            </tr>
                            <tr>
                                <td class="detil1">Nama Dokter</td>
				                <td class="detil2"><?php echo $data_resep->dokter; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover">
                        <thead>
                                <th class="even-col" style="text-align: center">Nama Obat</th>
                                <th style="text-align: center">Jenis Obat</th>
                                <th class="even-col" style="text-align: center">Satuan</th>
                                <th style="text-align: center">Jumlah</th>
                                <th class="even-col" style="text-align: center">Harga</th>
                                <th style="text-align: center">Keterangan</th>
                            
                        </thead>
                        <tbody>
                            <?php 
                            $jumlah = 0;
                            $harga = 0;
                            foreach ($daftar_obats->result() as $daftar_obat) { ?>
                            <tr class="grid">
                                <td style="text-align: center"><?php echo $daftar_obat->nama_obat; ?></td>
                                <td style="text-align: center"><?php echo $daftar_obat->jenis; ?></td>
                                <td style="text-align: center"><?php echo $daftar_obat->sediaan; ?></td>
                                <td style="text-align: center"><?php echo $daftar_obat->jumlah; ?></td>
                                <td style="text-align: center"><?php echo $daftar_obat->harga_satuan; ?></td>
                                <td style="text-align: center"><?php echo $daftar_obat->komposisi; ?></td>
                            </tr>
                            <?php 
                            $jumlah = $jumlah + $daftar_obat->jumlah;
                            $harga = $harga + ($daftar_obat->jumlah*$daftar_obat->harga_satuan);
                            } 
                            ?>
                        </tbody>
                    </table>
                    <br>
                    <table>
                        <tr>
                                <td class="detil1" style="text-align: center">Total Jumlah Obat</th>
                                <td style="text-align: center"><?php echo $jumlah; ?></th>
                        </tr>
                        <tr>
                                <td class="detil1" style="text-align: center">Total Harga Resep Obat</th>
                                <td style="text-align: center">Rp <?php echo $harga ?></th>
                        </tr>
                    </table>

                    <table class="table table-condensed">
                        <thead>
                            <tr>
                        <div class="form-actions" style="background-color: #D2D1D1">
                        <input type="submit" class="button green" title="keluarkan" value="Keluarkan" id="keluarkan" name="keluarkan" onClick="return confirm( 'Transaksi ini tidak dapat dibatalkan. Anda yakin ingin mengeluarkan daftar nama obat berikut ini?' )"/>             
                        </div>
                        </tr>
                        </thead>
                    </table>

                <?php echo form_close(); ?></div>
                </div>
            </div>
        </div>