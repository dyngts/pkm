 <div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Resep Obat</h1>
                </div>
                <div style="margin: 20px 0 0 0">
                <?php if(count($reseps) > 0) { ?>
                    <table class="table table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="even-col" style="text-align: center">No</td>
                                <th style="text-align: center">Nama Resep</td>
                                <th class="even-col" style="text-align: center">Tanggal Pembuatan</td>
                                <th style="text-align: center">Jam</td>
                                <th class="even-col" style="text-align: center">Pengirim</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                           
                            foreach ($reseps as $resep) { 

                                $temp = explode('/', $resep['tanggal']);
                                $tgl_asli = '';
                                for($i = 0; $i < count($temp); ++$i) {
                                   if($i > 0) {
                                      $tgl_asli = $tgl_asli.'-'.$temp[$i];
                                  }
                                  else {
                                      $tgl_asli = $temp[$i];
                                  }
                              }
                            ?>

                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td style="text-align: center"><a style="color:blue;" href="<?php echo base_url('index.php/resep_controller/detil/'.$resep['id_resep']); ?>"><?php echo $resep['id_resep']; ?></a></td>
                                <td style="text-align: center"><?php echo $resep['tanggal']; ?></td>
                                <td style="text-align: center"><?php echo $resep['waktu']; ?></td>
                                <td style="text-align: center"><?php echo $resep['ur_dokter']; ?></td>
                            </tr>
                            <?php }  ?>
                        </tbody>
                    </table>
<?php } else {
 ?>
<h3> Tidak Ada Resep yang Baru </h3>
<?php } ?>
                    <?php echo $pagination_links; ?>

                </div>

            </div>
        </div>