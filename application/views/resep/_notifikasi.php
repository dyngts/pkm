  <div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1 style="color: red;">Perhatian</h1>
                </div>
                <div style="margin: 20px 0 0 0">

                    <table>
                        <tr>
                            <th>Nama Resep : <?php echo $id_rekam_medis.'_'.$tanggal.'_'.$jam ; ?></th>
                        </tr>
                    </table>

                    <h3>Stok di Gudang Tidak Mencukupi untuk Daftar Nama Obat Berikut Ini</h3>
                    <table class="table table-hover">
                        
                        <tr>
                            <th style="text-align: center">No</th>
                            <th style="text-align: center">Nama Obat</th>
                            <th style="text-align: center">Jumlah Obat yang Dibutuhkan</th>
                            <th style="text-align: center">Jumlah Obat yang Tersedia</th>
                        </tr>
                        <?php for($i = 0; $i < count($daftar_tidak_cukup); ++$i) { ?>
                        <tr>
                            <td style="text-align: center"><?php echo ($i+1); ?></td>
                            <td style="text-align: center"><?php echo $daftar_tidak_cukup[$i]; ?></td>
                            <td style="text-align: center"><?php echo $jumlah_dibutuhkan[$i]; ?></td>
                            <td style="text-align: center"><?php echo $jumlah_tersedia[$i]; ?></td>
                        </tr>
                        <?php } ?>
                    </table>

	              <?php  
			$temp = explode('/', $tanggal);
			$tgl_asli = '';
				for($i = 0; $i < count($temp); ++$i) {
					if($i > 0) {
						$tgl_asli = $tgl_asli.'-'.$temp[$i];
					}
					else {
						$tgl_asli = $temp[$i];
					}
				}
		  	?> 

                    <a href="<?php echo base_url().'index.php/resep_controller/detil/'.$id_rekam_medis.'/'.$tgl_asli.'/'.$jam; ?>">Kembali ke Resep Sebelumnya</a>
                </div>
                </div>
            </div>
        </div>