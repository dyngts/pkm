<div class="container">
  <!--Body content-->
  <div class="page-header" style="margin: 150px 0 0 0">
    <h1>Statistik Grafik Pemakaian Obat Umum</h1>
  </div>
  
  <div style="margin: 20px 20% 0 0">
    <div class="control-group">

      <?php

      $mulai = preg_replace('/^\s+|\n|\r|\s+$/m', '', $this->session->userdata('awal_grafik_umum') );
      $akhir = preg_replace('/^\s+|\n|\r|\s+$/m', '', $this->session->userdata('akhir_grafik_umum') );

      ?>

        <script type="text/javascript" src="<?php echo base_url('js/jsapi.js') ?>"></script>
        
        <script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChart);

          function drawChart() {

            var jsonData = $.ajax({
              type: 'post',
              url:"<?php echo base_url('index.php/statistik_controller/get_data_obat_umum_json/'.$mulai.'/'.$akhir); ?>", 
              dataType:"json",
              async: false
            }).responseText;

            var obj = jQuery.parseJSON(jsonData);
            var data = new google.visualization.arrayToDataTable(obj);

            var options = {
              title: 'Top 10 Obat yang Paling Sering Dikeluarkan',
              hAxis: {title: 'Nama Obat', titleTextStyle: {color: 'green'}},
              vAxis: {title: 'Jumlah Unit', titleTextStyle: {color: 'red'}}
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
          }
        </script>

    <link href="<?php echo base_url(); ?>style/statistik_umum.css" rel="stylesheet" type="text/css">

  <div id='tabs'> 
    <ul> 
      <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_umum'); ?>" >Lihat statistik tabel umum</a></li> 
      <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_umum_grafik'); ?>" class="active">Statistik grafik umum</a></li> 
    </ul> 
  </div>

  <br/>
  <br/>

<?php echo form_open('statistik_controller/search_grafik_umum/', array('name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_search();')); ?>
  <script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
  <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
  <br/>

  <?php

  $array_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

  if($this->session->userdata('awal_grafik_umum') && $this->session->userdata('akhir_grafik_umum')) {
    setlocale (LC_TIME, 'INDONESIA');
    $tgl_awal = date('d-m-Y',strtotime($this->session->userdata('awal_grafik_umum')));
    $tgl_akhir = date('d-m-Y',strtotime($this->session->userdata('akhir_grafik_umum')));

    $temp1 = explode('-', $tgl_awal);
    $temp2 = explode('-', $tgl_akhir);

    $tgl_awal = $temp1[0].' '.$array_bulan[$temp1[1]].' '.$temp1[2];
    $tgl_akhir = $temp2[0].' '.$array_bulan[$temp2[1]].' '.$temp2[2];
  }

  ?>

  <table style="color:white;">
    <thead>
      <?php
      if($this->session->userdata('awal_grafik_umum') && $this->session->userdata('akhir_grafik_umum')) {
        ?>
        <th style="font-size:17px; text-align:left" colspan="3">Cari statistik rentang tertentu</th>
        <th style="font-size:17px; text-align:center" colspan="3"><?php echo  $tgl_awal.' - '.$tgl_akhir; ?></th>
        <?php } else { ?>
        <th style="font-size:17px; text-align:left" colspan="6">Cari statistik rentang tertentu</th>
        <?php } ?>
      </thead>
      <thead>
        <th>Tanggal Awal</th>
        <th style="padding-left:6px">
          <p class="p_form" id="p_birthday"></p>
          <div id="place" class="input">
            <input type="text" id="demo4" value="<?php echo $this->session->userdata('awal_grafik_umum'); ?>" class="input_form_disabled" onclick="javascript:NewCssCal('demo4','yyyyMMdd')" name="tanggal_awal"  readonly="readonly">
          </div>
        </th>
        <th>Tanggal Akhir</th>
        <th style="padding-left:6px">
          <p class="p_form" id="p_birthday"></p>
          <div id="place" class="input">
            <input type="text" id="demo5" value="<?php echo $this->session->userdata('akhir_grafik_umum'); ?>" class="input_form_disabled"  name="tanggal_akhir" readonly="readonly" onclick="javascript:NewCssCal('demo5','yyyyMMdd')">
          </div>
        </th>
        <th>
          <input type="submit" class="button green" style="width:50px; text-align:center" title="cari" value="Cari" id="cari" name="cari" /> 
        </th>
        <th>
          <input type="submit" class="button green" style="width:150px; text-align:center" title="hapus" value="Hapus Pencarian" name="hapus" />
        </th>
      </thead>
    </table>
  <?php echo form_close(); ?>
  </br>
 

 <div id="chart_div" style="width: 130%; height: 70%;"></div>

</div>
</div>
</div>