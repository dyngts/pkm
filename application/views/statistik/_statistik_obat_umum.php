<div class="container">
	<!--Body content-->
	<div class="page-header" style="margin: 150px 0 0 0">
		<h1>Statistik Pemakaian Obat Umum</h1>
	</div>

    <link href="<?php echo base_url(); ?>style/statistik_umum.css" rel="stylesheet" type="text/css">

	<div id='tabs'> 
		<ul> 
			<li><a href="<?php echo base_url('index.php/statistik_controller/statistik_umum'); ?>" class="active">Statistik tabel umum</a></li> 
			<li><a href="<?php echo base_url('index.php/statistik_controller/statistik_umum_grafik'); ?>">Lihat statistik grafik umum</a></li> 
		</ul> 
	</div>

	<br/>

	<div style="margin: 20px 0 0 0">
       <div class="control-group">
	
	<?php echo form_open('statistik_controller/search_stat_umum/', array('name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_search();')); ?>
	<script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
	<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
	<br/>

	<?php
	$array_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
	?>

	<!-- <table style="color:white;">
		<thead>
			<th style="font-size:17px; text-align:left" colspan="6">Cari statistik pada rentang tertentu</th>
		</thead>
		<thead>
			<th>Tanggal Awal</th>
			<th style="padding-left:6px">
				<p class="p_form" id="p_birthday"></p>
				<div id="place" class="input">
					<input type="text" id="demo4" class="input_form_disabled" onclick="javascript:NewCssCal('demo4','yyyyMMdd')" name="tanggal_awal"  readonly="readonly">
				</div>
			</th>
			<th>Tanggal Akhir</th>
			<th style="padding-left:6px">
				<p class="p_form" id="p_birthday"></p>
				<div id="place" class="input">
					<input type="text" id="demo5" class="input_form_disabled"  name="tanggal_akhir" readonly="readonly" onclick="javascript:NewCssCal('demo5','yyyyMMdd')">
				</div>
			</th>
			<th>
				<input type="submit" class="button green" style="width:50px; text-align:center" title="cari" value="Cari" id="cari" name="cari" /> 
			</th>
		</thead>
	</table> -->
	<?php echo form_close(); ?>
	</br>
	<div class="contents">
		<div class="left-cont">
		<?php if(count($statistik_umum) > 0) { ?>
			<table class="table table-hover" style="margin-top: 30px">
				<thead>
					<tr>
						<th rowspan="2" style="text-align: center">No</th>
						<th rowspan="2" style="text-align: center">Nama Obat</th>
						<th rowspan="2" style="text-align: center">
							Total
							<br>Terpakai</th>
							<th colspan="3" style="text-align: center">Rincian Terpakai</th>

							<tr>
								<th style="text-align: center">Salemba</th>
								<th style="text-align: center">Pasien</th>
								<th style="text-align: center">Event</th>
							</tr>
						</thead>
						<tbody>
			<!-- <tr>
				<td colspan="10">Antivirus</td>
			</tr> -->
			<?php foreach($statistik_umum as $daftar) { ?>
			<tr>
				<td style="text-align: center"><?php echo ++$mulai; ?></td>
				<td><?php echo $daftar['nama_obat']; ?></td>
				<td style="text-align: center"><?php echo ($daftar['event'] + $daftar['salemba'] + $daftar['resep']); ?></td>
				<td style="text-align: center"><?php echo $daftar['salemba']; ?></td>
				<td style="text-align: center"><?php echo $daftar['resep']; ?></td>
				<td style="text-align: center"><?php echo $daftar['event']; ?></td>
				
			</tr>

			<?php } ?>

			
		</tbody>
	</table>
	<?php echo $pagination_links; ?>

	<?php } else { ?>

	<h2>Tidak ditemukan statistik umum</h2>

	<?php } ?>
</div>
<div class="right-cont">
<div id="chart_div" style="width: 130%; height: 70%;"></div>
</div>
</div>
	
</div>
</div>
</div>


<script type="text/javascript">

    function cek_search() {

        var tgl_awal = document.getElementById("demo4");
        var tgl_akhir = document.getElementById("demo5");

        if(tgl_awal.value == "" && tgl_akhir.value != "") {
            alert("Tanggal awal harus diisi");
            return false;
        }
        else if(tgl_awal.value != "" && tgl_akhir.value == "") {
            alert("Tanggal akhir harus diisi");
            return false;
        }
    }

</script>

<script type="text/javascript" src="<?php echo base_url('js/jsapi.js') ?>"></script>

