<div class="container">
  <!--Body content-->
  <div class="page-header" style="margin: 150px 0 0 0">
    <h1>Statistik Pemakaian Obat Umum</h1>
  </div>
  <div style="margin: 20px 0 0 0">
        <ul class="nav nav-tabs">
            <li>
                <a href="<?php echo site_url('index.php/statistik_controller/statistik_umum'); ?>">Lihat Statistik Tabel</a>
            </li>
            <li class="active">
                <a href="#">Statistik Grafik</a>
            </li>
        </ul>
  </div>
  <div style="margin: 20px 20% 0 0">
    <div class="control-group">

    <script type="text/javascript">
            $(function() {
                $('.datepicker').datepicker({
                    
                });
            });
        </script>

    <?php echo validation_errors(); ?> 
        <?php echo form_open('index.php/statistik_controller/cari_grafik_umum/', array('name' => 'cari', 'id' => 'cari')); ?>

             <label class="control-label" for="" style="float: left; margin-top: 7px; margin-right: 3px">Tanggal</label>
             <div class="input-append date datepicker" data-date="default" data-date-format="yyyy-mm-dd" style="margin-left: 16px; float: left">
                 <input size="16" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai" readonly style="background-color: #EAEDF1; height: 30px"> 
                 <span class="add-on" style="height: 30px"> <i class="icon-th" style="margin-top: 5px"></i>
                 </span>
             </div>
             <label class="control-label" for="" style="float: left; margin: 7px 5px 0 5%">sampai</label>

             <div class="input-append date datepicker" data-date="default" data-date-format="yyyy-mm-dd" style="margin-left: 16px; float: left">
                 <input size="16" type="text" name="tanggal_akhir" placeholder="Tanggal Akhir" readonly style="background-color: #EAEDF1; height: 30px"> 
                 <span class="add-on" style="height: 30px"> <i class="icon-th" style="margin-top: 5px"></i>
                 </span>
             </div>
            <div class="control-group">
              <div class="controls">
                <div class="input-prepend" style="margin-left:5%; margin-top: 4px">
                   <input type="submit" class="btn btn-primary" title="cari" value="Cari Statistik" id="cari" name="cari" /> 
                </div>
              </div>
            </div>
         </div>
         <?php echo form_close(); ?>
  </br>

  <h3>Tidak ditemukan statistik dalam jangka waktu tersebut</h3>

</div>
</div>
</div>