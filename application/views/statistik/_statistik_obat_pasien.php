 <div class="container">
     <!--Body content--> 
     <div class="page-header" style="margin: 150px 0 0 0">
         <h1>Statistik Pemakaian Obat oleh Pasien</h1>
     </div>
     <div style="margin: 20px 25% 0 0">
         <div class="control-group">

     </br>

     <?php if($jumlah_baris) { ?>

     <table class="table table-hover" style="margin-top: 30px; width:140%">
        <thead>
             <tr>
                 <th style="text-align: center">No</th>
                 <th style="text-align: center">Nama Pasien</th>
                 <th style="text-align: center">Detail</th>
             </tr>
        </thead>
        <tbody>
            <?php 
                foreach($daftar_pasiens as $daftar) { ?>
             <tr>
                 <td style="text-align: center"><?php echo ++$mulai; ?></td>
                 <td style="text-align: center"><?php echo $daftar->nama; ?></td>
                 <td style="text-align: center">
                     <a href="<?php echo site_url('statistik_controller/statistik_detil_pasien/'.$daftar->id); ?>" style="color: #51aded">
                         <i class="icon-info-sign"></i>
                         Detail
                     </a>
                 </td>
             </tr>
            <?php } ?>
        </tbody>
 </table>

   <?php } else { ?>
        <h2>Tidak ditemukan daftar statistik pasien</h2>
   <?php } ?>

 </div>
 </div>
 </div>