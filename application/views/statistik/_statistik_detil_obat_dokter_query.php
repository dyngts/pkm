<div class="container">
            <!--Body content-->
            <div class="page-header" style="margin: 150px 0 0 0">
                <h1>Statistik Grafik Pemakaian Obat oleh Dokter</h1>
            </div>

    <link href="<?php echo base_url(); ?>style/statistik_umum.css" rel="stylesheet" type="text/css">

            <div id='tabs'> 
                <ul> 
                <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_detil_dokter/'.$username); ?>" class="active">Statistik tabel dokter</a></li> 
                    <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_detil_dokter_grafik/'.$username); ?>">Lihat statistik grafik dokter</a></li> 
                </ul> 
            </div>

            <br/>
            
            <div style="margin: 20px 0 0 0">
                <id style="font-size:20px;">Username Dokter : <?php echo $username; ?></id>
                <br>

                <div class="control-group">
                <?php echo form_open('statistik_controller/search_stat_dokter/'.$username, array('name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_search();')); ?>
                    <script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
                    <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                    <br/>

                    <?php

                    $array_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

                    if($this->session->userdata('tanggal_awal_dokter') && $this->session->userdata('tanggal_akhir_dokter')) {
                        setlocale (LC_TIME, 'INDONESIA');
                        $tgl_awal = date('d-m-Y',strtotime($this->session->userdata('tanggal_awal_dokter')));
                        $tgl_akhir = date('d-m-Y',strtotime($this->session->userdata('tanggal_akhir_dokter')));

                        $temp1 = explode('-', $tgl_awal);
                        $temp2 = explode('-', $tgl_akhir);

                        $tgl_awal = $temp1[0].' '.$array_bulan[$temp1[1]].' '.$temp1[2];
                        $tgl_akhir = $temp2[0].' '.$array_bulan[$temp2[1]].' '.$temp2[2];
                    }

                    ?>

                    <table style="color:white;">
                        <thead>
                            <?php
                            if($this->session->userdata('tanggal_awal_dokter') && $this->session->userdata('tanggal_akhir_dokter')) {
                                ?>
                                <th style="font-size:17px; text-align:left" colspan="2">Cari statistik rentang tertentu</th>
                                <th style="font-size:17px; text-align:center" colspan="4"><?php echo  $tgl_awal.' - '.$tgl_akhir; ?></th>
                                <?php } else { ?>
                                <th style="font-size:17px; text-align:left" colspan="6">Cari statistik rentang tertentu</th>
                                <?php } ?>
                            </thead>
                            <thead>
                                <th>Tanggal Awal</th>
                                <th style="padding-left:6px">
                                    <p class="p_form" id="p_birthday"></p>
                                    <div id="place" class="input">
                                        <input type="text" id="demo4" value="<?php echo $this->session->userdata('tanggal_awal_dokter'); ?>" class="input_form_disabled" onclick="javascript:NewCssCal('demo4','yyyyMMdd')" name="tanggal_awal"  readonly="readonly">
                                    </div>
                                </th>
                                <th>Tanggal Akhir</th>
                                <th style="padding-left:6px">
                                    <p class="p_form" id="p_birthday"></p>
                                    <div id="place" class="input">
                                        <input type="text" id="demo5" value="<?php echo $this->session->userdata('tanggal_akhir_dokter'); ?>" class="input_form_disabled"  name="tanggal_akhir" readonly="readonly" onclick="javascript:NewCssCal('demo5','yyyyMMdd')">
                                    </div>
                                </th>
                                <th>
                                    <input type="submit" class="button green" style="width:50px; text-align:center" title="cari" value="Cari" id="cari" name="cari" /> 
                                </th>
                                <th>
                                    <input type="submit" class="button green" style="width:150px; text-align:center" title="hapus" value="Hapus Pencarian" name="hapus" />
                                </th>
                            </thead>
                        </table>
                        <?php echo form_close(); ?>           
                </div>
                <?php if($jumlah_baris > 0) { ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th style="text-align: center">Nama Obat</th>
                            <th style="text-align: center">Jumlah Obat yang Telah Dikeluarkan</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($daftar_obats as $daftar_obat) { ?>
                        <tr>
                            <td><?php echo $daftar_obat->nama_obat ?></td>
                            <td style="text-align: center"><?php echo $daftar_obat->total_pemakaian ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $pagination_links;
                 } else { ?>

                 <h2>Tidak ditemukan daftar obat berdasarkan dokter tertentu</h2>

                 <?php } ?>
            </div>
        </div>
        </div>



        <script type="text/javascript">

            function cek_search() {

                var tgl_awal = document.getElementById("demo4");
                var tgl_akhir = document.getElementById("demo5");

                if(tgl_awal.value == "" && tgl_akhir.value != "") {
                    alert("Tanggal awal harus diisi");
                    return false;
                }
                else if(tgl_awal.value != "" && tgl_akhir.value == "") {
                    alert("Tanggal akhir harus diisi");
                    return false;
                }
            }

        </script>