 <div class="container">
     <!--Body content--> 
     <div class="page-header" style="margin: 150px 0 0 0">
         <h1>Statistik Pemakaian Obat oleh Dokter</h1>
     </div>
     <div style="margin: 20px 25% 0 0">
         <div class="control-group">

     </br>
     <?php if($daftar_dokter->num_rows() > 0) { ?>
     <table class="table table-hover">
        <thead>
             <tr>
                 <th style="text-align: center">No</th>
                 <th style="text-align: center">Nama Dokter</th>
                 <th style="text-align: center">Detail</th>
             </tr>
        </thead>
        <tbody>
            <?php foreach ($daftar_dokter->result() as $dokter) { ?>
             <tr>
                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                 <td style="text-align: center"><?php echo $dokter->username_dokter; ?></td>
                 <td style="text-align: center">
                     <a href="<?php echo site_url('statistik_controller/statistik_detil_dokter/'.$dokter->username_dokter); ?>" style="color: #51aded">
                         <i class="icon-info-sign"></i>
                         Detail
                     </a>
                 </td>
             </tr>
            <?php } ?>
        </tbody>
 </table>
 <?php } else { ?>

<h2>Tidak ditemukan statistik dokter</h2>

 <?php } ?>

 </div>
 </div>
 </div>