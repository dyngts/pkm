
<div class="container">
  <!--Body content-->
  <div class="page-header" style="margin: 150px 0 0 0">
    <h1>Statistik Pemakaian Obat oleh Pasien</h1>
  </div>
  <div style="margin: 20px 0 0 0">
        <ul class="nav nav-tabs">
            <li>
                <a href="<?php echo site_url('index.php/statistik_controller/statistik_detil_pasien/'.$id_pasien); ?>">Lihat Statistik Tabel</a>
            </li>
            <li class="active">
                <a href="#">Statistik Grafik</a>
            </li>
        </ul>
    </div>
      <?php
    $temp = explode('-', $tgl_mulai);
    $temp2 = explode('-', $tgl_akhir);
    $tgl_asli = '';
    $tgl_akhir_asli = '';
    $i = count($temp) - 1;
    for(; $i > -1; --$i) {
        $bulan = '';
        $bulan1 = '';
        if($i < 2) {
            if($i == 1) {
                switch ($temp[$i]) {
                case '01':
                    $bulan = 'Jan';
                    break;

                case '02':
                    $bulan = 'Feb';
                    break;

                case '03':
                    $bulan = 'Mar';
                    break;

                case '04':
                    $bulan = 'Apr';
                    break;

                case '05':
                    $bulan = 'May';
                    break;

                case '06':
                    $bulan = 'Jun';
                    break;

                case '07':
                    $bulan = 'Jul';
                    break;

                case '08':
                    $bulan = 'Aug';
                    break;

                case '09':
                    $bulan = 'Sep';
                    break;

                case '10':
                    $bulan = 'Oct';
                    break; 

                case '11':
                    $bulan = 'Nov';
                    break;

                case '12':
                    $bulan = 'Dec';
                    break;               
                }
                switch ($temp2[$i]) {
                case '01':
                    $bulan1 = 'Jan';
                    break;

                case '02':
                    $bulan1 = 'Feb';
                    break;

                case '03':
                    $bulan1 = 'Mar';
                    break;

                case '04':
                    $bulan1 = 'Apr';
                    break;

                case '05':
                    $bulan1 = 'May';
                    break;

                case '06':
                    $bulan1 = 'Jun';
                    break;

                case '07':
                    $bulan1 = 'Jul';
                    break;

                case '08':
                    $bulan1 = 'Aug';
                    break;

                case '09':
                    $bulan1 = 'Sep';
                    break;

                case '10':
                    $bulan1 = 'Oct';
                    break; 

                case '11':
                    $bulan1 = 'Nov';
                    break;

                case '12':
                    $bulan1 = 'Dec';
                    break;               
                }
                $tgl_asli = $tgl_asli.'-'.$bulan;
                $tgl_akhir_asli = $tgl_akhir_asli.'-'.$bulan1;
            }
            else {
                 $tgl_asli = $tgl_asli.'-'.$temp[$i];
                 $tgl_akhir_asli = $tgl_akhir_asli.'-'.$temp2[$i];
            }
         
        }
        else {
          $tgl_asli = $temp[$i];
          $tgl_akhir_asli = $temp2[$i];
        }
    }

    ?>
  <div style="margin: 20px 20% 0 0">
    <div class="control-group">
      <label class="control-label" for="">Pemakaian : <?php echo $tgl_asli; ?> - <?php echo $tgl_akhir_asli; ?></label>
    </div>
    <p>
      Nama Pasien : 
      <?php echo $nama_pasien; ?></p>
    <br>    
    <div class="control-group">

       <script type="text/javascript">
            $(function() {
                $('.datepicker').datepicker({
                    
                });
            });
        </script>


        <?php echo validation_errors(); ?> 
        <?php echo form_open('index.php/statistik_controller/cari_grafik_pasien/'.$id_pasien, array('name' => 'cari', 'id' => 'cari')); ?>

             <label class="control-label" for="" style="float: left; margin-top: 7px; margin-right: 3px">Tanggal</label>
             <div class="input-append date datepicker" data-date="default" data-date-format="yyyy-mm-dd" style="margin-left: 16px; float: left">
                 <input size="16" type="text" name="tanggal_mulai" placeholder="Tanggal Mulai" readonly style="background-color: #EAEDF1; height: 30px"> 
                 <span class="add-on" style="height: 30px"> <i class="icon-th" style="margin-top: 5px"></i>
                 </span>
             </div>
             <label class="control-label" for="" style="float: left; margin: 7px 5px 0 5%">sampai</label>

             <div class="input-append date datepicker" data-date="default" data-date-format="yyyy-mm-dd" style="margin-left: 16px; float: left">
                 <input size="16" type="text" name="tanggal_akhir" placeholder="Tanggal Akhir" readonly style="background-color: #EAEDF1; height: 30px"> 
                 <span class="add-on" style="height: 30px"> <i class="icon-th" style="margin-top: 5px"></i>
                 </span>
             </div>
            <div class="control-group">
              <div class="controls">
                <div class="input-prepend" style="margin-left:5%; margin-top: 4px">
                   <input type="submit" class="btn btn-primary" title="cari" value="Cari Statistik" id="cari" name="cari" />
                </div>
              </div>
            </div>
         </div>
         <?php echo form_close(); ?>
  </br>

      <h3>Tidak ditemukan statistik dalam jangka waktu tersebut</h3>
</div>
</div>
</div>