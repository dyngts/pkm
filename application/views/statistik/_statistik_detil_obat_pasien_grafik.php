<div class="container">
  <!--Body content-->
  <div class="page-header" style="margin: 150px 0 0 0">
    <h1>Statistik Grafik Pemakaian Obat oleh Pasien</h1>
  </div>

    <link href="<?php echo base_url(); ?>style/statistik_umum.css" rel="stylesheet" type="text/css">

  <div id='tabs'> 
    <ul> 
     <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_detil_pasien/'.$id_pasien); ?>" >Lihat statistik tabel pasien</a></li> 
      <li><a href="<?php echo base_url('index.php/statistik_controller/statistik_detil_pasien_grafik/'.$id_pasien); ?>" class="active">Statistik grafik pasien</a></li> 
    </ul> 
  </div>

  <br/>
   
 <div style="margin: 20px 20% 0 0; font-size:20px">
      Nama Pasien : 
      <?php echo $nama_pasien; ?>
    <br>    
    <div class="control-group">

    <?php 
    $alamat = base_url('index.php/statistik_controller/get_data_obat_pasien_json/'.$id_pasien);
     ?>

        <script type="text/javascript" src="<?php echo base_url('js/jsapi.js') ?>"></script>
        
        <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

      function drawChart() {

        var jsonData = $.ajax({
           url:'<?php echo $alamat; ?>', 
           dataType:"json",
           async: false
        }).responseText;

        var obj = jQuery.parseJSON(jsonData);
        var data = new google.visualization.arrayToDataTable(obj);

        var options = {
          title: 'Top 10 Obat yang Paling Sering Dikeluarkan',
          hAxis: {title: 'Nama Obat', titleTextStyle: {color: 'green'}},
          vAxis: {title: 'Jumlah Unit', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>


        <?php echo form_open('statistik_controller/search_grafik_pasien/'.$id_pasien, array('name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_search();')); ?>
        <script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
        <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
        <br/>

        <?php
        $array_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
        ?>

        <table style="color:white;">
          <thead>
            <th style="font-size:17px; text-align:left" colspan="6">Cari statistik grafik pada rentang tertentu</th>
          </thead>
          <thead>
            <th>Tanggal Awal</th>
            <th style="padding-left:6px">
              <p class="p_form" id="p_birthday"></p>
              <div id="place" class="input">
                <input type="text" id="demo4" class="input_form_disabled" onclick="javascript:NewCssCal('demo4','yyyyMMdd')" name="tanggal_awal"  readonly="readonly">
              </div>
            </th>
            <th>Tanggal Akhir</th>
            <th style="padding-left:6px">
              <p class="p_form" id="p_birthday"></p>
              <div id="place" class="input">
                <input type="text" id="demo5" class="input_form_disabled"  name="tanggal_akhir" readonly="readonly" onclick="javascript:NewCssCal('demo5','yyyyMMdd')">
              </div>
            </th>
            <th>
              <input type="submit" class="button green" style="width:50px; text-align:center" title="cari" value="Cari" id="cari" name="cari" /> 
            </th>
          </thead>
        </table>
        <?php echo form_close(); ?>
  </br>

 <div id="chart_div" style="width: 130%; height: 70%;"></div>

</div>
</div>
</div>