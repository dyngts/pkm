<div id='menutindakan'>
<div id='header_menutindakan'>
	<div id="searchdiv">
		<?php echo form_open("koorpel/searchtindakan"); ?>
		<input type='text' name='searchquery' value= 'Cari Tindakan' onfocus="this.value=''"/>
		<input type='submit' value='Cari Tindakan' class='btn btn-primary'/>
		<?php echo form_close(); ?>
	</div>
	<div id="tambahtindakan">
		<?php echo form_open("koorpel/addtindakan"); ?>
		<br>
		<input type='submit' value='Tambah Tindakan' class='btn'/>
		<?php echo form_close(); ?>
	</div>	
</div>
<br>
	<center>
	<div id = 'tabel_tindakan'>
	<div class="tabel_pasien" id='tabel_tindakan'>
		<div id="kurung"><legend>Daftar Tindakan yang ada: </legend></div>
	    <table>
	        <thead class="odd-row inner-table">
	            <th class="first">
	                Nama Tindakan
	            </th>
	            <th class="even-col">
	                Harga Tindakan untuk Umum
	            </th>
	            <th >
	                Harga Tindakan untuk Karyawan
	            </th>
	            <th class="even-col">
	                Harga Tindakan untuk Mahasiswa
	            </th>
	            <th>
	                Tindakan
	            </th>
	        </thead>
	        <?php
                foreach ($query as $row) {
				   echo '<tr>';
				   echo '<td class="text-left">';
				   echo $row['nama'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_umum'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_karyawan'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_mahasiswa'];
				   echo '</td>';
				   echo '<td>'; 
				   echo anchor("koorpel/updatetindakan/".$row['id']." ","<button class='btn btn-info'>Update</button>");
				   echo " ";
				   echo anchor("koorpel/deletetindakan/".$row['id']." ","<button class='btn btn-danger'>Delete</button>");
				   echo '</td>';
				   echo '</tr>';
				}
	        ?>
	    </table>	
	</div>
	</div>
	<br>
	
	</center>
</div>