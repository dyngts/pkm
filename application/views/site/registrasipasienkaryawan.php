            <!-- FORM BEGIN -->
            <div class="container">


            <?php echo validation_errors();?>
            <?php if(isset($error) && $error != NULL) foreach ($error as $key) {
                echo '<div><div class="row"><div class="message message-red">'.$key.'</div></div>';
            }?>

            <?php echo form_open_multipart('register/karyawan', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              <!-- <form action="<?php //echo base_url(); ?>index.php/site/registrasisuccess" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.');"> -->
                <fieldset>
                  <h2>Formulir Registrasi Pasien PKM UI</h2>

                    <div class="row" style="height:30px">
                        <div class="label">
                            <label><strong class="required">*</strong>Wajib diisi.</label>
                        </div>
                    </div>

                    <div class="row" style="display:none">
                        <div class="label">
                            <label>Peran<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="peran" value="Karyawan" class="input_form" id="name_form" >
                        </div>
                        <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="nama" value="<?php echo set_value('nama'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>No Identitas<strong class="required">*</strong><br/><em class="english">ID number</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="no_identitas" value="<?php echo set_value('no_identitas'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan nomor identitas Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Lembaga<strong class="required">*</strong><br/><em class="english">Institution</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="lembaga" value="<?php echo set_value('lembaga'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan nama lembaga tempat Anda Bekerja di lingkungan UI.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nomor Telepon<strong class="required">*</strong><br/><em class="english">Phone Number</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="no_telp" value="<?php echo set_value('no_telp'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan Nomor Telepon Anda (hanya boleh angka). Contoh format: 08123456789.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <textarea name="alamat" class="input_form address_form"><?php echo set_value('alamat'); ?></textarea>
                        </div>
                        <div class="note">
                            <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>



                    <div class="row">
                        <div class="row">
                            <div class="label">
                                <label>Tempat Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                            </div>
                            <div class="input">
                                <p class="p_form" id="p_birthday">:</p>
                                <div id="place" class="input">
                                    <input type="text" id="birth_place_form" class="input_form" value="<?php echo set_value('tempat_lahir'); ?>" name="tempat_lahir">
                                </div>
                            </div>
                            <div class="note">
                                <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="row">
                            <div class="label">
                                <label>Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                            </div>
                            <div class="input">
                                <p class="p_form" id="p_birthday">:</p>
                                <div id="place" class="input">
                                    <input type="text" id="demo3" class="input_form" value="<?php echo set_value('demo3'); ?>" name="demo3" style="display:none">
                                    <input type="text" id="demo4" class="input_form_disabled" value="<?php echo set_value('demo4'); ?>" name="demo4" readonly="readonly">
                                    <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                                    <script src="<?php echo base_url('js/datetimepicker_css.js');?>" type="text/javascript"></script>
                                    <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo3','yyyyMMdd','','','','','past')" style="cursor:pointer"/>
                                </div>
                            </div>
                            <div class="note">
                                <span><label>Masukkan tanggal lahir dengan menekan tombol "pilih tanggal".</label></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="radio" name="jenis_kelamin" value="L" class="input_form sex_form" <?php echo set_radio('jenis_kelamin', 'L'); ?>> Laki-laki &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" name="jenis_kelamin" value="P" class="input_form sex_form" <?php echo set_radio('jenis_kelamin', 'P'); ?>> Perempuan
                        </div>
                        <div class="note">
                            <span><label>Pilih jenis kelamin Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="agama" size="1" class="dropdown_form">
                                <option value="" <?php echo set_select('agama', '', TRUE); ?>>---</option>
                                <option value="Islam" <?php echo set_select('agama', 'Islam'); ?>>Islam</option>
                                <option value="Kristen" <?php echo set_select('agama', 'Kristen'); ?>>Kristen</option>
                                <option value="Katolik" <?php echo set_select('agama', 'Katolik'); ?>>Katolik</option>
                                <option value="Hindu" <?php echo set_select('agama', 'Hindu'); ?>>Hindu</option>
                                <option value="Budha" <?php echo set_select('agama', 'Budha'); ?>>Budha</option>
                                <option value="Kong Hu Cu" <?php echo set_select('agama', 'Kong Hu Cu'); ?>>Kong Hu Cu</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih agama Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="status_pernikahan" size="1" class="dropdown_form">
                                <option value="" <?php echo set_select('status_pernikahan', ''); ?>>Pilih Status</option>
                                <option value="Belum Menikah" <?php echo set_select('status_pernikahan', 'Belum Menikah'); ?>>Belum menikah</option>
                                <option value="Sudah Menikah" <?php echo set_select('status_pernikahan', 'Sudah Menikah'); ?>>Menikah</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih status pernikahan Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="kewarganegaraan" value="<?php echo set_value('kewarganegaraan','Indonesia'); ?>" class="input_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan kewarganegaraan Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="golongan_darah" size="1" class="dropdown_form">
                                <option value="-" <?php echo set_select('golongan_darah', ''); ?>>---</option>
                                <option value="A" <?php echo set_select('golongan_darah', 'A'); ?>>A</option>
                                <option value="B" <?php echo set_select('golongan_darah', 'B'); ?>>B</option>
                                <option value="AB" <?php echo set_select('golongan_darah', 'AB'); ?>>AB</option>
                                <option value="O" <?php echo set_select('golongan_darah', 'O'); ?>>O</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Masukkan golongan darah Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Foto<br/><em class="english">Photograph</em></label>
                      </div>
                      <div class="input">
                        <input type="file" name="foto" class="input_form" id="name_form">
                      </div>
                      <div class="note">
                        <span><label>Upload foto Anda. Maksimal 1 MB, resolusi 600 x 400, format jpeg, jpg, png, gif.</label></span>
                      </div>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <div class="row" id="submit">
                            </form>
                            <?php echo form_open_multipart('site', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
                            <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
                            </form>
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
