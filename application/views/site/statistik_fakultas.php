            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
              <div class="row inlineblock">
                <div class="row">

                    <?php $bulanlist = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September", "Oktober", "November","Desember"); $ii = 0;?>

                  <table class="highchart" data-graph-container-before="1" data-graph-type="column" style="display:none">
                    <caption>Statistik Kunjungan Per Fakultas Bulan <?php echo $bulanlist[$bulan];?> <?php echo $tahun;?></caption>
                    <thead>
                        <tr>
                            <th>Bulan</th>
                            <?php foreach($data_fakultas as $df)
                            {
                              echo "<th>".$df[0]."</th>";
                            }
                            ;?>
                        </tr>
                     </thead>
                     <tbody>

                      

                      <?php 
                        echo "<tr>";
                        echo "<td>"."Fakultas"."</th>";
                        foreach($data_fakultas as $df)
                        {
                          echo "<td>".$df[1]."</th>";
                        }
                        echo "</tr>";
                      ;?>

                    </tbody>
                  </table>

                  <script type="text/javascript">
                    $(document).ready(function() {
                      $('table.highchart').highchartTable();
                    });
                  </script>

                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/fakultas");?>">
                    <div class="statslabel">
                      <label>a. Statistik jumlah data pengunjung pasien PKM berdasarkan fakultas.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                      <div class="inlineblock">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                      </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/periode");?>">
                    <div class="statslabel">
                      <label>b. Statistik jumlah data pengunjung pasien PKM berdasarkan tahun periode.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="p_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              <?php if ($this->authentication->is_pelayanan() || $this->authentication->is_loket()) { echo "
              <div class=\"row inlineblock\">
                  <form method=\"post\" action=\"".site_url("statistik/statistikkepuasan")."\">
                    <div class=\"statslabel\">
                      <label>c. Statistik kepuasan pengunjung PKM berdasarkan tahun periode.<em class=\"english\"></em></label>
                    </div>
                    <div class=\"row inlineblock\">
                      <div class=\"margin_left\">
                        <div class=\"input\">
                          <p class=\"p_form\"></p> 
                          <select name=\"k_tahun\" size=1 class=\"dropdown_form\">
                              <option value=\"\">- Pilih Tahun -</option>
                              ";?>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                              <?php echo"</select>
                        </div>
                        <div class=\"inlineblock\">
                          <input type=\"submit\" value=\"Tampilkan\" class=\"button green table_button\">
                        </div>
                        <div class=\"note\">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              ";}?>
              </div>

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
            
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>

