            <!-- BEGIN -->
            <div class="container">
              <div class="row">
                <h1>Daftar Pegawai</h1>
              </div>

              <div class="row">
                <input type="button" value="Tambah Pegawai" class="button green">
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>{}</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=4></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>119855</td>
                        <td>dr. Wawa Wawa Wawa</td>
                        <td class="last">
                          <div class="row table_button_group">
                          <input type="button" value="Ubah" class="button orange table_button">
                          <input type="button" value="Hapus" class="button red table_button">
                         </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">2</td>
                        <td>119855</td>
                        <td>dr. Wawa</td>
                        <td class="last">
                          <div class="row table_button_group">
                          <input type="button" value="Ubah" class="button orange table_button">
                          <input type="button" value="Hapus" class="button red table_button">
                         </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>