            <!-- FORM BEGIN -->
            <div class="container">
              <!--<form onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ?\n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');respon();"> -->
               <?php echo form_open('site/add_article'); ?>
			   <fieldset>
				
					
                  <div class="row">
                    <div class="label">
                      <label>Judul<strong class="required">*</strong></br><em class="english">Title</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="judul" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Masukkan judul artikel.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Tag<strong class="required">*</strong></br><em class="english">Tag</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="tag" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Masukkan tag atau kata kunci.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea name="teks" class="input_form content_form"></textarea>
                    </div>
                    <div class="note">
                      <span><label>Tuliskan isi artikel.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Gambar<br/><em class="english">Image</em></label>
                    </div>
                    <div class="input">
                      <input type="file" name="gambar" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Upload gambar.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <input name ="status" type="button" value="Simpan sebagai draft" class="button orange">
                      <?php echo anchor("site/articlelist/",'<input name ="status" type="submit" value="Terbitkan sekarang" class="button green">');?>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>