            <!-- FORM BEGIN -->
            <div class="container">

            <?php echo validation_errors();?>
            <?php echo $msg;?>
            <?php 
                if(isset($set_password_error)) {
                    if($set_password_error) 
                    echo"
                      <div class=\"row\">
                        <div class=\"message message-red\">
                          <p class=\"p_message\">Password lama tidak cocok</p>
                        </div>
                      </div>";
                }
            ;?>
            <?php echo form_open_multipart('akun/editpassword/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              
                <fieldset>
                  <h2>Edit Password Pasien PKM UI</h2>
		<?php echo "<h3>Buat password baru untuk ".$id." - ".$this->user_model->get_user_name_by_id($id)."</h3>"?>
                <?php                                        
                    if ($its_me) echo "
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Password lama<strong class=\"required\">*</strong><br/><em class=\"english\">Old password</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"password\" name=\"oldpassword\" value=\"".set_value('password')."\" class=\"input_form\" id=\"password\">
                            
                            <input type=\"button\" value=\"Lihat\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('password').type = 'text'\" onmouseup=\"document.getElementById('password').type = 'password'\">
                        
                        </div>
                        <div class=\"note\">
                            <span><label>Password lama Anda.</label></span>
                        </div>
                    </div>";                                     
                    echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Password baru<strong class=\"required\">*</strong><br/><em class=\"english\">New password</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"password\" name=\"password\" value=\"".set_value('password')."\" class=\"input_form\" id=\"newpassword\">
                            
                            <input type=\"button\" value=\"Lihat\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('newpassword').type = 'text'\" onmouseup=\"document.getElementById('newpassword').type = 'password'\">
                        
                            <input type=\"button\" value=\"Generate\" class=\"pilihtanggal\" onclick=\"document.getElementById('newpassword').value = random(6)\"\">
                        
                        </div>
                        <div class=\"note\">
                            <span><label>Password baru yang Anda Inginkan.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label><strong class=\"required\">*</strong>Wajib diisi.</label>
                        </div>
                    </div>
                    <!-- WHITESPACE -->
                    <div class=\"whitespace\"></div>
                    "
                ;?>

                    <div class="row">
                        <div class="row" id="submit">
                            </form>
                            <?php echo form_open_multipart('site', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
                            <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
                            </form>
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->

            <script type="text/javascript">
                function random(length)
                {
                    var text = "";
                    var set1 = "bcdghjklmnpstw";
                    var set2 = "aiueouaiueo";
                    var set3 = "abcdefghijklmnopqrstuvwxyz";

                    for( var i = 0; i < length; i++ ) {
                        if ( i % 2 == 0 ) {
                            text += set1.charAt(Math.floor(Math.random() * set1.length));
                        }
                        else text += set2.charAt(Math.floor(Math.random() * set2.length));           
                    }
                        
                    return text;
                }
            </script>

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
