            <!-- FORM BEGIN -->
           
		   <div class="container">
		   <?php echo validation_errors(); ?>
		   
		   <?php
		   foreach ($row -> result() as $entry) :
		   $jadwalmingguanid = $entry->jadwalmingguanid;
		   endforeach;
		   
		   ?>
		   
			<?php echo form_open('site/updateschedule'.'/'.$jadwalmingguanid, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
		
               <fieldset>
                  <div class="row">
                    <div class="label">
                      <label>Nama Dokter<strong class="required">*</strong></br><em class="english">Doctor Name</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" name="nama" class="dropdown_form">
						<?php
						$ii=0;
						foreach ($doctor_name as $entry){
						if($doctor_name[$ii]["id"] == set_value('nama'))
						{
							echo "<option value=\"" . $doctor_name[$ii]["id"] . "\" selected=\"selected\">" . $doctor_name[$ii]["id"] . " - " . $doctor_name[$ii]["nama"] . "</option>";
						}
						else
						{
							echo "<option value=\"" . $doctor_name[$ii]["id"] . "\">" . $doctor_name[$ii]["id"] . " - " . $doctor_name[$ii]["nama"] . "</option>";
						}
						$ii++;
						}	
						?>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Pilih nama dokter.</label></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="label">
                      <label>Hari Praktik<strong class="required">*</strong><br/><em class="english"></em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" name="hari" class="dropdown_form">
                        <option value="" <?php echo (set_value('hari') == "")? "selected=\"selected\"" : "" ?>>Pilih Hari</option>
                        <option value="1" <?php echo (set_value('hari') == "1")? "selected=\"selected\"" : "" ?>>Senin</option>
                        <option value="2" <?php echo (set_value('hari') == "2")? "selected=\"selected\"" : "" ?>>Selasa</option>
                        <option value="3" <?php echo (set_value('hari') == "3")? "selected=\"selected\"" : "" ?>>Rabu</option>
                        <option value="4" <?php echo (set_value('hari') == "4")? "selected=\"selected\"" : "" ?>>Kamis</option>
                        <option value="5" <?php echo (set_value('hari') == "5")? "selected=\"selected\"" : "" ?>>Jumat</option>
						<option value="6" <?php echo (set_value('hari') == "6")? "selected=\"selected\"" : "" ?>>Sabtu</option>
						<option value="7" <?php echo (set_value('hari') == "7")? "selected=\"selected\"" : "" ?>>Minggu</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Pilih hari praktik dokter.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Mulai<strong class="required">*</strong><br><em class="english">Start Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" name="jamMulai" class="birthday_form dropdown_form">
                            <option value="">Pilih Jam</option>
                            <?php
								for($ii = 0; $ii<24; $ii++)
								{
									if(set_value('jamMulai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{								
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" name="menitMulai" class="birthday_form dropdown_form" value="<?php echo set_value('menitMulai'); ?>">
                            <option value="">Pilih Menit</option>
                            
                            <?php
								for($ii = 0; $ii<60; $ii++)
								{
									if(set_value('menitMulai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu mulainya praktik.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Selesai<strong class="required">*</strong><br><em class="english">Finish Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" name="jamSelesai" class="birthday_form dropdown_form" value="<?php echo set_value('jamSelesai'); ?>">
                            <option value="">Pilih Jam</option>
                            <?php
								for($ii = 0; $ii<24; $ii++)
								{
									if(set_value('jamSelesai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{								
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" name="menitSelesai" class="birthday_form dropdown_form" value="<?php echo set_value('menitSelesai'); ?>">
                            <option value="">Pilih Menit</option>
                            
                            <?php
								for($ii = 0; $ii<60; $ii++)
								{
									if(set_value('menitSelesai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu berakhirnya praktik.</label></span>
                    </div>
                    <strong class="required">* Wajib diisi</strong>
                  </div>

                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row" id="submit">
                      <a href="http://localhost/ci/index.php/site/schedule"><input type="button" value="cancel" class="button orange submit_button"></a>
                      <a href="http://localhost/ci/index.php/site/schedulesuccess.php"><input type="submit" value="submit" class="button green submit_button"></a>
                    </div>
                  </div>
                </fieldset>
            </div>
			<?php echo form_close(); ?>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>