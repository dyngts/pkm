            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">

              <div class="row" id="tabtabtab">
                <div class="toggles">
                   <p><a href="#" id="reset-graph-button">Reset</a></p>
                </div>
                <!-- 
                <div id="wrapper">
                    <div class="chart">
                      <h2>Statistik</h2>
                      
                      <table id="data-table">
                         <caption>Population in thousands</caption>
                         <thead>
                            <tr>
                               <td>&nbsp;</td>
                               <th scope="col">2012</th>
                               <th scope="col">2013</th>
                               <th scope="col">2014</th>
                               <th scope="col">2015</th>
                               <th scope="col">2016</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>1760</td>
                               <td>2880</td>
                               <td>4720</td>
                               <td>7520</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>6080</td>
                               <td>6240</td>
                               <td>3520</td>
                               <td>2240</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>1760</td>
                               <td>2880</td>
                               <td>4720</td>
                               <td>7520</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>1760</td>
                               <td>2880</td>
                               <td>4720</td>
                               <td>7520</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>6080</td>
                               <td>6240</td>
                               <td>3520</td>
                               <td>2240</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>1760</td>
                               <td>2880</td>
                               <td>4720</td>
                               <td>7520</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>1760</td>
                               <td>2880</td>
                               <td>4720</td>
                               <td>7520</td>
                            </tr>
                            <tr>
                               <th scope="row">Blue Monkey</th>
                               <td>5680</td>
                               <td>6880</td>
                               <td>5760</td>
                               <td>5120</td>
                               <td>2640</td>
                            </tr>
                         </tbody>
                      </table>
-->
<!--  
                <div id="wrapper">
                    <div class="chart">
                      <h2>Statistik Kunjungan Bulan April 2013</h2>
                      <table id="data-table">
                         <caption> </caption>
                         <thead>
                            <tr>
                               <td>&nbsp;</td>
                               <th scope="col">April</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <th scope="row">FK</th>
                               <td>1040</td>
                            </tr>
                            <tr>
                               <th scope="row">FKG</th>
                               <td>4080</td>
                            </tr>
                            <tr>
                               <th scope="row">FH</th>
                               <td>1090</td>
                            </tr>
                            <tr>
                               <th scope="row">FE</th>
                               <td>1540</td>
                            </tr>
                            <tr>
                               <th scope="row">FT</th>
                               <td>4080</td>
                            </tr>
                            <tr>
                               <th scope="row">FIB</th>
                               <td>2040</td>
                            </tr>
                            <tr>
                               <th scope="row">Fasilkom</th>
                               <td>900</td>
                            </tr>
                            <tr>
                               <th scope="row">FPsi</th>
                               <td>5680</td>
                            </tr>
                            <tr>
                               <th scope="row">FKM</th>
                               <td>1100</td>
                            </tr>
                            <tr>
                               <th scope="row">FIK</th>
                               <td>4080</td>
                            </tr>
                            <tr>
                               <th scope="row">FISIP</th>
                               <td>2140</td>
                            </tr>
                            <tr>
                               <th scope="row">FMIPA</th>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">FF</th>
                               <td>440</td>
                            </tr>
                            <tr>
                               <th scope="row">Vokasi</th>
                               <td>3040</td>
                            </tr>
                            <tr>
                               <th scope="row">Pascasarjana</th>
                               <td>110</td>
                            </tr>
                         </tbody>
                      </table>
                    </div>
                </div>
 -->                

                  <div id="wrapper">
                    <div class="chart">
                      <h2>Statistik Kepuasan Pasien Tahun <?php echo $tahun;?></h2>
                      <table id="data-table">
                         <caption></caption>
                         <thead>
                            <!-- <tr>
                               <td>&nbsp;</td>
                          //<?php //foreach($query as $row){ echo"
                          //     <th scope=\"col\">".$row->time_stamp."</th>"
                          //};?>
                            </tr> -->
                            <tr>
                               <td>&nbsp;</td>
                               <th scope="col">Jan</th>
                               <th scope="col">Feb</th>
                               <th scope="col">Mar</th>
                               <th scope="col">Apr</th>
                               <th scope="col">Mei</th>
                               <!-- <th scope="col">Jun</th>
                               <th scope="col">Jul</th>
                               <th scope="col">Agt</th>
                               <th scope="col">Sep</th>
                               <th scope="col">Okt</th>
                               <th scope="col">Nov</th>
                               <th scope="col">Des</th> -->
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                              <th scope="row">Tidak Puas</th>
                                <?php foreach ($query_tidakpuas as $row) {
                                  echo "<td>".$row->tidakpuas."</td>";
                                };?>
                               <!-- <td>3</td>
                               <td>8</td>
                               <td>1</td>
                               <td>1</td>
                               <td>1</td>
                               <td>8</td>
                               <td>1</td>
                               <td>3</td>
                               <td>8</td>
                               <td>1</td>
                               <td>1</td> -->
                            </tr>
                            <tr>
                               <th scope="row">Kurang Puas</th>
                                <?php foreach ($query_kurangpuas as $row) {
                                  echo "<td>".$row->kurangpuas."</td>";
                                };?>
                               <!-- <td>1</td>
                               <td>8</td>
                               <td>1</td>
                               <td>6</td>
                               <td>4</td>
                               <td>2</td>
                               <td>5</td>
                               <td>1</td>
                               <td>1</td>
                               <td>8</td>
                               <td>1</td>
                               <td>1</td> -->
                            </tr>
                         </tbody>
                      </table>
           
                    </div>
                </div>
                      <!-- 
                <div id="wrapper">
                    <div class="chart">
                      <h2>Statistik Kepuasan Pasien Tahun 2013</h2>
                      <table id="data-table">
                         <caption></caption>
                         <thead>
                            <tr>
                               <td>&nbsp;</td>
                               <th scope="col">Januari</th>
                               <th scope="col">Februari</th>
                               <th scope="col">Maret</th>
                               <th scope="col">April</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <th scope="row">Tidak Puas</th>
                               <td>580</td>
                               <td>700</td>
                               <td>200</td>
                               <td>300</td>
                            </tr>
                            <tr>
                               <th scope="row">Puas</th>
                               <td>1040</td>
                               <td>800</td>
                               <td>1500</td>
                               <td>1600</td>
                            </tr>
                         </tbody>
                      </table>
           
                    </div>
                </div>
 -->

<!-- 
                      <table id="data-table">
                         <caption>Population in thousands</caption>
                         <thead>
                            <tr>
                               <td>&nbsp;</td>
                               <th scope="col">2012</th>
                               <th scope="col">2013</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>3440</td>
                               <td>1040</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Blue Monkey</th>
                               <td>5680</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Carbon Tiger</th>
                               <td>4080</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Tanned Zombie</th>
                               <td>1040</td>
                               <td>3440</td>
                            </tr>
                            <tr>
                               <th scope="row">Blue Monkey</th>
                               <td>5680</td>
                               <td>3440</td>
                            </tr>
                         </tbody>
                      </table>
                    </div>
                </div>
  -->             

              </div>

              <div class="row inlineblock">
                <div>
                  <p>Statistik jumlah data pengunjung PKM.<em class="english"></em></p>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/fakultas");?>">
                    <div class="statslabel">
                      <label>a. Statistik jumlah data pengunjung pasien PKM berdasarkan fakultas.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                      <div class="inlineblock">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                      </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/periode");?>">
                    <div class="statslabel">
                      <label>b. Statistik jumlah data pengunjung pasien PKM berdasarkan tahun periode.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="p_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              <?php if ($this->authentication->is_pelayanan() || $this->authentication->is_loket()) { echo "
              <div class=\"row inlineblock\">
                  <form method=\"post\" action=\"".site_url("statistik/statistikkepuasan")."\">
                    <div class=\"statslabel\">
                      <label>c. Statistik kepuasan pengunjung PKM berdasarkan tahun periode.<em class=\"english\"></em></label>
                    </div>
                    <div class=\"row inlineblock\">
                      <div class=\"margin_left\">
                        <div class=\"input\">
                          <p class=\"p_form\"></p> 
                          <select name=\"k_tahun\" size=1 class=\"dropdown_form\">
                              <option value=\"\">- Pilih Tahun -</option>
                              ";?>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear&&$i>0;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                              <?php echo"</select>
                        </div>
                        <div class=\"inlineblock\">
                          <input type=\"submit\" value=\"Tampilkan\" class=\"button green table_button\">
                        </div>
                        <div class=\"note\">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              ";}?>
              </div>

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
            
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>