          </div>
        </div>
        <style type="text/css" media="screen">
          .aboutproject {
            padding: 0 20px;
            margin: auto;
            max-width: 900px;
          }
          .aboutproject .img.itm {
            margin: auto;
            display: block;
            max-width: 300px;
          }
          .aboutproject .pikachoose .jcarousel-clip.jcarousel-clip-horizontal {
            display: none;
          }
          .aboutproject .link {
            color: #4171B1;
          }
          .aboutproject .link:hover {
            text-decoration: underline;
          }
          .aboutproject .header-link {
            text-transform: none;
            font-size: 0.7em;
          }
        </style>
        <link type="text/css" href="<?php echo base_url('css/base.css');?>" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo base_url('js/jquery.jcarousel.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('js/jquery.pikachoose.js');?>"></script>
            <!-- FORM BEGIN -->
            <div class="container">
              <div class="aboutproject">
                <h1>Tentang Proyek <spam class="header-link">(<a class="link" href="https://bitbucket.org/dyngts/pkm.git" title="">Bitbucket</a>)</spam></h1>
                
                <img class="img itm" src="<?php echo base_url('image/s0.jpg');?>"/>
                <p>Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) adalah 
                  suatu badan yang bergerak dalam bidang pelayanan kesehatan yang 
                  beroperasi di dua cabang, yaitu di UI Salemba dan UI Depok. Sampai 
                  saat ini, layanan kesehatan yang disediakan masih diproses secara 
                  manual, seperti penyimpanan data pasien dan pegawai PKM UI, 
                  pencatatan kegiatan operasional apotek, dan pembuatan laporan yang 
                  terkait dengan keuangan dan penyakit. Sistem informasi poliklinik 
                  ini dapat memberikan solusi terhadap kendala tersebut dan 
                  mengintegrasikan proses bisnis PKM UI.
                </p>
                <ol style="list-style: 1">
                  <p>Sistem ini terdiri dari tiga subsistem sebagai berikut:.
                </p>
                  <li><h3>Sistem Pelayanan</h3>
                    <p>
                      <img class="img itm" src="<?php echo base_url('image/s1.jpg');?>"/>
                      Dapat dilihat, ada beberapa fitur di modul ini yang berkaitan 
                      erat dengan modul lain, yaitu modul sistem apotek dimana fitur-fitur 
                      seperti rekam medis yang membuat resep dan dikirimkan ke fitur daftar 
                      resep di sistem apotek. Selain itu, fitur laporan apotek didapat dari 
                      hasil log transaksi log obat keluar dan log obat masuk di modul sistem 
                      apotek.
                    </p>
                  </li>
                  <li><h3>Sistem Registrasi</h3>
                    <p>
                      <img class="img itm" src="<?php echo base_url('image/s2.jpg');?>"/>
                     Dapat dilihat, fitur manajemen akun berhubungan langsung dengan dua 
                     modul sistem lainnya, yaitu sistem pelayanan dan sistem apotek untuk 
                     proses otorisasi dan otentikasi.
                    </p>
                  </li>
                  <li><h3>Sistem Apotek</h3>
                    <p>
                      <img class="img itm" src="<?php echo base_url('image/s3.jpg');?>"/>
                      Dapat dilihat, fitur resep terintegrasi dengan fitur rekam medis di 
                      modul sistem pelayanan dimana saat seorang dokter membuat sebuah 
                      rekam medis, maka secara otomatis resep akan terbuat dan terdaftar di 
                      fitur pengeluaran resep yang nantinya akan diproses oleh modul sistem 
                      apotek.
                    </p>
                  </li>
                </ol>
                <br/>
                <h3>Fitur Utama</h3>
                <ol>
                  <li>Otorisasi sistem</li>
                  <li>Manajemen data pasien</li>
                  <li>Menyimpan dan menampilkan daftar obat</li>
                  <li>Pencatatan stok obat</li>
                  <li>enampilkan statistik penggunaan obat</li>
                  <li>Membuat rekam medis pasien</li>
                  <li>Membuat laporan pemasukan keuangan</li>
                </ol>

                <h3>Screenshot</h3>
                <div class="pikachoose">
                  <ul id="pikame" class="jcarousel-skin-pika" style="margin:auto;">
                    <?php $sipkm_imgs = array('login.png','rekammedis.png','daftarpasien.png','laporan.png','register.png','resep.png','statistik.png','tindakan.png'); ?>
                    <?php foreach($sipkm_imgs as $sipkm_img) { ?>
                    <li><img src="<?php echo base_url('image/'.$sipkm_img);?>"></a><span></span></li>
                    <?php } ?>
                  </ul>
                </div>
                <script language="javascript">
                  $(document).ready(
                    function (){
                      $("#pikame").PikaChoose({carousel:true});
                    });
                </script>
              </div>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>