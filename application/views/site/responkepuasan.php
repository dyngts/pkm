              <div class="container">
                <div class="">
        
                  <div class="table_box">
                    <h2>Sejarah Berobat</h2>
          <br>
          <?php 
            if (empty($row)){
            echo "
              <div class='message message-green'>
                        <p class='p_message'>Anda tidak mempunyai riwayat pengobatan yang belum diberi respon. </p>
                      </div>";
            }
            else{
            ?>
                    <table>
                      <thead>
                        <tr class="odd-row inner-table">
                          <th class="first">No</th>
                          <th class="even-col">Tanggal</th>
                          <th>Isi Respon Kepuasan</th>
              <th class="even-col">Tindakan</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr class="last inner-table">
                          <td colspan=5></td>
                        </tr>
                      </tfoot>
                      <tbody>
              
            <?php
            $rownum =0;
            foreach ($row as $r):
              $tanggal = $r->tanggal;
              $id_registrasi = $r->id;
              $rownum++; ?>
          
                        <tr class="inner-table">
            
                          <td class="first"><?php echo $rownum; ?></td>
                          <td><?php echo $tanggal?></td>
                <!--
                          
                <input type="radio" name="respons" id="respons" value="Sangat Puas"<?php echo set_select('respons', "1"); ?>/> Sangat Puas <br>
                <input type="radio" name="respons" id="respons" value="2"<?php echo set_select('respons', "2"); ?>/> Puas <br>
                <input type="radio" name="respons" id="respons" value="3"<?php echo set_select('respons', "3"); ?>/> Kurang Puas <br>
                <input type="radio" name="respons" id="respons" value="4"<?php echo set_select('respons', "4"); ?>/> Tidak Puas <br>
            -->
            <td class ="last">
            <?php echo form_open ('site/responkepuasansukses/'.$id_registrasi,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
                              <select name = "respons" size="1" >
                                  <option value="" selected="selected">- pilih respon -</option>
                                  <option value="1" <?php echo set_select('respons', "1"); ?> >Tidak Puas</option>
                                  <option value="2" <?php echo set_select('respons', "2"); ?>>Kurang Puas</option>
                                  <option value="3" <?php echo set_select('respons', "3"); ?>>Puas</option>
                                  <option value="4" <?php echo set_select('respons', "4"); ?>>Sangat Puas</option>
                              </select> 
              </td>
              <td>              
               <input type="submit" value="submit" class="button green">
               <?php echo form_close (); ?>
              </td>
              
                        </tr>
            <?php endforeach; } ?>
                      </tbody>
                    </table>
          <?php echo form_close();?>
                  </div>
                </div>
              </div>
              <!-- MAIN CONTENT 1 END -->
              <!-- WHITESPACE-->
               <div class="whitespace"></div>