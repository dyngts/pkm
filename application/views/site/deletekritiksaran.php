            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->

             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Kritik dan Saran</h2>
                  
				<div class="row">
					<div class="message message-green">
					  <p class="p_message">Kritik dan Saran berhasil dihapus! </p>
					</div>
				</div>
			  
				  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Tanggal Dibuat</th>
                        <th>ID Pasien</th>
                        <th class="even-col">Kritik/Saran</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_kritik_saran_list">
                      <?php 
							$rownum =0;
							foreach ($row ->result() as $r) :
							$rownum++;
						?>
					  <tr class="inner-table">						
						<td><?php echo $rownum; ?></td>
						<td><?php echo $r->time_stamp;?></td>
						<td><?php echo $r->id_pasien;?></td>
                        <td><?php echo $r->text_isi; ?></td>
						<?php if ($this->authentication->is_pelayanan() || $this->authentication->is_loket()){ ?>
						<td> <div class="row" class="table_button_group">
							<?php echo form_open ("site/delete_kritiksaran/".$r->id_kritik,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?> 
							<a href="http://localhost/ci/index.php/site/deletekritiksaran"><input type="submit" value="Hapus" class="button red table_button"></a>
                            <?php echo form_close();?>
                           </div> 
                        </td> 
						<?php 
						}
						?>
					  </tr>
					  <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>