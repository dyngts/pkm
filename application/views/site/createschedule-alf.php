            <!-- FORM BEGIN -->
            <div class="container">

              <form  action="<?php echo site_url("site/schedulesuccess"); ?>" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.');">
                <fieldset>
                  <div class="row">
                    <div class="label">
                      <label>Nama Dokter<strong class="required">*</strong></br><em class="english"></em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                        <option value="a1">-none-</option>
                        <option value="b1">dr. Fauria</option>
                        <option value="c1">dr. Apit</option>
                        <option value="d1">dr. Bisara Aurauraura</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Choose one.</label></span>
                    </div>
                  </div>
                  <!-- 
                  <div class="row">
                    <div class="label">
                      <label>Jenis Poli<strong class="required">*</strong><br/><em class="english"></em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                        <option value="a1">-none-</option>
                        <option value="b1">Poli Umum</option>
                        <option value="c1">Poli Gigi</option>
                        <option value="d1">Poli Estetika Medis</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Choose one.</label></span>
                    </div>
                  </div>
 -->
                  <div class="row">
                    <div class="label">
                      <label>Hari Praktik<strong class="required">*</strong><br/><em class="english"></em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                        <option value="h1">-none-</option>
                        <option value="h2">Senin</option>
                        <option value="h3">Selasa</option>
                        <option value="h4">Rabu</option>
                        <option value="h5">Kamis</option>
                        <option value="h6">Jumat</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Choose one.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Mulai<strong class="required">*</strong><br><em class="english"></em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">--</option>
                            <option value="1">8</option>
                            <option value="2">9</option>
                            <option value="3">10</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">19</option>
                            <option value="3">20</option>
                        </select>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">--</option>
                            <option value="1">00</option>
                            <option value="2">15</option>
                            <option value="3">30</option>
                            <option value="3">45</option>
                        </select>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Selesai<strong class="required">*</strong><br><em class="english"></em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">--</option>
                            <option value="1">8</option>
                            <option value="2">9</option>
                            <option value="3">10</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">19</option>
                            <option value="3">20</option>
                        </select>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">--</option>
                            <option value="1">00</option>
                            <option value="2">15</option>
                            <option value="3">30</option>
                            <option value="3">45</option>
                        </select>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu.</label></span>
                    </div>
                    <strong class="required">* required</strong>
                  </div>

                  <!-- CAPTCHA -->
                  <!--<div class="r_captcha">
                    <h1>KODE KEAMANAN</h1>
                  </div> -->


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row" id="submit">
                      <a href="<?php echo site_url("site/schedule"); ?>"><input type="button" value="cancel" class="button orange submit_button"></a>
                      <input type="submit" value="submit" class="button green submit_button">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>