            <!-- BEGIN -->
            <div class="container">
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PASIEN</h2>
                  <!-- 
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Data berhasil disimpan! </p>
                      <p class="p_message">Anda akan mendapatkan ID pasien setelah diverifikasi.</p>
                      <p class="p_message p_message_eng">Success!</p>
                    </div>
                  </div>
                   -->
                  <div>
                    <?php if (isset($msg2)) echo $msg2;?>
                  <?php 
                      foreach($query as $row){ if(!($this->authentication->is_baru()||$this->authentication->is_mendaftar())) echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>ID Pasien<br/><em class=\"english\">ID</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->id."</p>
                    </div>";

                    echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Peran<br/><em class=\"english\">Role</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->peran."</p>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Nama Lengkap<br/><em class=\"english\">Full Name</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->nama."</p>
                    </div>


                    <div class=\"row\">
                        <div class=\"label\">
                          <label>No telp<br/><em class=\"english\">no telp</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->no_telepon."</p>
                        </div>
                    </div>
                    
                    <div class=\"row\">
                        <div class=\"label\">
                          <label>Alamat<br/><em class=\"english\">Address</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->alamat."</p>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"label\">
                          <label> Tempat, Tanggal Lahir<br/><em class=\"english\">Place and date of birth </em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->tempatlahir.", ".$row->tanggallahir."</p>
                        </div>
                    </div>
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jenis Kelamin<br/><em class=\"english\">Sex</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->jeniskelamin."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Agama<br/><em class=\"english\">Religion</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->agama."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Status Pernikahan<br/><em class=\"english\">Marital Status</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->statuspernikahan."</p>
                      </div>
                    </div>
        
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Kewarganegaraan <br/><em class=\"english\">Nationality</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->kewarganegaraan."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Golongan Darah<br/><em class=\"english\">Bloodtype</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->goldarah."</p>
                      </div>
                    </div>
                    ";}?>

                    <?php if (isset($query2)) {
                    if ($role == "Masyarakat Umum") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Pekerjaan<br/><em class=\"english\">Occupation</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->pekerjaan."</p>
                      </div>
                    </div>
                    "; }

                    else if ($role == "Karyawan") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Lembaga<br/><em class=\"english\">Institution</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->lembaga."</p>
                      </div>
                    </div>
                    "; }

                    else if ($role == "Mahasiswa") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>NPM<br/><em class=\"english\">NPM</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->npm."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Fakultas<br/><em class=\"english\">Faculty</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->fakultas."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jurusan<br/><em class=\"english\">Majors</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->jurusan."</p>
                      </div>
                    </div>
                    ";}}?>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">

                        <?php if ($show_button) {
                                if($this->authentication->is_loket() || $this->authentication->is_pasien()) {
                                  foreach($query as $row){
                                    echo "<a href=".site_url("site/editpasien/$row->id")."><input type=\"button\" value=\"Ubah Data\" class=\"button orange submit_button\"></a>";
                                    //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    //<form style=\"display:inline\" action=".site_url("site/deletepasien/$row->id")."  method=\"post\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.')\"><input type=\"submit\" value=\"Hapus Pasien\" class=\"button red table_button\" /></form>" ;
                                  }
                                }
                              }?>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo base_url("foto/".$row->id.".jpg"); ?>" alt="tanpa foto" height=264 width=198>
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>