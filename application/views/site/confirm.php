            <!-- POPUP BEGIN -->
            <div id="konfirmasi" class="popup">
              <div class="message">
                <div class="row title">
                  <h3>Konfirmasi</h3>
                </div>
                <div class="row">
                  <div class="popup_message">
                    <p class="p_message">Apakah Anda yakin?</p>
                  </div>
                </div>

              <!-- WHITESPACE -->
              <div class="whitespace"></div>

                <div class="row">
                  <div class="row" id="submit">
                    <input type="button" value="Ya" class="button green submit_button">
                    <input type="button" value="Tidak" class="button red submit_button">
                  </div>
                </div>
              </div>
            </div>
            <!-- POPUP END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>