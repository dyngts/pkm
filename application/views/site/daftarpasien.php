            <!-- BEGIN -->
            <div class="container">

              <div class="row">
                <a href="<?php echo site_url("site/registrasi"); ?>">
                  <input type="button" value="Buat Akun Pasien Baru" class="button green">
                </a>
              </div>

              <div class="row">
                <!-- <h1>Daftar Pasien PKM UI</h1> -->
              </div>

             <?php echo $msg;?>

              <div class="">
                <div class="table_box">
                  <h2>Daftar Pasien</h2>
  
                  <br/><div><?php echo $nav;?></div><br/>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID Pasien</th>
                        <th >Nama</th>
                        <th class="even-col">Peran</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td class="last" colspan = 5></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i = $page_num;
                        if(isset($query)) {
                          foreach($query as $row){ 
                            $i++;
                            echo "<tr class=\"";if(($i%2)==1) echo "odd-row "; echo"inner-table\">";
                            echo "<td class=\"first\">".$i."</td>";
                            echo "<td class=\"first\">".$row->id."</td>";
                            echo "<td class=\"left\">".$row->nama."</td>";
                            echo "<td class=\"left\">".$row->jabatan."</td>";
                            echo "<td class=\"last\">
                              <div class=\"row\" class=\"table_button_group\">
                                <a href=\"".site_url("akun/profil/".$row->id)."\"><input type=\"button\" value=\"Lihat Profil\" class=\"button green table_button\"></a>
                                <a href=\"".site_url("akun/edit/".$row->id)."\"><input type=\"button\" value=\"Ubah Profil\" class=\"button orange table_button\"></a>
                             </div>
                            </td>
                          </tr>";
                          }
                        }
                      ?>
                    </tbody>
                  </table>     
                  <div style="text-align: center"><?php echo $nav;?></div>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>