            <!-- FORM BEGIN -->
            <div class="container">
              <form>
                <fieldset>
                  <div class="row">
                    <div class="message message-red">
                      
                      <p class="p_message">Yang hampir fix tuh
                        </p><ol type="1">
                          <li></li>
                          <li></li>
                          <li></li>
                        </ol>
                      <p class="p_message"></p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="message message-green">
                      
                      <p class="p_message">Data berhasil disimpan!</p>
                      <p class="p_message p_message_eng">Success!</p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="message message-yellow">
                      
                      <p class="p_message">Data berhasil disimpan!</p>
                      <p class="p_message p_message_eng">Success!</p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Nama Lengkap<strong class="required">*</strong></br><em class="english">Full Name</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Alamat<br/><em class="english">Address</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea class="input_form address_form"></textarea>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="row">
                      <div class="label">
                        <label>Tempat, Tanggal Lahir<br><em class="english">Place and date of birth</em></label>
                      </div>
                      <div class="input">
                          <p class="p_form" id="p_birthday">:</p> 
                        <div id="place" class="input">
                          <input type="text" id="birth_place_form" class="input_form" value="" name="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Hari<br><em class="english">Day</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">30</option>
                            <option value="3">31</option>
                        </select>
                        <p class="p_form p_birthday">Bulan<br><em class="english">Month</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">11</option>
                            <option value="3">12</option>
                        </select>
                        <p class="p_form p_birthday">Tahun<br><em class="english">Year</em></p> 
                        <select size="1" class="birthday_form dropdown_form">
                            <option value="1">1900</option>
                            <option value="2">1901</option>
                            <option value="3">1902</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">.</option>
                            <option value="3">2009</option>
                            <option value="3">2010</option>
                        </select>
                      </div>
                      <div class="note">
                        <span><label>Email utama tidak dapat diubah.</label></span>
                      </div>
                    </div>
                  </div>

                    
                  
                  
                  <div class="row">
                    <div class="label">
                      <label>Jenis Kelamin<br><em class="english">Sex</em></label>
                    </div>
                   <div class="input">
                      <p class="p_form">:</p> 
                      <input type="radio" value="male" name="sex" class="input_form sex_form"> Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                      <input type="radio" value="female" name="sex" class="input_form sex_form"> Perempuan
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Agama<br><em class="english">Religion</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                          <option value="aa">1</option>
                          <option value="bb">2</option>
                          <option value="ab">3</option>
                          <option value="oo">4</option>
                          <option value="ab">5</option>
                          <option value="oo">6</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Status Pernikahan<br><em class="english">Marriage Status</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                          <option value="bb">Belum menikah</option>
                          <option value="aa">Menikah</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Kewarganegaraan<strong class="required">*</strong><br><em class="english">Nationality</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="" value="" class="input_form">
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Golongan Darah<br><em class="english">Bloodtype</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                          <option value="aa">A</option>
                          <option value="bb">B</option>
                          <option value="ab">AB</option>
                          <option value="oo">O</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Fakultas<br><em class="english">Faculty</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" class="dropdown_form">
                          <option value="fasilkom">Fakultas Ilmu Komputer</option>
                          <option value="fisip">Fakultas Ilmu Sosial dan Ilmu Politik</option>
                          <option value="fh">Fakultas Hukum</option>
                          <option value="fib">Fakultas Ilmu Pengetahuan budaya</option>
                      </select>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <!-- CAPTCHA -->
                  <!--<div class="r_captcha">
                    <h1>KODE KEAMANAN</h1>
                  </div> -->


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row" id="submit">
                      <input type="button" value="back" class="button orange submit_button">
                      <input type="submit" value="submit" class="button green submit_button">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>