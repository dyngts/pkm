            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->
            <script type="text/javascript">
                function toggle(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Jadikan Draft') {
                      document.getElementById(b).value = 'Terbitkan';
                    }
                    else {
                    document.getElementById(b).value = 'Jadikan Draft';
                    }
                  }
                }
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_articlelist').deleteRow(i);
                  }
                }
                function showconfirm()
                {
                  confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                }
            </script>

              <div class="row">
                <a href="<?php echo site_url("index.php/site/article"); ?>">
                  <input type="button" value="Buat Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Artikel / Informasi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Tanggal Dibuat</th>
                        <th>Judul Artikel / Informasi</th>
                        <th class="even-col">Status</th>  
                        <th>Penulis</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=6></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_articlelist">
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>11/09/2013</td>
                        <td class="left">Tips Mengobati Sakit Nyeri Punggung</td>
                        <td>Sudah diterbitkan</td>
                        <td>Koordinator Pelayanan</td>
                        <td class="last">
                          <div class="row table_button_group">
                            <input type="button" value="Ubah" class="button orange table_button" onclick="window.location = '<?php echo base_url("site/editarticle");?>'">
                            <input type="button" value="Hapus" class="button red table_button" onclick="deleterow(this)">
                           </div>
                          <div class="row table_button_group">
                            <input id="ooo" type="button" value="Jadikan Draft" class="button green table_button" onclick="toggle(id)">
                           </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">2</td>
                        <td>11/09/2013</td>
                        <td class="left">Tips Menangani Darah yang tidak Kunjung Membeku</td>
                        <td> Draft </td>
                        <td>Koordinator Pelayanan</td>
                        <td class="last">
                          <div class="row table_button_group">
                            <input type="button" value="Ubah" class="button orange table_button" onclick="window.location = '<?php echo base_url("site/editarticle");?>'">
                            <input type="button" value="Hapus" class="button red table_button" onclick="deleterow(this)">
                           </div>
                          <div class="row table_button_group">
                            <input id="oob" type="button" value="Jadikan Draft" class="button green table_button" onclick="toggle(id)">
                           </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">3</td>
                        <td>11/09/2013</td>
                        <td class="left">Seminar Kesehatan: Menjaga Kesehatan Lambung</td>
                        <td>Sudah diterbitkan</td>
                        <td>Staff Loket</td>
                        <td class="last">
                          <div class="row table_button_group">
                            <input type="button" value="Ubah" class="button orange table_button" onclick="window.location = '<?php echo base_url("site/editarticle");?>'">
                            <input type="button" value="Hapus" class="button red table_button" onclick="deleterow(this)">
                           </div>
                          <div class="row table_button_group">
                            <input id="ooc" type="button" value="Jadikan Draft" class="button green table_button" onclick="toggle(id)">
                           </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">4</td>
                        <td>11/09/2013</td>
                        <td class="left">Informasi Kegiatan : Donor Darah di Fakultas Teknik</td>
                        <td>Sudah diterbitkan</td>
                        <td>Staff Loket</td>
                        <td class="last">
                          <div class="row table_button_group">
                            <input type="button" value="Ubah" class="button orange table_button" onclick="window.location = '<?php echo base_url("site/editarticle");?>'">
                            <input type="button" value="Hapus" class="button red table_button" onclick="deleterow(this)">
                           </div>
                          <div class="row table_button_group">
                            <input id="ood" type="button" value="Jadikan Draft" class="button green table_button" onclick="toggle(id)">
                           </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>