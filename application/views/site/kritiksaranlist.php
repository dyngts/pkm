            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->

             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Kritik dan Saran</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Tanggal Dibuat</th>
						<th>ID Pasien</th>
                        <th class="even-col">Kritik/Saran</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_kritik_saran_list">
                      <?php 
							$rownum= $page_num;
							foreach ($row as $r) :
							$rownum++;
						?>
					  <tr class="inner-table">						
						<td><?php echo $rownum; ?></td>
						<td><?php echo $r->waktu;?></td>
						<td><?php echo $r->id_pasien;?></td>
            <td class="text-left"><?php echo $r->isi; ?></td>
						<?php if ($this->authentication->is_pj() || $this->authentication->is_pelayanan() || $this->authentication->is_loket()) {
						echo '<td> <div class="row" class="table_button_group">';
						echo form_open ("site/delete_kritiksaran/".$r->id,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');'));
						echo	'<a href="http://localhost/ci/index.php/site/deletekritiksaran"><input type="submit" value="Hapus" class="button red table_button">';
                echo form_close();?>
               </div> 
            </td> 
						<?php 
						}
						?>
					  </tr>
					  <?php endforeach; ?>
                    </tbody>
                  </table>
				  <div style="text-align: center"><?php echo $nav;?></div>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>