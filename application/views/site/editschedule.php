            <!-- FORM BEGIN -->
           
		   <div class="container">		   
               <fieldset>			   
				<?php 					
						foreach ($row -> result() as $entry) :						
							$jadwalmingguanid = $entry->jadwalmingguanid;
							$hari = $entry->hari;
							$waktuawal = $entry->waktuawal;
							$dateparts = date_parse($waktuawal);
							$nama = $entry->id_pegawai;
							$waktuakhir = $entry->waktuakhir;
							$datepartsend = date_parse($waktuakhir);							
						endforeach;
           						
						?>
			<?php echo form_open("site/updateschedule/".$jadwalmingguanid, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
            <div class="row">
              <div class="label">
                <label>Nama Dokter<strong class="required">*</strong></br><em class="english">Doctor Name</em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
                <select size="1" name="nama" class="dropdown_form">
  						    <option value="">Pilih Nama Dokter</option>
      						<?php
        						$ii=0;
        						foreach ($doctor_name as $entry){
          						if($doctor_name[$ii]["id"] == $nama) {
          							echo "<option value=\"" . $doctor_name[$ii]["id"] . "\" selected=\"selected\">" . $doctor_name[$ii]["id"] . " - " . $doctor_name[$ii]["nama"] . "</option>";
          						}
          						else {
          							echo "<option value=\"" . $doctor_name[$ii]["id"] . "\">" . $doctor_name[$ii]["id"] . " - " . $doctor_name[$ii]["nama"] . "</option>";
          						}
          						$ii++;
        						}	
      						?>
                </select>
              </div>
					      <div class="required">
                  <?php echo form_error('nama'); ?>
                </div>
                <div class="note">
                  <span><label>Pilih nama dokter.</label></span>
                </div>
            </div>

            <div class="row">
              <div class="label">
                <label>Hari Praktik<strong class="required">*</strong><br/><em class="english">Schedule</em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
						    <?php
                  $pilihan = array(""=>'Pilih Hari', 1=>'Senin', 2=>'Selasa', 3=>'Rabu', 4=>'Kamis', 5=>'Jumat', 6=>'Sabtu');						
  						    echo form_dropdown('hari', $pilihan, $hari);
						    ?>
              </div>
					      <div class="required"> <?php echo form_error('hari'); ?> </div>
                <div class="note">
                  <span><label>Pilih hari praktik dokter.</label></span>
                </div>
            </div>

            <div class="row">
              <div class="label">
                <label>Waktu Mulai<strong class="required">*</strong><br><em class="english">Start Time</em></label>
              </div>
              <div class="input input_birthday"> 
                <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                <div class="birthday_form dropdown_form">
							  <?php
								  $jammulai = array(""=>'Pilih Jam', '00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23');
								  $jamawal=$dateparts['hour'];
								  echo form_dropdown('jamMulai', $jammulai, $jamawal);
							  ?>
                </div>
  						  <div class="required"> <?php echo form_error('jamMulai'); ?> </div>
                <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                <div class="birthday_form dropdown_form">
                <?php
							 	  $menitmulai = array(""=>'Pilih Menit', '00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23', '24'=>'24', '25'=>'25', '26'=>'26', '27'=>'27', '28'=>'28', '29'=>'29', '30'=>'30', '31'=>'31', '32'=>'32', '33'=>'33', '34'=>'34', '35'=>'35', '36'=>'36', '37'=>'37', '38'=>'38', '39'=>'39', '40'=>'40', '41'=>'41', '42'=>'42', '43'=>'43', '44'=>'44', '45'=>'45', '46'=>'46', '47'=>'47', '48'=>'48', '49'=>'49', '50'=>'50', '51'=>'51', '52'=>'52', '53'=>'53', '54'=>'54', '55'=>'55', '56'=>'56', '57'=>'57', '58'=>'58', '59'=>'59');
								  $menitawal=$dateparts['minute'];
								  echo form_dropdown('menitMulai', $menitmulai, $menitawal);
							  ?>
                </div>
						    <div class="required"> <?php echo form_error('menitMulai'); ?> </div>
              </div>
              <div class="note">
                <span><label>Pilih waktu mulainya praktik.</label></span>
              </div>
            </div>

            <div class="row">
              <div class="label">
                <label>Waktu Selesai<strong class="required">*</strong><br><em class="english">Finish Time</em></label>
              </div>
              <div class="input input_birthday"> 
                <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                <div class="birthday_form dropdown_form">
    							<?php
    								$jamselesai = array(""=>'Pilih Jam', '00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23');
    								$jamakhir=$datepartsend['hour'];
    								echo form_dropdown('jamSelesai', $jamselesai, $jamakhir);
    							?>
                </div>
						    <div class="required"> <?php echo form_error('jamSelesai'); ?> </div>
                <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                <div class="birthday_form dropdown_form">   
                  <?php
    								$menitselesai = array(""=>'Pilih Menit', '00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23', '24'=>'24', '25'=>'25', '26'=>'26', '27'=>'27', '28'=>'28', '29'=>'29', '30'=>'30', '31'=>'31', '32'=>'32', '33'=>'33', '34'=>'34', '35'=>'35', '36'=>'36', '37'=>'37', '38'=>'38', '39'=>'39', '40'=>'40', '41'=>'41', '42'=>'42', '43'=>'43', '44'=>'44', '45'=>'45', '46'=>'46', '47'=>'47', '48'=>'48', '49'=>'49', '50'=>'50', '51'=>'51', '52'=>'52', '53'=>'53', '54'=>'54', '55'=>'55', '56'=>'56', '57'=>'57', '58'=>'58', '59'=>'59');
    								$menitakhir=$datepartsend['minute']; 
    								echo form_dropdown('menitSelesai', $menitselesai, $menitakhir);
    							?>
                </div>
						    <div class="required"> <?php echo form_error('menitSelesai'); ?> </div>
              </div>
              <div class="note">
                <span><label>Pilih waktu berakhirnya praktik.</label></span>
              </div>
              <strong class="required">* Wajib diisi</strong>
            </div>
				  
  					<div class="required"> 
  						<?php if($sql!=null)
  						echo $sql;
  						?>
  					</div>
					
                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row" id="submit">
                      
                      <?php echo anchor("site/updateschedule/".$jadwalmingguanid, '<input type="submit" value="submit" class="button green submit_button">');
                      ?>
			<?php echo form_close(); ?>
			<?php echo form_open("site/schedule/", array()); ?>
			<?php echo "<input type=\"submit\" value=\"cancel\" class=\"button orange submit_button\">";?>
                      <?php echo form_close(); ?>
                    </div>
                  </div>
                </fieldset>
            </div>
			
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>