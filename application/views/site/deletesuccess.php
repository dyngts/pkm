              <div class="container">
			  
			  <div class="row">
                <h1>Jadwal Praktik Dokter</h1>
              </div>
              

              <?php if ($this->authentication->is_pelayanan()) echo "
              <div class=\"row\">
                <a href=\"".base_url()."index.php/site/createschedule\">
                  <input type=\"button\" value=\"Buat Jadwal Baru\" class=\"button green\">
                </a>
              </div>
              "; ?>

              <div class="row">
                <div class="message message-green">
                  <p class="p_message">Jadwal berhasil dihapus! </p>
                </div>
              </div>

              <div class="">
                <div class="table_box">
                  <h2>Jadwal Praktik Dokter Umum</h2>
                  <table class="doctor">
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="last even-col">Waktu Akhir</th>
						<?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_doctor">
                      
					  
					  <?php
						$ii=0;
						foreach ($doctor_table_umum->result() as $entry) :
						$ii++;	
						?>
						<tr class="inner-table">
                        <td class="first"><?php if($entry->hari==1) echo "Senin";
						else if($entry->hari==2) echo "Selasa";
						else if($entry->hari==3) echo "Rabu";
						else if($entry->hari==4) echo "Kamis";
						else if($entry->hari==5) echo "Jumat";
						else if($entry->hari==6) echo "Sabtu";?></td>
                        <td><?php echo $entry->nama; ?></td>
                        <td><?php echo $entry->waktuawal; ?></td>
						<td><?php echo $entry->waktuakhir; ?></td>
                        <?php if ($this->authentication->is_pelayanan()){ ?>
                        
                          <td class=\"last\"><div class=\"row table_button_group\">
                          <?php echo form_open("site/deleteschedule/".$entry->jadwalmingguanid, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
						  <?php echo anchor("site/editschedule/".$entry->jadwalmingguanid,'<input type="button" value="Ubah" class="button orange table_button">');?>                           						  
						  <a href="http://localhost/ci/index.php/site/deletesuccess"><input type="submit" value="Hapus" class="button red table_button"/></a>
						  <?php echo form_close(); ?>
						  </td>
							<?php 
						}
						?>	
                      </tr>
                      <?php endforeach; ?>
					  
                      
                    </tbody>
                  </table>
                  <h2>Jadwal Praktik Dokter Gigi</h2>
                  <table class="doctor">
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="last even-col">Waktu Akhir</th>
						<?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      
					  
					  <?php
						$ii=0;
						foreach ($doctor_table_gigi->result() as $entry) :
						$ii++;	
						?>
						<tr class="inner-table">
                        <td class="first"><?php if($entry->hari==1) echo "Senin";
						else if($entry->hari==2) echo "Selasa";
						else if($entry->hari==3) echo "Rabu";
						else if($entry->hari==4) echo "Kamis";
						else if($entry->hari==5) echo "Jumat";
						else if($entry->hari==6) echo "Sabtu";?></td>
                        <td><?php echo $entry->nama; ?></td>
                        <td><?php echo $entry->waktuawal; ?></td>
						<td><?php echo $entry->waktuakhir; ?></td>
                        <?php if ($this->authentication->is_pelayanan()){ ?>
                        
                          <td class=\"last\"><div class=\"row table_button_group\">
                          <?php echo form_open("site/deleteschedule/".$entry->jadwalmingguanid, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
						  <?php echo anchor("site/editschedule/".$entry->jadwalmingguanid,'<input type="button" value="Ubah" class="button orange table_button">');?>                           						  
						  <a href="http://localhost/ci/index.php/site/deletesuccess"><input type="submit" value="Hapus" class="button red table_button"/></a>
						  <?php echo form_close(); ?>
						  </td>
							<?php 
						}
						?>	
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                  <h2>Jadwal Praktik Dokter Estetika Medis</h2>
                  <table class="doctor">
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="last even-col">Waktu Akhir</th>
						<?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      
                        <?php
						$ii=0;
						foreach ($doctor_table_medis->result() as $entry) :
						$ii++;	
						?>
						<tr class="inner-table">
                        <td class="first"><?php if($entry->hari==1) echo "Senin";
						else if($entry->hari==2) echo "Selasa";
						else if($entry->hari==3) echo "Rabu";
						else if($entry->hari==4) echo "Kamis";
						else if($entry->hari==5) echo "Jumat";
						else if($entry->hari==6) echo "Sabtu";?></td>
                        <td><?php echo $entry->nama; ?></td>
                        <td><?php echo $entry->waktuawal; ?></td>
						<td><?php echo $entry->waktuakhir; ?></td>
                        <?php if ($this->authentication->is_pelayanan()){ ?>
                        
                          <td class=\"last\"><div class=\"row table_button_group\">
                          <?php echo form_open("site/deleteschedule/".$entry->jadwalmingguanid, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
						  <?php echo anchor("site/editschedule/".$entry->jadwalmingguanid,'<input type="button" value="Ubah" class="button orange table_button">');?>                           						  
						  <a href="http://localhost/ci/index.php/site/deletesuccess"><input type="submit" value="Hapus" class="button red table_button"/></a>
						  <?php echo form_close(); ?>
						  </td>
							<?php 
						}
						?>	
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>