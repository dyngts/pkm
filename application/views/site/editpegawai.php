            <!-- FORM BEGIN -->
            <div class="container">
              <div class="profil">
                  <!--<form action="<?php echo base_url(); ?>index.php/site/editpegawaisukses" onsubmit="return confirm('Apakah Anda yakin ? \n\nTekan &#34;Yes&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.');"> -->
                    
					<fieldset>
                        <h2>FORM PEGAWAI</h2>

						<?php 
						
						foreach ($row -> result() as $r) :
						
							$id = $r->id;
							$username = $r->username;
							$password = $r->password_pegawai;
							$nama = $r->nama;
							$jenisKelamin = $r->jeniskelamin;
							$golDarah = $r->goldarah;
							$kewarga = $r->kewarganegaraan;
							$tmpLahir = $r->tempatlahir;
							$tglLahir = $r->tanggallahir;
							$agama = $r->agama;
							$alamat = $r->alamat;
							$status = $r->statuspernikahan;
							$peran = $r->peran;
							$noTelp = $r->no_telepon;
						endforeach;
						
						$attributes = array( 'id'=> $id);
						echo form_open('site/updatepegawai'.'/'.$id); 
						?>
                        <div class="row">
                          <div class="label">
                            <label>ID Pegawai<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="id" value="<?php echo $id;?>" class="input_form" id="name_form" disabled="disabled">
                          </div>
                          <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                      <div class="row">
                        <div class="label">
                          <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <input type="text" name="nama" value="<?php echo $nama;?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                          <span><label>Masukkan nama lengkap sesuai dengan kartu identitas.</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>No telp<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p>
                          <input type="text" name="no_telepon" value="<?php echo $noTelp;?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                          <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <textarea name ="alamat" class="input_form address_form"><?php echo $alamat;?></textarea>
                        </div>
                        <div class="note">
                          <span><label>Masukkan alamat sesuai dengan kartu identitas</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="row">
                          <div class="label">
                            <label>Tempat, Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form" id="p_birthday">:</p> 
                            <div id="place" class="input">
                              <input type="text" value="<?php echo $tmpLahir;?>"id="birth_place_form" class="input_form" name="tempatLahir">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input input_birthday"> 
                            <p class="p_form p_birthday">Hari<br/><em class="english">Day</em></p> 
                            <select name ="tgl" size="1" class="birthday_form dropdown_form">
							  <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="3">.</option>
                              <option value="3">.</option>
                              <option value="3">.</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                            </select>
                            <p class="p_form p_birthday">Bulan<br/><em class="english">Month</em></p> 
                            <select name ="bulan" size="1" class="birthday_form dropdown_form">
							
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="3">.</option>
                              <option value="3">.</option>
                              <option value="3">.</option>
                              <option value="3">11</option>
                              <option value="3">12</option>
                            </select>
                            <p class="p_form p_birthday">Tahun<br/><em class="english">Year</em></p> 
                            <select name ="tahun" size="1" class="birthday_form dropdown_form">
                              <option value="1900">1990</option>
                              <option value="1991">1991</option>
                              <option value="1992">1992</option>
                              <option value="1993">.</option>
                              <option value="1994">.</option>
                              <option value="1992">.</option>
                              <option value="1992">2009</option>
                              <option value="1992">2010</option>
                            </select>
                          </div>
                          <div class="note">
                            <span><label>Masukkan tempat dan tanggal lahir.</label></span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                        </div>
                       <div class="input">
                          <p class="p_form">:</p> 
                          <input type="radio" value="male" name="sex" class="input_form sex_form" checked="checked"> Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                          <input type="radio" value="female" name="sex" class="input_form sex_form"> Perempuan
                        </div>
                        <div class="note">
                          <span><label>Pilih jenis kelamin.</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <select name = "agama" size=1 class="" id="blood_form">
						<option  value="Islam">Islam</option>
						<option  value="Katolik">Katolik</option>
						<option  value="Pretestan">Protestan</option>
						<option  value="Hindu">Hindu</option>
						<option  value="Buddha">Buddha</option>
                          </select>
                        </div>
                        <div class="note">
                          <span><label>Pilih Agama.</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <select name ="status" size="1" class="dropdown_form">
                            <option value="Belum Menikah">Belum menikah</option>
                            <option value="Menikah">Menikah</option>
                          </select>
                        </div>
                        <div class="note">
                          <span><label>Pilih status pernikahan</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <input type="text" name="kewarga" value="<?php echo $kewarga; ?>" class="input_form">
                        </div>
                        <div class="note">
                          <span><label>Pilih kewarganegaraan</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">:</p> 
                          <select name ="golDarah"size="1" class="dropdown_form">
                              <option  value="Tidak Tahu">Tidak Tahu</option>
							  <option  value="A">A</option>
							  <option  value="B">B</option>
							  <option  value="AB">AB</option>
							  <option  value="O">O</option>
                          </select>
                        </div>
                        <div class="note">
                          <span><label>Pilih golongan darah.</label></span>
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label>Peran Pegawai<strong class="required">*</strong><br/><em class="english">Role</em></label>
                        </div>
                         <div class="input">
                            <p class="p_form">:</p> 
                            <select name ="peran" size=1 class="" id="pegawaiPkm_form" disabled="disabled">
                              <option value="<?php echo $peran;?>"><?php echo $peran;?></option>

                            </select>
                        </div>
                        <div class="note">
                          <span><label>Pilih peran pegawai.</label></span>
                        </div>
                      </div>

					  <div class="row">
                          <div class="label">
                            <label>Username <strong class="required">*</strong><br/><em class="english">Username</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="username" value="<?php echo $username;?>" class="input_form" id="name_form">
                          </div>
                          <div class="note">
                            <span><label>Masukkan username baru.</label></span>
                          </div>
                       </div>
					  <!--
					  <div class="row">
                          <div class="label">
                            <label>Password <strong class="required">*</strong><br/><em class="english">Password</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="password" value="<?php //echo $password;?>" class="input_form" id="name_form">
                          </div>
                          <div class="note">
                            <span><label>Masukkan password baru.</label></span>
                          </div>
                       </div> -->
					  
					  
                      <div class="row">
                        <div class="label">
                          <label>Foto<strong class="required">*</strong><br/><em class="english">Photograph</em></label>
                        </div>
                        <div class="input">
                          <input type="file" name="" value="" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                          <span><label>Upload foto Anda.</label></span>
                        </div>
                        <div class="image">
                          <img src="<?php base_url();?>image/4.png" alt="foto profil">
                        </div>
                      </div>

                      <div class="row">
                        <div class="label">
                          <label><strong class="required">*</strong>Wajib diisi.</label>
                        </div>
                      </div>

                      <!-- WHITESPACE -->
                      <div class="whitespace"></div>

                      <div class="row">
                        <div class="row" id="submit">
                          <a href="<?php echo base_url(); ?>index.php/site/profilepegawai"><input type="button" value="cancel" class="button orange submit_button"></a>
                          <?php echo anchor("site/updatepegawai/".$id,'<input type="submit" value="submit" class="button green submit_button">');?>
                        </div>
                      </div>
                    </fieldset>
                  </form>
              </div>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>