<article class="faq">
	<p class="bold">Q: Apa itu PKM?</p>
	<p>A: silahkan lihat <a href="<?php echo site_url("site/about"); ?>" class="blue">disini</a></p>

	<p class="bold">Q: Apakah bisa mendaftar anggota pasien melalui Website?</p>
	<p>A: Bisa.</p>

	<p class="bold">Q: Bagaimana cara untuk mendaftarkan sebagai pasien pkm melalui website?</p>
	<p>A: Bagi mahasiswa UI dan karyawan UI, lakukan login dengan akun JUITA anda. Lalu anda akan diminta untuk melengkapi form data identitas yang diperlukan untuk pihak pkm.
Bagi masyarakat umum, anda cukup menekan tombol registrasi. Website akan menampilkan form pengisian identitas yang dibutuhkan pihak PKM.
Yang perlu diperhatikan adalah anda memilih peran yang benar, karena setiap peran akan berbeda form pengisian. Setelah form diisi dengan lengkap, tekan tombol submit. Kemudian anda datang ke PKM untuk melakukan verifikasi mengenai pasien terkait. Setelah diverifikasi, pasien umum akan diberikan akun website pkm untuk dapat melakukan login. Setelah itu maka anda dapat melakukan pengobatan di PKM.</p>

	<p class="bold">Q: Apakah bisa mendaftar anggota secara langsung di PKM?</p>
	<p>A: Tentu saja bisa. Namun alangkah lebih baik anda telah melakukan registrasi online untuk mengisi data identitas agar saat berkunjung ke pkm, petugas pkm cukup memverifikasi data identitas yang telah dibuat sehingga waktu registrasi lebih singkat.</p>

	
	<p class="bold">Q: Apakah pada website pkm terdapat info banyaknya antrian saat ini?</p>
	<p>A: Ya. Info antrian ada pada bagian kanan atas halaman website pkm.</p>

	<p class="bold">Q: Apakah saya dapat melakukan antrian melalui website? Bagaimana cara melakukan antrian online?</p>
	<p>A: Anda dapat melakukan antrian online. Anda cukup melakukan login kedalam website PKM lalu di sebelah kanan atas akan muncul tombol "Antre". Tekan tombol tersebut, maka akan tampil halaman antri. Anda dapat memilih untuk antrian berobat poli umum atau poli gigi.</p>

	<p class="bold">Q: Kapan saya dapat melakukan antrian online?</p>
	<p>A: Antrian online hanya dapat dilakukan pada hari pengobatan saat ini. Anda tidak dapat melakukan antrian untuk hari esoknya seperti melakukan reservasi.</p>

	<p class="bold">Q: Bagaimana cara untuk melihat jadwal praktik dokter?</p>
	<p>A: Untuk melihat jadwal dokter tertentu, anda dapat melihatnya pada menu Jadwal Praktik Dokter. Untuk melihat jadwal dokter saat ini dapat dilihat pada menu Antre.</p>

	<p class="bold">Q: Apakah saya dapat mengubah data diri?</p>
	<p>A: Ya. Namun hanya beberapa data diri saja yang dapat diubah.</p>
	<div class="whitespace"></div>
</article>