            <!-- BEGIN -->
            <div class="container">
             
              <div class="profil">
                <div class="leftside">

                  <h2><?php echo $title;?></h2>
                  <!-- 
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Data berhasil disimpan! </p>
                      <p class="p_message">Anda akan mendapatkan ID pasien setelah diverifikasi.</p>
                      <p class="p_message p_message_eng">Success!</p>
                    </div>
                  </div>
                   -->

                  <?php
                  
                    $role = $query['otorisasi_id'];
                    $rolename = $query['jabatan'];

                    $username = $query['username'];
                    $name = $query['nama'];
                    $sex = $query['jenis_kelamin'];
                    if ($sex == "L") {
                      $sex_text = "Laki-laki";
                    }
                    else $sex_text = "Perempuan";
                    $phone = $query['no_telepon'];
                    $address = $query['alamat'];
                    $photo = $query['foto'];
                    if ($role <= 3) $photo_folder = "pasien"; else $photo_folder = "pegawai";
                    $photo_url = base_url("foto/".$photo_folder."/".$photo);

                    if ($role <= 3) {
                      $id = $query['id'];
                      $blood = $query['gol_darah'];
                      $nation = $query['kewarganegaraan'];
                      $birthplace = $query['tempat_lahir'];
                      $birthday = $query['tanggal_lahir'];
                      $religion = $query['agama'];
                      $marital = $query['status_pernikahan'];
                      $identitas = $query['no_identitas'];
                      $jurusan = $query['jurusan'];
                      $tahunmasuk = $query['tahun_masuk'];
                      $lembaga = $query['lembaga_fakultas'];
                      $status_aktif = $query['status_verifikasi'];
                    }
                    else {
                      $id = $query['nomor'];
                      $email = $query['email'];
                      $status_aktif = $query['status_aktif'];
                    }
                  ;?>

                  <div>
                  <?php if (isset($msg2)) echo $msg2;?>
                  <?php { 
                    if(!($status_aktif == 'f' || $this->authentication->is_baru()||$this->authentication->is_mendaftar())) echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>ID<br/><em class=\"english\">ID</em></label>
                      </div>
                        <p class=\"p_form\">: ".$id."</p>
                    </div>";
                    if ($role > 3)
                    echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Username<br/><em class=\"english\">Username</em></label>
                      </div>
                        <p class=\"p_form\">: ".$username."</p>
                    </div>";

                    echo "

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Nama Lengkap<br/><em class=\"english\">Full Name</em></label>
                      </div>
                        <p class=\"p_form\">: ".$name."</p>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Peran<br/><em class=\"english\">Role</em></label>
                      </div>
                        <p class=\"p_form\">: ".$rolename."</p>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                          <label>Nomor Telepon<br/><em class=\"english\">Phone Number</em></label>
                        </div>
                          <p class=\"p_form\">: ".$phone."</p>
                    </div>
                    
                    <div class=\"row\">
                        <div class=\"label\">
                          <label>Alamat<br/><em class=\"english\">Address</em></label>
                        </div>
                          <p class=\"p_form\">: ".$address."</p>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jenis Kelamin<br/><em class=\"english\">Sex</em></label>
                      </div>
                        <p class=\"p_form\">: ".$sex_text."</p>
                    </div>

                    ";}?>

                    <?php if ($role > 3) {
                      echo "
                        <div class=\"row\">
                          <div class=\"label\">
                            <label>Email<br/><em class=\"english\">Email</em></label>
                          </div>
                            <p class=\"p_form\">: ".$email."</p>
                        </div>";
                    }?>

                    <?php if ($role <= 3) {

                      if ($role == 3) { echo "
                        
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Nomor Identitas<br/><em class=\"english\">ID Number</em></label>
                        </div>
                          <p class=\"p_form\">: ".$identitas."</p
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Pekerjaan<br/><em class=\"english\">Occupation</em></label>
                        </div>
                          <p class=\"p_form\">: ".$lembaga."</p>
                      </div>
                      "; }

                      else if ($role == 2) { echo "

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Lembaga<br/><em class=\"english\">Institution</em></label>
                        </div>
                          <p class=\"p_form\">: ".$lembaga."</p>
                      </div>
                      "; }

                      else if ($role == 1) { echo "
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>NPM<br/><em class=\"english\">NPM</em></label>
                        </div>
                          <p class=\"p_form\">: ".$identitas."</p
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Fakultas<br/><em class=\"english\">Faculty</em></label>
                        </div>
                          <p class=\"p_form\">: ".$lembaga."</p>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Jurusan<br/><em class=\"english\">Majors</em></label>
                        </div>
                          <p class=\"p_form\">: ".$jurusan."</p>
                      </div>
                    ";}

                      echo "
                      <div class=\"row\">
                          <div class=\"label\">
                            <label> Tempat, Tanggal Lahir<br/><em class=\"english\">Place and date of birth </em></label>
                          </div>
                            <p class=\"p_form\">: ".$birthplace.", ".$birthday."</p>
                      </div>
                      
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Agama<br/><em class=\"english\">Religion</em></label>
                        </div>
                          <p class=\"p_form\">: ".$religion."</p>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Status Pernikahan<br/><em class=\"english\">Marital Status</em></label>
                        </div>
                          <p class=\"p_form\">: ".$marital."</p>
                      </div>
          
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Kewarganegaraan <br/><em class=\"english\">Nationality</em></label>
                        </div>
                          <p class=\"p_form\">: ".$nation."</p>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Golongan Darah<br/><em class=\"english\">Bloodtype</em></label>
                        </div>
                          <p class=\"p_form\">: ".$blood."</p>
                      </div>";
                    
                    }?>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">

                        <?php if ($show_button) {
                            echo "&nbsp;&nbsp;";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/edit/$id")."><input type=\"button\" value=\"Ubah Data\" class=\"button orange submit_button\"></a>";
                            if($role > 3 || ALLOW_REGISTER) echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/editpassword/$id")."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a>";
                            
                            //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            //<form style=\"display:inline\" action=".site_url("site/deletepasien/$row->id")."  method=\"post\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.')\"><input type=\"submit\" value=\"Hapus Pasien\" class=\"button red table_button\" /></form>" ;
   
                          if($this->authentication->is_loket() && $this->authentication->get_my_id() != $id && ALLOW_REGISTER) {
                            echo "&nbsp;&nbsp;&nbsp;".form_open_multipart('akun/generate_password/'.$id, array('style'=>'display:inherit','onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                            echo "<button type=\"submit\" class=\"button orange\">Generate Password</button></form>";
                          }
                        }?>

                        <?php if ($show_button_verifikasi) {
                            echo "&nbsp;&nbsp;";
                            echo "&nbsp;&nbsp;&nbsp;".form_open_multipart('akun/delete/'.$id, array('style'=>'display:inherit','onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); 
                            echo "<button type=\"submit\" class=\"button red\">Hapus</button></form>";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/edit/$id")."><input type=\"button\" value=\"Ubah Data\" class=\"button orange submit_button\"></a>";
                            echo "&nbsp;&nbsp;&nbsp;".form_open_multipart('akun/verify/'.$id, array('style'=>'display:inherit','onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); 
                            echo "<button type=\"submit\" class=\"button green\">Verifikasi</button></form>";
                            //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            //<form style=\"display:inline\" action=".site_url("site/deletepasien/$row->id")."  method=\"post\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.')\"><input type=\"submit\" value=\"Hapus Pasien\" class=\"button red table_button\" /></form>" ;
                        }?>
                    </div>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo $photo_url;?>" alt="tanpa foto" style="max-height:264px; max-width:198px;">
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>

            <script type="text/javascript">
              function conf() {
                confirm('Apakah Anda yakin untuk melanjutkan?\n\nTekan "OK" untuk lanjut, "Cancel" untuk batal.');
              }
            </script>
            