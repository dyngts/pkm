<?php 
	$role = $this->session->userdata('role');
	$access = 0;
	switch($role) {
		case 'Koordinator Pelayanan': $access = 1;break;
		case 'Koord. Adm. dan Keuangan': $access = 2;break;
		case 'Staf Keuangan': $access = 2;break;
		case 'Perawat Gigi': $access = 1;break;
		case 'Perawat Umum': $access = 1;break;
		case 'Penanggung Jawab':$access = 3; break;
		case 'Apoteker' : $access = 1; break;
		case 'Asisten Apoteker' : $access = 1; break;
	}
	$kategori = preg_replace('/^\s+|\n|\r|\s+$/m', '', form_dropdown('kategori', $kategori, '', 'id="kategori"'));
?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#bulanhari").change(function(){
			showDiv();
		  });
		  showDiv();
	});

	// $(document).ready(function() {
	// 	$( "#datepicker" ).datepicker();
	// });

	function showDiv() {
		 // hide any showing
	  $(".pilihan_div").each(function(){
		  $(this).css('display','none');
	  });

	  // our target
	  var target = "#pilihan_" + $("#bulanhari").val();                                                                                        
	  var target2 = "#pilihan_bulanan_poli";
	  var target3 = '#pilihan_tahun';
	  var e =  document.getElementById("bulanhari");  
	  var value = e.options[e.selectedIndex].value;

	  if( $(target).length > 0 && value == "bulanan") {
			$(target).css('display','block');
			$(target2).css('display','block');
			$(target3).css('display','block');
	  }
	  else $(target).css('display','block');

	}

	$(function() {
		$("#jenis_laporan").change(function(){
			var jenis_laporan = document.getElementById("jenis_laporan").value;
			if(jenis_laporan == "laporan_stok_obat") {
				$('.isi').html('<id id="form_kat"><label>Kategori Obat</label><?php echo $kategori; ?></id>');
			}
			else {
				$('.isi').html('');
			}
		});
	});

	function cek_inputs() {

		var jenis_laporan = document.getElementById('jenis_laporan');
		var month = document.getElementById('month');
		var year = document.getElementById('year');
		var kategori = document.getElementById('kategori');



		if(jenis_laporan.value == '') {
			alert('Jenis laporan harus dipilih');
			return false;
		}

		if(kategori != null) {
			if(kategori.value == '' && jenis_laporan.value == 'laporan_stok_obat') {
				alert('Kategori obat harus dipilih');
				return false;
			}
		}
		

		if(month.value == '') {
			alert('Bulan harus dipilih');
			return false;
		}

		if(year.value == '') {
			alert('Tahun harus dipilih');
			return false;
		}

	}

</script>

<?php 
	$arraybulan = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
	date_default_timezone_set('Asia/Jakarta');
	$tahun_skrg = date("Y");
	$bulan_skrg = date("n");
	$hari_skrg = date("j");
?>
			<!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
              <div class="block">
                <div>
                  <h1>LAPORAN</h1>
                </div>
                <?php echo $msg;
                  
                  // $datestring = "%Y %m %d";
                  // $time = time();

                  // echo mdate($datestring, $time);
                  
                ?>
                <?php if (ENABLE_ANTREAN) { ?>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/laporanpoli/umum");?>">
                    <div class="statslabel">
                      <label>Laporan Kunjungan Poli Umum.<em class="english"></em></label>
                    </div>
                    <div class="block">
                      <div class="margin_left">
                        <!-- <div class="input">
                          <select name="u_lokasi" size=1 class="dropdown_form">
                              <option value="">- Pilih Lokasi -</option>
                              <option value="DEPOK">Depok</option>
                          </select>
                        </div> -->
                        <div class="input">
                          <select name="u_bulan" size=1 class="dropdown_form">
														<option value="">- Pilih Bulan -</option>
														<?php 
															for($i = 1; $i <= 12; $i++) { ?>
																<option value = "<?php echo $i; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
														<?php } ?>
                          </select>
                        </div>
                        <div class="input">
													<?php echo form_dropdown('u_tahun', $year, $tahun_skrg, 'id="tahun"'); ?>
                          <!-- <select id = 'tahun' name = 'u_tahun'>
														<option value="">- Pilih Tahun -</option>
														<?php 
															$tahun_skrg = date("Y");
															for($i = '2013'; $i <= $tahun_skrg; $i++){
																echo '<option value = '.$i.'>'.$i.'</option>';
															}
														?>
													</select>-->
                        </div>
                        <div class="block">
                          <input type="submit" value="Unduh Laporan" class="button-laporan button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/laporanpoli/gigi");?>">
                    <div class="statslabel">
                      <label>Laporan Kunjungan Poli Gigi.<em class="english"></em></label>
                    </div>
                    <div class="block">
                      <div class="margin_left">
                        <div class="input">
                          <select name="g_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
														<?php 
															for($i = 1; $i <= 12; $i++) { ?>
																<option value = "<?php echo $i; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
														<?php } ?>
                          </select>
                        </div>
                        <div class="input">
													<?php echo form_dropdown('g_tahun', $year, $tahun_skrg, 'id="tahun"'); ?>
                          <!-- <select id = 'tahun' name = 'g_tahun'>
														<option value="">- Pilih Tahun -</option>
														<?php 
															$tahun_skrg = date("Y");
															for($i = '2013'; $i <= $tahun_skrg; $i++){
																echo '<option value = '.$i.'>'.$i.'</option>';
															}
														?>
													</select> -->
                        </div>
                        <div class="block">
                          <input type="submit" value="Unduh Laporan" class="button-laporan button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>

				

                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/umur");?>">
                    <div class="statslabel">
                      <label>Laporan Kunjungan Berdasarkan Umur.<em class="english"></em></label>
                    </div>
                    <div class="block">
                      <div class="margin_left">
                        <div class="input">
                          <select name="a_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
														<?php 
															for($i = 1; $i <= 12; $i++) { ?>
																<option value = "<?php echo $i; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
														<?php } ?>
                          </select>
                        </div>
                        <div class="input">
													<?php echo form_dropdown('a_tahun', $year, $tahun_skrg, 'id="tahun"'); ?>
                          <!-- <select name="a_tahun" size=1 class="dropdown_form">
														<option value="">- Pilih Tahun -</option>
														<?php 
															$tahun_skrg = date("Y");
															for($i = '2013'; $i <= $tahun_skrg; $i++){
																echo '<option value = '.$i.'>'.$i.'</option>';
															}
														?>
                          </select> -->
                        </div>
                        <div class="block">
                          <input type="submit" value="Unduh Laporan" class="button-laporan button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <?php } ?>
				<!--================================================================
				=====Laporan Keuangan- Cuma PJ PKM/Koordinator Pelayanan============
				=================================================================-->
				<?php if ($access == 1 || $access == 3) {?>
				<?php echo form_open("laporan/laporan_kesakitan"); ?>

					<div class = 'row inlineblock'>
						<div class="statslabel">
							<label>Laporan Penyakit.<em class="english"></em></label>
						</div>
						<div class="margin_left">
							<div class="input">
								<select id='laporan' name='jenislaporan'>
									 <!--<option value="data-kesakitan">Data Kesakitan</option>-->
									 <option value="">- Pilih Laporan Penyakit -</option>
									 <option value="data-kesakitan"> Data Kesakitan </option>
									 <option value="pola-penyakit"> Pola Penyakit </option>
									 <option value="tindakan-poli-gigi"> Tindakan Poli Gigi </option>
								</select>
							</div>
							<div class="input">
								<select id='bulan' name ='pilihbulan'>
									<option value="">- Pilih Bulan -</option>
										<?php 
											for($i = 1; $i <= 12; $i++) { ?>
												<option value = "<?php echo $i; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
										<?php } ?>
								</select>
							</div>
							
							<div class="input">
								<?php echo form_dropdown('pilihtahun', $year, $tahun_skrg, 'id="tahun"'); ?>
								<!-- <select id = 'tahun' name = 'pilihtahun'>
									<option value="">- Pilih Tahun -</option>
									<option value = '2013'>2013</option>
									<?php 
										$tahun_skrg = date("Y");
										for($i = 2013; $i<$tahun_skrg; $i++){
											echo "<option value = '$i'>".$i."</option>";
										}
									?>
								</select> -->
							</div>
							<input type="submit" value="Lihat Laporan" class="button-laporan button green table_button">
						</div>
					</div>
					<?php echo form_close(); ?>
				<div class = 'row inlineblock'>
					<div class="statslabel">
						<label>Laporan Kinerja.<em class="english"></em></label>
					</div>
					<?php echo form_open("laporan/lihat_laporan_kinerja"); ?>
					<div class="margin_left">
						<div class="input">
								<select id='bulan' name ='pilihbulan'>
									<option value="">- Pilih Bulan -</option>
										<?php 
											for($i = 1; $i <= 12; $i++) { ?>
												<option value = "<?php echo $i; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
										<?php } ?>
								</select>
							</div>
							<div class="input">
								<?php echo form_dropdown('pilihtahun', $year, $tahun_skrg, 'id="tahun"'); ?>
								<!-- <select id = 'tahun' name = 'pilihtahun'>
									<option value="">- Pilih Tahun -</option>
									<option value = '2013'>2013</option>
									<?php 
										$tahun_skrg = date("Y");
										for($i = 2013; $i<$tahun_skrg; $i++){
											echo "<option value = '$i'>".$i."</option>";
										}
									?>
								</select> -->
							</div>
						<div class="block">
							<input type="submit" value="Lihat Laporan" class="button-laporan button green table_button">
						</div>	
					</div>
				</div>
				<?php echo form_close(); ?>

				<form class="form" id="laporan_form" method = "post" onsubmit="return cek_inputs()" action = "<?php echo base_url('index.php/laporan_controller/proses_laporan'); ?>">
					<div class = 'row inlineblock'>
					<div class="statslabel">
						<label>Laporan Apotek.<em class="english"></em></label>
					</div>
						<div class="margin_left">
							<div class="input">
								<?php echo form_dropdown('jenis_laporan', $jenis_laporan, '', 'id="jenis_laporan"'); ?>
							</div>

							<id class="isi">
								
							</id>
							
							<div class="input">
								<?php echo form_dropdown('month', $month, $bulan_skrg, 'id="month"'); ?>
							</div>

							<div class="input">
								<?php echo form_dropdown('year', $year, $tahun_skrg, 'id="year"'); ?>
							</div>

						    <div class="block">
						    	<input name="submit" type="submit" value="Buat Laporan" class="button-laporan button green table_button" />
						    </div>
					    </div>
					</div>
				</form>
				<?php } ?>
				<!--================================================================
				=====Laporan Keuangan- Cuma PJ PKM/Koordinator Adm dan Keuangan=====
				=================================================================-->
				<?php if ($access == 2 || $access == 3) {?>
				<div class = 'row inlineblock'>
					<?php echo form_open("laporan/laporan_keuangan"); ?>
					<div class="statslabel">
						<label>Laporan Keuangan.<em class="english"></em></label>
					</div>
					<div class="margin_left">
						<div class="input">
							<select id='bulanhari' name='jenis' class='laporandd'>
							 <option value="bulanan"> Bulanan</option>
							 <option value='harian'> Harian</option>
							</select>
						</div>
						<div class="input">
							<div id = 'pilihan_bulanan' class='pilihan_div'>
							<select id='bulan' name ='pilihbulan' class='laporandd'>
								<option value="">- Pilih Bulan -</option>
										<?php 
											for($i = 1; $i <= 12; $i++) { ?>
												<option value = "<?php echo $arraybulan[$i]; ?>" <?php if($i == $bulan_skrg) { ?> selected = "selected" <?php } ?>><?php echo $arraybulan[$i]; ?></option>;
										<?php } ?>
							</select>
							</div>
						</div>
						<div class="input">
							<div id = 'pilihan_tahun' class = 'pilihan_div'>
								<?php echo form_dropdown('pilihtahun', $year, $tahun_skrg, 'id="tahun" class="laporandd"'); ?>
							<!-- <select id = 'tahun' name = 'pilihtahun' class='laporandd'>
								<option value = '2013'>2013</option>
								<?php 
									$tahun_skrg = date("Y");
									for($i = 2013; $i<$tahun_skrg; $i++){
										echo "<option value = '$i'>".$i."</option>";
									}
								?>
							</select> -->
							</div>
						</div>
						<div class="input">
							<div id = 'pilihan_bulanan_poli' class='pilihan_div'>
								<select id='poli' name ='pilihpoli' class='laporandd'>
								<option value="">- Pilih Poli -</option>
								<option value='Semua' selected="selected">Semua Poli</option>
								<option value="Umum"> Umum</option>
								<option value='Gigi'> Gigi</option>
								<option value='Estetika'> Estetika Medis</option>
								<option value='Farmasi'> Farmasi</option>
								<option value='Lab'> Laboratorium</option>
								<option value='Radiologi'> Radiologi</option>
								<option value='Ambulans'> Ambulans</option>
								<option value='Salemba'> Salemba</option>
								</select>
							</div>
						</div>
						<div class="input">
							<div id = 'pilihan_harian' class='pilihan_div'><label>Pilih hari yang diinginkan:</label>
								<input type="text" id="datepicker" name="tanggal" value="<?php echo date("d-m-Y"); ?>"/>
							</div>
						</div>
						<input type="submit" value="Lihat Laporan" class="button-laporan button green table_button">
					</div>
				</div>
				<?php } ?>
				
<!--
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/umur");?>">
                    <div class="statslabel">
                      <label>c. Laporan Kunjungan Berdasarkan Umur.<em class="english"></em></label>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
