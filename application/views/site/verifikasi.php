            <div class="container">
                                
                <?php
                //$this->load->model('antrian_model');
                $jmllast = $this->antrian_model->get_jml_antrian();
                $jmll = $jmllast[0]+$jmllast[1];
                $data = array(
                'clicked' => true,
                'jml_last' => $jmll,
                'flush' => true,
              );

              $this->session->set_userdata($data);
            ?>
              <!-- <div class="row" id="pesan_sukses" style="display:none">
                <div class="message message-green">
                  <p class="p_message">Antrean berhasil ditutup </p>
                </div>
              </div>
              <div class="row" id="pesan_sukses2" style="display:none">
                <div class="message message-green">
                  <p class="p_message">Pasien berhasil ditambahkan ke dalam antrean </p>
                </div>
              </div> -->

              <div class="whitespace"></div>

              <!-- Mulai dari Pengecekan apakah antrian dibuka atau tidak-->
          <div class="row">
                <h1>Antrean -
                  <?php
                    date_default_timezone_set('Asia/Jakarta');
                    $datestring = "%d %m %Y";
                    $time = time();
                    $today = getdate();
                    if ($today['wday'] = 0) {
                      echo "Minggu, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } elseif ($today['wday'] = 1) {
                      echo "Senin, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } elseif ($today['wday'] = 2) {
                      echo "Selasa, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } elseif ($today['wday'] = 3) {
                      echo "Rabu, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } elseif ($today['wday'] = 4) {
                      echo "Kamis, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } elseif ($today['wday'] = 5) {
                      echo "Jumat, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    } else {
                      echo "Sabtu, ".$today['mday']."-".$today['mon']."-".$today['year']."</h2>";
                    }
                  ?>
                </h1>
              </div>

          <?php
            /*
              echo "<div class=\"row center\">";
              echo "<h2>Registrasi Berobat Sedang Tutup</h2>";
              echo form_open('antrian/buka_registrasi_berobat', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
              echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Buka Antrian\" class=\"button green\" onclick=\"\">";
              echo form_close();
              echo "</div>";
            */
          ?>
                
          <!-- Tutup dari Pengecekan apakah antrian dibuka atau tidak-->


                <!-- Form tambah Antrian-->
                <?php if($this->authentication->is_loket()){ ?>
                
                <?php echo $msg;?>
              <?php echo form_open('antrian/tambah_antrian', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));?>
              <h2>Tambah Antrean Pasien</h2>
              <div class="row">
                <div class="label">
                  <label>Tambah Pasien Berobat<strong class="required">*</strong><br><em class="english">Add Registration</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p>
                  <input type="text" name="id_pasien" value="" class="input_form" id="name_form">
                </div>
                <div class="note">
                  <span><label>Masukkan ID pasien PKM.</label></span>
                </div>
              </div>

              <div class="row">
                <div class="label">
                  <label>Pilih Jadwal<strong class="required">*</strong><br><em class="english">Add Schedule</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p>
                  <?php
                  date_default_timezone_set('Asia/Jakarta');
                  $today = getdate();
                  echo "<select size=\"1\" class=\"dropdown_form\" name=\"jadwal\">";
                        echo "<optgroup label=\"".$doctor_table_umum[0]['l_nama']."\">";
                      if (isset($doctor_table_umum)&&($doctor_table_umum!=NULL)) {
                        foreach ($doctor_table_umum as $row) {
                          if ($row['hari'] == $today['wday']) {
                            echo "<option value=\"".$row['id_mingguan']."\">".$row['p_nama']." -  (".$row['waktu_aktual_awal']." s/d ".$row['waktu_aktual_akhir'].")</option>";
                          }
                        }
                      }
                  else echo "<option value=\"\">Jadwal tidak tersedia</option>";

                      echo "</optgroup>";
                        echo "<optgroup label=\"".$doctor_table_gigi[0]['l_nama']."\">";
                      if (isset($doctor_table_gigi)&&($doctor_table_gigi!=NULL)) { 
                        foreach ($doctor_table_gigi as $row) {
                          if ($row['hari'] == $today['wday']) {
                            echo "<option value=\"".$row['id_mingguan']."\">".$row['p_nama']." -  (".$row['waktu_aktual_awal']." s/d ".$row['waktu_aktual_akhir'].")</option>";
                          }
                        }
                      }
                  else echo "<option value=\"\">Jadwal tidak tersedia</option>";
                  echo "</optgroup>";
                  echo "</select>";
                  ?>
                  
                  <!---->
                </div>
                <div class="note">
                    <span><label>Pilih Jadwal</label></span>
                </div>

                <div class="row">
                  <input type="submit" value="Tambah Antrian" class="button green"/>
                </div>
                <?php echo form_close();?>
              </div>
               <?php } ?>
                  <!-- End of Form tambah Antrian-->
              

              <div class="whitespace"></div>
              <!-- Tabel Status Klinik-->
              <?php if($this->authentication->is_loket()){ ?>
              <div class="table_box">
                  <h2>Status Registrasi Berobat Pasien</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Klinik</th>
                        <th class="even-col">Status</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=3 class="last"></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php
                              echo "<tr class=\"odd-row inner-table\">";
                                echo "<td class=\"center\">Poli Umum</td>";
                                echo "<td class=\"center\">";
                                if ($buka_umum == 't') {
                                  echo "Antrian Telah Dibuka";
                                } else {
                                  echo "Antrian Telah Ditutup";
                                }
                                echo "</td>";
                                echo "<td class=\"center\">";
                                  if ($buka_umum == 't') {
                                    echo form_open('antrian/tutup_registrasi_umum', array());
                                    echo "<input id=\"\" type=\"submit\" value=\"Tutup Antrian\" class=\"table-button red\" onclick=\"\">";
                                    echo form_close();
                                  } else {
                                    echo form_open('antrian/buka_registrasi_umum', array());
                                    echo "<input id=\"\" type=\"submit\" value=\"Buka Antrian\" class=\"table-button green\" onclick=\"\">";
                                    echo form_close();
                                  }
                                echo "</td>";
                              echo "</tr>";
                              echo "<tr class=\"odd-row inner-table\">";
                                echo "<td class=\"center\">Poli Gigi</td>";
                                echo "<td class=\"center\">";
                                if ($buka_gigi == 't') {
                                  echo "Antrian Telah Dibuka";
                                } else {
                                  echo "Antrian Telah Ditutup";
                                }
                                echo "</td>";
                                echo "<td class=\"center\">";
                                  if ($buka_gigi == 't') {
                                    echo form_open('antrian/tutup_registrasi_gigi', array());
                                    echo "<input id=\"\" type=\"submit\" value=\"Tutup Antrian\" class=\"table-button red\" onclick=\"\">";
                                    echo form_close();
                                  } else {
                                    echo form_open('antrian/buka_registrasi_gigi', array());
                                    echo "<input id=\"\" type=\"submit\" value=\"Buka Antrian\" class=\"table-button green\" onclick=\"\">";
                                    echo form_close();
                                  }
                                echo "</td>";
                              echo "</tr>";
                      ?>
                    </tbody>
                  </table>
                </div>
                <?php } ?>
                <!-- End of Tabel Status Klinik-->


              <div class="inline">
                <div class="row inline">
                  <?php
                  /*
                  $disable = "";
                  if (!$status_umum && !$status_gigi) {
                    $disable = "disabled=\"disabled\"";
                  }
                    echo "<input ".$disable." value=\"Tambahkan Pasien ke Dalam Antrean\" type=\"submit\" class=\"button green table_button\">";
                  */
                  ?>
                </div>
              </div>
              <?php //echo form_close();?>

              <a class="link showhide" href=<?php echo site_url('antrian/toggle');?>>
                <button class="table-button orange"><?php if($hideselesai) echo "Tampilkan"; else echo "Sembunyikan" ;?> pasien yang selesai/batal berobat</button>
              </a>
            

              <!-- Tabel Verifikasi Antrian Umum -->
              <div class="">
                <div class="table_box">
                  <h2>Poli Umum</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID Pasien</th>
                        <th>Nama</th>
                        <th class="even-col">Status</th>
                        <th class="">Dokter Periksa</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=6 class="last"></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $ii = 0;
                          $umum = 0;
                        if (isset($query)&&$query!=NULL) {
                          foreach ($query as $row) {
                            $a = $row['status'];
                            if ($row['id_layanan'] == '1') {
                              $ii++;
                              if ($a == 1){
                                $stat = "Batal";
                              } else if($a == 2) {
                                $stat = "Belum Hadir";
                              } else if($a == 3) {
                                $stat = "Hadir";
                              } else if($a == 4) {
                                $stat = "Tunda";
                              } else if($a == 5) {
                                $stat = "Sedang Berobat";
                              } else if($a == 6) {
                                $stat = "Selesai";
                              } else {
                                $stat = "???";
                              }
                              echo "<tr id=u".$ii." class=\"";if(($ii%2)==1) echo "odd-row ";if($hideselesai && ($a == 6 || $a == 1)) echo "hide "; echo"inner-table\">";
                                echo "<td class=\"first\">".$row['no_urutan']."</td>";
                                echo "<td class=\"center\">".$row['id_pasien']."</td>";
                                echo "<td class=\"left\">".$row['nama']."</td>";
                                echo "<td class=\"center\">".$stat."</td>";
                                if ($a == 5) {
                                echo "<td class=\"center\">";
                                  echo form_open('antrian/ubah_dokter_periksa/'.$row['id'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                                      date_default_timezone_set('Asia/Jakarta');
                                      $today = getdate();
                                      echo "<select size=\"1\" class=\"dropdown_form1\" name=\"jadwal\">";
                                          if ($doctor_table_umum!=NULL) {
                                            foreach ($doctor_table_umum as $key) {
                                              if ($key['hari'] == $today['wday']) {
                                                echo $key['id_mingguan'];
                                                echo "<option value=\"".$key['id_mingguan']."\">".$key['p_nama']."</option>";
                                              }
                                            }
                                          }
                                      echo "</select>";
                                      echo "&nbsp;&nbsp;<input type=\"submit\" value=\"Ubah dokter & set selesai\" class=\"table-button green\">";
                                  echo form_close();
                                echo "</td>";
                                } elseif($a == 6) {
                                  echo "<td class=\"center\">".$this->user_model->get_user_name_by_id($row['nomor_petugas'])."</td>";
                                } else {
                                  echo "<td class=\"center\">(Belum Tersedia)</td>";
                                }
                                echo "<td class=\"center last\">";
                                echo "<div class=\"row text-left\">";
                                $set_text = "Set ";
                                  $value = ""; $value1 = "";$action = "";$action1 = ""; $disable = "";
                                  if ($a == 1) {
                                    $disable = "disabled=\"disabled\"";
                                  } else if ($a == 2) {
                                    $value = $set_text."Hadir";
                                    $action = "antrian/ubah_status/3/".$row['id'];
                                    $value1 = $set_text."Batal";
                                    $action1 = "antrian/ubah_status/1/".$row['id'];
                                  } else if($a == 3) {
                                    $value = $set_text."Tunda";
                                    $value1 = $set_text."Berobat";
                                    $action = "antrian/ubah_status/4/".$row['id'];
                                    $action1 = "antrian/ubah_status/5/".$row['id'];
                                  } else if($a == 4) {
                                    $value = $set_text."Berobat";
                                    $value1 = $set_text."Batal";
                                    $action = "antrian/ubah_status/5/".$row['id'];
                                    $action1 = "antrian/ubah_status/1/".$row['id'];
                                  } elseif ($a  == 5) {
                                    $value = $set_text."selesai";
                                    $action = "antrian/ubah_status/6/".$row['id'];
                                  }else {
                                    $disable = "disabled=\"disabled\"";
                                  }

                                  if ($a == 1) {
                                    echo "Pasien Batal Berobat";
                                  } elseif ($a == 2) {
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 3) {
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 4) {
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 5) {
                                    echo form_open($action, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                                      echo "<input type=\"submit\" value=\"".$value."\" class=\"table-button orange\">";
                                    echo form_close();
                                  } elseif ($a == 6) {
                                    echo "Pasien Selesai Berobat";
                                  }
                                echo "</div>";
                                echo "</td>";
                                
                              echo "</tr>";
                            $umum++;
                            }
                          }
                        }
                         if ($umum == 0) {
                          echo "
                        <tr class=\"inner-table\">
                          <td colspan=\"6\"class=\"first\">
                          -- Tidak Ada Antrian --</td>
                        </tr>
                          ";}
                      ?>
                    </tbody>
                  </table>
                  <!-- Ending Tabel Verifikasi Antrian Umum -->

              <!-- Tabel Verifikasi Antrian Gigi -->
              <!-- <div class="whitespace"></div> -->
                  <h2>Poli Gigi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID Pasien</th>
                        <th>Nama</th>
                        <th class="even-col">Status</th>
                        <th>Dokter Periksa</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=6 class="last"></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $ii = 0;
                          $gigi = 0;
                        if (isset($query)&&$query!=NULL) {
                          foreach ($query as $row) {
                            $a = $row['status'];
                            if ($row['id_layanan'] == '2') {
                              $ii++;
                              if ($a == 1){
                                $stat = "Batal";
                              } else if($a == 2) {
                                $stat = "Belum Hadir";
                              } else if($a == 3) {
                                $stat = "Hadir";
                              } else if($a == 4) {
                                $stat = "Tunda";
                              } else if($a == 5) {
                                $stat = "Sedang Berobat";
                              } else if($a == 6) {
                                $stat = "Selesai";
                              } else {
                                $stat = "???";
                              }
                              echo "<tr id=g".$ii." class=\"";if(($ii%2)==1) echo "odd-row ";if($hideselesai && ($a == 6 || $a == 1)) echo "hide "; echo"inner-table\">";
                                echo "<td class=\"first\">".$row['no_urutan']."</td>";
                                echo "<td class=\"center\">".$row['id_pasien']."</td>";
                                echo "<td class=\"center\">".$row['nama']."</td>";
                                echo "<td class=\"center\">".$stat."</td>";
                                if ($a == 5) {
                                echo "<td class=\"center\">";
                                  echo form_open('antrian/ubah_dokter_periksa/'.$row['id'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                                      date_default_timezone_set('Asia/Jakarta');
                                      $today = getdate();
                                      echo "<select size=\"1\" class=\"dropdown_form1\" name=\"jadwal\">";
                                          if ($doctor_table_gigi!=NULL) {
                                            foreach ($doctor_table_gigi as $key) {
                                              if ($key['hari'] == $today['wday']) {
                                                echo $key['id_mingguan'];
                                                echo "<option value=\"".$key['id_mingguan']."\">".$key['p_nama']."</option>";
                                              }
                                            }
                                          }
                                      echo "</select>";
                                      echo "&nbsp;&nbsp;<input type=\"submit\" value=\"Ubah dokter & set selesai\" class=\"table-button green\">";
                                  echo form_close();
                                echo "</td>";
                                } elseif($a == 6) {
                                  echo "<td class=\"center\">".$this->user_model->get_user_name_by_id($row['nomor_petugas'])."</td>";
                                } else {
                                  echo "<td class=\"center\">(Belum Tersedia)</td>";
                                }
                                echo "<td class=\"center last\">";
                                echo "<div class=\"row text-left\">";
                                $set_text = "Set ";
                                  $value = ""; $value1 = "";$action = "";$action1 = ""; $disable = "";
                                  if ($a == 1) {
                                    $disable = "disabled=\"disabled\"";
                                  } else if ($a == 2) {
                                    $value = $set_text."Hadir";
                                    $action = "antrian/ubah_status/3/".$row['id'];
                                    $value1 = $set_text."Batal";
                                    $action1 = "antrian/ubah_status/1/".$row['id'];
                                  } else if($a == 3) {
                                    $value = $set_text."Tunda";
                                    $value1 = $set_text."Berobat";
                                    $action = "antrian/ubah_status/4/".$row['id'];
                                    $action1 = "antrian/ubah_status/5/".$row['id'];
                                  } else if($a == 4) {
                                    $value = $set_text."Berobat";
                                    $value1 = $set_text."Batal";
                                    $action = "antrian/ubah_status/5/".$row['id'];
                                    $action1 = "antrian/ubah_status/1/".$row['id'];
                                  } elseif ($a  == 5) {
                                    $value = "Set selesai";
                                    $action = "antrian/ubah_status/6/".$row['id'];
                                  }else {
                                    $disable = "disabled=\"disabled\"";
                                  }

                                  if ($a == 1) {
                                    echo "Pasien Batal Berobat";
                                  } elseif ($a == 2) {
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 3) {
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 4) {
                                    echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value."\" class=\"table-button green\">
                                    </form>";
                                    echo "<form action=\"".site_url($action1)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                      <input type=\"submit\" value=\"".$value1."\" class=\"table-button green\">
                                    </form>";
                                  } elseif ($a == 5) {
                                    echo form_open($action, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                                      echo "<input type=\"submit\" value=\"".$value."\" class=\"table-button orange\">";
                                    echo form_close();
                                  } elseif ($a == 6) {
                                    echo "Pasien Selesai Berobat";
                                  }
                                echo "</div>";
                                echo "</td>";
                              echo "</tr>";
                            $gigi++;
                            }
                          }
                        }
                         if ($gigi == 0) {
                          echo "
                        <tr class=\"inner-table\">
                          <td colspan=\"6\"class=\"first\">
                          -- Tidak Ada Antrian --</td>
                        </tr>
                          ";}
                      ?>
                    </tbody>
                  </table>
                  <!-- Tutup Tabel Verifikasi Antrian Gigi -->

              <div class="row inline">
                <div class="center medium">
                  <?php
                  /*
                    if ($status_registrasi) {
                      echo form_open('antrian/buka_registrasi_berobat', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan? Perubahan yang terjadi tidak dapat dikembalikan.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                      echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Rekap Registrasi Berobat\" class=\"button red\" onclick=\"\">";
                      echo form_close();
                    } else {
                      if (!$status_umum) {
                        echo form_open('antrian/batalkan_tutup_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Batalkan Tutup Antrean Poli Umum\" class=\"button red\">";
                        echo form_close();
                      } else {
                        echo form_open('antrian/tutup_antrian_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Tutup Antrean Poli Umum\" class=\"button red\">";
                        echo form_close();
                      }
                      if (!$status_gigi) {
                        echo form_open('antrian/batalkan_tutup_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Batalkan Tutup Antrean Poli Gigi\" class=\"button red\">";
                        echo form_close();
                      } else {
                        echo form_open('antrian/tutup_antrian_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"close_umum_button\" type=\"submit\" value=\"Tutup Antrean Poli Gigi\" class=\"button red\">";
                        echo form_close();
                      }
                    }*/
                    ?>
                    </div>
                  </div>
                </div>
              </div>

          

            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
