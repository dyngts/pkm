            <!-- BEGIN -->
            <div class="container">
              <?php if(ALLOW_REGISTER) { ?>
                   <?php echo "
                      <div class=\"row\">
                        <div class=\"message message-yellow\">";
                          if (isset($verify_page)) if($verify_page) echo "<p class=\"p_message\">Akun berhasil diverifikasi! </p>";
                          echo "<p class=\"p_message\">Informasi akun ".$data['name']."</p>
                          <p class=\"p_message\">
                            Username : ".$data['username']."<br/>
                            Password : ".$data['password']."
                          </p>
                          <p class=\"p_message\">Segera ubah password Anda</p>
                        </div>
                      </div>"
                    ;?>
                <?php } else { ?>
                   <?php echo "
                      <div class=\"row\">
                        <div class=\"message message-yellow\">";
                          if (isset($verify_page)) if($verify_page) echo "<p class=\"p_message\">Akun ".$data['name']." berhasil dibuat! </p>";
                          echo "
                        </div>
                      </div>"
                    ;?>
                <?php } ?>

                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">

                    <?php if(ALLOW_REGISTER) { ?>
                        <?php if($this->authentication->is_loggedin()) {
                            echo "&nbsp;&nbsp;";
                            if ($verify_page && $this->authentication->is_loket()) echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/verifikasi")."><input type=\"button\" value=\"Verifikasi Akun Lain\" class=\"button green\"></a>";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/profil/".$data['id'])."><input type=\"button\" value=\"Lihat Profil Lengkap\" class=\"button green\"></a>";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/editpassword/".$data['id'])."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a>";
                        }?>

                    <?php } elseif($this->authentication->is_pelayanan() || $this->authentication->is_pj()) { ?>
                        <?php if($this->authentication->is_loggedin()) {
                            echo "&nbsp;&nbsp;";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/profil/".$data['id'])."><input type=\"button\" value=\"Lihat Profil Lengkap\" class=\"button green\"></a>";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/editpassword/".$data['id'])."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a>";
                        }?>

                    <?php } else { ?>
                        <?php if($this->authentication->is_loggedin()) {
                            echo "&nbsp;&nbsp;";
                            echo "&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/profil/".$data['id'])."><input type=\"button\" value=\"Lihat Profil Lengkap\" class=\"button green\"></a>";
                        }?>
                    <?php } ?>
                    </div>


                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>