            <div class="container">

            <script type="text/javascript">
                function closegigi()
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    document.getElementById('close_gigi_button').disabled = 'disabled';
                    document.getElementById('close_gigi_button').value   = 'Antrean Ditutup';
                    document.getElementById('pesan_sukses').style.display = 'block';
                    document.getElementById('pesan_sukses2').style.display = 'none';
                  }
                }
                function closeumum()
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    document.getElementById('close_umum_button').disabled = 'disabled';
                    document.getElementById('close_umum_button').value = 'Antrean Ditutup';
                    document.getElementById('pesan_sukses').style.display = 'block';
                    document.getElementById('pesan_sukses2').style.display = 'none';
                  }
                }
                function showconfirm()
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    document.getElementById('pesan_sukses2').style.display = 'block';
                    document.getElementById('pesan_sukses').style.display = 'none';
                  }
                }
            </script>

                                
              <div class="row">
                <h1>Antrean - Kamis, 4 April 2013</h1>
              </div>
              <div class="row" id="pesan_sukses" style="display:none">
                <div class="message message-green">
                  <p class="p_message">Antrean berhasil ditutup </p>
                </div>
              </div>
              <div class="row" id="pesan_sukses2" style="display:none">
                <div class="message message-green">
                  <p class="p_message">Pasien berhasil ditambahkan ke dalam antrean </p>
                </div>
              </div>
              <div class="whitespace"></div>

              <div class="row">
                <div class="label">
                  <label>Tambah Pasien Berobat<strong class="required">*</strong><br><em class="english">Full Name</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p>
                  <input type="text" name="" value="" class="input_form" id="name_form">
                </div>
                <div class="note">
                  <span><label>Masukkan ID pasien PKM.</label></span>
                </div>
              </div>
              <div class="row">
                <div class="label">
                  <label>Pilih Pengobatan<strong class="required">*</strong><br><em class="english">Full Name</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p>
                  <select size="1" class="dropdown_form">
                      <option value="1">Poli Umum</option>
                      <option value="2">Poli Gigi</option>
                  </select>
                </div>
                <div class="note">
                    <span><label>Pilih poli.</label></span>
                </div>
              </div>

              <div class="inline">
                <div class="row inline">
                  <button type="button" class="button green table_button" onclick="showconfirm()">Tambahkan Pasien ke Dalam Antrean</button>
                </div>
              </div>

              <div class="whitespace"></div>

              <div class="">
                <div class="table_box">
                  <h2>Poli Umum</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Waktu Daftar</th>
                        <th class="even-col">Status</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=5 class="last"></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $ii = 0;
                        if (isset($query)) {
                          foreach ($query as $row) {
                            $ii++; $a = $row->status;
                            if ($row->poli == 1) {
                              $a = $row->status;
                              if ($a == 1){
                                $stat = "Belum Hadir";
                              } 
                              else if($a == 2) {
                                $stat = "Hadir";
                              } 
                              else if($a == 3) {
                                $stat = "Selesai";
                              } 
                              else {
                                $stat = "???";
                              }
                              echo "<tr class=\""; if(($ii%2)==1) echo "odd-row "; echo"inner-table\">";
                              echo "<td class=\"first\">".$ii."</td>";
                              echo "<td class=\"left\">".$row->id_pasien."</td>";
                              echo "<td class=\"left\">".$row->nama."</td>";
                                echo "<td class=\"left\">".$stat."</td>";
                                echo "<td class=\"last\">";
                                $value = ""; $action = ""; $disable = "";
                                if ($a == 1) {
                                $value = "Ubah ke Hadir";
                                $action = "manage_antrian/ubah_status/2/".$row->id;
                                } else if ($a == 2) {
                                $value = "Ubah ke Selesai";
                                $action = "manage_antrian/ubah_status/3/".$row->id;
                                } else {
                                $disable = "disabled=\"disabled\"";
                                }
                                echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                <input ".$disable." type=\"submit\" value=\"".$value."\">
                                </form>";
                                echo "</td>";
                                echo "</tr>";
                            }
                          }
                        }
                      ?>
                      
                    </tbody>
                  </table>

              <div class="row inline">
                <div class="center medium">
                    <input id="close_umum_button" type="button" value="Tutup Antrean Poli Umum" class="button red" onclick="closeumum();">
                </div>
              </div>
              <div class="whitespace"></div>
                  <h2>Poli Gigi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Waktu Daftar</th>
                        <th class="even-col">Tindakan</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=5 class="last"></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $ii = 1;
                        if (isset($query)) {
                          foreach ($query as $row) {
                            $ii++; $a = $row->status;
                            if ($row->poli == 2) {
                              $a = $row->status;
                              if ($a == 1){
                                $stat = "Belum Hadir";
                              } 
                              else if($a == 2) {
                                $stat = "Hadir";
                              } 
                              else if($a == 3) {
                                $stat = "Selesai";
                              } 
                              else {
                                $stat = "???";
                              }
                              echo "<tr class=\""; if(($ii%2)==1) echo "odd-row "; echo"inner-table\">";
                              echo "<td class=\"first\">".$ii."</td>";
                              echo "<td class=\"left\">".$row->id_pasien."</td>";
                              echo "<td class=\"left\">".$row->nama."</td>";
                                echo "<td class=\"left\">".$stat."</td>";
                                echo "<td class=\"last\">";
                                $value = ""; $action = ""; $disable = "";
                                if ($a == 1) {
                                $value = "Ubah ke Hadir";
                                $action = "manage_antrian/ubah_status/2/".$row->id;
                                } else if ($a == 2) {
                                $value = "Ubah ke Selesai";
                                $action = "manage_antrian/ubah_status/3/".$row->id;
                                } else {
                                $disable = "disabled=\"disabled\"";
                                }
                                echo "<form action=\"".site_url($action)."\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ?\\n\\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');\">
                                <input ".$disable." type=\"submit\" value=\"".$value."\">
                                </form>";
                                echo "</td>";
                                echo "</tr>";
                            }
                          }
                        }
                      ?>
                    </tbody>
                  </table>

              <div class="row inline">
                <div class="center medium">
                    <input id="close_gigi_button" type="button" value="Tutup Antrean Poli Gigi" class="button red" onclick="closegigi();">
                </div>
              </div>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>