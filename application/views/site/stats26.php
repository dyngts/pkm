            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
              <div class="row inlineblock">
                <div>
                  <p>Statistik jumlah data pengunjung pasien PKM.<em class="english"></em></p>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/fakultas");?>">
                    <div class="statslabel">
                      <label>a. Statistik jumlah data pengunjung pasien PKM berdasarkan fakultas.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                      <div class="inlineblock">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="f_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <option value="2011">2011</option>
                              <option value="2012">2012</option>
                              <option value="2013">2013</option>
                          </select>
                        </div>
                      </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("statistik/periode");?>">
                    <div class="statslabel">
                      <label>b. Statistik jumlah data pengunjung pasien PKM berdasarkan tahun periode.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <option value="2011">2011</option>
                              <option value="2012">2012</option>
                              <option value="2013">2013</option>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Tampilkan" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>