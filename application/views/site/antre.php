            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
            <script type="text/javascript">
                function togglegigi(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg2").style.display = 'block';
                      document.getElementById("nowgigi").style.display = 'none';
                      document.getElementById("nowgigi2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg2").style.display = 'none';
                      document.getElementById("nowgigi").style.display = 'block';
                      document.getElementById("nowgigi2").style.display = 'none';
                    }
                  }
                }
                function toggleumum(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg").style.display = 'block';
                      document.getElementById("nowumum").style.display = 'none';
                      document.getElementById("nowumum2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg").style.display = 'none';
                      document.getElementById("nowumum").style.display = 'block';
                      document.getElementById("nowumum2").style.display = 'none';
                    }
                  }
                }
            </script>


                <!-- Mulai Cek Tampilan Antre-->
              <?php
                if (false&&$terdaftar) //$row['id_mingguan'] = $id_umum;
                {
              ?>
                <div class="row">
                <div id="msg" class="row">
                  <div class="message message-green">
                    <p class="p_message text-left">Anda sedang mengantre</p>
                  </div>
                </div>
              <?php 
                }
              ?>

                
                               
                <div class="row_poli">
                  <div class="row">
                    <?php
                      date_default_timezone_set('Asia/Jakarta');
                      $today = getdate();
                      $daftarbulan = array('',
                        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                      $today['month'] = $daftarbulan[$today['mon']];
                      if ($today['wday'] == 0) {
                        echo "<h2>Minggu, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } elseif ($today['wday'] == 1) {
                        echo "<h2>Senin, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } elseif ($today['wday'] == 2) {
                        echo "<h2>Selasa, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } elseif ($today['wday'] == 3) {
                        echo "<h2>Rabu, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } elseif ($today['wday'] == 4) {
                        echo "<h2>Kamis, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } elseif ($today['wday'] == 5) {
                        echo "<h2>Jumat, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      } else {
                        echo "<h2>Sabtu, ".$today['mday']." ".$today['month']." ".$today['year']."</h2>";
                      }

                    ?>
                  </div> 

              <?php 
                if($this->authentication->is_pasien())
                {
                  echo"

                  <div class=\"poli poli_left\">
                    <h2>Poli Umum"; if (false) echo "<span class=\"total-gate-open\">(".$total_gate_umum." dokter)</span>"; echo"</h2>
                    
                    <h3>Jumlah pengantre saat ini</h3>";
                      
                  echo "<strong class=\"big\" id=\"nowgigi\">";echo $jml_pengantre[0];echo"</strong>";
                      

                    //<!-- <h3>".$waktu[0]."<strong class="star">*</strong></h3> -->

                    /*
                      if (!$terdaftar_umum) {
                        $disable = "";
                        if (!$status_umum) {
                           $disable = "disabled=\"disabled\"";
                        }
                        echo form_open('antrian/antri_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input ".$disable." id=\"om\" value=\"Antre\" type=\"submit\" class=\"button orange table_button long\">";
                        echo form_close();
                      } else {
                        echo form_open('antrian/batalkan_antrian_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                        echo form_close();
                      }
                    */
                    
                echo "</div>";
              
                echo "
                  <div class=\"poli poli_right\">
                    <h2>Poli Gigi"; if (false) echo "<span class=\"total-gate-open\">(".$total_gate_gigi." dokter)</span>"; echo"</h2>
                    <h3>Jumlah pengantre saat ini</h3>
                    <strong class=\"big\" id=\"nowgigi\">".$jml_pengantre[1]."</strong>";
                    //<!-- <h3>.$waktu[1].<strong class="star">*</strong></h3> -->

                    /*
                      if (!$terdaftar_gigi) {
                        $disable = "";
                        if (!$status_gigi) {
                           $disable = "disabled=\"disabled\"";
                        }
                        echo form_open('antrian/antri_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input ".$disable." id=\"om\" value=\"Antre\" type=\"submit\" class=\"button orange table_button long\">";
                        echo form_close();
                      } else {
                        echo form_open('antrian/batalkan_antrian_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                        echo form_close();
                      }
                    */
                  echo "</div>";
                  //<p><!--<strong class="star">*</strong> Estimasi merupakan waktu hingga pengantre terakhir selesai dilayani. Estimasi dihitung dari rata-rata lama pelayanan per pasien dikali jumlah pengantre.(Umum: 15 menit, Gigi: 30 menit) --></p>
                echo "</div>";
              }?>
              <!-- Selesai tampilan Antre -->
            

        <div class="table_box">
        <?php
          date_default_timezone_set('Asia/Jakarta');
          $today = getdate();
        ?>
        
          <h2>Jadwal Praktik Dokter Umum</h2>
          <table class="doctor">
            <thead>
              <tr class="odd-row inner-table">
                <th class="first">Hari</th>
                <th class="even-col">Nama Dokter</th>
                <th>Waktu Awal</th>
                <th class="even-col">Waktu Akhir</th>
                <?php
                  if ($this->authentication->is_pelayanan() || $this->authentication->is_pasien()) {
                    echo "<th>Tindakan</th>";
                  }
                ?>
              </tr>
            </thead>
            <tfoot>
              <tr class="inner-table last">
                <?php
                  if ($this->authentication->is_pelayanan() || $this->authentication->is_pasien()) {
                    echo "<td class=\"last\" colspan=5></td>";
                  } else {
                    echo "<td class=\"last\" colspan=4></td>";
                  }
                ?>
              </tr>
            </tfoot>


            <tbody>
              <?php
              if (isset($doctor_table_umum)&&$doctor_table_umum!=NULL)
              {
              $count_umum = 0;
              foreach ($doctor_table_umum as $row) {
                if ($row['hari'] == $today['wday']) {

                  $count_umum++;
                  echo "<tr class=\"inner-table\">";
                    echo "<td class=\"first\">";
                      if($row['hari']==1) echo "Senin";
                      else if($row['hari']==2) echo "Selasa";
                      else if($row['hari']==3) echo "Rabu";
                      else if($row['hari']==4) echo "Kamis";
                      else if($row['hari']==5) echo "Jumat";
                      else if($row['hari']==6) echo "Sabtu";
                      else if($row['hari']==0) echo "Minggu";
                    echo "</td>";
                    echo "<td>";
                    echo $this->user_model->get_user_name_by_id($row['nomor']);
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_awal']));
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_akhir']));
                    echo "</td>";
                    $jdwl = $row['id_mingguan'];
                    if ($buka_umum == 't') {
                      if ($this->authentication->is_pelayanan()) {
                        echo form_open('jadwalharian/edit/'.$jdwl);
                        echo "<td>";
                        echo "<div class=\"row table_button_group\">";
                        echo "<input type=\"submit\" value=\"Ubah\" class=\"button orange table_button\">";
                        echo "</div>";
                        echo "</td>";
                        echo form_close();
                      }
                    
                      if ($this->authentication->is_pasien()) {
                        if ($terdaftar) $row['id_mingguan'] = $id_umum;

                        if (!$terdaftar) {
                          echo form_open('antrian/antri/'.$row['id_mingguan'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                          
                          echo "<td class=\"last\">";
                          echo "<div class=\"row table_button_group\">";
                          echo "<input type=\"submit\" value=\"Antre\" class=\"button orange table_button\">";
                          echo "</div>";
                          echo "</td>";
                          echo form_close();
                        } else {
                          echo form_open('antrian/batalkan_antrian/'.$row['id_mingguan'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                          
                          echo "<td class=\"last\">";
                          echo "<div class=\"row table_button_group\">";
                          echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                          echo "</div>";
                          echo "</td>";
                          echo form_close();
                        }
                      }
                  } else {
                    echo "<td>Antrian sedang tutup</td>";
                  }
                  echo "</tr>";
                }
              }
              if ($count_umum == 0)
              {
                  echo "
              <tr class=\"inner-table\">
                <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan()||$this->authentication->is_pasien())echo 5; else echo 4;echo"\"class=\"first\">
                -- Belum Dibuka --</td>
              </tr>
                ";
                }
              }
             else {echo "

            <tr class=\"inner-table\">
              <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan()||$this->authentication->is_pasien())echo 5; else echo 4;echo"\"class=\"first\">
              -- Tidak Ada Jadwal --</td>
            </tr>
              ";}
              ?>
            </tbody>
          </table>

          <?php
            date_default_timezone_set('Asia/Jakarta');
            $today = getdate();
          ?>
        
          <h2>Jadwal Praktik Dokter Gigi</h2>
          <table class="doctor">
            <thead>
              <tr class="odd-row inner-table">
                <th class="first">Hari</th>
                <th class="even-col">Nama Dokter</th>
                <th>Waktu Awal</th>
                <th class="even-col">Waktu Akhir</th>
                <?php
                  if ($this->authentication->is_pelayanan() || $this->authentication->is_pasien()) {
                    echo "<th>Tindakan</th>";
                  }
                ?>
              </tr>
            </thead>
            <tfoot>
              <tr class="inner-table last">
                <?php
                  if ($this->authentication->is_pelayanan() || $this->authentication->is_pasien()) {
                    echo "<td class=\"last\" colspan=5></td>";
                  } else {
                    echo "<td class=\"last\" colspan=4></td>";
                  }
                ?>
              </tr>
            </tfoot>
          


          <tbody>
              <?php
              if (isset($doctor_table_gigi)&&$doctor_table_gigi!=NULL)
              {
              $count_gigi = 0;
              foreach ($doctor_table_gigi as $row) {
                if ($row['hari'] == $today['wday']) {

                  $count_gigi++;
                  echo "<tr class=\"inner-table\">";
                    echo "<td class=\"first\">";
                      if($row['hari']==1) echo "Senin";
                      else if($row['hari']==2) echo "Selasa";
                      else if($row['hari']==3) echo "Rabu";
                      else if($row['hari']==4) echo "Kamis";
                      else if($row['hari']==5) echo "Jumat";
                      else if($row['hari']==6) echo "Sabtu";
                      else if($row['hari']==0) echo "Minggu";
                    echo "</td>";
                    echo "<td>";
                      $this->load->model('pegawai_model');
                      echo $this->user_model->get_user_name_by_id($row['nomor']);
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_awal']));
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_akhir']));
                    echo "</td>";
                    $jdwl = $row['id_mingguan'];
                    if ($buka_gigi == 't') {
                      if ($this->authentication->is_pelayanan()) {
                        echo form_open('jadwalharian/edit/'.$jdwl);
                        echo "<td>";
                        echo "<div class=\"row table_button_group\">";
                        echo "<input type=\"submit\" value=\"Ubah\" class=\"button orange table_button\">";
                        echo "</div>";
                        echo "</td>";
                        echo form_close();
                      }
                    
                      if ($this->authentication->is_pasien()) {

                        if ($terdaftar)
                          $row['id_mingguan'] = $id_umum;
                        
                        if (!$terdaftar) {
                          echo form_open('antrian/antri/'.$row['id_mingguan'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                          
                          echo "<td class=\"last\">";
                          echo "<div class=\"row table_button_group\">";
                          echo "<input type=\"submit\" value=\"Antre\" class=\"button orange table_button\">";
                          echo "</div>";
                          echo "</td>";
                          echo form_close();
                        } else {
                          echo form_open('antrian/batalkan_antrian/'.$row['id_mingguan'], array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                          
                          echo "<td class=\"last\">";
                          echo "<div class=\"row table_button_group\">";
                          echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                          echo "</div>";
                          echo "</td>";
                          echo form_close();
                        }
                      }
                  } else {
                        echo "<td>Antrian sedang tutup</td>";
                      }
                  echo "</tr>";
                }
              }
              if ($count_umum == 0)
              {
                  echo "
              <tr class=\"inner-table\">
                <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan()||$this->authentication->is_pasien())echo 5; else echo 4;echo"\"class=\"first\">
                -- Belum Dibuka --</td>
              </tr>
                ";
                }
              }
             else {echo "

            <tr class=\"inner-table\">
              <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan()||$this->authentication->is_pasien())echo 5; else echo 4;echo"\"class=\"first\">
              -- Tidak Ada Jadwal --</td>
            </tr>
              ";}
              ?>
            </tbody>
          </table>
                  
          <?php
            date_default_timezone_set('Asia/Jakarta');
            $today = getdate();
          ?>
        
          <h2>Jadwal Praktik Dokter Estetika Medis</h2>
          <table class="doctor">
            <thead>
              <tr class="odd-row inner-table">
                <th class="first">Hari</th>
                <th class="even-col">Nama Dokter</th>
                <th>Waktu Awal</th>
                <th class="even-col">Waktu Akhir</th>
                <?php
                  if ($this->authentication->is_pelayanan()) {
                    echo "<th>Tindakan</th>";
                  }
                ?>
              </tr>
            </thead>
            <tfoot>
              <tr class="inner-table last">
                <?php
                  if ($this->authentication->is_pelayanan()) {
                    echo "<td class=\"last\" colspan=5></td>";
                  } else {
                    echo "<td class=\"last\" colspan=4></td>";
                  }
                ?>
              </tr>
            </tfoot>

          <tbody>
              <?php
              if(isset($doctor_table_medis)&&$doctor_table_medis!=NULL)
              {

              $count_medis = 0;
              foreach ($doctor_table_medis as $row) {
                if ($row['hari'] == $today['wday']) {
                  $count_medis++;
                  echo "<tr class=\"inner-table\">";
                    echo "<td class=\"first\">";
                      if($row['hari']==1) echo "Senin";
                      else if($row['hari']==2) echo "Selasa";
                      else if($row['hari']==3) echo "Rabu";
                      else if($row['hari']==4) echo "Kamis";
                      else if($row['hari']==5) echo "Jumat";
                      else if($row['hari']==6) echo "Sabtu";
                      else if($row['hari']==0) echo "Minggu";
                    echo "</td>";
                    echo "<td>";
                      $this->load->model('pegawai_model');
                      echo $this->user_model->get_user_name_by_id($row['nomor']);
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_awal']));
                    echo "</td>";
                    echo "<td>";
                      echo date ('H:i', strtotime($row['waktu_aktual_akhir']));
                    echo "</td>";
                    $jdwl = $row['id_mingguan'];
                    if ($this->authentication->is_pelayanan()) {
                      echo form_open('jadwalharian/edit/'.$jdwl);
                      echo "<td>";
                      echo "<div class=\"row table_button_group\">";
                      echo "<input type=\"submit\" value=\"Ubah\" class=\"button orange table_button\">";
                      echo "</div>";
                      echo "</td>";
                      echo form_close();
                    }
                  echo "</tr>";
                }
              } 
              if ($count_medis == 0)
              {
                  echo "
              <tr class=\"inner-table\">
                <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan()||$this->authentication->is_pasien())echo 5; else echo 4;echo"\"class=\"first\">
                -- Belum Dibuka --</td>
              </tr>
                ";
                }
              }
             else {echo "

            <tr class=\"inner-table\">
              <td class=\"last\" colspan=\"";if($this->authentication->is_pelayanan())echo 5; else echo 4;echo"\"class=\"first\">
              -- Tidak Ada Jadwal --</td>
            </tr>
              ";}
              ?>
            </tbody>
          </table>
                </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>