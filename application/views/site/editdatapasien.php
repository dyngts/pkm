            <!-- FORM BEGIN -->
            <div class="container">

            <?php echo validation_errors();?>
            <?php if(isset($error) && count($error) > 0) foreach ($error as $key) {
                echo '<div><div class="row"><div class="message message-red">'.$key.'</div></div>';
            }?>
            <?php echo $msg;?>
            <?php echo form_open_multipart('akun/edit/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              
                <fieldset>
                  <h2>Edit Profil</h2>
                  <?php 
                  
                    $role = $query['otorisasi_id'];
                    $rolename = $query['jabatan'];

                    $username = $query['username'];
                    $name = $query['nama'];
                    $sex = $query['jenis_kelamin'];
                    $phone = $query['no_telepon'];
                    $address = $query['alamat'];
                    $photo = $query['foto'];

                    if ($role <= 3) {
                      $id = $query['id'];
                      $blood = $query['gol_darah'];
                      $nation = $query['kewarganegaraan'];
                      $birthplace = $query['tempat_lahir'];
                      $birthday = $query['tanggal_lahir'];
                      $religion = $query['agama'];
                      $marital = $query['status_pernikahan'];
                      $identitas = $query['no_identitas'];
                      $jurusan = $query['jurusan'];
                      $tahunmasuk = $query['tahun_masuk'];
                      $lembaga = $query['lembaga_fakultas'];
                      $status_aktif = $query['status_verifikasi'];
                    }
                    else {
                      $id = $query['nomor'];
                      $email = $query['email'];
                      $status_aktif = $query['status_aktif'];
                    }
                  ?>

                    <?php if ((($this->authentication->is_loket() && ALLOW_REGISTER) || $this->authentication->is_pelayanan() || $this->authentication->is_pj()) && $status_aktif == 't') {
                        echo "<div class=\"row\" style=\"text-align: right\">";
                        echo "<a href=".site_url("akun/editpassword/$id")."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a></div>";
                    }?>  

                    <div class="row">
                        <div class="label" style="float: none; margin-bottom: 18px;">
                            <label><strong class="required">*</strong> = Wajib diisi.</label>
                        </div>
                    </div>

                    <div class="row<?php if($status_aktif == "f") echo " hide";?>">
                        <div class="label">
                            <label>ID Pasien<strong class="required">*</strong><br/><em class="english">ID</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="id" value="<?php echo set_value('id',$id); ?>" class="input_form_disabled" readonly="readonly">
                        </div>
                        <div class="note">
                            <span><label>ID Pasien tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            
                            <input type="text" name="nama" value="<?php echo set_value('nama',$name); ?>" id="name_form" <?php if (!$this->authentication->is_pelayanan() && !$this->authentication->is_loket()) echo "readonly=\"readonly\""; echo "class=\"input_form_disabled\"" ;?>>
                        </div>
                        <div class="note">
                            <span><label><?php echo "Nama tidak dapat diubah";?></label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Peran<strong class="required">*</strong><br/><em class="english">Role</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="peran" value="<?php echo set_value('peran',$rolename); ?>" class="input_form_disabled" readonly="readonly">
                        </div>
                        <div class="note">
                            <span><label>Peran tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class="row<?php if($role <= 3) echo " hide";?>">
                      <div class="label">
                        <label>Username<br/><em class="english">Username</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">:</p>
                        <input type="text" name="username" value="<?php echo set_value('username',$username); ?>" class="input_form_disabled" <?php if($role <= 3) echo "readonly=\"readonly\"";?>>
                      </div>
                        <div class="note">
                            <span><label>Username.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nomor Telepon<strong class="required">*</strong><br/><em class="english">Phone Number</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="no_telp" value="<?php echo set_value('no_telp',$phone); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan Nomor Telepon Anda (hanya boleh angka). Contoh format: 08123456789.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <textarea name="alamat" class="input_form address_form"><?php echo set_value('alamat',$address); ?></textarea>
                        </div>
                        <div class="note">
                            <span><label></label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="radio" name="jenis_kelamin" value="L" class="input_form sex_form" <?php if ($sex == "L") $one = TRUE; else $one = FALSE ;  echo set_radio('jenis_kelamin', 'L', $one); ?>> Laki-laki &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" name="jenis_kelamin" value="P" class="input_form sex_form" <?php if ($sex == "P") $two = TRUE; else $two = FALSE ;  echo set_radio('jenis_kelamin', 'P', $two); ?>> Perempuan
                        </div>
                        <div class="note">
                            <span><label>Pilih jenis kelamin Anda.</label></span>
                        </div>
                    </div>
                   
                <?php if ($role == 1) echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>NPM<strong class=\"required\">*</strong><br/><em class=\"english\">NPM</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"npm\" value=\"".set_value('npm',$identitas)."\" class=\"input_form_disabled\"  readonly=\"readonly\">
                        </div>
                        <div class=\"note\">
                            <span><label>NPM tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Fakultas<strong class=\"required\">*</strong><br/><em class=\"english\">Faculty</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"fakultas\" value=\"".set_value('fakultas',$lembaga)."\" class=\"input_form_disabled\" id=\"name_form\" readonly=\"readonly\">
                        </div>
                        <div class=\"note\">
                            <span><label>Fakultas tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Jurusan<strong class=\"required\">*</strong><br/><em class=\"english\">Majors</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"jurusan\" value=\"".set_value('jurusan', $jurusan)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan jurusan Anda.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Tahun Masuk<strong class=\"required\">*</strong><br/><em class=\"english\">Year</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"tahunmasuk\" value=\"".set_value('tahunmasuk', $tahunmasuk)."\" class=\"input_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Tahun daftar di PKM UI.</label></span>
                        </div>
                    </div>
                ";?>
                <?php if ($role == 2) echo"

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Lembaga<strong class=\"required\">*</strong><br/><em class=\"english\">Institution</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"lembaga\" value=\"".set_value('lembaga',$lembaga)."\" class=\"input_form_disabled\" id=\"name_form\" readonly=\"readonly\">
                        </div>
                        <div class=\"note\">
                            <span><label>Fakultas tidak dapat diubah.</label></span>
                        </div>
                    </div>
                ";?>
                <?php if ($role > 3) echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Email<strong class=\"required\">*</strong><br/><em class=\"english\">Email</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"email\" value=\"".set_value('email',$email)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan alamat email.</label></span>
                        </div>
                    </div>
                ";?>
                <?php if ($role == 3) {
                    echo "
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>No Identitas<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"no_identitas\" value=\"".set_value('no_identitas',$identitas)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan nomor SIM/KTP Anda.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Pekerjaan<strong class=\"required\">*</strong><br/><em class=\"english\">Occupation</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"pekerjaan\" value=\"".set_value('pekerjaan', $lembaga)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan Pekerjaan Anda.</label></span>
                        </div>
                    </div>
                    ";}?>

                  <?php if ($role <= 3) { 
                    echo"
                    <div class=\"row\">
                        <div class=\"row\">
                            <div class=\"label\">
                                <label>Tempat Lahir<strong class=\"required\">*</strong><br/><em class=\"english\">Place of birth</em></label>
                            </div>
                            <div class=\"input\">
                                <p class=\"p_form\" id=\"p_birthday\">:</p>
                                <div id=\"place\" class=\"input\">
                                    <input type=\"text\" id=\"birth_place_form\" class=\"input_form\" value=\"".set_value('tempat_lahir',$birthplace)."\" name=\"tempat_lahir\">
                                </div>
                            </div>
                            <div class=\"note\">
                                <span><label>Masukkan tempat lahir Anda.</label></span>
                            </div>
                        </div>
                    </div>
                
                    <div class=\"row\">
                        <div class=\"row\">
                            <div class=\"label\">
                                <label>Tanggal Lahir<strong class=\"required\">*</strong><br/><em class=\"english\">Date of birth</em></label>
                            </div>
                            <div class=\"input\">
                                <p class=\"p_form\" id=\"p_birthday\">:</p>
                                <div id=\"place\" class=\"input\">
                                    <input type=\"text\" id=\"demo3\" class=\"input_form\" value=\"".set_value('demo3')."\" name=\"demo3\" style=\"display:none\">
                                    <input type=\"text\" id=\"demo4\" class=\"input_form_disabled\" value=\"".set_value('demo4',$birthday)."\" name=\"demo4\" readonly=\"readonly\">
                                    <link href=\"".base_url()."style/calendar.css\" rel=\"stylesheet\" type=\"text/css\">
                                    <script src=\"".base_url('js/datetimepicker_css.js')."\" type=\"text/javascript\"></script>
                                    <input type=\"button\" class=\"pilihtanggal\" value=\"pilih tanggal\" onclick=\"javascript:NewCssCal('demo3','yyyyMMdd','','','','','past')\" style=\"cursor:pointer\"/>
                                </div>
                            </div>
                            <div class=\"note\">
                                <span><label>Masukkan tanggal lahir dengan menekan tombol \"pilih tanggal\".</label></span>
                            </div>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Agama<strong class=\"required\">*</strong><br/><em class=\"english\">Religion</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <select name=\"agama\" size=\"1\" class=\"dropdown_form\">";
                                echo "<option value=\"Islam\" ";if ($religion == "Islam") $one = TRUE; else $one = FALSE ;  echo set_select('agama', 'Islam', $one); echo ">Islam</option>";
                                echo "<option value=\"Kristen\" ";if ($religion == "Kristen") $two = TRUE; else $two = FALSE ;  echo set_select('agama', 'Kristen', $two); echo ">Kristen</option>";
                                echo "<option value=\"Katolik\" ";if ($religion == "Katolik") $three = TRUE; else $three = FALSE ;  echo set_select('agama', 'Katolik', $three); echo ">Katolik</option>";
                                echo "<option value=\"Hindu\" ";if ($religion == "Hindu") $four = TRUE; else $four = FALSE ;  echo set_select('agama', 'Hindu', $four); echo ">Hindu</option>";
                                echo "<option value=\"Budha\" ";if ($religion == "Budha") $five = TRUE; else $five = FALSE ;  echo set_select('agama', 'Budha', $five); echo ">Budha</option>";
                                echo "<option value=\"Kong Hu Cu\" ";if ($religion == "Kong Hu Cu") $six = TRUE; else $six = FALSE ;  echo set_select('agama', 'Kong Hu Cu', $six); echo ">Kong Hu Cu</option>";
                            echo"</select>
                        </div>
                        <div class=\"note\">
                            <span><label>Pilih agama Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Status Pernikahan<strong class=\"required\">*</strong><br/><em class=\"english\">Marital Status</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <select name=\"status_pernikahan\" size=\"1\" class=\"dropdown_form\">";
                                echo "<option value=\"Belum Menikah\" ";if ($marital == "Belum Menikah") $nope = TRUE ; else $nope = FALSE; echo set_select('status_pernikahan', 'Belum Menikah', $nope).">Belum menikah</option>";
                                echo "<option value=\"Sudah Menikah\" ";if ($marital == "Sudah Menikah") $yep = TRUE ; else $yep = FALSE; echo set_select('status_pernikahan', 'Sudah Menikah', $yep).">Menikah</option>";
                            echo "</select>
                        </div>
                        <div class=\"note\">
                            <span><label>Pilih status pernikahan Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Kewarganegaraan<strong class=\"required\">*</strong><br/><em class=\"english\">Nationality</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"kewarganegaraan\" value=\"".set_value('kewarganegaraan',$nation)."\" class=\"input_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan kewarganegaraan Anda.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Golongan Darah<br/><em class=\"english\">Bloodtype</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <select name=\"golongan_darah\" size=\"1\" class=\"dropdown_form\">";
                                echo "<option value=\"-\" ";if ($blood == "-") $nope = TRUE ; else $nope = FALSE; echo set_select('golongan_darah', '-', $nope).">Tidak Tahu</option>";
                                echo "<option value=\"A\" ";if ($blood == "A") $a = TRUE ; else $a = FALSE; echo set_select('golongan_darah', 'A', $a).">A</option>";
                                echo "<option value=\"B\" ";if ($blood == "B") $b = TRUE ; else $b = FALSE; echo set_select('golongan_darah', 'B', $b).">B</option>";
                                echo "<option value=\"AB\" ";if ($blood == "AB") $ab = TRUE ; else $ab = FALSE; echo set_select('golongan_darah', 'AB', $ab).">AB</option>";
                                echo "<option value=\"O\" ";if ($blood == "O") $o = TRUE ; else $o = FALSE; echo set_select('golongan_darah', 'O', $o).">O</option>";
                            echo "</select>
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan golongan darah Anda.</label></span>
                        </div>
                    </div>"
                    ;}?>
                    <div class="row">
                      <div class="label">
                        <label>Foto<br/><em class="english">Photograph</em></label>
                      </div>
                      <div class="input">
                        <input type="file" name="foto" value="" class="input_form" id="name_form">
                      </div>
                      <div class="note">
                        <span><label>Unggah/upload foto Anda. Maksimal 1 MB, resolusi 600 x 400, format jpeg, jpg, png, gif.</label></span>
                      </div>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <div class="row"> <!-- id="submit" -->
                            </form>
                            <?php echo form_open_multipart('site', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
                            <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
                            </form>
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
