<?php 
	$role = $this->session->userdata('role');
	$access = 0;
	switch($role) {
		case 'Koordinator Pelayanan': $access = 1;break;
		case 'Koord Administrasi&Keuangan': $access = 2;break;
		case 'Penanggung Jawab':$access = 3; break;
	}
?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#bulanhari").change(function(){
			showDiv();
		  });
		  showDiv();
	});

	$(document).ready(function() {
		$( "#datepicker" ).datepicker();
	});
	function showDiv() {
		 // hide any showing
	  $(".pilihan_div").each(function(){
		  $(this).css('display','none');
	  });

	  // our target
	  var target = "#pilihan_" + $("#bulanhari").val();                                                                                        
	  var target2 = "#pilihan_bulanan_poli";
	  var target3 = '#pilihan_tahun';
	  var e =  document.getElementById("bulanhari");  
	  var value = e.options[e.selectedIndex].value;

	  if( $(target).length > 0 && value == "bulanan") {
			$(target).css('display','block');
			$(target2).css('display','block');
			$(target3).css('display','block');
	  }
	  else $(target).css('display','block');

	}

</script>
			
			<!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
              <div class="inlineblock">
                <div>
                  <h1>LAPORAN</h1>
                </div>
                <?php echo $msg;
                  
                  // $datestring = "%Y %m %d";
                  // $time = time();

                  // echo mdate($datestring, $time);
                  
                ?>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("test/poli/umum");?>">
                    <div class="statslabel">
                      <label>Laporan Kunjungan Poli Umum.<em class="english"></em></label>
                    </div>
                    <div class="inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <select name="u_lokasi" size=1 class="dropdown_form">
                              <option value="">- Pilih Lokasi -</option>
                              <option value="DEPOK">Depok</option>
                              <!--<option value="SALEMBA">Salemba</option>-->
                          </select>
                        </div>
                        <div class="input">
                          <select name="u_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                        <div class="input">
                          <select id = 'tahun' name = 'pilihtahun'>
								<option value="">- Pilih Tahun -</option>
								<option value = '2013'>2013</option>
								<?php 
									$tahun_skrg = date("Y");
									for($i = 2013; i<$tahun_skrg; $i++){
										echo "<option value = '$i'>".$i."</option>";
									}
								?>
							</select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Lihat Laporan" class="button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/poli/gigi");?>">
                    <div class="statslabel">
                      <label>Laporan Kunjungan Poli Gigi.<em class="english"></em></label>
                    </div>
                    <div class="inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <select name="g_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                        <div class="input">
                          <select id = 'tahun' name = 'pilihtahun'>
								<option value="">- Pilih Tahun -</option>
								<option value = '2013'>2013</option>
								<?php 
									$tahun_skrg = date("Y");
									for($i = 2013; i<$tahun_skrg; $i++){
										echo "<option value = '$i'>".$i."</option>";
									}
								?>
							</select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Lihat Laporan" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
				
<!--
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/umur");?>">
                    <div class="statslabel">
                      <label>c. Laporan Kunjungan Berdasarkan Umur.<em class="english"></em></label>
                    </div>
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="a_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                        <div class="input">
                          <p class="a_form"></p> 
                          <select name="a_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Unduh" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>-->
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>
