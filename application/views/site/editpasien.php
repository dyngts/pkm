            <!-- FORM BEGIN -->
            <div class="container">

              <div class="profil">
                  <form action="<?php echo site_url("site/editpasiensukses"); ?>" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');">
                    <fieldset>
                      <h2>Formulir Registrasi Pasien PKM UI</h2>
                        <div class="row">
                            
                        </div>

                        <div class="row">
                            
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>ID Pasien<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="" value="123456" class="input_form" id="name_form" disabled="disabled">
                          </div>
                          <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Peran<font style="color:#F00">*</font></label></br><em>Role</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <select size=1 class="" id="peran_form">
                              <option value="aa">-Pilih Peran-</option>
                              <option value="bb" selected="selected">Mahasiswa UI</option>
                              <option value="ab">Karyawan UI</option>
                              <option value="oo">Masyarakat Umum</option>
                            </select>
                          </div>
                          <div class="note">
                              <span><label>Pilih peran Anda.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="" value="Andrew Garfield" class="input_form" id="name_form">
                          </div>
                          <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>No telp<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="" value="74848084" class="input_form" id="name_form">
                          </div>
                          <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <textarea class="input_form address_form">Jl. Raya Badak 28</textarea>
                          </div>
                          <div class="note">
                            <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="row">
                              <div class="label">
                                  <label>Tempat, Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                              </div>
                              <div class="input">
                                  <p class="p_form" id="p_birthday">:</p>
                                  <div id="place" class="input">
                                      <input type="text" id="birth_place_form" class="input_form" value="Jakarta" name="">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                            <div class="input input_birthday">
                              <p class="p_form p_birthday">Hari<br/><em class="english">Day</em></p>
                              <select size="1" class="birthday_form dropdown_form">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">30</option>
                                <option value="3">31</option>
                              </select>
                              <p class="p_form p_birthday">Bulan<br/><em class="english">Month</em></p>
                              <select size="1" class="birthday_form dropdown_form">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">11</option>
                                <option value="3">12</option>
                              </select>
                              <p class="p_form p_birthday">Tahun<br/><em class="english">Year</em></p>
                              <select size="1" class="birthday_form dropdown_form">
                                <option value="1">1900</option>
                                <option value="2">1901</option>
                                <option value="3">1902</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">.</option>
                                <option value="3">2009</option>
                                <option value="3">2010</option>
                              </select>
                            </div>
                            <div class="note">
                              <span><label>Masukkan tempat dan tanggal lahir Anda.</label></span>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="radio" value="male" name="sex" class="input_form sex_form" checked="checked"> Laki-Laki &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" value="female" name="sex" class="input_form sex_form"> Perempuan
                          </div>
                          <div class="note">
                            <span><label>Pilih jenis kelamin Anda.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <select size="1" class="dropdown_form">
                              <option value="aa">Islam</option>
                              <option value="bb">Katolik</option>
                              <option value="ab">Protestan</option>
                              <option value="oo">Hindu</option>
                              <option value="ab">Buddha</option>
                            </select>
                          </div>
                          <div class="note">
                            <span><label>Pilih agama Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <select size="1" class="dropdown_form">
                              <option value="bb">Belum menikah</option>
                              <option value="aa">Menikah</option>
                            </select>
                          </div>
                          <div class="note">
                            <span><label>Pilih status pernikahan Anda sesuai dengan kartu identitas.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="" value="Indonesia" class="input_form">
                          </div>
                          <div class="note">
                            <span><label>Masukkan kewarganegaraan Anda.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <select size="1" class="dropdown_form">
                              <option value="aa">A</option>
                              <option value="bb">B</option>
                              <option value="ab">AB</option>
                              <option value="oo">O</option>
                            </select>
                          </div>
                          <div class="note">
                            <span><label>Masukkan golongan darah Anda.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Fakultas<strong class="required">*</strong><br/><em class="english">Faculty</em></label>
                          </div>
                          <div class="input">
                            <p class="p_form">:</p>
                            <select size="1" class="dropdown_form">
                              <option value="fk">Fakultas Kedokteran</option>
                              <option value="fkg">Fakultas Kedokteran Gigi</option>
                              <option value="fmipa">Fakultas Matematika dan Ilmu Pengetahuan Alam</option>
                              <option value="ft">Fakultas Teknik</option>
                              <option value="fh">Fakultas Hukum</option>
                              <option value="fe">Fakultas Ekonomi</option>
                              <option value="fib">Fakultas Ilmu Pengetahuan budaya</option>
                              <option value="fpsi">Fakultas Psikologi</option>
                              <option value="fisip">Fakultas Ilmu Sosial dan Ilmu Politik</option>
                              <option value="fkm">Fakultas Kesehatan Masyarakat</option>
                              <option value="fasilkom">Fakultas Ilmu Komputer</option>
                              <option value="fik">Fakultas Ilmu Keperawatan</option>
                              <option value="farmasi">Fakultas Farmasi</option>
                              <option value="vokasi">Program Vokasi</option>
                              <option value="psarjana" selected="selected">Program Pascasarjana</option>
                            </select>
                          </div>
                          <div class="note">
                            <span><label>Pilih fakultas Anda.</label></span>
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label>Foto<strong class="required">*</strong><br/><em class="english">Photograph</em></label>
                          </div>
                          <div class="input">
                            <input type="file" name="" value="" class="input_form" id="name_form">
                          </div>
                          <div class="note">
                            <span><label>Upload foto Anda. Maksimal 1 MB, resolusi 600 x 400, format jpeg, jpg, png, gif.</label></span>
                          </div>
                          <div class="image">
                            <img src="http://localhost/ci/image/4.png" alt="foto profil">
                          </div>
                        </div>

                        <div class="row">
                          <div class="label">
                            <label><strong class="required">*</strong>Wajib diisi.</label>
                          </div>
                        </div>

                        <!-- WHITESPACE -->
                        <div class="whitespace"></div>

                        <div class="row">
                          <div class="row" id="submit">
                            <a href="<?php echo base_url('site/profilepasien'); ?>"><input type="button" value="cancel" class="button orange submit_button"></a>
                            <input type="submit" value="submit" class="button green submit_button">
                          </div>
                        </div>
                    </fieldset>
                  </form>
              </div>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
