            <!-- FORM BEGIN -->
            <div class="container">

            <!-- captcha script -->
            <script type="text/javascript">
            var RecaptchaOptions = {
                theme : 'clean',

                custom_translations : {
                        instructions_visual : "Ketik disini",
                        instructions_audio : "Ketik disini",
                        play_again : "Putar sekali lagi",
                        cant_hear_this : "Download suara sebagai MP3",
                        visual_challenge : "Tantangan gambar",
                        audio_challenge : "Tantangan suara",
                        refresh_btn : "Coba kata lainnya",
                        help_btn : "Bantuan",
                        incorrect_try_again : "Kata yang Anda ketikkan tidak cocok, silakan mencoba kembali.",
                }

            };
            </script>

            <?php echo validation_errors();?>
            <?php if(isset($error) && $error != NULL) foreach ($error as $key) {
                echo '<div><div class="row"><div class="message message-red">'.$key.'</div></div>';
            }?>

            <?php echo form_open_multipart('register/pegawai', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              <!-- <form action="<?php //echo base_url(); ?>index.php/site/registrasisuccess" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.');"> -->
                <fieldset>
                  <h2>Formulir Registrasi</h2>

                    <div class="row" style="height: 30px;">
                        <div class="label">
                            <label><strong class="required">*</strong>Wajib diisi.</label>
                        </div>
                    </div>

                  <div class="row">
                    <div class="label">
                      <label>Peran Pegawai<strong class="required">*</strong><br/><em class="english">Role</em></label>
                    </div>
                     <div class="input">
                        <p class="p_form">:</p> 
                        <select name="peran" size=1 class="" id="pegawaiPkm_form">
                          <option> - Pilih Salah Satu -</option>
                          <?php
                            foreach ($listperan as $row) {
                                if ($this->authentication->firsttime()) { if ($row['otorisasi_id'] == 5) echo "<option value=\"".$row['otorisasi_id']."\" ".set_select('peran', $row['otorisasi_id']).">".$row['jabatan']."</option>";}
                                elseif ($row['otorisasi_id'] > 3) echo "<option value=\"".$row['otorisasi_id']."\" ".set_select('peran', $row['otorisasi_id']).">".$row['jabatan']."</option>";
                            }
                          ?>
                        </select>
                    </div>
                    <div class="note">
                      <span><label>Pilih peran pegawai.</label></span>
                    </div>
                  </div>

                    <div class="row">
                        <div class="label">
                            <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="nama" value="<?php echo set_value('nama'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Username<strong class="required">*</strong><br/><em class="english">Username</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="username" value="<?php echo set_value('username'); ?>" class="input_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan username yang Anda inginkan.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <textarea name="alamat" class="input_form address_form"><?php echo set_value('alamat'); ?></textarea>
                        </div>
                        <div class="note">
                            <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nomor Telepon<strong class="required">*</strong><br/><em class="english">Phone Number</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="no_telp" value="<?php echo set_value('no_telp'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan Nomor Telepon Anda (hanya boleh angka). Contoh format: 08123456789.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Email<strong class="required">*</strong><br/><em class="english">Email</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan Nomor Telepon Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="radio" name="jenis_kelamin" value="L" class="input_form sex_form" <?php echo set_radio('jenis_kelamin', 'L'); ?>> Laki-laki &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" name="jenis_kelamin" value="P" class="input_form sex_form" <?php echo set_radio('jenis_kelamin', 'P'); ?>> Perempuan
                        </div>
                        <div class="note">
                            <span><label>Pilih jenis kelamin Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Foto<br/><em class="english">Photograph</em></label>
                      </div>
                      <div class="input">
                        <input type="file" name="foto" class="input_form" id="photo_form"  onclick="document.getElementById('tanpafoto').style='display:block'">
                        <input type="button" class="pilihtanggal" value="Tanpa Foto" onclick="document.getElementById('photo_form').value=''" style="cursor:pointer;display:none;" id="tanpafoto" />
                      </div>
                      <div class="note">
                        <span><label>Unggah/upload foto Anda. Maksimal 1 MB, resolusi 600 x 400, format jpeg, jpg, png, gif.</label></span>
                      </div>
                    </div>
<!-- 
                    <img id="captcha" src="<?php //echo base_url('securimage/securimage_show.php');?>" alt="CAPTCHA Image" />
                    <input type="text" name="captcha_code" size="10" maxlength="6" />
                    <a href="#" onclick="document.getElementById('captcha').src = '<?php //echo base_url('securimage/securimage_show.php?');?>' + Math.random(); return false">[ Ganti Gambar ]</a>
 -->


<!--
                    <div class="row inlineblock">
                      <div class="label">
                        <label style="color: #F00">Bypass Captcha<br/>(keperluan development. tidak perlu mengetik dua kata di bawah ini.)<em class="english"></em></label>
                      </div>
                      <div class="input">
                        <input type="checkbox" name="bypass_captcha" value="true" checked="checked"/>
                      </div>
                    </div>
-->                    
                    <?php $show=false; if ($show) {
                    echo "<div class=\"row\">";
                        echo "<!-- The form error message will be displayed here in case the value entered is incorrect. -->";
                        echo form_error('recaptcha','<span class="warning">', '</span>');
                        echo "<p>Ketik kata berikut ini: (reload halaman, jika gambar tidak muncul. harus terkoneksi dengan internet.</br>jika tidak, tunggu beberapa lama hingga tombol muncul.)</p>";
                            echo recaptcha_get_html($publickey);
                            echo "<!-- We will use this hidden field to validate our recaptcha. -->";
                            echo "<input type=\"hidden\" name=\"recaptcha\" value=\"recaptcha\" />";
                    echo "</div>";
                    }?>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <div class="row" id="submit">
                            </form>
                            <?php echo form_open_multipart('site', array('id' => 'formbatal', 'name' => 'formbatal', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk membatalkan proses? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
                            <input form="formbatal" type="submit" value="batal" class="button orange submit_button">
                            </form>
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>
