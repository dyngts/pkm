              <div class="container">
              <div class="row">
                <h1>Antrean - Kamis, 4 Mei 2013</h1>
              </div>
              <div class="whitespace"></div>

              <div class="row">
                  <div class="label">
                      <label>Tambah Pasien Berobat<strong class="required">*</strong><br><em class="english">Full Name</em></label>
                  </div>
                  <div class="input">
                      <p class="p_form">:</p>
                      <input type="text" name="" value="" class="input_form" id="name_form">
                  </div>
                  <div class="note">
                      <span><label>Masukkan ID pasien PKM.</label></span>
                  </div>
              </div>
              <div class="row">
                <div class="label">
                      <label>Pilih Pengobatan<strong class="required">*</strong><br><em class="english">Full Name</em></label>
                  </div>
                  <div class="input">
                      <p class="p_form">:</p>
                      <select size="1" class="birthday_form dropdown_form">
                          <option value="1">Poli Umum</option>
                          <option value="2">Poli Gigi</option>
                      </select>
                  </div>
                  <div class="note">
                      <span><label>Pilih poli.</label></span>
                  </div>
              </div>

              <div class="whitespace"></div>

              <div class="row">
                <button type="button" class="button green table_button">Tambah Pasien</button>
              </div>

              <div class="">
                <div class="table_box">
                  <h2>Poli Umum</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Waktu Daftar</th>
                        <th class="even-col">Tindakan</th>
                        <th class="last">Tindakan Tindakan Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>Cut Nyak Dien</td>
                        <td>04/04/2013 10:02</td>
                        <td>kill</td>
                        <td class="last">
                          <div class="row table_button_group">
                          <input type="button" value="Hadir" class="button green table_button">
                          <input type="button" value="Belum Hadir" class="button orange table_button">
                          <input type="button" value="Selesai" class="button red table_button">
                          <input type="button" value="Batal" class="button green table_button">
                         </div>
                        </td>
                      </tr>
                      <tr class="inner-table odd-row">
                        <td class="first">2</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>
                          <div> 
                            <select size="1" class="dropdown_form">
                                <option value="aa">Hadir</option>
                                <option value="bb">Belum Hadir</option>
                                <option value="ab">Selesai</option>
                                <option value="oo">Batal</option>
                            </select>
                          </div></td>
                        <td class="last">Batal</td>
                      </tr>
                    </tbody>
                  </table>
                  <h2>Poli Gigi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Waktu Daftar</th>
                        <th class="even-col">Tindakan</th>
                        <th class="last">Tindakan Tindakan Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>Cut Nyak Dien</td>
                        <td>04/04/2013 10:02</td>
                        <td>kill</td>
                        <td class="last">
                          <div class="row table_button_group">
                          <input type="button" value="Hadir" class="button green table_button">
                          <input type="button" value="Belum Hadir" class="button orange table_button">
                          <input type="button" value="Selesai" class="button red table_button">
                          <input type="button" value="Batal" class="button green table_button">
                         </div>
                        </td>
                      </tr>
                      <tr class="inner-table odd-row">
                        <td class="first">2</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>
                          <div> 
                            <select size="1" class="dropdown_form">
                                <option value="aa">Hadir</option>
                                <option value="bb">Belum Hadir</option>
                                <option value="ab">Selesai</option>
                                <option value="oo">Batal</option>
                            </select>
                          </div></td>
                        <td class="last">Batal</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>