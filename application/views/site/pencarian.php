            <!-- BEGIN -->
            <div class="container">
              
              <div class="">
                <?php if ($this->authentication->is_pelayanan()){ ?>
				<div class="table_box">
                  <h2>Hasil Pencarian</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
						<th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama Pegawai</th>
                        <th class="even-col">Peran</th>
						<th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_kritik_saran_list">
					
					<?php 
					$rownum=0;
						foreach ($results as $val) :
						$rownum++;
						
					?>
                      <tr class="inner-table">
						
							<td><?php echo $rownum; ?></td>
							<td><?php echo anchor("akun/profil/".$val->id,$val->id);?></td>
							<td class="text-left"><?php echo anchor("akun/profil/".$val->id,$val->nama);?></td>
							<td class="text-left"><?php echo $val->peran; ?></td>
							<td class="last"> <div class="row" class="table_button_group">
							<?php echo anchor("akun/profil/".$val->id,'<input type="button" value="Lihat" class="button green table_button">');?>							
							<?php echo anchor("akun/edit/".$val->id,'<input type="button" value="Ubah" class="button orange table_button">');?>
							<?php echo form_open ("akun/delete/".$val->id,array('onSubmit'=>'return confirm(\'Apakah Anda yakin untuk melanjutkan?\nData tidak dapat diubah setelah Anda memilik OK.\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
							<input type="submit" value="Hapus" class="button red table_button">
               <?php echo form_close();?>
							</div>
							</td>
						
						</tr>
						<?php endforeach; ?>  
                    </tbody>
                  </table>
				  <div style="text-align: center"><?php echo $nav;?></div>
                </div>
				<?php if($hasil==1) {?>
				<div class="required">
					<?php echo $message; ?>
				</div>
				<?php } ?>
						<?php 
							} else if ($this->authentication->is_loket() || $this->authentication->is_perawat()){
						?>
						
				
					<div class="table_box">
                  <h2>Hasil Pencarian</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
												<th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama Pasien</th>
                        <th class="even-col">Peran</th>
												<th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_kritik_saran_list">
					
					<?php 
					$rownum= $page_num;
						foreach ($resultspasien as $val) :
						$rownum++;
						
					?>
                      <tr class="inner-table">
						
							<td><?php echo $rownum; ?></td>
							<td><?php echo $val->id;?></td>
							<td class="text-left"><?php echo $val->nama;?></td>
							<td class="text-left"><?php echo $val->peran; ?></td>
							<td class="last">
                              <div class="row" class="table_button_group">
								<?php echo anchor("akun/profil/".$val->id,'<input type="button" value="Lihat Profil" class="button green table_button">');?>
								<?php if ($this->authentication->is_loket()) {
								 echo anchor("akun/edit/".$val->id,'<input type="button" value="Ubah Profil" class="button orange table_button">');
								} ?>
                             </div>
                            </td>
						
						</tr>
						<?php endforeach; ?> 
                    </tbody>
                  </table>
				  <div style="text-align: center"><?php echo $nav;?></div>
                </div>
				<?php if($hasilpasien==1) {?>
				<div class="required">
					<?php echo $message; ?>
				</div>
				<?php } ?>
				<?php 
						} else {
				?>
					<div class="table_box">
					  <h2>Hasil Pencarian</h2>
					  <table>
						<thead>
						  <tr class="odd-row inner-table">
							<th class="first">No</th>
							<th class="even-col">Tanggal Dibuat</th>
							<th>Judul Artikel / Informasi</th>
							<th class="even-col">Penulis</th>
							<th class="last even-col">Tindakan</th>
						  </tr>
						</thead>
						<tfoot>
						  <tr class="last inner-table">
							<td colspan=6></td>
						  </tr>
						</tfoot>
						<tbody id="table_kritik_saran_list">
						
						<?php 
						$rownum= $page_num;
							foreach ($resultsarticle as $val) :
							$rownum++;
							
						?>
						  <tr class="inner-table">
							
								<td class="text-right"><?php echo $rownum; ?></td>
								<td><?php echo $val->tgl_naik;?></td>
								<td class="text-left"><?php echo anchor("artikel_controller/readarticle/".$val->id,$val->judul); ?></td>
								<td class="text-left"> <?php echo $val->nama?> </td>
								<td class="last">
									<div class="row" class="table_button_group">
									<?php echo anchor("artikel_controller/readarticle/".$val->id,'<input type="button" value="Lihat Artikel" class="button green table_button">');?>
									</div>
								</td>
						  </tr>
							<?php endforeach; ?> 
						</tbody>
					  </table>
					  <div style="text-align: center"><?php echo $nav;?></div>
					</div>
						<?php if($hasilartikel==1) {?>
					<div class="required">
						<?php echo $message; ?>
					</div>
					<?php } ?>
				<?php } ?>
				</div>
			</div> 
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>