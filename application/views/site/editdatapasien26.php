            <!-- FORM BEGIN -->
            <div class="container">

            <?php echo validation_errors();?>
            <?php echo $msg;?>
            <?php echo form_open_multipart('site/editpasien/'.$id, array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')')); ?>
              
                <fieldset>
                  <h2>Edit Data Pasien PKM UI</h2>
                    <?php 
                            foreach ($query as $row) {
                                $my_id = $row->id;
                                $my_role = $row->peran;

                                $my_username = $row->username;
                                $my_nama = $row->nama;
                                $my_sex = $row->jeniskelamin;
                                $my_blood = $row->goldarah;
                                $my_nation = $row->kewarganegaraan;
                                $my_birthplace = $row->tempatlahir;
                                $my_birthday = $row->tanggallahir;
                                $my_religion = $row->agama;
                                $my_address = $row->alamat;
                                $my_marital = $row->statuspernikahan;
                                $my_phone = $row->no_telepon;
                            }

                            foreach ($query2 as $row) {
                                if ($my_role == "Mahasiswa") 
                                {                                    
                                    $my_npm = $row->npm;
                                    $my_jurusan = $row->jurusan;
                                    $my_fakultas = $row->fakultas;
                                    $my_tahundaftar = $row->tahun_daftar;
                                    $my_semesterdaftar = $row->semester_daftar;

                                }
                                if ($my_role == "Karyawan") 
                                {                                    
                                    $my_lembaga = $row->lembaga;
                                    
                                }
                                if ($my_role == "Masyarakat Umum") 
                                {                                    
                                    $my_pekerjaan = $row->pekerjaan;
                                    $my_password = $row->password;
                                    $my_identitas = $row->no_identitas;

                                }
                            }

                            ;?>

                    <div class="row">
                        <div class="label">
                            <label>ID Pasien<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="id" value="<?php echo set_value('id',$my_id); ?>" class="input_form_disabled" readonly="readonly">
                        </div>
                        <div class="note">
                            <span><label>ID Pasien tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Peran<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="peran" value="<?php echo set_value('peran',$my_role); ?>" class="input_form_disabled" readonly="readonly">
                        </div>
                        <div class="note">
                            <span><label>Peran tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Nama Lengkap<strong class="required">*</strong><br/><em class="english">Full Name</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            
                            <input type="text" name="nama" value="<?php echo set_value('nama',$my_nama); ?>" id="name_form" <?php if (!($my_role == "Masyarakat Umum")) { echo "readonly=\"readonly\" class=\"input_form_disabled\"" ;} else echo "class=\"input_form\"";?>>
                        </div>
                        <div class="note">
                            <span><label>Masukkan nama lengkap Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>
                   
                <?php if ($my_role == "Mahasiswa") echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>NPM<strong class=\"required\">*</strong><br/><em class=\"english\">NPM</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"npm\" value=\"".set_value('npm',$my_npm)."\" class=\"input_form_disabled\"  readonly=\"readonly\">
                        </div>
                        <div class=\"note\">
                            <span><label>NPM tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Fakultas<strong class=\"required\">*</strong><br/><em class=\"english\">NPM</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"fakultas\" value=\"".set_value('fakultas',$my_fakultas)."\" class=\"input_form_disabled\" id=\"name_form\" readonly=\"readonly\">
                        </div>
                        <div class=\"note\">
                            <span><label>Fakultas tidak dapat diubah.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Jurusan<strong class=\"required\">*</strong><br/><em class=\"english\">Majors</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"jurusan\" value=\"".set_value('jurusan', $my_jurusan)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan jurusan Anda.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Semester<strong class=\"required\">*</strong><br/><em class=\"english\">Semester</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"semester\" value=\"".set_value('semester', $my_semesterdaftar)."\" class=\"input_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Semester.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Tahun Daftar<strong class=\"required\">*</strong><br/><em class=\"english\">Year</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"tahun_daftar\" value=\"".set_value('tahun_daftar', $my_tahundaftar)."\" class=\"input_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Tahun daftar di PKM UI.</label></span>
                        </div>
                    </div>
                ";?>

                    
<?php if ($my_role == "Karyawan") echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Lembaga<strong class=\"required\">*</strong><br/><em class=\"english\">Institution</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"lembaga\" value=\"".set_value('lembaga',$my_lembaga)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan nama lembaga tempat Anda Bekerja di lingkungan UI.</label></span>
                        </div>
                    </div>
";?>
<?php if ($my_role == "Masyarakat Umum") {/*echo"

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Username<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"no_username\" value=\"".set_value('no_username',$my_username)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan nomor SIM/KTP Anda.</label></span>
                        </div>
                    </div>";*/
                    echo"
                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Password<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"password\" name=\"password\" value=\"".set_value('password',$my_password)."\" class=\"input_form\" id=\"password\">";
                            if ($this->user_model->get_my_id() == $my_id) echo "
                            <input type=\"button\" value=\"Lihat Password\" class=\"pilihtanggal\" onmousedown=\"document.getElementById('password').type = 'text'\" onmouseup=\"document.getElementById('password').type = 'password'\">";
                        echo "
                        </div>
                        <div class=\"note\">
                            <span><label>Ubah password dengan mengetik secara langsung.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>No Identitas<strong class=\"required\">*</strong><br/><em class=\"english\">ID number</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"no_identitas\" value=\"".set_value('no_identitas',$my_identitas)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan nomor SIM/KTP Anda.</label></span>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                            <label>Pekerjaan<strong class=\"required\">*</strong><br/><em class=\"english\">Institution</em></label>
                        </div>
                        <div class=\"input\">
                            <p class=\"p_form\">:</p>
                            <input type=\"text\" name=\"pekerjaan\" value=\"".set_value('pekerjaan', $my_pekerjaan)."\" class=\"input_form\" id=\"name_form\">
                        </div>
                        <div class=\"note\">
                            <span><label>Masukkan Pekerjaan Anda.</label></span>
                        </div>
                    </div>
";}?>


                    <div class="row">
                        <div class="label">
                            <label>Nomor Telepon<strong class="required">*</strong><br/><em class="english">Phone Number</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="no_telp" value="<?php echo set_value('no_telp',$my_phone); ?>" class="input_form" id="name_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan Nomor Telepon Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Alamat<strong class="required">*</strong><br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <textarea name="alamat" class="input_form address_form"><?php echo set_value('alamat',$my_address); ?></textarea>
                        </div>
                        <div class="note">
                            <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="row">
                            <div class="label">
                                <label>Tempat Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                            </div>
                            <div class="input">
                                <p class="p_form" id="p_birthday">:</p>
                                <div id="place" class="input">
                                    <input type="text" id="birth_place_form" class="input_form" value="<?php echo set_value('tempat_lahir',$my_birthplace); ?>" name="tempat_lahir">
                                </div>
                            </div>
                            <div class="note">
                                <span><label>Masukkan alamat Anda sesuai dengan kartu identitas.</label></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="row">
                            <div class="label">
                                <label>Tanggal Lahir<strong class="required">*</strong><br/><em class="english">Place and date of birth</em></label>
                            </div>
                            <div class="input">
                                <p class="p_form" id="p_birthday">:</p>
                                <div id="place" class="input">
                                    <input type="text" id="demo3" class="input_form" value="<?php echo set_value('demo3'); ?>" name="demo3" style="display:none">
                                    <input type="text" id="demo4" class="input_form_disabled" value="<?php echo set_value('demo4',$my_birthday); ?>" name="demo4" readonly="readonly">
                                    <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                                    <script src="<?php echo base_url('js/datetimepicker_css.js');?>" type="text/javascript"></script>
                                    <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo3','yyyyMMdd','','','','','past')" style="cursor:pointer"/>
                                </div>
                            </div>
                            <div class="note">
                                <span><label>Masukkan tanggal lahir dengan menekan tombol "pilih tanggal".</label></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="label">
                            <label>Jenis Kelamin<strong class="required">*</strong><br/><em class="english">Sex</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="radio" name="jenis_kelamin" value="Laki-laki" class="input_form sex_form" <?php if ($my_sex == "Laki-laki") $one = TRUE; else $one = false ;  echo set_radio('jenis_kelamin', 'Laki-laki', $one); ?>> Laki-laki &nbsp; &nbsp; &nbsp; &nbsp;
                            <input type="radio" name="jenis_kelamin" value="Perempuan" class="input_form sex_form" <?php if ($my_sex == "Perempuan") $two = TRUE; else $two = false ;  echo set_radio('jenis_kelamin', 'Perempuan', $two); ?>> Perempuan
                        </div>
                        <div class="note">
                            <span><label>Pilih jenis kelamin Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Agama<strong class="required">*</strong><br/><em class="english">Religion</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="agama" size="1" class="dropdown_form">
                                <option value="Islam" <?php if ($my_religion == "Islam") $one = TRUE; else $one = false ;  echo set_select('agama', 'Islam', $one); ?>>Islam</option>
                                <option value="Kristen" <?php if ($my_religion == "Kristen") $two = TRUE; else $two = false ;  echo set_select('agama', 'Kristen', $two); ?>>Kristen</option>
                                <option value="Katolik" <?php if ($my_religion == "Katolik") $three = TRUE; else $three = false ;  echo set_select('agama', 'Katolik', $three); ?>>Katolik</option>
                                <option value="Hindu" <?php if ($my_religion == "Hindu") $four = TRUE; else $four = false ;  echo set_select('agama', 'Hindu', $four); ?>>Hindu</option>
                                <option value="Budha" <?php if ($my_religion == "Budha") $five = TRUE; else $five = false ;  echo set_select('agama', 'Budha', $five); ?>>Budha</option>
                                <option value="Kong Hu Cu" <?php if ($my_religion == "Kong Hu Cu") $six = TRUE; else $six = false ;  echo set_select('agama', 'Kong Hu Cu', $six); ?>>Kong Hu Cu</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih agama Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Status Pernikahan<strong class="required">*</strong><br/><em class="english">Marital Status</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="status_pernikahan" size="1" class="dropdown_form">
                                <option value="Belum Menikah" <?php if ($my_marital == "Belum Menikah") $nope = TRUE ; else $nope = false; echo set_select('status_pernikahan', 'Belum Menikah', $nope); ?>>Belum menikah</option>
                                <option value="Sudah Menikah" <?php if ($my_marital == "Sudah Menikah") $yep = TRUE ; else $yep = false; echo set_select('status_pernikahan', 'Sudah Menikah', $yep); ?>>Menikah</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Pilih status pernikahan Anda sesuai dengan kartu identitas.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Kewarganegaraan<strong class="required">*</strong><br/><em class="english">Nationality</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <input type="text" name="kewarganegaraan" value="<?php echo set_value('kewarganegaraan',$my_nation); ?>" class="input_form">
                        </div>
                        <div class="note">
                            <span><label>Masukkan kewarganegaraan Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                        </div>
                        <div class="input">
                            <p class="p_form">:</p>
                            <select name="golongan_darah" size="1" class="dropdown_form">
                                <option value="-" <?php if ($my_blood == "-") $nope = TRUE ; else $nope = false; echo set_select('golongan_darah', '-', $nope); ?>>---</option>
                                <option value="A" <?php if ($my_blood == "A") $a = TRUE ; else $a = false; echo set_select('golongan_darah', 'A', $a); ?>>A</option>
                                <option value="B" <?php if ($my_blood == "B") $b = TRUE ; else $b = false; echo set_select('golongan_darah', 'B', $b); ?>>B</option>
                                <option value="AB" <?php if ($my_blood == "AB") $ab = TRUE ; else $ab = false; echo set_select('golongan_darah', 'AB', $ab); ?>>AB</option>
                                <option value="O" <?php if ($my_blood == "O") $o = TRUE ; else $o = false; echo set_select('golongan_darah', 'O', $o); ?>>O</option>
                            </select>
                        </div>
                        <div class="note">
                            <span><label>Masukkan golongan darah Anda.</label></span>
                        </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Foto<br/><em class="english">Photograph</em></label>
                      </div>
                      <div class="input">
                        <input type="file" name="foto" value="" class="input_form" id="name_form">
                      </div>
                      <div class="note">
                        <span><label>Upload foto Anda.</label></span>
                      </div>
                    </div>

                    <div class="row">
                        <div class="label">
                            <label><strong class="required">*</strong>Wajib diisi.</label>
                        </div>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <div class="row" id="submit">
                            <a href="<?php echo site_url("site");?>"><input type="button" value="batal" class="button orange submit_button"></a>
                            <input type="submit" value="submit" class="button green submit_button">
                        </div>
                    </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>