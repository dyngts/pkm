			<!-- FORM BEGIN -->
			<div class="container">

			
				<!-- WHITESPACE -->
				<div class="whitespace"></div>


				<div class="row">
					<div><?php if (!$this->authentication->is_loket()) echo "
					<p class=\"p_message\">Selamat Datang!</p>
					<p class=\"p_message\">Saat ini Anda belum terdaftar sebagai pasien PKM,</p>
					<p class=\"p_message\">Silakan melakukan registrasi dengan memilih peran berikut.</p>
					</div>
					";
					else echo "
					<p class=\"p_message\">Pilih akun yang akan dibuat. </p>
					<p class=\"p_message\">Mahasiswa dan karyawan UI dapat melakukan registrasi dengan akun LDAP,</p>
					</div>
					";?>
				</div>

				<!-- WHITESPACE -->
				<div class="whitespace"></div>

				<div class="row">
					<div class="button_group">
						<?php if (!$this->authentication->is_loket()) {
							echo "<a href=\"".site_url("register/mahasiswa")."\"><input type=\"button\" value=\"Saya adalah mahasiswa UI\" class=\"button green\"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
							echo "<a href=\"".site_url("register/karyawan")."\"><input type=\"button\" value=\"Saya adalah karyawan UI\" class=\"button orange\"></a>" ;
						}

						else {
							if (FALSE)
							{
								echo "<input disabled=\"disabled\" type=\"button\" value=\"Buat akun mahasiswa UI\" class=\"button\" onclick=\"window.location.href ='".site_url("register/mahasiswa")."'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
								echo "<input disabled=\"disabled\" type=\"button\" value=\"Buat akun karyawan UI\" class=\"button\" onclick=\"window.location.href ='".site_url("register/karyawan")."'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
								echo "<input type=\"button\" value=\"Buat akun masyarakat umum\" class=\"button green\" onclick=\"window.location.href ='".site_url("register/umum")."'\">" ;
							}	
							else
							{
								echo "<input type=\"button\" value=\"Buat akun mahasiswa UI\" class=\"button green\" onclick=\"window.location.href ='".site_url("register/mahasiswa")."'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
								echo "<input type=\"button\" value=\"Buat akun karyawan UI\" class=\"button green\" onclick=\"window.location.href ='".site_url("register/karyawan")."'\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
								echo "<input type=\"button\" value=\"Buat akun masyarakat umum\" class=\"button green\" onclick=\"window.location.href ='".site_url("register/umum")."'\">" ;
							}						
						}
						?>
					
					</div>
				</div>

				<!-- WHITESPACE -->
				<div class="whitespace"></div>

			</div>
			<!-- FORM END -->


			<!-- WHITESPACE -->
			<div class="whitespace"></div>