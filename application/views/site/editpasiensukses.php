            <!-- BEGIN -->
            <div class="container">
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PASIEN</h2>
                  
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Data berhasil disimpan! </p>
                      <?php if($this->authentication->is_baru()) echo "
                      <p class=\"p_message\">Anda belum terdaftar sebagai pasien PKM karena belum terverifikasi.</p>
                      <p class=\"p_message\">Silakan datang ke PKM UI untuk melakukan verifikasi data Anda.</p>";?>
                    </div>
                  </div>

                  <div>
                  <?php 
                      foreach($query as $row){ echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>ID Pasien<br/><em class=\"english\">123456</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->id."</p>
                    </div>
                    <div class=\"row\">

                      <div class=\"label\">
                        <label>Peran<br/><em class=\"english\">Role</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->peran."</p>
                    </div>
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Nama Lengkap<br/><em class=\"english\">Full Name</em></label>
                      </div>
                        <p class=\"p_form\">: ".$row->nama."</p>
                    </div>


                    <div class=\"row\">
                        <div class=\"label\">
                          <label>No telp<br/><em class=\"english\">no telp</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->no_telepon."</p>
                        </div>
                    </div>
                    
                    <div class=\"row\">
                        <div class=\"label\">
                          <label>Alamat<br/><em class=\"english\">Address</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->alamat."</p>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"label\">
                          <label> Tempat, Tanggal Lahir<br/><em class=\"english\">Place and date of birth </em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$row->tempatlahir.", ".$row->tanggallahir."</p>
                        </div>
                    </div>
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jenis Kelamin<br/><em class=\"english\">Sex</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->jeniskelamin."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Agama<br/><em class=\"english\">Religion</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->agama."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Status Pernikahan<br/><em class=\"english\">Marital Status</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->statuspernikahan."</p>
                      </div>
                    </div>
        
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Kewarganegaraan <br/><em class=\"english\">Nationality</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->kewarganegaraan."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Golongan Darah<br/><em class=\"english\">Bloodtype</em></label>
                      </div>
                      <div class=\"input\">
                        <p class=\"p_form\">: ".$row->goldarah."</p>
                      </div>
                    </div>
                    ";}?>

                    <?php if (isset($query2)) {
                    if ($role == "Masyarakat Umum") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Pekerjaan<br/><em class=\"english\">Occupation</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->pekerjaan."</p>
                      </div>
                    </div>
                    "; }

                    else if ($role == "Karyawan") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Lembaga<br/><em class=\"english\">Institution</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->lembaga."</p>
                      </div>
                    </div>
                    "; }

                    else if ($role == "Mahasiswa") foreach ($query2 as $row) { echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>NPM<br/><em class=\"english\">NPM</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->npm."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Fakultas<br/><em class=\"english\">Faculty</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->fakultas."</p>
                      </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jurusan<br/><em class=\"english\">Majors</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$row->jurusan."</p>
                      </div>
                    </div>
                    ";}}?>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
                        <a href="<?php echo site_url("site/editpasien"); ?>"><input type="button" value="Ubah Data" class="button orange submit_button"></a>
                       <?php if ($this->authentication->is_loket()) echo "<a href=\"".site_url("site/verifikasiakunsukses")."\"><input type=\"button\" value=\"Verifikasi\" class=\"button orange table_button\"></a>" ;?>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo base_url("image/4.png"); ?>" alt="foto profil">
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>

