            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->
            <script type="text/javascript">
                function toggle(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Jadikan Draft') {
                      document.getElementById(b).value = 'Terbitkan';
                    }
                    else {
                    document.getElementById(b).value = 'Jadikan Draft';
                    }
                  }
                }
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_articlelist').deleteRow(i);
                  }
                }
                function showconfirm()
                {
                  confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                }
            </script>

              <div class="row">
                <a href="<?php echo base_url(); ?>index.php/site/article">
                  <input type="button" value="Buat Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Artikel / Informasi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Tanggal Dibuat</th>
                        <th>Judul Artikel / Informasi</th>
                        <th class="even-col">Status</th>  
                        <th>Penulis</th>
                      </tr>
                    </thead>
 						<?php 
						$rownum =0;
						foreach ($row -> result() as $r) :
						$rownum++;
						?>
							<tr>
							<td><?php echo $rownum; ?></td>
							<td><?php echo $r->timestamp;?></td>
							<td><?php echo anchor("site/readarticle/".$r->artikel_id,$r->judul); ?></td>
							<td><?php echo $r->status; ?></td>
							<td>Koordinator Pelayanan</td>
							</tr>
					<?php endforeach; ?>

                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>