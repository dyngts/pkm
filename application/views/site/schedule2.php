            <!-- BEGIN -->
            <div class="container">
              <div class="row">
                <h1>Judul - lalalala</h1>
              </div>
              <div class="row">
                <div class="label">
                  <label>Sesuatu<br /><em class="english">sesuatu</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p> 
                  <select size=1 class="dropdown_form">
                      <option value="aa">A</option>
                      <option value="bb">B</option>
                      <option value="ab">AB</option>
                      <option value="oo">O</option>
                  </select>
                </div>
                <div class="note">
                  <span><label>Email utama tidak dapat diubah.</label></span>
                </div>
              </div>
              <div class="row">
                <div class="label">
                  <label>Sesuatu<br /><em class="english">sesuatu</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p> 
                  <select size=1 class="dropdown_form">
                      <option value="aa">A</option>
                      <option value="bb">B</option>
                      <option value="ab">AB</option>
                      <option value="oo">O</option>
                  </select>
                </div>
                <div class="note">
                  <span><label>Email utama tidak dapat diubah.</label></span>
                </div>
              </div>
              <div class="">
                <div class="table_box">
                  <h2>Table lalalaal</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Dokter</th>
                        <th class="even-col">Tindakan</th>
                        <th class="last">Tindakan Tindakan Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>Wa</td>
                        <td>dr. Wawa Wawa Wawa</td>
                        <td>kill</td>
                        <td class="last">
                          <div class="row table_button_group">
                          <input type="button" value="sesuatu" class="button green table_button">
                          <input type="button" value="sesuatu" class="button orange table_button">
                          <input type="button" value="sesuatu" class="button red table_button">
                         </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">2</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>kill</td>
                        <td class="last">kill</td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">3</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>kill</td>
                        <td class="last">kill</td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">4</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>kill</td>
                        <td class="last">kill</td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">5</td>
                        <td>Wa</td>
                        <td>dr. Wawa</td>
                        <td>
                          <div> 
                            <select size="1" class="dropdown_form">
                                <option value="aa">kill</option>
                                <option value="bb">kill</option>
                                <option value="ab">kill</option>
                                <option value="oo">killkillkillkill</option>
                                <option value="ab">5</option>
                                <option value="oo">6</option>
                            </select>
                          </div></td>
                        <td class="last">kill</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>