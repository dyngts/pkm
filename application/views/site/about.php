<article>
	<p>PKM UI merupakan singkatan dari Pusat Kesehatan Mahasiswa Universitas Indonesia, yaitu sebuah pelayanan kesehatan yang disediakan oleh Universitas Indonesia bagi civitas akademik dan juga masyarakat umum dalam memberikan pelayanan pengobatan kesehatan. PKM UI menawarkan beberapa jenis pelayanan, diantaranya poli umum, poli gigi, dan estetika medis. PKM UI didukung oleh tenaga medis ahli berkategori dokter serta perawat. Gedung PKM UI terletak disamping Fakultas Ilmu Keperawatan.</p>
	<br/>
	<p class="bold no-indent">Fasilitas:</p>
	<ul class="no-indent verdana">
		<li>Apotek Ambulance (2 unit)</li>
		<li>UGD (buka selama waktu beroperasinya poliklinik)</li>
		<li>Radiologi</li>
	</ul>
	<br/>
	<p class="bold no-indent">Jumlah Tenaga Medis :</p>
	<table class="listpegawai">
		<tr>
			<td>Dokter Umum</td><td>: 6 orang</td>
		</tr>
		<tr>
			<td>Dokter Jantung</td><td>: 2 orang</td>
		</tr>
		<tr>
			<td>Dokter Gigi </td><td>: 1 orang</td>
		</tr>
		<tr>
			<td>Perawat Umum</td><td>: 5 orang</td>
		</tr>
		<tr>
			<td>Perawat Gigi</td><td>: 2 orang</td>
		</tr>
		<tr>
			<td>Staf Administrasi</td><td>: 3 orang</td>
		</tr>
		<tr>
			<td>Staf Apotek</td><td>: 	2 orang</td>
		</tr>
		<tr>
			<td>Kasir</td><td>: 1 orang</td>
		</tr>
		<tr>
			<td>Cleaning Service</td><td>: 2 orang</td>
		</tr>
		<tr>
			<td>Keamanan</td><td>: 6 orang</td>
		</tr>
	</table>
	<br/>
	<p class="bold no-indent">Persyaratan Berobat</p>
	<p>Mahasiswa : Menunjukkan Kartu Tanda Mahasiswa (KTM). Bagi mahasiswa yang belum memilki KTM, dapat menunjukkan slip bukti pembayaran semester dan pas foto berukuran 2x3 (2 lembar).
	Karyawan/Pegawai : Menujukkan surat keterangan karyawan Pas foto berukuran 2x3 (2 lembar)
	Tamu (Umum) : Tidak ada persyaratan administrasi khusus untuk masyarakat umum yang akan berobat, namun tetap dikenakan biaya konsultasi maupun biaya resep obat-obatan</p>

	<div class="whitespace"></div>
	<p class="no-indent">sumber: http://www.ui.ac.id/id/administration/page/poliklinik</p>
	<div class="whitespace"></div>
</article>