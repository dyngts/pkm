 <div class="container">
                <div class="row">
                  <h1>Laporan Tahunan PKM UI</h1>
                </div>
                <div class="whitespace"></div>

              <div class="row">
                <div class="label">
                  <label>Peran Pasien<br /><em class="english">sesuatu</em></label>
                </div>
                <div class="input">
                  <p class="p_form">:</p> 
                  <select size=1 class="dropdown_form">
                      <option value="aa">Mahasiswa UI</option>
                      <option value="bb">Karyawan UI</option>
                      <option value="ab">Masyarakat Umum</option>
                  </select>
                </div>
                <div class="note">
                  <span><label>Email utama tidak dapat diubah.</label></span>
                </div>
              </div>
              <div class="row">
                  <div class="label">
                    <label>Periode<br /><em class="english">sesuatu</em></label>
                  </div>
                  <div class="input">
                    <p class="p_form">:</p> 
                    <select size=1 class="dropdown_form">
                        <option value="aa">2009/2010</option>
                        <option value="bb">2010/2011</option>
                        <option value="ab">2011/2012</option>
                        <option value="oo">2012/2013</option>
                    </select>
                  </div>
                  <div class="note">
                    <span><label>Email utama tidak dapat diubah.</label></span>
                  </div>
                </div>


                  <div class="row">
                    <button type="button">Cetak Laporan</button>
                  </div>

                <div class="">
                  <div class="table_box">
                    <h2>Laporan Pasien Mahasiswa Periode 2010/2011</h2>
                    <table>
                      <thead>
                        <tr class="odd-row inner-table">
                          <th rowspan=2 class="first">Fakultas</th>
                          <th colspan=7>Jumlah Pasien Berobat</th>
                          <th colspan=7>Jumlah Pasien Baru</th>
                        </tr>
                        <tr>
                          <th class="even-col">Jan</th>
                          <th>Feb</th>
                          <th class="even-col">Mar</th>
                          <th>Apr</th>
                          <th class="even-col">Mei</th>
                          <th>Jun</th>
                          <th class="even-col">Jul</th>
                          <th>Jan</th>
                          <th class="even-col">Feb</th>
                          <th>Mar</th>
                          <th class="even-col">Apr</th>
                          <th>Mei</th>
                          <th class="even-col">Jun</th>
                          <th>Jul</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr class="inner-table">
                          <td colspan=15></td>
                        </tr>
                      </tfoot>
                      <tbody>
                        <tr class="inner-table">
                          <td class="first">Fakultas Ilmu Komputer</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                        </tr>
                        <tr class="inner-table odd-row">
                          <td class="first">Fakultas Psikologi</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                        </tr>
                        <tr class="inner-table">
                          <td class="first">Fakultas Ekonomi</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                        </tr>
                        <tr class="inner-table odd-row last">
                          <td class="first">Fakultas Ilmu Keperawatan</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                          <td>25</td>
                          <td>23</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="">
                  <div class="table_box">
                    <h2>Laporan Pengobatan Pasien Bulan April 2013</h2>
                    <table>
                      <thead>
                        <tr class="odd-row inner-table">
                          <th class="first" rowspan="2">Peran Pasien</th>
                          <th colspan=2>Jumlah Pasien Lama</th>
                          <th colspan=2>Jumlah Pasien Baru</th>
                        </tr>
                        <tr class="odd-row inner-table">
                          <th class="first">Poli Umum</th>
                          <th class="even-col">Poli Gigi</th>
                          <th class="first">Poli Umum</th>
                          <th class="even-col">Poli Gigi</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr class="inner-table">
                          <td colspan=5></td>
                        </tr>
                      </tfoot>
                      <tbody>
                        <tr class="inner-table">
                          <td>Mahasiswa UI</td>
                          <td>123</td>
                          <td>35</td>
                          <td>73</td>
                          <td>11</td>
                        </tr>
                        <tr class="inner-table odd-row">
                          <td>Karyawan UI</td>
                          <td>63</td>
                          <td>35</td>
                          <td>73</td>
                          <td>11</td>
                        </tr>
                        <tr class="inner-table">
                          <td>Masyarakat Umum</td>
                          <td>43</td>
                          <td>35</td>
                          <td>43</td>
                          <td>11</td>
                        </tr>
                        <tr class="inner-table">
                          <td>Total</td>
                          <td>229</td>
                          <td>105</td>
                          <td>189</td>
                          <td>33</td>
                        </tr>
                        <tr class="inner-table">
                          <td>Total</td>
                          <td colspan=2>334</td>
                          <td colspan=2>222</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
              <!-- MAIN CONTENT 1 END -->

              <!-- WHITESPACE -->
              <div class="whitespace"></div>