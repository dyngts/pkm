            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PEGAWAI</h2>
                  <div>
				  <?php 
						
						foreach ($row -> result() as $r) :
						
						$id = $r->id;
						$username = $r->username;
						$password = $r->password_pegawai;
						$nama = $r->nama;
						$jenisKelamin = $r->jeniskelamin;
						$golDarah = $r->goldarah;
						$kewarga = $r->kewarganegaraan;
						$tmpLahir = $r->tempatlahir;
						$tglLahir = $r->tanggallahir;
						$agama = $r->agama;
						$alamat = $r->alamat;
						$status = $r->statuspernikahan;
						$peran = $r->peran;
						$noTelp = $r->no_telepon;
						endforeach;
						?>
						
						
						
                    <div class="row">
                      <div class="label">
                        <label>ID Pegawai<br/><em class="english">ID</em></label>
                      </div>
                        <p class="p_form">: <?php echo $id ?></p>
                    </div>
                    <div class="row">
                      <div class="label">
                        <label>Nama Lengkap<br/><em class="english">Full Name</em></label>
                      </div>
                        <p class="p_form">: <?php echo $nama ?></p>
                    </div>

                    <div class="row">
                        <div class="label">
                          <label>No telp<br/><em class="english">no telp</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: <?php echo $noTelp ?></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label">
                          <label>Alamat<br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: <?php echo $alamat ?></p>
                        </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Jenis Kelamin<br/><em class="english">Sex</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: <?php echo $jenisKelamin ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Agama<br/><em class="english">Religion</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $agama ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Status Pernikahan<br/><em class="english">Marital Status</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $status ?></p>
                      </div>
                    </div>
        
                    <div class="row">
                      <div class="label">
                        <label>Kewarganegaraan <br/><em class="english">Nationality</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $kewarga ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: <?php echo $golDarah ?></p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Peran Pegawai<br/><em class="english">Role</em></label>
                      </div>
                     <div class="input">
                      <p class="p_form">: <?php echo $peran ?></p>
                    </div>
                    </div>

					<div class="row">
                      <div class="label">
                        <label>No.Telepon<br/><em class="english">Telephone</em></label>
                      </div>
                     <div class="input">
                      <p class="p_form">: <?php echo $noTelp; ?></p>
                    </div>
                    </div>

                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">
						<?php echo anchor("site/editpegawai/".$id,'<input type="button" value="Ubah Data" class="button orange submit_button">');?>
                        
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo base_url();?>image/3.png" alt="foto profil">
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>