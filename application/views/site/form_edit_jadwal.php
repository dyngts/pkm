            <!-- FORM BEGIN -->
           
		   <div class="container">
          <?php if(isset($show_msg_sukses) && $show_msg_sukses) echo "<div class=\"row\"><div class=\"message message-green\">sukses</div></div>";?>
          <?php if(isset($show_msg_overlap) && $show_msg_overlap) echo "<div class=\"row\"><div class=\"message message-red\">overlap</div></div>";?>
          <?php if(isset($show_msg_konflik) && $show_msg_konflik && !$show_msg_overlap) echo "<div class=\"row\"><div class=\"message message-red\">sudah ada</div></div>";?>
    		  <?php echo validation_errors();?>

    			<?php echo form_open('jadwal/process_edit/'.$id, array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
		
         <fieldset>
            <div class="row" style="height: 30px;">
                <div class="label">
                    <label><strong class="required">*</strong>Wajib diisi.</label>
                </div>
            </div>
            
            <div class="row">
              <div class="label">
                <label>Jenis Poli<strong class="required">*</strong></br><em class="english"></em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
                <select size="1" name="poli" class="dropdown_form">
                  <option value="1" <?php if($jadwal_data['id_layanan'] == "1") $val = TRUE; else $val = FALSE; echo set_select('poli', '1', $val); ?>>Poli Umum</option>
                  <option value="2" <?php if($jadwal_data['id_layanan'] == "2") $val = TRUE; else $val = FALSE; echo set_select('poli', '2', $val); ?>>Poli Gigi</option>
                  <option value="3" <?php if($jadwal_data['id_layanan'] == "3") $val = TRUE; else $val = FALSE; echo set_select('poli', '3', $val); ?>>Poli Estetika Medis</option>
                </select>
              </div>
              <div class="note">
                <span><label>Pilih jenis poli.</label></span>
              </div>
            </div>

            <div class="row">
              <div class="label">
                <label>Nama Dokter<strong class="required">*</strong></br><em class="english">Doctor Name</em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
                <select size="1" name="nama" class="dropdown_form">
    						<?php
                if($doctor_name == NULL) echo "<option> - Tidak ada dokter - </option>";
    						else {
                  echo "<option value=\"\"> - Pilih Dokter - </option>";
                  foreach ($doctor_name as $doc){
                    if ($jadwal_data['nomor_dokter'] == $doc['nomor']) $bol = TRUE; else $bol = FALSE;
                    echo "<option value=\"" . $doc['nomor'] . "\"".set_select('nama', $doc['nomor'], $bol).">" . $doc["nomor"] . " - " . $doc["nama"] . "</option>";
                  }
                }
    						?>
                      </select>
                    </div>
					<div class="required"> <?php //echo form_error('nama'); ?> </div>
                    <div class="note">
                      <span><label>Pilih nama dokter.</label></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="label">
                      <label>Hari Praktik<strong class="required">*</strong><br/><em class="english">Schedule</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" name="hari" class="dropdown_form">
                        <option value="" <?php echo ($jadwal_data['hari'] == "")? "selected=\"selected\"" : "" ?>>Pilih Hari</option>                        
						            <option value="1" <?php echo ($jadwal_data['hari'] == "1")? "selected=\"selected\"" : "" ?>>Senin</option>
                        <option value="2" <?php echo ($jadwal_data['hari'] == "2")? "selected=\"selected\"" : "" ?>>Selasa</option>
                        <option value="3" <?php echo ($jadwal_data['hari'] == "3")? "selected=\"selected\"" : "" ?>>Rabu</option>
                        <option value="4" <?php echo ($jadwal_data['hari'] == "4")? "selected=\"selected\"" : "" ?>>Kamis</option>
                        <option value="5" <?php echo ($jadwal_data['hari'] == "5")? "selected=\"selected\"" : "" ?>>Jumat</option>
						            <option value="6" <?php echo ($jadwal_data['hari'] == "6")? "selected=\"selected\"" : "" ?>>Sabtu</option>	
												
                      </select>                    
					</div>
					<div class="required"> <?php //echo form_error('hari'); ?> </div>
                    <div class="note">
                      <span><label>Pilih hari praktik dokter.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Mulai<strong class="required">*</strong><br><em class="english">Start Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                <?php
                  $jammulai = array("Default"=>array('08'=>'08','13'=>'13'),'Lainnya'=>array('00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23'));
                  $jamawal=$dateparts['hour'];
                  echo form_dropdown('jamMulai', $jammulai, $jamawal, " class=\"birthday_form dropdown_form\"");
                ?>

						<div class="required"> <?php //echo form_error('jamMulai'); ?> </div>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                <?php
                  $menitmulai = array(""=>'Pilih Menit', '00'=>'00', '15'=>'15', '30'=>'30', '45'=>'45');
                  $menitawal=$dateparts['minute'];
                  echo form_dropdown('menitMulai', $menitmulai, $menitawal, " class=\"birthday_form dropdown_form\"");
                ?>
						<div class="required"> <?php //echo form_error('menitMulai'); ?> </div>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu mulainya praktik.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Selesai<strong class="required">*</strong><br><em class="english">Finish Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                  <?php
                    $jamselesai = array("Default"=>array('12'=>'12','17'=>'17'),'Lainnya'=>array('00'=>'00', '01'=>'01', '02'=>'02', '03'=>'03', '04'=>'04', '05'=>'05', '06'=>'06', '07'=>'07', '08'=>'08', '09'=>'09', '10'=>'10', '11'=>'11', '12'=>'12', '13'=>'13', '14'=>'14', '15'=>'15', '16'=>'16', '17'=>'17', '18'=>'18', '19'=>'19', '20'=>'20', '21'=>'21', '22'=>'22', '23'=>'23'));
                    $jamakhir=$datepartsend['hour'];
                    echo form_dropdown('jamSelesai', $jamselesai, $jamakhir, " class=\"birthday_form dropdown_form\"");
                  ?>
						<div class="required"> <?php //echo form_error('jamSelesai'); ?> </div>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p>   
                  <?php
                    $menitselesai = array(""=>'Pilih Menit', '00'=>'00', '15'=>'15', '30'=>'30', '45'=>'45');
                    $menitakhir=$datepartsend['minute']; 
                    echo form_dropdown('menitSelesai', $menitselesai, $menitakhir, " class=\"birthday_form dropdown_form\"");
                  ?>
						<div class="required"> <?php //echo form_error('menitSelesai'); ?> </div>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu berakhirnya praktik.</label></span>
                    </div>
                  </div>
					<div class="required"> 
						<?php if(isset($query) && $query != null)
						echo $query;
						?>
					</div>
                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                      <div class="row">
                        <a href="<?php echo site_url('jadwal/mingguan');?>"><button type="button" class="button orange">List Jadwal Mingguan</button></a>
                      <input type="submit" value="submit" class="button green submit_button"/>
                    </div>
                  </div>
                </fieldset>
            </div>
			<?php echo form_close(); ?>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>