            <!-- BEGIN -->
            <div class="container">
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PASIEN</h2>
                  <!-- 
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Data berhasil disimpan! </p>
                      <p class="p_message">Anda akan mendapatkan ID pasien setelah diverifikasi.</p>
                      <p class="p_message p_message_eng">Success!</p>
                    </div>
                  </div>
                   -->

                  <?php
                  
                    $role = $query['otorisasi_id'];
                    $rolename = $query['jabatan'];

                    $username = $query['username'];
                    $name = $query['nama'];
                    $sex = $query['jenis_kelamin'];
                    $phone = $query['no_telepon'];
                    $address = $query['alamat'];
                    $photo = $query['foto'];

                    if ($role <= 3) {
                      $id = $query['id'];
                      $blood = $query['gol_darah'];
                      $nation = $query['kewarganegaraan'];
                      $birthplace = $query['tempat_lahir'];
                      $birthday = $query['tanggal_lahir'];
                      $religion = $query['agama'];
                      $marital = $query['status_pernikahan'];
                      $identitas = $query['no_identitas'];
                      $jurusan = $query['jurusan'];
                      $tahunmasuk = $query['tahun_masuk'];
                      $lembaga = $query['lembaga_fakultas'];
                      $status_aktif = $query['status_verifikasi'];
                    }
                    else {
                      $id = $query['nomor'];
                      $email = $query['email'];
                      $status_aktif = $query['status_aktif'];
                    }
                  ;?>

                  <div>
                  <?php if (isset($msg2)) echo $msg2;?>
                  <?php { 
                    if(!($this->authentication->is_baru()||$this->authentication->is_mendaftar())) echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>ID Pasien<br/><em class=\"english\">ID</em></label>
                      </div>
                        <p class=\"p_form\">: ".$id."</p>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Username<br/><em class=\"english\">Username</em></label>
                      </div>
                        <p class=\"p_form\">: ".$username."</p>
                    </div>";

                    echo "
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Nama Lengkap<br/><em class=\"english\">Full Name</em></label>
                      </div>
                        <p class=\"p_form\">: ".$name."</p>
                    </div>
                    
                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Peran<br/><em class=\"english\">Role</em></label>
                      </div>
                        <p class=\"p_form\">: ".$rolename."</p>
                    </div>

                    <div class=\"row\">
                        <div class=\"label\">
                          <label>No telp<br/><em class=\"english\">no telp</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$phone."</p>
                        </div>
                    </div>
                    
                    <div class=\"row\">
                        <div class=\"label\">
                          <label>Alamat<br/><em class=\"english\">Address</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$address."</p>
                        </div>
                    </div>

                    <div class=\"row\">
                      <div class=\"label\">
                        <label>Jenis Kelamin<br/><em class=\"english\">Sex</em></label>
                      </div>
                     <div class=\"input\">
                        <p class=\"p_form\">: ".$sex."</p>
                      </div>
                    </div>

                    ";}?>

                    <?php if ($role > 3) {
                      echo "
                        <div class=\"row\">
                          <div class=\"label\">
                            <label>email<br/><em class=\"english\">Sex</em></label>
                          </div>
                         <div class=\"input\">
                            <p class=\"p_form\">: ".$email."</p>
                          </div>
                        </div>";
                    }?>

                    <?php if ($role <= 3) {
                      echo "
                      <div class=\"row\">
                          <div class=\"label\">
                            <label> Tempat, Tanggal Lahir<br/><em class=\"english\">Place and date of birth </em></label>
                          </div>
                          <div class=\"input\">
                            <p class=\"p_form\">: ".$birthplace.", ".$birthday."</p>
                          </div>
                      </div>
                      
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Agama<br/><em class=\"english\">Religion</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$religion."</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Status Pernikahan<br/><em class=\"english\">Marital Status</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$marital."</p>
                        </div>
                      </div>
          
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Kewarganegaraan <br/><em class=\"english\">Nationality</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$nation."</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Golongan Darah<br/><em class=\"english\">Bloodtype</em></label>
                        </div>
                        <div class=\"input\">
                          <p class=\"p_form\">: ".$blood."</p>
                        </div>
                      </div>";

                      if ($role == 3) { echo "

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Pekerjaan<br/><em class=\"english\">Occupation</em></label>
                        </div>
                       <div class=\"input\">
                          <p class=\"p_form\">: ".$lembaga."</p>
                        </div>
                      </div>
                      "; }

                      else if ($role == 2) { echo "
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Lembaga<br/><em class=\"english\">Institution</em></label>
                        </div>
                       <div class=\"input\">
                          <p class=\"p_form\">: ".$lembaga."</p>
                        </div>
                      </div>
                      "; }

                      else if ($role == 1) { echo "
                      <div class=\"row\">
                        <div class=\"label\">
                          <label>NPM<br/><em class=\"english\">NPM</em></label>
                        </div>
                       <div class=\"input\">
                          <p class=\"p_form\">: ".$identitas."</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Fakultas<br/><em class=\"english\">Faculty</em></label>
                        </div>
                       <div class=\"input\">
                          <p class=\"p_form\">: ".$lembaga."</p>
                        </div>
                      </div>

                      <div class=\"row\">
                        <div class=\"label\">
                          <label>Jurusan<br/><em class=\"english\">Majors</em></label>
                        </div>
                       <div class=\"input\">
                          <p class=\"p_form\">: ".$jurusan."</p>
                        </div>
                      </div>
                    ";}
                    
                    }?>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                    <div class="row">

                        <?php if ($show_button) {
                            echo "<a href=".site_url("akun/edit/$id")."><input type=\"button\" value=\"Ubah Data\" class=\"button orange submit_button\"></a>";
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=".site_url("akun/editpassword/$id")."><input type=\"button\" value=\"Ubah Password\" class=\"button orange\"></a>";
                            //echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            //<form style=\"display:inline\" action=".site_url("site/deletepasien/$row->id")."  method=\"post\" onsubmit=\"return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.')\"><input type=\"submit\" value=\"Hapus Pasien\" class=\"button red table_button\" /></form>" ;
                        }?>
                    </div>

                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo base_url("foto/pasien/".$id.".jpg"); ?>" alt="tanpa foto" height=264 width=198>
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>