<html>
  <head>
    <link href="<?php echo base_url(); ?>style/homepage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/form.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/table.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/bar.css" rel="stylesheet" type="text/css">
    <title>SITRASIPLUS</title>
  </head>
  <body style="margin-top: 60px; background-color: #313131; text-align: left; color:#999; padding:0; font-size: 11px">
	<h2 style="color:rgba(0,0,0,0.3); padding-left:25px;"></h2>
  <?php $member = array('Novanto','Fauria','Afrishal','Amelia','Alfian','Elizabeth','Hadyan') ?>
	<ul>
      <li><a href="<?php echo site_url(); ?>" title="back to homepage">&#8592; back</a></li>
      <li>&nbsp;</li>
    <?php 
      $rand = rand(0,6);
      $count = count($member);
      for ($i=0; $i < $count; $i++) { ?>
      <li><?php echo $member[( $rand + $i ) % $count]; ?></li>
    <?php 
      }
    ?>
	</ul>
  </body>
</html>