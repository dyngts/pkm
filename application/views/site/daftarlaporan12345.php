            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
              <div class="inlineblock">
                <div>
                  <h1>LAPORAN</h1>
                </div>
                <?php echo $msg;
                  
                  // $datestring = "%Y %m %d";
                  // $time = time();

                  // echo mdate($datestring, $time);
                  
                ?>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/poli/umum");?>">
                    <div class="statslabel">
                      <label>a. Laporan Kunjungan Poli Umum.<em class="english"></em></label>
                    </div>
                    <div class="inlineblock">
                      <div class="margin_left">
                      <div class="inlineblock">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="u_lokasi" size=1 class="dropdown_form">
                              <option value="">- Pilih Lokasi -</option>
                              <option value="DEPOK">Depok</option>
                              <option value="SALEMBA">Salemba</option>
                          </select>
                        </div>
                      </div>
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="u_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                      <div class="inlineblock">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="u_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                      </div>
                        <div class="inlineblock">
                          <input type="submit" value="Unduh" class="button green table_button">
                        </div>
                      </div>
                      <div class="note">
                        <span><label></label></span>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/poli/gigi");?>">
                    <div class="statslabel">
                      <label>b. Laporan Kunjungan Poli Gigi.<em class="english"></em></label>
                    </div>
                    <div class="inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="g_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                        <div class="input">
                          <p class="form"></p> 
                          <select name="g_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Unduh" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row inlineblock">
                  <form method="post" action="<?php echo site_url("laporan/umur");?>">
                  <!--  <div class="statslabel">
                      <label>c. Laporan Kunjungan Berdasarkan Umur.<em class="english"></em></label>
                    </div> -->
                    <div class="row inlineblock">
                      <div class="margin_left">
                        <div class="input">
                          <p class="p_form"></p> 
                          <select name="a_bulan" size=1 class="dropdown_form">
                              <option value="">- Pilih Bulan -</option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                              <option value="4">April</option>
                              <option value="5">Mei</option>
                              <option value="6">Juni</option>
                              <option value="7">Juli</option>
                              <option value="8">Agustus</option>
                              <option value="9">September</option>
                              <option value="10">Oktober</option>
                              <option value="11">November</option>
                              <option value="12">Desember</option>
                          </select>
                        </div>
                        <div class="input">
                          <p class="a_form"></p> 
                          <select name="a_tahun" size=1 class="dropdown_form">
                              <option value="">- Pilih Tahun -</option>
                              <?php 
                                for ($i=$firstyear;$i<=$lastyear;$i++) {
                                echo "<option value=\"".$i."\">".$i."</option>";
                              }?>
                          </select>
                        </div>
                        <div class="inlineblock">
                          <input type="submit" value="Unduh" class="button green table_button">
                        </div>
                        <div class="note">
                          <span><label></label></span>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>