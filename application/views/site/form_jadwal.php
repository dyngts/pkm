            <!-- FORM BEGIN -->
           
		   <div class="container">
          <?php if(isset($show_msg_sukses) && $show_msg_sukses) echo "<div class=\"row\"><div class=\"message message-green\">sukses</div></div>";?>
          <?php if(isset($show_msg_overlap) && $show_msg_overlap) echo "<div class=\"row\"><div class=\"message message-red\">overlap</div></div>";?>
          <?php if(isset($show_msg_konflik) && $show_msg_konflik && !$show_msg_overlap) echo "<div class=\"row\"><div class=\"message message-red\">sudah ada</div></div>";?>
    		  <?php echo validation_errors();?>

    			<?php echo form_open('jadwal/process', array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
		
         <fieldset>
            <div class="row" style="height: 30px;">
                <div class="label">
                    <label><strong class="required">*</strong>Wajib diisi.</label>
                </div>
            </div>
            
            <div class="row">
              <div class="label">
                <label>Jenis Poli<strong class="required">*</strong></br><em class="english"></em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
                <select size="1" name="poli" class="dropdown_form">
                  <option value="1" <?php if(set_value('poli') == "1") $val = TRUE; else $val = FALSE; echo set_select('poli', '1', $val); ?>>Poli Umum</option>
                  <option value="2" <?php if(set_value('poli') == "2") $val = TRUE; else $val = FALSE; echo set_select('poli', '2', $val); ?>>Poli Gigi</option>
                  <option value="3" <?php if(set_value('poli') == "3") $val = TRUE; else $val = FALSE; echo set_select('poli', '3', $val); ?>>Poli Estetika Medis</option>
                </select>
              </div>
              <div class="note">
                <span><label>Pilih jenis poli.</label></span>
              </div>
            </div>

            <div class="row">
              <div class="label">
                <label>Nama Dokter<strong class="required">*</strong></br><em class="english">Doctor Name</em></label>
              </div>
              <div class="input">
                <p class="p_form">:</p> 
                <select size="1" name="nama" class="dropdown_form">
						<?php
            if($doctor_name == NULL) echo "<option> - Tidak ada dokter - </option>";
						else {
              echo "<option value=\"\"> - Pilih Dokter - </option>";
              foreach ($doctor_name as $doc){
                if ($doc['nomor'] == set_value('nama')) $bol = TRUE; else $bol = FALSE;
                echo "<option value=\"" . $doc['nomor'] . "\"".set_select('nama', $doc['nomor'], $bol).">" . $doc["nomor"] . " - " . $doc["nama"] . "</option>";
              }
            }
						?>
                      </select>
                    </div>
					<div class="required"> <?php //echo form_error('nama'); ?> </div>
                    <div class="note">
                      <span><label>Pilih nama dokter.</label></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="label">
                      <label>Hari Praktik<strong class="required">*</strong><br/><em class="english">Schedule</em></label>
                    </div>
                    <?php date_default_timezone_set('Asia/Jakarta'); ?>
                    <?php $hari = date ( 'N' , $timestamp = time() ); ?>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <select size="1" name="hari" class="dropdown_form">
                        <option value="" <?php echo (set_value('hari') == "" || $hari == '7')? "selected=\"selected\"" : "" ?>>Pilih Hari</option>                        
						            <option value="1" <?php echo (set_value('hari') == "1" || $hari == '1')? "selected=\"selected\"" : "" ?>>Senin</option>
                        <option value="2" <?php echo (set_value('hari') == "2" || $hari == '2')? "selected=\"selected\"" : "" ?>>Selasa</option>
                        <option value="3" <?php echo (set_value('hari') == "3" || $hari == '3')? "selected=\"selected\"" : "" ?>>Rabu</option>
                        <option value="4" <?php echo (set_value('hari') == "4" || $hari == '4')? "selected=\"selected\"" : "" ?>>Kamis</option>
                        <option value="5" <?php echo (set_value('hari') == "5" || $hari == '5')? "selected=\"selected\"" : "" ?>>Jumat</option>
						            <option value="6" <?php echo (set_value('hari') == "6" || $hari == '6')? "selected=\"selected\"" : "" ?>>Sabtu</option>	
												
                      </select>                    
					</div>
					<div class="required"> <?php //echo form_error('hari'); ?> </div>
                    <div class="note">
                      <span><label>Pilih hari praktik dokter.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Mulai<strong class="required">*</strong><br><em class="english">Start Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" name="jamMulai" class="birthday_form dropdown_form">
                            <!-- <option value="">Pilih Jam</option> -->
                            <optgroup label="Default">
                              <option value="08">08</option>"
                              <option value="13">13</option>"
                            </optgroup>
                            <optgroup label="Lainnya">
                            <?php
								for($ii = 0; $ii<24; $ii++)
								{
									if(set_value('jamMulai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{	
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" "; if ($ii == "08") echo "selected=\"selected\""; echo ">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                            </optgroup>
                        </select>
						<div class="required"> <?php //echo form_error('jamMulai'); ?> </div>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" name="menitMulai" class="birthday_form dropdown_form" value="<?php echo set_value('menitMulai'); ?>">
                            <!-- <option value="">Pilih Menit</option> -->
                            
                            <?php
								for($ii = 0; $ii<60; $ii+=15)
								{
									if(set_value('menitMulai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
						<div class="required"> <?php //echo form_error('menitMulai'); ?> </div>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu mulainya praktik.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Waktu Selesai<strong class="required">*</strong><br><em class="english">Finish Time</em></label>
                    </div>
                    <div class="input input_birthday"> 
                        <p class="p_form p_birthday">Jam<br><em class="english">Hour</em></p> 
                        <select size="1" name="jamSelesai" class="birthday_form dropdown_form" value="<?php echo set_value('jamSelesai'); ?>">
                            <!-- <option value="">Pilih Jam</option> -->                            
                            <optgroup label="Default">
                              <option value="12">12</option>"
                              <option value="17">17</option>"
                            </optgroup>
                            <optgroup label="Lainnya">
                            <?php
								for($ii = 0; $ii<24; $ii++)
								{
									if(set_value('jamSelesai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{								
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" "; if ($ii == "12") echo "selected=\"selected\""; echo ">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                            </optgroup>
                        </select>
						<div class="required"> <?php //echo form_error('jamSelesai'); ?> </div>
                        <p class="p_form p_birthday">Menit<br><em class="english">Minute</em></p> 
                        <select size="1" name="menitSelesai" class="birthday_form dropdown_form" value="<?php echo set_value('menitSelesai'); ?>">
                            <!-- <option value="">Pilih Menit</option> -->
                            
                            <?php
								for($ii = 0; $ii<60; $ii+=15)
								{
									if(set_value('menitSelesai') == str_pad($ii, 2, "0", STR_PAD_LEFT))
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\" selected=\"selected\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
									else
									{
										echo "<option value=\"". str_pad($ii, 2, "0", STR_PAD_LEFT) . "\">" . str_pad($ii, 2, "0", STR_PAD_LEFT) . "</option>";
									}
								}
							?>
                        </select>
						<div class="required"> <?php //echo form_error('menitSelesai'); ?> </div>
                      </div>
                    <div class="note">
                      <span><label>Pilih waktu berakhirnya praktik.</label></span>
                    </div>
                  </div>
					<div class="required"> 
						<?php if(isset($query) && $query != null)
						echo $query;
						?>
					</div>
                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <a href="<?php echo site_url('jadwal/mingguan');?>"><button type="button" class="button orange">List Jadwal Mingguan</button></a>
                      <input type="submit" value="Buat Jadwal" class="button green"/>
                    </div>
                  </div>
                </fieldset>
            </div>
			<?php echo form_close(); ?>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>