            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->

            <script type="text/javascript">
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_pegawailist').deleteRow(i);
                  }
                }
            </script>

		
              <div class="row">
                <a href="<?php echo site_url('site/registrasipegawai');?>">
                  <input type="button" value="Buat Akun Pegawai Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Pegawai PKM</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama Pegawai</th>
                        <th class="even-col">Peran Pegawai</th>
                        <th class="last">Tindakan </th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table">
                        <td class="last" colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_pegawailist">
                     
						
						<?php 
						$rownum =0;
						foreach ($row -> result() as $r) :
						$rownum++;
						$ro = $r->id;?>
						
							<tr>
							<td><?php echo $rownum; ?></td>
							<td><?php echo anchor("site/profilepegawai/".$r->id,$r->id);?></td>
							<td><?php echo $r->nama; ?></td>
							<td><?php echo $r->peran; ?></td>
							<td> <div class="row" class="table_button_group">
                            <?php echo anchor("site/editpegawai/".$r->id,'<input type="button" value="Ubah" class="button green table_button">');?>
                            <?php echo anchor("site/deletepegawai/".$r->id,'<input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">');?>
                           </div> 
                        </td> 
                      </tr>
						
					<?php endforeach; ?>

                    </tbody>
                  </table>
                </div>
              </div>
         </div>
			
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>