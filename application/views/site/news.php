            <!-- ARTICLE AND NEWS BEGIN -->
		
      <?php 
      $day = array('','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
      if (isset ($row)) {
	  foreach ($row as $r) { ?>
            <article title="<?php echo $r->judul; ?>">
              <a href="<?php echo site_url('artikel_controller/readarticle/'.$r->id);?>"><h1><?php echo $r->judul; ?></h1></a>
              <time datetime="<?php echo date('d-m-Y H:i',strtotime($r->tgl_naik)); ?>"><?php echo $day[date('N',strtotime($r->tgl_naik))].", ".date('d-m-Y H:i',strtotime($r->tgl_naik)); ?></time>
        <?php $foto= $r->foto; 
        if (!empty($foto)){?>
  
        <div class="image"><img src="<?php echo base_url('foto/artikel/'.$foto);?>" alt="<?php echo $foto;?>"/></div>
        <?php } ?>
        
        <?php $string_isi= word_limiter($r->isi, 100); ?>
              <p> <?php echo $string_isi; ?></p>
              <?php echo anchor("artikel_controller/readarticle/".$r->id,'read more...');?>
             
            </article>
            
      <?php } ?>
      
            
            <!-- ARTICLE AND NEWS END -->
      <?php } ?>
      <div> <?php echo $pagination;?> </div>
            <!-- WHITESPACE -->
            <div class="whitespace"></div>
