            <!-- BEGIN -->
            <div class="container">
             
              <div class="profil">
                <div class="leftside">
                  <h2>PROFIL PEGAWAI</h2>
                  <div class="row">
                    <div class="message message-yellow">
                      <p class="p_message">Data berhasil disimpan! </p>
                    </div>
                  </div>
                  <div>
                    <div class="row">
                      <div class="label">
                        <label>Peran<br/><em class="english">Role</em></label>
                      </div>
                        <p class="p_form">: Mahasiswa UI</p>
                    </div>
                    <div class="row">
                      <div class="label">
                        <label>Nama Lengkap<br/><em class="english">Full Name</em></label>
                      </div>
                        <p class="p_form">: Andrew Garfield</p>
                    </div>
                    <div class="row">
                        <div class="label">
                          <label>Alamat<br/><em class="english">Address</em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: Jl. Raya Badak 28</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">
                          <label> Tempat, Tanggal Lahir<br/><em class="english">Place and date of birth </em></label>
                        </div>
                        <div class="input">
                          <p class="p_form">: Jakarta, 15 Mei 1987</p>
                        </div>
                    </div>
                    <div class="row">
                      <div class="label">
                        <label>Jenis Kelamin<br/><em class="english">Sex</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: Laki-laki</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Agama<br/><em class="english">Religion</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Islam</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Status Pernikahan<br/><em class="english">Marital Status</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Belum Menikah</p>
                      </div>
                    </div>
        
                    <div class="row">
                      <div class="label">
                        <label>Kewarganegaraan <br/><em class="english">Nationality</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: Indonesia</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Golongan Darah<br/><em class="english">Bloodtype</em></label>
                      </div>
                      <div class="input">
                        <p class="p_form">: O</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="label">
                        <label>Fakultas<br/><em class="english">Faculty</em></label>
                      </div>
                     <div class="input">
                        <p class="p_form">: Program Pascasarjana</p>
                      </div>
                    </div>


                   <!-- WHITESPACE -->
                    <div class="whitespace"></div>
                    <div class="row">
                        <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah Data" class="button orange submit_button"></a>
                    </div>
                    <!-- WHITESPACE -->
                    <div class="whitespace"></div>

                  </div>
                </div>
                <div class="rightside">
                  <div class="image">
                    <img src="<?php echo base_url("image/4.png"); ?>" alt="foto profil">
                  </div>
                </div>
              </div>


            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>

