            <!-- FORM BEGIN -->
            <div class="container">
				<?php echo validation_errors();?>
    			<?php echo form_open('site/kritiksaran_add', array('onSubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\');')); ?>
			
                <div class="row">
                  <h1>Kritik dan Saran</h1>
                </div>
                <div class="whitespace"></div>
                <fieldset>


                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea name="teks" class="input_form address_form"><?php set_value('text');?></textarea>
                    </div>
                    <div class="note">
                      <span><label>Kritik atau saran.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <a href="http://localhost/ci/index.php/site/kritiksaransukses"><input type="submit" value="Submit" class="button green"></a>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>