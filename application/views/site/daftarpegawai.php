            <!-- BEGIN -->
            <div class="container">

              <div class="row">
                <a href="<?php echo site_url("register/pegawai"); ?>">
                  <input type="button" value="Buat Akun Pegawai Baru" class="button green">
                </a>
              </div>

             <?php echo $msg;?>

              <div class="">
                <div class="table_box">
                  <h2>Daftar Pegawai</h2>
  
                  <br/><div><?php echo $nav;?></div><br/>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID Pegawai</th>
                        <th >Nama</th>
                        <th class="even-col">Peran</th>
                        <th class="last">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td class="last" colspan = 5></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i = $page_num;
                        if(isset($query)) {
                          foreach($query as $row){ 
                            $i++;
                            echo "<tr class=\"";if(($i%2)==1) echo "odd-row "; echo"inner-table\">";
                            echo "<td class=\"first\">".$i."</td>";
                            echo "<td class=\"first\">".$row->nomor."</td>";
                            echo "<td class=\"left\">".$row->nama."</td>";
                            echo "<td class=\"left\">".$row->jabatan."</td>";
                            echo "<td class=\"last\">
                              <div class=\"row\" class=\"table_button_group\">
                                <a href=\"".site_url("akun/profil/".$row->nomor)."\"><input type=\"button\" value=\"Lihat\" class=\"button green table_button\"></a>
                                <a href=\"".site_url("akun/edit/".$row->nomor)."\"><input type=\"button\" value=\"Ubah\" class=\"button orange table_button\"></a>";
                                if(FALSE&&$this->authentication->is_pj()){echo form_open_multipart('akun/delete/'.$row->nomor, array('id' => 'formhapus', 'name' => 'formhapus', 'onsubmit' => 'return confirm(\'Apakah Anda yakin untuk menghapus akun? \n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                                echo '&nbsp;&nbsp;<input form="formhapus" type="submit" value="hapus" class="button red table_button">
                                </form>';}
                              echo"
                             </div>
                            </td>
                          </tr>";
                          }
                        }
                      ?>
                    </tbody>
                  </table>     
                  <div style="text-align: center"><?php echo $nav;?></div>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>