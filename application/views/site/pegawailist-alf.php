            <!-- BEGIN -->
            <div class="container">
              <!-- <div class="row">
                <h1>Daftar Artikel</h1>
              </div> -->

            <script type="text/javascript">
                function deleterow(c)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    var i=c.parentNode.parentNode.rowIndex;
                    document.getElementById('table_pegawailist').deleteRow(i);
                  }
                }
            </script>


              <div class="row">
                <a href="<?php echo site_url("site/registrasipegawai"); ?>">
                  <input type="button" value="Buat Akun Pegawai Baru" class="button green">
                </a>
              </div>
             
              <div class="">
                <div class="table_box">
                  <h2>Daftar Pegawai PKM</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">ID</th>
                        <th>Nama Pegawai</th>
                        <th class="even-col">Peran Pegawai</th>
                        <th class="last">Tindakan </th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table">
                        <td class="last" colspan=5></td>
                      </tr>
                    </tfoot>
                    <tbody id="table_pegawailist">
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td>123456</td>
                        <td class="left">DR. Harun A. Gunawan, MS.PAK</td>
                        <td class="left">PJ PKM</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                           </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">2</td>
                        <td>123457</td>
                        <td class="left">dr. Chaerunnisa R</td>
                        <td class="left">Koordinator Pelayanan</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                           </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">3</td>
                        <td>123458</td>
                        <td class="left">dr. Johanda Damanik</td>
                        <td class="left">Dokter Umum</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">4</td>
                        <td>123459</td>
                        <td class="left">drg. Firdaus Faisal</td>
                        <td class="left">Koordinator Logistik</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">5</td>
                        <td>123410</td>
                        <td class="left">drg. Ida Mahmuda </td>
                        <td class="left">Dokter Gigi</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table odd-row">
                        <td class="first">6</td>
                        <td>123412</td>
                        <td class="left">drg. Marisa Aristiawati H, MKM </td>
                        <td class="left">Koordinator Administrasi dan Keuangan</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">7</td>
                        <td>123413</td>
                        <td class="left">drg. Chandra</td>
                        <td class="left">Dokter Gigi</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table odd-row">
                        <td class="first">8</td>
                        <td>123414</td>
                        <td class="left">Zr. Rusmini </td>
                        <td class="left">Perawat Gigi</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">9</td>
                        <td>123415</td>
                        <td class="left">Zr. Happy Damayanti </td>
                        <td class="left">Perawat Umum</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo site_url("site/editpegawai"); ?>"><input type="button" value="Ubah" class="button green table_button"></a>
                            <input type="button" value="Hapus" class="button orange table_button" onclick="deleterow(this)">
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>