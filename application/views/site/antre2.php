            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
            <script type="text/javascript">
                function togglegigi(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg2").style.display = 'block';
                      document.getElementById("nowgigi").style.display = 'none';
                      document.getElementById("nowgigi2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg2").style.display = 'none';
                      document.getElementById("nowgigi").style.display = 'block';
                      document.getElementById("nowgigi2").style.display = 'none';
                    }
                  }
                }
                function toggleumum(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg").style.display = 'block';
                      document.getElementById("nowumum").style.display = 'none';
                      document.getElementById("nowumum2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg").style.display = 'none';
                      document.getElementById("nowumum").style.display = 'block';
                      document.getElementById("nowumum2").style.display = 'none';
                    }
                  }
                }
            </script>

              <div class="row">

                <div id="msg" class="row" style="display:none">
                  <div class="message message-green">
                    <p class="p_message">Nomor Antrian Anda: 25 (poli umum) </p>
                  </div>
                </div>
                <div id="msg2" class="row" style="display:none">
                  <div class="message message-green">
                    <p class="p_message">Nomor Antrian Anda: 8 (poli gigi) </p>
                  </div>
                </div>

                <div class="row_poli">
                  <div class="poli poli_left">
                    <h2>Poli Umum</h2>
                    <h3 >Jumlah antrean saat ini</h3>
                    <strong id="nowumum">25</strong>
                    <strong id="nowumum2" style="display:none">26</strong>
                    <h3>Estimasi: 90 menit</h3>
                    <input id="on" type="button" value="Antre" class="button orange table_button long" onclick="toggleumum(id)">
                  </div>
                  <div class="poli poli_right">
                    <h2>Poli Gigi</h2>
                    <h3>Jumlah antrean saat ini</h3>
                    <strong id="nowgigi">8</strong>
                    <strong id="nowgigi2" style="display:none">9</strong>
                    <h3>Estimasi: 20 menit</h3>
                    <input id="om" type="button" value="Antre" class="button orange table_button long" onclick="togglegigi(id)">
                  </div>
                </div>
                <div class="row">
                  <div class="message message-neutral">                  
                    <p class="p_message">pesan.
                    </p>
                  </div>
                </div>
              </div>
              
              
                <div class="table_box">
                  <h2>Jadwal Praktik Dokter Umum</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="even-col">Waktu Akhir</th>
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>

                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Fauria</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Rabu</td>
                        <td>dr. Fauria</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>

                  <h2>Jadwal Praktik Dokter Gigi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="5even-col">Waktu Akhir</th>                        
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Rabu</td>
                        <td>dr. Jojon</td>
                        <td>13:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Jumat</td>
                        <td>dr. Jojon</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>
                  
                  <h2>Jadwal Praktik Dokter Estetika Medis</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="even-col">Waktu Akhir</th>
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Apit</td>
                        <td>10:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Apit</td>
                        <td>13:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule")."';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>