            <!-- BEGIN -->
            <div class="container">

              <div class="row">
                <a href="<?php echo site_url("site/registrasi"); ?>">
                  <input type="button" value="Buat Akun Pasien Baru" class="button green">
                </a>
              </div>

              <div class="row">
                <!-- <h1>Verifikasi Pasien Baru</h1> -->
              </div>

             <?php echo $msg;?>

              <div class="">
                <div class="table_box">
                  <h2>Verifikasi Pasien</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Peran</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="last inner-table">
                        <td class="last" colspan=4></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i = 0;
                        if(isset($query)) {
                          foreach($query as $row){ 
                            $i++;
                            echo "<tr class=\"";if(($i%2)==1) echo "odd-row "; echo"inner-table\">";
                            echo "<td class=\"first\">".$i."</td>";
                            echo "<td class=\"left\">".$row->nama."</td>";
                            echo "<td class=\"left\">".$row->jabatan."</td>";
                            echo "<td class=\"last\">
                              <div class=\"row\" class=\"table_button_group\">
                                <a href=\"".site_url("akun/profilcalonpasien/".$row->id)."\"><input type=\"button\" value=\"Lihat Profil\" class=\"button green table_button\"></a>
                             </div>
                            </td>
                          </tr>";
                          }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>