            <!-- BEGIN -->
            <div class="container">

              <div class="row">
                <a href="<?php echo site_url("site/registrasipasien"); ?>">
                  <input type="button" value="Buat Akun Pasien Baru" class="button green">
                </a>
              </div>

              <div class="row">
                <h1>Verifikasi Pasien Baru</h1>
              </div>
             
                  
              <div class="row">
                <div class="message message-green">
                  <p class="p_message">Verifikasi berhasil! </p>
                </div>
              </div>
              
              <div class="">
                <div class="table_box">
                  <h2>Pendaftar</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">No</th>
                        <th class="even-col">Nama</th>
                        <th>Peran</th>
                        <th class="last even-col">Tindakan</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr class="inner-table">
                        <td class="last" colspan=4></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">1</td>
                        <td class="left">DR. Harun A. Gunawan, MS.PAK</td>
                        <td class="left">Karyawan UI</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo base_url(); ?>index.php/site/profilepasien"><input type="button" value="Lihat Profil" class="button green table_button"></a>
                          <input type="button" value="Verifikasi" class="button orange table_button">
                         </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">2</td>
                        <td class="left">Andrew Garfield</td>
                        <td class="left">Mahasiswa UI</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo base_url(); ?>index.php/site/profilepasien"><input type="button" value="Lihat Profil" class="button green table_button"></a>
                            <input type="button" value="Verifikasi" class="button orange table_button">
                          </div>
                           </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">3</td>
                        <td class="left">Johanda Damanik</td>
                        <td class="left">Mahasiswa</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo base_url(); ?>index.php/site/profilepasien"><input type="button" value="Lihat Profil" class="button green table_button"></a>
                            <input type="button" value="Verifikasi" class="button orange table_button">
                          </div>
                        </td>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">4</td>
                        <td class="left">Firdaus Faisal</td>
                        <td class="left">Umum</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo base_url(); ?>index.php/site/profilepasien"><input type="button" value="Lihat Profil" class="button green table_button"></a>
                            <input type="button" value="Verifikasi" class="button orange table_button">
                          </div>
                        </td>
                      </tr>
                      <tr class="inner-table">
                        <td class="first">5</td>
                        <td class="left">drg. Ida Mahmuda </td>
                        <td class="left">Karyawan UI</td>
                        <td class="last">
                          <div class="row" class="table_button_group">
                            <a href="<?php echo base_url(); ?>index.php/site/profilepasien"><input type="button" value="Lihat Profil" class="button green table_button"></a>
                            <input type="button" value="Verifikasi" class="button orange table_button">
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>