            <!-- FORM BEGIN -->
            <div class="container">
                <div class="row">
                  <h1>Kritik dan Saran</h1>
                </div>
                  <div class="row">
                    <div class="message message-green">
                      <p class="p_message">Kritik/saran berhasil dikirim! </p>
                    </div>
                  </div>
                  
              <form action="<?php echo site_url("site/kritiksaransukses"); ?>" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');">
                <fieldset>


                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea class="input_form address_form"></textarea>
                    </div>
                    <div class="note">
                      <span><label>Kritik atau saran.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <input type="submit" value="Submit" class="button green">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>