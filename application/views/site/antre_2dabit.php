            <!-- MAIN CONTENT 1 BEGIN -->
            <div class="container">
            <script type="text/javascript">
                function togglegigi(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg2").style.display = 'block';
                      document.getElementById("nowgigi").style.display = 'none';
                      document.getElementById("nowgigi2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg2").style.display = 'none';
                      document.getElementById("nowgigi").style.display = 'block';
                      document.getElementById("nowgigi2").style.display = 'none';
                    }
                  }
                }
                function toggleumum(b)
                {
                  var a = confirm('Apakah Anda yakin untuk melanjutkan ? \n\nTekan "OK" untuk melanjutkan, "Cancel" untuk batal.');
                  if (a==true) {
                    if (document.getElementById(b).value == 'Antre') {
                      document.getElementById(b).value = 'Batalkan';
                      document.getElementById(b).className = 'button red table_button long';
                      document.getElementById("msg").style.display = 'block';
                      document.getElementById("nowumum").style.display = 'none';
                      document.getElementById("nowumum2").style.display = 'block';
                    }
                    else {
                    document.getElementById(b).value = 'Antre';
                      document.getElementById(b).className = 'button orange table_button long';
                      document.getElementById("msg").style.display = 'none';
                      document.getElementById("nowumum").style.display = 'block';
                      document.getElementById("nowumum2").style.display = 'none';
                    }
                  }
                }
            </script>

              <div class="row">

                <div id="msg" class="row" style="display:none">
                  <div class="message message-green">
                    <p class="p_message">Nomor Antrian Anda: 25 (poli umum) </p>
                  </div>
                </div>
                <div id="msg2" class="row" style="display:none">
                  <div class="message message-green">
                    <p class="p_message">Nomor Antrian Anda: 8 (poli gigi) </p>
                  </div>
                </div>

                
                
                <div class="row_poli">
                

                  <div class="poli poli_left">
                    <h2>Poli Umum</h2>
                    <h3>Jumlah antrean saat ini</h3>
                    <?php/*
                      $data = 0;
                      if (($jml[0] - $no_antrian[0]) >= 0)
                        $data = ($jml[0] - $no_antrian[0]); 
                      echo "<strong id=\"nowgigi\">".$data."</strong>";
                      echo "<h3>".$waktu[0]."</h3>";
                    */?>
                    <?php
                    /*
                      if (!$terdaftar_umum) {
                        $disable = "";
                        if (!$status_umum) {
                           $disable = "disabled=\"disabled\"";
                        }
                        echo form_open('manage_antrian/antri_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input ".$disable." id=\"om\" value=\"Antre\" type=\"submit\" class=\"button orange table_button long\">";
                        echo form_close();
                      } else {
                        echo form_open('manage_antrian/batalkan_antrian_umum', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                        echo form_close();
                      }
                    */
                    ?>
                  </div>

                  <div class="poli poli_right">
                    <h2>Poli Gigi</h2>
                    <h3>Jumlah antrean saat ini</h3>
                    <?php
                      /*
                      $data = 0;
                      if (($jml[1] - $no_antrian[1]) >= 0)
                        $data = ($jml[1] - $no_antrian[1]);
                      echo "<strong id=\"nowgigi\">".$data."</strong>";
                      echo "<h3>".$waktu[1]."</h3>";
                      */
                    ?>
                    <?php
                    /*
                      if (!$terdaftar_gigi) {
                        $disable = "";
                        if (!$status_gigi) {
                           $disable = "disabled=\"disabled\"";
                        }
                        echo form_open('manage_antrian/antri_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input ".$disable." id=\"om\" value=\"Antre\" type=\"submit\" class=\"button orange table_button long\">";
                        echo form_close();
                      } else {
                        echo form_open('manage_antrian/batalkan_antrian_gigi', array('onsubmit' => 'return confirm(\'Apakah Anda yakin untuk melanjutkan?\n\nTekan &#34;OK&#34; untuk lanjut, &#34;Cancel&#34; untuk batal.\')'));
                        echo "<input id=\"om\" value=\"Batalkan\" type=\"submit\" class=\"button red table_button long\">";
                        echo form_close();
                      }
                    */
                    ?>
                  </div>
          
                </div>
                
                

                <div class="row">
                  <div class="message message-neutral">                  
                    <p class="p_message"></p>
                  </div>
                </div>

              </div>
              


        <div class="table_box">
        <?php
          date_default_timezone_set('Asia/Jakarta');
          $today = getdate();
          if($today['wday']!=0){
            ?>
        
          <h2>Jadwal Praktik Dokter Umum</h2>
          <table class="doctor">
          <thead>
            <tr class="odd-row inner-table">
              <th class="first">Hari</th>
              <th class="even-col">Nama Dokter</th>
              <th>Waktu Awal</th>
              <th class="even-col">Waktu Akhir</th>
              <?php
                if ($this->authentication->is_pelayanan()) {
                  echo "<th>Tindakan</th>";
                }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr class="inner-table last">
              <?php
                if ($this->authentication->is_pelayanan()) {
                  echo "<td colspan=5></td>";
                } else {
                  echo "<td colspan=4></td>";
                }
              ?>
            </tr>
          </tfoot>


          <?php 
            $ii=0;
            foreach ($doctor_table_umum->result() as $entry) :
                $ii++;
                if( $today['wday']==$entry->hari){
          ?>
            <tbody>
                <tr class="inner-table">
                  <td class="first"><?php if($entry->hari==1) echo "Senin";
                  else if($entry->hari==2) echo "Selasa";
                  else if($entry->hari==3) echo "Rabu";
                  else if($entry->hari==4) echo "Kamis";
                  else if($entry->hari==5) echo "Jumat";
                  else if($entry->hari==6) echo "Sabtu";?></td>
                  <td><?php echo $entry->nama; ?></td>
                  <td><?php echo $entry->waktuawal; ?></td>
                  <td><?php echo $entry->waktuakhir; ?></td>
                </tr>
          <?php 
          } 
          ?> 
          <?php endforeach; ?>
          </tbody>
          </table>
          <?php 
          } 
          ?>

          <?php
          date_default_timezone_set('Asia/Jakarta');
          $today = getdate();
          if($today['wday']!=0){ ?>
        
          <h2>Jadwal Praktik Dokter Gigi</h2>
          <table class="doctor">
          <thead>
          <tr class="odd-row inner-table">
            <th class="first">Hari</th>
            <th class="even-col">Nama Dokter</th>
            <th>Waktu Awal</th>
            <th class="even-col">Waktu Akhir</th>
          </tr>
          </thead>
          <tfoot>
          <tr class="inner-table last">
            <td colspan=4></td>
          </tr>
          </tfoot>
          <?php 
          $ii=0;
          foreach ($doctor_table_gigi->result() as $entry) :
          $ii++; 
          ?>
          <?php if( $today['wday']==$entry->hari){ ?>


          <tbody>
          <tr class="inner-table">
            <td class="first"><?php if($entry->hari==1) echo "Senin";
            else if($entry->hari==2) echo "Selasa";
            else if($entry->hari==3) echo "Rabu";
            else if($entry->hari==4) echo "Kamis";
            else if($entry->hari==5) echo "Jumat";
            else if($entry->hari==6) echo "Sabtu";?></td>
            <td><?php echo $entry->nama; ?></td>
            <td><?php echo $entry->waktuawal; ?></td>
            <td><?php echo $entry->waktuakhir; ?></td>
          </tr>
          <?php 
          } 
          ?> 
          <?php endforeach; ?>
          </tbody>
          </table>
          <?php 
          } 
          ?>
                  
          <?php
          date_default_timezone_set('Asia/Jakarta');
          $today = getdate();
          if($today['wday']!=0){ ?>
        
          <h2>Jadwal Praktik Dokter Estetika Medis</h2>
          <table class="doctor">
          <thead>
          <tr class="odd-row inner-table">
            <th class="first">Hari</th>
            <th class="even-col">Nama Dokter</th>
            <th>Waktu Awal</th>
            <th class="even-col">Waktu Akhir</th>
          </tr>
          </thead>
          <tfoot>
          <tr class="inner-table last">
            <td colspan=4></td>
          </tr>
          </tfoot>
          <?php 
          $ii=0;
          foreach ($doctor_table_medis->result() as $entry) :
          $ii++; 
          ?>
          <?php if( $today['wday']==$entry->hari){ ?>


          <tbody>
          <tr class="inner-table">
            <td class="first"><?php if($entry->hari==1) echo "Senin";
            else if($entry->hari==2) echo "Selasa";
            else if($entry->hari==3) echo "Rabu";
            else if($entry->hari==4) echo "Kamis";
            else if($entry->hari==5) echo "Jumat";
            else if($entry->hari==6) echo "Sabtu";?></td>
            <td><?php echo $entry->nama; ?></td>
            <td><?php echo $entry->waktuawal; ?></td>
            <td><?php echo $entry->waktuakhir; ?></td>
          </tr>
          <?php 
          } 
          ?> 
          <?php endforeach; ?>
          </tbody>
          </table>
          <?php 
          } 
          ?>
                </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>