            <!-- FORM BEGIN -->
            <div class="container">
              <form action="<?php echo site_url("site/news");?>" onsubmit="return confirm('Apakah Anda yakin untuk melanjutkan ?\n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.');respon();">
                <fieldset>

                  <div class="row">
                    <div class="label">
                      <label>Judul<strong class="required">*</strong></br><em class="english">Title</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Tag<strong class="required">*</strong></br><em class="english">Tag</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <input type="text" name="" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Isi<strong class="required">*</strong><br/><em class="english">Content</em></label>
                    </div>
                    <div class="input">
                      <p class="p_form">:</p> 
                      <textarea class="input_form content_form"></textarea>
                    </div>
                    <div class="note">
                      <span><label>Email utama tidak dapat diubah.</label></span>
                    </div>
                  </div>

                  <div class="row">
                    <div class="label">
                      <label>Gambar<br/><em class="english">Image</em></label>
                    </div>
                    <div class="input">
                      <input type="file" name="" value="" class="input_form" id="name_form">
                    </div>
                    <div class="note">
                      <span><label>Upload gambar.</label></span>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="label">
                      <label><strong class="required">*</strong>Wajib diisi.</label>
                    </div>
                  </div>


                  <!-- WHITESPACE -->
                  <div class="whitespace"></div>

                  <div class="row">
                    <div class="row">
                      <a href="<?php echo site_url("site/articlelist"); ?>"><input type="button" value="Simpan sebagai draft" class="button orange"></a>
                      <input type="submit" value="Terbitkan sekarang" class="button green">
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
            <!-- FORM END -->


            <!-- WHITESPACE -->
            <div class="whitespace"></div>