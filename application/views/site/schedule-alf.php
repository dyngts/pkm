              <div class="container">
              <div class="row">
                <h1>Jadwal Praktik Dokter</h1>
              </div>
              

              <?php if ($this->authentication->is_pelayanan()) echo "
              <div class=\"row\">
                <a href=\"".site_url("site/createschedule")."\">
                  <input type=\"button\" value=\"Buat Jadwal Baru\" class=\"button green\">
                </a>
              </div>
              "; ?>

              <div class="">
                <div class="table_box">
                  <h2>Jadwal Praktik Dokter Umum</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="even-col">Waktu Akhir</th>
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>

                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Fauria</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule").";\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Rabu</td>
                        <td>dr. Fauria</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule").";\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>

                  <h2>Jadwal Praktik Dokter Gigi</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="5even-col">Waktu Akhir</th>                        
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Rabu</td>
                        <td>dr. Jojon</td>
                        <td>13:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule").";\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Jumat</td>
                        <td>dr. Jojon</td>
                        <td>08:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".base_url()."index.php/site/createschedule';\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>
                  
                  <h2>Jadwal Praktik Dokter Estetika Medis</h2>
                  <table>
                    <thead>
                      <tr class="odd-row inner-table">
                        <th class="first">Hari</th>
                        <th class="even-col">Nama Dokter</th>
                        <th>Waktu Awal</th>
                        <th class="even-col">Waktu Akhir</th>
                        <?php if ($this->authentication->is_pelayanan()) echo "<th>Tindakan</th>";?>
                      </tr>
                    <thead>
                    <tfoot>
                      <tr class="inner-table last">
                        <td colspan=<?php if ($this->authentication->is_pelayanan()) echo "5"; else echo "4"?>></td>
                      </tr>
                    </tfoot>
                    <tbody>
                      <tr class="inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Apit</td>
                        <td>10:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule").";\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                      <tr class="odd-row inner-table">
                        <td class="first">Senin</td>
                        <td>dr. Apit</td>
                        <td>13:00</td>
                        <td <?php if ($this->authentication->is_pelayanan()) echo "class=\"last\"";?>>11:30</td>
                        <?php if ($this->authentication->is_pelayanan()) echo "
                        <td class=\"last\">
                          <div class=\"row table_button_group\">
                            <input type=\"button\" value=\"Ubah\" class=\"button orange table_button\" onclick=\"window.location = '".site_url("site/createschedule").";\">
                            <input type=\"button\" value=\"Hapus\" class=\"button red table_button\" onclick=\"deleterow(this)\">
                           </div>
                        </td>";?>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- MAIN CONTENT 1 END -->

            <!-- WHITESPACE -->
            <div class="whitespace"></div>