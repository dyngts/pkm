<html>
<head>
	<title>dynamic dropdown</title>
	
    
</head>
<body>

<select id="first-choice">
	<option selected value="base">Please Select</option>
	<option value="Alkes">Alkes</option>
	<option value="Antibiotik">Antibiotik</option>
</select>

<br />

<select id="second-choice">
	<option>Please choose from above</option>
</select>

<script type="text/javascript" src="<?php echo site_url('js/jquery-1.7.1.js'); ?>"></script>

<script type="text/javascript">
     	$("#first-choice").change(function() {
			$("#second-choice").load("<?php echo site_url('dynamic/ambil'); ?>" + "/" + $('#first-choice').val());
		});
</script>
</body>
</html>