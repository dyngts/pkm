          <!-- LOGIN FORM BEGIN -->
          <div class="side_left"  <?php if( ! $this->authentication->is_loggedin()) { ?> style="background: #FFF;" <?php } ?>>
            <div class="user_login">
              <?php if (FALSE) { ?>
              <form action="<?php echo site_url("site/process"); ?>" method="post" id="login">
                <label id="login_label">LOGIN</label><br/>
                <?php if (isset($msg3)) echo $msg3?>
                <p class="smaller italic">Mahasiswa dan karyawan UI yang belum terdaftar sebagai pasien PKM dapat login dengan menggunakan akun LDAP. </p>
                <input type="text" name="username" id="username_box" placeholder="username" class="login_box input_form"/><br />
                <input type="password" name="password" id="password_box" placeholder="password" class="login_box input_form" /><br />
                <div class="row_login">
                  <input type="submit" value="Login" id="login_button" class="button green"/>
                  <?php if (ALLOW_REGISTER) { ?>
                  <a href="<?php echo site_url("register");?>"><button type="button" id="register_button" class="button orange">Registrasi</button></a>
                  <?php } ?>
                </div>
                <div>
                  <p class="smaller italic">Masyarakat umum dapat melakukan pendaftaran dengan menekan tombol "Registrasi". Setelah terverifikasi sebagai pasien, masyarakat umum dapat login menggunakan ID pasien sebagai username.</p>
                </div>
                <div class="info_umum">
                  <p class="bold">Jadwal pelayanan PKM UI: <br />Senin-Sabtu<br/>Sesi I: 08.00-12.00 WIB<br/>Sesi II 13.30-17.00 WIB</p>
                </div>
              </form>
              <?php } ?>
            </div>
          </div>
          <!-- LOGIN FORM END -->
          
          <!-- MAIN CONTENT BEGIN -->
          <div class="side_center">