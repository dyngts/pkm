          <!-- USER NAV BEGIN -->
          <div class="side_left" <?php if( ! $this->authentication->is_loggedin()) { ?> style="background: #FFF;" <?php } ?>>
            <?php if (FALSE) { ?>
            <div class="user_login">
              <form action="<?php if($this->authentication->is_civitas()) echo site_url('cas/logout'); else echo site_url("site/do_logout");?>" method="post" id="login" onsubmit="return confirm('Apakah Anda yakin akan melakukan logout?\n\nTekan &#34;OK&#34; untuk melanjutkan, &#34;Cancel&#34; untuk batal.')">
                <label id="login_label">INFO</label><br/>
                <p>Selamat datang,
                  <?php 
                    if ( ! $this->authentication->is_baru()) 
                    {
                      if ( ! $this->authentication->is_mendaftar() || $this->authentication->is_civitas()) 
                      {
                        echo "<br /><br />".($this->authentication->get_my_name()).
                            "<br/> ID&nbsp;&nbsp; &nbsp;: ".
                            ($this->authentication->get_my_id()).
                            "<br/> Role : ".($this->authentication->get_my_role_name()).
                            "<br/>";
                      }
                    } 
                    else 
                      echo $this->authentication->get_my_username().
                    ".<br/><br/>Silakan mengisi formulir terlebih dahulu.";?>
                  </p>
                
                <div class="row_login">
                  <input type="submit" value="Logout" id="logout_button" class="button red"/>
                  <?php if($this->authentication->is_baru()) {
                  echo "<a href=\"".site_url("site/registrasi")."\"><button type=\"button\" id=\"register_button\" class=\"button orange\">Registrasi</button></a>";
                  }?>
                </div>
              </form>

              <script type="text/javascript">
                function show_confirm()
                {
                var r=confirm("Press a button");
                if (r==true)
                  {
                  alert("You pressed OK!");
                  }
                else
                  {
                  alert("You pressed Cancel!");
                  }
                }
              </script>
                          </div>
            <?php } ?>
            <div class="user_nav">
              <ul class="user_menu">
                <?php 

                //basic menu
                if (DISPLAY_MENU_PROFIL && $this->authentication->is_loggedin()&&$this->authentication->is_mendaftar()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/profil/".$this->session->userdata('id'))."\"><h3>Profil &#9658;</h3></a>
                <div class=\"dropdown\">
                    <ul>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/profilcalonpasien/".$this->session->userdata('id'))."\"><h3>Lihat Profil</h3></a></li>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/edit/".$this->session->userdata('id'))."\"><h3>Ubah Profil</h3></a></li>
                    </ul>
                </div>
                </li>
                <hr/>
                "; }

                //basic menu
                else if (DISPLAY_MENU_PROFIL && $this->authentication->is_loggedin()&&!$this->authentication->is_baru()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/profil/".$this->session->userdata('id'))."\"><h3>Profil &#9658;</h3></a>
                <div class=\"dropdown\">
                    <ul>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/profil/".$this->session->userdata('id'))."\"><h3>Lihat Profil</h3></a></li>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/edit/".$this->session->userdata('id'))."\"><h3>Ubah Profil</h3></a></li>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/editpassword/".$this->session->userdata('id'))."\"><h3>Ubah Password</h3></a></li>
                    </ul>
                </div>
                </li>
                <hr/>
                "; }

                //role menu
                if ($this->authentication->is_pasien()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("site/antre")."\"><h3>Antre</h3></a></li>
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("site/kritiksaranlist")."\"><h3>Kritik dan Saran</h3></a></li>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("site/responkepuasan")."\"><h3>Respons Kepuasan</h3></a></li>
                <hr/>
                "; }
                elseif ($this->authentication->is_admin()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/verifikasi")."\"><h3>Verifikasi Akun Pasien</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("site/registrasi")."\"><h3>Buat Akun Pasien</h3></a></li>
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("antrian")."\"><h3>Antrean</h3></a></li>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/pasien")."\"><h3>Daftar Pasien</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Akun Pegawai  &#9658;</h3></a>
                <div class=\"dropdown\">
                    <ul>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Daftar Pegawai</h3></a>
                        <li class=\"nav_left\"><a href=\"".site_url("register/pegawai")."\"><h3>Buat Baru</h3></a>
                    </ul>
                </div>
                </li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("artikel_controller/articlelist")."\"><h3>Artikel</h3></a></li>
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("site/kritiksaranlist")."\"><h3>Kritik dan Saran</h3></a></li>"; echo "
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo ""; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("statistik")."\"><h3>Statistik</h3></a></li>"; echo ""; echo "
                <hr/>
                "; if (ENABLE_ANTREAN) echo "<li class=\"nav_left\"><a href=\"".site_url("jadwal/mingguan")."\"><h3>Jadwal Praktik Dokter</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("site/antre")."\"><h3>Jadwal Harian</h3></a></li>
                <hr/>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("kasir/daftar_pasien")."\"><h3>Kuitansi</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/tindakan")."\"><h3>Tindakan</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/diagnosis")."\"><h3>Diagnosis</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah_masuk")."\"><h3>Entri Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah")."\"><h3>Tambahkan Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_detil")."\"><h3>Obat Per Tanggal Kadaluarsa</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_menjelang_kadaluarsa")."\"><h3>Daftar Kadaluarsa Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_masuk")."\"><h3>Log Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_keluar")."\"><h3>Log Obat Keluar</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah_log_salemba")."\"><h3>Tambah Log Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_salemba")."\"><h3>Daftar Log Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("resep_controller/kumpulan")."\"><h3>Resep Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/tambah")."\"><h3>Tambahkan Data Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/kumpulan")."\"><h3>Lihat Data Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("surat_jalan_controller/tambah")."\"><h3>Buat Surat Jalan</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                "; }
                elseif ($this->authentication->is_pelayanan()) { echo "                
                <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Akun Pegawai  &#9658;</h3></a>
                <div class=\"dropdown\">
                    <ul>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Daftar Pegawai</h3></a>
                        <li class=\"nav_left\"><a href=\"".site_url("register/pegawai")."\"><h3>Buat Baru</h3></a>
                    </ul>
                </div>
                </li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("artikel_controller/articlelist")."\"><h3>Artikel</h3></a></li>
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("site/kritiksaranlist")."\"><h3>Kritik dan Saran</h3></a></li>"; echo "
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("statistik")."\"><h3>Statistik</h3></a></li>"; echo "
                <hr/>
                "; if (ENABLE_ANTREAN) echo "<li class=\"nav_left\"><a href=\"".site_url("jadwal/mingguan")."\"><h3>Jadwal Praktik Dokter</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("site/antre")."\"><h3>Jadwal Harian</h3></a></li>
                <hr/>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/tindakan")."\"><h3>Tindakan</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/diagnosis")."\"><h3>Diagnosis</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                "; }
                elseif ($this->authentication->is_loket()) { echo "
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("akun/verifikasi")."\"><h3>Verifikasi Akun Pasien</h3></a></li>"; echo "                
                <li class=\"nav_left\"><a href=\"".site_url("site/registrasi")."\"><h3>Buat Akun Pasien</h3></a></li>
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("antrian")."\"><h3>Antrean</h3></a></li>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/pasien")."\"><h3>Daftar Pasien</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("artikel_controller/articlelist")."\"><h3>Artikel</h3></a></li>
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("site/kritiksaranlist")."\"><h3>Kritik dan Saran</h3></a></li>"; echo "
                <hr/>
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("statistik")."\"><h3>Statistik</h3></a></li>"; 
                if (ENABLE_ANTREAN) echo "<li class=\"nav_left\"><a href=\"".site_url("jadwal/mingguan")."\"><h3>Jadwal Praktik Dokter</h3></a></li>
                <hr/>"; }
                elseif ($this->authentication->is_perawat()) { echo "
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("antrian")."\"><h3>Antrean</h3></a></li>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";}
                elseif ($this->authentication->is_dokter()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("antrian")."\"><h3>Antrean</h3></a></li>"; /*echo "
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";*/}
                elseif ($this->authentication->is_kasir()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("kasir/daftar_pasien")."\"><h3>Kuitansi</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("kasir/buat_laporan_salemba")."\"><h3>Laporan Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";}
                elseif ($this->authentication->is_adm_keu()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("kasir/daftar_pasien")."\"><h3>Kuitansi</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/tindakan")."\"><h3>Tindakan</h3></a></li>
                <hr/> "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("antrian")."\"><h3>Antrean</h3></a></li>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";}
                elseif ($this->authentication->is_pj()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Akun Pegawai  &#9658;</h3></a>
                <div class=\"dropdown\">
                    <ul>
                        <li class=\"nav_left\"><a href=\"".site_url("akun/pegawai")."\"><h3>Daftar Pegawai</h3></a>
                        <li class=\"nav_left\"><a href=\"".site_url("register/pegawai")."\"><h3>Buat Baru</h3></a>
                    </ul>
                </div>
                </li>
                <li class=\"nav_left\"><a href=\"".site_url("artikel_controller/articlelist")."\"><h3>Artikel</h3></a></li>
                "; if (ALLOW_REGISTER) echo "<li class=\"nav_left\"><a href=\"".site_url("site/kritiksaranlist")."\"><h3>Kritik dan Saran</h3></a></li>"; echo "
                <hr/>
                "; if (ENABLE_STATISTIK_KUNJUNGAN) echo "<li class=\"nav_left\"><a href=\"".site_url("statistik")."\"><h3>Statistik</h3></a></li>
                <hr/>"; 
                if (ENABLE_ANTREAN) echo "<li class=\"nav_left\"><a href=\"".site_url("jadwal/mingguan")."\"><h3>Jadwal Praktik Dokter</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("site/antre")."\"><h3>Jadwal Harian</h3></a></li>
                <hr/>"; echo "
                <li class=\"nav_left\"><a href=\"".site_url("dokter/rekam_medis_isi")."\"><h3>Buat Rekam Medis</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("dokter/lihatrekam")."\"><h3>Cari Rekam Medis</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("kasir/daftar_pasien")."\"><h3>Kuitansi</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/tindakan")."\"><h3>Tindakan</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("simpel/diagnosis")."\"><h3>Diagnosis</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah_masuk")."\"><h3>Entri Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah")."\"><h3>Tambahkan Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_detil")."\"><h3>Obat Per Tanggal Kadaluarsa</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_menjelang_kadaluarsa")."\"><h3>Daftar Kadaluarsa Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_masuk")."\"><h3>Log Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_keluar")."\"><h3>Log Obat Keluar</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah_log_salemba")."\"><h3>Tambah Log Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_salemba")."\"><h3>Daftar Log Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("resep_controller/kumpulan")."\"><h3>Resep Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/tambah")."\"><h3>Tambahkan Data Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/kumpulan")."\"><h3>Lihat Data Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("surat_jalan_controller/tambah")."\"><h3>Buat Surat Jalan</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";}
                elseif ($this->authentication->is_apoteker()) { echo "
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah_masuk")."\"><h3>Entri Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/tambah")."\"><h3>Tambahkan Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_detil")."\"><h3>Daftar Obat yang Aktif</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_obat_menjelang_kadaluarsa")."\"><h3>Daftar Obat Menjelang Kadaluarsa</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_masuk")."\"><h3>Daftar Log Obat Masuk</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_obat_keluar")."\"><h3>Daftar Log Obat Keluar</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("obat_controller/kumpulan_log_salemba")."\"><h3>Daftar Log Salemba</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("resep_controller/kumpulan")."\"><h3>Daftar Resep Obat</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/tambah")."\"><h3>Tambahkan Data Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("supplier_controller/kumpulan")."\"><h3>Lihat Daftar Supplier</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("surat_jalan_controller/tambah")."\"><h3>Buat Surat Jalan</h3></a></li>
                <hr/>
                <li class=\"nav_left\"><a href=\"".site_url("statistik_controller/statistik_umum")."\"><h3>Statistik Umum</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("statistik_controller/statistik_dokter")."\"><h3>Statistik Dokter</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("statistik_controller/statistik_pasien")."\"><h3>Statistik Pasien</h3></a></li>
                <li class=\"nav_left\"><a href=\"".site_url("laporan")."\"><h3>Laporan</h3></a></li>
                ";}
                elseif ($this->authentication->is_logistik()) { echo "
                
                ";}
                elseif ($this->authentication->is_sekretariat()) { echo "
                
                ";}?>
              </ul>
            </div>
            <?php if (DISPLAY_INFO_UMUM) { ?>
            <div class="info_umum">
              <p class="bold">Jadwal pelayanan PKM UI: <br />Senin-Sabtu<br/>Sesi I: 08.00-12.00 WIB<br/>Sesi II 13.30-17.00 WIB</p>
            </div>
            <?php } ?>
          </div>
          <!-- USER NAV -->
          
          <!-- MAIN CONTENT BEGIN -->
          <div class="side_center">
