 <div class="container">
  <!--Body content-->
  <div class="page-header" style="margin: 150px 0 0 0">
    <h1>Daftar Obat Aktif</h1>
  </div>
  <?php if($this->session->userdata('notif_mess')) { ?>
    <div class="message message-green">
      <p class="p_message"><?php echo $this->session->userdata('notif_mess'); ?> </p>
    </div>
    <?php 
    $this->session->unset_userdata('notif_mess');
    }
     ?>
    <div style="margin: 20px 0 0 0">
     <div>
      <!-- SEARCH -->
      <form action="<?php echo base_url(); ?>index.php/obat_controller/cari_sesuatu" method="post" class="search">

       <input type="text" name="search" id="search_box" placeholder="Cari obat..." class="input_form"/>
       <input type="submit" value="cari" class="button green search_button"/>
     </form>
   </div>  
   <?php if(count($obats->result()) > 0) { ?>

   <table class="table table-hover" style="margin-top: 30px">
    <thead>
      <tr>
        <th class="even-col" style="text-align: center">No</th>
        <th style="text-align: center; width: 170px;">Kategori</th>
        <th class="even-col" style="text-align: center; width: 170px;">Jenis Obat</th>
        <th style="text-align: center">Nama Obat</th>
        <th class="even-col" style="text-align: center">Sediaan</th>
        <th style="text-align: center">Kemasan</th>
        <th class="even-col" style="text-align: center">Total Stok Obat</th>
        <th></th>
        <th class="even-col" style="text-align: center"></th>
      </tr>
    </thead>
    <br/>
    <br/>                      

    <tbody>
      <?php 
      foreach ($obats->result() as $obat) {
       ?>

       <style>
         .habis {
           background-color:#ff8080; 
           color:white; 
         }
       </style>


       <tr <?php if($obat->total_stok < 100) { ?> class="habis" <?php } ?> >

        <td><?php echo ++$mulai; ?></td>
        <td><?php echo $obat->kategori; ?></td>
        <td><?php echo $obat->jenis; ?></td>
        <td><?php echo $obat->nama_obat; ?></td>
        <td><?php echo $obat->sediaan; ?></td>
        <td><?php echo $obat->kemasan; ?></td>
        <td><?php echo ($obat->total_stok == NULL) ? '0' : $obat->total_stok; ?></td>
        <td><a href="<?php echo site_url('obat_controller/ubah_detil/'.$obat->id); ?>" style="color: #51aded"><i class="icon-refresh"></i> Ubah</a></td>
        <td><a href="<?php echo site_url('obat_controller/lihat_detil/'.$obat->id); ?>" style="color: #51aded"><i class="icon-refresh"></i> Detil</a></td>
      </tr>				
      <?php }  ?>
    </tbody>
  </table>
  <?php } else {
    ?>
    <br/>
    <br/>
    <h4> Tidak Ditemukan Daftar Obat yang Masih Aktif </h4>
    <?php } ?>

  </div>
  <?php echo $pagination_links; ?>
</div>
</div>
</div>
</div>