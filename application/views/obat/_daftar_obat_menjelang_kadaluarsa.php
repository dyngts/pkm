<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Daftar Nama Obat Menjelang Kadalursa</h1>
                    <span style="color:red; margin-left:1%">*dihitung 6 bulan dari sekarang</span>
                </div>
                <div style="margin: 20px 0 0 0">
<?php if(count($obats->result()) > 0) { ?>

                    <table border="" class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th rowspan="2" style="text-align: center">No</th>
                                <th rowspan="2" style="text-align: center">Nama Obat</th>
                                <th rowspan="2" style="text-align: center">Tanggal Kadaluarsa</th>
                                <th rowspan="2" style="text-align: center">Jumlah Unit</th>
                            </tr>
                        </thead>
                        <tbody>


                            <?php 
                            foreach ($obats->result() as $obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $obat->nama_obat; ?></td>
                                <td style="text-align: center"><?php echo $obat->tgl_kadaluarsa; ?></td>
                                <td style="text-align: center"><?php echo $obat->jml_satuan; ?></td>
                                
                            </tr>               
                            <?php }  ?>
                        </tbody>
                    </table>
 <?php 
 echo $pagination_links;
} else {
		  ?>
          <br/>
          <br/>
		  <h3 style="margin-left:1%"> Tidak ditemukan daftar obat yang tanggal kadaluarsanya sebentar lagi </h3>
		  <?php } ?>

              </div>
                
        </div>
</div>