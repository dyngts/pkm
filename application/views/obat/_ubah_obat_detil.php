 <div class="container">
   <!--Body content--> 
   <div class="page-header" style="margin: 150px 0 0 0">
       <h1>Ubah Obat </h1>
   </div>
   <?php
	// $temp = explode('-', $obat->tgl_kadaluarsa);
	// $tgl_asli = '';
 //    $i = count($temp) - 1;
	// for(; $i > -1; --$i) {
 //        $bulan = '';
	// 	if($i < 2) {
 //            if($i == 1) {
 //                switch ($temp[$i]) {
 //                case '01':
 //                    $bulan = 'Jan';
 //                    break;

 //                case '02':
 //                    $bulan = 'Feb';
 //                    break;

 //                case '03':
 //                    $bulan = 'Mar';
 //                    break;

 //                case '04':
 //                    $bulan = 'Apr';
 //                    break;

 //                case '05':
 //                    $bulan = 'May';
 //                    break;

 //                case '06':
 //                    $bulan = 'Jun';
 //                    break;

 //                case '07':
 //                    $bulan = 'Jul';
 //                    break;

 //                case '08':
 //                    $bulan = 'Aug';
 //                    break;

 //                case '09':
 //                    $bulan = 'Sep';
 //                    break;

 //                case '10':
 //                    $bulan = 'Oct';
 //                    break; 

 //                case '11':
 //                    $bulan = 'Nov';
 //                    break;

 //                case '12':
 //                    $bulan = 'Dec';
 //                    break;               
 //                }
 //                $tgl_asli = $tgl_asli.'-'.$bulan;
 //            }
 //            else {
 //                 $tgl_asli = $tgl_asli.'-'.$temp[$i];
 //            }

	// 	}
	// 	else {
	// 	  $tgl_asli = $temp[$i];
	// 	}
	// }

   ?>

   <style type="text/css">

   .isian {
    margin-top: -0.1%;
   }

   </style>

   <div id="tableForm" style="margin: 15px 0 0 0">
    <?php echo form_open('obat_controller/ubah_detil/'.$obat->id_obat, array('class' => 'form-horizontal', 'name' => 'ubah', 'id' => 'ubah')); ?>
    <p><b style="color:red; font-size: 16px">* Harus Diisi</b></p>

    <div class="control-group">

        <label class="control-label" for="inputEmail">Nama Obat <b style="color:red; font-size: 15px">*</b></label>

        <div class="isian">
            <input type="text" placeholder="Nama Obat" id="nama_obat" name="nama_obat" maxlength="50" value="<?php echo set_value('nama_obat', $obat->nama_obat); ?>" >
            <div style="color:red;margin-left:21%;"><?php echo form_error('nama_obat'); ?></div>  
        </div>

    </div>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Kategori<b style="color:red; font-size: 15px">*</b></label>
        <div class="isian"> <?php echo form_dropdown('kategori', $data_kategori, $obat->kategori, 'id="first-choice"'); ?> </div>
        <div style="color:red;margin-left:3%;"><?php echo form_error('kategori'); ?></div> 
    </div>

    <?php  $temp = explode(' ', $obat->jenis);
                 $jenis_asli = '';
                 for($i = 0; $i < count($temp); ++$i) {
                  if($i > 0) {
                      $jenis_asli = $jenis_asli.'-'.$temp[$i];
                  }
                  else {
                      $jenis_asli = $temp[$i];
                  }
                }
    ?>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Jenis Obat<b style="color:red; font-size: 15px">*</b> </label>
        <div class="isian" style="margin-top: 9px">
            <?php echo form_dropdown('jenis', $data_jenis_obat, $jenis_asli, 'id="second-choice"'); ?>
        </div>
        <div style="color:red;margin-left:21%;"><?php echo form_error('jenis'); ?></div> 
    </div>


    <div class="control-group">
        <label class="control-label" for="inputPassword">Komposisi</label>
        <div class="isian">
            <textarea rows="20" cols="10000" id="komposisi" name="komposisi" placeholder="Komposisi"><?php echo $obat->komposisi; ?></textarea>
            <div style="color:red;margin-left:21%;"><?php echo form_error('komposisi'); ?></div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Sediaan<b style="color:red; font-size: 15px">*</b></label>
        <div class="isian"> <?php echo form_dropdown('sediaan', $data_sediaan_obat, $obat->sediaan); ?> 
            <div style="color:red;margin-left:21%;"><?php echo form_error('sediaan'); ?></div>
        </div>
    </div>

    <div class="control-group">

        <label class="control-label" for="inputEmail">Kemasan <b style="color:red; font-size: 15px">*</b></label>

        <div class="isian">
            <input type="text" placeholder="Kemasan" id="kemasan" name="kemasan" maxlength="30" value="<?php echo set_value('kemasan', $obat->kemasan); ?>">
            <div style="color:red;margin-left:21%;"><?php echo form_error('kemasan'); ?></div>  
        </div>

    </div>

    <div class="control-group">

        <label class="control-label" for="inputEmail">Satuan per Kemasan</label>

        <div class="isian">
            <input type="text" placeholder="Satuan per Kemasan" id="satuan_per_kemasan" name="satuan_per_kemasan" maxlength="12" value="<?php echo set_value('satuan_per_kemasan', $obat->satuan_per_kemasan); ?>">
            <div style="color:red;margin-left:21%;"><?php echo form_error('satuan_per_kemasan'); ?></div>  
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputPassword">Harga per Satuan</label>
        <div class="isian">
            <input type="int" id="harga_satuan" name="harga_satuan" placeholder="Harga Satuan" maxlength="12" value="<?php echo set_value('harga_satuan', $obat->harga_satuan); ?>" >
            <div style="color:red;margin-left:21%;"><?php echo form_error('harga_satuan'); ?></div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputPassword">Harga per Kemasan</label>
        <div class="isian">
            <input type="int" id="harga_per_kemasan" name="harga_per_kemasan" placeholder="Harga Per Kemasan" maxlength="12" value="<?php echo set_value('harga_per_kemasan', $obat->harga_per_kemasan); ?>" >
            <div style="color:red;margin-left:21%;"><?php echo form_error('harga_per_kemasan'); ?></div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Status Pemakaian<b style="color:red; font-size: 15px">*</b></label>
        <div class="isian">
            <?php echo form_dropdown('status_pemakaian', array('aktif'=>'Aktif', 'tidak'=>'Tidak Aktif'), ($obat->status_pemakaian == 1 ? 'aktif' : 'tidak')); ?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Supplier</br></label>
        <div class="isian" style="margin-top: 9px">
            <?php echo form_dropdown('supplier', $data_supplier, $obat->id_supplier, 'id="supplier"'); ?>
        </div>
    </div>

    <div class="form-actions" style="background-color: #D2D1D1">
       <input type="submit" class="button green" title="simpan" value="Simpan" id="simpan" name="simpan" /> 
       <input type="submit" class="button green" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" /> 
   </div>


   <?php echo form_close(); ?></div>
    <script type = "text/javascript">
       $("#first-choice").change(function() {
           $("#second-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_jenis'); ?>" + "/" + $('#first-choice').val());
           //alert($('#first-choice').val());
       });

    </script>

<script type="text/javascript">
    function cek_supplier() {

        var a = document.getElementById("supplier").value;
        if(!a) {
            window.alert("Nama Supplier Tidak Terpilih");
        }
    }
</script>
</div>
</div>