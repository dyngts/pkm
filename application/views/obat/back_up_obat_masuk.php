<div class="container">
	<!--Body content-->
	<div class="page-header" style="margin: 150px 0 0 0">
		<h1>Entri Obat Masuk</h1>
	</div>
	<div id="tableForm" style="margin: 12px 0 0 0">
		<?php echo count($data_jenis_obat); ?>
		<?php echo count($data_nama_obat); ?>
        <?php echo form_open('obat_controller/tambah_masuk', array('class' => 'form-horizontal', 'name' => 'tambah', 'id' => 'tambah')); ?>
        	<p><b style="color:red; font-size: 16px">* Harus Diisi</b></p>
        	<div class="control-group">
        		<label class="control-label" for="inputEmail">Sumber Obat <b style="color:red; font-size: 12px">*</b></br>
        	</label>
        	<div class="controls" style="margin-top: 9px">
        		<?php echo form_dropdown('sumber_obat', array(''=>'Pilih Sumber Obat','Supplier'=>'Supplier', 'Event'=>'Event')); ?>
			<div style="color:red;margin-left:3%;"><?php echo form_error('sumber_obat'); ?></div>
        	</div>
        		
        	</div>
        	<div class="control-group">
        	<label class="control-label" for="inputEmail">Kategori <b style="color:red; font-size: 12px">*</b></label>
        		<div class="controls"> <?php echo form_dropdown('kategori', $data_kategori, $kategori, 'id="first-choice"'); ?> 
        			<div style="color:red;margin-left:3%;"><?php echo form_error('kategori'); ?></div>
        		</div>

        	</div>

	         



         	<div class="control-group">
             	<label class="control-label" for="inputEmail">Jenis Obat <b style="color:red; font-size: 12px">*</b></br>
         		</label>
         		<div class="controls" style="margin-top: 9px">
         		<?php if(count($data_jenis_obat) == 1 && $kategori == NULL) { ?>
            		<select id="second-choice" name="jenis">
            			<option value="">Pilih Kategori Dulu</option>
            		</select>
            	<?php } else { ?>
            		<?php echo form_dropdown('jenis', $data_jenis_obat, $jenis, 'id="second-choice"'); ?> 
            	<?php } ?>
            		<div style="color:red;margin-left:3%;"><?php echo form_error('jenis'); ?></div>
         		</div>
     		</div>


			<div class="control-group">
             	<label class="control-label" for="inputEmail">Nama Obat <b style="color:red; font-size: 12px">*</b></br>
         		</label>
				<div class="controls" style="margin-top: 9px">
				<?php if(count($data_nama_obat) == 1 && $kategori == NULL) { ?>
            		<select id="third-choice" name="nama_obat">
            			<option value="">Pilih Jenis Obat Dulu</option>
            		</select>
            	<?php } else { ?>
            		<?php echo form_dropdown('nama_obat', $data_nama_obat, $nama_obat, 'id="third-choice"'); ?> 
            	<?php } ?>
					<div style="color:red;margin-left:3%;"><?php echo form_error('nama_obat'); ?></div>
				</div>
			</div>

      <div class="row">
          <div class="row">
              <div class="label">
                  <label>Tanggal Kadaluarsa<strong class="required">*</strong><br/><em class="english">Expired Date</em></label>
              </div>
              <div class="input">
                  <p class="p_form" id="p_birthday"></p>
                  <div id="place" class="input">
                      <input type="text" id="demo3" class="input_form" value="<?php echo set_value('demo3'); ?>" name="demo3" style="display:none">
                      <input type="text" id="demo4" class="input_form_disabled" value="<?php echo set_value('tanggal'); ?>" name="tanggal" readonly="readonly">
                      <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
                      <script src="<?php echo base_url('js/datetimepicker_css.js');?>" type="text/javascript"></script>
                      <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo3','yyyyMMdd','','','','','future')" style="cursor:pointer"/>
                  </div>
              </div>
              <div class="note">
                  <span><label>Masukkan tanggal kadaluarsa dengan menekan tombol "pilih tanggal".</label></span>
              </div>
          </div>
      </div>

			<div class="control-group">
				<label class="control-label" for="inputPassword">Jumlah Obat Masuk <b style="color:red; font-size: 12px">*</b></label>
				<div class="controls">
					<input type="int" id="inputPassword" name="jml_obat_masuk" placeholder="Jumlah Obat Masuk" maxlength="12" style="background-color: #EAEDF1; height: 30px; width: 200px" value="<?php echo set_value('jml_obat_masuk', $jml_satuan); ?>">
					</br>

					<div style="color:red;margin-left:3%;"><?php echo form_error('jml_obat_masuk'); ?></div>
				</div>
			</div>
			<div class="form-actions" style="background-color: #D2D1D1">
				<input type="submit" class="btn btn-primary" title="simpan" value="Simpan" id="simpan" name="simpan" />				
				<input type="submit" class="btn btn-primary" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" />		
			</div>
		</form>
	</div>

	<script type="text/javascript">
     	$("#first-choice").click(function() {
			$("#second-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_jenis'); ?>" + "/" + $('#first-choice').val());
			$("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#first-choice').val());
		});

	</script>

	<script type="text/javascript">
		$("#second-choice").click(function() {
			$("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#second-choice').val());
		});
	</script>
</div>
</div>