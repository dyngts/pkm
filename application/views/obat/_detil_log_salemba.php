<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Detil Log Obat Salemba - <?php echo $this->uri->segment(3); ?></h1>
                </div>
                <style type="text/css">
                .detil {
                    background-color: #008F08;
                    color: white;
                    width:35%;
                    text-align: left;
                }

                </style>
                <div style="margin: 20px 0 0 0">
                    <table class="table table-hover" style="margin-top: 30px; width:50% ">
                        <tbody>
                            <tr>
                                <td class="detil" ><b>Tanggal</b></td>
                                <td><?php echo $datas->tgl; ?></td>
                            </tr>
                            <tr>
                                <td class="detil"><b>Jam</b></td>
                                <td><?php echo $datas->jam; ?></td>
                            </tr>
                            <tr>
                                <td class="detil"><b>Deskripsi</b></td>
                                <td><?php echo $datas->deskripsi; ?></td>
                            </tr>
                            <tr>
                                <td class="detil"><b>Jenis Transaksi</b></td>
                                <td><?php echo $datas->jenis; ?></td>
                            </tr>
                            <tr>
                                <td class="detil"><b>Apotek yang Bertugas</b></td>
                                <td><?php echo $datas->username_pegawai; ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <?php  if($obats->num_rows() > 0) {
                    $mulai = 0; ?>
                    <table class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th style="text-align: center">No</th>
                                <th class="even-col" style="text-align: center">Nama Obat</th>
                                <th style="text-align: center">Jumlah Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($obats->result() as $obats_obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $obats_obat->nama_obat; ?></td>
                                <td><?php echo $obats_obat->jml_satuan ?></td>
                            </tr>           
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <h2>Tidak ditemukan daftar log salemba</h2>
                <?php } ?>

                </div>
            </div>
        </div>

        <script type="text/javascript">
        function tambah_form() {
            var form = document.getElementById("form");
            var elem = document.getElementById("tambah");
            form.innerHTML = '<form method="post" onSubmit="return valid_form_faktur()" enctype="multipart/form-data" action="<?php echo $alamat1; ?>"><div class="control-group"><label>Nomor Faktur</label><input type="text" placeholder="Nomor Faktur" id="no_faktur" name="no_faktur" style="background-color: #EAEDF1; height: 30px; width: 215px" maxlength="20" /></div><div class="control-group"><label>File Faktur</label><input type="file" name="file" id="file" style="background-color: #EAEDF1; height: 30px; width: 215px"/></br><input type="submit" value="Tambah" class="button green"/></div></form>';
            elem.parentNode.removeChild(elem);
        }

        function update_form() {
            var form = document.getElementById("form");
            var elem = document.getElementById("tambah");
            form.innerHTML = '<form method="post" onSubmit="return valid_form_faktur()" enctype="multipart/form-data" action="<?php echo $alamat2; ?>"><div class="control-group"><label>Nomor Faktur</label><input type="text" placeholder="Nomor Faktur" id="no_faktur" name="no_faktur" style="background-color: #EAEDF1; height: 30px; width: 215px" maxlength="20" /></div><div class="control-group"><label>File Faktur</label><input type="file" name="file" id="file" style="background-color: #EAEDF1; height: 30px; width: 215px"/></br><input type="submit" value="Tambah" class="button green"/></div></form>';
            elem.parentNode.removeChild(elem);
        }

        function valid_form_faktur() {
            var no_faktur = document.getElementById("no_faktur");
            var file_faktur = document.getElementById("file");

            if(no_faktur.value == "") {
                alert("Nomor faktur harus diisi");
                return false;
            } else if(file_faktur.value == "") {
                alert("Pilih file faktur yang akan di upload");
                return false;
            }


            var nama_faktur = file_faktur.value;

            var ext = nama_faktur.substring(nama_faktur.lastIndexOf('.') + 1);

            if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png" || ext == "")
            {

            }
            else {
                alert("Extension faktur yang dibolehkan adalah .gif, .jpeg, .jpg, .gif, dan .png");
                return false;
            }

            var filesize = file_faktur.files[0].size;

            if((filesize/(1024*1024)) > 1) {
                alert("Ukuran faktur pembelian terlalu besar, tidak boleh melebihi 1 MB");
                return false;
            }

        }


        </script>



