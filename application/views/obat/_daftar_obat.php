 <div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Daftar Obat</h1>
                </div>
                <div style="margin: 20px 0 0 0">
		  <?php if(count($obats->result()) > 0) { ?>
                    <table class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th style="text-align: center">No</th>
                                <th style="text-align: center">Nama Obat</th>
                                <th style="text-align: center">Kategori Obat</th>
                                <th style="text-align: center">Jenis Obat</th>
                                <th style="text-align: center">Satuan</th>
                                <th style="text-align: center">Harga per Satuan</th>
                                <th style="text-align: center">Nama Supplier</th>
                                <th style="text-align: center">Total Unit Keseluruhan</th>
                                <th style="text-align: center">Status Pemakaian</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($obats->result() as $obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $obat->nama_obat; ?></td>
                                 <td><?php echo $obat->kategori; ?></td>
                                <td><?php echo $obat->jenis; ?></td>
                                <td style="text-align: center"><?php echo $obat->sediaan; ?></td>
                                <td style="text-align: center"><?php echo $obat->harga_satuan; ?></td>
                                <td style="text-align: center"><?php echo $obat->nama_supplier; ?></td>
                                <td style="text-align: center"><?php echo $obat->total_obat_satuan; ?></td>
                                <td style="text-align: center"><?php echo ($obat->status_pemakaian == 1 ? 'Aktif' : 'Tidak Aktif' ); ?></td>
                                <td><a href="<?php echo site_url('index.php/obat_controller/ubah/'.$obat->id_obat); ?>" style="color: #51aded"><i class="icon-refresh"></i> Ubah</a></td>
                                
                            </tr>				
                            <?php }  ?>
                        </tbody>
                    </table>
		  <?php } else {
		  ?>
		  <h4> Tidak Ditemukan Daftar Obat yang Masih Aktif </h4>
		  <?php } ?>
                </div>
                 <?php echo $pagination_links; ?>
            </div>
           </div>
       </div>