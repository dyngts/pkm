<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Daftar Obat yang Sudah Kadaluarsa</h1>
                </div>
                
                <div style="margin: 20px 0 0 0">
                    <?php if($jumlah_baris > 0 ) { ?>
                    <table border="" class="table table-hover" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th rowspan="2" style="text-align: center">No</th>
                                <th rowspan="2" style="text-align: center">Jenis Obat</th>
                                <th rowspan="2" style="text-align: center">Nama Obat</th>
                                <th rowspan="2" style="text-align: center">Satuan</th>
                                <th rowspan="2" style="text-align: center">Tanggal Kadaluarsa</th>
                                <th rowspan="2" style="text-align: center">Jumlah Unit</th>
                            </tr>
                        </thead>
                        <tbody>


                            <?php 
                                foreach ($obats as $obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $obat->jenis; ?></td>
                                <td><?php echo $obat->nama_obat; ?></td>
                                <td style="text-align: center"><?php echo $obat->sediaan; ?></td>
                                <td style="text-align: center"><?php echo $obat->tgl_kadaluarsa; ?></td>
                                <td style="text-align: center"><?php echo $obat->jml_satuan; ?></td>
                                
                            </tr>               
                            <?php }
                            }
                            else {  ?>

                            <tr >
                                <td><h3>Tidak Ada Daftar Obat yang Telah Kadaluarsa</h3></td>
                            </tr>

                            <?php } ?>
                        </tbody>
                    </table>
              </div>
              <?php echo $pagination_links; ?>    
        </div>
</div>