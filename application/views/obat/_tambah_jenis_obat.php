<div class="container">
    <!--Body content-->

    <script type="text/javascript">
      $(function() {
        $('span').live('click', function() {
                $('<div class="control-group"><label class="control-label" for="inputPassword">Nama Jenis Obat</label><div class="controls"><input id="item" name="jenis[]" type="text"size="15" maxlength="10" style="background-color: #EAEDF1; height: 30px; float: right; margin-left: 6px; margin-top: 3px" /></div></div>').appendTo('#fields');
        });
        $('.minus').live('click', function() {
                        $(this).parents('#isi').remove();
        });
});
    </script>

    <div class="page-header" style="margin: 150px 0 0 0">
        <h1>Tambahkan Jenis Obat Baru</h1>
    </div>
    <div id="tableForm" style="margin: 20px 0 0 0">
        <form class="form-horizontal">
            <div id="fields">
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Nama Jenis Obat</label>
                    <div class="controls">
                        <input id="item" name="jenis[]" type="text"size="15" maxlength="10" style="background-color: #EAEDF1; height: 30px; float: right; margin-left: 6px; margin-top: 3px" />
                    </div>
                </div>
            </div>
            <div id="" style="font-size: 12px">
                <a>
                    <span> <i class="icon-plus-sign"></i>
                        Tambahkan Jenis Obat
                    </span>
                </a>
            </div>

            <div class="form-actions" style="background-color: #D2D1D1">
                <a href="tambahkan-jenis-obat-confirm-page.html">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </a>
                <a href="tambahkan-jenis-obat.html">
                    <button type="button" class="btn btn-primary" style="margin-left: 30px">Batal</button>
                </a>
            </div>
        </form>

    </div>
</div>
</div>