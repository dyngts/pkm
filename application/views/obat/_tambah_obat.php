 
 <div class="container">
   <!--Body content--> 
   <div class="page-header" style="margin: 150px 0 0 0">
       <h1>Tambahkan Obat Baru</h1>
   </div>

   <div id="tableForm" style="margin: 15px 0 0 0">

    <?php echo form_open('obat_controller/tambah', array('class' => 'form-horizontal', 'name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_input()')); ?>

    <div class="control-group">

        <label class="control-label" for="inputEmail">Nama Obat <b style="color:red; font-size: 15px">*</b></label>

        <div class="isian">
            <input type="text" placeholder="Nama Obat" id="nama_obat" name="nama_obat"  maxlength="50" value="<?php echo set_value('nama_obat', $nama_obat); ?>" >
            <div class="error"><?php echo form_error('nama_obat'); ?></div>  
        </div>

    </div>

    <div class="control-group">
        <label class="control-label" for="inputEmail">Kategori<b style="color:red; font-size: 15px">*</b></label>
        <div class="isian"> <?php echo form_dropdown('kategori', $data_kategori, $kategori, 'id="first-choice"'); ?> 
           <div class="error"><?php echo form_error('kategori'); ?></div>
       </div>

   </div>

   <?php 
        $jenis_asli = str_replace('-', ' ', $jenis);
   ?>

   <style type="text/css">

   .isian {
    margin-top: -0.1%;
   }

   .error {
    margin-left: 21%;
    color:red;
   }

   </style>

   <div class="isian">
    <label class="control-label" for="inputEmail">Jenis Obat<b style="color:red; font-size: 15px">*</b></br> </label>
    <div class="isian">  
     <select id="second-choice" name="jenis" >
         <option value= ""> Pilih Kategori Dulu </option>
         <?php 
         if(count($data_jenis_obat) > 0) {
         foreach ($data_jenis_obat as $jenis_obat) { ?>
         <option value="<?php echo $jenis_obat->jenis; ?>" <?php if($jenis_asli == $jenis_obat->jenis) { ?> selected = "selected" <?php } ?> > <?php echo $jenis_obat->jenis; ?> </option>
         <?php } 
         }?>
     </select>
     <div class="error"><?php echo form_error('jenis'); ?></div>
 </div>

</div>
<div class="control-group">
    <label class="control-label" for="inputPassword">Komposisi</label>
    <div class="isian">
        <textarea name="komposisi" style="height: 100px; width: 215px" placeholder="Komposisi"><?php echo $komposisi; ?></textarea>
    </div>
    <div class="error"><?php echo form_error('komposisi'); ?></div>
</div>


<div class="control-group">
    <label class="control-label" for="inputEmail">Sediaan<b style="color:red; font-size: 15px">*</b></label>
    <div class="isian"> <?php echo form_dropdown('sediaan', $data_sediaan_obat, $sediaan); ?> 
        <div class="error"><?php echo form_error('sediaan'); ?></div>
    </div>
</div>

<div class="control-group">

    <label class="control-label" for="inputEmail">Kemasan <b style="color:red; font-size: 15px">*</b></label>

    <div class="isian">
        <input type="text" placeholder="Kemasan" id="kemasan" name="kemasan"  maxlength="30" value="<?php echo set_value('kemasan', $kemasan); ?>">
        <div class="error"><?php echo form_error('kemasan'); ?></div>  
    </div>

</div>

<div class="control-group">

    <label class="control-label" for="inputEmail">Satuan per Kemasan</label>

    <div class="isian">
        <input type="text" placeholder="Satuan per Kemasan" id="satuan_per_kemasan" name="satuan_per_kemasan" maxlength="12"  value="<?php echo set_value('satuan_per_kemasan', $satuan_per_kemasan); ?>">
        <div class="error"><?php echo form_error('satuan_per_kemasan'); ?></div>  
    </div>

</div>

<div class="control-group">
    <label class="control-label" for="inputPassword">Harga per Satuan</label>
    <div class="isian">
        <input type="int" id="harga_satuan" name="harga_satuan" placeholder="Harga Satuan" maxlength="12"  value="<?php echo set_value('harga_satuan', $harga_satuan); ?>" >
        <div class="error"><?php echo form_error('harga_satuan'); ?></div>
    </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="inputPassword">Harga per Kemasan</label>
        <div class="isian">
            <input type="int" id="harga_per_kemasan" name="harga_per_kemasan" placeholder="Harga Per Kemasan" maxlength="12"  value="<?php echo set_value('harga_per_kemasan', $harga_per_kemasan); ?>" >
            <div class="error"><?php echo form_error('harga_per_kemasan'); ?></div>
        </div>
        </div>

       <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Supplier</label>
        <div class="isian">
            <?php echo form_dropdown('supplier', $data_supplier, $supplier); ?>
            <div class="error"><?php echo form_error('supplier'); ?></div>
        </div>
        </div>

    </div>

    <script type="text/javascript">
        $("#first-choice").change(function() {
            $("#second-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_jenis'); ?>" + "/" + $('#first-choice').val());
            //alert($('#first-choice').val());
        });

    </script>


    <div class="form-actions" style="background-color: #D2D1D1">
       <input type="submit" class="button green" title="simpan" value="Simpan" id="simpan" name="simpan" onClick="return confirm( 'Yakin ingin menambahkan data ini?' )" /> 
       <input type="submit" class="button green" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" /> 
    </div>
   <?php echo form_close(); ?></div>

</div>
</div>
