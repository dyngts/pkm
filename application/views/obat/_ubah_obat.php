 <div class="container">
     <!--Body content--> 
     <div class="page-header" style="margin: 150px 0 0 0">
         <h1>Ubah Obat </h1>
     </div>

     <div id="tableForm" style="margin: 20px 0 0 0">
        <?php echo validation_errors(); ?> 
        <?php echo form_open('index.php/obat_controller/ubah/'.$obat->id_obat, array('class' => 'form-horizontal', 'name' => 'ubah', 'id' => 'ubah')); ?>

         <div class="control-group">
            <label class="control-label" for="inputEmail">Sumber Obat</br> </label>
            <div class="controls" style="margin-top: 9px"> <?php echo form_dropdown('sumber_obat', array('Supplier'=>'Supplier', 'Event'=>'Event') ); ?> </div>
         </div>

        <div class="control-group">
            <label class="control-label" for="inputEmail">Nama Obat</label>
            <div class="controls">
                <input type="text" placeholder="Nama Obat" id="nama_obat" name="nama_obat" value="<?php echo $obat->nama_obat; ?>" style="background-color: #EAEDF1; height: 30px; width: 220px">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputEmail">Kategori</label>
            <div class="controls"> <?php echo form_dropdown('kategori', $data_kategori, $obat->kategori); ?> </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputEmail">Jenis Obat</br> </label>
            <div class="controls" style="margin-top: 9px"> <?php echo form_dropdown('jenis', $data_jenis_obat, $obat->jenis); ?> </div>
        </div>


        <div class="control-group">
            <label class="control-label" for="inputPassword">Komposisi</label>
            <div class="controls">
                <textarea rows="20" cols="10000" id="komposisi" name="komposisi" style="background-color: #EAEDF1; height: 30px; width: 220px" placeholder="Komposisi"><?php echo $obat->komposisi; ?></textarea>
            </div>
        </div>


        <div class="control-group">
            <label class="control-label" for="inputEmail">Sediaan</label>
            <div class="controls"> <?php echo form_dropdown('sediaan', $data_sediaan_obat, $obat->sediaan); ?> </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Harga per Satuan</label>
            <div class="controls">
                <input type="int" id="harga_satuan" name="harga_satuan" placeholder="Harga Per Satuan" value="<?php echo $obat->harga_satuan; ?>" style="background-color: #EAEDF1; height: 30px; width: 220px"></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputEmail">Status Pemakaian</label>
            <div class="controls">
            <?php echo form_dropdown('status_pemakaian', array('aktif'=>'Aktif', 'tidak'=>'Tidak Aktif'), ($obat->status_pemakaian == 1 ? 'aktif' : 'tidak')); ?>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="inputEmail">Nama Supplier</br></label>
            <div class="controls" style="margin-top: 9px">
                <?php echo form_dropdown('supplier', $data_supplier, $obat->id_supplier); ?>
            </div>
        </div>
 
 <div class="form-actions" style="background-color: #D2D1D1">
     <input type="submit" class="btn btn-primary" title="simpan" value="Simpan" id="simpan" name="simpan" /> 
     <input type="submit" class="btn btn-primary" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" /> 
 </div>
 <?php echo form_close(); ?></div>
 </div>
 </div>