<div class="container">
	<!--Body content-->
	<div class="page-header" style="margin: 150px 0 0 0">
		<h1>Entri Obat Masuk</h1>
	</div>

  <script type="text/javascript">
    $(function() {

      var i = 1;

      
      var in2 = "'yyyyMMdd'";
      var in3 = "''";
      var in4 = "''";
      var in5 = "''";
      var in6 = "''";
      var in7 = "'future'";

      
      $('#tambah').on('click', function() {
        i++;
        var in1 = "'demo4"+i+"'";
        $('<table id="field'+i+'" style="border:1px solid #CCC; padding: 10px 8px; border-radius:0;"><tr><td><label>Kategori</label></td><td><select id="first-choice'+i+'"><option value="">Pilih Kategori Obat</option><option value="Obat-Umum">Obat Umum</option><option value="Obat-Alkes-dan-BMHP-Gigi">Obat, Alkes, dan BMHP Gigi</option><option value="Alkes-dan-BMHP-Umum">Alkes dan BMHP Umum</option><option value="Obat-Emergency">Obat Emergency</option></select></td><td><label for="inputPassword">Jenis Obat</label></td><td><select id="second-choice'+i+'" name="jenis"><option value="">Pilih Kategori Dulu</option></select></td></tr><tr><td><label for="inputPassword">Nama Obat<b style="color:#C40000; font-size: 12px;">*</b></label></td><td style="padding-left:8px;"><select id="third-choice'+i+'" name="nama_obat[]"><option value="">Pilih Jenis Obat Dulu</option></select></td><td><label for="inputPassword">Tanggal Kadaluarsa <b style="color:#C40000; font-size: 12px;">*</b><span id=""></span></label></td><td style="padding-left:6px"><p class="p_form" id="p_birthday"></p><div id="place" class="input"><input type="text" id="demo4'+i+'" class="input_form_disabled" value="" name="tanggal[]" readonly="readonly"><link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css"><script src="<?php echo base_url("js/datetimepickers_css.js");?>" type="text/javascript"><\/script></div></td><td><input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('+in1+','+in2+','+in3+','+in4+','+in5+','+in6+','+in7+')" style="cursor:pointer"/></td></tr><tr><td><label for="inputPassword">Jumlah Unit Obat Keluar<b style="color:#C40000; font-size: 12px;">*</b><span id=""></span></label></td><td style="padding-left:6px"><input type="text" name="jml_obat_masuk[]" maxlength="12" style=""></td></tr></table><script type="text/javascript">$("#first-choice'+i+'").click(function(){$("#second-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_jenis");?>"+"/"+$("#first-choice'+i+'").val());$("#third-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_nama"); ?>" + "/" + $("#second-choice'+i+'").val());});<\/script><script type="text/javascript">$("#second-choice'+i+'").click(function(){$("#third-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_nama"); ?>"+"/"+$("#second-choice'+i+'").val());});<\/script>').appendTo('.isi');
        
      });

$('#kurang').on('click', function() {
  if(i > 1) {
    $("#field"+i).remove();
    i--;
  }
});

});


</script>

<style>

  .isian {
    margin-top: -0.1%;
  }

</style>

<div id="tableForm" style="margin: 12px 0 0 0">
  <?php echo form_open_multipart('obat_controller/tambah_masuk', array('class' => 'form-horizontal', 'name' => 'entri', 'id' => 'entri', 'onsubmit' => 'return cek_input()')); ?>
  <p><b style="color:#C40000; font-size: 16px">* Harus Diisi</b></p>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Sumber Obat <b style="color:#C40000; font-size: 12px">*</b></br>
    </label>
    <div class="isian">
      <select name="sumber_obat[]" id="sumber_obat">
        <option value="">Pilih Sumber Obat</option>
        <option value="Supplier">Supplier</option>
        <option value="Event">Event</option>
      </select>
      <div style="color:#C40000;margin-left:3%;"><?php echo form_error('sumber_obat'); ?></div>
    </div>
    <br/>


    <div class="isi">
     <table style="border:1px solid #CCC; padding: 10px 8px; border-radius:0;">
       <tr>
         <td>
           <label>Kategori</label>
         </td>
         <td>
          <?php echo form_dropdown('kategori', $data_kategori, $kategori, 'id="first-choice"'); ?> 
        </td>
        <td>
         <label for="inputPassword">Jenis Obat</label>
       </td>
       <td>
        <select id="second-choice" name="jenis">
          <option value="">Pilih Kategori Dulu</option>
        </select>
      </td>
    </tr>
    <tr>
     <td>
       <label for="inputPassword">Nama Obat<b style="color:#C40000; font-size: 12px;">*</b></label>
     </td>
     <td style="padding-left:8px;">
      <select id="third-choice" name='nama_obat[]'>
        <option value=''>Pilih Jenis Obat Dulu</option>
      </select>
    </td>

    <td>
      <label for="inputPassword">
       Tanggal Kadaluarsa <b style="color:#C40000; font-size: 12px;">*</b>
       <span id=""></span>
     </label>
   </td>
   
   <script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
   <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
   <td style="padding-left:6px">
    <p class="p_form" id="p_birthday"></p>
    <div id="place" class="input">
      <input type="text" id="demo4" class="input_form_disabled"  name="tanggal[]" readonly="readonly">
      
    </div>
  </td>
  <td>
   <input type="button" class="pilihtanggal" value="pilih tanggal" onclick="javascript:NewCssCal('demo4','yyyyMMdd','','','','','future')" style="cursor:pointer"/>
 </td>
</tr>

<td>
 <label for="inputPassword">
   Jumlah Unit Obat Masuk <b style="color:#C40000; font-size: 12px;">*</b>
   <span id=""></span>
 </label>
</td>
<td style="padding-left:6px">
 <input type="text" name="jml_obat_masuk[]"  style="" maxlength="12" > 
</td>
</tr>
</table>

</div>
</div>

<table style="border:0; margin-left:1%">
 <tr>
   <td style="background-color: #1ABB9B">
     <div id="" style="font-size: 12px; ">
       <a>
         <span id="tambah" style="background-color: #1ABB9B; color:white;">
           +
         </span>
       </a>
     </div>  
   </td>
   <td style="background-color: #C40000">
    <div id="" style="font-size: 12px;">
     <a>
       <span id="kurang" style="background-color: #C40000; color:white;">
         -
       </span>
     </a>
   </div> 
 </td>
</tr>
</table>

<div  class="isian">
  <label class="control-label">Nomor Faktur
  </label>
  <div class="controls" style="margin-top: -0.3%">
    <input type="text" name="no_faktur[]" maxlength="20" />
  </div>
  <br/>

  <div  class="isian">
    <label class="control-label">Faktur Pembelian
    </label>
    <div class="controls" style="margin-top: -0.3%">
      <input type="file" multiple name="file[]" style="border-style:none;" />
      <br>
      <em style="margin-left:3%">Tipe file (.gif, .jpeg, .jpg, .gif, dan .png), Max. size 1 MB</em>
    </div>
    <br/>


    <div class="form-actions" >
      <input type="submit" class="button green" title="simpan" value="Simpan" id="simpan" name="simpan" />
    </div>
  </form>
</div>

<script type="text/javascript">
  $("#first-choice").click(function() {
   $("#second-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_jenis'); ?>" + "/" + $('#first-choice').val());
   $("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#first-choice').val());
		});

</script>

<script type="text/javascript">
  $("#second-choice").click(function() {
   $("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#second-choice').val());
		});
</script>


<script type="text/javascript">

  function cek_input() {
    var sumber_obat = document.getElementsByName("sumber_obat[]");
    var nama_obat = document.getElementsByName("nama_obat[]");
    var tgl_kadaluarsa = document.getElementsByName("tanggal[]");
    var jml_obat_masuk = document.getElementsByName("jml_obat_masuk[]");
    var no_faktur = document.getElementsByName("no_faktur[]");
    var faktur = document.getElementsByName("file[]");

    var n = nama_obat.length;

    if(sumber_obat[0].value == "") {
      alert("Masih ada field yang harus diisi");
      return false;
    }

    for(var i = 0; i < n; i++) {
      // alert(nama_obat[i].value);
      // alert(tgl_kadaluarsa[i].value);
      // alert(jml_obat_masuk[i].value);
      if(nama_obat[i].value == "" || tgl_kadaluarsa[i].value == "" || jml_obat_masuk[i].value == "") {
        alert("Masih ada field yang harus diisi");
        return false;
      }

      var inputReg = /[^0-9]/;

      if(inputReg.test(jml_obat_masuk[i].value)) {
        alert("Nilai jumlah obat yang dimasukkan hanya berupa angka saja");
        return false;
      }

      for(var j = (i+1); j < n; j++) {
        if(nama_obat[i].value == nama_obat[j].value) {
          alert("Tidak boleh menambahkan stok dengan dua atau lebih nama obat yang sama");
          return false;
        }
      }
    }

    if(no_faktur[0].value == "" && faktur[0].value != "") {
     alert("Nomor faktur harus diisi");
     return false;
   } else if(no_faktur[0].value != "" && faktur[0].value == "") {
    alert("File faktur harus di upload");
    return false;
  }

  var nama_faktur = faktur[0].value;

  var ext = nama_faktur.substring(nama_faktur.lastIndexOf('.') + 1);

  if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png" || ext == "")
  {

  }
  else {
    alert("Extension faktur yang dibolehkan adalah .gif, .jpeg, .jpg, .gif, dan .png");
    return false;
  }

  var filesize = faktur[0].files[0].size;

  if((filesize/(1024*1024)) > 1) {
    alert("Ukuran faktur pembelian terlalu besar, tidak boleh melebihi 1 MB");
    return false;
  }
}
</script>
</div>
</div>










