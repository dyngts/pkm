 <div class="container">
   <!--Body content--> 
   <div class="page-header" style="margin: 150px 0 0 0">
       <h1>Log Obat</h1>
   </div>
   <div style="margin: 20px 0 0 0">
       <div class="control-group">

        <?php echo form_open('obat_controller/search_log_keluar/', array('name' => 'tambah', 'id' => 'tambah', 'onsubmit' => 'return cek_search();')); ?>
        <script src="<?php echo base_url('js/datetimepickers_css.js'); ?>" type="text/javascript"></script>
        <link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css">
        <br/>

        <?php

        $array_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');

        if($this->session->userdata('tanggal_awal_keluar') && $this->session->userdata('tanggal_akhir_keluar')) {
            setlocale (LC_TIME, 'INDONESIA');
            $tgl_awal = date('d-m-Y',strtotime($this->session->userdata('tanggal_awal_keluar')));
            $tgl_akhir = date('d-m-Y',strtotime($this->session->userdata('tanggal_akhir_keluar')));

            $temp1 = explode('-', $tgl_awal);
            $temp2 = explode('-', $tgl_akhir);

            $tgl_awal = $temp1[0].' '.$array_bulan[$temp1[1]].' '.$temp1[2];
            $tgl_akhir = $temp2[0].' '.$array_bulan[$temp2[1]].' '.$temp2[2];
        }

        ?>

        <table style="color:white;">
            <thead>
                <?php
                    if($this->session->userdata('tanggal_awal_keluar') && $this->session->userdata('tanggal_akhir_keluar')) {
                ?>
                <th style="font-size:17px; text-align:left" colspan="2">Cari Log Obat Keluar</th>
                <th style="font-size:17px; text-align:center" colspan="4"><?php echo  $tgl_awal.' - '.$tgl_akhir; ?></th>
                <?php } else { ?>
                <th style="font-size:17px; text-align:left" colspan="6">Cari Log Obat Keluar</th>
                <?php } ?>
            </thead>
            <thead>
                <th>Tanggal Awal</th>
                <th style="padding-left:6px">
                    <p class="p_form" id="p_birthday"></p>
                    <div id="place" class="input">
                      <input type="text" id="demo4" value="<?php echo $this->session->userdata('tanggal_awal_keluar'); ?>" class="input_form_disabled" onclick="javascript:NewCssCal('demo4','yyyyMMdd')" name="tanggal_awal"  readonly="readonly">
                  </div>
              </th>
              <th>Tanggal Akhir</th>
              <th style="padding-left:6px">
                <p class="p_form" id="p_birthday"></p>
                <div id="place" class="input">
                  <input type="text" id="demo5" value="<?php echo $this->session->userdata('tanggal_akhir_keluar'); ?>" class="input_form_disabled"  name="tanggal_akhir" readonly="readonly" onclick="javascript:NewCssCal('demo5','yyyyMMdd')">
              </div>
          </th>
          <th>
            <input type="submit" class="button green" style="width:50px; text-align:center" title="cari" value="Cari" id="cari" name="cari" /> 
        </th>
        <th>
        <input type="submit" class="button green" style="width:150px; text-align:center" title="hapus" value="Hapus Pencarian" name="hapus" />
       </th>
   </thead>
</table>
<?php echo form_close(); ?>
<?php if(count($keluar) > 0) { ?>
<table class="table table-hover" style="margin-top: 30px">
   <thead>
       <tr>
           <th class="even-col" rowspan="2" style="text-align: center">No</th>
           <th colspan="2" style="text-align: center">Waktu Transaksi</th>
           <th class="even-col" rowspan="2" style="text-align: center">Jenis Pengeluaran</th>
           <th rowspan="2" style="text-align: center">Apotek yang Bertugas</th>
           <th rowspan="2"></th>
       </tr>
       <tr>
           <th style="width:20%" style="text-align: center">Tanggal</th>
           <th style="text-align: center">Jam</th>
       </tr>
   </thead>
   <tbody>
     <?php 
     foreach ($keluar as $log_obat_keluar) {
        $tanggal = date('d-m-Y',strtotime($log_obat_keluar->tanggal.''));
        $temp = explode('-', $tanggal);
        $tanggal = $temp[0].' '.$array_bulan[$temp[1].''].' '.$temp[2];
        ?>
        <tr>
           <td style="text-align: center"><?php echo ++$mulai; ?></td>
           <td width="200px" style="text-align: center"><?php echo $tanggal; ?></td>
           <td style="text-align: center"><?php echo $log_obat_keluar->jam; ?></td>
           <td style="text-align: center"><?php echo $log_obat_keluar->jenis; ?></td>
           <td style="text-align: center"><?php echo $log_obat_keluar->username_pemberi; ?></td>
           <td style="text-align: center"><a href="<?php echo site_url('obat_controller/detil_log_keluar/'.$log_obat_keluar->id); ?>" style="color: #51aded"><i class="icon-refresh"></i> Detil</a></td>
       </tr>

       <?php }  ?> 



   </tbody>
</table>
<?php 
echo $pagination_links;
} else {?>
<h3> Tidak ditemukan daftar log obat keluar </h3>
<?php } ?>

</div>
</div>
</div>

<script type="text/javascript">

    function cek_search() {

        var tgl_awal = document.getElementById("demo4");
        var tgl_akhir = document.getElementById("demo5");

        if(tgl_awal.value == "" && tgl_akhir.value != "") {
            alert("Tanggal awal harus diisi");
            return false;
        }
        else if(tgl_awal.value != "" && tgl_akhir.value == "") {
            alert("Tanggal akhir harus diisi");
            return false;
        }
    }

</script>