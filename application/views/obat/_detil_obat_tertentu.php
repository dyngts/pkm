<div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Detil Data Obat</h1>
                </div>
                
                <a class="button green" href="<?php echo site_url('obat_controller/kumpulan_obat_detil'); ?>" title="simpan" value="<< Kembali ke Daftar Obat Aktif" id="simpan" name="simpan" ><< Kembali ke Daftar Obat Aktif</a>
                <div style="margin: 20px 0 0 0">
                    <table class="table table-hover text-left" style="margin-top: 30px; width:50% ">
                    	<tbody>
                    		<tr>
                    			<td><b>Nama Obat</b></td>
                    			<td><?php echo $datas->nama_obat; ?></td>
                    		</tr>
                    		<tr>
                    			<td><b>Kategori</b></td>
                    			<td><?php echo $datas->kategori; ?></td>
                    		</tr>
                    		<tr>
                    			<td><b>Jenis Obat</b></td>
                    			<td><?php echo $datas->jenis; ?></td>
                    		</tr>
                    		<tr>
                    			<td><b>Sediaan</b></td>
                    			<td><?php echo $datas->sediaan; ?></td>
                    		</tr>
                    		<tr>
                    			<td><b>Harga Satuan</b></td>
                    			<td><?php echo $datas->harga_satuan; ?></td>
                    		</tr>
                    		<tr>
                    			<td><b>Harga per Kemasan</b></td>
                    			<td><?php echo $datas->harga_per_kemasan; ?></td>
                    		</tr>
                            <tr>
                                <td><b>Total Satuan</b></td>
                                <td><?php echo $datas->total_satuan; ?></td>
                            </tr>
                            <tr>
                                <td><b>Satuan per Kemasan</b></td>
                                <td><?php echo $datas->satuan_per_kemasan; ?></td>
                            </tr>
                            <tr>
                                <td><b>Komposisi</b></td>
                                <td><?php echo $datas->komposisi; ?></td>
                            </tr>
                            <tr>
                                <td><b>Nama Penyuplai</b></td>
                                <td><?php echo $datas->nama_supplier; ?></td>
                            </tr>
                    	</tbody>
                    </table>
<!--
                    <div class="control-group">
                        <label class="control-label" for="">Nama Supplier : <?php echo $obats->nama_supplier; ?></label>
                    </div>
                    <p>Alamat Supplier : <?php echo $obats->alamat; ?></p>
                    <p>Nomor Telepon : <?php echo $obats->no_telp; ?></p>
                    <p>Kota : <?php echo $obats->kota; ?></p>
                    <p>Kontak yang Dapat dihubungi : <?php echo $obats->contact_person; ?></p>
                    <p>E-mail : <?php echo $obats->email; ?></p>
                    <p>Status : <?php echo ($obats->status == 1) ? 'Aktif' : 'Tidak Aktif'  ?></p>-->

                    <?php  if($obats->num_rows() > 0) { ?>
                    <table class="table table-hover text-left" style="margin-top: 30px">
                        <thead>
                            <tr>
                                <th style="text-align: center">No</th>
                                <th class="even-col" style="text-align: center">Tanggal Kadaluarsa</th>
                                <th style="text-align: center">Status Obat</th>
                                <th class="even-col"  style="text-align: center">Jumlah Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($obats->result() as $obats_obat) { 
                            ?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td><?php echo $obats_obat->tgl_kadaluarsa; ?></td>
                                <td><?php echo ($obats_obat->status_kadaluarsa == 1) ? ('Sudah Kadaluarsa') : ('Aktif'); ?></td>
                                <td style="text-align: center"><?php echo $obats_obat->jml_satuan; ?></td>
                            </tr>			
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <h2>Tidak ditemukan daftar obat yang aktif</h2>
                <?php } ?>
                     <?php echo $pagination_links; ?>
                </div>
            </div>
        </div>