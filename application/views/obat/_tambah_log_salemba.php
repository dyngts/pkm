<div class="container">
	<!--Body content-->
	<div class="page-header" style="margin: 150px 0 0 0">
		<h1>Catat Log Salemba</h1>
	</div>

	<script type="text/javascript">
		$(function() {

			var i = 1;


			var in2 = "'yyyyMMdd'";
			var in3 = "''";
			var in4 = "''";
			var in5 = "''";
			var in6 = "''";
			var in7 = "'future'";


			$('#tambah').live('click', function() {
				i++;
				var in1 = "'demo4"+i+"'";
				$('<table id="field'+i+'" style="font-size:12px; margin-left:1%; background-color:#008F08; color:white;"><tr><td><label>Kategori</label></td><td><select id="first-choice'+i+'"><option value="">Pilih Kategori Obat</option><option value="umum">Obat Umum</option><option value="gigi">Obat, Alkes, dan BMHP Gigi</option><option value="alkes">Alkes dan BMHP Umum</option><option value="emergency">Obat Emergency</option></select></td><td><label for="inputPassword">Jenis Obat</label></td><td><select id="second-choice'+i+'" name="jenis"><option value="">Pilih Kategori Dulu</option></select></td></tr><tr><td><label for="inputPassword">Nama Obat<b style="color:red; font-size: 12px;">*</b></label></td><td style="padding-left:8px;"><select id="third-choice'+i+'" name="nama_obat[]"><option value="">Pilih Jenis Obat Dulu</option></select></td><td><label for="inputPassword">Jumlah Unit Obat<b style="color:red; font-size: 12px;">*</b><span id=""></span></label></td><td style="padding-left:6px"><input type="text" name="jml_obat_masuk[]" maxlength="12" style="background-color: #EAEDF1; height: 30px"></td></tr></table><script type="text/javascript">$("#first-choice'+i+'").click(function(){$("#second-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_jenis");?>"+"/"+$("#first-choice'+i+'").val());$("#third-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_nama"); ?>" + "/" + $("#second-choice'+i+'").val());});<\/script><script type="text/javascript">$("#second-choice'+i+'").click(function(){$("#third-choice'+i+'").load("<?php echo base_url("index.php/obat_controller/data_dropdown_nama"); ?>"+"/"+$("#second-choice'+i+'").val());});<\/script>').appendTo('.isi');

			});

$('#kurang').live('click', function() {
	if(i > 1) {
		$("#field"+i).remove();
		i--;
	}
});

});


</script>

<style>

	.isian {
		margin-top: -0.1%;
	}

</style>

<div id="tableForm" style="margin: 12px 0 0 0">
	<?php echo form_open_multipart('obat_controller/tambah_log_salemba', array('class' => 'form-horizontal', 'name' => 'entri', 'id' => 'entri', 'onsubmit' => 'return cek_input()')); ?>
	<p><b style="color:red; font-size: 16px">* Harus Diisi</b></p>
	<div class="control-group">
		<label class="control-label" for="inputEmail">Jenis Obat <b style="color:red; font-size: 12px">*</b></br>
		</label>
		<div class="isian">
			<select name="jenis_obat" id="jenis_obat">
				<option value="">Pilih Jenis Obat</option>
				<option value="Masuk">Masuk</option>
				<option value="Keluar">Keluar</option>
			</select>
		</div>
		<br/>
		<div class="control-group">
			<label class="control-label" for="inputPassword">Deskripsi<b style="color:red; font-size: 12px">*</b></label>
			<div class="isian">
				<textarea name="deskripsi" style="height: 100px; width: 215px" placeholder="Deskripsi"></textarea>
			</div>

		</div>

		<div class="isi">
			<table style="font-size:12px; margin-left:1%; background-color:#008F08; color:white;">
				<tr>
					<td>
						<label>Kategori</label>
					</td>
					<td>
						<?php echo form_dropdown('kategori', $data_kategori, '', 'id="first-choice"'); ?> 
					</td>
					<td>
						<label for="inputPassword">Jenis Obat</label>
					</td>
					<td>
						<select id="second-choice" name="jenis">
							<option value="">Pilih Kategori Dulu</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="inputPassword">Nama Obat<b style="color:red; font-size: 12px;">*</b></label>
					</td>
					<td style="padding-left:8px;">
						<select id="third-choice" name='nama_obat[]'>
							<option value=''>Pilih Jenis Obat Dulu</option>
						</select>
					</td>
					<td>
						<label for="inputPassword">
							Jumlah Unit Obat <b style="color:red; font-size: 12px;">*</b>
							<span id=""></span>
						</label>
					</td>
					<td style="padding-left:6px">
						<input type="text" name="jml_obat_masuk[]"  style="background-color: #EAEDF1; height: 30px" maxlength="12" > 
					</td>
				</tr>
			</table>

		</div>
	</div>

	<table style="border:0; margin-left:1%">
		<tr>
			<td style="background-color: green">
				<div id="" style="font-size: 12px; ">
					<a>
						<span id="tambah" style="background-color: green; color:white;">
							+
						</span>
					</a>
				</div>  
			</td>
			<td style="background-color: red">
				<div id="" style="font-size: 12px;">
					<a>
						<span id="kurang" style="background-color: red; color:white;">
							-
						</span>
					</a>
				</div> 
			</td>
		</tr>
	</table>

	<div class="form-actions" >
		<input type="submit" class="button green" title="simpan" value="Simpan" id="simpan" name="simpan" />				
		<input type="submit" class="button green" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" />		
	</div>
</form>
</div>

<script type="text/javascript">
	$("#first-choice").click(function() {
		$("#second-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_jenis'); ?>" + "/" + $('#first-choice').val());
		$("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#first-choice').val());
		});

</script>

<script type="text/javascript">
	$("#second-choice").click(function() {
		$("#third-choice").load("<?php echo base_url('index.php/obat_controller/data_dropdown_nama'); ?>" + "/" + $('#second-choice').val());
			//alert($('#second-choice').val());
		});
</script>


<script type="text/javascript">

	function cek_input() {
		var sumber_obat = document.getElementById("jenis_obat");
		var nama_obat = document.getElementsByName("nama_obat[]");
		var tgl_kadaluarsa = document.getElementsByName("tanggal[]");
		var jml_obat_masuk = document.getElementsByName("jml_obat_masuk[]");
		var no_faktur = document.getElementsByName("no_faktur[]");
		var faktur = document.getElementsByName("file[]");

		var n = nama_obat.length;

		if(sumber_obat.value == "") {
			alert("Masih ada field yang harus diisi");
			return false;
		}

		for(var i = 0; i < n; i++) {

			if(nama_obat[i].value == "" || tgl_kadaluarsa[i].value == "" || jml_obat_masuk[i].value == "") {
				alert("Masih ada field yang harus diisi");
				return false;
			}

			var inputReg = /[^0-9]/;

			if(inputReg.test(jml_obat_masuk[i].value)) {
				alert("Nilai jumlah obat yang dimasukkan hanya berupa angka saja");
				return false;
			}

			for(var j = (i+1); j < n; j++) {
				if(nama_obat[i].value == nama_obat[j].value) {
					alert("Tidak boleh menambahkan stok dengan dua atau lebih nama obat yang sama");
					return false;
				}
			}
		}

		if(no_faktur[0].value == "" && faktur[0].value != "") {
			alert("Nomor faktur harus diisi");
			return false;
		} else if(no_faktur[0].value != "" && faktur[0].value == "") {
			alert("File faktur harus di upload");
			return false;
		}

		var nama_faktur = faktur[0].value;

		var ext = nama_faktur.substring(nama_faktur.lastIndexOf('.') + 1);

		if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "PNG" || ext == "png" || ext == "")
		{

		}
		else {
			alert("Extension faktur yang dibolehkan adalah .gif, .jpeg, .jpg, .gif, dan .png");
			return false;
		}

		var filesize = faktur[0].files[0].size;

		if((filesize/(1024*1024)) > 1) {
			alert("Ukuran faktur pembelian terlalu besar, tidak boleh melebihi 1 MB");
			return false;
		}
	}
</script>
</div>
</div>










