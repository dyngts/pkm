<script>
$(document).ready(function() {
	$("#tindakan_wrap").css('display','none');
	$("#resep_wrap").css('display','none');
	$("#addtindakan").css('display','none');
	$("#removetindakan").css('display','none');
	$("#addobat").css('display','none');
	$("#removeobat").css('display','none');
});

function confirmPost() {
  var agree=confirm("Apakah anda yakin ingin memasukkan data ini?");
  if (agree)
  return true ;
  else
  return false ;
} 
</script>

<style>
.errors {
	color:red;
	font-size:10px;
	font-style: italic;
	padding-left: 190px;
}
</style>
<div id ='formrekmed'>
<?php 
    $attr = array('onsubmit'=>"return confirmPost()");
	echo form_open_multipart("dokter/form_rekmed",$attr); 
?>
<center>
<h2 id = 'rekmedhead'> <legend>Form Rekam Medis Pasien  <select class='idpasien' name='idpasien' style="width:300px;">
	<option value="">-- cari pasien --</option>
							  <?php
							  	foreach ($idpasien as $row) {
							  		echo '<option value = '.$row['id'].'>';
							  		echo $row['nama'];
							  		echo '</option>';
							  	}
							  ?>
					</select>
					</legend></h2>
</center>
		<ul id ='rekammedis'>
			<fieldset>
			<div id="formatas">
				<li><label><b style="color:red;">*Harus diisi</b></label></li>
				<li><label>Tanggal(DD-MM-YYYY)<b style="color:red;">*</b> </label><input type="text" readonly="readonly" name='tanggal' value='<?php echo date("d-m-Y");?>'/> </li>
				
				<li><label>Nama Dokter<b style="color:red;">*</b>  </label> 
					<select class='dokter' name='usernamedokter' id ='dokterselect' style="width:200px;">
							  <?php
							  	foreach ($dokter as $row) {
									if($row['nama'] == $nama) {
										echo '<option value = '.$row['username'].' selected="selected">';
										echo $row['nama'];	
										echo '</option>';
									}
									else{
										echo '<option value = '.$row['username'].'>';
										echo $row['nama'];
										echo '</option>';
									}
								}
							  ?>
					</select>
				</li>
				<li><label>Perawat<b style="color:red;">*</b> </label>
					<select class='suster' name='usernameperawat' id ='perawatselect' style="width:200px;">
							  <?php
							  	foreach ($perawat as $row) {
							  		echo '<option value = '.$row['username'].'>';
							  		echo $row['nama'];
							  		echo '</option>';
							  	}
							  ?>
					</select>
				</li>
				<li><label>Keluhan Utama <b style="color:red;">*</b></label><input class="input_form long300" type ='text' name='keluhanutama'/>
					<?php echo form_error('keluhanutama'); ?>
				</li>
				<li><label>Anamnesa </label><textarea class="input_form address_form" rows="8" name="anamnesa" value="Tidak ada"></textarea> 
					<?php echo form_error('anamnesa'); ?>
				</li>
				<li><label>Alergi </label><textarea rows="8" class="input_form address_form" name="alergi" value="Tidak ada"></textarea>
					<?php echo form_error('alergi'); ?>
				 </li>
				</div>
			<br>
			<div id="hasilpemeriksaan">
				<li><h2>&nbsp;&nbsp;Hasil Pemeriksaan</h2></li>
				<li><label>Tekanan Darah </label></label><input class="input_form" type = 'text' name='tekanandarah' />&nbsp;/mmHg
					<?php echo form_error('tekanandarah'); ?>
				</li>
				<li><label>Suhu</label><input class="input_form" type = 'text' name='suhu' value='N/A'/> derajat Celcius
					<?php echo form_error('suhu'); ?>
				</li>
				<li><label>Frekuensi Nadi</label> <input class="input_form" type = 'text' name='nadi' value='N/A'/>&nbsp;per menit
					<?php echo form_error('nadi'); ?>
				</li>
				<li><label>Frekuensi Pernapasan</label><input class="input_form" type = 'text' name='napas' value='N/A'/>&nbsp;per menit
					<?php echo form_error('napas'); ?>
				</li>
				<li><label>Hasil Lab  </label><input type = 'file' name='hasillab'/><span class="helper">pdf, jpg, png</span></li>
				<div id="rontgenrekmed"><li><label>Rontgen</label><input type = 'file' name='rontgen[]'/><span class="helper">pdf, jpg, png</span></li></div>
			</div>
			<div class="row"><button type="button" onClick="tambahrontgen()" class='button green' id="addrontgen">Tambah Rontgen</button></div>
			<br>
			<li><h2>&nbsp;&nbsp;Diagnosa</h2></li>
			<div id = 'diagnosa_wrap'>
				<div id ='diagnosa'>
					
					<!--<li><label>Tipe  </label><input type="radio"  name="tipediagnosa" value="Lama">Lama
							<input type="radio" name="tipediagnosa" value="Baru">Baru</li>-->
					<li><label>Istilah/Nama Diagnosa<b style="color:red;">*</b> </label>
						<select class='e1' name='kodediag[]' id ='diagnosaselect' style="width:200px;">
						  <?php
						  	foreach ($diagnosa as $row) {
						  		if($row['kode'] == "Z00") {
									echo '<option value = '.$row['kode'].' selected="selected">';
							  		echo $row['jenis_penyakit'];
							  		echo '</option>';
								}
						  		else {
						  			echo '<option value = '.$row['kode'].'>';
							  		echo $row['jenis_penyakit'];
							  		echo '</option>';
						  		}		  		
						  	}
						  ?>
						</select>
					</li>
					<li><label>Status kasus  </label> 
						<select id = 'status' name='status_kasus[]'> 
							<option value='lama'/>Lama</option> 
							<option value='baru'/>Baru</option>
						</select>
						</li>
					<li><label>Keterangan Diagnosa</label><textarea class="input_form address_form" rows="8" name="keterangand"></textarea></li>
				</div>
			</div>
			<br>
			<div class="row">
				<button type="button" onClick="addFieldDiag()" class='button green'>Tambah Diagnosa</button>
				<button type="button" onClick="removeFieldDiag()" class='button orange'>Hilangkan Diagnosa</button>
			</div>		
				<div id = 'resep_wrap'>
				<li><h2>&nbsp;&nbsp;Resep</h2></li>
			
					<div id = 'resep'>
						<li><label>Nama Obat<b style="color:red;">*</b>  </label>
							<select class='e2' name='idobat[]' style="width:200px;">
						 <?php
						  	foreach ($obat as $row2) {
						  		if($row2['nama'] == 'Tidak Ada Obat') {
							  		echo '<option value = '.$row2['id'].' selected="selected">';
							  		echo $row2['nama'];
						  		}
						  		else{
						  			echo '<option value = '.$row2['id'].'>';
						  			echo $row2['nama'];
						  		}  		
						  		echo '</option>';
						  	}
						  ?>
						</select></li>
						<!--<li><label>Nama Obat<b style="color:red;">*</b>  </label><input type = 'text' name='namaobat[]'  value='Tidak ada'/></li>-->
						<li><label>Dosis<b style="color:red;">*</b>  </label><input class="input_form" type = 'text' name='dosisresep[]' value='0'/></li>
						<li><label>Jumlah<b style="color:red;">*</b> </label><input class="input_form" type = 'text' name='jmlresep[]' value='0'/></li>
					</div>
				</div>
				<div class="row"><button type="button" onClick="addFieldResep()" class='button green' id="addobat">Tambah Obat</button>
				<button type="button" onClick="removeFieldResep()" class='button orange' id="removeobat">Hilangkan Obat</button></div>
				<br>
				
			<div id ='tindakan_wrap'>
			<li><h2>&nbsp;&nbsp;Tindakan</h2></li>
				<div id='tindakan'> 
					<li><label>Nama<b style="color:red;">*</b> </label><!--<input type = 'text' name='namatindakan[]' id='rekmed1' value ='Tidak ada tindakan'/></li>-->
					 <select class='e3' name='idtindakan[]' style="width:200px;">
						  <?php
						  	foreach ($tindakan as $row) {
						  		if($row['nama'] == 'Tidak ada tindakan') {
						  		echo '<option value = '.$row['id'].' selected="selected">';
						  		echo $row['nama'];
						  		}
						  		else {
						  			echo '<option value = '.$row['id'].'>';
						  			echo $row['nama'];
						  			echo '</option>';
						  		}
						  	}
						  ?>
						</select>
					  </li>
					  <li><label>Keterangan Tindakan </label><textarea class="input_form address_form" rows="8" name="keterangant"></textarea></li>
				</div>				
			</div>
			<div class="row"><button type="button" onClick="addFieldTindakan()" class='button green' id="addtindakan">Tambah Tindakan</button>
			<button type="button" onClick="removeFieldTindakan()" class='button orange' id="removetindakan">Hilangkan Tindakan</button>
			<br/>
			<br/>
			</div>
			</fieldset>
			<div class="row">
			<button type="button" class='button green' onClick='addResep()' id="addRButton">Tambahkan Resep</button>
			<button type="button" class='button green' onClick="addTindakan()" id="addTButton">Tambahkan Tindakan</button>
			<input type="submit" value="Submit" id='rekmedsub' class='button red submit_button'>
			</div>
			<br>
			</ul>
	<?php echo form_close(); ?>
</div>
<script>
    $(document).ready(function() { $(".e1").select2(); });
    $(document).ready(function() { $(".idpasien").select2(); });
    $(document).ready(function() { $(".suster").select2(); });
    $(document).ready(function() { $(".dokter").select2(); });
    function select2yangbelum(dari) {
    	dari.each(function() {
    		if ($(this).is(".udahs2") == false) {
    			$(this).select2();
    			$(this).addClass("udahs2");
    		}
    	});
    }
     
    function select2yangbelumlagi(yea) {
    	yea.each(function() {
    		if ($(this).is(".udahs3") == false) {
    			$(this).select2();
    			$(this).addClass("udahs3");
    		}
    	});
    }
</script>

<script type ='text/javascript'>
	var i = 0;
	var j = 0;
	var k = 0;
	var l = 0;
	var original = $("#diagnosa").clone(); 
	var parentxx   = $("#diagnosa").parent();
	var originalResep = $("#resep").clone(); 
	var parentx2   = $("#resep").parent();
	var originalTindakan = $("#tindakan").clone(); 
	var parentx3   = $("#tindakan").parent();
	var origrontgen = $("#rontgenrekmed").clone();
	var parentront = $("#rontgenrekmed").parent();
	//var originalResep2 = document.getElementById('resep');
	//var originalTindakan2 = document.getElementById('tindakan');

	function addFieldDiag() {
		++i;
		var clone = original.clone();
		var diagId = original.attr("id");
		clone.attr("id", original.attr("id") + i) ;
		$(clone).find("input").val("");
		parentxx.append(clone); 
		clone.find(".e1").select2();
	}


	function tambahrontgen() {
		++l;
		var clone = origrontgen.clone(true);
		var rontgenid = origrontgen.attr("id");
		clone.attr("id", origrontgen.attr("id") + l) ;
		$(clone).find("input").val("");
		parentront.append(clone); 
	}

	function addResep() {
		var target = "#resep_wrap";
		var target2 = "#addobat";
		var target3 = "#removeobat";
		var hidetarget = "#addRButton";                                                                                                                              
		if( $(target).length > 0) {
			$(target).css('display','block');
			$(target2).css('display','inline-block');
			$(target3).css('display','inline-block');
			$(hidetarget).css('display','none');
			select2yangbelum($(".e2"));
	  }
	}

	function addTindakan() {
		var target = "#tindakan_wrap";
		var target2 = "#addtindakan";
		var target3 = "#removetindakan";
		var hidetarget = "#addTButton";                                                                                                                              
		if( $(target).length > 0) {
			$(target).css('display','block');
			$(target2).css('display','inline-block');
			$(target3).css('display','inline-block');
			$(hidetarget).css('display','none');
			select2yangbelumlagi($(".e3"));
	  }		
	}
	

	function addFieldResep(){
		++j;
		var cloneResep = originalResep.clone();	
		var idResep = originalResep.attr("id");
		cloneResep.attr("id", originalResep.attr("id") + j) ;
		$(cloneResep).find("input").val("");
		parentx2.append(cloneResep);
		select2yangbelum($(".e2"));
	}	

	function addFieldTindakan(){
		++k;
		var cloneTindakan = originalTindakan.clone();
		var idTindakan = originalTindakan.attr("id");
		cloneTindakan.attr("id", originalTindakan.attr("id") + k) ;
		$(cloneTindakan).find("input").val("");
		parentx3.append(cloneTindakan);
		select2yangbelumlagi($(".e3"));
	}

	function addFieldResep2(){
		++j;
		var cloneResep = originalResep.cloneNode(true);
		var idResep = originalResep.id;
		cloneResep.id = idResep + j;
		$(cloneResep).find("input").val("");
		originalResep.parentNode.appendChild(cloneResep);
	}	

	function addFieldTindakan2(){
		++k;
		var cloneTindakan = originalTindakan.cloneNode(true);
		var idTindakan = originalTindakan.id;
		cloneTindakan.id = idTindakan+k;
		$(cloneTindakan).find("input").val("");
		originalTindakan.parentNode.appendChild(cloneTindakan);
	}

	function removeFieldDiag() {
		var hapusDiagnosa = document.getElementById('diagnosa'+i);
		$(hapusDiagnosa).remove();
		i--;
	}

	function removeFieldResep() {
		$("#resep_wrap").find("> div").last().remove()
	}

	function removeFieldTindakan() {
		$("#tindakan_wrap").find("> div").last().remove()
	}

</script>