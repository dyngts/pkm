<div class="row">
<h2>Pencarian Rekam Medis Pasien</h2>
<br>
<?php echo form_open("dokter/searchrekam_2"); ?>
<div class="row">
<div class="label"><label>ID Pasien</label></div><p class="p_form">:</p>
<div class="input"><input class="input_form" type = 'text' name='idpasien' id='idpasien'/></div>
</div>
<div class="row">
<div class="label"><label>Nama Pasien:</label></div><p class="p_form">:</p>
<div class="input"><input class="input_form" type = 'text' name='namapasien' id='namapasien'/></div>
</div>
<br>
<div class="row"><div class="label"><input type="submit" value="Submit" id='pasiensub' class='button green submit_button'></div></div>
<?php echo form_close(); ?>
</div>
<?php echo form_error('idpasien'); ?>
<br><br>
<div class="row">
<?php
	if($rekmed != null){
?>
	<table class="table">
	<tr>
		<th class="first">ID Pasien</th>
		<th class="even-col">Nama Pasien</th>
		<th class="first">Tindakan</th>
	</tr>
	<?php
		foreach ($rekmed as $row){
			echo '<tr>';
			echo '<td class="text-center">'.$row['id'].'</td>';
			echo '<td class="text-center">'.$row['nama'].'</td>';
			echo '<td class="text-center">';
			echo '<a href="'.base_url().'dokter/hasil_rekam_medis/'.$row['id'].'"><button class="button orange table_button">Lihat Rekam Medis</button></a>';
			echo '</td>';
			echo '</tr>';
		}
	?>
</table>
<?php	
	}else {
		echo '<h2>Tidak ada pasien dengan ID atau Nama seperti itu</h2>';
	}
	?>
</div>
</div>