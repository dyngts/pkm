<center>
<h1 id = 'rekmedhead'> Form Rekam Medis Pasien Nomor <?php echo $idpasien; ?> </h1>
<div id ='formrekmed'>
	<?php echo form_open("dokter/form_rekmed"); ?>
		<table id="tabelrincian">
		<tr>
			<td>Tanggal(DD-MM-YYYY)</td>
			<td>:</td>
			<td>
				<input list="hari" name="hari" id ='tanggalday' value="<?php date_default_timezone_set('UTC'); echo date('d'); ?>">
				<datalist id="hari">
				  <option value="1">
				  <option value="2">
				  <option value="3">
				  <option value="4">
				  <option value="5">
				  <option value="6">
				  <option value="7">
				  <option value="8">
				  <option value="9">
				  <option value="10">
				  <option value="11">
				  <option value="12">
				  <option value="13">
				  <option value="14">
				  <option value="15">
				  <option value="16">
				  <option value="17">
				  <option value="18">
				  <option value="19">
				  <option value="20">
				  <option value="21">
				  <option value="22">
				  <option value="23">
				  <option value="24">
				  <option value="25">
				  <option value="26">
				  <option value="27">
				  <option value="28">
				  <option value="29">
				  <option value="30">
				  <option value="31">
				</datalist>
				-
				<input list="bulan" name="bulan" id ='tanggalmonth' value="<?php date_default_timezone_set('UTC'); echo date('m'); ?>">
				<datalist id="bulan">
				  <option value="01">
				  <option value="02">
				  <option value="03">
				  <option value="04">
				  <option value="05">
				  <option value="06">
				  <option value="07">
				  <option value="08">
				  <option value="09">
				  <option value="10">
				  <option value="11">
				  <option value="12">
				</datalist>
				-
				<input type='text' name='tahun' id='tanggalyear' value="<?php date_default_timezone_set('UTC'); echo date('Y'); ?>"/>
			</td>
		</tr>
		<tr>
			<td>Inisial Dokter</td>
			<td>:</td>
			<td><input type = 'text' name='inisialdr' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Anamnesa</td>
			<td>:</td>
			<td><input type = 'text' name='anamnesa' id='rekmed2'/></td>
		</tr>
		<tr><td><br><td></tr>
		<tr>
			<td colspan="3"><h2><center>Diagnosa</center></h2></td>
		</tr>
		<tr id ='tipediagnosa0'>
			<td>Tipe</td>
			<td>:</td>
		</tr>
		<tr id ='kodediagnosa0'>
			<td>Kode</td>
			<td>:</td>
			<td><input type = 'text' name = 'kodediag' id = 'rekmed'/></td>
		</tr>
		<tr id ='istilahdiagnosa0'>
			<td>Istilah</td>
			<td>:</td>
			<td><input type = 'text' name='istdiag' id='rekmed1'/></td>
		</tr>
		<tr id = 'deskripsidiagnosa0'>
			<td>Deskripsi</td>
			<td>:</td>
			<td><input type = 'text' name='deskdiag' id='rekmed2'/></td>
		</tr>
		<tr>
			<td>
				<button type="button" onClick="addField()">Add another field</button><br>
				
			<td>
		</tr>
		<tr>
			<td colspan="3"><h2><center>Hasil Pemeriksaan</center></h2></td>
		</tr>
		<tr>
			<td>Tekanan Darah</td>
			<td>:</td>
			<td><input type = 'text' name='tekanandarah' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Frekuensi Suhu</td>
			<td>:</td>
			<td><input type = 'text' name='suhu' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Frekuensi Nadi</td>
			<td>:</td>
			<td><input type = 'text' name='nadi' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Frekuensi Pernapasan</td>
			<td>:</td>
			<td><input type = 'text' name='napas' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Hasil Lab</td>
			<td>:</td>
			<td><input type = 'file' name='hasillab' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Rontgen</td>
			<td>:</td>
			<td><input type = 'file' name='rontgen' id='rekmed1'/></td>
		</tr>
		<tr><td><br><td></tr>
		<tr>
			<td colspan="3"><h2><center>Resep</center></h2></td>
		</tr>
		<tr>
			<td>Nama Resep</td>
			<td>:</td>
			<td><input type = 'text' name='namaresep' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Dosis</td>
			<td>:</td>
			<td><input type = 'text' name='dosisresep' id='rekmed1'/></td>
		</tr>
		<tr>
			<td>Jumlah</td>
			<td>:</td>
			<td><input type = 'text' name='jmlresep' id='rekmed1'/></td>
		</tr>
		<tr><td><br><td></tr>
		<tr>
			<td colspan="2"><input type="submit" value="Submit" id='rekmedsub'></td>
			<td colspan="2"><a href='#'><button type="button" class='addbutton'>Masukkan Resep</button></a></td>
		</tr>
		<tr>
			<td>
				<br>
			<td>
		</tr>
	</table>
	<?php echo form_close(); ?>
</div>
</center>

<script>
	var i = 0;
	var originaltipe = document.getElementById('tipediagnosa0');
	var originalkode = document.getElementById('kodediagnosa0');
	var originalistilah = document.getElementById('istilahdiagnosa0');
	var originaldeskripsi = document.getElementById('deskripsidiagnosa0');


	function addField() {
		++i;
		var clone1 = originaltipe.cloneNode(true);
		var clone2 = originalkode.cloneNode(true);
		var clone3 = originalistilah.cloneNode(true);
		var clone4 = originaldeskripsi.cloneNode(true);
		clone1.id = "tipediagnosa" + i;
		clone2.id = "kodediagnosa" + i;
		clone3.id = "istilahdiagnosa" + i;
		clone4.id = "deskripsidiagnosa" + i;
		originaltipe.parentNode.appendChild(clone1);
		originalkode.parentNode.appendChild(clone2);
		originalistilah.parentNode.appendChild(clone3);
		originaldeskripsi.parentNode.appendChild(clone4);
	}
</script>