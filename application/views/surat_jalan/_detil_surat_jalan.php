 <div class="container">
            <!--Body content-->
            <div class="page-header" style="margin: 150px 25% 0 0">
                <h1>Surat Jalan</h1>
            </div>
            <div style="margin: 20px 25% 0 0">
                <div class="control-group">
                    <label class="control-label" for="">Tanggal Pembuatan : 15 April 2013</label>
                </div>
                <p>Penerima : PKM UI Salemba</p>
                <p>Pengirim : Rahma Islami Lukitasari</p>
                <table class="table table-hover" style="margin-top: 30px">
		<thead>
			<tr>
				<th style="text-align: center">No</th>
				<th style="text-align: center">Jenis Obat</th>
				<th style="text-align: center">Nama Obat</th>
				<th style="text-align: center">Satuan</th>
				<th style="text-align: center">Jumlah Unit</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="text-align: center">1</td>
				<td>Antivirus</td>
				<td>Acifar 400 mg tab </td>
				<td style="text-align: center">Tablet</td>
				<td style="text-align: center">-</td>
			</tr>			
			<tr>
				<td style="text-align: center">2</td>
				<td>Antivirus</td>
				<td>Acifar slp</td>
				<td style="text-align: center">Tube</td>
				<td style="text-align: center">-</td>
			</tr>
			<tr>
				<td style="text-align: center">3</td>
				<td>Antibiotik</td>
				<td>Amoxycillin 500 tab</td>
				<td style="text-align: center">Tablet</td>
				<td style="text-align: center">-</td>
			</tr>
			<tr>
				<td style="text-align: center">4</td>
				<td>Antibiotik</td>
				<td>Cefadroxil 500mg</td>
				<td style="text-align: center">Tablet</td>
				<td style="text-align: center">-</td>
			</tr>
			<tr>
				<td style="text-align: center">5</td>
				<td>Antibiotik</td>
				<td>Dothrocyn 500mg</td>
				<td style="text-align: center">Tablet</td>
				<td style="text-align: center">-</td>
			</tr>
			<tr>	
				<td style="text-align: center">6</td>
				<td>Antibiotik</td>
				<td>Farizol 500mg</td>
				<td style="text-align: center">Tablet</td>
				<td style="text-align: center">-</td>
			</tr>
	</tbody>
</table>
            </div>
        </div>
            </div>