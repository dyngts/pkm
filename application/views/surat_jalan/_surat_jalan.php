<html>
<head>
	<title>Surat Jalan</title>
	<style type="text/css">
       /* 
Generic Styling, for Desktops/Laptops 
*/
table { 
	width: 100%;
	border-collapse: collapse;
}
/* Zebra striping */
tr:nth-of-type(odd) { 
	background: #eee; 
}
th { 
	background: #333; 
	color: white; 
	font-weight: bold; 
}
td, th { 
	padding: 6px;
	border: 1px solid #ccc; 
	text-align: left; 
}

html{
	font-family: Segoe UI;
}

ul {
	font-weight: bold;
}

#footer {
   position:absolute;
   bottom:0;
   width:100%;
   height:60px;   /* Height of the footer */
   background:#FFFFFF;
}

</style>
</head>
<body class="wow">
	<div id="kwitansi">
		<div id="header">
			<table>
				<tbody>
					<tr>
					<td style="width:1%; margin-left:auto; margin-right:auto;"><a href="#"><img src="image/judul.png" alt="" width="519px" height="94px" title="" /></a></td>
					
						<td style="width:20%; text-align:left;">
							<ul>
								Apotek Pusat Kesehatan Mahasiswa <br>
								Universitas Indonesia <br>
								Kampus UI Depok 16424 <br>
								Telepon: 021-765432 <br>
								Email: pkm@ui.ac.id
							</ul>								
						</td>
						<td style="width:45%; text-align: left;">
							<ul>
								Nama Apoteker: <?php echo $pembuat_surat_jalan; ?> <br>
								Tanggal Pembuatan: <?php echo $tgl_pembuatan; ?> <br>
								Nama Penerima: <?php echo $penerima; ?> <br>
								No. Surat Jalan: <?php echo $no_surat_jalan;  ?>
							</ul>								
						</td>
					</tr>
				</tbody>
			</table>
        </div>
        <div id="divtabel">
			<h2 style="text-align: center">DAFTAR OBAT YANG DIKELUARKAN</h2>

			<table class ="tabel" border="2">
				<thead style="font-size: 16px">
					<tr>
						<th style="text-align: center">Nama Obat</th>
						<th style="text-align: center">Kemasan</th>
						<th style="text-align: center">Jumlah Unit</th>
					</tr>
				</thead>
				<tbody  style="font-size: 14px">
					<?php for($i = 0; $i<$jumlah_input; ++$i) { ?>
					<tr>
						<td style="text-align: center"><?php echo $nama_obats[$i]; ?></td>
						<td style="text-align: center"><?php echo $sediaans[$i]; ?></td>
						<td style="text-align: center"><?php echo $jumlah_keluars[$i]; ?></td>
					</tr>  
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>