 <div class="container">
     <!--Body content--> 
     <div class="page-header" style="margin: 150px 0 0 0">
         <h1>Buat Surat Jalan</h1>

    <?php
        $jenis_obats = preg_replace('/^\s+|\n|\r|\s+$/m', '', form_dropdown('jenis[]', $data_jenis_obat, '', 'id="first-choice"'));
        $sediaan_obats = preg_replace('/^\s+|\n|\r|\s+$/m', '', form_dropdown('sediaan[]', $data_sediaan_obat, set_value('sediaan'), 'id="sediaan"'));
        $nama_obats = preg_replace('/^\s+|\n|\r|\s+$/m', '', form_dropdown('nama_obat[]', $data_nama_obat, set_value('nama_obat'), 'id="second-choice"'));
        $situs = preg_replace('/^\s+|\n|\r|\s+$/m', '', base_url('index.php/surat_jalan_controller/data_dropdown_nama'));
    ?>
        
    <script type="text/javascript">
      $(function() {

        var i = 1;
        $('#tambah').on('click', function() {
           	i++;
                $('<table style="border: 1px solid #CCC;" id="field'+i+'"><tr><td><label>Jenis Obat</label></td><td><select id="first-choice'+i+'" name="jenis[]"><script>$("#first-choice'+i+'").load("<?php echo base_url("index.php/surat_jalan_controller/data_jenis"); ?>");<\/script></select></td><td><label for="inputPassword">Sediaan</label></td><td><?php echo $sediaan_obats; ?></td></tr><tr><td><label for="inputPassword">Nama Obat</label></td><td style="padding-left:8px;"><select id="second-choice'+i+'" name="nama_obat[]"><option>Pilih Jenis Obat Dulu</option></select></td><td><label for="inputPassword">Jumlah Unit Obat Keluar<b style="color:red; font-size: 12px">*</b><span id=""></span></label></td><td style="padding-left:6px"><input id="jumlah_keluar" size="12" name="jumlah_keluar[]" type="text" maxlength="12" style=""/></td></tr></table><script type="text/javascript">$("#first-choice'+i+'").change(function(){$("#second-choice'+i+'").load("<?php echo base_url()."index.php/surat_jalan_controller/data_dropdown_nama"; ?>"+"/"+$("#first-choice'+i+'").val());}); <\/script>').appendTo('.isi');
        });

        $('#kurang').on('click', function() {
            if(i > 1) {
                $("#field"+i).remove();
                i--;
            }
        
        });
		
        });

        
    </script>

    <style>
    .isian {
        margin-top: -0.1%;
    }

    </style>
         
     </div>
     <div id="tableForm" style="margin: 20px 0 0 0">
         <?php echo form_open('surat_jalan_controller/hasil_surat_jalan', array('class' => 'form-horizontal', 'name' => 'buat', 'id' => 'buat')); ?>
         <p><b style="color:red; font-size: 16px">* Harus Diisi</b></p>
             <div class="control-group">
                 <label class="control-label" for="inputPassword">Penerima <b style="color:red; font-size: 12px">*</b></label>
                 <div class="isian">
                     <input type="text" maxlength="30" size="30" id="penerima" name="penerima"  placeholder="Penerima" style=""></div>
             </div>

             <div class="control-group">
                 <label class="control-label" for="inputPassword">Jenis Penerima <b style="color:red; font-size: 12px">*</b></label>
                 <div class="isian">
                     <select name="jenis_penerima" id="jenis_penerima">
                         <option value="">Pilih Penerima Surat Jalan</option>
                         <option value="Salemba">Salemba</option>
                         <option value="Event">Event</option>
                     </select>
                 </div>
             </div>

             <div class="control-group">
                 <label class="control-label" for="inputPassword">Nomor Surat Jalan <b style="color:red; font-size: 12px">*</b></label>
                 <div class="isian">
                     <input type="text" maxlength="10" disabled size="30" id="no_surat_jalan" name="no_surat_jalan"  placeholder="" style="">
                     <span style="color:red; margin-left:21%; display:none;">Maaf nomor tersebut sudah ada</span></div>
             </div>

             <div class="control-group">
                 <label class="control-label" for="inputPassword">Deskripsi</label>
                 <div class="isian">
                     <textarea name="deskripsi" id="deskripsi" style="width:27%; height:50px;" placeholder="Deskripsi"></textarea></div>
             </div>

             <div class="isi" style="margin-left:1%">
             <table style="border: 1px solid #CCC;">
                 <tr>
                     <td>
                         <label>Jenis Obat</label>
                     </td>
                     <td>
                        <?php echo form_dropdown("jenis[]", $data_jenis_obat, '', 'id="first-choice"'); ?>
                     </td>
                     <td>
                         <label for="inputPassword">Sediaan</label>
                     </td>
                     <td>
                        <?php echo form_dropdown("sediaan[]", $data_sediaan_obat) ?>
                     </td>
                 </tr>
                 <tr>
                     <td>
                         <label for="inputPassword">Nama Obat</label>
                     </td>
                     <td style="padding-left:8px;">
                        <select id='second-choice' name='nama_obat[]'>
                            <option value=''>Pilih Jenis Obat Dulu</option>
                        </select>
                     </td>
                     
                     <td>
                         <label for="inputPassword">
                             Jumlah Unit Obat Keluar <b style="color:red; font-size: 12px;">*</b>
                             <span id=""></span>
                         </label>
                     </td>
                     <td style="padding-left:6px">
                         <input type="text" id="jumlah_keluar" name="jumlah_keluar[]" maxlength="12" size="12" style="" > 
                     </td>
                 </tr>
             </table>
             
         </div>
            
         </div>

         <table style="border:0; margin-left:1%">
             <tr>
                 <td style="background-color: green">
                     <div id="" style="font-size: 12px; ">
                       <a>
                           <span id="tambah" style="background-color: green; color:white;">
                               +
                           </span>
                       </a>
                   </div>  
                 </td>
                 <td style="background-color: red">
                    <div id="" style="font-size: 12px;">
                       <a>
                           <span id="kurang" style="background-color: red; color:white;">
                               -
                           </span>
                       </a>
                   </div> 
                 </td>
             </tr>
         </table>

         <br/>

         <div class="form-actions" style="background-color: #D2D1D1">
             <input type="submit" class="button green" value="Selanjutnya >>" name="simpan" />          
         </div>

     <?php echo form_close(); ?>
 </div>
        <script type="text/javascript">
            $("#first-choice").change(function(){
                $("#second-choice").load("<?php echo base_url('index.php/surat_jalan_controller/data_dropdown_nama'); ?>"+"/"+$("#first-choice").val());
                //alert($('#first-choice').val());
            });

        </script>

        <script type="text/javascript">
            $("#jenis_penerima").change(function(){
                var jenis_penerima = document.getElementById("jenis_penerima").value;
                if(jenis_penerima == "Event" || jenis_penerima == "") {
                    var no_surat_jalan = document.getElementById("no_surat_jalan");
                    no_surat_jalan.disabled = true;
                    no_surat_jalan.value = "";
                    no_surat_jalan.placeholder = "";
                }
                else if(jenis_penerima == "Salemba") {
                    var no_surat_jalan = document.getElementById("no_surat_jalan");
                    no_surat_jalan.disabled = false;
                    no_surat_jalan.placeholder = "Nomor Surat Jalan";
                }
            });

        </script>

        <?php

        $alamat = base_url('index.php/surat_jalan_controller/ajax_cek_no_surat_jalan');

        ?>

        
        <script type="text/javascript">
        
        var validasi = {
            no_surat: true
        }

        var cek_inputs = function (e,x,y) {
            if (validasi.no_surat === false) {
                //alert("Maaf anda kurang ganteng, masih lebih ganteng denny");
                return false;
            }

            // alert("<?php echo $alamat; ?>");
            // return false;
            var penerima = document.getElementById("penerima");
            var nama_obats = document.getElementsByName("nama_obat[]");
            var sediaans = document.getElementsByName("sediaan[]");
            var jenis = document.getElementsByName("jenis[]");
            var jumlah_keluar = document.getElementsByName("jumlah_keluar[]");
            var deskripsi = document.getElementById("deskripsi");
            var jenis_penerima = document.getElementById("jenis_penerima");
            var no_surat_jalan = document.getElementById("no_surat_jalan");

            var n = nama_obats.length;
            var intReg = /[^0-9]/;
            var charReg1 = /[^A-Za-z\s]/;
            var charReg2 = /[^A-Za-z0-9]/;
            var charReg3 = /[^A-Za-z0-9\s.,-]/;
            var cekIntReg = /^[1-9]+/;

            if(penerima.value == "") {
                alert("Masih ada field yang wajib untuk diisi");
                return false;
            }

            if(charReg1.test(penerima.value)) {
                alert("nilai masukkan mengandung karakter yang terlarang");
                return false;
            }

            if(jenis_penerima.value == "") {
                alert("Masih ada field yang wajib untuk diisi");
                return false;
            }

            if(jenis_penerima.value == "Salemba" && no_surat_jalan.value == "") {
                alert("Nomor surat jalan harus diisi");
                return false;
            }

            if(charReg2.test(no_surat_jalan.value)) {
                alert("nilai masukkan mengandung karakter yang terlarang");
                return false;
            }

            if(deskripsi.value != "") {
                if(charReg3.test(deskripsi.value)) {
                 alert("nilai masukkan mengandung karakter yang terlarang");
                 return false;
                }
            }

            for(var i = 0; i < n; i++) {

                if(nama_obats[i].value == "" || sediaans[i].value == "" || jenis[i].value == "" || jumlah_keluar[i].value == "") {
                    alert("Masih ada field yang wajib untuk diisi");
                    return false;
                }

                if(intReg.test(jumlah_keluar[i].value)) {
                    alert("input jumlah obat keluar hanya berupa angka saja");
                    return false;
                }

                if(!cekIntReg.test(jumlah_keluar[i].value)) {
                    alert("nilai masukkan jumlah obat tidak boleh diawali dengan angka 0");
                    return false;
                }

                for(var j = (i+1); j < n; j++) {
                    if(nama_obats[i].value == nama_obats[j].value) {
                        alert("Tidak boleh mengeluarkan dua atau lebih dengan nama obat yang sama");
                        return false;
                    }
                }   
            }

            
        }
        var timeout_handle;
        $("[name=no_surat_jalan]").keyup(function(x,y,z) {
            clearInterval(timeout_handle);
            var span = $(this).parent().find("span");
            var th = $(this);
            span.css({display: "none"});
            timeout_handle = setTimeout(function() {
            $.post( 
                '<?php echo $alamat; ?>',
                { 'no_surat_jalan' : th.val() },
                function(response) {
                    if (response.nilai == 1) {
                        validasi.no_surat = false;
                        debugger;
                        span.css({display: "block"});
                    } else {
                        validasi.no_surat = true;
                    }
                },
                'json' 
            );
            }, 2000);
        });

        function cek_no_surat_jalan(name) {
            var val_no_surat = true;
            var i = 1;

            window.alert(i);
        }
        $(document).ready(function() {
            $("form#buat").submit(cek_inputs);
        } );
        
        </script>
 </div>
 </div>


