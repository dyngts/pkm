<div class="container">
    <!--Body content-->
    <div class="page-header" style="margin: 150px 0 0 0">
        <h1>Daftar isian surat jalan yang akan dikeluarkan</h1>
    </div>

    <?php echo form_open('surat_jalan_controller/tambah', array('name' => 'buat', 'id' => 'buat')); ?>

    <div class="error"><?php echo form_error('no_surat_jalan'); ?></div>

    <style type="text/css">
        .detil {
            background-color: #008F08;
            color: white;
            width:35%;
            text-align: left;
        }

    </style>

    <div>
        <table class="table table-hover" style="margin-top: 30px; width:50% ">
            <tr>
                <td class="detil">Penerima</td>
                <td><?php echo $penerima; ?></td>
                <input type="hidden" name="penerima" value="<?php echo $penerima; ?>" />
            </tr>
            <tr>
                <td class="detil">Jenis Penerima</td>
                <td><?php echo $jenis_penerima; ?></td>
                <input type="hidden" name="jenis_penerima" value="<?php echo $jenis_penerima; ?>" />
            </tr>
            <tr>
                <td class="detil">Nomor Surat Jalan</td>
                <td><?php echo $no_surat_jalan; ?></td>
                <input type="hidden" name="no_surat_jalan" value="<?php echo $no_surat_jalan; ?>" />
            </tr>
            <tr>
                <td class="detil">Deskripsi</td>
                <td><?php echo $deskripsi; ?></td>
                <input type="hidden" name="deskripsi" value="<?php echo $deskripsi; ?>" />
            </tr>
            <tr>
                <td class="detil">Username yang mengeluarkan</td>
                <td><?php echo $username; ?></td>
            </tr>
        </table>
    </div>

    <div style="margin: 20px 0 0 0">
        <?php if($jumlah_input > 0) { ?>
        <table class="table table-hover" style="margin-top: 30px">
            <thead>
                <tr>
                    <th style="text-align: center">No</th>
                    <th style="text-align: center">Nama Obat</th>
                    <th style="text-align: center">Sediaan</th>
                    <th style="text-align: center">Jumlah Unit Obat Keluar</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                for($i = 0; $i < $jumlah_input; $i++) { 
                    ?>
                    <tr>
                        <td style="text-align: center"><?php echo ++$mulai; ?></td> 
                        <td><?php echo $array_nama_obat[$i]; ?></td> <input type="hidden" name="nama_obat[]" value="<?php echo $array_nama_obat[$i]; ?>" />
                        <td><?php echo $array_sediaan[$i]; ?></td> <input type="hidden" name="sediaan[]" value="<?php echo $array_sediaan[$i]; ?>" />
                        <td><?php echo $array_jumlah_keluar[$i]; ?></td> <input type="hidden" name="jumlah_keluar[]" value="<?php echo $array_jumlah_keluar[$i]; ?>" />
                    </tr>
                    <?php } ?>			
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>


    <div>
     <input type="submit" class="button green" style="font-size:15px;" value="<< Kembali" name="kembali" />  
     <input type="submit" class="button green" style="font-size:15px;" value="Buat Surat Jalan" name="simpan" onclick="return confirm('Anda yakin ingin memproses ini? Proses ini tidak dapat diubah kembali')" />    
    </div>



    <?php echo form_close(); ?> 