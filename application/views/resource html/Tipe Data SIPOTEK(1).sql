// postgre

create schema sipotek;
set search_path to sipotek;


// entity

create table pengguna (
	username varchar(30) not null,
	password varchar(20) not null,
	nip char(10) not null, 
		flag_admin boolean not null,
	primary key(username),
	/*jabatan varchar(20) not null, */
	/* foreign key(username) references pegawai(username) on delete cascade on update cascade */
	/* foreign key(password) references pegawai(password) on delete cascade on update cascade */
	/* foreign key(nip) references pegawai on delete cascade on update cascade */
);

create table supplier (
	id_supplier serial not null,
	nama varchar(25) not null,
	no_telp varchar(12),
	alamat text not null,
	kota varchar(20) not null,
	contact_person varchar(30),
	email varchar(30),
	status boolean not null,
	primary key(id_supplier)
);


create table obat (
	id_obat serial not null,
	kategori varchar(50) not null,
	jenis varchar(50),
	nama varchar(30) not null,
	komposisi text,
	sediaan varchar(30) not null,
	kemasan_obat varchar(30),
	jml_kemasan int not null,
	satuan_per_kemasan int not null,
	total_obat_satuan int not null,
	harga_per_kemasan int,
	harga_satuan int,
	status_pemakaian boolean not null,

	id_supplier int,
	primary key(id_obat),
	foreign key(id_supplier) references supplier 
	on delete restrict on update cascade
);


create table kadaluarsa_obat (
	id_obat int not null,
	tgl_kadaluarsa date not null,
	jml_kemasan int not null,
	jml_satuan int,

	primary key(id_obat, tgl_kadaluarsa),
	foreign key(id_obat) references obat 
	on delete cascade on update cascade
);

create table log_obat_keluar (
	id_log_obat_keluar serial not null,
	tgl date not null,
	jenis varchar(20) not null, /*ada 3 jenis: resep, event, surat jalan*/
	deskripsi text,
	primary key(id_log_obat_keluar)
);

create table log_obat_masuk (
	id_log_obat_masuk serial not null,
	tgl date not null,
	deskripsi text not null,
	flag_faktur boolean not null,
	scanned_faktur varchar(255) not null,
	
	penerima_obat varchar(30) not null,
	primary key(id_log_obat_masuk),
	foreign key(penerima_obat) references pengguna
	on delete cascade on update cascade
);

create table surat_jalan (
	id_log_obat_keluar int not null,
	nomor char(10) not null,
	pembuat_surat_jalan varchar(30) not null, 
	tgl_penerimaan date,
	tgl_pengiriman date not null,

	primary key(id_log_obat_keluar, nomor),
	foreign key(id_log_obat_keluar) references log_obat_keluar 
	on delete restrict on update cascade,
	foreign key(pembuat_surat_jalan) references pengguna
	on delete restrict on update cascade
);

/* tambahan untuk coba resep*/
// TUNGGU DATABASE OTHEB TERBARU JADI BELUM BISA DIPAKE ENTITIY RESEP
create table resep (
	id_resep serial not null
	
	


);

// relationship

create table pengeluaran_obat (
	id_log_obat_keluar int not null,
	id_obat int not null,
	tgl_kadaluarsa date not null,
	jumlah int not null,
	total int,
	id_resep int, //Belum terhubung ke bagian RESEP
	
	primary key(id_log_obat_keluar, id_obat, tgl_kadaluarsa),
	foreign key(id_obat, tgl_kadaluarsa) references kadaluarsa_obat 
	on delete restrict on update cascade,
	foreign key(id_log_obat_keluar) references log_obat_keluar 
	on delete restrict on update cascade
	
	foreign key(id_resep) references resep
	on delete restrict on update cascade
);

create table penerimaan_obat (
	id_log_obat_masuk int not null,
	id_obat int not null,
	tgl_kadaluarsa date not null,
	jumlah int not null,
	total int,
	primary key(id_log_obat_masuk, id_obat, tgl_kadaluarsa),
	foreign key(id_obat, tgl_kadaluarsa) references kadaluarsa_obat 
	on delete restrict on update cascade,
	foreign key(id_log_obat_masuk) references log_obat_masuk 
	on delete restrict on update cascade
);

create table update_obat (
	id_update serial not null,
	deskripsi text not null,
	id_obat int not null,
	id_user varchar(30) not null,
	primary key(id_update),
	foreign key(id_obat) references obat
	on delete restrict on update cascade,
	foreign key(id_user) references pengguna
	on delete restrict on update cascade
);

// tambahan

create table log_salemba (
	id_log_salemba serial not null,
	jenis_log varchar(10) not null, /*masuk/keluar*/
	tgl_log date not null,
	deskripsi text not null,
	id_obat int not null,
	jumlah int not null,
	primary key(id_log_salemba),
	foreign key(id_obat) references obat
	on delete restrict on update cascade
);