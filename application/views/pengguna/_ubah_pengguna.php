<div class="container">
        <!--Body content-->
	        <div class="page-header" style="margin: 150px 0 0 0">
		        <h1>Ubah Apoteker - <?php echo $this->session->userdata('username'); ?></h1>
	        </div>
			<div id="tableForm" style="margin: 20px 0 0 0">
				<?php echo validation_errors(); ?>
				<?php echo form_open('admin/pengguna_controller/ubah/'.$pengguna->username, array('class'=>'form-horizontal','name'=>'ubah','id'=>'ubah')); ?>
			        <div class="control-group">
				        <label class="control-label" for="inputEmail">Username</label>
		    		    <div class="controls">
		        			<input type="text" placeholder="Username" value="<?php echo $pengguna->username; ?>" style="background-color: #EAEDF1; height: 30px" id="username" name="username">
		        		</div>
		        	</div>
			        <div class="control-group">
				        <label class="control-label" for="inputEmail">NIP</label>
		    		    <div class="controls">
		        			<input type="text" placeholder="" value="<?php echo $pengguna->nip; ?>"  style="background-color: #EAEDF1; height: 30px" id="nip" name="nip" >
		        		</div>
		        	</div>
			        <div class="control-group">
				        <label class="control-label" for="inputPassword">Password Baru</br></label>
		    		    <div class="controls">
							<input type="password" placeholder="Password" style="background-color: #EAEDF1; height: 30px" id="password" name="password">
		        		</div>
		        	</div>
		        	<div class="control-group">
		        		<label class="control-label" for="inputPassword">Konfirmasi Password</label>
		        		<div class="controls">
		        			<input type="password" id="inputPassword" placeholder="Konfirmasi Password" style="background-color: #EAEDF1; height: 30px" id="konfirmasi" name="konfirmasi">
		        		</div>
		        	</div>
		        	<div class="control-group">
		        		<label class="control-label" for="inputPassword">Peran</label>
		        		<div class="controls" >
		        			<select>
		        				
		        				<?php echo form_dropdown($flag_admins, ($pengguna->flag_admin == 1) ? 'kepala' : 'apoteker' ); ?>
								
							</select>
		        		</div>
		        	</div>
			        <div class="form-actions">
				        <button type="submit" class="btn btn-primary">Simpan</button>
				        <button type="button" class="btn btn-primary"  style="margin-left: 30px" >Batal</button>
			        </div>
		        <?php echo form_close(); ?>
			</div>
        </div>
        </div>