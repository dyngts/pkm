<div class="container">
        <!--Body content-->
	        <div class="page-header" style="margin: 150px 0 0 0">
		        <h1>Tambahkan Apoteker Baru</h1>
	        </div>
			<div id="tableForm" style="margin: 20px 0 0 0">
				<?php echo validation_errors(); ?>
				<?php echo form_open('admin/pengguna_controller/tambah', array('class'=>'form-horizontal','name'=>'tambah','id'=>'tambah')); ?>
			        <div class="control-group">
				        <label class="control-label" for="inputEmail">Username</label>
		    		    <div class="controls">
		        			<input type="text" placeholder="Username" style="background-color: #EAEDF1; height: 30px" name="username" id="username">
		        		</div>
		        	</div>
		        	<div class="control-group">
		        		<label class="control-label" for="inputPassword">Peran</label>
		        		<div class="controls">
		        			<select id='peran' name='peran'>
								<option value='apoteker' >Apoteker</option>
								<option value='kepala' selected='true'>Kepala Apoteker</option>
							</select>
		        		</div>
		        	</div>
			        <div class="form-actions">
<input type="submit" class="btn btn-primary" title="simpan" value="Simpan" id="simpan" name="simpan" /> 
<input type="submit" class="btn btn-primary" style="margin-left: 30px" title="batal" value="Batal" id="batal" name="batal" onClick="return confirm( 'Yakin ingin keluar dari halaman ini?' )" /> 
			        </div>
		        <?php echo form_close(); ?>
			</div>
        </div>
        </div>