 <div class="container">
                <!--Body content-->
                <div class="page-header" style="margin: 150px 0 0 0">
                    <h1>Daftar Apoteker</h1>
                </div>
                <div id="tableForm" style="margin: 20px 0 0 0">
                    <?php if(count($apotekers) > 0) { ?>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td style="text-align: center">No</td>
                                <td style="text-align: center">Username</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($apotekers as $apoteker) {?>
                            <tr>
                                <td style="text-align: center"><?php echo ++$mulai; ?></td>
                                <td style="text-align: center"><?php echo $apoteker->username; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php } else { ?>
                    <h3>Tidak ada apoteker yang terdaftar</h3>
                    <?php } ?>
                    <?php echo $pagination_links; ?>
                </div>
            </div>
        </div>