<center>
	<h2>Laporan Pendapatan Poli Umum<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) Depok<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            JENIS KELAMIN
	                        </th>
	                        <th>
	                            UMUR
	                        </th>
	                        <th>
	                            DIAGNOSA
	                        </th>
	                        <th>
	                            TINDAKAN
	                        </th>
	                        <th>
	                            JASA DOKTER
	                        </th>
	                        <th>
	                            JASA TINDAKAN
	                        </th>
	                        <th>
	                            POLI UMUM NETTO
	                        </th>
	                        <th>
	                            POLI UMUM JP PKM
	                        </th>
	                        <th>
	                            PENDAPATAN POLI UMUM
	                        </th>
	                        <th>
	                            DOKTER PEMERIKSA
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totaljasa = 0;
	                    $totaltindakan = 0;
	                    $totalnetto = 0;
	                    $totaljp = 0;
	                    $totalpendapatan = 0;
	                    if($laporanbulanan == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    }
	                   		/*echo('<code><pre>');
	                    	print_r($laporanbulanan);
	                    	echo('</pre></code>');*/
	                    foreach ($laporanbulanan as $row) {
	                    	$umur = date("Y/m/d") - $row['tanggal_lahir'];
	                    	$tindakanumum = $this->kasirmod->getbiayatindakanumum($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
	                    	echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['jenis_kelamin']."</td>";
		                    echo "<td>".$umur."</td>";
		                    $datadiagnosa = $this->doktermod->getDataDiagnosa($row['id_rekam_medis'],$row['tanggal']);
		                    $konsultasi = $this->kasirmod->getKonsultasi($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
		                    $username_dokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
		                    $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
		                    $diagnosa = null;
		                    $coy = 1;
		                    $length = sizeof($datadiagnosa);
		                    foreach($datadiagnosa as $diagnosapasien) {	
		                    	if($coy == $length){
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'];
		                    	}
		                    	else {
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
		                    	}
		                    	$coy++;
		                    }
	                    	echo "<td>".$diagnosa."</td>";
		                    $j = 0;
		                    $tarifpoliumum = 0;
		                    $jasadokter = 0;

	                    	if($row['jenis_pasien'] == '1'){
								$tarifpoliumum += $row['tarif_mahasiswa'];
							}
							else if($row['jenis_pasien'] == '2'){
								$tarifpoliumum += $row['tarif_karyawan'];								
							}
							else {
								$tarifpoliumum += $row['tarif_umum'];									
							}   
		                   
		                    $jasatindakan = $tarifpoliumum - $jasadokter;
		                	$jepe = 0.2*($tarifpoliumum);
		                	$pendapatan = $tarifpoliumum - $jepe;
		                    echo "<td>".$row['nama']."</td>";
		                    if($konsultasi != null) {
		                    	$jasadokter = 15000;
		                    }
		                    echo "<td>".$jasadokter."</td>";
		                    echo "<td>".$jasatindakan."</td>";
		                    echo "<td>".$tarifpoliumum."</td>";
		                    echo "<td>".$jepe."</td>";
		                    echo "<td>".$pendapatan."</td>";
		                    echo "<td>".$namadokter."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totaljasa += $jasadokter;
		                    $totaltindakan += $jasatindakan;
		                    $totalnetto += $tarifpoliumum;
		                    $totaljp += $jepe;
		                    $totalpendapatan += $pendapatan;
	                   	}
	                   	?>
	                   		<tr>
	                   			<td colspan = '7'>JUMLAH</td>
	                   			<td><?php echo $totaljasa;?></td>
	                   			<td><?php echo $totaltindakan;?></td>
	                   			<td><?php echo $totalnetto;?></td>
	                   			<td><?php echo $totaljp;?></td>
	                   			<td><?php echo $totalpendapatan;?></td>
	                   			<td></td>
	                   		</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_umum_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_umum'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_umum'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>