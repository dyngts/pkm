<center>
	<h2>Laporan Bulanan, Bulan <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NOMOR
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            POLI UMUM
	                        </th>
	                        <th>
	                            PENDAPATAN POLI UMUM
	                        </th>
	                        <th >
	                            POLI GIGI
	                        </th>
	                        <th>
	                            BIAYA DENTAL LAB
	                        </th>
	                        <th>
	                            POLI GIGI NETTO
	                        </th>
	                        <th>
	                            PENDAPATAN POLI GIGI
	                        </th>
	                        <th>
	                            PENDAPATAN RUANG FARMASI KARYAWAN
	                        </th>
	                        <th>
	                            PENDAPATAN RUANG FARMASI UMUM
	                        </th>
	                         <th>
	                            RADIOLOGI
	                        </th>
	                         <th>
	                            AMBULANS
	                        </th>
	                        <th>
	                            SALDO AKHIR
	                        </th>
	                        <th>
	                            PENDAPATAN POLI UMUM DAN GIGI
	                        </th>
	                        <th>
	                            PENDAPATAN PKM
	                        </th>
	                        <th>
	                            PENDAPATAN UI
	                        </th>
	                        <th>
	                            TOTAL
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $total1 = 0;
	                    $total2 = 0;
	                    $total3 = 0;
	                    $total4 = 0;
	                    $total5 = 0;
	                    $total6 = 0;
	                    $total7 = 0;
	                    $total8 = 0;
	                    $total9 = 0;
	                    $total10 = 0;
	                    $total11 = 0;
	                    $total12 = 0;
	                    $total13 = 0;
	                    $total14 = 0;
	                    $totalpendapatanbulanan = 0;
	                    if($laporanbulanan == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    }
	                    foreach($laporanbulanan as $row) {
	                    	echo "<tr>";
	                    	echo "<td>"."$i"."</td>";
	                    	echo "<td>".$row['tanggal']."</td>";
	                    	$biayapoliumum = $this->kasirmod->getPoliUmumHarian($row['tanggal']);
	                    	$biayapoligigi = $this->kasirmod->getPoliGigiHarian($row['tanggal']);
	                    	$biayafarmasi = $this->kasirmod->getFarmasiHarian($row['tanggal']); 
	                    	$biayaradiologi = $this->kasirmod->getRadiologiHarian($row['tanggal']); 
	                    	$biayaambulans = $this->kasirmod->getAmbulansHarian($row['tanggal']); 

	                    	$poliumum = 0;

	                    	$poligigi = 0;
	                    	$biayadental = 0;

	                    	$obatkaryawan = 0;
	                    	$obatumum = 0;

	                    	$radiologi = 0;
	                    	$ambulans = 0;
	        
	                    	foreach($biayapoliumum as $bumum) {  

		                    	if($bumum['jenis_pasien'] == '1'){
									$poliumum += $bumum['tarif_mahasiswa'];
								}
								else if($bumum['jenis_pasien'] == '2'){
									$poliumum += $bumum['tarif_karyawan'];								
								}
								else {
									$poliumum += $bumum['tarif_umum'];									
								}    	
		                    } 
		                    
		                    $pendapatanpoliumum = 0.8*($poliumum);

		                    foreach($biayapoligigi as $bgigi) {  

		                    	if($bgigi['jenis_pasien'] == '1'){
									$poligigi += $bgigi['tarif_mahasiswa'];
								}
								else if($bgigi['jenis_pasien'] == '2'){
									$poligigi += $bgigi['tarif_karyawan'];								
								}
								else {
									$poligigi += $bgigi['tarif_umum'];									
								}      	
		                    } 


	                    	$poligiginetto = $poligigi-$biayadental;
	                    	$pendapatanpoligigi = 0.8*($poligiginetto);

		                    foreach($biayafarmasi as $bfarm) {  
								if($bfarm['jenis_pasien'] == '2'){
									$obatkaryawan += 2500;							
								}
								else if($bfarm['jenis_pasien'] == '3') {
									$hargaobat = $bfarm['harga_per_kemasan']*$bfarm['jumlah'];
									$obatumum +=$hargaobat;						
								}      	
		                    } 

		                    foreach($biayaradiologi as $bradio) {
		                    	if($bradio['jenis_pasien'] == '1'){
									$radiologi += $bradio['tarif_mahasiswa'];
								}
								else if($bgigi['jenis_pasien'] == '2'){
									$radiologi += $bradio['tarif_karyawan'];								
								}
								else {
									$radiologi += $bradio['tarif_umum'];									
								}
		                    }

		                     foreach($biayaambulans as $bambu) {
		                    	if($bambu['jenis_pasien'] == '1'){
									$ambulans += $bambu['tarif_mahasiswa'];
								}
								else if($bambu['jenis_pasien'] == '2'){
									$ambulans += $bambu['tarif_karyawan'];								
								}
								else {
									$ambulans += $bambu['tarif_umum'];									
								}
		                    }

		                    $saldoakhir = $pendapatanpoliumum+$poligiginetto+$obatumum+$obatkaryawan+$radiologi+$ambulans;
		                    $totalgigiumum = $pendapatanpoliumum+$pendapatanpoligigi;
		                    $pendapatanPKM = 0.2*($poliumum)+0.2*($poligiginetto);
		                    $totalsemua = $poliumum+$poligigi+$obatumum+$obatkaryawan+$radiologi+$ambulans;
		                    $pendapatanUI = 0.8*($totalsemua);
		                    echo "<td>".$poliumum."</td>"; 
		                    echo "<td>".$pendapatanpoliumum."</td>"; 
		                    echo "<td>".$poligigi."</td>"; 
		                    echo "<td>".$biayadental."</td>"; 
		                    echo "<td>".$poligiginetto."</td>"; 
		                    echo "<td>".$pendapatanpoligigi."</td>"; 
		                    echo "<td>".$obatkaryawan."</td>"; 
		                    echo "<td>".$obatumum."</td>"; 
		                    echo "<td>".$radiologi."</td>"; 
		                    echo "<td>".$ambulans."</td>"; 
		                    echo "<td>".$saldoakhir."</td>"; 
		                    echo "<td>".$totalgigiumum."</td>"; 
		                    echo "<td>".$pendapatanPKM."</td>"; 
		                    echo "<td>".$pendapatanUI."</td>"; 
		                    echo "<td>".$totalsemua."</td>"; 
		                    $total1 += $poliumum;
		                    $total2 += $pendapatanpoliumum;
		                    $total3 += $poligigi;
		                    $total4 += $biayadental;
		                    $total5 += $poligiginetto;
		                    $total6 += $pendapatanpoligigi;
		                    $total7 += $obatkaryawan;
		                    $total8 += $obatumum;
		                    $total9 += $radiologi;
		                    $total10 += $ambulans;
		                    $total11 += $saldoakhir;
		                    $total12 += $totalgigiumum;
		                    $total13 += $pendapatanPKM;
		                    $total14 += $pendapatanUI;
		                    $totalpendapatanbulanan += $totalsemua;
	                    	$i++;
	                    }
	                    ?>
	                    <tr>
	                    <td id='jumlahbulanan' colspan='2'>Jumlah Pendapatan</td>
	                    <td><?php echo $total1;?></td>
	                    <td><?php echo $total2;?></td>
	                    <td><?php echo $total3;?></td>
	                    <td><?php echo $total4;?></td>
	                    <td><?php echo $total5;?></td>
	                    <td><?php echo $total6;?></td>
	                    <td><?php echo $total7;?></td>
	                    <td><?php echo $total8;?></td>
	                    <td><?php echo $total9;?></td>
	                    <td><?php echo $total10;?></td>
	                    <td><?php echo $total11;?></td>
	                    <td><?php echo $total12;?></td>
	                    <td><?php echo $total13;?></td>
	                    <td><?php echo $total14;?></td>
	                    <td><?php echo $totalpendapatanbulanan;?></td>
	                    </tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_bulanan'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>