<h2 id='pasienbayar'>Daftar Pasien yang telah Disetujui:</h2> <br>
	<center>
	<div id = 'tabel_tagihan'>
		<div class="tabel_pasien" >
		                <table >
		                    <tr>
		                    	<th class="first">
		                    		Tanggal Tagihan
		                    	</th>
		                        <th class="even-col">
		                            Id Pasien
		                        </th>
		                        <th class="first">
		                            Dokter <br/>yang Menyetujui
		                        </th>
		                        <th class="even-col">
		                            Status Pembayaran
		                        </th>
														<th class="first">
															Waktu <br/>Selesai Pemeriksaan
														</th>
		                        <th class="even-col">
		                            Lihat Rincian
		                        </th>
		                    </tr>
		                    <?php
	      			        	foreach ($query as $row) {
	            				if($row['status'] == 'f') {
	            					echo '<tr>';
	            					echo '<td class="text-left">';
			                    	echo $row['tanggal'];
			                    	echo '</td>';
			                    	echo '<td class="text-right">';
			                    	echo $row['id_rekam_medis'];
			                    	echo '</td>';
			                    	echo '<td class="text-left">';
			                    	$namadokter = $this->kasirmod->getNamebyUsername($row['username_dokter']);
			                    	echo $namadokter;
			                    	echo '</td>';
			                    	echo '<td>';
			                    	echo 'Belum Lunas';
			                    	echo '</td>';
									echo '<td>';
			                    	echo $row['waktu'];
			                    	echo '</td>';
			                    	echo '<td>';?>
			                    	<?php echo form_open('kasir/rincian_pembayaran');?>
										<input type='hidden' name="waktu" value="<?php echo $row['waktu'];?>" />
				                    	<input type='hidden' name="tanggal" value="<?php echo $row['tanggal'];?>" />
				                    	<input type='hidden' name="idrekam" value="<?php echo $row['id_rekam_medis'];?>" />
				                    	<input type='submit' value = 'Lihat Rincian' class = 'button green table_button'/>
			                   		<?php echo form_close();?>
			                    	<?php
			                    	echo '</td>';
			                    	echo '</tr>';
	            				}
	                    	}	                			  	
		                    	
		                    ?>
		                </table>
		        </div>
	        </div>
	            <br>
	</center>
