<center>
	<h2>LAPORAN PENDAPATAN LABORATORIUM<br>
		BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            NO KUITANSI
	                        </th>
	                        <th>
	                            JENIS PEMERIKSAAN
	                        </th>
	                        <th>
	                            HARGA
	                        </th>
	                        <th>
	                            BAGI HASIL PKM
	                        </th>
	                        <th>
	                            DOKTER PENGIRIM
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
                    	$totalhargalab = 0;
                    	$totalbagihasil = 0;
                    	if($laporanbulanan == null) {
	                    	echo "<h2>Tidak ada data pada bulan ini</h2>";
	                    }
	                    foreach ($laporanbulanan as $row) {
	                    	$hargatindakanlab = $row['tarif_umum'];
	                    	$bagihasilpkm = 0.3 * $hargatindakanlab;
	                    	$namadokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
                    		echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['no_kuitansi']."</td>";
		                    echo "<td>".$row['nama']."</td>";
		                    echo "<td>Rp. ".$hargatindakanlab."</td>";
		                    echo "<td>Rp. ".$bagihasilpkm."</td>";
		                    echo "<td>".$namadokter."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totalhargalab += $hargatindakanlab;
		                    $totalbagihasil += $bagihasilpkm;
		                }
	                   	?>
	                   	<tr>
		                    <td colspan = '5' style='text-align:center;'><h2>Jumlah:</h2></td>
		                   	<td><?php echo 'Rp. '.$totalhargalab ?></td>
		                   	<td><?php echo 'Rp. '.$totalbagihasil ?></td>
		                   	<td></td>
	                	</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_lab_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_laboratorium'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_lab'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>