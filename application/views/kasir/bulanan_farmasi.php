<center>
	<h2>Laporan Pendapatan Ruang Farmasi<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) Depok<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            KARYAWAN
	                        </th>
	                        <th>
	                            UMUM
	                        </th>
	                        <th>
	                            DOKTER PEMERIKSA
	                        </th>
	                        <th>
	                            BIAYA KARYAWAN
	                        </th>
	                        <th>
	                            BIAYA UMUM
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalhargakaryawan = 0;
                    	$totalhargaumum = 0;
                    	if($laporanbulanan == null) {
	                    	echo "<h2>Tidak ada data pada bulan ini</h2>";
	                    }
	                    foreach ($laporanbulanan as $row) {
	                    		$usernamedokter = $this->kasirmod->getUsernameDokter($row['tanggal'],$row['id_rekam_medis'], $row['waktu_tagih']);
	                    		$namadokter = $this->kasirmod->getNamebyUsername($usernamedokter);
	                    		echo "<tr>";
			                    echo "<td>"."$i"."</td>";
			                    echo "<td>".$row['tanggal']."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    $hargaobatkaryawan = 0;
			                   	if ($row['jenis_pasien'] == '2'){
			                   		$hargaobatkaryawan = 2500;
			                   		 echo "<td>YA</td>";
			                   		 echo "<td>BUKAN</td>";
			                   		 echo "<td>".$namadokter."</td>";
			                   		 echo "<td>".$hargaobatkaryawan."</td>";
			                   		 echo "<td> 0 </td>";
			                   		 $totalhargakaryawan += $hargaobatkaryawan;
			                   	}
			                   	else if ($row['jenis_pasien'] == '3'){
			                   		$hargaobatkaryawan = 0;
			                   		echo "<td>BUKAN</td>";
			                   		echo "<td>YA</td>";
			                   		echo "<td>".$namadokter."</td>";
			                   		echo "<td>".$hargaobatkaryawan."</td>";
			                   		$data1 = $this->kasirmod->gethargaobat($row['id_rekam_medis'],$row['tanggal'], $row['waktu_tagih']);
			                   		$biayaobat = $data1->result_array();
			                   		$hargaobat = 0;
			                   		foreach ($biayaobat as $bobat) {
			                   			$jumlah = $bobat['jumlah'];
			                   			$hargaperkemasan = $bobat['harga_per_kemasan'];
			                   			$hargaobat += $jumlah * $hargaperkemasan;
			                   		}
			                   		echo "<td>".$hargaobat."</td>";
			                   		$totalhargaumum += $hargaobat;
			                   	}
			                   	
			                    $i++;
			                    echo "</tr>";
	                    
	                    	
	                   	}
	                   	?>
	                   	<tr>
		                    <td colspan = '6' style='text-align:center;'><h2>Jumlah:</h2></td>
		                	<td><?php echo $totalhargakaryawan;?></td>
		                	<td><?php echo $totalhargaumum;?></td>
	                	</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_farmasi_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_farmasi'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_farmasi'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>