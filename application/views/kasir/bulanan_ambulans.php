<center>
	<h2>LAPORAN PENDAPATAN AMBULANS <br>
	PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK																			
<br> BULAN: <?php echo $bulan; ?></h2>

	                <table  class="tabel_laporan" >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                        	JENIS PEMASUKKAN AMBULANS
	                        </th>
	                        <th>
	                            BIAYA
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalbiaya = 0;
	                    if($laporanbulanan == null) {
	                    	echo "<h2>Tidak ada data pada bulan ini</h2>";
	                    }
	                    foreach ($laporanbulanan as $row) {
	                    	echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                 	echo "<td>".$row['nama']."</td>";
		                 	if ($row['jenis_pasien'] == '1'){
		                 		echo "<td> Rp. ".$row['tarif_mahasiswa']."</td>";
		                 		$totalbiaya += $row['tarif_mahasiswa'];
		                 	}
		                 	else if ($row['jenis_pasien'] == '2'){
		                 		echo "<td>Rp. ".$row['tarif_karyawan']."</td>";
		                 		$totalbiaya += $row['tarif_karyawan'];
		                 	}
		                 	else {
		                 		echo "<td> Rp. ".$row['tarif_umum']."</td>";
		                 		$totalbiaya += $row['tarif_umum'];
		                 	}

		                    $i++;
		                    echo "</tr>";
	                   	}
	                   	?>
	                   	<tr>
		                   	<td colspan='4'> TOTAL </td>
		                   	<td><?php echo 'Rp. '.$totalbiaya; ?></td>
	                   	</tr>
	                </table>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_ambulans_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_ambulans'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_ambulans'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>