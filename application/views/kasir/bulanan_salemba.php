<center>
	<h2>Laporan Pendapatan Poli Umum</h2>
	<br><h2>Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) Salemba</h2> 
	<br><h2>Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            Nomor
	                        </th>
	                        <th>
	                            Tanggal
	                        </th>
	                        <th>
	                            Nama 
	                            Pasien
	                        </th>
	                        <th>
	                            Jenis
	                            Kelamin
	                        </th>
	                        <th >
	                            Umur
	                        </th>
	                        <th>
	                            Diagnosa
	                        </th>
	                        <th>
	                            Tindakan
	                        </th>
	                        <th>
	                            Jasa
	                            Dokter
	                        </th>
	                        <th>
	                            Jasa
	                            Tindakan
	                        </th>
	                        <th>
	                            Poli
	                            Umum
	                            Netto
	                        </th>
	                         <th>
	                            Poli 
	                            Umum
	                            JP PKM
	                        </th>
	                         <th>
	                            Pendapatan 
	                            Poli Umum
	                        </th>
	                        <th>
	                            Dokter
	                            Pemeriksa
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $total1 = 0;
	                    $total2 = 0;
	                    $total3 = 0;
	                    $total4 = 0;
	                    $total5 = 0;
	                    

	                    $totalpendapatanbulanan = 0;
	                    if($laporan_salemba == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    }
	                    foreach($laporan_salemba as $row) {
	                    	echo "<tr>";
	                    	echo "<td>"."$i"."</td>";
	                    	echo "<td>".$row['tanggal']."</td>";
	                    	echo "<td>".$row['nama_pasien']."</td>";
	                    	echo "<td>".$row['jenis_kelamin']."</td>";
	                    	echo "<td>".$row['umur']."</td>";
	                    	echo "<td>".$row['diagnosa']."</td>";
	                    	echo "<td>".$row['tindakan']."</td>";
	                    	echo "<td>".$row['jasa_dokter']."</td>";
	                    	echo "<td>".$row['jasa_tindakan']."</td>";
	                    	echo "<td>".$row['poli_umum_netto']."</td>";
	                    	echo "<td>".$row['poli_umum_jp_pkm']."</td>";
	                    	echo "<td>".$row['pendapatan_poli_umum']."</td>";
	                    	echo "<td>".$row['dokter_pemeriksa']."</td>";
	                    	
		                   
		                   
		                    $total1 = $total1 + $row['jasa_dokter'];
		                    $total2 = $total2 + $row['jasa_tindakan'];
		                    $total3 = $total3 + $row['poli_umum_netto'];
		                    $total4 = $total4 + $row['poli_umum_jp_pkm'];
		                    $total5 = $total5 + $row['pendapatan_poli_umum'];

		                    //$totalpendapatanbulanan += $totalsemua;
	                    	$i++;
	                    }
	                    ?>
	                    <tr>
	                    <td id='jumlahbulanan' colspan='7' bgcolor = '#F0F0F0'>Jumlah Pendapatan</td>
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total1;?></td>
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total2;?></td>
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total3;?></td>
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total4;?></td>
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total5;?></td>
	                    <!--<td>Rp. <?php echo $totalpendapatanbulanan;?></td>-->
	                    <td bgcolor = '#F0F0F0'></td>
	                    </tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_salemba_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_salemba'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_salemba'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>
