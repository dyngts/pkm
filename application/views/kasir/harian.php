<center>
	<div id ='laporanharian'>
	<h2>Pendapatan Harian PKM UI Depok <br>Tanggal <?php echo $tanggal; ?></h2>
	</div>
	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            Nomor
	                        </th>
	                        <th>
	                            Nama Dokter
	                        </th>
	                        <th>
	                            Nama Pasien
	                        </th>
	                        <th>
	                            No. Kuitansi
	                        </th>
	                        <th >
	                            Fakultas
	                        </th>
	                        <th>
	                            Poli Umum
	                        </th>
	                        <th>
	                            Poli Umum JP PKM
	                        </th>
	                        <th>
	                            Pendapatan Poli Umum
	                        </th>
	                        <th>
	                            Poli Gigi
	                        </th>
	                        <th>
	                            Biaya Dental Lab
	                        </th>
	                        <th>
	                            Poli Gigi NETTO
	                        </th>
	                         <th>
	                            Poli Gigi JP PKM
	                        </th>
	                         <th>
	                            Pendapatan Poli Gigi
	                        </th>
	                        <th>
	                            Pendapatan Ruang Farmasi Karyawan
	                        </th>
	                        <th>
	                           	Pendapatan Ruang Farmasi Umum
	                        </th>
	                         <th>
	                            Radiologi
	                        </th>
	                         <th>
	                            Ambulans
	                        </th>
	                    </tr>
	                    <?php 
	                    $i=1;
	                    $totalpoliumum = 0;
	                    $totalbiayajpumum = 0;
	                    $totalnettoumum =0;
	                    $totalpoligigi = 0;
	                    $totalbiayadental = 0;
	                    $totalnettogigi = 0;
	                    $totalbiayajpgigi =0;
	                    $totalpendapatangigi = 0;
	                    $totalobatkaryawan = 0;
	                    $totalobatumum = 0;
	                    $totalradiologi = 0;
	                    $totalambulans = 0;
            
	                    foreach ($laporanharian as $row) {
	                    	$usernamedokter = $this->kasirmod->getUsernameDokter($tanggal,$row['id_rekam_medis'], $row['waktu_tagih']);
	                    	$namadokter = $this->kasirmod->getNamebyUsername($usernamedokter);
                    		$query2 = $this->kasirmod->getbiayatindakan($row['id_rekam_medis'], $tanggal, $row['waktu_tagih']);
							$query3 =  $this->kasirmod->gethargaobat($row['id_rekam_medis'], $tanggal, $row['waktu_tagih']);
							$array2 = $query2->result_array();
							$array3 = $query3->result_array();
							$tarifpoliumum = 0;
							$tarifpoligigi = 0;
							$biayadental = 0;
							$obatkaryawan = 0;
							$obatumum = 0;
							$biayaradiologi = 0;
							$biayaambulans = 0;
							
							foreach ($array2 as $rowtindakan) {
								if($row['jenis_pasien'] == '1'){
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan['tarif_mahasiswa'];
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_mahasiswa'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_mahasiswa'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_mahasiswa'];
									}
								}
								else if($row['jenis_pasien'] == '2'){
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan->tarif_karyawan;
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_karyawan'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_karyawan'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_karyawan'];
									}
								}
								else {
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan['tarif_umum'];
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_umum'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_umum'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_umum'];
									}
								}
							}

							foreach($array3 as $rowobat){
								if($row['jenis_pasien'] == '1'){
									$obatkaryawan = 0;
									$obatumum = 0;
								}
								else if($row['jenis_pasien'] == '2'){
									$obatkaryawan = 2500;
								}
								else {
									$obatumum += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
								}
							}
							
							$biayajp = 0.2*($tarifpoliumum);
							$biayajpgigi = 0.2*($tarifpoligigi);
							$nettotindakanumum = $tarifpoliumum-$biayajp;
							$nettogigi = $tarifpoligigi-$biayadental;
							$pendapatangigi = $nettogigi-$biayajpgigi;
							if($tarifpoliumum != 0 || $tarifpoligigi != 0 || $obatkaryawan != 0 || $obatumum != 0 || $biayaradiologi != 0 || $biayaambulans != 0 ){
								echo "<tr>";
			                    echo "<td>"."$i"."</td>";
			                    echo "<td>".$namadokter."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    echo "<td>".$row['no_kuitansi']."</td>";
			                    echo "<td>".$row['lembaga_fakultas']."</td>";
								echo "<td> ".$tarifpoliumum."</td>";
			                    echo "<td> ".$biayajp."</td>";
			                    echo "<td> ".$nettotindakanumum."</td>";
			                    echo "<td> ".$tarifpoligigi."</td>";
			                    echo "<td> ".$biayadental."</td>";
			                    echo "<td> ".$nettogigi."</td>";
			                    echo "<td> ".$biayajpgigi."</td>";
			                    echo "<td> ".$pendapatangigi."</td>";
			                    echo "<td> ".$obatkaryawan."</td>";
			                    echo "<td> ".$obatumum."</td>";
			                    echo "<td> ".$biayaradiologi."</td>";
			                    echo "<td> ".$biayaambulans."</td>";
			                    $i++;
			                    echo "</tr>";
		                    }
		                    
		                    $totalpoliumum += $tarifpoliumum;
		                    $totalbiayajpumum += $biayajp;
		                    $totalnettoumum +=$nettotindakanumum;
		                    $totalpoligigi += $tarifpoligigi;
		                    $totalbiayadental += $biayadental;
		                    $totalnettogigi += $nettogigi;
		                    $totalbiayajpgigi +=$biayajpgigi;
		                    $totalpendapatangigi += $pendapatangigi;
		                    $totalobatkaryawan += $obatkaryawan;
		                    $totalobatumum += $obatumum;
		                    $totalradiologi += $biayaradiologi;
		                    $totalambulans += $biayaambulans;
	                    	
	                   	}
	                    ?>
	                    <tr>
		                    <td colspan="5"><h2>Jumlah:</h2></td>
		                	<td><?php echo ' '.$totalpoliumum;?></td>
		                	<td><?php echo ' '.$totalbiayajpumum;?></td>
		                	<td><?php echo ' '.$totalnettoumum;?></td>
		                	<td><?php echo ' '.$totalpoligigi;?></td>
		                	<td><?php echo ' '.$totalbiayadental;?></td>
		                	<td><?php echo ' '.$totalnettogigi;?></td>
		                	<td><?php echo ' '.$totalbiayajpgigi;?></td>
		                	<td><?php echo ' '.$totalpendapatangigi;?></td>
		                	<td><?php echo ' '.$totalobatkaryawan;?></td>
		                	<td><?php echo ' '.$totalobatumum;?></td>
		                	<td><?php echo ' '.$totalradiologi;?></td>
		                	<td><?php echo ' '.$totalambulans;?></td>
	                	</tr>
	                	<tr>
	                		<td colspan = '16' style='text-align:center;'><strong>Jumlah Pendapatan</strong> </td>
	                		<td><?php echo 'Rp. '.$totalobatumum+$totalnettoumum+$totalbiayadental+$totalpendapatangigi+$totalobatumum+$totalobatkaryawan;?></td>

	                	</tr>
	                </table>
	                
	            </div>
	            <br><br>
	            <button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/harian_xls'; ?>';">Unduh dalam format excel</button>		
				<!--
				<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_harian'; ?>'" class='button red'>Lihat Detail Print</button>
				<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_harian'; ?>'" class='button orange'>Unduh sebagai pdf</button>
	            -->
	            <br>
</center>
<br>