<center>
	<h2>Laporan Pendapatan Poli Gigi<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI)<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            JENIS KELAMIN
	                        </th>
	                        <th>
	                            UMUR
	                        </th>
	                        <th>
	                            DIAGNOSA
	                        </th>
	                        <th>
	                            TINDAKAN
	                        </th>
	                        <th>
	                            POLI GIGI
	                        </th>
	                        <th>
	                            BIAYA DENTAL LAB
	                        </th>
	                        <th>
	                            POLI GIGI NETTO
	                        </th>
	                        <th>
	                            POLI GIGI JP PKM
	                        </th>
	                        <th>
	                            PENDAPATAN POLI GIGI
	                        </th>
	                        <th>
	                            NAMA DOKTER
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalpoligigi = 0;
	                    $totalbiayadental = 0;
	                    $totalnetto = 0;
	                    $totaljp = 0;
	                    $totalpendapatan = 0;
	                    if($laporanbulanan == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    }
	                    foreach ($laporanbulanan as $row) {
	                    	$umur = date("Y/m/d") - $row['tanggal_lahir'];
	                    	$tindakangigi = $this->kasirmod->getbiayatindakangigi($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
		                    $datadiagnosa = $this->doktermod->getDataDiagnosa($row['id_rekam_medis'],$row['tanggal']);
		                    $username_dokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
		                    $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
		                    $diagnosa = null;
		                    $coy = 1;
		                    $length = sizeof($datadiagnosa);
		                    foreach($datadiagnosa as $diagnosapasien) {	
		                    	if($coy == $length){
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'];
		                    	}
		                    	else {
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
		                    	}
		                    	$coy++;
		                    }
	                    	
		                    $j = 0;
		                    $tarifpoligigi = 0;
		               
		              
	                    	if($row['jenis_pasien'] == '1'){
								$tarifpoligigi += $row['tarif_mahasiswa'];
							}
							else if($row['jenis_pasien'] == '2'){
								$tarifpoligigi += $row['tarif_karyawan'];								
							}
							else {
								$tarifpoligigi += $row['tarif_umum'];									
							}      	
		                   
		                    $biayadental = 0;
		                	$jepe = 0.2*($tarifpoligigi);
		                	$pendapatan = $tarifpoligigi - $jepe;
		                	$netto = $tarifpoligigi - $biayadental;
		                	
		                	if($tarifpoligigi != 0) {
		                		echo "<tr>";
		                		echo "<td>"."$i"."</td>";
			                    echo "<td>".$row['tanggal']."</td>";
			                    echo "<td>".$row['nama_pasien']."</td>";
			                    echo "<td>".$row['jenis_kelamin']."</td>";
			                    echo "<td>".$umur."</td>";
			                    echo "<td>".$diagnosa."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    echo "<td>".$tarifpoligigi."</td>";
			                    echo "<td>".$biayadental."</td>";
			                    echo "<td>".$netto."</td>";
			                    echo "<td>".$jepe."</td>";
			                    echo "<td>".$pendapatan."</td>";
			                    echo "<td>".$namadokter."</td>";
			                    $i++;
		                    	echo "</tr>";
		                	}
		 
		                    $totalpoligigi += $tarifpoligigi;
		                    $totalbiayadental +=$biayadental;
		                    $totalnetto += $netto;
		                    $totaljp += $jepe;
		                    $totalpendapatan += $pendapatan;
	                   	}
	                   	?>
	                   	<tr>
	                   		<td colspan= '7'> JUMLAH </td>
	                   		<td><?php echo $totalpoligigi;?></td>
	                   		<td><?php echo $totalbiayadental;?></td>
	                   		<td><?php echo $totalnetto;?></td>
	                   		<td><?php echo $totaljp;?></td>
	                   		<td><?php echo $totalpendapatan;?></td>
	                   		<td></td>
	                   	</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_gigi_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_gigi'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_gigi'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>