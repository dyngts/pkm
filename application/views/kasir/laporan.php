<script type="text/javascript">
	$(document).ready(function(){
		$("#bulanhari").change(function(){
			showDiv();
		  });
		  showDiv();
	});

	$(document).ready(function() {
		$( "#datepicker" ).datepicker();
	});

	function showDiv() {
		 // hide any showing
		$(".pilihan_div").each(function(){
			$(this).css('display','none');
		});

		// our target
		var target = "#pilihan_" + $("#bulanhari").val();                                                                                        
		var target2 = "#pilihan_bulanan_poli";
		var target3 = '#pilihan_tahun';
		var e =  document.getElementById("bulanhari");  
		var value = e.options[e.selectedIndex].value;

		if( $(target).length > 0 && value == "bulanan") {
			$(target).css('display','block');
			$(target2).css('display','block');
			$(target3).css('display','block');
		}
		else $(target).css('display','block');
	}

</script>

<center>
	<div id ='formuang'>
	<?php echo form_open("kasir/laporan_keuangan"); ?>
		<p><label>Jenis Laporan: </label>
				<select id='bulanhari' name='jenis' class='laporandd'>
				 <option value="bulanan"> Bulanan</option>
				 <option value='harian'> Harian</option>
				</select>
		</p>
		<p>
		<div id = 'pilihan_bulanan' class='pilihan_div'><label>Periode: </label>
				 <select id='bulan' name ='pilihbulan' class='laporandd'>
				 <option value="Desember"> Desember</option>
				 <option value='November'> November</option>
				 <option value="Oktober"> Oktober</option>
				 <option value='September'> September</option>
				 <option value="Agustus"> Agustus</option>
				 <option value='Juli'> Juli</option>
				 <option value="Juni"> Juni</option>
				 <option value='Mei'> Mei</option>
				 <option value='April'> April</option>
				 <option value='Maret'> Maret</option>
				 <option value="Februari"> Februari</option>
				 <option value='Januari'> Januari</option>
				</select>
		</div>
		</p>

		<p>
		<div id = 'pilihan_tahun' class = 'pilihan_div'>
			<label >Tahun : </label>
			<select id = 'tahun' name = 'pilihtahun' class='laporandd'>
				<option value = '2013'>2013</option>
				<?php 
					$tahun_skrg = date("Y");
					for($i = 2013; i<$tahun_skrg; $i++){
						echo "<option value = '$i'>".$i."</option>";
					}
				?>
			</select>
		</div>
		</p>

		<div id = 'pilihan_bulanan_poli' class='pilihan_div'>
			<label>Poli: </label>
				<select id='poli' name ='pilihpoli' class='laporandd'>
				<option value='Semua'> Semua</option>
				<option value="Umum"> Umum</option>
				<option value='Gigi'> Gigi</option>
				<option value='Estetika'> Estetika Medis</option>
				<option value='Farmasi'> Farmasi</option>
				<option value='Lab'> Laboratorium</option>
				<option value='Radiologi'> Radiologi</option>
				<option value='Ambulans'> Ambulans</option>
				<option value='Salemba'> Salemba</option>
				</select>
		</div>


		<div id = 'pilihan_harian' class='pilihan_div'><label>Pilih hari yang diinginkan:</label>
			<input type="text" id="datepicker" name="tanggal" value="<?php echo date("d-m-Y");?>"/>
		</div>
		<?php echo form_error('tanggal'); ?>
	</div>
	<br>
	<input type='submit' value='Submit' id='uangsub' class='btn btn-primary'/>
	<br>	
	<?php echo form_close(); ?>
	<br>
	<a href='..'> <button class='btn btn-info'>Kembali ke menu sebelumnya </button> </a>
</center>