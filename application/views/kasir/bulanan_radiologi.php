<center>
	<h2>LAPORAN PENDAPATAN RADIOLOGI<br>PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA ( PKM UI ) DEPOK<br>
		BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            JENIS PEMERIKSAAN RADIOLOGI
	                        </th>
	                        <th>
	                            JUMLAH PEMERIKSAAN RADIOLOGI
	                        </th>
	                        <th>
	                            BIAYA
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
                    	$totalbiayaradiologi = 0;
                    	if($laporanbulanan == null) {
	                    	echo "Tidak ada data pada bulan ini";
	                    }
	                    foreach ($laporanbulanan as $row) {
        	               
	                    	$biayaradiologi = $row['tarif_umum'];
	                    	if ($biayaradiologi!=null){
	                    		$jumlahtindakanradio = 1;
	                    	}
	                    	else $jumlahtindakanradio = 0;
                    		echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['nama']."</td>";
		                    echo "<td>".$jumlahtindakanradio."</td>";
		                    echo "<td>Rp. ".$biayaradiologi."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totalbiayaradiologi += $biayaradiologi;
		                }
	                   	?>
	                   	<tr>
		                    <td colspan = '5' style='text-align:center;'><h2>Jumlah:</h2></td>
		                   	<td><?php echo 'Rp. '.$totalbiayaradiologi ?></td>
	                	</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_radiologi_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_radiologi'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_radiologi'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>