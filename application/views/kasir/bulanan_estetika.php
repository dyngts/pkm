<center>
	<h2>LAPORAN PENDAPATAN ESTETIKA MEDIS 									
<br> BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_keuangan" >
	                <table >
	                    <tr>
	                    	<th>
	                            NO
	                        </th>
	                        <th>
	                            TANGGAL
	                        </th>
	                        <th>
	                            NAMA PASIEN
	                        </th>
	                        <th>
	                            NO KUITANSI
	                        </th>
	                        <th>
	                            NAMA TINDAKAN
	                        </th>
	                        <th>
	                        	HARGA
	                        </th>
	                        <th>
	                            BAGI HASIL PKM
	                        </th>
	                        <th>
	                            NAMA PRODUK
	                        </th>
	                        <th>
	                            BANYAKNYA
	                        </th>
	                        <th>
	                            HARGA
	                        </th>
	                        <th>
	                            BAGI HASIL PKM
	                        </th>
	                        <th>
	                            TOTAL PENDAPATAN ESTETIKA MEDIS
	                        </th>
	                        <th>
	                            TOTAL BAGI HASIL PKM
	                        </th>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $sumhargatindakan=0;
	                    $sumbagihasil = 0;
	                    $sumbanyakproduk = 0;
	                    $sumhargaproduk = 0;
	                    $sumbagihasilprod = 0;
	                    $sumpendapatan = 0;
	                    $sumtotalbagihasil = 0;
	                    if($laporanbulanan == null) {
	                    	echo "<h2>Tidak ada data pada bulan ini</h2>";
	                    }
	                    foreach ($laporanbulanan as $row) {
	                    	$hargatindakan = 0;
	                    	$dataobatestetika = $this->kasirmod->getObatEstetika($row['id'], $row['idrekmed'], $row['tanggal']);
	                    	echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['no_kuitansi']."</td>";
		                 	echo "<td>".$row['nama']."</td>";
		                 	if ($row['jenis_pasien'] == '1'){
		                 		$hargatindakan= $row['tarif_mahasiswa'];
		                 		$bagihasiltindakan = 0.3* $hargatindakan;
		                 		echo "<td>".$hargatindakan."</td>";
		                 		echo "<td>".$bagihasiltindakan."</td>";
		                 	}
		                 	else if ($row['jenis_pasien'] == '2'){
		                 		$hargatindakan = $row['tarif_karyawan'];
		                 		$bagihasiltindakan = 0.3* $hargatindakan;
		                 		echo "<td>".$hargatindakan."</td>";
		                 		echo "<td>".$bagihasiltindakan."</td>";
		                 	}
		                 	else {
		                 		$hargatindakan = $row['tarif_umum'];
		                 		$bagihasiltindakan = 0.3* $hargatindakan;
		                 		echo "<td>".$hargatindakan."</td>";
		                 		echo "<td>".$bagihasiltindakan."</td>";
		                 	}
		                 	foreach ($dataobatestetika as $rowobat) {
		                 		$jumlah =$rowobat['jumlah'];
		                 		$totalhargaobat = $jumlah * $rowobat['harga_per_kemasan'];
		                 		$bagihasilobat = 0.1 *($totalhargaobat);
		                 		echo "<td>".$rowobat['nama']."</td>";
		                 		echo "<td>".$jumlah."</td>";
		                 		echo "<td>".$totalhargaobat."</td>";
		                 		echo "<td>".$bagihasilobat."</td>";
		                 	}
		                 	$totalpendapatan = $hargatindakan + $totalhargaobat;
		                 	$totalbagihasil = $bagihasilobat +$bagihasiltindakan;
		                 	echo "<td>".$totalpendapatan."</td>";
		                 	echo "<td>".$totalbagihasil."</td>";
		                    $i++;
		                    $sumhargatindakan += $hargatindakan;
		                    $sumbagihasil += $bagihasiltindakan;
		                    $sumbanyakproduk += $jumlah;
		                    $sumhargaproduk += $totalhargaobat;
		                    $sumbagihasilprod += $bagihasilobat;
		                    $sumpendapatan += $totalpendapatan;
		                    $sumtotalbagihasil += $totalbagihasil;
		                    echo "</tr>";
	                   	}
	                   	?>
	                   	<tr>
	                   		<td colspan = '5'>TOTAL</td>
	                   		<td><?php echo $sumhargatindakan;?></td>
	                   		<td><?php echo $sumbagihasil;?></td>
	                   		<td></td>
	                   		<td><?php echo $sumbanyakproduk;?></td>
	                   		<td><?php echo $sumhargaproduk;?></td>
	                   		<td><?php echo $sumbagihasilprod;?></td>
	                   		<td><?php echo $sumpendapatan;?></td>
	                   		<td><?php echo $sumtotalbagihasil;?></td>
	                   	</tr>
	                </table>
	            </div>
	<br>
	<div id ='gridkasir'>
		<button class='button green' onClick="location.href='<?php echo base_url() . 'index.php/kasir/bulanan_estetika_xls'; ?>';">Unduh dalam format excel</button>		
		&nbsp;&nbsp;
		<!--
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/pdf_bulanan_estetika'; ?>'" class='button red'>Unduh sebagai pdf</button>
		&nbsp;&nbsp;
		<button onClick="location.href='<?php echo base_url() . 'index.php/kasir/print_estetika'; ?>'" class='button orange'>Lihat Detail Print</button>
		-->
	</div>
	<br>
</center>
<br>