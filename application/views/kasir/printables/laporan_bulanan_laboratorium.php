<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>LAPORAN PENDAPATAN LABORATORIUM<br>
		BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                            NO KUITANSI
	                        </td>
	                        <td>
	                            JENIS PEMERIKSAAN
	                        </td>
	                        <td>
	                            HARGA
	                        </td>
	                        <td>
	                            BAGI HASIL PKM
	                        </td>
	                        <td>
	                            DOKTER PENGIRIM
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
                    	$totalhargalab = 0;
                    	$totalbagihasil = 0;
	                    foreach ($laporanbulanan as $row) {
	                    	$hargatindakanlab = $row['tarif_umum'];
	                    	$bagihasilpkm = 0.3 * $hargatindakanlab;
	                    	$namadokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
                    		echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['no_kuitansi']."</td>";
		                    echo "<td>".$row['nama']."</td>";
		                    echo "<td>Rp. ".$hargatindakanlab."</td>";
		                    echo "<td>Rp. ".$bagihasilpkm."</td>";
		                    echo "<td>".$namadokter."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totalhargalab += $hargatindakanlab;
		                    $totalbagihasil += $bagihasilpkm;
		                }
	                   	?>
	                   	<tr>
		                    <td colspan = '5' style='text-align:center;'><h2>Jumlah:</h2></td>
		                   	<td><?php echo 'Rp. '.$totalhargalab ?></td>
		                   	<td><?php echo 'Rp. '.$totalbagihasil ?></td>
		                   	<td></td>
	                	</tr>
	                </table>
	            </div>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>