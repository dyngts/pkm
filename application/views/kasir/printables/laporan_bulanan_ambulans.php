<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>LAPORAN PENDAPATAN AMBULANS <br>
	PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK																			
<br> BULAN: <?php echo $bulan; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                        	JENIS PEMASUKKAN AMBULANS
	                        </td>
	                        <td>
	                            BIAYA
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalbiaya = 0;
	                    foreach ($laporanbulanan as $row) {
	                    	echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                 	echo "<td>".$row['nama']."</td>";
		                 	if ($row['jenis_pasien'] == '1'){
		                 		echo "<td> Rp. ".$row['tarif_mahasiswa']."</td>";
		                 		$totalbiaya += $row['tarif_mahasiswa'];
		                 	}
		                 	else if ($row['jenis_pasien'] == '2'){
		                 		echo "<td>Rp. ".$row['tarif_karyawan']."</td>";
		                 		$totalbiaya += $row['tarif_karyawan'];
		                 	}
		                 	else {
		                 		echo "<td> Rp. ".$row['tarif_umum']."</td>";
		                 		$totalbiaya += $row['tarif_umum'];
		                 	}

		                    $i++;
		                    echo "</tr>";
	                   	}
	                   	?>
	                   	<tr>
		                   	<td colspan='4'> TOTAL </td>
		                   	<td><?php echo 'Rp. '.$totalbiaya; ?></td>
	                   	</tr>
	                </table>
	            </div>
	<br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
<a href='..' id='kembali'>Kembali</a>
</footer>