<head>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
	</head>
<center>
	<header>
		<div id="banner">
			<img src='<?php echo base_url(); ?>images/banner.png'		
		</div>
	</header>
	
	<h4>LAPORAN PENDAPATAN POLI UMUM<br>
		PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) SALEMBA<br>
		BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h4>

	<div class="tabel_print" >
	                <table align = 'center'>
	                    <tr>
	                    	<td>
	                            Nomor
	                        </td>
	                        <td>
	                            Tanggal
	                        </td>
	                        <td>
	                            Nama 
	                            Pasien
	                        </td>
	                        <td>
	                            Jenis
	                            Kelamin
	                        </td>
	                        <td >
	                            Umur
	                        </td>
	                        <td>
	                            Diagnosa
	                        </td>
	                        <td>
	                            Tindakan
	                        </td>
	                         <td>
	                            Pendapatan 
	                            Poli Umum
	                        </td>
	                        <td>
	                            Dokter
	                            Pemeriksa
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $total5 = 0;
	                    
	                   foreach($laporan_salemba as $row) {
	                    	echo "<tr>";
	                    	echo "<td>"."$i"."</td>";
	                    	echo "<td>".$row['tanggal']."</td>";
	                    	echo "<td>".$row['nama_pasien']."</td>";
	                    	echo "<td>".$row['jenis_kelamin']."</td>";
	                    	echo "<td>".$row['umur']."</td>";
	                    	echo "<td>".$row['diagnosa']."</td>";
	                    	echo "<td>".$row['tindakan']."</td>";
	                    	echo "<td>".$row['pendapatan_poli_umum']."</td>";
	                    	echo "<td>".$row['dokter_pemeriksa']."</td>";
		                    $total5 = $total5 + $row['pendapatan_poli_umum'];

		                    //$totalpendapatanbulanan += $totalsemua;
	                    	$i++;
	                    }
	                   	?>
	                   	<tr>
	                    <td id='jumlahbulanan' colspan='7' bgcolor = '#F0F0F0'>Jumlah Pendapatan</td>
	                    
	                    <td bgcolor = '#F0F0F0'>Rp. <?php echo $total5;?></td>
	                    <!--<td>Rp. <?php echo $totalpendapatanbulanan;?></td>-->
	                    <td bgcolor = '#F0F0F0'></td>
	                    </tr>
	                </table>
	            </div>
	
	</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
<br>NIP 195210301979021001
</div>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>