<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>Laporan Pendapatan Poli Umum<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) Depok<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                            JENIS KELAMIN
	                        </td>
	                        <td>
	                            UMUR
	                        </td>
	                        <td>
	                            DIAGNOSA
	                        </td>
	                        <td>
	                            TINDAKAN
	                        </td>
	                        <td>
	                            PENDAPATAN POLI UMUM
	                        </td>
	                        <td>
	                            DOKTER PEMERIKSA
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalpendapatan = 0;
	                   	foreach ($laporanbulanan as $row) {
	                    	$umur = date("Y/m/d") - $row['tanggal_lahir'];
	                    	$tindakanumum = $this->kasirmod->getbiayatindakanumum($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
	                    	echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['jenis_kelamin']."</td>";
		                    echo "<td>".$umur."</td>";
		                    $datadiagnosa = $this->doktermod->getDataDiagnosa($row['id_rekam_medis'],$row['tanggal']);
		                    $konsultasi = $this->kasirmod->getKonsultasi($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
		                    $username_dokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
		                    $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
		                    $diagnosa = null;
		                    $coy = 1;
		                    $length = sizeof($datadiagnosa);
		                    foreach($datadiagnosa as $diagnosapasien) {	
		                    	if($coy == $length){
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'];
		                    	}
		                    	else {
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
		                    	}
		                    	$coy++;
		                    }
	                    	echo "<td>".$diagnosa."</td>";
		                    $j = 0;
		                    $tarifpoliumum = 0;
		                    $jasadokter = 0;

	                    	if($row['jenis_pasien'] == '1'){
								$tarifpoliumum += $row['tarif_mahasiswa'];
							}
							else if($row['jenis_pasien'] == '2'){
								$tarifpoliumum += $row['tarif_karyawan'];								
							}
							else {
								$tarifpoliumum += $row['tarif_umum'];									
							}   
		                   
		                    $jasatindakan = $tarifpoliumum - $jasadokter;
		                	$jepe = 0.2*($tarifpoliumum);
		                	$pendapatan = $tarifpoliumum - $jepe;
		                    echo "<td>".$row['nama']."</td>";
		                    if($konsultasi != null) {
		                    	$jasadokter = 15000;
		                    }
		                    echo "<td>".$pendapatan."</td>";
		                    echo "<td>".$namadokter."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totalpendapatan += $pendapatan;
	                   	}
	                   	?>
	                   	<tr>
	                   		<td colspan='7'>JUMLAH</td>
	                   		<td><?php echo $totalpendapatan;?></td>
	                   		<td></td>
	                   	</tr>
	                </table>
	            </div>
	<br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>