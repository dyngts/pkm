<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>Laporan Pendapatan Ruang Farmasi<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI) Depok<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                            KARYAWAN
	                        </td>
	                        <td>
	                            UMUM
	                        </td>
	                        <td>
	                            DOKTER PEMERIKSA
	                        </td>
	                        <td>
	                            BIAYA KARYAWAN
	                        </td>
	                        <td>
	                            BIAYA UMUM
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $totalhargakaryawan = 0;
                    	$totalhargaumum = 0;
	                    foreach ($laporanbulanan as $row) {
	                    		$usernamedokter = $this->kasirmod->getUsernameDokter($row['tanggal'],$row['id_rekam_medis'], $row['waktu_tagih']);
	                    		$namadokter = $this->kasirmod->getNamebyUsername($usernamedokter);
	                    		echo "<tr>";
			                    echo "<td>"."$i"."</td>";
			                    echo "<td>".$row['tanggal']."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    $hargaobatkaryawan = 2500;
			                   	if ($row['jenis_pasien'] == '2'){
			                   		 echo "<td>YA</td>";
			                   		 echo "<td>BUKAN</td>";
			                   		 echo "<td>".$namadokter."</td>";
			                   		 echo "<td>".$hargaobatkaryawan."</td>";
			                   		 echo "<td> 0 </td>";
			                   		 $totalhargakaryawan += $hargaobatkaryawan;
			                   	}
			                   	else if ($row['jenis_pasien'] == '3'){
			                   		$hargaobatkaryawan = 0;
			                   		echo "<td>BUKAN</td>";
			                   		echo "<td>YA</td>";
			                   		echo "<td>".$namadokter."</td>";
			                   		echo "<td>".$hargaobatkaryawan."</td>";
			                   		$data1 = $this->kasirmod->gethargaobat($row['id_rekam_medis'],$row['tanggal'], $row['waktu_tagih']);
			                   		$biayaobat = $data1->result_array();
			                   		$hargaobat = 0;
			                   		foreach ($biayaobat as $bobat) {
			                   			$jumlah = $bobat['jumlah'];
			                   			$hargaperkemasan = $bobat['harga_per_kemasan'];
			                   			$hargaobat += $jumlah * $hargaperkemasan;
			                   			$totalhargaumum += $hargaobat;
			                   		}
			                   		echo "<td>".$hargaobat."</td>";

			                   	}
			                    $i++;
			                    echo "</tr>";
	                   	}
	                   	?>
	                   	<tr>
		                    <td colspan='6' style='text-align:center;'><h2>Jumlah:</h2></td>
		                	<td><?php echo $totalhargakaryawan;?></td>
		                	<td><?php echo $totalhargaumum;?></td>
	                	</tr>
	                </table>
	            </div>
	<br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>