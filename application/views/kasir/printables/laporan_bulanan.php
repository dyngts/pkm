<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>Laporan Bulanan, Bulan <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NOMOR
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            PENDAPATAN POLI UMUM
	                        </td>
	                        <td>
	                            BIAYA DENTAL LAB
	                        </td>
	                       <td>
	                            PENDAPATAN POLI GIGI
	                        </td>
	                        <td>
	                            PENDAPATAN RUANG FARMASI KARYAWAN
	                        </td>
	                        <td>
	                            PENDAPATAN RUANG FARMASI UMUM
	                        </td>
	                         <td>
	                            RADIOLOGI
	                        </td>
	                         <td>
	                            AMBULANS
	                        </td>
	                        <td>
	                            SALDO AKHIR
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                    $total1 = 0;
	                    $total2 = 0;
	                    $total3 = 0;
	                    $total4 = 0;
	                    $total5 = 0;
	                    $total6 = 0;
	                    $total7 = 0;
	                    $total8 = 0;
	                    $total9 = 0;
	                    $total10 = 0;
	                    $total11 = 0;
	                    $total12 = 0;
	                    $total13 = 0;
	                    $total14 = 0;
	                    $totalpendapatanbulanan = 0;
	                    foreach($laporanbulanan as $row) {
	                    	echo "<tr>";
	                    	echo "<td>"."$i"."</td>";
	                    	echo "<td>".$row['tanggal']."</td>";
	                    	$biayapoliumum = $this->kasirmod->getPoliUmumHarian($row['tanggal']);
	                    	$biayapoligigi = $this->kasirmod->getPoliGigiHarian($row['tanggal']);
	                    	$biayafarmasi = $this->kasirmod->getFarmasiHarian($row['tanggal']); 
	                    	$biayaradiologi = $this->kasirmod->getRadiologiHarian($row['tanggal']); 
	                    	$biayaambulans = $this->kasirmod->getAmbulansHarian($row['tanggal']); 

	                    	$poliumum = 0;

	                    	$poligigi = 0;
	                    	$biayadental = 0;

	                    	$obatkaryawan = 0;
	                    	$obatumum = 0;

	                    	$radiologi = 0;
	                    	$ambulans = 0;

	                    	foreach($biayapoliumum as $bumum) {  
		                    	if($bumum['jenis_pasien'] == '1'){
									$poliumum += $bumum['tarif_mahasiswa'];
								}
								else if($bumum['jenis_pasien'] == '2'){
									$poliumum += $bumum['tarif_karyawan'];								
								}
								else {
									$poliumum += $bumum['tarif_umum'];									
								}      	
		                    } 
		                    $pendapatanpoliumum = 0.8*($poliumum);

		                    foreach($biayapoligigi as $bgigi) {  
		                    	if($bgigi['jenis_pasien'] == '1'){
									$poligigi += $bgigi['tarif_mahasiswa'];
								}
								else if($bgigi['jenis_pasien'] == '2'){
									$poligigi += $bgigi['tarif_karyawan'];								
								}
								else {
									$poligigi += $bgigi['tarif_umum'];									
								}      	
		                    } 


	                    	$poligiginetto = $poligigi-$biayadental;
	                    	$pendapatanpoligigi = 0.8*($poligiginetto);

		                    foreach($biayafarmasi as $bfarm) {  
								if($bfarm['jenis_pasien'] == '2'){
									$obatkaryawan += 2500;							
								}
								else if($bfarm['jenis_pasien'] == '3') {
									$hargaobat = $bfarm['harga_per_kemasan']*$bfarm['jumlah'];
									$obatumum +=$hargaobat;						
								}      	
		                    } 

		                    foreach($biayaradiologi as $bradio) {
		                    	if($bradio['jenis_pasien'] == '1'){
									$radiologi += $bradio['tarif_mahasiswa'];
								}
								else if($bgigi['jenis_pasien'] == '2'){
									$radiologi += $bradio['tarif_karyawan'];								
								}
								else {
									$radiologi += $bradio['tarif_umum'];									
								}
		                    }

		                     foreach($biayaambulans as $bambu) {
		                    	if($bambu['jenis_pasien'] == '1'){
									$ambulans += $bambu['tarif_mahasiswa'];
								}
								else if($bambu['jenis_pasien'] == '2'){
									$ambulans += $bambu['tarif_karyawan'];								
								}
								else {
									$ambulans += $bambu['tarif_umum'];									
								}
		                    }

		                    $saldoakhir = $pendapatanpoliumum+$poligiginetto+$obatumum+$obatkaryawan+$radiologi+$ambulans;
		                    $totalgigiumum = $pendapatanpoliumum+$pendapatanpoligigi;
		                    $pendapatanPKM = 0.2*($poliumum)+0.2*($poligiginetto);
		                    $totalsemua = $poliumum+$poligigi+$obatumum+$obatkaryawan+$radiologi+$ambulans;
		                    $pendapatanUI = 0.8*($totalsemua);
		                    echo "<td>"."$pendapatanpoliumum"."</td>"; 
		                    echo "<td>"."$biayadental"."</td>"; 
		                    echo "<td>"."$pendapatanpoligigi"."</td>"; 
		                    echo "<td>"."$obatkaryawan"."</td>"; 
		                    echo "<td>"."$obatumum"."</td>"; 
		                    echo "<td>"."$radiologi"."</td>"; 
		                    echo "<td>"."$ambulans"."</td>"; 
		                    echo "<td>"."$saldoakhir"."</td>"; 
		                    $total2 += $pendapatanpoliumum;
		                    $total4 += $biayadental;
		                    $total6 += $pendapatanpoligigi;
		                    $total7 += $obatkaryawan;
		                    $total8 += $obatumum;
		                    $total9 += $radiologi;
		                    $total10 += $ambulans;
		                    $total11 += $saldoakhir;
	                    	$i++;
	                    }
	                   	?>
	                   	<tr>
	                    <td id='jumlahbulanan' colspan='2'>Jumlah Pendapatan</td>
	                    <td><?php echo $total2;?></td>
	                    <td><?php echo $total4;?></td>
	                    <td><?php echo $total6;?></td>
	                    <td><?php echo $total7;?></td>
	                    <td><?php echo $total8;?></td>
	                    <td><?php echo $total9;?></td>
	                    <td><?php echo $total10;?></td>
	                    <td><?php echo $total11;?></td>
	                    </tr>
	                </table>
	            </div>
	<br>
	</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>