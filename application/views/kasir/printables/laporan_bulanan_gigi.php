<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>Laporan Pendapatan Poli Gigi<br>
		Pusat Kesehatan Mahasiswa Universitas Indonesia (PKM UI)<br> Bulan: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>
	<br>
	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                            JENIS KELAMIN
	                        </td>
	                        <td>
	                            UMUR
	                        </td>
	                        <td>
	                            DIAGNOSA
	                        </td>
	                        <td>
	                            TINDAKAN
	                        </td>
	                        <td>
	                            BIAYA DENTAL LAB
	                        </td>
	                        <td>
	                            PENDAPATAN POLI GIGI
	                        </td>
	                        <td>
	                            NAMA DOKTER
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
	                   	foreach ($laporanbulanan as $row) {
	                    	$umur = date("Y/m/d") - $row['tanggal_lahir'];
	                    	$tindakangigi = $this->kasirmod->getbiayatindakangigi($row['id_rekam_medis'],$row['tanggal'],$row['waktu_tagih']);
	                    	
		                    $datadiagnosa = $this->doktermod->getDataDiagnosa($row['id_rekam_medis'],$row['tanggal']);
		                    $username_dokter = $this->kasirmod->getNamaDokter($row['id_rekam_medis'],$row['tanggal']);
		                    $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
		                    $diagnosa = null;
		                    $coy = 1;
		                    $length = sizeof($datadiagnosa);
		                    foreach($datadiagnosa as $diagnosapasien) {	
		                    	if($coy == $length){
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'];
		                    	}
		                    	else {
		                    		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
		                    	}
		                    	$coy++;
		                    }
	                    	
		                    $tarifpoligigi = 0;
		               
		              
	                    	if($row['jenis_pasien'] == '1'){
								$tarifpoligigi += $row['tarif_mahasiswa'];
							}
							else if($row['jenis_pasien'] == '2'){
								$tarifpoligigi += $row['tarif_karyawan'];								
							}
							else {
								$tarifpoligigi += $row['tarif_umum'];									
							}      	
		                   
		                    $biayadental = 0;
		                	$jepe = 0.2*($tarifpoligigi);
		                	$pendapatan = $tarifpoligigi - $jepe;
		                	$netto = $tarifpoligigi - $biayadental;
		                    if($tarifpoligigi != 0) {
		                    	echo "<tr>";
			                    echo "<td>"."$i"."</td>";
			                    echo "<td>".$row['tanggal']."</td>";
			                    echo "<td>".$row['nama_pasien']."</td>";
			                    echo "<td>".$row['jenis_kelamin']."</td>";
			                    echo "<td>".$umur."</td>";
		                    	echo "<td>".$diagnosa."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    echo "<td>".$biayadental."</td>";
			                    echo "<td>".$pendapatan."</td>";
			                    echo "<td>".$namadokter."</td>";
			                    $i++;
			                    echo "</tr>";
			                }
	                   	}
	                   	?>
	                </table>
	            </div>
	<br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>