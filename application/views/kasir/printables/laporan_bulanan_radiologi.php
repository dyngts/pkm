<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>LAPORAN PENDAPATAN RADIOLOGI<br>PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA ( PKM UI ) DEPOK<br>
		BULAN: <?php echo $bulan; ?>&nbsp;<?php echo $tahun; ?></h2>

	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            NO
	                        </td>
	                        <td>
	                            TANGGAL
	                        </td>
	                        <td>
	                            NAMA PASIEN
	                        </td>
	                        <td>
	                            JENIS PEMERIKSAAN RADIOLOGI
	                        </td>
	                        <td>
	                            JUMLAH PEMERIKSAAN RADIOLOGI
	                        </td>
	                        <td>
	                            BIAYA
	                        </td>
	                    </tr>
	                   <?php 
	                    $i=1;
                    	$totalbiayaradiologi = 0;
	                    foreach ($laporanbulanan as $row) {
        	               
	                    	$biayaradiologi = $row['tarif_umum'];
	                    	if ($biayaradiologi!=null){
	                    		$jumlahtindakanradio = 1;
	                    	}
	                    	else $jumlahtindakanradio = 0;
                    		echo "<tr>";
		                    echo "<td>"."$i"."</td>";
		                    echo "<td>".$row['tanggal']."</td>";
		                    echo "<td>".$row['nama_pasien']."</td>";
		                    echo "<td>".$row['nama']."</td>";
		                    echo "<td>".$jumlahtindakanradio."</td>";
		                    echo "<td>Rp. ".$biayaradiologi."</td>";
		                    $i++;
		                    echo "</tr>";
		                    $totalbiayaradiologi += $biayaradiologi;
		                }
	                   	?>
	                   	<tr>
		                    <td colspan = '5' style='text-align:center;'><h2>Jumlah:</h2></td>
		                   	<td><?php echo 'Rp. '.$totalbiayaradiologi ?></td>
	                	</tr>
	                </table>
	            </div>
	<br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>