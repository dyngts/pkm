<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<table id='kuitansi' align = 'center'>	
			<h2>Kuitansi Nomor <?php echo $nokuitansi+1;?></h2>	
			<h2>Tanggal <?php echo date('d-m-Y');?></h2>	
			<tr><td>Nama Pasien:</td><td><?php echo $datapasien->nama?></td></tr>
			<tr><td>Kategori:</td><td>
				<?php 
				if($datapasien->jenis_pasien == '1')echo 'Mahasiswa'; else if($datapasien->jenis_pasien == '2') echo 'Karyawan'; else echo'Umum';
				?></td></tr>
					
			<tr><td>Umur:</td><td><?php echo(date("Y/m/d")-$datapasien->tanggal_lahir).' tahun';?></td></tr>
			<tr><td>Alamat:</td><td><?php echo $datapasien->alamat;?></td></tr>
			<?php 
				$tarifpoliumum = 0;
				$tarifpoligigi = 0;
				$tarifambulans = 0;
				$tarifradiologi = 0;
				$tariflaboratorium = 0;
				$tarifestetika = 0;
				$biayadental = 0;
				$obat = 0;
				foreach ($biayatindakan as $rowtindakan) {
					if($datapasien->jenis_pasien == '1'){
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_mahasiswa'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_mahasiswa'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_mahasiswa'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_mahasiswa'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_mahasiswa'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_mahasiswa'];
						}
					}
					else if($datapasien->jenis_pasien == '2'){
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_karyawan'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_karyawan'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_karyawan'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_karyawan'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_karyawan'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_karyawan'];
						}
					}
					else {
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_umum'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_umum'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_umum'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_umum'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_umum'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_umum'];
						}
					}
				}
				foreach($biayaobat as $rowobat){
					if($datapasien->jenis_pasien == '1'){
						$obat=0;
					}
					else if($datapasien->jenis_pasien == '2'){
						$obat = 2500;
					}
					else {
						$obat += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
					}
				}
			?>
			<tr><td>Biaya Tindakan:</td><td>
				<?php 
				$totaltindakan = $tarifpoliumum+$tarifpoligigi+$tariflaboratorium+$tarifambulans+$tarifradiologi+$tarifestetika;
				echo 'Rp .'.$totaltindakan;
				?></td></tr>
				
			<tr><td>Obat:</td><td>
				<?php 

					echo 'Rp .'.$obat;
				?></td></tr>
			
			<tr><td>Jasa Dokter Umum/Jantung:</td><td>
				<?php 
				$jasa = 0.2*($totaltindakan);
				echo 'Rp .'.$jasa;
				?></td></tr>
			
			<!--Biaya Laboratorium/Rontgen:-->
			<tr><td>Jumlah Pembayaran:</td><td><?php $total = $obat+$totaltindakan+$jasa; echo 'Rp .'.($total);?></td></tr>
			<br>
		</table>	
	<br>

<div class = 'sigcontainer'>
	<div class = 'signature'>
	<p align = 'center'><?php echo $tanggal;?>
	<br>
	Dokter 
	<?php 
		$rowrekmed = $this->kasirmod->searchrekmed($idrekam, $tanggal);
		$namadokter = $this->kasirmod->getNamebyUsername($rowrekmed->username_dokter);
		echo $namadokter;
	?></p>
	</div>
</div>
</center>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
