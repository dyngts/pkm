<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/print.css" />
</head>
	<body>
	<header>
		<div id="banner" style = 'border-bottom: 1px solid #888888;'>
			<img src='<?php echo base_url(); ?>image/banner.png'/>	
		</div>

	</header>
<center>	
	<h2>
	Pendapatan Harian PKM UI Depok <br>Tanggal <?php echo $tanggal; ?>
	</h2>
	<br>
	<div class="tabel_print" >
	                <table >
	                    <tr>
	                    	<td>
	                            Nomor
	                        </td>
	                        <td>
	                            Nama Dokter
	                        </td>
	                        <td>
	                            Nama Pasien
	                        </td>
	                        <td>
	                            No. Kuitansi
	                        </td>
	                        <td >
	                            Fakultas
	                        </td>
	                        <td>
	                            Pendapatan Poli Umum
	                        </td>
	                        <td>
	                            Biaya Dental Lab
	                        </td>
	                         <td>
	                            Pendapatan Poli Gigi
	                        </td>
	                        <td>
	                            Pendapatan Ruang Farmasi Karyawan
	                        </td>
	                        <td>
	                           	Pendapatan Ruang Farmasi Umum
	                        </td>
	                         <td>
	                            Radiologi
	                        </td>
	                         <td>
	                            Ambulans
	                        </td>
	                    </tr>
	                    <?php 
	                    $i=1;
	                    $totalpoliumum = 0;
	                    $totalbiayajpumum = 0;
	                    $totalnettoumum =0;
	                    $totalpoligigi = 0;
	                    $totalbiayadental = 0;
	                    $totalnettogigi = 0;
	                    $totalbiayajpgigi =0;
	                    $totalpendapatangigi = 0;
	                    $totalobatkaryawan = 0;
	                    $totalobatumum = 0;
	                    $totalradiologi = 0;
	                    $totalambulans = 0;
	                    foreach ($laporanharian as $row) {
	                    	$usernamedokter = $this->kasirmod->getUsernameDokter($tanggal,$row['id_rekam_medis'], $row['waktu_tagih']);
	                    	$namadokter = $this->kasirmod->getNamebyUsername($usernamedokter);
                    		$query2 = $this->kasirmod->getbiayatindakan($row['id_rekam_medis'], $tanggal, $row['waktu_tagih']);
							$query3 =  $this->kasirmod->gethargaobat($row['id_rekam_medis'], $tanggal, $row['waktu_tagih']);
							$array2 = $query2->result_array();
							$array3 = $query3->result_array();
							$tarifpoliumum = 0;
							$tarifpoligigi = 0;
							$biayadental = 0;
							$obatkaryawan = 0;
							$obatumum = 0;
							$biayaradiologi = 0;
							$biayaambulans = 0;
							
							foreach ($array2 as $rowtindakan) {
								if($row['jenis_pasien'] == '1'){
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan['tarif_mahasiswa'];
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_mahasiswa'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_mahasiswa'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_mahasiswa'];
									}
								}
								else if($row['jenis_pasien'] == '2'){
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan->tarif_karyawan;
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_karyawan'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_karyawan'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_karyawan'];
									}
								}
								else {
									$checkumum = array_search('Poli Umum', $rowtindakan);
									$checklab = array_search('Laboratorium', $rowtindakan);
									$checkest = array_search('Estetika Medis', $rowtindakan);
									$checkradio = array_search('Radiologi', $rowtindakan);
									$checkambulans = array_search('Ambulans', $rowtindakan);

									if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
										$tarifpoliumum += $rowtindakan['tarif_umum'];
									}
									$checkgigi = array_search('Poli Gigi', $rowtindakan);
									if($checkgigi !=null){
										$tarifpoligigi += $rowtindakan['tarif_umum'];
									}
									if($checkradio !=null){
										$biayaradiologi += $rowtindakan['tarif_umum'];
									}
									if($checkambulans !=null){
										$biayaambulans += $rowtindakan['tarif_umum'];
									}
								}
							}

							foreach($array3 as $rowobat){
								if($row['jenis_pasien'] == '1'){
									$obatkaryawan = 0;
									$obatumum = 0;
								}
								else if($row['jenis_pasien'] == '2'){
									$obatkaryawan = 2500;
								}
								else {
									$obatumum += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
								}
							}
							
							$biayajp = 0.2*($tarifpoliumum);
							$biayajpgigi = 0.2*($tarifpoligigi);
							$nettotindakanumum = $tarifpoliumum-$biayajp;
							$nettogigi = $tarifpoligigi-$biayadental;
							$pendapatangigi = $nettogigi-$biayajpgigi;
							if($tarifpoliumum != 0 || $tarifpoligigi != 0 || $obatkaryawan != 0 || $obatumum != 0 || $biayaradiologi != 0 || $biayaambulans != 0 ){
								echo "<tr>";
			                    echo "<td>"."$i"."</td>";
			                    echo "<td>".$namadokter."</td>";
			                    echo "<td>".$row['nama']."</td>";
			                    echo "<td>".$row['no_kuitansi']."</td>";
			                    echo "<td>".$row['lembaga_fakultas']."</td>";
			                    echo "<td>Rp. ".$nettotindakanumum."</td>";
			                    echo "<td>Rp. ".$biayadental."</td>";
			                    echo "<td>Rp. ".$pendapatangigi."</td>";
			                    echo "<td>Rp. ".$obatkaryawan."</td>";
			                    echo "<td>Rp. ".$obatumum."</td>";
			                    echo "<td>0</td>";
			                    echo "<td>0</td>";
			                }
		                    $i++;
		                    echo "</tr>";
		                    $totalpoliumum += $tarifpoliumum;
		                    $totalbiayajpumum += $biayajp;
		                    $totalnettoumum +=$nettotindakanumum;
		                    $totalpoligigi += $tarifpoligigi;
		                    $totalbiayadental += $biayadental;
		                    $totalnettogigi += $nettogigi;
		                    $totalbiayajpgigi +=$biayajpgigi;
		                    $totalpendapatangigi += $pendapatangigi;
		                    $totalobatkaryawan += $obatkaryawan;
		                    $totalobatumum += $obatumum;
		                    $totalradiologi += $biayaradiologi;
		                    $totalambulans += $biayaambulans;
	                   	}
	                    ?>
	                    <tr>
		                    <td colspan='5' style='text-align:center;'><strong>Jumlah:</strong></td>
		                	<td><?php echo 'Rp. '.$totalnettoumum;?></td>
		                	<td><?php echo 'Rp. '.$totalbiayadental;?></td>
		                	<td><?php echo 'Rp. '.$totalpendapatangigi;?></td>
		                	<td><?php echo 'Rp. '.$totalobatkaryawan;?></td>
		                	<td><?php echo 'Rp. '.$totalobatumum;?></td>
		                	<td><?php echo 'Rp. '.$totalradiologi;?></td>
		                	<td><?php echo 'Rp. '.$totalambulans;?></td>
	                	</tr>

	                	<tr>
	                		<td colspan = '11' style='text-align:center;'><strong>Jumlah Pendapatan</strong> </td>
	                		<td><?php echo 'Rp. '.$totalobatumum+$totalnettoumum+$totalbiayadental+$totalpendapatangigi+$totalobatumum+$totalobatkaryawan;?></td>

	                	</tr>
	                </table>
	                
	            </div>
	            <br>
</center>
<br>
<div class = 'signature'>
Depok,  <br>
Wakil Direktur Umum dan Fasilitas
<br><br><br><br><br><br>
DR. drg. Harun A Gunawan, MS PAK
NIP 195210301979021001
</div>
<br>
<footer>
<a href='' onClick="window.print();return false" id='printhalaman'>Print</a>
&nbsp;&nbsp;
<a href='..' id='kembali'>Kembali</a>
</footer>
</body>
</html>