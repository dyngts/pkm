<script type="text/javascript">

	function showDiv() {
	  var target = "#hidden-rincian1";                                                                                 
	  $(target).css('display','block');
	}

	function hideDiv() {
		var target = "#hidden-rincian1";                                                                                 
	  	$(target).css('display','none');
	}

	function showDiv2() {
	  var target = "#hidden-rincian2";                                                                                 
	  $(target).css('display','block');
	}

	function hideDiv2() {
		var target = "#hidden-rincian2";                                                                                 
	  	$(target).css('display','none');
	}

	function confirmPost() {
		var agree=confirm("Apakah anda yakin ingin menyimpan tagihan ini?");
		if (agree)
		return true ;
		else
		return false ;
	}	

</script>
<center>
<div id='rincian'>
	<div id = 'rinciancontent'>
		<ul id='rekammedis'>
			<h2>Kuitansi Nomor <?php echo $nokuitansi+1;?></h2>
			<h2>Tanggal <?php echo date('d-m-Y');?></h2>
			<li><strong><label>Nama Pasien:</label></strong><?php echo $datapasien->nama?></li>
			<li><strong><label>Kategori:</label></strong>
				<?php 
				if($datapasien->jenis_pasien == '1')echo 'Mahasiswa'; else if($datapasien->jenis_pasien == '2') echo 'Karyawan'; else echo'Masyarakat Umum';
				?>
			</li>		
			<li><strong><label>Umur:</label></strong><?php echo(date("Y/m/d")-$datapasien->tanggal_lahir).' tahun';?></li>	
			<li><strong><label>Alamat:</label></strong><?php echo $datapasien->alamat;?></li>
			<?php 
				$tarifpoliumum = 0;
				$tarifpoligigi = 0;
				$tarifambulans = 0;
				$tarifradiologi = 0;
				$tariflaboratorium = 0;
				$tarifestetika = 0;
				$biayadental = 0;
				$obat = 0;
				foreach ($biayatindakan as $rowtindakan) {
					if($datapasien->jenis_pasien == '1'){
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_mahasiswa'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_mahasiswa'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_mahasiswa'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_mahasiswa'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_mahasiswa'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_mahasiswa'];
						}
					}
					else if($datapasien->jenis_pasien == '2'){
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_karyawan'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_karyawan'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_karyawan'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_karyawan'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_karyawan'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_karyawan'];
						}
					}
					else {
						$checkumum = array_search('Poli Umum', $rowtindakan);
						if($checkumum!=null) {
							$tarifpoliumum += $rowtindakan['tarif_umum'];
						}
						$checkgigi = array_search('Poli Gigi', $rowtindakan);
						if($checkgigi !=null){
							$tarifpoligigi += $rowtindakan['tarif_umum'];
						}
						$checkambulans = array_search('Ambulans', $rowtindakan);
						if($checkambulans !=null){
							$tarifambulans += $rowtindakan['tarif_umum'];
						}
						$checkradiologi = array_search('Radiologi', $rowtindakan);
						if($checkradiologi !=null){
							$tarifradiologi += $rowtindakan['tarif_umum'];
						}
						$checklaboratorium = array_search('Laboratorium', $rowtindakan);
						if($checklaboratorium !=null){
							$tariflaboratorium += $rowtindakan['tarif_umum'];
						}
						$checkestetika = array_search('Estetika Medis', $rowtindakan);
						if($checkestetika !=null){
							$tarifestetika += $rowtindakan['tarif_umum'];
						}
					}
				}
				foreach($biayaobat as $rowobat){
					if($datapasien->jenis_pasien == '1'){
						$obat=0;
					}
					else if($datapasien->jenis_pasien == '2'){
						$obat = 2500;
					}
					else {
						$obat += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
					}
					$idresep = $rowobat['id_resep'];
				}
			?>
			<li><strong><label>Biaya Tindakan:</label></strong>
				<?php 
				$totaltindakan = $tarifpoliumum+$tarifpoligigi+$tariflaboratorium+$tarifambulans+$tarifradiologi+$tarifestetika;
				echo 'Rp .'.$totaltindakan;
				?>
				&nbsp;&nbsp;
				<button class="button green table_button" onclick="showDiv()">Lihat Rincian</button>
				&nbsp;&nbsp;
				<button class="button orange table_button" onclick="hideDiv()">Tutup Rincian</button>
			</li>	
			<li id="hidden-rincian1" style="display:none; background-color:#eaeaea;">
				<?php 
					echo "Tarif Poli Umum = ".$tarifpoliumum; echo "<br>";echo "<br>";
					echo "Tarif Poli Gigi = ".$tarifpoligigi; echo "<br>";echo "<br>";
					echo "Tarif Poli Estetika = ".$tarifestetika;echo "<br>";echo "<br>";
					echo "Tarif Radiologi = ".$tarifradiologi;echo "<br>";echo "<br>";
					echo "Tarif Laboratorium = ".$tariflaboratorium;echo "<br>";echo "<br>";
					echo "Tarif Ambulans = ".$tarifambulans;
				?>
			</li>
			<li><strong><label>Obat:</label></strong>
				<?php 

					echo 'Rp .'.$obat;
				?>
				&nbsp;&nbsp;
				<button class="button green table_button" onclick="showDiv2()">Lihat Rincian</button>
				&nbsp;&nbsp;
				<button class="button orange table_button" onclick="hideDiv2()">Tutup Rincian</button>
			</li>
			<li id="hidden-rincian2" style="display:none; background-color:#eaeaea;">
				<?php 
					foreach($biayaobat as $rowobat){
						if($datapasien->jenis_pasien == '1'){
							$obat=0;
						}
						else if($datapasien->jenis_pasien == '2'){
							$obat = 2500;
						}
						else {
							$obat += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
						}
						$idresep = $rowobat['id_resep'];
						echo "Nama Obat = ".$rowobat['nama']; echo "<br>";echo "<br>";
						echo "Harga Obat per Kemasan = ".$rowobat['harga_per_kemasan']; echo "<br>";echo "<br>";
						echo "Jumlah = ".$rowobat['jumlah']; echo "<br>";echo "<br>";
					}

				?>
			</li>
			<li><strong><label>Jasa Dokter Umum/Jantung:</label></strong>
				<?php 
				$jasa = 0.2*($totaltindakan);
				echo 'Rp .'.$jasa;
				?>
			</li>

			<!--<li><label>Biaya Laboratorium/Rontgen:</label></li>-->
			<li><strong><label>Jumlah Pembayaran:</label></strong><?php $total = $obat+$totaltindakan+$jasa; echo 'Rp .'.($total);?></li>
			<br>
		</ul>	
	</div>
	<br>
	<div id ='cetakkuitansi'>
		<?php 
		$attribute = array('onsubmit'=>"return confirmPost()");
		echo form_open('kasir/konfirmasi_bayar',$attribute);?>
			<input type='hidden' name="waktu" value="<?php echo $waktu;?>" />
			<input type='hidden' name="idresep" value="<?php echo $idresep;?>" />
			<input type='hidden' name="tanggal" value="<?php echo $tanggal;?>" />
        	<input type='hidden' name="idrekam" value="<?php echo $idrekam;?>" />
			<input type='hidden' name="idpasien" value="<?php echo $datapasien->id;?>" />
			<input type='hidden' name="status" value="<?php echo TRUE;?>" />
			<input type='hidden' name="bayar" value="<?php echo $total;?>" />
			<button type='submit' class='button orange'> Simpan ke pembayaran </button>
	 	<?php echo form_close();?>
		<br>
	 	<button class='button green' onclick="location.href='<?php echo base_url() . 'index.php/kasir/print_kuitansi'; ?>';">Lihat Detail Print</button>
	 	<button class='button red' onclick="location.href='<?php echo base_url() . 'index.php/kasir/kuitansi'; ?>';">Unduh Kuitansi</button>
	 	<br>
	</div>
	<br>
</div>
</center>

