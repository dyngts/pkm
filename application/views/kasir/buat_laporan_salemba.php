<center>
	<h2> Laporan Keuangan Harian PKM UI Salemba </h2>
</center>
<script>
function confirmPost() {
	var agree=confirm("Apakah anda yakin ingin menyimpan data ini ?");
	if (agree)
	return true ;
	else
	return false ;
}	
</script>
	<div id = 'formkeuangan'>
	<?php 
		#$attribute = array('onsubmit'=>"return confirmPost()");
		echo validation_errors();
		echo form_open("kasir/masukkan_datakeuangan"); 
		?>
	<ul id = 'formsalemba'>
		<div class="row"><div class="label"><label><b style="color:red;">*Harus diisi</b></label></div></div>
		<br><br>
		<div class="row"><div class="label"><label>Tanggal(DD-MM-YYYY)<b style="color:red;">*</b> </label></div><div class="input">
        <p class="p_form">:</p> <input type="text" id="datepicker" name='tanggal' value='<?php echo date("d/m/Y");?>'/> </div></div>
		<div class="row"><div class="label"><label>Nama Pasien <b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="namaPasien" value="<?php echo set_value('namaPasien'); ?>" /></div></div>
		<div class="row"><div class="label"><label>Jenis Kelamin <b style="color:red;">*</b></label></div> <p class="p_form">:</p><div class="input"><select id = 'jenis' name='jenisKelamin' value="<?php echo set_value('jenisKelamin'); ?>">
			<option value ="L">Laki-Laki</option>
			<option value ="P">Perempuan</option>
		</select></div></div>
		<div class="row"><div class="label"><label>Umur<b style="color:red;">*</b></label></div><div class="input">
                <p class="p_form">:</p>  <input type="text" name="umur" value="<?php echo set_value('umur'); ?>"/></div>
		<div class="row"><div class="label"><label>Diagnosa <b style="color:red;">*</b></label></div><div class="input">
                <p class="p_form">:</p> <textarea rows="5" name="diagnosa"><?php echo set_value('diagnosa'); ?></textarea></div></div>
		<div class="row"><div class="label"><label>Tindakan <b style="color:red;">*</b></label></div><div class="input">
                <p class="p_form">:</p> <textarea rows="5" name="tindakan"><?php echo set_value('tindakan'); ?></textarea></div></div>
		<div class="row"><div class="label"><label>Jasa Dokter<b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="jasaDokter" value="<?php echo set_value('jasaDokter'); ?>"/></div></div>
		<div class="row"><div class="label"><label>Jasa Tindakan<b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="jasaTindakan" value="<?php echo set_value('jasaTindakan'); ?>" /></div></div>
		<div class="row"><div class="label"><label>Poli Umum Netto<b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="poliUmumNetto" value="<?php echo set_value('poliUmumNetto'); ?>" /></div></div>
		<div class="row"><div class="label"><label>Poli Umum JP PKM<b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="poliUmumJP" value="<?php echo set_value('poliUmumJP'); ?>" /></div></div>
		<div class="row"><div class="label"><label>Pendapatan Poli Umum<b style="color:red;">*</b></label></div> <div class="input">
                <p class="p_form">:</p> <input type="text" name="pendapatanPoliUmum" value="<?php echo set_value('pendapatanPoliUmum'); ?>" /></div></div>
		<div class="row"><div class="label"><label>Dokter Pemeriksa <b style="color:red;">*</b></label></div><div class="input">
                <p class="p_form">:</p> <input type="text" name="dokterPemeriksa" value="<?php echo set_value('dokterPemeriksa'); ?>" maxlength="50"/></div></div>

	<br>
	<center><input type="submit" value="Submit" onClick = "return confirmPost()" id='pasiensub' class='button orange'></center>
	</ul>

	<?php echo form_close(); ?>
	</div>

