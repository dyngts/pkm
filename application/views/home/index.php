<html>
	<head>
		<title>SIPOTEK | Login</title>
		 <link href="<?php echo site_url('css/foundation.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/foundation.min.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/normalize.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/bootstrap-responsive.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/boostrap-responsive.min.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/bootstrap.css') ?>" rel="stylesheet"  />
        <link href="<?php echo site_url('css/bootstrap.min.css') ?>" rel="stylesheet"  />
      	 <link href="<?php echo site_url('css/style.css') ?>" rel="stylesheet"  />

	</head>
	<body class="sipotek-index">
            <div id="wrap">
		<div class="fixed">
			<nav class="top-bar">
				<ul class="title-area">
					<!-- Title Area -->
					<!--    <li class="name">
					<a href="#"><img src="img/logo.png" alt="SIPOTEK" width="181" height="45" border="0" title="SIPOTEK" /></a>
					</li>-->
					<li class="name">
						<h1><a href="#"><img src="<?php echo site_url('img/logo.png') ?>" alt="SIPOTEK" width="181" height="45" border="0" title="SIPOTEK" /></a></h1>
					</li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon">
						<a href="#"><span>Menu</span></a>
					</li>
				</ul>
				<section class="top-bar-section">
					<!-- Right Nav Section -->
					<ul class="right">
						<li class="divider hide-for-small"></li>
						<li>
							<a href="#">Home</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Obat</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Resep Obat</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Supplier</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Surat Jalan</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Faktur</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#">Statistik</a>
						</li>
						<li class="divider"></li>
					</ul>
				</section>
			</nav>
		</div>
		<div id="before-row">
			<div class="row-fluid">
				<div class="span8">
					<div class="hero-unit">
						<a href="#"><img src="<?php echo site_url('img/UI.png')  ?>"alt="" width="150px" height="150px" title="" style="float: left; margin-right: 20px"/></a>
						<h1 style="margin-top: 20px; font-weight: normal">Sistem Informasi Apotek </br>Pusat Kesehatan Mahasiswa</br>Universitas Indonesia</h1></p></br>
						<p style="color: #F5FFF5">
							Apotek PKM (Pusat Kesehatan Mahasiswa) Universitas Indonesia merupakan gudang persediaan obat yang dimiliki oleh PKM UI dan berlokasi di PKM UI
						</p>
						<p style="color: #F5FFF5">
							PKM (Pusat Kesehatan Mahasiswa) Universitas Indonesia menyediakan layanan kesehatan bagi mahasiswa UI, staff UI, dan masyarakat umum sekitarnya
						</p>
					</div>
				</div>
                            <div id="login-area" style="margin-top: 50px">
					<div id="header-login">
						<p class="text-center" style="color: #F5FFF5">LOGIN<p>
					</div>
					<div id="middle-login">
				<form action="<?php echo site_url('index.php/home_controller/login'); ?>" method="post" name="login" id="login">
						<p style="font-size: 12px">
							Silahkan masukkan username dan</br> password sesuai dengan account</br> anda</br></br>
						</p>
						<div class="input-prepend">
							
								<?php if($error == 1) { ?>
									<p style="font-size: 12px; color: red; font-weight: bold;">
										Username atau Password salah
									</p>
								<?php } ?>

							<span class="add-on" style="height: 35px"><i class="icon-user" style="margin-top: 5px"></i></span>
							<input class="username" name="username" id="username" type="text" placeholder="Username" style="background-color: #EAEDF1; height: 35px">
						</div></br>
						<div class="input-prepend">
							<span class="add-on" style="height: 35px"><i class="icon-lock" style="margin-top: 5px"></i></span>
							<input class="password" name="password" id="password" type="password" placeholder="Password" style="background-color: #EAEDF1; height: 35px">
						</div>
					</div>
					<div id="footer-login">
						<p class="text-center">
							<i class="icon-circle-arrow-right icon-white"></i>
							<input type="submit" class="btn btn-info" style="width: 200px" title="login" value="login" /> 
						</p>
					</div>
				</form>
				</div>
			</div>
		</div>
        </div>
		<!--Footer Menu Part Starts -->
		<div id="footer">
			<!--Footer Menu Content Part Starts -->
                            <p class="text-center" style="padding-top: 20px"><a href="#" title="">Copyright SIPOTEK 2013 &copy; Propensi A07</a></p>
			<!--Footer Menu Content Part Ends -->
		</div>
	</body>
</html>