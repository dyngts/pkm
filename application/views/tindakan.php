<?php echo form_open("koorpel/searchtindakan"); ?>
<p><input type='text' name='searchquery' placeholder='Cari Tindakan' id='search_box' class='input_form' style='float:none;'/>
<input type='submit' value='Cari Tindakan' class='button red'/>
<a href='<?php echo base_url();?>simpel/tindakan'><button class='button orange'>Kembali</button></a>
<?php echo form_close(); ?>
<div id="tambahtindakan">
	<?php echo form_open("koorpel/addTindakan"); ?>
	<input type='submit' value='Tambah Tindakan Baru' class='button green'/>
	<?php echo form_close(); ?>
</div>	
<br>
	<center>
	<div id='tabel_tindakan'>
		<div id="kurung"><h2 class="text-left">Daftar Tindakan yang ada: </h2></div>
	    <table class="table">
	        <tr>
	            <th class="first">
	                Nama Tindakan
	            </th>
	            <th class="even-col">
	                Harga Tindakan <br/>untuk Umum
	            </th>
	            <th class="first">
	                Harga Tindakan <br/>untuk Karyawan
	            </th>
	            <th class="even-col">
	                Harga Tindakan <br/>untuk Mahasiswa
	            </th>
	            <th class="first">
	                Tindakan
	            </th>
	        </tr>
	        <?php
                foreach ($query as $row) {
				   echo '<tr>';
				   echo '<td class="text-left">';
				   echo $row['nama'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_umum'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_karyawan'];
				   echo '</td>';
				   echo '<td class="text-right">';
				   echo $row['tarif_mahasiswa'];
				   echo '</td>';
				   echo '<td id="actioncolumn" style="width:120px;">'; 
				   echo anchor("koorpel/updatetindakan/".$row['id']." ","<button class='button orange table_button'>Update</button>");
				   echo " ";
				   echo anchor("koorpel/deletetindakan/".$row['id']." ","<button class='button red table_button'>Delete</button>");
				   echo '</td>';
				   echo '</tr>';
				}
	        ?>
	    </table>	
	</div>
	<br>
	<div class="halaman">Halaman : <?php echo $halaman;?></div>

	</center>
</div>