<!DOCTYPE html>
<html>
  <head>
    <link href="<?php echo base_url(); ?>style/homepage.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/form.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/table.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>style/bar.css" rel="stylesheet" type="text/css">
    <title>Kesalahan</title>
  </head>
  <body style="margin-top: 60px; background-color: #1ABB9B; text-align: center; color:#FFF;">
	<h1 style="color:#FFF;">:(</h1>
	<p>Halaman yang Anda cari tidak ditemukan <br/>atau<br/> Anda tidak memiliki hak akses</p>
  </body>
</html>