<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class akun extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		redirect('');
	}

	public function detail()
	{
		$id = $this->user_model->get_my_id();
		redirect('akun/profil/'.$id);
	}

	public function profil($id) 
	{

		$my_id = $this->authentication->get_my_id();
		if ( ! $this->authentication->is_loggedin()) redirect('site');
		elseif ($id == NULL) redirect('akun/detail');
		elseif ($this->user_model->get_userdata($id) == NULL) redirect('site');

		if (!isset($id)) $this->detail();
		else {
			$page = 'profilepengguna';
			if ( ! file_exists('application/views/site/'.$page.'.php')) {
				show_404();
			}
			if( ! $this->authentication->is_loket() && $this->user_model->get_my_id() != $id && ! $this->authentication->is_pelayanan() && ! $this->authentication->is_pj()) {
				$this->detail();
			}
			else {
			 	
		    $data['query'] = $this->user_model->get_user_by_id($id);
		    $data['show_button'] = true;
		    $data['show_button_verifikasi'] = false;
				$data['title'] = "Profil"; 
				$data['msg'] = null;
				$data['role'] = $this->user_model->get_user_role_name_by_id($id);
				if (isset($data['query']['status_verifikasi'])) if ($data['query']['status_verifikasi'] == 'f') $this->profilcalonpasien($id);
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('site/'.$page, $data);
				$this->load->view('templates/footer', $data);

			}
		}
	}

	public function profilcalonpasien($id) 
	{

		$my_id = $this->authentication->get_my_id();
		if (!$this->authentication->is_loggedin()) redirect('site');
		elseif ($id == NULL) redirect('akun/detail');
		elseif ($this->user_model->get_userdata($id) == NULL) redirect('akun/detail');

  	$jenis_user = $this->user_model->get_user_role_by_id($id);
  	
  	if ($id != $my_id) {
			if ($jenis_user <= 3 && !$this->authentication->is_loket()) redirect('akun/detail');
			elseif ($jenis_user > 3 && !$this->authentication->is_pelayanan()) redirect('akun/detail');
  	}


		if ($id == NULL) redirect('akun/detail');
		else {
			$page = 'profilepengguna';
			if ( ! file_exists('application/views/site/'.$page.'.php')) {
				show_404();
			}
			if( ! $this->authentication->is_loket() && $this->user_model->get_my_id() != $id) {
				show_404();
			}
			else {
			 	
		    $data['query'] = $this->user_model->get_user_by_id($id);
		    $data['show_button'] = false;
		    $data['show_button_verifikasi'] = $this->authentication->is_loket();
				$data['title'] = "Profil Calon Pasien"; 
				$data['msg'] = null;
				$data['role'] = $this->user_model->get_user_role_name_by_id($id);
				if (isset($data['query']['status_verifikasi'])) if ($data['query']['status_verifikasi'] == 't') $this->profil($id);
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('site/'.$page, $data);
				$this->load->view('templates/footer', $data);

			}
		}
	}

  public function editpassword($id) 
  {

		$my_id = $this->authentication->get_my_id();
		$userdata = $this->user_model->get_user_by_id($id);
		if (!$this->authentication->is_loggedin()) redirect('site');
		elseif ($id == NULL) redirect('akun/editpassword/'.$my_id);
		elseif ($userdata == NULL) redirect('akun/editpassword/'.$my_id);

  	$jenis_user = $userdata['id_otorisasi'];
  	
  	if ($id != $my_id) {
				if ($jenis_user <= 3 && !$this->authentication->is_loket()) redirect('akun/editpassword/'.$my_id);
				elseif ($jenis_user > 3 && ! ( $this->authentication->is_pelayanan() || $this->authentication->is_pj())) redirect('akun/editpassword/'.$my_id);
  	}

    if (!isset($id)) {
    	$id = $this->user_model->get_my_id();
    	$this->editpassword($id);
    }
    else {

    	$my_id = $this->user_model->get_my_id();

    	$this->load->helper(array('form', 'url'));

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

			$this->form_validation->set_message('required', '%s harus diisi');
			$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');

			$this->form_validation->set_rules('password', 'Password baru', 'required|min_length[6]');
			
			if ($my_id == $id) 
			{
				$bypass = false;
				$this->form_validation->set_rules('oldpassword', 'Password Lama', 'required');
			}
			else $bypass = true;

			if ($this->form_validation->run() == TRUE)
			{

				$oldpassword = $this->input->post('oldpassword');
				$mypassword = $this->user_model->get_user_password_by_id($my_id);

				if (md5($oldpassword) == $mypassword) 
					$old_password_valid = true; 
				else $old_password_valid = false;

				if ($bypass || $old_password_valid) {
					$this->savenewpassword($id);
				}
				else 
				{
					$data_in['id'] = $id;
					$data_in['set_password_error'] = true;
					$this->formeditpassword($data_in);
				}	
			}
			else
			{
				$data_in['id'] = $id;
				$data_in['set_password_error'] = false;
				$this->formeditpassword($data_in);
			}	
    }
  }

	private function formeditpassword($data_in) 
	{
		$id = $data_in['id'];
		$page = 'formeditpassword';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}

		$my_id = $this->user_model->get_my_id();

		if(!$this->authentication->is_pj()) {
			if(!$this->authentication->is_loket()) {
				if (!$this->authentication->is_pelayanan()) {
					if($my_id != $id) {
						redirect('site');
					}
				}
			}
		}

			$data['set_password_error'] = $data_in['set_password_error'];
		  $data['title'] = "Edit Password"; 
			$data['msg'] = "";
			$data['role'] = $this->user_model->get_user_role_by_id($id);
			$data['id'] = $id;
			if ($my_id == $id) $data['its_me'] = true;
			else $data['its_me'] = false;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
	}

	private function savenewpassword($id)
	{
		$page = "sukses";
		$data_in['id'] = $id;
    $data_in['username'] = $this->user_model->get_username_by_id($id);
    $data_in['password'] = $this->input->post('password');

		$this->user_model->updatepassword($data_in);
		
		$data['title'] = "Edit Password Sukses"; 
		$data['msg'] = "
                <div class=\"row\">
                  <div class=\"message message-green\">
                    <p class=\"p_message\">Password untuk ".$id." - ".$this->user_model->get_user_name_by_id($id)." berhasil diperbarui </p>
                  </div>
                </div>";

		$data['role'] = $this->user_model->get_user_role_by_id($id);
		$data['id'] = $id;

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
   
	public function edit($id) {

		$my_id = $this->authentication->get_my_id();
		$user_data = $this->user_model->get_userdata($id);

		if ( ! $this->authentication->is_loggedin()) redirect('site');
		elseif ($id == NULL) redirect('akun/edit/'.$my_id);
		elseif ($user_data == NULL) redirect('akun/edit/'.$my_id);

  	$jenis_user = $this->user_model->get_user_role_by_id($id);

  	if ($id != $my_id) {
			if ($jenis_user <= 3 && ! $this->authentication->is_loket() && ! $this->authentication->is_pj()) redirect('akun/edit/'.$my_id);
			elseif ($jenis_user > 3 && ! $this->authentication->is_pelayanan() && ! $this->authentication->is_pj()) redirect('akun/edit/'.$my_id);
  	}


  	$default_id_foto = $user_data['foto'];
		$id_foto = $this->random(16).".jpg";
		$data = array('id' => $id, 'jenis_user' => $jenis_user, 'id_foto' => $id_foto,);

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		
		$this->form_validation->set_message('required', '%s wajib diisi');
		$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('is_natural', '%s harus angka');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		$role = $jenis_user;

		if ($role == 3) {
			$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|max_length[20]|callback_char_space_only');
			$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required|max_length[25]|is_natural');
		}

		if ($role == 2) {
			$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|required|max_length[30]|callback_char_space_only');
		}

		if ($role == 1) {
			$this->form_validation->set_rules('npm', 'NPM', 'required|is_natural|exact_length[10]');
			$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
			$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required|max_length[50]|callback_char_space_only');
			$this->form_validation->set_rules('tahunmasuk', 'Tahun Masuk UI', 'required|is_natural_no_zero|max_length[4]');
		}

		if ($role <= 3) {
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'required|max_length[30]|callback_char_space_only');
			$this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
			$this->form_validation->set_rules('agama', 'Agama', 'required');
			$this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
			$this->form_validation->set_rules('golongan_darah', 'Golongan darah', 'required');
			$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');
		}

		if ($role > 3) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		}

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_dash_only');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');

	 if ($this->form_validation->run() == TRUE)
	 {
			if ($role <= 3) $path = './foto/pasien/';
			else $path = './foto/pegawai/';
	    $config = array(
	     'allowed_types' => 'jpg|jpeg|png|gif',
	     'upload_path' => $path,
	     'max_size' => 1024,
	     'max_height' => 600,
	     'max_width' => 400,
	     'file_name' => $id_foto,
	     'overwrite' => TRUE
	     //'remove_spaces' => TRUE
	    );

	    $this->load->helper('file');
	    $this->load->library('upload', $config);

	    if ($_FILES && $_FILES['foto']['name'] !== "") {
			// Upload code here
				if ($this->upload->do_upload('foto'))
				{
					$this->update_data($data);
				}
				else
				{

					$error = array('error' => $this->upload->display_errors());

					$page = 'editdatapasien';
					if ( ! file_exists('application/views/site/'.$page.'.php')) {
						show_404();
					}
					if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
						show_404();
					}
					else {
						$this->load->model('user_model');
						$data['query'] = $this->user_model->get_userdata($id);

						$data['title'] = "Edit Profil"; 
						$data['msg'] = $this->upload->display_errors('<p>', '</p>');
						$data['role'] = $this->user_model->get_user_role_by_id($id);
						$data['id'] = $id;
						$data['error'] = $error;

						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('site/'.$page, $data);
						$this->load->view('templates/footer', $data);
					}
				}
	    }

	    else {
				$data['id_foto'] = $default_id_foto;
				$this->update_data($data);
	    }

	 	}

		else
		{
			$this->editdata($id);
		} 	

	}

	private function editdata($id) {
		$page = 'editdatapasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		else {
			$this->load->model('user_model');
		    $data['query'] = $this->user_model->get_userdata($id);

			$data['title'] = "Edit Profil"; 
			$data['msg'] = null;
			$data['role'] = $this->user_model->get_user_role_by_id($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

  private function update_data($data){
		$my_id = $this->authentication->get_my_id();
		$id = $data['id'];
	  $success = $this->process_edit($data);
	  
	  if ($success) {
	  	$this->authentication->update_session_data($my_id);
	  	$this->editsukses($id);
	  }
	  else $this->index();
  }

	private function process_edit($data) {

		// Ambil data yang dimasukkan atau yang dipilih user
		$nama = $this->user_model->get_user_name_by_id($data['id']);
		if ($this->authentication->is_loket() || $this->authentication->is_pelayanan() || $this->authentication->is_pj()) $nama = $this->input->post('nama');
		if ($data['jenis_user'] > 3) $username = $this->input->post('username');
		else $username = $this->user_model->get_username_by_id($data['id']);
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$agama = $this->input->post('agama');
		$gol_darah = $this->input->post('golongan_darah');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$no_telepon = $this->input->post('no_telp');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$alamat = $this->input->post('alamat');
		$peran = $this->input->post('peran');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$jurusan = $this->input->post('jurusan');
		$no_identitas = $this->input->post('no_identitas');
		if($no_identitas) {}
		else $no_identitas = $this->input->post('npm');


		$fakultas = $this->input->post('fakultas');
		$pekerjaan = $this->input->post('pekerjaan');
		$lembaga = $this->input->post('lembaga');
		if ($fakultas) {
			$lembaga_fakultas = $fakultas;
		}
		elseif ($pekerjaan) {
			$lembaga_fakultas = $pekerjaan;
		}
		elseif ($lembaga) {
			$lembaga_fakultas = $lembaga;
		}

		$semester_daftar = $this->input->post('tahunmasuk');
		if($semester_daftar) $tahun_masuk = $semester_daftar;
		else $tahun_masuk = (integer) date("Y");

		$id_otorisasi = $data['jenis_user'];
		$jenis_pasien = $id_otorisasi;
		$foto = $data['id_foto'];
		$password = md5('pass');
		$id = $data['id'];
		$old_username = $this->user_model->get_username_by_id($id);

		
		if ($id_otorisasi <= 3)
		{

			$datapasien_gen = array(
				'nama' => $nama, 
				'jenis_kelamin' => $jenis_kelamin,
				'agama' => $agama,  
				'gol_darah' => $gol_darah,
				'tempat_lahir' => $tempat_lahir, 
				'tanggal_lahir' => $tanggal_lahir, 
				'status_pernikahan' => $status_pernikahan,
				'alamat' => $alamat,
				'jenis_pasien' => $jenis_pasien,
				'lembaga_fakultas' => $lembaga_fakultas,
				'kewarganegaraan' => $kewarganegaraan,
				'foto' => $foto,
				'no_telepon' => $no_telepon,
				'no_identitas' => $no_identitas,
				'username' => $username,
				'jurusan' => $jurusan,
				'tahun_masuk' => $tahun_masuk,
				);

			$datapasien_auth = array(
				'username' => $username,
				'id_otorisasi' => $id_otorisasi,
			);

			$data_id = array(
					'id' => $id,
					'username' => $old_username,
				);

			$datapasien = array(
				'data_id' => $data_id,
				'data_auth' => $datapasien_auth,
				'data_general' => $datapasien_gen,
			);

			return $this->user_model->update_pasien($datapasien);
		
		}
		else
		{
			$datapegawai_gen = array(
				'nama' => $nama, 
				'jenis_kelamin' => $jenis_kelamin,
				'email' => $email,
				'alamat' => $alamat,
				'no_telepon' => $no_telepon,
				'foto' => $foto,
				'username' => $username,
				'id_otorisasi' => $id_otorisasi,
				);

			$datapegawai_auth = array(
				'username' => $username, 
				'id_otorisasi' => $id_otorisasi,
				);

			$data_id = array(
				'id' => $id,
				'username' => $old_username,
				);

			$datapegawai = array(
				'data_id' => $data_id,
				'data_auth' => $datapegawai_auth,
				'data_general' => $datapegawai_gen,
				);

			return $this->user_model->update_pegawai($datapegawai);
		
		}
	}

	private function editsukses($id) {
		$page = 'profilepengguna';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		else {
		 	
	    $data['query'] = $this->user_model->get_userdata($id);
	    
	    if ($data['query']['otorisasi_id'] > 3) {
	    	if ($data['query']['status_aktif'] == "t")
	    		$data['show_button_verifikasi'] = false;
	    	else $data['show_button_verifikasi'] = true;
	    }
	  	elseif ($data['query']['otorisasi_id'] <= 3) {
	    	if ($data['query']['status_verifikasi'] == "t")
	    		$data['show_button_verifikasi'] = false;
	    	else $data['show_button_verifikasi'] = true;
	    }

	    $data['show_button'] = !$data['show_button_verifikasi'];	 
			$data['title'] = "Profil";
			$data['msg2'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Data berhasil disimpan! </p>
                    </div>
                  </div>";
			$data['role'] = $this->user_model->get_user_role_by_id($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function delete($id) 
	{

		$my_id = $this->authentication->get_my_id();
		if (!$this->authentication->is_loggedin()) redirect('site');
		elseif ($id == NULL) redirect('site');
		elseif ($this->user_model->get_userdata($id) == NULL) redirect('site');

  	$jenis_user = $this->user_model->get_user_role_by_id($id);
  	
  	if ($id != $my_id) {
			if ($jenis_user <= 3 && !$this->authentication->is_loket()) redirect('site');
			elseif ($jenis_user > 3 && ! ($this->authentication->is_pelayanan() || $this->authentication->is_pj())) redirect('site');
  	}
  	else redirect('site');

		$sukses = $this->del($id);

		if ($sukses) {
			$data['title'] = "Akun berhasil dihapus"; 
			$data['msg'] = "
	      <div class=\"row\">
	        <div class=\"message message-green\">
	          <p class=\"p_message\">Akun berhasil dihapus</p>
	        </div>
	      </div>";
		}
		else {
			$data['title'] = "Operasi gagal";
			$data['msg'] = "
	      <div class=\"row\">
	        <div class=\"message message-red\">
	          <p class=\"p_message\">Akun ".$this->user_model->get_user_name_by_id($id)." tidak berhasil dihapus</p>
	        </div>
	      </div>";
		}

		$this->sukses($data);
	}

	private function del($id) {
		$userdata = $this->user_model->get_user_by_id($id);
		if ($userdata == NULL) return FALSE;
		$peran = $userdata['otorisasi_id'];
		if ($this->authentication->get_my_id() == $id) return FALSE;
		if ($this->authentication->is_loket()) {
			if ($peran <= 3) return $this->user_model->delete($userdata);
		}
		if ($this->authentication->is_pelayanan() || $this->authentication->is_pj()) {
			if ($peran > 3) return $this->user_model->delete($userdata);
		}		
		return FALSE;
	}

	private function sukses($data)
	{
		$page = "sukses";

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function pegawai()
	{

		$page = 'daftarpegawai';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if( ! $this->authentication->is_pelayanan() && ! $this->authentication->is_pj()) {
			show_404();
		}
		else{
			$all = $this->user_model->get_num_of_pegawai();
		  $data['all'] = $all;

			$config['base_url'] = site_url('/akun/pegawai');
			$config['total_rows'] = $all;
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	    $data["query"] = $this->user_model->get_limited_pegawai($config["per_page"], $page_num);

			$data['nav'] = $this->pagination->create_links();
		 
		  $data['msg'] = "";
			$data['title'] = "Daftar Pegawai"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function pasien() {

		$page = 'daftarpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket()) {
			show_404();
		}
		else {
			$all = $this->user_model->get_num_of_pasien();
		  $data['all'] = $all;

			$config['base_url'] = site_url('/akun/pasien');
			$config['total_rows'] = $all;
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	    $data["query"] = $this->user_model->get_limited_pasien($config["per_page"], $page_num);

			$data['nav'] = $this->pagination->create_links();
		 
		  $data['msg'] = "";
			$data['title'] = "Daftar Pasien"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function verifikasi() {
		$this->calonpasien();
	}

	public function verify($user_id) {

		$my_id = $this->authentication->get_my_id();

		if (!$this->authentication->is_loggedin() && !$this->authentication->firsttime()) redirect('site');
		elseif ($user_id == NULL) redirect('site');
		elseif ($this->user_model->get_userdata($user_id) == NULL) redirect('site');

  	$jenis_user = $this->user_model->get_user_role_by_id($user_id);
  	
  	if ($user_id != $my_id) {
			if ($jenis_user <= 3 && $this->authentication->is_loket()) {}
			elseif ($jenis_user > 3 && ($this->authentication->is_pelayanan() || $this->authentication->is_pj())) {}
			elseif ($this->authentication->firsttime()) {}
			else redirect('site');
  	}
  	else redirect('site');
		if ($jenis_user <= 3) $table = "pasien";
		else $table = "pegawai";
		$username = $this->user_model->get_username_by_id($user_id);	
		$data = $this->user_model->get_user_by_username($username);
		$data_out = $this->user_model->verify($table,$data);
		$this->verifikasisukses($data_out);
	}

	private function verifikasisukses($data_in) {
		$data['data'] = $data_in;
		$data['data']['name'] = $this->user_model->get_user_name_by_id($data_in['id']);
		$page = "verifikasisukses";
		$data['title'] = "Verifikasi Berhasil"; 
		$data['verify_page'] = true;
		$this->load->view('templates/header', $data);
		if ($this->authentication->is_loggedin())
		$this->load->view('usermenu/usermenu', $data);
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function generate_password($user_id) {

		$my_id = $this->authentication->get_my_id();
		if (!$this->authentication->is_loggedin()) redirect('site');
		elseif ($user_id == NULL) redirect('site');
		elseif ($this->user_model->get_userdata($user_id) == NULL) redirect('site');

		if ($this->user_model->get_user_role_by_id($user_id) <= 3)
		{
			if (!$this->authentication->is_loket()) redirect('site');
		}	
		elseif ($this->user_model->get_user_role_by_id($user_id) > 3)
		{
			if (!$this->authentication->is_pelayanan()) redirect('site');
		}

		$newpassword = $this->random_string(6);
    $data_out = array(
      'id' => $user_id,
      'username' => $user_id,
      'password' => $newpassword,
    );

		$this->user_model->updatepassword($data_out);

		$data['data'] = $data_out;
		$data['data']['name'] = $this->user_model->get_user_name_by_id($user_id);
		$page = "verifikasisukses";
		$data['title'] = "Informasi Akun"; 
		$data['verify_page'] = false;
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	private function calonpasien() {
		$page = 'verifikasiakun';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket()) {
			show_404();
			//show_error('Anda tidak punya hak akses');
		}
		else {


			$this->load->model('calon_pasien');
			$all = $this->user_model->get_all_calon_pasien();

			$config['base_url'] = site_url('/site/verifikasiakun');
			$config['total_rows'] = $this->user_model->get_num_of_calon_pasien();;
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	        $data["query"] = $this->user_model->get_limited_calon_pasien($config["per_page"], $page_num);

			$data['nav'] = $this->pagination->create_links();

		 
		    $data['msg'] = "";
			$data['title'] = "Verifikasi Akun"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

  public function random($length) 
  {
  	$key = "";
    $keys = array_merge(range(0,9), range('a', 'z'));
    for($i=0; $i < $length; $i++) {
        $key = $key.$keys[array_rand($keys)];
    }
    return $key;
	}

  public function random_string($length) 
  {
    $key = "";
    $keys = range('a', 'z');
    $keys0 = array('c','r','q','f','x','v','z');
    $keys1 = array('a','i','u','e','o');
    $keys2 = array_diff($keys, array_merge($keys0,$keys1));
    
    for($i=0; $i < $length; $i++) {
        if ($i%2 == 1) {
          $key = $key.$keys1[array_rand($keys1)];
        }
        else {
          $key = $key.$keys2[array_rand($keys2)];
        }
    }
    return $key;
  }

	public function char_dash_only($str) {

		if (! preg_match("/^([-a-z .,'])+$/i", $str))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function char_dash_num_only($str) {

		if (! preg_match("/^([-a-z .,0-9\/])+$/i", $str))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function char_space_only($str) {

    if (! preg_match("/^([a-z ])+$/i", $str))
    {
    	return FALSE;
    }
    else
    {
    	return TRUE;
    }
	}

}

/* End of file  */
/* Location: ./application/controllers/ */
