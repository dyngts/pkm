<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		$data['title'] = "Login"; 
		$this->load->view('templates/header', $data);
		$this->load->view('site/login', $data);
		$this->load->view('templates/footer');
	}

	public function login()
	{

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($username != NULL && $password != NULL)
		$authenticated = $this->authentication->validate($username,$password);
		
		redirect('site');
	}

	public function process()
  {
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($username == NULL || $password == NULL)
		{
			$data['msg3'] = '<p style="color:#F00; font-size:0.95em;">Username atau password harus diisi.</p>';
			//$this->load->view('templates/header', $data);
			//$this->load->view('usermenu/usermenu', $data); 
			
			$data['title'] = "Login"; 
			$this->load->view('templates/header', $data);
			$this->load->view('site/login', $data);
			$this->load->view('templates/footer');
		}

		else 
		{
	    // Validate the user can login
	    $result = $this->authentication->validate($username,$password);
	    $data['result'] = $result;
	    // Now we verify the result
	    if( ! $result)
	    {
	       // If user did not validate, then show them login page again
				$data['msg3'] = '<p style="color:#F00; font-size:0.95em;">Username atau password salah.</p>';
				//$this->load->view('templates/header', $data);
				//$this->load->view('usermenu/usermenu', $data); 
				
				$data['title'] = "Login"; 
				$this->load->view('templates/header', $data);
				$this->load->view('site/login', $data);
				$this->load->view('templates/footer');

	    } 

      elseif ($this->authentication->is_baru())
      {
          // If user did validate, 
          // Send them to members area
          //redirect('home');
          redirect('site/registrasi');
      }  

	    elseif ($this->authentication->is_mendaftar()) 
	    {
	      

				$page = 'registrasisuccess';
				if ( ! file_exists('application/views/site/'.$page.'.php')) 
				{
							show_404();
				}
			
        $this->load->model('user_model');

				$id = $this->user_model->get_my_id();
				$this->profilecalonpasien($id);

	    }

      else
      {
          // If user did validate, 
          // Send them to members area
          //redirect('home');
          redirect('site');
      }    
		}    
  }
    
  public function logout(){
    $this->session->sess_destroy();
    redirect('site');
  }

}

/* End of file  */
/* Location: ./application/controllers/ */
