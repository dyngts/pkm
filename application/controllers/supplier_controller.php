<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('supplier_model');
	}

	public function index()
	{
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

	}



	public function tambah() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		$this->load->library('form_validation');

		$aturan_tambah = array(
			   array(
                     'field'   => 'supplier',
                     'label'   => 'Nama Supplier',
                     'rules'   => 'trim|required|callback__cek_nama_supplier|callback__cek_input_teks'
                  ),
			   array(
                     'field'   => 'alamat',
                     'label'   => 'Alamat',
                     'rules'   => 'trim|required|callback__cek_input_teks'
                  ),
               array(
                     'field'   => 'telepon',
                     'label'   => 'Nomor Telepon',
                     'rules'   => 'trim|callback__cek_no_telepon|callback__cek_kode_area|min_length[6]'
                  ),
               array(
                     'field'   => 'kode_area',
                     'label'   => 'Kode Area',
                     'rules'   => 'trim|callback__cek_no_telepon|callback__cek_kode_area'
                  ),
               array(
                     'field'   => 'kota',
                     'label'   => 'Nama Kota',
                     'rules'   => 'trim|required|callback__cek_nama_supplier|callback__cek_input_teks'
                  ),   
               array(
                     'field'   => 'kontak',
                     'label'   => 'Kontak yang Bisa Dihubungi',
                     'rules'   => 'trim'
                  ),
               array(
                     'field'   => 'email',
                     'label'   => 'Email',
                     'rules'   => 'trim|valid_email'
                  ),
               array(
                     'field'   => 'status',
                     'label'   => 'Status',
                     'rules'   => 'trim|required'
                  ),
            );

		if($this->input->post('batal') == "Batal") {
			redirect(site_url('/supplier_controller/kumpulan'));
		}

		$this->form_validation->set_rules($aturan_tambah); 

		$this->form_validation->set_message('required', '%s Harus Diisi'); 
		$this->form_validation->set_message('greater_than', 'Jumlah Obat Masuk Harus Lebih dari 0');
		$this->form_validation->set_message('is_natural', 'Jumlah Obat Masuk Harus Bilangan Bulat');
		$this->form_validation->set_message('min_length', 'Panjang nomor telepon minimal 6 digit');

		if($this->form_validation->run() == TRUE) {

			if($_POST) {
				$data = array();

				$data['nama'] = $this->security->xss_clean($this->input->post('supplier', TRUE));
				$data['no_telepon'] = $this->security->xss_clean($this->input->post('kode_area', TRUE)).'-'.$this->security->xss_clean($this->input->post('telepon', TRUE));
				$data['alamat'] = $this->security->xss_clean($this->input->post('alamat', TRUE));
				$data['kota'] = $this->security->xss_clean($this->input->post('kota', TRUE));
				$data['contact_person'] = $this->security->xss_clean($this->input->post('kontak', TRUE));
				$data['email'] = $this->security->xss_clean($this->input->post('email', TRUE));
				$data['status'] = ($this->input->post('status', TRUE) == 'aktif') ? '1' : '0';

				$this->supplier_model->_tambah($data, $this->t_supplier);
				$this->session->sess_expiration = '5';
				$this->session->set_userdata('notif_mess', 'Data supplier telah berhasil ditambahkan!');
				redirect(site_url('/supplier_controller/kumpulan'));
			}
		}
		
		$data['nama'] = $this->security->xss_clean($this->input->post('supplier', TRUE));
		$data['no_telepon'] = $this->security->xss_clean($this->input->post('telepon', TRUE));
		$data['kode_area'] = $this->security->xss_clean($this->input->post('kode_area', TRUE));
 		$data['alamat'] = $this->security->xss_clean($this->input->post('alamat', TRUE));
		$data['kota'] = $this->security->xss_clean($this->input->post('kota', TRUE));
		$data['contact_person'] = $this->security->xss_clean($this->input->post('kontak', TRUE));
		$data['email'] = $this->security->xss_clean($this->input->post('email', TRUE));
		$data['status'] = ($this->input->post('status', TRUE) == 'aktif') ? '1' : '0';
		$data['nama_aktif'] = 'supplier';
		$data['title'] = 'Tambah Supplier';
		$this->cetak_halaman('supplier/_tambah_supplier', $data);
	}

	public function _cek_input_teks($str) {

		if($str != null) {
			if(!preg_match('/^[0-9a-zA-Z,.-\s]+$/', $str)) {
				$this->form_validation->set_message('_cek_input_teks', 'Format masukkan mengandung karakter yang tidak diperbolehkan');
				return FALSE;
			}
		}
		return TRUE;
	}

	public function _cek_no_telepon($str) {
		if($str != null) {
			if(!preg_match('/^[0-9]+$/', $str)) {
				$this->form_validation->set_message('_cek_no_telepon', 'Nomor Telepon/Kode Area hanya boleh angka saja');
				return FALSE;
			}
		}
		return TRUE;
	}

	public function _cek_kode_area() {
		$kode_area = $this->security->xss_clean($this->input->post('kode_area'));
		$telepon = $this->security->xss_clean($this->input->post('telepon'));

		if($kode_area != null && $telepon == null) {
			$this->form_validation->set_message('_cek_kode_area', 'Nomor telepon tidak boleh kosong');
			return FALSE;
		}
		else if($kode_area == null && $telepon != null) {
			$this->form_validation->set_message('_cek_kode_area', 'Kode area tidak boleh kosong');
			return FALSE;
		}
		return TRUE;
	}

	public function _cek_nama_supplier($str) {
		$nama = $this->security->xss_clean($this->input->post('supplier', TRUE));
		$kota = $this->security->xss_clean($this->input->post('kota', TRUE));

		$hasil = $this->supplier_model->_ambil_baris_tertentu(array('nama'=>$nama, 'kota'=>$kota), $this->t_supplier, 'row', 'nama');

		if(count($hasil) > 0) {
			$this->form_validation->set_message('_cek_nama_supplier', 'Kombinasi nama supplier dan kota sudah terpakai ');
				return FALSE;
		}
		return TRUE;
	}

	

	public function kumpulan($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		$data = array();

		$hasil = $this->supplier_model-> _ambil_semua_supplier($mulai);

		$data['suppliers'] = $hasil['daftar_supplier'];

		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/supplier_controller/kumpulan';
		$config['total_rows'] = $hasil['jumlah_baris'];
    	$this->pagination->initialize($config);
    	$data['pagination_links'] = $this->pagination->create_links();	
    	$data['mulai'] = $mulai;
    	$data['title'] = 'Daftar Supplier';
		$this->cetak_halaman('supplier/_daftar_supplier', $data);
	}

	public function ganti_status($id) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		$supplier = $this->supplier_model->_ambil_baris_tertentu(array('id'=>$id), $this->t_supplier, 'row', 'id, status');

		$data = array();

		if($supplier->status == 't') {
			$data['status'] = '0';
		}
		else {
			$data['status'] = '1';
		}

		$this->supplier_model->_ubah(array('id'=>$id), $data, $this->t_supplier);

		redirect(site_url('/supplier_controller/kumpulan'));
	}

	public function detil($id, $mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		$this->load->library('pagination');
		$data = array();

		$data['supplier_obats'] = $this->supplier_model->_ambil_baris_supplier_obat_tertentu($id, 10, $mulai);
		$data['supplier'] = $this->supplier_model->_ambil_baris_tertentu(array('id'=>$id), $this->t_supplier, 'row');

		$config['base_url'] = base_url().'/supplier_controller/detil/'.$id;
		$config['total_rows'] = $this->supplier_model->_jumlah_baris_supplier_obat_tertentu($id);
		$config['uri_segment'] = 4;
		$config['num_links'] = 4;

    	$this->pagination->initialize($config);
    	$data['pagination_links'] = $this->pagination->create_links();
    	$data['mulai'] = $mulai;		
    	$data['title'] = 'Detil Supplier - '.$id;
		$this->cetak_halaman('supplier/_detil_data_supplier', $data);
	}

	public function ubah($id) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		$this->load->library('form_validation');

		$aturan_ubah = array(
			   array(
                     'field'   => 'supplier',
                     'label'   => 'Nama Supplier',
                     'rules'   => 'trim|required|callback__cek_nama_supplier_ubah'
                  ),
			   array(
                     'field'   => 'alamat',
                     'label'   => 'Alamat',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'telepon',
                     'label'   => 'Nomor Telepon',
                     'rules'   => 'trim|callback__cek_no_telepon|callback__cek_kode_area'
                  ),
               array(
                     'field'   => 'kode_area',
                     'label'   => 'Kode Area',
                     'rules'   => 'trim|callback__cek_no_telepon|callback__cek_kode_area'
                  ),
               array(
                     'field'   => 'kota',
                     'label'   => 'Kota',
                     'rules'   => 'trim|required|callback__cek_nama_supplier_ubah'
                  ),   
               array(
                     'field'   => 'kontak',
                     'label'   => 'Kontak yang Bisa Dihubungi',
                     'rules'   => 'trim'
                  ),
               array(
                     'field'   => 'email',
                     'label'   => 'Email',
                     'rules'   => 'trim|valid_email'
                  ),
               array(
                     'field'   => 'status',
                     'label'   => 'Status',
                     'rules'   => 'trim|required'
                  ),
            );

		if($this->input->post('batal')) {
			redirect(site_url('/supplier_controller/kumpulan'));
		}

		$this->form_validation->set_rules($aturan_ubah);

		if($this->form_validation->run() == TRUE) {
			if($_POST) {
				$data = array();
				$data['nama'] = $this->input->post('supplier', TRUE);
				$data['alamat'] = $this->input->post('alamat', TRUE);
				$data['kota'] = $this->input->post('kota', TRUE);
				$data['contact_person'] = $this->input->post('kontak', TRUE);
				$data['no_telepon'] = $this->input->post('kode_area', TRUE)."-".$this->input->post('telepon', TRUE);
				$data['email'] = $this->input->post('email', TRUE);
				$data['status'] = ($this->input->post('status', TRUE) == 'aktif') ? '1' : '0';

				$this->supplier_model->_ubah(array('id'=>$id), $data, $this->t_supplier);
				$this->session->sess_expiration = '5';
				$this->session->set_userdata('notif_mess', 'Data supplier telah berhasil diubah!');
				redirect(site_url('/supplier_controller/kumpulan'));
			}		
		}
		
		$data['supplier'] = $this->supplier_model->_ambil_baris_tertentu(array('id'=>$id), $this->t_supplier,  'row');
		$data['title'] = 'Ubah Data Supplier - '.$id;
		$this->cetak_halaman('supplier/_ubah_supplier', $data);
	}

	public function _cek_nama_supplier_ubah($str) {
		$nama_baru = $this->security->xss_clean($this->input->post('supplier', TRUE));
		$kota = $this->security->xss_clean($this->input->post('kota', TRUE));
		
		$id = $this->input->post('id_supplier', TRUE);
		$obat = $this->supplier_model->_ambil_baris_tertentu(array('nama'=>$nama_baru, 'kota'=>$kota), $this->t_supplier, 'row', 'nama, id');
		
		if(count($obat) > 0 && ($obat->id != $id)) {
			$this->form_validation->set_message('_cek_nama_supplier_ubah', 'Kombinasi nama supplier dan kota sudah terpakai');
			return FALSE;
		}
		return TRUE; 
	}

}

/* End of file supplier_controller.php */
/* Location: ./application/controllers/supplier_controller.php */