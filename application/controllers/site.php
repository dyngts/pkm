<?php

class Site extends CI_Controller {
    
  function __construct(){
  parent::__construct();
  $this->load->helper('url');
	$this->load->model('authentication');
	$this->load->model('antrian_model');
	$this->load->model('pegawai_model');
	$this->load->model('user_model');

	$this->load->library('pagination');
	$this->load->helper('form');
	$this->load->library('form_validation');
	$this->load->library('javascript');
	$this->load->library('calendar');

	$this->load->helper('date');
	$this->load->helper('security');
	$this->load->helper('captcha');

	$this->load->helper('date');
	$this->load->library('session');
	$this->load->model('artikel_model');
	$this->load->helper('text');

  }

  public function phpinfo()
  {
  	echo phpinfo();
  }

  public function md5($term)
  {
  	echo md5($term);
  }

	public function index() 
	{

		if( ! ENABLE_HOMEPAGE && ! $this->authentication->is_loggedin()) 
		{
			redirect('auth');
		}

		$page = 'news';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}

		if ($this->authentication->firsttime())
		{
			redirect('register/pegawai');
		}

		$data['title'] = "Beranda"; 
		$this->load->view('templates/header', $data);
	      $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
	          $this->load->view('usermenu/usermenu', $data);
	      } 
		else {
			$this->load->view('usermenu/loginform', $data);
		}
		$this->read_news();
		//$this->load->view('site/'.$page);
		$this->load->view('templates/footer');
	}

	public function read_news()
	{
		$this->load->model('artikel_model');
	  $semuaartikel = $this->artikel_model->daftar_artikel_terbit();
		$data ['all']= $semuaartikel->result();
	      // generate pagination
		
		$config['base_url'] = site_url('/site/index/');
		$config['total_rows'] = $semuaartikel->num_rows();
		$config['per_page'] = 3;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['first_link'] = 'Pertama';
		$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';
		$config['last_link'] = 'Terakhir';
		
		$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['page_num']= $page_num;
		$this->pagination->initialize($config);
		
		$data['row'] = $this->artikel_model->get_paged_list_terbit($config['per_page'], $page_num)->result();
		$data['pagination'] = $this->pagination->create_links();
		$data['title'] = "Home";
		//$this->load->view('templates/header', $data);
		//$this->load->view('usermenu/usermenu', $data); 
		$this->load->view('site/news',$data);
		//$this->load->view('templates/footer', $data);
	}
    
  public function process()
  {
		
		$page = 'news';
    // Load the model
    $this->load->model('authentication');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($username == NULL || $password == NULL)
			redirect('site');

    // Validate the user can login
    $result = $this->authentication->validate($username,$password);
    $data['result'] = $result;
    // Now we verify the result
    if(!$result){
        // If user did not validate, then show them login page again
		$data['msg3'] = '<p style="color:#F00; font-size:0.95em;">Username atau password salah.</p>';
		$data['title'] = "Home";
		//$this->load->view('templates/header', $data);
		//$this->load->view('usermenu/usermenu', $data); 
	

		$data['title'] = "Beranda"; 
		$this->load->view('templates/header', $data);
        	$this->load->model('authentication');
		$this->load->view('usermenu/loginform', $data);
		$this->read_news();
		$this->load->view('templates/footer');
      } 

      elseif ($this->authentication->is_baru()){
          // If user did validate, 
          // Send them to members area
          //redirect('home');
          redirect('site/registrasi');
      }  

      elseif ($this->authentication->is_mendaftar()) {
      

		$page = 'registrasisuccess';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
        $this->load->model('user_model');

		$id = $this->user_model->get_my_id();
		$this->profilecalonpasien($id);

      }

      else{
          // If user did validate, 
          // Send them to members area
          //redirect('home');
          redirect('site');
      }        
  }

  
  public function registrasi() 
  {
    $page = "registrasipasien";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}
		
		if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		if ($this->authentication->is_pasien()) {
			$this->index();
		}
		$data['title'] = "Berita"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
  }

  // TRASH
  // public function registrasipasien($peran) 
  // {
  // 	if ($peran == "umum") {
		// if ($this->session->userdata('is_pasien')) {
		// 	$this->index();
		// }
		// $this->registrasipasienumum();
  // 	}
  // 	elseif ($peran == "mahasiswa") {
		// if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
		// 	show_404();
		// }
		// if ($this->session->userdata('is_pasien')) {
		// 	$this->index();
		// }
		// $this->registrasipasienmahasiswa();
  // 	}
  // 	elseif ($peran == "karyawan") {
		// if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
		// 	show_404();
		// }
		// if ($this->session->userdata('is_pasien')) {
		// 	$this->index();
		// }
		// $this->registrasipasienkaryawan();
  // 	}
  // 	else {
		// $this->index();
  // 	}
  // }

  // TRASH
  // public function registrasipasienmahasiswa() {
		
		// $this->load->database();
		// $query = $this->db->get('last_id');
		// $row = $query->row(); 
		
		// $id_foto = $row->last_id_calon_pasien;
		// $id_foto++;
	    
		// $this->load->helper(array('form', 'url'));

		// $this->load->library('form_validation');
		// $this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		// $this->form_validation->set_message('required', '%s wajib diisi');
		// $this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		// $this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		// $this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		// $this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		// $this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		// $this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		// $this->form_validation->set_message('is_natural', '%s harus angka');
		// $this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		// $this->form_validation->set_rules('npm', 'NPM', 'required|is_natural|exact_length[10]');
		// $this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
		// $this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required|max_length[50]|callback_char_space_only');
		// $this->form_validation->set_rules('semester', 'Semester', 'required|is_natural_no_zero|max_length[2]');

		// $this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		// $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		// $this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'trim|required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
		// $this->form_validation->set_rules('agama', 'Agama', 'required');
		// $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		// $this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
		// $this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');

		// // if ($this->form_validation->run() == FALSE)
		// // {
		// // 	$this->form_pasien("mahasiswa");
		// // }
		// // elseif (!$this->authentication->is_loket())
		// // {
		// // 	$this->save();
		// // 	//$this->registrasisuccess();
		// // }
		// // else
		// // {
		// // 	$this->createandvalidate();
		// // }
	 //   if ($this->form_validation->run() == TRUE)
	 //   {

		//     $config = array(
		//      'allowed_types' => 'jpg|jpeg|png|gif',
		//      'upload_path' => './foto/pasien/',//base_url('foto'),
		//      'max_size' => 1024,
		//      'max_height' => 600,
		//      'max_width' => 400, 
		//      'file_name' => $id_foto.".jpg",
		//      'overwrite' => TRUE,
		//      //'remove_spaces' => TRUE
		//     );

		//     $this->load->helper('file');
		//     $this->load->library('upload', $config);

		//     if ($_FILES && $_FILES['foto']['name'] !== "") {
	 //  		// Upload code here


		//        if ($this->upload->do_upload('foto'))
		//        {
		//           //$this->Insert->add_file();
		//           //$this->session->set_flashdata('file_uploaded', TRUE);
		//           //$data['fuploaded'] = $this->session->flashdata('file_uploaded');
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "berhasil";

		// 			if (!$this->authentication->is_loket())
		// 			{
		// 				$this->save();
		// 				//$this->registrasisuccess();
		// 			}
		// 			else
		// 			{
		// 				$this->createandvalidate();
		// 			}
		//        }
		//        else
		//        {
		//           //$data['uploaderror'] = $this->upload->display_errors(); // <- this line is the solution for my second problem
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "gagal upload";
		//           //echo $data['uploaderror'];


		// 			$this->form_pasien("mahasiswa");
		//        }
		//     }

		//     else {

		// 		if (!$this->authentication->is_loket())
		// 		{
		// 			$this->save();
		// 			//$this->registrasisuccess();
		// 		}
		// 		else
		// 		{
		// 			$this->createandvalidate();
		// 		}
		//     }

	 //   	}

		// else
		// {
		// //$this->load->view('upload/scheme', $data);
		// $this->form_pasien("mahasiswa");

		// } 	
  //   }

  // TRASH
  //   public function jeniskelamin($str) {

		// if ($str == 'Laki-laki')
		// {
		// 	return FALSE;
		// }
		// else
		// {
		// 	return TRUE;
		// }
  //   }

  // TRASH
  // public function registrasipasienkaryawan() {

		// $this->load->database();
		// $query = $this->db->get('last_id');
		// $row = $query->row(); 
		
		// $id_foto = $row->last_id_calon_pasien;
		// $id_foto++;
	    
		// $this->load->helper(array('form', 'url'));

		// $this->load->library('form_validation');
		// $this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		// $this->form_validation->set_message('required', '%s wajib diisi');
		// $this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		// $this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		// $this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		// $this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		// $this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		// $this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		// $this->form_validation->set_message('is_natural', '%s harus angka');
		// $this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		// $this->form_validation->set_rules('lembaga', 'Lembaga', 'required|max_length[30]|callback_char_space_only');

		// $this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		// $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		// $this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
		// $this->form_validation->set_rules('agama', 'Agama', 'required');
		// $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		// $this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
		// $this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');

		// // if ($this->form_validation->run() == FALSE)
		// // {
		// // 	$this->form_pasien("karyawan");
		// // }
		// // elseif (!$this->authentication->is_loket())
		// // {
		// // 	$this->save();
		// // 	//$this->registrasisuccess();
		// // }
		// // else
		// // {
		// // 	$this->createandvalidate();
		// // }
	 //   if ($this->form_validation->run() == TRUE)
	 //   {

		//     $config = array(
		//      'allowed_types' => 'jpg|jpeg|png|gif',
		//      'upload_path' => './foto/pasien/',//base_url('foto'),
		//      'max_size' => 1024,
		//      'max_height' => 600,
		//      'max_width' => 400,
		//      'file_name' => $id_foto.".jpg",
		//      'overwrite' => TRUE
		//      //'remove_spaces' => TRUE
		//     );

		//     $this->load->helper('file');
		//     $this->load->library('upload', $config);

		//     if ($_FILES && $_FILES['foto']['name'] !== "") {
	 //  		// Upload code here


		//        if ($this->upload->do_upload('foto'))
		//        {
		//           //$this->Insert->add_file();
		//           //$this->session->set_flashdata('file_uploaded', TRUE);
		//           //$data['fuploaded'] = $this->session->flashdata('file_uploaded');
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "berhasil";

		// 			if (!$this->authentication->is_loket())
		// 			{
		// 				$this->save();
		// 				//$this->registrasisuccess();
		// 			}
		// 			else
		// 			{
		// 				$this->createandvalidate();
		// 			}
		//        }
		//        else
		//        {
		//           //$data['uploaderror'] = $this->upload->display_errors(); // <- this line is the solution for my second problem
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "gagal upload";
		//           //echo $data['uploaderror'];


		// 			$this->form_pasien("karyawan");
		//        }
		//     }

		//     else {

		// 		if (!$this->authentication->is_loket())
		// 		{
		// 			$this->save();
		// 			//$this->registrasisuccess();
		// 		}
		// 		else
		// 		{
		// 			$this->createandvalidate();
		// 		}
		//     }

	 //   	}

		// else
		// {
		// //$this->load->view('upload/scheme', $data);
		// $this->form_pasien("karyawan");

		// } 	
  //   }

  // TRASH
  //   public function registrasipasienumum() {

		// $this->load->database();
		// $query = $this->db->get('last_id');
		// $row = $query->row(); 
		
		// $id_foto = $row->last_id_calon_pasien;
		// $id_foto++;
	    
		// $this->load->helper(array('form', 'url'));

		// $this->load->library('form_validation');
		// $this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		// $this->form_validation->set_message('required', '%s wajib diisi');
		// $this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		// $this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		// $this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		// $this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		// $this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		// $this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		// $this->form_validation->set_message('is_natural', '%s harus angka');
		// $this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		// $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|max_length[20]|callback_char_space_only');
		// $this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required|max_length[25]|is_natural');

		// $this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		// $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		// $this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'trim|required|max_length[30]|callback_char_space_only');
		// $this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
		// $this->form_validation->set_rules('agama', 'Agama', 'required');
		// $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		// $this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
		// $this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');
		// $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|callback_char_space_only');

		// require('./recaptcha/recaptchalib.php');

		// if (!$this->input->post('bypass_captcha') == "true") {

		// 	$this->form_validation->set_rules('recaptcha', 'recaptcha', 'callback_checkRecaptcha');
		// }


		// // if ($this->form_validation->run() == FALSE)
		// // {
		// // 	$this->form_pasien("umum");
		// // }
		// // elseif (!$this->authentication->is_loket())
		// // {
		// // 	$this->save();
		// // 	//$this->registrasisuccess();
		// // }
		// // else
		// // {
		// // 	$this->createandvalidate();
		// // }






	 //   if ($this->form_validation->run() == TRUE)
	 //   {

		//     $config = array(
		//      'allowed_types' => 'jpg|jpeg|png|gif',
		//      'upload_path' => './foto/pasien/',//base_url('foto'),
		//      'max_size' => 1024,
		//      'max_height' => 600,
		//      'max_width' => 400,
		//      'file_name' => $id_foto.".jpg",
		//      'overwrite' => TRUE
		//      //'remove_spaces' => TRUE
		//     );

		//     $this->load->helper('file');
		//     $this->load->library('upload', $config);

		//     if ($_FILES && $_FILES['foto']['name'] !== "") {
	 //  		// Upload code here


		//        if ($this->upload->do_upload('foto'))
		//        {
		//           //$this->Insert->add_file();
		//           //$this->session->set_flashdata('file_uploaded', TRUE);
		//           //$data['fuploaded'] = $this->session->flashdata('file_uploaded');
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "berhasil";

		// 			if (!$this->authentication->is_loket())
		// 			{
		// 				$this->save();
		// 				//$this->registrasisuccess();
		// 			}
		// 			else
		// 			{
		// 				$this->createandvalidate();
		// 			}
		//        }
		//        else
		//        {
		//           //$data['uploaderror'] = $this->upload->display_errors(); // <- this line is the solution for my second problem
		//           //$this->load->view('upload/scheme', $data);
		//           //echo "gagal upload";
		//           //echo $data['uploaderror'];

		// 			$this->form_pasien("umum");
		//        }
		//     }

		//     else {

		// 		if (!$this->authentication->is_loket())
		// 		{
		// 			$this->save();
		// 			//$this->registrasisuccess();
		// 		}
		// 		else
		// 		{
		// 			$this->createandvalidate();
		// 		}
		//     }

	 //   	}

		// else
		// {
		// //$this->load->view('upload/scheme', $data);
		
		// $this->form_pasien("umum");

		// } 

	
  //   }

  // TRASH
  // public function save(){
  //     $this->load->model('form_model');        
  //     $success = $this->form_model->process();
  //     if ($success) {
  //     	$this->load->model('user_model');
  //     	//$id_calon_pasien = $this->session->userdata('data1');
  //     	//$id = $id_calon_pasien['id'];
  //     	$id = $this->session->userdata('id');
  //     	//$data_calon_pasien = $this->user_model->get_calon_pasien_data($id);
  //     	//$this->registrasisuccess($data_calon_pasien);
  //     	$this->registrasisuccess($id);
  //     	$this->session->sess_destroy();
  //     }
  //     else $this->index();
  //   }

  // TRASH
    // public function verify_this_account($id){
    //     $this->load->model('form_model');
    //     $this->form_model->verifikasi_akun($id);

    //     redirect('site/verifikasiakunsukses');
    // }

  	// TRASH
    public function createandvalidate(){
        $this->load->model('form_model');
        $id_sementara = $this->form_model->create();
        $id = $this->form_model->verifikasi_sekarang($id_sementara)."";
        $this->form_model->delete_calon_pasien($id_sementara);

		$page = 'profilepasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		else {
			$this->load->model('user_model');
		 	
		    $data['query'] = $this->user_model->get_userdata($id);
		    $data['show_button'] = true;
		    $data['show_button_verifikasi'] = false;
			$data['title'] = "Profil Pasien"; 
			$data['msg2'] = "
                  <div class=\"row\">
                    <div class=\"message message-green\">
                      <p class=\"p_message\">Pembuatan Akun Berhasil! </p>
                    </div>
                  </div>";
			$data['role'] = $this->user_model->get_user_role($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
    }

    // TRASH
	public function registrasisuccess($id) {
		$page = 'registrasisuccess';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
        //$this->load->model('user_model');

		//$id = $this->user_model->get_my_id();

		$this->profilecalonpasien($id);
	}

		// TRASH
    public function update_data_pasien($id){
        $this->load->model('form_model');
        $this->load->model('user_model');
        $this->load->model('authentication');

		$my_id = $this->user_model->get_my_id();

        $success = $this->form_model->update($id);
        if ($success) {
        	if ($my_id == $id ) $this->authentication->update_session_data_pasien($id);
        	$this->editpasiensukses($id);
        }
        else $this->index();
    }

    // TRASH
	public function profilecalonpasien($id) {
		$page = 'profilecalonpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		else {
			$this->load->model('user_model');
		 	
		    $data['query'] = $this->user_model->get_calon_pasien_data($id);
		    $data['query2'] = null;
		    //$data['query2'] = $this->user_model->get_calon_pasien_additional_data($id);
		    $data['show_button'] = !($this->authentication->is_baru()||$this->authentication->is_mendaftar()||$this->authentication->is_calon_pasien());
		    if ($this->authentication->is_loket()) $data['show_button'] = true;
			$data['title'] = "Profil Pasien"; 
			$data['role'] = $this->user_model->get_calon_pasien_role($id);

		    if ($this->authentication->is_loket()) {
		    	$data['msg'] = "";
		    }
			else 
			{
				$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Data Anda berhasil dikirim! 
			</br>Untuk mendapatkan username dan password,
			</br>silakan datang ke PKM UI dan melakukan verifikasi.</p>
                    </div>
                  </div>";
			}
					
			$this->load->view('templates/header', $data);
	        $this->load->model('authentication');
			if($this->authentication->is_loggedin()){
	            $this->load->view('usermenu/usermenu', $data);
	        }
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	// TRASH
	public function delete_calon_pasien($id) {

		$page = 'verifikasiakun';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket()) {
			show_404();
			//show_error('Anda tidak punya hak akses');
		}
		else {

			$this->load->model('form_model');
			$this->form_model->delete_calon_pasien($id);

			$this->load->model('calon_pasien');
			$all = $this->calon_pasien->get_calon_pasien();

			$config['base_url'] = site_url('/site/verifikasiakun');
			$config['total_rows'] = $all->num_rows();
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = (($this->uri->segment(3)) < 99999) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	        $data["query"] = $this->calon_pasien->get_calon_pasien_limited($config["per_page"], $page_num)->result();

			$data['nav'] = $this->pagination->create_links();

		    $data['msg'] = "
		              <div class=\"row\">
		                <div class=\"message message-yellow\">
		                  <p class=\"p_message\">Data berhasil dihapus! </p>
		                </div>
		              </div>";
			$data['title'] = "Verifikasi Akun"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	// TRASH
	public function deletepasien($id) {

		$page = 'daftarpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if (!$this->authentication->is_pelayanan()) {
			show_404();
		}
		else {

			$this->load->model('pasien');
			$this->pasien->deletepasien($id);

			$all = $this->pasien->get_all_pasien();
		    $data['all'] = $all->result();

			$config['base_url'] = site_url('/site/daftarpasien');
			$config['total_rows'] = $all->num_rows();
			$config['per_page'] = 15;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = (($this->uri->segment(3)) < 99999) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	        $data["query"] = $this->pasien->get_all_pasien_limited($config["per_page"], $page_num)->result();

			$data['nav'] = $this->pagination->create_links();
		 
		    $data['msg'] = "
		              <div class=\"row\">
		                <div class=\"message message-yellow\">
		                  <p class=\"p_message\">Data berhasil dihapus! </p>
		                </div>
		              </div>";

			$data['title'] = "Daftar Pasien"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

		// TRASH
    private function check_isvalidated(){
        if(! $this->session->userdata('validated')){
            //redirect('login');
        }
    }
    
    public function do_logout(){
        $this->session->sess_destroy();
        //redirect('login');
        redirect('site');
    }

  // TRASH
	public function news($page = 'news') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Berita"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function stats($page = 'stats') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Statistik"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function statsf($page = 'statsf') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Statistik"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function statsp($page = 'statsp') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
        $this->load->model('authentication');
		
		$data['title'] = "Statistik"; 
		$this->load->view('templates/header', $data);
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function statsk($page = 'statsk') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
        $this->load->model('authentication');
        if($this->authentication->is_pelayanan() || $this->authentication->is_loket()) {

        	$this->load->model('statistik_model');
        	$data['query']=$this->statistik_model->get_statistik_kepuasan();
           
			$data['title'] = "Statistik Kepuasan"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
        }
		else {
			redirect ('site/index');
		}
	}

	public function schedule()
	{
		redirect('jadwal/mingguan');
	}

	// TRASH
	public function schedule_lama($page = 'schedule') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('jadwal_model');
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
		$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
		
		$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();

		$data['msg'] = NULL;
		$data['title'] = "Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	function buka_registrasi_berobat()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->set_all_open();
		$this->load->model('jadwal_model');
		$this->jadwal_model->generate_jadwal_harian();
		redirect('site/antre/', 'refresh');
	}	

	public function about($page = 'about') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Tentang PKM"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function faq($page = 'faq') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Frequently Asked Question"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function antre($page = 'antre') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
        $this->load->model('authentication');
        if($this->authentication->is_loggedin() == false){
            redirect ('site/index');
        }
		$this->load->model('antrian_model');
		$this->load->model('klinik_model');
		$this->load->model('jadwal_model');
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_umum_harian();
		$data['doctor_table_gigi'] = $this->jadwal_model->get_gigi_harian();
		$data['doctor_table_medis'] = $this->jadwal_model->get_estetika_harian();
		
		$data['total_gate_umum'] = count($data['doctor_table_umum']);
		$data['total_gate_gigi'] = count($data['doctor_table_gigi']);
		
		$data['jml'] = $this->antrian_model->get_jml_antrian();
		$data['no_antrian'] = $this->antrian_model->get_no_antrian();
		$data['waktu'] = $this->antrian_model->estimasi_waktu();
		$data['jml_pengantre'] = $this->antrian_model->get_jml_pengantre();
		$data['terdaftar'] = $this->antrian_model->is_terdaftar();

		$data['id_umum'] = $this->antrian_model->terdaftar_umum();
		$data['id_gigi'] = $this->antrian_model->terdaftar_gigi();
		//redirect('test/ab/'.$data['id_umum']."/a/".$data['id_umum']."/papapa");

		//$data['terdaftar_gigi'] = $this->antrian_model->is_terdaftar_gigi();
		$data['buka_umum'] = $this->klinik_model->get_status('1');
		$data['buka_gigi'] = $this->klinik_model->get_status('2');

		$data['title'] = "Antrean"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function register($page = 'register') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Registrasi"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function form_pasien($peran) {
		if ( ! file_exists('application/views/site/registrasipasien'.$peran.'.php')) {
					show_404();
		}
		
		$data['publickey'] = '6LeZlOASAAAAANrX70FxDLD6wQfdWcGXE-upHd_C';
		$data['title'] = "Registrasi"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/registrasipasien'.$peran, $data);
		$this->load->view('templates/footer', $data);
	}

	
	// TRASH	
	public function article($page = 'article') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}

		$data['title'] = "Buat Artikel"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	// TRASH
	public function editarticle($id) {
		$this->load->model('artikel_model');
		$data['row'] = $this->artikel_model->get_artikel($id);
		$data['title'] = "Buat Artikel"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/editarticle', $data);
		$this->load->view('templates/footer', $data);
	}
	
	// TRASH
	public function staff($page = 'staff') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function articlelist($page = 'articlelist') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('artikel_model');
		$data['title'] = "Daftar Artikel"; 
		$data['row'] = $this->artikel_model->daftar_artikel_terbit();
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	public function verifikasi($page = 'verifikasi') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Verifikasi Status Antrean Pasien"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function createschedule($page = 'createschedule') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('jadwal_model');
		$query=$this->jadwal_model->get_doctor_name();
		$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
		$data['doctor_name']=$doctor_name;
		$data['query']=null;
		$data['title'] = "Buat Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	// TRASH
	public function submit_schedule($page = 'schedulesuccess'){
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		$data = $this->pegawai_model->getprofilepegawai($this->input->post('nama'));
		foreach ($data as $row) {
			$peran = $row->peran;
		}
		$klinik = substr($peran, 7);
		$data_schedule = array(
					'hari' => $this->input->post('hari'),		
					'waktuawal' => $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00",
					'waktuakhir' => $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00",
					'id_pegawai' => $this->input->post('nama'),
					'klinik' => $klinik
		);
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'nama dokter', 'required');
		$this->form_validation->set_rules('hari', 'hari praktik', 'required');
		$this->form_validation->set_rules('jamMulai', 'jam mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'menit mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'jam selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'menit selesai', 'required');
		
		$this->load->model('jadwal_model');
		$data['query']=null;
		if ($this->form_validation->run()==FALSE){
			$query=$this->jadwal_model->get_doctor_name();
			$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
			$data['doctor_name']=$doctor_name;
			
			$data['title'] = "Buat Jadwal Praktik Dokter"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/createschedule');
			$this->load->view('templates/footer', $data);
		} else{
			$cek= $this->jadwal_model->add_doctor_schedule($data_schedule);
			date_default_timezone_set('Asia/Jakarta');
			$today = getdate();
			$tanggal = date("Y-m-d");
			$jharian = array();
			if ($data_schedule['hari'] == $today['wday']) {
				$jharian = array(
						'tanggal' => $tanggal,
						'waktuaktualawal' => $data_schedule['waktuawal'],
						'waktuaktualakhir' => $data_schedule['waktuakhir'],
						'id_pegawai' => $data_schedule['id_pegawai'],
						'hari' => $data_schedule['hari'],
						'klinik' => $data_schedule['klinik']
					);
				$this->jadwal_model->add_harian($jharian);
			}
			if(is_string($cek)) {
				$data['query']=$cek;
			
				$query=$this->jadwal_model->get_doctor_name();
				$doctor_name=array();
				if ($query->num_rows()>0){
					foreach ($query->result() as $entry){ 
						$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
						$doctor_name[] = $temp;
					}
				}
				$data['title'] = "Buat Jadwal Praktik Dokter";
				$data['doctor_name']=$doctor_name;
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('site/createschedule', $data);
				$this->load->view('templates/footer', $data);
			} else {
			
				$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
				
				$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
				
				$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
				$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
				$data['title'] = "Buat Jadwal Praktik Dokter"; 
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('site/'.$page, $data);
				$this->load->view('templates/footer', $data);
			}
		}
	}

// TRASH
	public function schedulesuccess($page = 'schedulesuccess') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$this->load->model('jadwal_model');
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
		$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
		
		$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
		$data['title'] = "Buat Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
		//$this->load->view('usermenu/usermenu', $data);
		
		$this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	// TRASH
	function deleteschedule($jadwalmingguanid){
	$this->load->model('jadwal_model');
        
        $this->jadwal_model->delete_schedule($jadwalmingguanid);
        
		$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
		$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
		
		$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
		$data['msg'] = NULL;
		$data['title'] = "Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
		
		$this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/deletesuccess', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('templates/footer', $data);
		        
    }
	
	// TRASH
	function deletesuccess($page = 'deletesuccess'){
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('jadwal_model');
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
		$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
		
		$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
		$data['title'] = "Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
		
		$this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	// TRASH
	function updatesuccess($page = 'updatesuccess'){
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('jadwal_model');
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
		$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
		
		$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
		$data['title'] = "Jadwal Praktik Dokter"; 
		$this->load->view('templates/header', $data);
		
		$this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function editschedule($jadwalmingguanid) {
		if ( ! file_exists('application/views/site/editschedule.php')) {
					show_404();
		}
		
		$this->load->model('jadwal_model');
		$query=$this->jadwal_model->get_doctor_name();
		$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
		$data['doctor_name']=$doctor_name;
		$data['sql']=null;
		$data['title'] = "Edit Jadwal Dokter"; 
		$data['row'] = $this->jadwal_model->getscheduleupdate($jadwalmingguanid);
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/editschedule', $data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	function updateschedule($jadwalmingguanid)
	{
		$data_schedule = array(
					'hari' => $this->input->post('hari'),		
					'waktuawal' => $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00",
					'waktuakhir' => $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00",
					'id_pegawai' => $this->input->post('nama')
		);
		
		$this->form_validation->set_message('required', 'Field %s harus diisi');
		$this->form_validation->set_rules('nama', 'Nama Dokter', 'required');
		$this->form_validation->set_rules('hari', 'Hari Praktik', 'required');
		$this->form_validation->set_rules('jamMulai', 'Jam Mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'Menit Mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'Jam Selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'Menit Selesai', 'required');
		
		$this->load->model('jadwal_model');
		$data['row'] = $this->jadwal_model->getscheduleupdate($jadwalmingguanid);
		$data['sql']=null;
		if ($this->form_validation->run()==FALSE){
		
			$query=$this->jadwal_model->get_doctor_name();
			$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
			$data['doctor_name']=$doctor_name;
			$data['title'] = "Buat Jadwal Praktik Dokter"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/editschedule');
			$this->load->view('templates/footer', $data);
		}
		else{
		$cek=$this->jadwal_model->update_schedule($data_schedule, $jadwalmingguanid);
			if(is_string($cek)){
				$data['sql']=$cek;
			
			$query=$this->jadwal_model->get_doctor_name();
			$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
			
			$data['doctor_name']=$doctor_name;
			$data['title'] = "Ubah Jadwal Praktik Dokter";
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/editschedule', $data);
			$this->load->view('templates/footer', $data);
			}
			
			$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
			$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
			
			$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		$data['is_kosong'] = $this->jadwal_model->is_harian_kosong();
			$data['title'] = "Jadwal Praktik Dokter"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/updatesuccess', $data);
			$this->load->view('templates/footer', $data);
		}
		
	}
// TRASH
	public function editjadwal($id) {
		if ( ! file_exists('application/views/site/editjadwal.php')) {
					show_404();
		}
		
		$this->load->model('jadwal_model');
		$query=$this->jadwal_model->get_doctor_name();
		$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
		date_default_timezone_set('Asia/Jakarta');
        	$datestring = "%Y-%m-%d";
        	$time = time();
        	$tanggal = mdate($datestring, $time);
		$data['doctor_name']=$doctor_name;
		$data['jadwalid'] = $id;
		$data['tanggal'] = $tanggal;
		$data['row'] = $this->jadwal_model->get_harian($id, $tanggal);
		$data['sql']=null;
		$data['title'] = "Edit Jadwal Dokter Harian";
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/editjadwal', $data);
		$this->load->view('templates/footer', $data);
	}
		// TRASH
	function update_harian($jadwal)
	{
		$data_schedule = array(		
					'waktuaktualawal' => $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00",
					'waktuaktualakhir' => $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00",
					'id_pegawai' => $this->input->post('nama')
		);
		date_default_timezone_set('Asia/Jakarta');
        	$datestring = "%Y-%m-%d";
        	$time = time();
        	$tanggal = mdate($datestring, $time);
		$this->form_validation->set_message('required', 'Field %s harus diisi');
		$this->form_validation->set_rules('nama', 'Nama Dokter', 'required');
		$this->form_validation->set_rules('jamMulai', 'Jam Mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'Menit Mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'Jam Selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'Menit Selesai', 'required');
		$this->load->model('jadwal_model');
		$data['row'] = $this->jadwal_model->get_harian($jadwal, $tanggal);
		$data['sql']=null;
		if ($this->form_validation->run()==FALSE){
		
			$query=$this->jadwal_model->get_doctor_name();
			$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
			$data['doctor_name']=$doctor_name;
			$data['title'] = "Buat Jadwal Praktik Dokter"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/editjadwal');
			$this->load->view('templates/footer', $data);
		}
		else{
		$cek=$this->jadwal_model->update_harian($data_schedule, $jadwal, $tanggal);
			if(is_string($cek)){
				$data['sql']=$cek;
			
			$query=$this->jadwal_model->get_doctor_name();
			$doctor_name=array();
			if ($query->num_rows()>0){
				foreach ($query->result() as $entry){ 
					$temp = array("id"=>$entry->id, "nama"=>$entry->nama);
					$doctor_name[] = $temp;
				}
			}
			
			$data['doctor_name']=$doctor_name;
			$data['title'] = "Ubah Jadwal Praktik Dokter";
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/editjadwal', $data);
			$this->load->view('templates/footer', $data);
			}
			redirect('site/antre');
			/*
			$data['doctor_table_umum'] = $this->jadwal_model->get_doctor_table('Umum');
		
			$data['doctor_table_gigi'] = $this->jadwal_model->get_doctor_table('Gigi');
			
			$data['doctor_table_medis'] = $this->jadwal_model->get_doctor_table('Estetika Medis');
		
			$data['title'] = "Jadwal Praktik Dokter"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/antre', $data);
			$this->load->view('templates/footer', $data);
			*/
		}
		
	}
	// TRASH
	public function registrasipegawai($page = 'registrasipegawai') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Form Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	public function registrasipegawaisuccess($page = 'registrasipegawaisuccess') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Profil Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	public function pegawailist($page = 'pegawailist') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('pegawai_model');
		$data['row'] = $this->pegawai_model->daftarpegawaimodel();
		
		$data['title'] = "Daftar Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page,$data);
		//$this->load->view('site/pegawailist',$data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	public function profilepasien($id) {
		$page = 'profilepasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket() && $this->user_model->get_my_id() != $id) {

			show_404();
		}
		else {
			$this->load->model('user_model');
		 	
		    $data['query'] = $this->user_model->get_userdata($id);
		    $data['show_button'] = true;
		    $data['show_button_verifikasi'] = false;
			$data['title'] = "Profil Pasien"; 
			$data['msg'] = null;
			$data['role'] = $this->user_model->get_user_role($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}
	// TRASH
	public function profilepengguna($id) {
		$page = 'profilepengguna';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}
		if( ! $this->authentication->is_loket() && $this->user_model->get_my_id() != $id) {
			show_404();
		}
		else {
		 	
	    $data['query'] = $this->user_model->get_user_by_id($id);
	    $data['show_button'] = true;
	    $data['show_button_verifikasi'] = false;
			$data['title'] = "Profil Pasien"; 
			$data['msg'] = null;
			$data['role'] = $this->user_model->get_user_role_name_by_id($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);

		}
	}
	// TRASH
	public function profilepegawai($id) {
		
		if ( ! file_exists('application/views/site/profilepegawai.php')) {
					show_404();
		}
		$this->load->model('pegawai_model');
		$data['row'] = $this->pegawai_model->getprofilepegawai($id);
		$data['title'] = "Profil Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/profilepegawai.php', $data);
		$this->load->view('templates/footer', $data);
	}
	// TRASH
	public function readarticle($id) {
		if ( ! file_exists('application/views/site/readarticle.php')) {
					show_404();
		}
		$data['title'] = "Artikel"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else {
		$this->load->view('usermenu/loginform', $data);}
		$this->load->model('artikel_model');
		$data['row']= $this->artikel_model->read_artikel($id);
		$this->load->view('site/readarticle', $data);
		$this->load->view('templates/footer', $data);
	}

/*
	public function responkepuasan($page = 'responkepuasan') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Respon Kepuasan"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	} 

	public function responkepuasansukses($page = 'responkepuasansukses') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Respon Kepuasan"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
*/
	
	public function responkepuasan() {
		$page = 'responkepuasan';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$user_id = $this->authentication->get_my_id('id');
		$this->load->model('respons');
		$data['title'] = "Respon Kepuasan";
		$data['row']= $this->respons->get_data($user_id);
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/responkepuasan', $data);
		$this->load->view('templates/footer', $data);
	}

	public function responkepuasansukses($id) {
		$page = 'responkepuasansukses';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('respons');

		$datarespon = $this->input->post('respons');
		$this->respons->set_respons($datarespon, $id);
		$this->responkepuasan();
	
	}
	
	public function responsukses ($user_id){
		$data['row']= $this->respons->get_data($user_id)->result();
		
		$data['title'] = "Respon Kepuasan"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/responkepuasansukses', $data);
		$this->load->view('templates/footer', $data);
	}

	public function verifikasiakun() {
		$page = 'verifikasiakun';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket()) {
			show_404();
			//show_error('Anda tidak punya hak akses');
		}
		else {


			$this->load->model('calon_pasien');
			$all = $this->calon_pasien->get_calon_pasien();

			$config['base_url'] = site_url('/site/verifikasiakun');
			$config['total_rows'] = $all->num_rows();
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	        $data["query"] = $this->calon_pasien->get_calon_pasien_limited($config["per_page"], $page_num)->result();

			$data['nav'] = $this->pagination->create_links();

		 
		    $data['msg'] = "";
			$data['title'] = "Verifikasi Akun"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	// TRASH
	public function verifikasiakunsukses() {

		$page = "verifikasiakun";


		$this->load->model('calon_pasien');
		$all = $this->calon_pasien->get_calon_pasien();

		$config['base_url'] = site_url('/site/verifikasiakun');
		$config['total_rows'] = $all->num_rows();
		$config['per_page'] = 15;
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;
		$config['first_link'] = 'Pertama';
		$config['last_link'] = 'Terakhir';
		$config['next_link'] = 'Berikutnya';
		$config['prev_link'] = 'Sebelumnya';

		$page_num = (($this->uri->segment(3)) < 99999) ? $this->uri->segment(3) : 0;
		$data['page_num'] = $page_num;

		$this->pagination->initialize($config);

        $data["query"] = $this->calon_pasien->get_calon_pasien_limited($config["per_page"], $page_num)->result();

		$data['nav'] = $this->pagination->create_links();

	    $data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-green\">
                      <p class=\"p_message\">Calon pasien berhasil diverifikasi! </p>
                    </div>
                  </div>";

		$data['title'] = "Verifikasi Akun"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function verifikasipasien($page = 'verifikasipasien') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Respon Kepuasan"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	// TRASH
	public function laporan($page = 'laporan') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Laporan"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

		// TRASH
    public function editpasien($id) {

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		
		$this->form_validation->set_message('required', '%s wajib diisi');
		$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('is_natural', '%s harus angka');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		$role = $this->user_model->get_user_role($id);

		if ($role == "Masyarakat Umum") {
			//$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|max_length[20]|callback_char_space_only');
			$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required|max_length[25]|is_natural');

		}

		if ($role == "Karyawan") {

			$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|required|max_length[30]|callback_char_space_only');
		}

		if ($role == "Mahasiswa") {

			$this->form_validation->set_rules('npm', 'NPM', 'required|is_natural|exact_length[10]');
			$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
			$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required|max_length[50]|callback_char_space_only');
			$this->form_validation->set_rules('semester', 'Semester', 'required|is_natural_no_zero|max_length[2]');
		}

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_space_only');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'required|max_length[30]|callback_char_space_only');
		$this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');

		// if ($this->form_validation->run() == FALSE)
		// {
		// 	$this->editdatapasien($id);
		// }
		// else
		// {
		// 	$this->update_data_pasien($id);
		// 	//$this->registrasisuccess();
		// }
	   if ($this->form_validation->run() == TRUE)
	   {

		    $config = array(
		     'allowed_types' => 'jpg|jpeg|png|gif',
		     'upload_path' => './foto/pasien/',//base_url('foto'),
		     'max_size' => 1024,
		     'max_height' => 600,
		     'max_width' => 400,
		     'file_name' => $id.".jpg",
		     'overwrite' => TRUE
		     //'remove_spaces' => TRUE
		    );

		    $this->load->helper('file');
		    $this->load->library('upload', $config);

		    if ($_FILES && $_FILES['foto']['name'] !== "") {
	  		// Upload code here


		       if ($this->upload->do_upload('foto'))
		       {
		          //$this->Insert->add_file();
		          //$this->session->set_flashdata('file_uploaded', TRUE);
		          //$data['fuploaded'] = $this->session->flashdata('file_uploaded');
		          //$this->load->view('upload/scheme', $data);
		          //echo "berhasil";

					$this->update_data_pasien($id);
		       }
		       else
		       {
		          //$data['uploaderror'] = $this->upload->display_errors(); // <- this line is the solution for my second problem
		          //$this->load->view('upload/scheme', $data);
		          //echo "gagal upload";
		          //echo $data['uploaderror'];


					// $this->editdatapasien($id);
					
					
					$page = 'editdatapasien';
					if ( ! file_exists('application/views/site/'.$page.'.php')) {
								show_404();
					}
					if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
						show_404();
					}
					else {
						$this->load->model('user_model');
					    $data['query'] = $this->user_model->get_userdata($id);

						$data['title'] = "Edit Profil"; 
						$data['msg'] = null;
						$data['role'] = $this->user_model->get_user_role($id);
						$data['id'] = $id;

						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('site/'.$page, $data);
						$this->load->view('templates/footer', $data);
					}
		       }
		    }

		    else {

				$this->update_data_pasien($id);
		    }

	   	}

		else
		{
		//$this->load->view('upload/scheme', $data);
		$this->editdatapasien($id);

		} 	
    }
	public function editdatapasien($id) {
		$page = 'editdatapasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		else {
			$this->load->model('user_model');
		    $data['query'] = $this->user_model->get_userdata($id);

			$data['title'] = "Edit Profil"; 
			$data['msg'] = null;
			$data['role'] = $this->user_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function editpasiensukses($id) {
		$page = 'profilepasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			show_404();
		}
		else {
			$this->load->model('user_model');
		 	
		    $data['query'] = $this->user_model->get_userdata($id);
		    $data['show_button'] = true;	 
			$data['title'] = "Profil Pasien";
			$data['msg2'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Data berhasil disimpan! </p>
                    </div>
                  </div>";
			$data['role'] = $this->user_model->get_user_role($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}



    public function editpassword($id) {
	    
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		if ($this->form_validation->run() == TRUE)
		{
			$this->savenewpassword($id);
		}
		else
		{
			$this->formeditpassword($id);
		}	
	       
    }

	public function formeditpassword($id) {
		$page = 'formeditpassword';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}

		$this->load->model('user_model');

		if(!$this->authentication->is_loket()) {
		
			if($this->user_model->get_user_role($id) != "Masyarakat Umum") {
				show_404();
			}
			
			elseif($this->user_model->get_my_id() != $id) {
				show_404();
			}
			
		}

		
		    $data['title'] = "Edit Password"; 
			$data['msg'] = "";
			$data['role'] = $this->user_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		
	}

	public function savenewpassword($id)
	{
		$page = "sukses";
		$this->load->model('form_model');
		$this->form_model->updatepassword($id);
		

		$this->load->model('user_model');

		if(!$this->authentication->is_loket() && $this->user_model->get_my_id() != $id && $this->user_model->get_user_role($id) != "Masyarakat Umum") {
			show_404();
		}
		
		$data['title'] = "Edit Password Sukses"; 
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-green\">
                      <p class=\"p_message\">Password untuk ".$id." - ".$this->user_model->get_name($id)." berhasil diperbarui </p>
                    </div>
                  </div>";

			$data['role'] = $this->user_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
	}
	
	public function editpegawai($id) {
		if ( ! file_exists('application/views/site/editpegawai.php')) {
					show_404();
		}
		$data['title'] = "Edit Profil"; 
		$this->load->model('pegawai_model');
		$data['row'] = $this->pegawai_model->getprofilepegawai($id);
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/editpegawai', $data);
		$this->load->view('templates/footer', $data);
	}

	public function editpegawaisukses($page = 'editpegawaisukses') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Edit Profil"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function kritiksaran($page = 'kritiksaran') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_pasien()) redirect('site');

		$data['title'] = "Kritik dan Saran"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function kritiksaran_add(){
		date_default_timezone_set('Asia/Jakarta');
		$datestring = "%Y %m %d - %h:%i:%s %a";
		$time = time();
		
		$date = mdate($datestring, $time);
		
		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('no_script', '%s mengandung karakter yang dilarang');

		$this->form_validation->set_rules('teks','Isi','required|xss_clean');

		$this->load->model('kritiksaran_model');
		if ($this->form_validation->run()==FALSE){
			$data['title'] = "Kritik dan Saran"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/kritiksaran', $data);
			$this->load->view('templates/footer', $data);
		}
		else{
			$user_id = $this->authentication->get_my_id();
			$data = array(
					'isi' => $this->input->post('teks'),
					'waktu'=> $date,
					'id_pasien' => $user_id,
			);
			$this->kritiksaran_model->add_kritiksaran($data);
			$data['title'] = "Kritik dan Saran"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/kritiksaransukses', $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function kritiksaransukses($page = 'kritiksaransukses') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Kritik dan Saran"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function kritiksaranlist() {
		
		$this->load->model('kritiksaran_model');
		$all = $this->kritiksaran_model->get_kritiksaran();
		//pagination
			$data['all'] = $all->result();
			$config['base_url'] = site_url('/site/kritiksaranlist');
				$config['total_rows'] = $all->num_rows();
				$config['per_page'] = 15;
				$config['uri_segment'] = 3;
				$config['num_links'] = 5;
				$config['first_link'] = 'Pertama';
				$config['last_link'] = 'Terakhir';
				$config['next_link'] = 'Berikutnya';
				$config['prev_link'] = 'Sebelumnya';
				$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['page_num'] = $page_num;
				$this->pagination->initialize($config);
				$data["row"] = $this->kritiksaran_model->get_kritiksaran_limited($config["per_page"], $page_num)->result();

				$data['nav'] = $this->pagination->create_links();
			//-end-
		$data['msg'] = "";
		$data['title'] = "Daftar Kritik dan Saran"; 
		$this->load->view('templates/header', $data);
		if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/kritiksaranlist', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function delete_kritiksaran($id_kritik){
		$this->load->model('kritiksaran_model');
        
        $this->kritiksaran_model->delete_kritiksaran($id_kritik);
		$data['row'] = $this->kritiksaran_model->get_kritiksaran();
		$data['title'] = "Hapus Kritik Saran"; 
		$this->load->view('templates/header', $data);
		
		$this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/deletekritiksaran', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function deletekritiksaran($page = 'deletekritiksaran'){
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->load->model('kritiksaran_model');
		$data['row'] = $this->kritiksaran_model->get_kritiksaran();
		$data['title'] = "Daftar Kritik dan Saran"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function textarea($page = 'textarea') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Daftar Kritik dan Saran"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	public function add_pegawai (){
		// validasi input liat library CI
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nama', 'min_length[1]|max_length[32]');
		$this->form_validation->set_rules('nama','Nama Lengkap', 'required');
		$this->form_validation->set_rules('alamat','Alamat', 'required');
		$this->form_validation->set_rules('tempatLahir','Tempat,Tanggal Lahir', 'required');
		$this->form_validation->set_rules('tgl','Tempat,Tanggal Lahir', 'required');
		$this->form_validation->set_rules('bulan','Tempat,Tanggal Lahir','required');
		$this->form_validation->set_rules('tahun','Tempat,Tanggal Lahir','required');
		$this->form_validation->set_rules('sex','Jenis Kelamin', 'required');
		$this->form_validation->set_rules('agama','Agama', 'required');
		$this->form_validation->set_rules('status','Status Pernikahan', 'required');
		$this->form_validation->set_rules('kewarga','Kewarganegaraan', 'required');
		$this->form_validation->set_rules('golDarah','Golongan Darah');
		$this->form_validation->set_rules ('peran','Peran', 'required');
		
		//cek apakah validasi bernilai true
		if ($this->form_validation->run() == TRUE){
		$tanggal=array(
			'tgl'=> $this->input->post('tgl'),
			'bulan'=>$this->input->post('bulan'),
			'tahun'=>$this->input->post('tahun')
			);
		$date = $tanggal['tgl']."-".$tanggal['bulan']."-".$tanggal['tahun'];
		$random = random_string('numeric',6);
		 $data=array(
			'id'=> $random,
			'username' =>$this->input->post('nama'),
			'password_pegawai' => random_string('alnum',10),
			'nama' =>$this->input->post('nama'),
			'jeniskelamin'=>$this->input->post('sex'),
			'goldarah'=>$this->input->post('golDarah'),
			'kewarganegaraan'=>$this->input->post('kewarga'),
			'tempatlahir'=>$this->input->post('tempatLahir'),
			'tanggallahir'=>$date,
			'agama'=>$this->input->post('agama'),
			'alamat'=>$this->input->post('alamat'),
			'statuspernikahan'=>$this->input->post('status'),
			'peran'=>$this->input->post('peran'),
			'no_telepon'=>$this->input->post('notelepon')
			);
			
			$this->load->model('pegawai_model');
			$this->pegawai_model->insert($data);
			$data['title'] = "Registrasi Pasien Sukses"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/registrasipegawaisuccess', $data);
			$this->load->view('templates/footer', $data);
			
		}
		else{
			$data['title'] = "Registrasi Pasien"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/registrasipegawai', $data);
			$this->load->view('templates/footer', $data);
		}
	}
	
	public function updatepegawai($id){
	$tanggal=array (
			'tgl'=> $this->input->post('tgl'),
			'bulan'=>$this->input->post('bulan'),
			'tahun'=>$this->input->post('tahun')
			);
	$date = $tanggal['tgl']."-".$tanggal['bulan']."-".$tanggal['tahun'];
	
	$data=array(
			
			'username' =>$this->input->post('username'),
			'password_pegawai' =>$this->input->post('password'),
			'nama' =>$this->input->post('nama'),
			'jeniskelamin'=>$this->input->post('sex'),
			'goldarah'=>$this->input->post('golDarah'),
			'kewarganegaraan'=>$this->input->post('kewarga'),
			'tempatlahir'=>$this->input->post('tempatLahir'),
			'tanggallahir'=>$date,
			'agama'=>$this->input->post('agama'),
			'alamat'=>$this->input->post('alamat'),
			'statuspernikahan'=>$this->input->post('status'),
			
			'no_telepon'=>$this->input->post('no_telepon')
			);
			
			//echo $data['username'];
			$this->load->model('pegawai_model');
			$this->pegawai_model->updatepegawai($id,$data);
			
			$data['title'] = "Perubahan Data Pasien Sukses"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/editpegawaisukses', $data);
			$this->load->view('templates/footer', $data);
	}
	
	public function deletepegawai($id){
		$this->load->model('pegawai_model');
		$data_nama ['nama'] =$this->pegawai_model->getnama($id);
		$this->pegawai_model->deletepegawai($id);
		$data['row'] = $this->pegawai_model->daftarpegawaimodel();
		$data['title'] = "Daftar Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/deletepegawai', $data_nama,$data);
		$this->load->view('templates/footer', $data);
		
	}
	
	public function add_article(){
	
		$datestring = "%Y %m %d - %h:%i %a";
		$time = time();
		$date = mdate($datestring, $time);
		//validasi
		$this->load->library('form_validation');
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('tag','Kata Kunci', 'required');
		$this->form_validation->set_rules('teks','Isi', 'required');
		
		if ($this->form_validation->run() == TRUE){
			$data= array(
			'isi' =>$this->input->post('teks'),
			'katakunci'=>$this->input->post('tag'),
			'judul' =>$this->input->post('judul'),
			'timestamp' => $date,
			'status'=>$this->input->post('status'));
			
		
			$this->load->model('artikel_model');
			$this->artikel_model->add_artikel($data);
			$data['title'] = "Buat Artikel";
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			
			if ($data['status']=="Terbitkan sekarang"){
				$this->load->view('site/addarticlesuccess', $data);
				$this->artikel_model->set_status($data['timestamp'],"sudah terbit" );
			}
			if ($data['status']=="Simpan sebagai draft"){
				$this->load->view('site/articlelist');
				$this->artikel_model->set_status($data['timestamp'], "draft");
			}
			$this->load->view('templates/footer', $data);
		} 
		else{
			$data['title'] = "Buat Artikel"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/article', $data);
			$this->load->view('templates/footer', $data);
		}
	}



    public function editcalonpasien($id) {

		$this->load->helper(array('form', 'url'));
		$this->load->model('user_model');
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		
		$this->form_validation->set_message('required', '%s wajib diisi');
		$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('is_natural', '%s harus angka');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		$role = $this->user_model->get_calon_pasien_role($id);

		if ($role == "Masyarakat Umum") {
			$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|max_length[20]|callback_char_space_only');
			$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required|max_length[25]|is_natural');

		}

		if ($role == "Karyawan") {

			$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|required|max_length[30]|callback_char_space_only');
		}

		if ($role == "Mahasiswa") {

			$this->form_validation->set_rules('npm', 'NPM', 'required|is_natural|exact_length[10]');
			$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
			$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required|max_length[50]|callback_char_space_only');
			$this->form_validation->set_rules('semester', 'Semester', 'required|is_natural_no_zero|max_length[2]');
		}

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_space_only');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'trim|required|max_length[30]|callback_char_space_only');
		$this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');

		// if ($this->form_validation->run() == FALSE)
		// {
		// 	$this->editdatacalonpasien($id);
		// }
		// else
		// {
		// 	$this->update_data_calon_pasien($id);
		// 	//$this->registrasisuccess();
		// }
		 
		if ($this->form_validation->run() == TRUE)
	   {

		    $config = array(
		     'allowed_types' => 'jpg|jpeg|png|gif',
		     'upload_path' => './foto/pasien/',//base_url('foto'),
		     'max_size' => 1024,
		     'max_height' => 600,
		     'max_width' => 400,
		     'file_name' => $id.".jpg",
		     'overwrite' => TRUE
		     //'remove_spaces' => TRUE
		    );

		    $this->load->helper('file');
		    $this->load->library('upload', $config);

		    if ($_FILES && $_FILES['foto']['name'] !== "") {
	  		// Upload code here


		       if ($this->upload->do_upload('foto'))
		       {
		          //$this->Insert->add_file();
		          //$this->session->set_flashdata('file_uploaded', TRUE);
		          //$data['fuploaded'] = $this->session->flashdata('file_uploaded');
		          //$this->load->view('upload/scheme', $data);
		          //echo "berhasil";

					$this->update_data_calon_pasien($id);
		       }
		       else
		       {
		          //$data['uploaderror'] = $this->upload->display_errors(); // <- this line is the solution for my second problem
		          //$this->load->view('upload/scheme', $data);
		          //echo "gagal upload";
		          //echo $data['uploaderror'];

				  //$this->editdatacalonpasien($id);
				  
				  
					$page = 'editdatacalonpasien';
					if ( ! file_exists('application/views/site/'.$page.'.php')) {
								show_404();
					}
					
					else {
						$this->load->model('user_model');
					    $data['query'] = $this->user_model->get_calon_pasien_data($id);

						$data['title'] = "Edit Profil";
						$data['msg'] = "
			                  <div class=\"row\">
			                    <div class=\"message message-red\">
			                      <p class=\"p_message\">Format foto salah.</p>
			                      <p class=\"p_message\">Tipe: jpg, jpeg, png, gif.<br/>
			                      	Dimensi maksimal (P x L) : 400 x 600 px<br/>
			                      	Ukuran file maksimal.1024 kB</p>
			                    </div>
			                  </div>";
						$data['role'] = $this->user_model->get_calon_pasien_role($id);
						$data['id'] = $id;

						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('site/'.$page, $data);
						$this->load->view('templates/footer', $data);
					}
		       }
		    }

		    else {
					$this->update_data_calon_pasien($id);
		    }

	   	}

		else
		{
		//$this->load->view('upload/scheme', $data);
		$this->editdatacalonpasien($id);

		}
	
    }

	public function editdatacalonpasien($id) {
		$page = 'editdatacalonpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		else {
			$this->load->model('user_model');
		    $data['query'] = $this->user_model->get_calon_pasien_data($id);

			$data['title'] = "Edit Profil"; 
			$data['msg'] = null;
			$data['role'] = $this->user_model->get_calon_pasien_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

    public function update_data_calon_pasien($id){
        $this->load->model('form_model');
        $this->load->model('user_model');

		$my_id = $this->user_model->get_my_id();

        $success = $this->form_model->updatecalonpasien($id);
        if ($success) {
        	$this->editcalonpasiensukses($id);
        }
        else $this->index();
    }

	public function editcalonpasiensukses($id) {
		$page = 'profilecalonpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		else {
			$this->load->model('user_model');
		 	
		    $data['query'] = $this->user_model->get_calon_pasien_data($id);
		    $data['show_button'] = true;	 
			$data['title'] = "Profil Calon Pasien";
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Data berhasil disimpan! </p>
                    </div>
                  </div>";
			$data['role'] = $this->user_model->get_calon_pasien_role($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function daftarpasien() {
		$page = 'daftarpasien';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_loket()) {
			//show_404();
			show_404();
		}
		else {
			$this->load->model('pasien');
			$all = $this->pasien->get_all_pasien();
		    $data['all'] = $all->result();

			$config['base_url'] = site_url('/site/daftarpasien');
			$config['total_rows'] = $all->num_rows();
			$config['per_page'] = 10;
			$config['uri_segment'] = 3;
			$config['num_links'] = 5;
			$config['first_link'] = 'Pertama';
			$config['last_link'] = 'Terakhir';
			$config['next_link'] = 'Berikutnya';
			$config['prev_link'] = 'Sebelumnya';

			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['page_num'] = $page_num;

			$this->pagination->initialize($config);

	        $data["query"] = $this->pasien->get_all_pasien_limited($config["per_page"], $page_num)->result();

			$data['nav'] = $this->pagination->create_links();
		 
		    $data['msg'] = "";
			$data['title'] = "Daftar Pasien"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

	

	public function char_dash_only($str) {

	    if (! preg_match("/^([-a-z .,'])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_dash_num_only($str) {

	    if (! preg_match("/^([-a-z .,0-9\/])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_space_only($str) {

	    if (! preg_match("/^([a-z ])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function execute_search(){
	$this->load->model('Search_model');
		$search_term = $this->input->post('search');
		if($this->authentication->is_pelayanan()) {
			$all = $this->Search_model->get_results($search_term);
			
			//pagination
			$data['all'] = $all->result();
			$config['base_url'] = site_url('/site/execute_search');
				$config['total_rows'] = $all->num_rows();
				$config['per_page'] = 15;
				$config['uri_segment'] = 3;
				$config['num_links'] = 5;
				$config['first_link'] = 'Pertama';
				$config['last_link'] = 'Terakhir';
				$config['next_link'] = 'Berikutnya';
				$config['prev_link'] = 'Sebelumnya';
			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['page_num'] = $page_num;
				$this->pagination->initialize($config);
				$data["results"] = $this->Search_model->get_results_limited($search_term, $config["per_page"], $page_num)->result();

				$data['nav'] = $this->pagination->create_links();
			//-end-
			
			$data['hasil']=0;
			if(($all->num_rows()) == 0){
				$data['hasil']=1;
				
				$data['message']="Hasil tidak dapat ditemukan.";
			}
			
			$data['title'] = "Hasil Pencarian"; 
			$this->load->view('templates/header', $data);
			
			if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
			
			$this->load->view('site/pencarian', $data);
			$this->load->view('templates/footer', $data);
		}
		
		else if($this->authentication->is_loket() || $this->authentication->is_perawat()) {
			
			$all2 = $this->Search_model->get_results_pasien($search_term);
			
			//pagination
			$data['all2'] = $all2->result();
			$config['base_url'] = site_url('/site/execute_search');
				$config['total_rows'] = $all2->num_rows();
				$config['per_page'] = 15;
				$config['uri_segment'] = 3;
				$config['num_links'] = 5;
				$config['first_link'] = 'Pertama';
				$config['last_link'] = 'Terakhir';
				$config['next_link'] = 'Berikutnya';
				$config['prev_link'] = 'Sebelumnya';
			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['page_num'] = $page_num;
				$this->pagination->initialize($config);
				$data["resultspasien"] = $this->Search_model->get_results_pasien_limited($search_term, $config["per_page"], $page_num)->result();

				$data['nav'] = $this->pagination->create_links();
			//-end-
			
			
			$data['hasilpasien']=0;
			if(($all2->num_rows()) == 0){
				
				$data['hasilpasien']=1;
				$data['message']="Hasil tidak dapat ditemukan.";
			}
			
			$data['title'] = "Hasil Pencarian"; 
			$this->load->view('templates/header', $data);
			
			if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
			
			$this->load->view('site/pencarian', $data);
			$this->load->view('templates/footer', $data); 
		}
		
		else{
			$all3 = $this->Search_model->get_results_article($search_term);
			
			//pagination
			$data['all3'] = $all3->result();
			$config['base_url'] = site_url('/site/execute_search');
				$config['total_rows'] = $all3->num_rows();
				$config['per_page'] = 15;
				$config['uri_segment'] = 3;
				$config['num_links'] = 5;
				$config['first_link'] = 'Pertama';
				$config['last_link'] = 'Terakhir';
				$config['next_link'] = 'Berikutnya';
				$config['prev_link'] = 'Sebelumnya';
			$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['page_num'] = $page_num;
				$this->pagination->initialize($config);
				$data["resultsarticle"] = $this->Search_model->get_results_article_limited($search_term, $config["per_page"], $page_num)->result();

				$data['nav'] = $this->pagination->create_links();
			//-end-
			
			
			$data['hasilartikel']=0;
			if(($all3->num_rows()) == 0){
				
				$data['hasilartikel']=1;
				$data['message']="Hasil tidak dapat ditemukan.";
			}
			
			$data['title'] = "Hasil Pencarian"; 
			$this->load->view('templates/header', $data);
			
			if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
			
			$this->load->view('site/pencarian', $data);
			$this->load->view('templates/footer', $data);
		}
	}
	
	public function pencarian($page = 'pencarian') {
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Hasil Pencarian"; 
		$this->load->view('templates/header', $data);
        
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function antrean_sekarang() {
		$this->load->view('templates/antrean_sekarang');
	}


	public function statusantreanpoli() {        

        //load model
        $jmllll = $this->antrian_model->get_total_antrian(); // ambil fungsi di model
        $jml = $jmllll[0]+$jmllll[1];
    	$data = array(
    		'clicked' => false,
    		'flush' => false,
    	);

    	$this->session->set_userdata($data);

		$jml_last = $this->session->userdata('jml_last');
		$baru = $jml-$jml_last;

		if ($baru < 0) {
			$data = array(
	    		'jml_last' => $jml,
	    	);
	    	$this->session->set_userdata($data);
		}
  		// echo $jml;
		// echo $jml_last;
		// echo $baru;

        if (!$this->session->userdata('clicked')) 
        {
        	if ($baru > 0) 
        	{
        		$hidelink = false; 
        	}

        	else $hidelink = true;
        }

        elseif (!$this->session->userdata('flush')) 
        {

         if ($baru > 0) 
    	{
    		$hidelink = false; 
    	}

    	else $hidelink = true;

    	}

    	else $hidelink = true;
    	
        $data['jml'] = $baru;
        $data['hidelink'] = $hidelink;

        $this->load->view('templates/status_antrean_poli',$data);
    }

    

    public function infonotifikasi()
    {
    	    
        			//load model
        
        $jmllll = $this->antrian_model->get_total_antrian(); // ambil fungsi di model
        $jml = $jmllll[0]+$jmllll[1];
    	$data = array(
    		'clicked' => false,
    		'flush' => false,
    	);

    	$this->session->set_userdata($data);

		$jml_last = $this->session->userdata('jml_last');
		$baru = $jml-$jml_last;

		if ($baru < 0) {
			$data = array(
	    		'jml_last' => $jml,
	    	);
	    	$this->session->set_userdata($data);
		}
  		// echo $jml;
		// echo $jml_last;
		// echo $baru;

        if (!$this->session->userdata('clicked')) 
        {
        	if ($baru > 0) 
        	{
        		$hidelink = false; 
        	}

        	else $hidelink = true;
        }

        elseif (!$this->session->userdata('flush')) 
        {

         if ($baru > 0) 
    	{
    		$hidelink = false; 
    	}

    	else $hidelink = true;

    	}

    	else $hidelink = true;
    	
        $data['jmlblm'] = $baru;
        $data['hidelink'] = $hidelink;

        $this->load->view('templates/infonotifikasi',$data);
    }

    public function b07()
    {
    	$this->load->view('site/b07');
    }

    public function sitrasiplus()
    {
    	$this->load->view('site/sitrasiplus');
    }

	public function no_script($str) {

	    if (! preg_match("/^([-!?()\"\'+=#@[]:;%a-z .,0-9\/])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	} 
}
