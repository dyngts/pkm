<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kasir extends CI_Controller {

	public $variable = array();

	public function __construct(){
		parent::__construct();
		$this->load->model('doktermod');
		$this->load->model('kasirmod');
		$this->load->model('authentication');
		$this->load->library('pagination');
		$this->load->helper('html');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		date_default_timezone_set("Asia/Jakarta");
	}

	function rolechecker(){
    	$role = $this->session->userdata('role');
		$access = 0;
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->authentication->is_adm_keu() || $this->authentication->is_kasir() ){
			$access = 1;
		}
        if ($access == 0) {
            show_404();
        }
        return $access;
    }

	public function masukkan_datakeuangan(){
		$this->rolechecker();
       	$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
    	$this->form_validation->set_rules('namaPasien', 'Nama Pasien', 'required|min_length[1]|max_length[50]' );
    	$this->form_validation->set_rules('jenisKelamin', 'Jenis Kelamin', 'required');
    	$this->form_validation->set_rules('umur', 'Umur', 'required');
    	$this->form_validation->set_rules('diagnosa', 'Diagnosa', 'required');
    	$this->form_validation->set_rules('tindakan', 'Tindakan', 'required');
    	$this->form_validation->set_rules('jasaDokter', 'Jasa Dokter', 'required');
    	$this->form_validation->set_rules('jasaTindakan', 'Jasa Tindakan', 'required');
    	$this->form_validation->set_rules('poliUmumNetto', 'Poli Umum Netto', 'required');
    	$this->form_validation->set_rules('poliUmumJP', 'Poli Umum JP', 'required');
    	$this->form_validation->set_rules('pendapatanPoliUmum', 'Pendapatan Poli Umum', 'required');
    	$this->form_validation->set_rules('dokterPemeriksa', 'Dokter Pemeriksa', 'required');

		$tanggal = $this->input->post('tanggal',true);
    	$nama_pasien = $this->input->post('namaPasien', true);
    	$jenis_kelamin = $this->input->post('jenisKelamin', true);
    	$umur = $this->input->post('umur',true);
    	$diagnosa = $this->input->post('diagnosa',true);
    	$tindakan = $this->input->post('tindakan', true);
    	$jasa_dokter = $this->input->post('jasaDokter', true);
    	$jasa_tindakan = $this->input->post('jasaTindakan', true);
    	$poli_umum_netto = $this->input->post('poliUmumNetto', true);
    	$poli_umum_JP = $this->input->post('poliUmumJP',true);
    	$pendapatan_poli_umum = $this->input->post('pendapatanPoliUmum',true);
    	$dokter_pemeriksa = $this->input->post('dokterPemeriksa',true);

    	$cek = $this->form_validation->run();
    	
    	if ($cek==TRUE) {
    		$data_salemba = array (
    			'tanggal' => $tanggal,
    			'nama_pasien'=> $nama_pasien,
    			'jenis_kelamin' => $jenis_kelamin,
    			'umur' => $umur,
    			'diagnosa' => $diagnosa,
    			'tindakan' => $tindakan,
    			'jasa_dokter' => $jasa_dokter,
    			'jasa_tindakan' => $jasa_tindakan,
    			'poli_umum_netto' => $poli_umum_netto,
    			'poli_umum_jp_pkm' => $poli_umum_JP,
    			'pendapatan_poli_umum' => $pendapatan_poli_umum,
    			'dokter_pemeriksa' => $dokter_pemeriksa
    			);

    		$data['title'] = 'Laporan Keuangan Salemba';
			$this->kasirmod->insert_salemba($data_salemba);
			redirect('kasir/notifikasi_isi');
    	}
    	else{
    		echo ("<script language ='JavaScript'>
			window.alert('Data yang anda masukkan belum benar!') 
			</script>");
			$this->buat_laporan_salemba();
    	}
        
	}

	public function notifikasi_isi(){
		$data['title'] = 'Laporan Berhasil Dimasukkan';
		$data['session'] = 'Masukkan Laporan Keuangan';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/notifikasi_isi');
		$this->load->view('templates/footer', $data);
	}

	public function daftar_pasien() {
		$this->rolechecker();
		$gettagihan = $this->kasirmod->gettagihan();
		$data['query'] = $gettagihan->result_array();
		$data['title'] = 'Daftar Pasien';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/daftar_pasien', $data);
		$this->load->view('templates/footer', $data);
	}

	public function rincian_pembayaran(){
		$nokuitansi = $this->kasirmod->getLatestNoKuitansi();
		$waktu = $this->input->post('waktu', true);
		$tanggal = $this->input->post('tanggal', true);
		$idrek = $this->input->post('idrekam', true);
		$datapasien = $this->kasirmod->getdatapasien($idrek);
		$biayatindakan = $this->kasirmod->getbiayatindakan($idrek, $tanggal, $waktu);
		$biayaobat = $this->kasirmod->getbiayaobat($idrek,$tanggal);
		/*==============Set Userdata buat download pdf =================*/
		$this->session->set_userdata('tanggal', $tanggal);
		$this->session->set_userdata('idrek', $idrek);
		$this->session->set_userdata('waktu', $waktu);
		/*==============================================================*/	
		$data['nokuitansi'] = $nokuitansi;
		$data['tanggal'] = $tanggal;
		$data['waktu'] = $waktu;
		$data['datapasien'] = $datapasien->row();
		$data['idrekam'] = $idrek;
		$data['biayatindakan'] = $biayatindakan->result_array();
		$data['biayaobat'] = $biayaobat->result_array();
		$data['title'] = 'Rincian Pembayaran';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/rincian', $data);
		$this->load->view('templates/footer', $data);
	}

	function konfirmasi_bayar(){
      	$databayar = array (
			'id_rekam_medis' => $this->input->post("idrekam", true),
			'tanggal' => $this->input->post("tanggal", true),
			'waktu_tagih' => $this->input->post("waktu", true),
			'waktu' => date("H:i:s"),
			'status_pembayaran' => $this->input->post("status", true),
			'jumlah_pembayaran' => $this->input->post('bayar',true),
			'username_keuangan' => $this->session->userdata('username')
		);
		$idresep = $this->input->post("idresep", true);
		$printflag = $this->kasirmod->bayar($databayar, $idresep);
		if($printflag) {
			redirect('kasir/notifikasi');
		}
	}

	public function laporan(){
		$data['title'] = 'Lihat Laporan';
		$data['css'] = 'css/style.css';
		$data['role'] = $this->session->userdata('role');
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data); 
		$this->load->view('kasir/laporan');
		$this->load->view('templates/footer', $data);		
	}

	public function notifikasi() {
    	$data['title'] = 'Penyimpanan Pembayaran Sukses';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/notifikasi');
		$this->load->view('templates/footer', $data);
	}

	public function notifikasi2() {
        $data['title'] = 'Print Sukses';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/notifikasi_2');
		$this->load->view('templates/footer', $data);	
	}

	public function buat_laporan(){
		$data['title'] = 'Buat Laporan Keuangan';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/buat_laporan');
		$this->load->view('templates/footer', $data);	
	}


	public function buat_laporan_salemba(){
		$data['title'] = 'Buat Laporan Keuangan Salemba';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('kasir/buat_laporan_salemba');
		$this->load->view('templates/footer', $data);
	}

	/*=====================================================================================
	==================PDF Start============================================================
	=======================================================================================*/

	function kuitansi() {
		$this->load->helper('file'); 
	    $this->load->helper(array('dompdf', 'file'));
	    
	    $tanggal = $this->session->userdata('tanggal');
	    $idrek = $this->session->userdata('idrek');
		$waktu = $this->session->userdata('waktu');
	    $datapasien = $this->kasirmod->getdatapasien($idrek);
		$biayatindakan = $this->kasirmod->getbiayatindakan($idrek, $tanggal, $waktu);
		$biayaobat = $this->kasirmod->getbiayaobat($idrek,$tanggal);

	    $data['tanggal'] = $tanggal;
		$data['datapasien'] = $datapasien->row();
		$data['idrekam'] = $idrek;
		$data['biayatindakan'] = $biayatindakan->result_array();
		$data['biayaobat'] = $biayaobat->result_array();

	    $html = $this->load->view('kasir/printables/kuitansi', $data, true);
	    $filename = 'kuitansi_'.$tanggal.'_'.$idrek;

	    pdf_create_simpel($html, $filename);
	}

	function pdf_harian() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $laporanharian = $this->kasirmod->getLaporanHarian($tanggal);
	     $data['tanggal'] = $tanggal;
	     $data['laporanharian'] = $laporanharian->result_array();
	     $html = $this->load->view('kasir/printables/laporan_harian', $data, true);
	     $filename = 'laporan_harian_'.$tanggal;
	     pdf_create_simpel($html, $filename);
	     $this->session->unset_userdata('tanggal');
	}

	function pdf_bulanan_gigi() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananGigi($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_gigi', $data, true);
	     $filename = 'laporan_bulanan_poligigi_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_umum() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananUmum($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_umum', $data, true);
	     $filename = 'laporan_bulanan_poliumum_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulanan($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan', $data, true);
	     $filename = 'laporan_bulanan_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_farmasi() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananFarmasi($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_farmasi', $data, true);
	     $filename = 'laporan_bulanan_farmasi_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_estetika() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananEstetika($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_estetika', $data, true);
	     $filename = 'laporan_bulanan_estetika_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_radiologi() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananRadio($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_radiologi', $data, true);
	     $filename = 'laporan_bulanan_radiologi_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_laboratorium() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananLab($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_laboratorium', $data, true);
	     $filename = 'laporan_bulanan_laboratorium_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_ambulans() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananAmbulans($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
 		 $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_ambulans', $data, true);
	     $filename = 'laporan_bulanan_ambulans_bulan_'.$bulan.'_'.$tahun;
	     pdf_create_simpel($html, $filename);
	}

	function pdf_bulanan_salemba() {
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporan_salemba = $this->kasirmod->get_salemba($bulan, $tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
	     $data['laporan_salemba'] = $laporan_salemba->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_salemba', $data, true);
	     pdf_create_simpel($html, 'laporan_bulanan_salemba_bulan_'.$bulan.'_'.$tahun);
	}

	function tes(){
		$this->load->helper('file'); 
	     $this->load->helper(array('dompdf', 'file'));
	     $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $laporanbulanan = $this->session->userdata('laporanbulanan');
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
	     $data['laporanbulanan'] = $laporanbulanan;
	     $html = $this->load->view('kasir/printables/laporan_bulanan_laboratorium', $data);
	}

	/*=====================================================================================
	=============================END OF PDF FUNCTIONS======================================
	=======================================================================================*/


	
	/*================================START OF XLS========================================*/
	function harian_xls(){
		$tanggal = $this->session->userdata('tanggal');
		$query = $this->kasirmod->getLaporanHarian($tanggal);	
		$this->load->library('excel');
		$this->excel->getProperties()->setTitle("Laporan Harian PKM UI".$tanggal)->setDescription("none");
		$this->excel->setActiveSheetIndex(0);
		$title = "LAPORAN PENDAPATAN HARIAN";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "TANGGAL: ".$tanggal;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'NAMA DOKTER');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'NO KUITANSI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'FAKULTAS');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'PENDAPATAN POLI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'BIAYA DENTAL LAB');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'PENDAPATAN POLI GIGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'PENDAPATAN RUANG FARMASI KARYAWAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'PENDAPATAN RUANG FARMASI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, 6, 'RADIOLOGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, 6, 'AMBULANS');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:M6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('2')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('1')->setWidth(25);

        $i=1;
        foreach($query->result_array() as $data) {
            $query2 = $this->kasirmod->getbiayatindakan($data['id_rekam_medis'], $tanggal);
			$query3 =  $this->kasirmod->getbiayaobat($data['id_rekam_medis'], $tanggal);
			$array2 = $query2->result_array();
			$array3 = $query3->result_array();
			$tarifpoliumum = 0;
			$tarifpoligigi = 0;
			$biayadental = 0;
			$obatkaryawan = 0;
			$obatumum = 0;
			$biayaradiologi = 0;
			$biayaambulans = 0;

			foreach ($array2 as $rowtindakan) {
				if($data['jenis_pasien'] == '1'){
					$checkumum = array_search('Poli Umum', $rowtindakan);
					$checklab = array_search('Laboratorium', $rowtindakan);
					$checkest = array_search('Estetika Medis', $rowtindakan);
					$checkradio = array_search('Radiologi', $rowtindakan);
					$checkambulans = array_search('Ambulans', $rowtindakan);

					if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
						$tarifpoliumum += $rowtindakan['tarif_mahasiswa'];
					}
					$checkgigi = array_search('Poli Gigi', $rowtindakan);
					if($checkgigi !=null){
						$tarifpoligigi += $rowtindakan['tarif_mahasiswa'];
					}
					if($checkradio !=null){
						$biayaradiologi += $rowtindakan['tarif_mahasiswa'];
					}
					if($checkambulans !=null){
						$biayaambulans += $rowtindakan['tarif_mahasiswa'];
					}
				}
				else if($data['jenis_pasien'] == '2'){
					$checkumum = array_search('Poli Umum', $rowtindakan);
					$checklab = array_search('Laboratorium', $rowtindakan);
					$checkest = array_search('Estetika Medis', $rowtindakan);
					$checkradio = array_search('Radiologi', $rowtindakan);
					$checkambulans = array_search('Ambulans', $rowtindakan);

					if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
						$tarifpoliumum += $rowtindakan->tarif_karyawan;
					}
					$checkgigi = array_search('Poli Gigi', $rowtindakan);
					if($checkgigi !=null){
						$tarifpoligigi += $rowtindakan['tarif_karyawan'];
					}
					if($checkradio !=null){
						$biayaradiologi += $rowtindakan['tarif_karyawan'];
					}
					if($checkambulans !=null){
						$biayaambulans += $rowtindakan['tarif_karyawan'];
					}
				}
				else {
					$checkumum = array_search('Poli Umum', $rowtindakan);
					$checklab = array_search('Laboratorium', $rowtindakan);
					$checkest = array_search('Estetika Medis', $rowtindakan);
					$checkradio = array_search('Radiologi', $rowtindakan);
					$checkambulans = array_search('Ambulans', $rowtindakan);

					if($checkumum!=null || $checklab!=null  || $checkest!=null ) {
						$tarifpoliumum += $rowtindakan['tarif_umum'];
					}
					$checkgigi = array_search('Poli Gigi', $rowtindakan);
					if($checkgigi !=null){
						$tarifpoligigi += $rowtindakan['tarif_umum'];
					}
					if($checkradio !=null){
						$biayaradiologi += $rowtindakan['tarif_umum'];
					}
					if($checkambulans !=null){
						$biayaambulans += $rowtindakan['tarif_umum'];
					}
				}
			}

			foreach($array3 as $rowobat){
				if($data['jenis_pasien'] == '1'){
					$obatkaryawan = 0;
					$obatumum = 0;
				}
				else if($data['jenis_pasien'] == '2'){
					$obatkaryawan = 2500;
				}
				else {
					$obatumum += $rowobat['jumlah'] * $rowobat['harga_per_kemasan'];
				}
			}

			$biayajp = 0.2*($tarifpoliumum);
			$biayajpgigi = 0.2*($tarifpoligigi);
			$nettotindakanumum = $tarifpoliumum-$biayajp;
			$nettogigi = $tarifpoligigi-$biayadental;
			$pendapatangigi = $nettogigi-$biayajpgigi;

			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['nama_dokter']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['no_kuitansi']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $data['lembaga']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $nettotindakanumum);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $biayadental);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $pendapatangigi);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $obatkaryawan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $obatumum);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+9, $row, $biayaradiologi);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row, $biayaambulans);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':M'.$row)->applyFromArray($bodystyle);
			$i++;
			$row++;
        }
        $rowbefore = $row -1 ;
        $rowafter = $row + 1 ;
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':F'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, '=SUM(G7:G'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, '=SUM(H7:H'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, '=SUM(I7:I'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, '=SUM(J7:J'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, '=SUM(K7:K'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row,'=SUM(L7:L'.$rowbefore.')') ;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, '=SUM(M7:M'.$rowbefore.')');
        $this->excel->getActiveSheet()->mergeCells('B'.$rowafter.':L'.$rowafter);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowafter, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $rowafter, '=G'.$row.'+I'.$row.'+J'.$row.'+K'.$row.'+L'.$row.'+M'.$row.'');

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row+3, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row+4, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row+5, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row+9, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row+10, "NIP 195210301979021001");

        $this->excel->getActiveSheet()->getStyle('B'.$row.':M'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->getStyle('B'.$rowafter.':M'.$rowafter)->applyFromArray($headerstyle);
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);
        ob_clean();
        // header and footer
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Harian_'.$tanggal.'.xls"');
        header('Cache-Control: max-age=0');
 
        
        $objWriter->save('php://output');
	}

	function bulanan_farmasi_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananFarmasi($bulan,$tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Ruang Farmasi PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        //$objPHPexcel = PHPExcel_IOFactory::load("bangke.xlsx");
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN RUANG FARMASI";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'NO KUITANSI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'JENIS PEMERIKSAAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'HARGA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'BAGI HASIL PKM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'DOKTER PENGIRIM');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:I6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);

        $i=1;
        $totalhargakaryawan = 0;
    	$totalhargaumum = 0;
        foreach($query->result_array() as $data) {
        	$hargaobatkaryawan = 2500;
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama']);
        	if ($data['jenis_pasien'] == '2'){
           		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, 'YA');
            	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, 'BUKAN');
            	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $data['nama_dokter']);
           		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $hargaobatkaryawan);
           		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, 0);
           	}

           	else if ($data['jenis_pasien'] == '3'){
           		$hargaobatkaryawan = 0;
           		$data1 = $this->kasirmod->getbiayaobat($data['id_rekam_medis'],$data['tanggal']);
           		$biayaobat = $data1->result_array();
           		$hargaobat = 0;
           		foreach ($biayaobat as $bobat) {
           			$jumlah = $bobat['jumlah'];
           			$hargaperkemasan = $bobat['harga_per_kemasan'];
           			$hargaobat += $jumlah * $hargaperkemasan;
           		}
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, 'BUKAN');
            	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, 'YA');
            	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $data['nama_dokter']);
           		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $hargaobatkaryawan);
           		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $hargaobat);
           	}	
        	
            $this->excel->getActiveSheet()->getStyle('B'.$row.':I'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;
            $totalhargakaryawan += $hargaobatkaryawan;
            $totalhargaumum += $hargaobat;    
        }
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $totalhargakaryawan);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $totalhargaumum);
        $this->excel->getActiveSheet()->getStyle('B'.$row.':I'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+9, "NIP 195210301979021001");

        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Farmasi_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function bulanan_umum_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananUmum($bulan,$tahun);
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Poli Umum PKM UI".$bulan)->setDescription("none");

        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN POLI UMUM";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JENIS KELAMIN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'UMUR');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'DIAGNOSA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'TINDAKAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'PENDAPATAN POLI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'DOKTER PEMERIKSA');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:J6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);

        $i=1;
        foreach($query->result_array() as $data) {
        	$umur = date("Y/m/d") - $data['tanggal_lahir'];
	       	$tindakanumum = $this->kasirmod->getbiayatindakanumum($data['id_rekam_medis'],$data['tanggal']);
	       	$datadiagnosa = $this->doktermod->getDataDiagnosa($data['id_rekam_medis'],$data['tanggal']);
            $konsultasi = $this->kasirmod->getKonsultasi($data['id_rekam_medis'],$data['tanggal']);
            $username_dokter = $this->kasirmod->getNamaDokter($data['id_rekam_medis'],$data['tanggal']);
            $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
            $diagnosa = null;
            $coy = 1;
            $length = sizeof($datadiagnosa);
            foreach($datadiagnosa as $diagnosapasien) {	
            	if($coy == $length){
            		$diagnosa .= $diagnosapasien['jenis_penyakit'];
            	}
            	else {
            		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
            	}
            	$coy++;
            }
            $j = 0;
            $tarifpoliumum = 0;
            $jasadokter = 0;

        	if($data['jenis_pasien'] == '1'){
				$tarifpoliumum += $data['tarif_mahasiswa'];
			}
			else if($data['jenis_pasien'] == '2'){
				$tarifpoliumum += $data['tarif_karyawan'];								
			}
			else {
				$tarifpoliumum += $data['tarif_umum'];									
			}   
           
            $jasatindakan = $tarifpoliumum - $jasadokter;
        	$jepe = 0.2*($tarifpoliumum);
        	$pendapatan = $tarifpoliumum - $jepe;

            if($konsultasi != null) {
            	$jasadokter = 15000;
            }
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['jenis_kelamin']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $umur);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $diagnosa);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $pendapatan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $namadokter);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':J'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;
        }
        $rowbefore = $row - 1 ;
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, '=SUM(I7:I'.$rowbefore.')');
        $this->excel->getActiveSheet()->getStyle('B'.$row.':J'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_PoliUmum_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');
	}

	function bulanan_gigi_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananGigi($bulan,$tahun);
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Poli Gigi PKM UI".$bulan)->setDescription("none");

        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN POLI GIGI";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JENIS KELAMIN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'UMUR');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'DIAGNOSA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'TINDAKAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'BIAYA DENTAL LAB');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'PENDAPATAN POLI GIGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'DOKTER PEMERIKSA');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:K6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('2')->setWidth(25);

        $i=1;
        $totaldental = 0;
        $totalpendapatan = 0;

        foreach($query->result_array() as $data) {
        	$umur = date("Y/m/d") - $data['tanggal_lahir'];
	       	$tindakangigi = $this->kasirmod->getbiayatindakangigi($data['id_rekam_medis'],$data['tanggal']);
	       	$datadiagnosa = $this->doktermod->getDataDiagnosa($data['id_rekam_medis'],$data['tanggal']);
            $username_dokter = $this->kasirmod->getNamaDokter($data['id_rekam_medis'],$data['tanggal']);
            $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
            $diagnosa = null;
            $coy = 1;
            $length = sizeof($datadiagnosa);
            foreach($datadiagnosa as $diagnosapasien) {	
            	if($coy == $length){
            		$diagnosa .= $diagnosapasien['jenis_penyakit'];
            	}
            	else {
            		$diagnosa .= $diagnosapasien['jenis_penyakit'].', ';
            	}
            	$coy++;
            }
            $j = 0;
            $tarifpoligigi = 0;

        	if($data['jenis_pasien'] == '1'){
				$tarifpoligigi += $data['tarif_mahasiswa'];
			}
			else if($row['jenis_pasien'] == '2'){
				$tarifpoligigi += $data['tarif_karyawan'];								
			}
			else {
				$tarifpoligigi += $data['tarif_umum'];									
			}     
           
            $biayadental = 0;
        	$jepe = 0.2*($tarifpoligigi);
        	$pendapatan = $tarifpoligigi - $jepe;
        	$netto = $tarifpoligigi - $biayadental;

        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['jenis_kelamin']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $umur);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $diagnosa);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $biayadental);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $pendapatan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $namadokter);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':K'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;
            $totaldental += $biayadental;
            $totalpendapatan += $pendapatan;
        }
        $rowbefore = $row - 1 ;
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':H'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, '=SUM(I7:I'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, '=SUM(J7:J'.$rowbefore.')');
        $this->excel->getActiveSheet()->getStyle('B'.$row.':K'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_PoliGigi_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');
	}

	function bulanan_lab_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananLab($bulan,$tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        //$objPHPexcel = PHPExcel_IOFactory::load("bangke.xlsx");
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN LABORATORIUM";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'NO KUITANSI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'JENIS PEMERIKSAAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'HARGA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'BAGI HASIL PKM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'DOKTER PENGIRIM');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:I6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);

        $i = 1;
        $totalhargalab = 0;
        $totalbagihasil = 0; 
        foreach($query->result_array() as $data) {
        	$hargatindakanlab = $data['tarif_umum'];
	       	$bagihasilpkm = 0.3 * $hargatindakanlab;
	       	$username_dokter = $this->kasirmod->getNamaDokter($data['id_rekam_medis'],$data['tanggal']);
            $namadokter = $this->kasirmod->getNamebyUsername($username_dokter);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['no_kuitansi']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $hargatindakanlab);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $bagihasilpkm);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $namadokter);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':I'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;
            $totalhargalab += $hargatindakanlab;
		    $totalbagihasil += $bagihasilpkm;    
        }
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':F'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $totalhargalab);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $totalbagihasil);
        $this->excel->getActiveSheet()->getStyle('B'.$row.':I'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Lab_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function bulanan_estetika_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananEstetika($bulan,$tahun);
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Estetika PKM UI".$bulan)->setDescription("none");
 
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN ESTETIKA MEDIS";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'NO KUITANSI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'JENIS PEMERIKSAAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'HARGA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'BAGI HASIL PKM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'DOKTER PENGIRIM');
        //diapus kalo template dah jalan...DAN GA AKAN JALAN KAYANYAAA

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:N6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('2')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('1')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(35);

        $i=1;
        foreach($query->result_array() as $data) {
			$hargatindakan = 0;
			$namaobat = null;
            $dataobatestetika = $this->kasirmod->getObatEstetika($data['id'], $data['idrekmed'], $data['tanggal']);
             if ($data['jenis_pasien'] == '1'){
                $hargatindakan= $data['tarif_mahasiswa'];
                $bagihasiltindakan = 0.3* $hargatindakan;
            }
            else if ($data['jenis_pasien'] == '2'){
                $hargatindakan = $data['tarif_karyawan'];
                $bagihasiltindakan = 0.3* $hargatindakan;
            }
            else {
                $hargatindakan = $data['tarif_umum'];
                $bagihasiltindakan = 0.3* $hargatindakan;
            }
            foreach ($dataobatestetika as $rowobat) {
                $jumlah = $rowobat['jumlah'];
                $totalhargaobat = $jumlah * $rowobat['harga_per_kemasan'];
                $bagihasilobat = 0.1 *($totalhargaobat);
                $namaobat .= $rowobat['nama'];
            }
            $totalpendapatan = $hargatindakan + $totalhargaobat;
            $totalbagihasil = $bagihasilobat +$bagihasiltindakan;
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['no_kuitansi']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $hargatindakan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $bagihasiltindakan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $namaobat);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $jumlah);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $totalhargaobat);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+9, $row, $bagihasilobat);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row, $totalpendapatan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row, $totalbagihasil);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':N'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;
        }
        $rowbefore = $row -1 ;
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':F'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, '=SUM(G7:G'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, '=SUM(H7:H'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, '');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, '=SUM(J7:J'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, '=SUM(K7:K'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, '=SUM(L7:L'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, '=SUM(M7:M'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, '=SUM(N7:N'.$rowbefore.')');
        $this->excel->getActiveSheet()->getStyle('B'.$row.':N'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_EstetikaMedis_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function bulanan_ambulans_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananAmbulans($bulan,$tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        //$objPHPexcel = PHPExcel_IOFactory::load("bangke.xlsx");
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN AMBULANS";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JENIS PEMASUKKAN AMBULANS');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'BIAYA');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:F6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);

        $i = 1; 
        foreach($query->result_array() as $data) {
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $data['tarif_umum']);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':F'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;    
        }

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Ambulans_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
        ob_clean();
        $objWriter->save('php://output');
	}

	function bulanan_radiologi_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulananRadio($bulan,$tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN RADIOLOGI";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JENIS PEMASUKKAN RADIOLOGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'JUMLAH PEMERIKSAAN RADIOLOGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'BIAYA');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:G6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

        $i = 1; 
        $totalbiayaradiologi = 0;
        foreach($query->result_array() as $data) {
        	$biayaradiologi = $data['tarif_umum'];
        	if ($biayaradiologi!=null){
        		$jumlahtindakanradio = 1;
        	}
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['nama']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $jumlahtindakanradio);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $biayaradiologi);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;    
            $totalbiayaradiologi += $biayaradiologi;
        }

        $this->excel->getActiveSheet()->mergeCells('B'.$row.':F'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $totalbiayaradiologi);
        $this->excel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+9, "NIP 195210301979021001");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Radiologi_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function bulanan_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->getLaporanBulanan($bulan,$tahun);
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan PKM UI".$bulan)->setDescription("none");
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'PENDAPATAN POLI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'BIAYA DENTAL LAB');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'PENDAPATAN POLI GIGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'PENDAPATAN RUANG FARMASI KARYAWAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'PENDAPATAN RUANG FARMASI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'PENDAPATAN RADIOLOGI');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'PENDAPATAN AMBULANS');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 6, 'SALDO AKHIR');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:K6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(38);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(38);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('2')->setWidth(30);

        $i = 1;
        $total1 = 0;
        $total2 = 0;
        $total3 = 0;
        $total4 = 0;
        $total5 = 0;
        $total6 = 0;
        $total7 = 0;
        $total8 = 0;
        $total9 = 0;
        $total10 = 0;
        $total11 = 0;
        $total12 = 0;
        $total13 = 0;
        $total14 = 0;
        $totalpendapatanbulanan = 0;
        foreach($query->result_array() as $data) {
        		 
	        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);

	            $biayapoliumum = $this->kasirmod->getPoliUmumHarian($data['tanggal']);
            	$biayapoligigi = $this->kasirmod->getPoliGigiHarian($data['tanggal']);
            	$biayafarmasi = $this->kasirmod->getFarmasiHarian($data['tanggal']); 
            	$biayaradiologi = $this->kasirmod->getRadiologiHarian($data['tanggal']); 
            	$biayaambulans = $this->kasirmod->getAmbulansHarian($data['tanggal']); 	

            	$poliumum = 0;

            	$poligigi = 0;
            	$biayadental = 0;

            	$obatkaryawan = 0;
            	$obatumum = 0;

            	$radiologi = 0;
            	$ambulans = 0;

            	foreach($biayapoliumum as $bumum) {  
                	if($bumum['jenis_pasien'] == '1'){
						$poliumum += $bumum['tarif_mahasiswa'];
					}
					else if($bumum['jenis_pasien'] == '2'){
						$poliumum += $bumum['tarif_karyawan'];								
					}
					else {
						$poliumum += $bumum['tarif_umum'];									
					}      	
                } 
                $pendapatanpoliumum = 0.8*($poliumum);

                foreach($biayapoligigi as $bgigi) {  
                	if($bgigi['jenis_pasien'] == '1'){
						$poligigi += $bgigi['tarif_mahasiswa'];
					}
					else if($bgigi['jenis_pasien'] == '2'){
						$poligigi += $bgigi['tarif_karyawan'];								
					}
					else {
						$poligigi += $bgigi['tarif_umum'];									
					}      	
                } 


            	$poligiginetto = $poligigi-$biayadental;
            	$pendapatanpoligigi = 0.8*($poligiginetto);

                foreach($biayafarmasi as $bfarm) {  
					if($bfarm['jenis_pasien'] == '2'){
						$obatkaryawan += 2500;							
					}
					else if($bfarm['jenis_pasien'] == '3') {
						$hargaobat = $bfarm['harga_per_kemasan']*$bfarm['jumlah'];
						$obatumum +=$hargaobat;						
					}      	
                } 

                foreach($biayaradiologi as $bradio) {
                	if($bradio['jenis_pasien'] == '1'){
						$radiologi += $bradio['tarif_mahasiswa'];
					}
					else if($bgigi['jenis_pasien'] == '2'){
						$radiologi += $bradio['tarif_karyawan'];								
					}
					else {
						$radiologi += $bradio['tarif_umum'];									
					}
                }

                 foreach($biayaambulans as $bambu) {
                	if($bambu['jenis_pasien'] == '1'){
						$ambulans += $bambu['tarif_mahasiswa'];
					}
					else if($bgigi['jenis_pasien'] == '2'){
						$ambulans += $bambu['tarif_karyawan'];								
					}
					else {
						$ambulans += $bambu['tarif_umum'];									
					}
                }

                $saldoakhir = $pendapatanpoliumum+$poligiginetto+$obatumum+$obatkaryawan+$radiologi+$ambulans;
                $totalgigiumum = $pendapatanpoliumum+$pendapatanpoligigi;
                $pendapatanPKM = 0.2*($poliumum)+0.2*($poligiginetto);
                $totalsemua = $poliumum+$poligigi+$obatumum+$obatkaryawan+$radiologi+$ambulans;
                $pendapatanUI = 0.8*($totalsemua);

	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $pendapatanpoliumum);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $biayadental);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $pendapatanpoligigi);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $obatkaryawan);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $obatumum);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $radiologi);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $ambulans);
	            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $saldoakhir);
	            $this->excel->getActiveSheet()->getStyle('B'.$row.':K'.$row)->applyFromArray($bodystyle);
	            $i++;
	            $row++;  

                $total2 += $pendapatanpoliumum;
                $total4 += $biayadental;
                $total6 += $pendapatanpoligigi;
                $total7 += $obatkaryawan;
                $total8 += $obatumum;
                $total9 += $radiologi;
                $total10 += $ambulans;
                $total11 += $saldoakhir;
            }
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $total2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $total4);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $total6);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $total7);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $total8);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $total9);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $total10 );
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $total11);
        $this->excel->getActiveSheet()->getStyle('B'.$row.':K'.$row)->applyFromArray($bodystyle);

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row+9, "NIP 195210301979021001");

        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function bulanan_salemba_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->kasirmod->get_salemba($bulan, $tahun);
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Salemba PKM UI".$bulan)->setDescription("none");
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN PENDAPATAN SALEMBA";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'TANGGAL');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'NAMA PASIEN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JENIS KELAMIN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'UMUR');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'DIAGNOSA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 6, 'TINDAKAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 6, 'PENDAPATAN POLI UMUM');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 6, 'DOKTER PEMERIKSA');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THIN,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'bottom' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:J6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);

        $i = 1;
         foreach($query->result_array() as $data) {
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['tanggal']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['nama_pasien']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $data['jenis_kelamin']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $data['umur']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $data['diagnosa']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $data['tindakan']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $data['pendapatan_poli_umum']);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $data['dokter_pemeriksa']);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':J'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;  
        }
        $rowbefore = $row - 1 ;
        $this->excel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, 'JUMLAH PENDAPATAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, '=SUM(H7:H'.$rowbefore.')');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, '');
        $this->excel->getActiveSheet()->getStyle('B'.$row.':K'.$row)->applyFromArray($bodystyle);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+3, "Wakil Direktur Umum dan Fasilitas");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+4, "Selaku Penanggung Jawab PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+8, "DR. drg. Harun A Gunawan, MS PAK");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row+9, "NIP 195210301979021001");

        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Salemba_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');
	}

	/*===============================================END OF XLS==================================================*/


	/*====================================*******************===================================================
	====================================FEURROTER PFEIL UND BOGEN================================================
	====================================_____________________________==============================================*/
	function print_ambulans() {
		 $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananAmbulans($bulan,$tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
 		 $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_ambulans', $data);
	}

	function print_gigi() {
		 $tanggal = $this->session->userdata('tanggal');
	     $bulan = $this->session->userdata('bulan');
	     $tahun = $this->session->userdata('tahun');
	     $laporanbulanan = $this->kasirmod->getLaporanBulananGigi($bulan,$tahun);
 		 $data['tanggal'] = $tanggal;
 		 $data['bulan'] = $bulan;
 		 $data['tahun'] = $tahun;
 		 $data['laporanbulanan'] = $laporanbulanan->result_array();
	     $html = $this->load->view('kasir/printables/laporan_bulanan_gigi', $data);
	}

	function print_harian() {
		 $tanggal = $this->session->userdata('tanggal');
	     $laporanharian = $this->kasirmod->getLaporanHarian($tanggal);		
 		 $data['tanggal'] = $tanggal;
 		 $data['laporanharian'] = $laporanharian->result_array();
	     $this->load->view('kasir/printables/laporan_harian', $data);
	}

	function print_bulanan() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulanan($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan', $data);
	}

	function print_umum() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulananUmum($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_umum', $data);
	}

	function print_estetika() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulananEstetika($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_estetika', $data);
	}

	function print_lab() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulananLab($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_laboratorium', $data);
	}

	function print_radiologi() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulananRadio($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_radiologi', $data);
	}

	function print_farmasi() {
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanbulanan = $this->kasirmod->getLaporanBulananFarmasi($bulan, $tahun);		
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
 		$data['laporanbulanan'] = $laporanbulanan->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_farmasi', $data);
	}

	function print_kuitansi(){
		$nokuitansi = $this->kasirmod->getLatestNoKuitansi();
		$tanggal = $this->session->userdata('tanggal');
	    $idrek = $this->session->userdata('idrek');
		$waktu = $this->session->userdata('waktu');
	    $datapasien = $this->kasirmod->getdatapasien($idrek);
		$biayatindakan = $this->kasirmod->getbiayatindakan($idrek, $tanggal, $waktu);
		$biayaobat = $this->kasirmod->getbiayaobat($idrek,$tanggal);
		$data['nokuitansi'] = $nokuitansi;
	    $data['tanggal'] = $tanggal;
		$data['datapasien'] = $datapasien->row();
		$data['idrekam'] = $idrek;
		$data['biayatindakan'] = $biayatindakan->result_array();
		$data['biayaobat'] = $biayaobat->result_array();
	    $this->load->view('kasir/printables/kuitansi', $data);
	}
	
	function print_salemba(){
		$tanggal = $this->session->userdata('tanggal');
	    $bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporan_salemba = $this->kasirmod->get_salemba($bulan, $tahun);
 		$data['tanggal'] = $tanggal;
 		$data['bulan'] = $bulan;
 		$data['tahun'] = $tahun;
	    $data['laporan_salemba'] = $laporan_salemba->result_array();
	    $this->load->view('kasir/printables/laporan_bulanan_salemba', $data);
	}
	/*=====================================Sie sind das Essen und wir sind die Jäger ===============================*/
}