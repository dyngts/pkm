<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
    $page = "registrasipasien";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}
		
		if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			if (ALLOW_REGISTER)
			{
				redirect('register/umum');
			}
			else {
				redirect('site');
			}
		}
		if ($this->authentication->is_pasien()) {
			redirect('site');
		}
		if (!$this->authentication->is_pelayanan()) {
			redirect('site');
		}

		$data['title'] = "Berita"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function umum()
	{
		if ($this->authentication->is_pasien()) 
		{
			$this->index();
		}

		else {
			$data['tipe_registrasi'] = 3;
			$this->registrasipasien($data);
		}
	}

	public function mahasiswa()
	{
		if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			redirect('register');
  	}
		elseif ($this->session->userdata('is_pasien')) 
		{
			$this->index();
		}		

		else {
			$data['tipe_registrasi'] = 1;
			$this->registrasipasien($data);
		}
	}

	public function karyawan()
	{
		if (!$this->authentication->is_loggedin()/* || !$this->session->userdata('is_baru')*/) {
			redirect('register');
  	}
		elseif ($this->session->userdata('is_pasien')) 
		{
			$this->index();
		}		

		else {
			$data['tipe_registrasi'] = 2;
			$this->registrasipasien($data);
		}
	}

	public function pegawai()
	{
		if (!$this->authentication->is_loggedin() && !$this->authentication->firsttime()/* || !$this->session->userdata('is_baru')*/) {
			redirect('register');
		}
		elseif ($this->session->userdata('is_pasien')) 
		{
			$this->index();
		}	
		else {
			$data['tipe_registrasi'] = 4;
			$this->registrasipasien($data);
		}
	}

  public function registrasipasien($data_in) 
  {
  	$default_id_foto = 'default.jpg';
  	$jenis_user = $data_in['tipe_registrasi'];
		$id_foto = $this->random(16).".jpg";
		$data = array('jenis_user' => $jenis_user, 'id_foto' => $id_foto,);

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		$this->form_validation->set_message('required', '%s wajib diisi');
		$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
		$this->form_validation->set_message('exact_length', '%s harus tepat %d karakter');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dot_num', '%s hanya boleh mengandung huruf, titik, dan angka');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('is_natural', '%s harus angka');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');

		if ($jenis_user == 1) 
		{
			$this->form_validation->set_rules('npm', 'NPM', 'required|is_natural|exact_length[10]');
			$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
			$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required|max_length[50]|callback_char_space_only');
			$this->form_validation->set_rules('tahunmasuk', 'Tahunmasuk', 'required|is_natural_no_zero|max_length[4]');
		}
		elseif ($jenis_user == 2)
		{
			$this->form_validation->set_rules('lembaga', 'Lembaga', 'trim|required|max_length[30]|callback_char_space_only');
		}
		elseif ($jenis_user == 3)
		{
			$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required|max_length[20]|callback_char_space_only');
			$this->form_validation->set_rules('no_identitas', 'Nomor Identitas', 'required|max_length[25]|is_natural');
		}

		if ($jenis_user <= 3) {
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Kelahiran', 'required|max_length[30]|callback_char_space_only');
			$this->form_validation->set_rules('demo4', 'Tanggal Kelahiran', 'required');
			$this->form_validation->set_rules('agama', 'Agama', 'required');
			$this->form_validation->set_rules('status_pernikahan', 'Status Pernikahan', 'required');
			$this->form_validation->set_rules('golongan_darah', 'Golongan darah', 'required');
			$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'trim|required|max_length[20]|callback_char_space_only');
		}
		elseif ($jenis_user > 3) {
			$this->form_validation->set_rules('peran', 'Peran', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|max_length[30]|is_unique[pengguna.username]|callback_char_dot_num');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		}

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[30]|callback_char_dash_only');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|min_length[6]|max_length[15]|is_natural');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|max_length[100]|callback_char_dash_num_only');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		
		if ($jenis_user == 3 && ! $this->authentication->is_loket()) {
			require_once 'recaptcha/recaptchalib.php';
		}
		
		if ($jenis_user == 3 && ! $this->authentication->is_loket()) 
		{
			$this->form_validation->set_rules('recaptcha', 'recaptcha', 'callback_checkRecaptcha');
		}

		if ($this->form_validation->run() == TRUE)
		{
			if ($jenis_user <= 3) $path = './foto/pasien/';
			else $path = './foto/pegawai/';
			$config = array(
			 'allowed_types' => 'jpg|jpeg|png|gif',
			 'upload_path' => $path,//base_url('foto'),
			 'max_size' => 1024,
			 'max_height' => 600,
			 'max_width' => 400,
			 'file_name' => $id_foto,
			 'overwrite' => TRUE
			 //'remove_spaces' => TRUE
			);

	    $this->load->helper('file');
	    $this->load->library('upload', $config);

	    if ($_FILES && $_FILES['foto']['name'] !== "") 
	    {
  		// Upload code here

				if ($this->upload->do_upload('foto'))
				{

					if (!$this->authentication->is_loket() && !$this->authentication->is_pelayanan() && !$this->authentication->is_pj() && !$this->authentication->firsttime())
					{
						$this->save($data);
					}
					else
					{
						$this->createandvalidate($data);
					}
				}
				else
				{

					$error = array('error' => $this->upload->display_errors());

					if ($jenis_user > 3)
					{
						$this->form_pasien("pegawai", $error);
					}
					elseif ($jenis_user == 3)
					{
						$this->form_pasien("umum", $error);
					}
					elseif ($jenis_user == 2)
					{
						$this->form_pasien("karyawan", $error);
					}
					elseif ($jenis_user == 1)
					{
						$this->form_pasien("mahasiswa", $error);
					}
					
				}
			}

	    else {

				if (!$this->authentication->is_loket() && !$this->authentication->is_pelayanan() && !$this->authentication->is_pj() && !$this->authentication->firsttime())
				{
					$data['id_foto'] = $default_id_foto;
					$this->save($data);
				}
				else
				{
					$data['id_foto'] = $default_id_foto;
					$this->createandvalidate($data);
				}
	    }
		}

		else
		{
			if ($jenis_user > 3)
			{
				$this->form_pasien("pegawai");
			}
			elseif ($jenis_user == 3)
			{	
				$this->form_pasien("umum");
			}
			elseif ($jenis_user == 2)
			{
				$this->form_pasien("karyawan");
			}
			elseif ($jenis_user == 1)
			{
				$this->form_pasien("mahasiswa");
			}
		} 

  }

	public function form_pasien($peran, $error = '') {
		if ( ! file_exists('application/views/site/registrasipasien'.$peran.'.php')) 
		{
			show_404();
		}
		
		$data['publickey'] = '6LcqBOkSAAAAAF2vZCTnX5TwFgl5lt9ggD7Njw_0';
		$data['title'] = "Registrasi"; 
		$data['listperan'] = $this->user_model->get_role_list();
		$data['error'] = $error;

		$this->load->view('templates/header', $data);
		$this->load->model('authentication');

		if($this->authentication->is_loggedin())
		{
			$this->load->view('usermenu/usermenu', $data);
    }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/registrasipasien'.$peran, $data);
		$this->load->view('templates/footer', $data);
	}

	private function createandvalidate($data) {

		$id = $this->process($data);

		if ($id != "") {
			redirect('akun/verify/'.$id);
		}
		else {
			$data['title'] = "Operasi gagal";
			$data['msg'] = "
	      <div class=\"row\">
	        <div class=\"message message-red\">
	          <p class=\"p_message\">Pembuatan akun tidak berhasil</p>
	        </div>
	      </div>";

			$this->sukses($data);
		}
	}

	private function save($data){
		$id = $this->process($data);
		if ($id != "") {
			$this->registrasisuccess($id);
			//$this->session->sess_destroy();
		}
		else $this->index();
  }

	public function registrasisuccess($id) {
		$page = 'registrasisuccess';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$this->profilecalonpasien($id);
	}

  /**
   * Mengolah masukan user pada form registrasi pasien
   * Membuat calon pasien
   * @return boolean return TRUE setelah dijalankan
   */
  public function process($data) {

		// Siapkan dan set data untuk session user
		if(!$this->authentication->is_loggedin() && !$this->authentication->firsttime())
		{
			$data1 = array(
        'is_mendaftar' => true,
        'is_baru' => false,
        'is_calon_pasien' => true
        );
    	$this->session->set_userdata($data1);
		}
		if ($data['jenis_user'] <= 3)
		return $this->insert_pasien($data);
		else return $this->insert_pegawai($data);
	}

	private function insert_pegawai($data)
	{

		$id = $this->user_model->create_id($data['jenis_user'], "false");
		
		// Periksa apakan user telah melakukan login (via LDAP).
		// Jika ya, set sesssion data
		if($this->authentication->is_loggedin() && !$this->authentication->is_pelayanan() && !$this->authentication->is_pj() && $this->authentication->firsttime()) {
			$username = $this->session->userdata('username');
		}
		// Jika tidak buat username baru (selanjutnya tidak digunakan)
		else $username = $this->input->post('username');

		// Ambil data yang dimasukkan atau yang dipilih user
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$agama = $this->input->post('agama');
		$golongan_darah = $this->input->post('golongan_darah');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$no_telepon = $this->input->post('no_telp');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$alamat = $this->input->post('alamat');
		$peran = $this->input->post('peran');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$npm = $this->input->post('npm');
		$jurusan = $this->input->post('jurusan');
		$lembaga_fakultas = $this->input->post('fakultas');
		$no_identitas = $this->input->post('no_identitas');
		$pekerjaan = $this->input->post('pekerjaan');
		$lembaga = $this->input->post('lembaga');
		$semester_daftar = $this->input->post('semester');
		$tahun_daftar = (integer) date("Y");
		$jenis_pasien = $data['jenis_user'];
		if ($jenis_pasien <= 3) $id_otorisasi = $jenis_pasien;
		else $id_otorisasi = $peran;
		$foto = $data['id_foto'];
		$password = md5('pass');
		$status_aktif = "false";

			$datapegawai_gen = array(
				'nama' => $nama, 
				'jenis_kelamin' => $jenis_kelamin,
				'email' => $email,
				'alamat' => $alamat,
				'no_telepon' => $no_telepon,
				'foto' => $foto,
				'username' => $username,
				'id_otorisasi' => $id_otorisasi,
				'status_aktif' => $status_aktif,
				);

			$datapegawai_auth = array(
					'username' => $username, 
					'password' => $password,
					'id_otorisasi' => $id_otorisasi,
				);

			$datapegawai = array(
					'data_auth' => $datapegawai_auth,
					'data_general' => $datapegawai_gen,
				);

		if ($this->user_model->insert_pegawai($datapegawai))
			return $id;
		else return "";
	}

	private function insert_pasien($data)
	{
		$id = $this->user_model->create_id($data['jenis_user'], "false");
		
		// Periksa apakan user telah melakukan login (via LDAP).
		// Jika ya, set sesssion data
		if($this->authentication->is_loggedin() && !$this->authentication->is_loket()) {
			$username = $this->session->userdata('username');
		}
		else if ($this->authentication->is_civitas())
			$username = $this->authentication->get_my_username();
		// Jika tidak buat username baru (selanjutnya tidak digunakan)
		else $username = $id;

		// Ambil data yang dimasukkan atau yang dipilih user
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$agama = $this->input->post('agama');
		$golongan_darah = $this->input->post('golongan_darah');
		$tempat_lahir = $this->input->post('tempat_lahir');
		$tanggal_lahir = $this->input->post('demo4');
		$status_pernikahan = $this->input->post('status_pernikahan');
		$no_telepon = $this->input->post('no_telp');
		$kewarganegaraan = $this->input->post('kewarganegaraan');
		$alamat = $this->input->post('alamat');
		$jurusan = $this->input->post('jurusan');

		$fakultas = $this->input->post('fakultas');
		$pekerjaan = $this->input->post('pekerjaan');
		$lembaga = $this->input->post('lembaga');

		if ($fakultas) { $lembaga_fakultas = $fakultas; }
			elseif ($pekerjaan)  { $lembaga_fakultas = $pekerjaan; }
			elseif ($lembaga) { $lembaga_fakultas = $lembaga; }
			else {}

		$npm = $this->input->post('npm');
		$no_identitas = $this->input->post('no_identitas');
		if ($no_identitas) {}
			else $no_identitas = $npm;

		$peran =$this->input->post('peran');
		$id_otorisasi = $data['jenis_user'];
		if ($id_otorisasi <= 3) $jenis_pasien = $id_otorisasi;
		else $jenis_pasien = $peran;

		$foto = $data['id_foto'];
		$password = md5('pass');
		$semester_daftar = $this->input->post('tahunmasuk');
		if($semester_daftar) $tahun_daftar = $semester_daftar;
		else $tahun_daftar = (integer) date("Y");

		$datapasien_gen = array(
			'id' => $id,
			'nama' => $nama, 
			'jenis_kelamin' => $jenis_kelamin,
			'agama' => $agama,  
			'gol_darah' => $golongan_darah,
			'tempat_lahir' => $tempat_lahir, 
			'tanggal_lahir' => $tanggal_lahir, 
			'status_pernikahan' => $status_pernikahan,
			'alamat' => $alamat,
			'jenis_pasien' => $jenis_pasien,
			'jurusan' => $jurusan,
			'lembaga_fakultas' => $lembaga_fakultas,
			'kewarganegaraan' => $kewarganegaraan,
			'foto' => $foto,
			'no_telepon' => $no_telepon,
			'no_identitas' => $no_identitas,
			'username' => $username,
			'password' => $password,
			'tahun_masuk' => $tahun_daftar,
			);

		$datapasien_auth = array(
				'username' => $username, 
				'password' => $password,
				'id_otorisasi' => $id_otorisasi,
			);

		$datapasien = array(
				'data_auth' => $datapasien_auth,
				'data_general' => $datapasien_gen,
			);

		if ($this->user_model->insert_pasien($datapasien))
			return $id;
		else return "";
	}

	public function profilecalonpasien($id) {
		$page = 'profilepengguna';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		else {
			$this->load->model('user_model');
		 	
	    $data['query'] = $this->user_model->get_calon_pasien_data($id);
	    $data['query2'] = null;
	    //$data['query2'] = $this->user_model->get_calon_pasien_additional_data($id);
	    $data['show_button'] = !($this->authentication->is_baru()||$this->authentication->is_mendaftar()||$this->authentication->is_calon_pasien());
	    if ($this->authentication->is_loket()) $data['show_button'] = true;
			$data['show_button_verifikasi'] = false;
			$data['title'] = "Profil Pasien"; 
			$data['role'] = $this->user_model->get_calon_pasien_role($id);

	    if ($this->authentication->is_loket()) {
	    	$data['msg2'] = "";
	    }
			else 
			{
				$data['msg2'] = "
          <div class=\"row\">
            <div class=\"message message-yellow\">
              <p class=\"p_message\">Data Anda berhasil dikirim! 
							</br>Untuk mendapatkan username dan password,
							</br>silakan datang ke PKM UI dan melakukan verifikasi.</p>
            </div>
          </div>";
			}
					
			$this->load->view('templates/header', $data);
	        $this->load->model('authentication');
			if($this->authentication->is_loggedin()){
	            $this->load->view('usermenu/usermenu', $data);
	        }
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
	}

  public function char_dot_num($str) {

		if (! preg_match("/^([a-z.0-9])+$/i", $str))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

  public function char_dash_only($str) {

		if (! preg_match("/^([-a-z .,'])+$/i", $str))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function char_dash_num_only($str) {

		if (! preg_match("/^([-a-z .,0-9\/])+$/i", $str))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function char_space_only($str) {

    if (! preg_match("/^([a-z ])+$/i", $str))
    {
			return FALSE;
    }
    else
    {
			return TRUE;
    }
	}

  public function random($length) 
  {
  	$key = "";
    $keys = array_merge(range(0,9), range('a', 'z'), range('A', 'Z'));
    for($i=0; $i < $length; $i++) {
        $key = $key.$keys[array_rand($keys)];
    }
    return $key;
	}

	public function is_unique_username($username) {
		if ($this->user_model->is_unique_username($username)) return TRUE;
		else return FALSE;
	}

  public function random_string($length) 
  {
  	$key = "";
  	$keys = range('a', 'z');
    $keys0 = array('c','q','f','x','v','z');
    $keys1 = array('a','i','u','e','o');
  	$keys2 = array_diff($keys, array_merge($keys0,$keys1));
    
    for($i=0; $i < $length; $i++) {
    		if ($i%2 == 1) {
    			$key = $key.$keys1[array_rand($keys1)];
    		}
    		else {
    			$key = $key.$keys2[array_rand($keys2)];
    		}
    }
    return $key;
	}

	public function sukses($data)
	{
		$page = "sukses";

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
  public function checkRecaptcha() {
    // initialize the variables needed to check the recaptcha
    $privatekey = '6LcqBOkSAAAAAOe3Z2gbQ7t7YtuxLCCqE3638psl';
    $resp = recaptcha_check_answer ($privatekey,
                                    $_SERVER["REMOTE_ADDR"],
                                    $_POST["recaptcha_challenge_field"],
                                    $_POST["recaptcha_response_field"]);
    // if the value entered in the recaptcha is incorrect, set the error message then return false
    if (!$resp->is_valid) {
        $this->form_validation->set_message('checkRecaptcha', "Kata yang Anda ketikkan tidak cocok, silakan mencoba kembali. ");
        // returning false will set $this->form_validation->run() to false
        return false;
    } else {
        // returning false will set $this->form_validation->run() to true
        return true;
    }
	}

}

/* End of file  */
/* Location: ./application/controllers/ */
