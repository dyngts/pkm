<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class Jadwalharian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('jadwal_model');
	}

	public function index()
	{
		redirect('site/antre');
	}

	public function edit($id)
	{
		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);

		$data['tanggal'] = $tanggal;

		if($id == NULL) redirect('jadwalharian');
		$data['id'] = $id;
		$this->form_edit_jadwal($data);
	}

	public function process_edit($id){

		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);

		$data['tanggal'] = $tanggal;

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'nama dokter', 'required');
		$this->form_validation->set_rules('hari', 'hari praktik', 'required');
		$this->form_validation->set_rules('jamMulai', 'jam mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'menit mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'jam selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'menit selesai', 'required');
		$this->form_validation->set_rules('poli', 'jenis poli', 'required');
		
		$this->load->model('jadwal_model');
		$data['id'] = $id;

		if ($this->form_validation->run() == TRUE)
		{

			$data['jadwal_data'] = $this->jadwal_model->get_jadwal_harian_by_id($data['id'],$tanggal);
			$data['id_jadwal'] = $data['jadwal_data']['id_mingguan'];

			$sukses = $this->process_update($data);
			if ($sukses['is_konflik'] == FALSE)
			{
				$data['show_msg_sukses'] = !$sukses['is_konflik'];
			}
			else
			{
				$data['show_msg_overlap'] = $sukses['is_overlap'];
				$data['show_msg_konflik'] = $sukses['is_konflik'];
			}
			$this->form_edit_jadwal($data);
		}
		else $this->form_edit_jadwal($data);

	}

	private function form_edit_jadwal($data) {
		$page = 'form_edit_jadwal_harian';

		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}

		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);

    $data['tanggal'] = $tanggal;
		$data['jadwal_data'] = $this->jadwal_model->get_jadwal_harian_by_id($data['id'],$tanggal);
		$data['id_jadwal'] = $data['jadwal_data']['id_mingguan'];

		$data['dateparts'] = date_parse($data['jadwal_data']['waktu_aktual_awal']);
		$data['datepartsend'] = date_parse($data['jadwal_data']['waktu_aktual_akhir']);	

		$doctor = $this->user_model->get_all_doctor();

		$data['doctor_name'] = $doctor;
		$data['title'] = "Buat Jadwal Praktik Dokter Mingguan"; 

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);

	}

	private function process_update($data_in)
	{


		date_default_timezone_set('Asia/Jakarta');
    $datestring = "%Y-%m-%d";
    $time = time();
    $tanggal = mdate($datestring, $time);

		$waktu_awal = $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktu_akhir = $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$jenis_poli = $this->input->post('poli');

		$username_dokter = $this->input->post('nama');
		$id_jadwal = $data_in['id_jadwal'];

		$data_update['username_dokter'] = $username_dokter;
		$data_update['id_jadwal'] = $data_in['id_jadwal'];
		$data_update['tanggal'] = $tanggal;
		$data_update['waktu_awal'] = $waktu_awal;
		$data_update['waktu_akhir'] = $waktu_akhir;

		$data_konflik = $this->periksa_konflik_jadwal($data_update);
		if($data_konflik['is_konflik']) return $data_konflik;

		return $this->update($data_update);
	}

	public function periksa_konflik_jadwal($data_in)
	{
		$waktu_awal = $data_in['waktu_awal'];
		$waktu_akhir = $data_in['waktu_akhir'];

		if ($waktu_awal >= $waktu_akhir) 
		{
			$data['is_konflik'] = TRUE; 
			$data['is_overlap'] = TRUE;

			return $data;
		}
		else 
		{
			$is_konflik = $this->periksa_konflik_adv($data_in);
			
			$data['is_konflik'] = $is_konflik; 
			$data['is_overlap'] = FALSE; 

			return $data;
		}
	}


	private function periksa_konflik_adv($data)
	{

		$multithread = true;

		if(isset($data['id_jadwal']))
		$id_jadwal = $data['id_jadwal'];
		else $id_jadwal = "";

		$tanggal = $data['tanggal'];
		$waktu_awal = $data['waktu_awal'];
		$waktu_akhir = $data['waktu_akhir'];
		$username_dokter = $data['username_dokter'];

		$data['base_table'] = SCHEMA."jadwal_harian";
		$data['join'] = array(
			array(
				'table' => SCHEMA."dokter_harian",
				'const' => 'id_harian = id_mingguan',
				),
			);
		$data['const'] = array(
				"jadwal_harian.tanggal = '".$tanggal."'",
				//'id_layanan = '.$id_layanan,
			); 

		$out = $this->jadwal_model->get_jadwal_konflik($data);

		if ($out == NULL) return $result = false;
		else
		{
			if ($multithread)
			{
				$result = false;
				foreach ($out as $key) {
					if($key['waktu_aktual_awal'] <= $waktu_awal && $key['waktu_aktual_akhir'] > $waktu_awal) 
					{
						if($key['id_mingguan']!=$id_jadwal && $key['username_dokter'] == $username_dokter) $result = $result || true;
					}
					elseif($key['waktu_aktual_awal'] >= $waktu_awal && $key['waktu_aktual_awal'] < $waktu_akhir)
					{
						if($key['id_mingguan']!=$id_jadwal && $key['username_dokter'] == $username_dokter) $result = $result || true;
					}
				}
			}
			else
			{
				$result = false;
				foreach ($out as $key) {
					if($key['waktu_aktual_awal'] <= $waktu_awal && $key['waktu_aktual_akhir'] > $waktu_awal) 
					{
						if($key['id_mingguan']!=$id_jadwal) $result = $result || true;
					}
					elseif($key['waktu_aktual_awal'] >= $waktu_awal && $key['waktu_aktual_awal'] < $waktu_akhir)
					{
						if($key['id_mingguan']!=$id_jadwal) $result = $result || true;
					}
				}
			}
		}

		return $result;
	}

	private function update($data_in)
	{
		$data['base_table'] = SCHEMA."jadwal_harian";
		$data['data_in'] = array(
			'waktu_aktual_awal' => $data_in['waktu_awal'],
			'waktu_aktual_akhir' => $data_in['waktu_akhir'],
			);
		$data['const'] = array(
			"id_mingguan = ".$data_in['id_jadwal'],
			"tanggal = '".$data_in['tanggal']."'",
			);
		//simpan ke database
		$this->jadwal_model->update_jadwal($data);

		//save lagi
		$data2['base_table'] = SCHEMA."dokter_harian";
		$data2['data_in'] = array(
			'username_dokter' => $data_in['username_dokter'],
			);
		$data['const'] = array(
			"id_harian = ".$data_in['id_jadwal'],
			"tanggal = '".$data_in['tanggal']."'",
			);
		//simpan ke database
		return $this->jadwal_model->update_jadwal($data2);
	}

	public function get_jadwal_harian()
	{
		return $this->jadwal_model->get_jadwal_harian();
	}
}

/* End of file  */
/* Location: ./application/controllers/ */
