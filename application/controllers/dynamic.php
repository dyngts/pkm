<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('obat_model');
	}

	public function index()
	{
		$this->load->view('dynamic_dropdown');
	}

	public function ambil($jenis) {
		$this->obat_model->_tampil_daftar_pilihan($jenis);
	}

}

/* End of file dynamic.php */
/* Location: ./application/controllers/dynamic.php */