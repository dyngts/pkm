<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan cas
 */
class Cas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();


		// Load the settings from the central config file
		require_once 'CAS/config.php';
		// Load the CAS lib
		require_once 'CAS/lib/CAS.php';
		
		// Initialize phpCAS
		phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);


		// For production use set the CA certificate that is the issuer of the cert
		// on the CAS server and uncomment the line below
		//phpCAS::setCasServerCACert($cas_server_ca_cert_path);

		// For quick testing you can disable SSL validation of the CAS server.
		// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
		// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
		phpCAS::setNoCasServerValidation();
	}

	public function index()
	{

		// force CAS authentication
		phpCAS::forceAuthentication();

		$attr = phpCAS::getAttributes();

		$username = $this->user_model->get_user_name_by_username(phpCAS::getUser());
		$userdata = $this->user_model->get_user_by_username($username);

		if ($userdata != NULL)
		{
			$this->authentication->set_session_data_pasien(phpCAS::getUser());
			redirect('site');
	  }
		else
		{
      $data = array(
        'is_baru' => true,
        'is_loggedin' => true,
        'username' => phpCAS::getUser(),
	      'is_civitas' => true,
        );	

      $this->session->set_userdata($data);

			if ( ! isset($attr['nip'])) redirect('register/mahasiswa');
			else redirect('register/karyawan');
		}
	}

	function logout() 
	{
		phpCAS::logout();
		redirect('site/do_logout');
	}

}

/* End of file  */
/* Location: ./application/controllers/ */
