<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faktur_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('faktur_model');
		$this->get_model('obat_model');
	}

	function index($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

		$this->load->library('pagination');
		$config['base_url'] = base_url().'/faktur_controller/index';
		$config['total_rows'] = $this->faktur_model->_hitung_jumlah_data('faktur');
    		$this->pagination->initialize($config);
    		$data['pagination_links'] = $this->pagination->create_links();
    		$data['mulai'] = $mulai;
    		$data['error'] = "";
    		$data['jumlah_baris'] = $config['total_rows'];
    		$data['fakturs'] = $this->faktur_model->_ambil_semua_baris('faktur', 30, $mulai);
    		$data['daftar_supplier'] = $this->obat_model->_ambil_daftar_supplier();
    		$data['nama_aktif'] = 'faktur';
		$this->cetak_halaman('faktur/_faktur', $data);
	}

	function do_upload($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));


			$this->load->library('upload');
			
			$config['upload_path'] = './assets/faktur/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1000';

			$this->upload->initialize($config);

			if (!$this->upload->do_upload()) {
				$this->load->library('pagination');
				$config['base_url'] = base_url().'/faktur_controller/index';
				$config['total_rows'] = $this->faktur_model->_hitung_jumlah_data('faktur');
    			$this->pagination->initialize($config);
    			$data['pagination_links'] = $this->pagination->create_links();
    			$data['mulai'] = $mulai;
    			$data['error'] = $this->upload->display_errors();
    			$data['jumlah_baris'] = $config['total_rows'];
    			$data['fakturs'] = $this->faktur_model->_ambil_semua_baris('faktur', 10, $mulai);

    			$data['daftar_supplier'] = $this->obat_model->_ambil_daftar_supplier();
    			$data['nama_aktif'] = 'faktur';
    			$this->cetak_halaman('faktur/_faktur', $data);
			}
			else {
			
			$this->load->library('form_validation');

			$aturan_tambah = array(
		 	   array(
                     	'field'   => 'id_supplier',
                     	'label'   => 'Nama Supplier',
                     	'rules'   => 'trim|required'
                  	   ),
			);

			$this->form_validation->set_rules($aturan_tambah); 
		
			if($this->form_validation->run() == TRUE) {
				if($_POST) {
				$data_gambar = $this->upload->data();
					chmod("./assets/faktur/".$data_gambar['file_name'], 0755);

					$data = array(
						'nama_faktur'=>$data_gambar['file_name'],
						'tgl_upload'=>date('d-M-Y'),
						'id_supplier'=>$this->input->post('id_supplier')
					);

				$this->faktur_model->_tambah($data, 'faktur');

				$data['nama_file'] = $data_gambar['file_name'];
				$data['status'] = 'baru';
				$data['nama_aktif'] = 'faktur';
				return $this->cetak_halaman('faktur/_faktur_preview', $data);
				}
			}
				
				$this->load->library('pagination');
				$config['base_url'] = base_url().'/faktur_controller/index';
				$config['total_rows'] = $this->faktur_model->_hitung_jumlah_data('faktur');
    			$this->pagination->initialize($config);
    			$data['pagination_links'] = $this->pagination->create_links();
    			$data['mulai'] = $mulai;
    			$data['error'] = $this->upload->display_errors();
    			$data['jumlah_baris'] = $config['total_rows'];
    			$data['fakturs'] = $this->faktur_model->_ambil_semua_baris('faktur', 10, $mulai);

    			$data['daftar_supplier'] = $this->obat_model->_ambil_daftar_supplier();
    			$data['nama_aktif'] = 'faktur';
    			$this->cetak_halaman('faktur/_faktur', $data);
			
			}
	}

	public function hapus($id_faktur) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

		$where = array('id_faktur'=>$id_faktur);
		$faktur = $this->faktur_model->_ambil_baris_tertentu($where, 'faktur', 'row');

		$path = PUBPATH.'/assets/faktur/'.$faktur->nama_faktur;

		unlink($path);
		$this->faktur_model->_hapus($where, 'faktur');
		redirect(site_url('faktur_controller/index'));
	}

	public function preview($id_faktur) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

		$where = array('id_faktur'=>$id_faktur);
		$faktur = $this->faktur_model->_ambil_baris_tertentu($where, 'faktur', 'row');

		$data['status'] = 'lama';
		$data['nama_file'] = $faktur->nama_faktur;
		$data['nama_aktif'] = 'faktur';
		$this->cetak_halaman('faktur/_faktur_preview', $data);
	}
}

/* End of file faktur_controller.php */
/* Location: ./application/controllers/faktur_controller.php */