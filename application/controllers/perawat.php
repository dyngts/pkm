<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perawat extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->load->model('perawatmod');
	}

	function pola_penyakit_pdf() {
		$this->load->helper('file'); 
		$this->load->helper(array('dompdf', 'file'));
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$laporanPolaPenyakit = $this->perawatmod->getPolaPenyakit($bulan, $tahun);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['laporanPolaPenyakit'] = $laporanPolaPenyakit->result_array();
		$html = $this->load->view('perawat/printables/laporan_pola_penyakit', $data, true);	   
		$filename = 'laporan_pola_penyakit_'.$bulan.'_'.$tahun;
		pdf_create_simpel($html, $filename);
	}

	function tindakan_poligigi_pdf() {
		$this->load->helper('file'); 
		$this->load->helper(array('dompdf', 'file'));
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$laporanTindakanGigi = $this->perawatmod->getJenisTindakan($bulan,$tahun);
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['laporanTindakanGigi'] = $laporanTindakanGigi->result_array();
		$html = $this->load->view('perawat/printables/laporan_tindakan_poligigi', $data, true);	  
		$filename = 'laporan_tindakan_poli_gigi_'.$bulan.'_'.$tahun;   
		pdf_create_simpel($html, $filename);
	}
	
	function laporan_kinerja_pdf(){
		$this->load->helper('file'); 
		$this->load->helper(array('dompdf', 'file'));
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$dokter = $this->perawatmod->getDokter();
		$data['bulan'] = $bulan;
	    $data['tahun'] = $tahun;
		$data['dokter'] = $dokter;
		$html = $this->load->view('perawat/printables/laporan_kinerja', $data, true);
		$filename = 'laporan_kinerja_dokter'.$bulan.'_'.$tahun;  
		pdf_create_simpel($html, $filename);
	}

	/*-----------------------------------Print Views--------------------------------------*/
	function print_polapenyakit(){
		$bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanPolaPenyakit = $this->perawatmod->getPolaPenyakit($bulan, $tahun);
	    $data['bulan'] = $bulan;
	    $data['tahun'] = $tahun;
	    $data['laporanPolaPenyakit'] = $laporanPolaPenyakit->result_array();
	    $this->load->view('perawat/printables/laporan_pola_penyakit', $data);
	}

	function print_poligigi(){
		$bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
	    $laporanTindakanGigi = $this->perawatmod->getJenisTindakan($bulan,$tahun);
	    $data['bulan'] = $bulan;
	    $data['tahun'] = $tahun;
	    $data['laporanTindakanGigi'] = $laporanTindakanGigi->result_array();
	    $this->load->view('perawat/printables/laporan_tindakan_poligigi', $data);
	}
	
	function print_laporankinerja(){
		$bulan = $this->session->userdata('bulan');
	    $tahun = $this->session->userdata('tahun');
		$dokter = $this->perawatmod->getDokter();
		$data['bulan'] = $bulan;
	    $data['tahun'] = $tahun;
		$data['dokter'] = $dokter;
		$this->load->view('perawat/printables/laporan_kinerja', $data);
	}


	/*-----------------------------------wakacauwakacau--------------------------------------*/

	/*================================START OF XLS========================================*/
	function bulanan_pola_penyakit_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->perawatmod->getPolaPenyakit($bulan, $tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Pola Penyakit PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN BULANAN POLA PENYAKIT";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'JENIS PENYAKIT');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'JUMLAH KASUS BARU');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JUMLAH KASUS LAMA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'JUMLAH TOTAL');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THICK,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:F6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(54);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(23);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(23);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);

        $i = 1; 
        
        foreach($query->result_array() as $data) {
        	$jmlkasusbaru = 0;
	    	$jmlkasuslama = 0;
	    	$total = 0;
        	
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['jenis_penyakit']);
            
            $query2 = $this->perawatmod->getKasusBaru($data['kode_diagnosis'], $bulan, $tahun);
	        $query3 = $this->perawatmod->getKasusLama($data['kode_diagnosis'], $bulan, $tahun);

	        foreach ($query2->result_array() as $data2){
				if($data['kode_diagnosis'] == $data2['kode_diagnosis']){
	                $jmlkasusbaru = $data2['kasus_baru'];                    			
           		}
	        }

	        foreach ($query3->result_array() as $data3){
				if($data['kode_diagnosis'] == $data3['kode_diagnosis']){
	                $jmlkasuslama = $data3['kasus_lama'];                    			
           		}
	        }

	        $total = $jmlkasusbaru + $jmlkasuslama;

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $jmlkasusbaru);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $jmlkasuslama);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $total);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':F'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;    
        }

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+3, "Koordinator Administrasi dan Keuangan");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+4, "PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+8, "drg. Marisa Aristiawati H, MKM");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row+9, "NIP 198003062010122002");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Bulanan_Pola_Penyakit_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
        ob_clean();
        $objWriter->save('php://output');

	}


	function bulanan_tindakan_poli_gigi_xls() {
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->perawatmod->getJenisTindakan($bulan, $tahun);
		$fields = $query->list_fields();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Bulanan Tindakan Poli Gigi PKM UI".$bulan)->setDescription("none");
 
        $baseurl = base_url();
        $this->excel->setActiveSheetIndex(0);

        // Head
        $title = "LAPORAN BULANAN TINDAKAN POLI GIGI";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'JENIS TINDAKAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'MAHASISWA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'DOSEN/KARYAWAN');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 6, 'TAMU');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 6, 'JUMLAH');
        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THICK,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:G6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);

        $i = 1; 
        foreach($query->result_array() as $data) {
        	$jmlmahasiswa = 0;
	        $jmlkaryawan = 0;
			$jmltamu = 0;
	        $total = 0;
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['nama']);
            
            $query2 = $this->perawatmod->getMahasiswa($bulan, $tahun);
	        $query3 = $this->perawatmod->getKaryawan($bulan, $tahun);
	        $query4 = $this->perawatmod->getTamu($bulan, $tahun);

	        foreach ($query2->result_array() as $data2){
				if($data['id_tindakan'] == $data2['id_tindakan']){
	                $jmlmahasiswa = $data2['mahasiswa'];                    			
           		}
	        }

	        foreach ($query3->result_array() as $data3){
				if($data['id_tindakan'] == $data3['id_tindakan']){
	                $jmlkaryawan = $data3['karyawan'];                    			
           		}
	        }

	        foreach ($query4->result_array() as $data4){
				if($data['id_tindakan'] == $data4['id_tindakan']){
	                $jmltamu = $data4['tamu'];                    			
           		}
	        }

	        $total = $jmlmahasiswa + $jmlkaryawan + $jmltamu;

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $jmlmahasiswa);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $jmlkaryawan);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $jmltamu);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $total);
            $this->excel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->applyFromArray($bodystyle);
            $i++;
            $row++;    
        }

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+3, "Koordinator Administrasi dan Keuangan");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+4, "PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+7, "drg. Marisa Aristiawati H, MKM");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row+8, "NIP 198003062010122002");

        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Tindakan_Poli_Gigi_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');

	}

	function laporan_kinerja_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->perawatmod->getDokter();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Kinerja Bulanan PKM UI".$bulan)->setDescription("none");
 
        $this->excel->setActiveSheetIndex(0);
        // Head
        $title = "LAPORAN KINERJA DOKTER";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 7;

        //Table Head
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'KODE ICD 10');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'JENIS PENYAKIT');

        //diapus kalo template dah jalan

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THICK,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:D6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);

        $i = 1; 
        foreach($query as $data) {
        	$getnama = $this->perawatmod->getNamaDokter($data['username_dokter']);
			$namadokter = $getnama->nama;
    		$pasienditangani = $this->perawatmod->getKinerja($data['username_dokter'], $bulan, $tahun);
    		$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $i);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $namadokter);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $pasienditangani->count);
        	$this->excel->getActiveSheet()->getStyle('B'.$row.':D'.$row)->applyFromArray($bodystyle); 
    		$i++;
    		$row++; 
        }

        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+3, "Koordinator Administrasi dan Keuangan");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+4, "PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+8, "drg. Marisa Aristiawati H, MKM");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+9, "NIP 198003062010122002");
        // Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Kinerja_Dokter_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');
	}

	function bulanan_data_kesakitan_xls(){
		$bulan = $this->session->userdata('bulan');
		$tahun = $this->session->userdata('tahun');
		$query = $this->perawatmod->getLaporanPenyakitUmur($bulan, $tahun);
		$query2 = $this->perawatmod->getLaporanPenyakitKasus($bulan, $tahun);
		$datakesakitan = $query->result_array();
		$datakesakitanstatus = $query2->result_array();
		$this->load->library('excel');
     
        $this->excel->getProperties()->setTitle("Laporan Data Kesakitan Bulanan PKM UI".$bulan)->setDescription("none");
 
        $this->excel->setActiveSheetIndex(0);
        // Head
        $title = "LAPORAN DATA KESAKITAN";
        $title2 = "PUSAT KESEHATAN MAHASISWA UNIVERSITAS INDONESIA (PKM UI) DEPOK";
        $subtitle = "BULAN: ".$bulan." ".$tahun;
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $title);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $title2);
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
 
        // Fetching the table data
        $col = 2;
        $row = 9;

        //Table Head
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->mergeCells('C6:C8');
        $this->excel->getActiveSheet()->mergeCells('D6:D8');
        $this->excel->getActiveSheet()->mergeCells('E6:T6');

        $this->excel->getActiveSheet()->mergeCells('E7:F7');
        $this->excel->getActiveSheet()->mergeCells('G7:H7');
        $this->excel->getActiveSheet()->mergeCells('I7:J7');
        $this->excel->getActiveSheet()->mergeCells('K7:L7');
        $this->excel->getActiveSheet()->mergeCells('M7:N7');
        $this->excel->getActiveSheet()->mergeCells('O7:P7');
        $this->excel->getActiveSheet()->mergeCells('Q7:R7');
        $this->excel->getActiveSheet()->mergeCells('S7:T7');

        $this->excel->getActiveSheet()->mergeCells('U6:W7');
        $this->excel->getActiveSheet()->mergeCells('X6:Z7');
        $this->excel->getActiveSheet()->mergeCells('AA6:AA8');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, 6, 'NO');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, 'NAMA DOKTER');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, 6, 'JENIS PENYAKIT');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 6, 'JUMLAH KASUS BARU MENURUT GOLONGAN UMUR');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 7, '0-4 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 7, '5-9 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 7, '10-14 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 7, '15-19 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, 7, '20-44 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, 7, '45-54 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, 7, '55-59 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, 7, '>=60 th');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(15, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(17, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(19, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, 6, 'KASUS BARU');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(21, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(22, 8, 'JML');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(23, 6, 'KASUS LAMA');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(23, 8, 'L');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(24, 8, 'P');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(25, 8, 'JML');
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow(26, 6, 'JUMLAH');

        $headerstyle = array (
        				'font' => array(
		                    'bold' => true,
		                    'size' => 12,
                		),
                		'alignment' => array(
		                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		                ),
		                'borders' => array(
		                    'outline' => array(
		                        'style' => PHPExcel_Style_Border::BORDER_THICK,
		                        'color' => array('argb' => 'FF000000'),
		                    )
               			)
        			);

        $bodystyle = array (
        			'borders' => array(
	                    'outline' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    'inside' => array(
	                        'style' => PHPExcel_Style_Border::BORDER_THIN,
	                        'color' => array('argb' => 'FF000000'),
	                    ),
	                    
	                ),
        		);
        $this->excel->getActiveSheet()->getStyle('B6:AA6')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getStyle('B7:AA7')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getStyle('E7:T7')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getStyle('B8:AA8')->applyFromArray($headerstyle);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(40);
        
        $no = 1;
        foreach ($datakesakitan as $data) {
   		 	$count1 = 0;
           	$count2  = 0;
           	$count3  = 0;
           	$count4  = 0;
           	$count5  = 0;
           	$count6  = 0;
           	$count7  = 0;
           	$count8 = 0;
           	$fcount1 = 0;
           	$fcount2  = 0;
           	$fcount3  = 0;
           	$fcount4  = 0;
           	$fcount5  = 0;
           	$fcount6  = 0;
           	$fcount7  = 0;
           	$fcount8 = 0;
           	$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $no);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data['kode_diagnosis']);
        	$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data['jenis_penyakit']);
       		
       		if($data['kategori'] == '0-4'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount1 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count1 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '5-9'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount2 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count2 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '10-14'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount3 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count3 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '15-19'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount4 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count4 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '20-44'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount5 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count5 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '45-54'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount6 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count6 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '55-59'){
       			if($data['jenis_kelamin'] == 'P'){
       				$fcount7 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count7 = $data['jumlah'];
       			}
       		}

       		elseif($data['kategori'] == '>60'){
       			if($data['jenis_kelamin'] == 'P'){
       				$count8 = $data['jumlah'];
       			}
       			else if ($data['jenis_kelamin'] == 'L'){ 
       				$count8 = $data['jumlah'];
       			}
       		} 
       		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $count1);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $fcount1);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $count2);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $fcount2);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $count3);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $fcount3);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $count4);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+9, $row, $fcount4);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row, $count5);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row, $fcount5);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+12, $row, $count6);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+13, $row, $fcount6);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+14, $row, $count7);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+15, $row, $fcount7);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+16, $row, $count8);
	   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row, $fcount8);
       		$no++;
    		$row++; 
       	}
   		$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row+2, "Depok,");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row+3, "Koordinator Administrasi dan Keuangan");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row+4, "PKM UI");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row+8, "drg. Marisa Aristiawati H, MKM");
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+17, $row+9, "NIP 198003062010122002");
   		
   		// Styling
        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
        $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
        $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
        $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

        // header and footer
        //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
        $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

        $this->excel->setActiveSheetIndex(0);
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Data_Kesakitan_'.$bulan.'_'.$tahun.'.xls"');
        header('Cache-Control: max-age=0');
 
        ob_clean();
        $objWriter->save('php://output');
	}

	/*===============================================END OF XLS==================================================*/

	public function notifikasi() {
		$data['title'] = 'Print Sukses';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('perawat/notifikasi');
		$this->load->view('templates/footer', $data);
	}
}