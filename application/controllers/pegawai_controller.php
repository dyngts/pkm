<?php

class pegawai_controller extends CI_Controller {
   
   
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
		$this->load->model('authentication');
		$this->load->model('antrian_model');
		$this->load->library('pagination');
		$this->load->helper('form');
		$this->load->library('javascript');
		$this->load->library('calendar');
		$this->load->helper('date');
		$this->load->helper('text');
		$this->load->library('form_validation');
		$this->load->model('pegawai_model');
		$this->load->model('user_model');
		
	
    }

	public function registrasipegawai($page = 'registrasipegawai') {
		if ( ! file_exists('application/views/pegawai/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_pelayanan()) {
			if (!$this->pegawai_model->firsttime())
			{
				show_404();
			}
			else
			{
				$data['title'] = "Form Pegawai";
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/loginform', $data);
				$this->load->view('pegawai/firsttime', $data);
				$this->load->view('templates/footer', $data);
			}
		}
		else 
		{		
			$data['title'] = "Form Pegawai"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('pegawai/'.$page, $data);
			$this->load->view('templates/footer', $data);	
		}
	}

	public function registrasipegawaisuccess($page = 'registrasipegawaisuccess') {
		if ( ! file_exists('application/views/pegawai/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_pelayanan() && !$this->authentication->is_pj()) {
			show_404();
		}
		$data['title'] = "Profil Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('pegawai/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function pegawailist() {
		$page = 'pegawailist';
		if ( ! file_exists('application/views/pegawai/'.$page.'.php')) {
					show_404();
		}
		if(!$this->authentication->is_pelayanan() && !$this->authentication->is_pj()) {
			show_404();
		}
		else{

        // load data
		$this->load->model('pegawai_model');
        $semuapegawai = $this->pegawai_model->daftarpegawaimodel();
		//$data ['all']= $semuapegawai->result();
        // generate pagination
		
        $config['base_url'] = site_url('/pegawai_controller/pegawailist/');
        $config['total_rows'] = $semuapegawai->num_rows();
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['first_link'] = 'Pertama';
		$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';
		$config['last_link'] = 'Terakhir';
		
        $page_num = ($this->uri->segment(3))? $this->uri->segment(3):0;
		//$data ['totalrow'] = $page_num;
		$data['page_num']= $page_num;
        $this->pagination->initialize($config);
		
		$data['row'] = $this->pegawai_model->get_paged_list($config['per_page'], $page_num)->result();
        //$semuapegawai = $this->pegawai_model->daftarpegawaimodel();
		//$data ['row']= $semuapegawai->result();
        $data['pagination'] = $this->pagination->create_links();
		$data['title'] = "Daftar Pegawai"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('pegawai/'.$page,$data);
		$this->load->view('templates/footer', $data);
		}
	}

	public function profilepegawai($id) {
		
		if ( ! file_exists('application/views/pegawai/profilepegawai.php')) {
					show_404();
		}

		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->user_model->get_my_id() == $id) {
		
			$this->load->model('pegawai_model');
			$data['row'] = $this->pegawai_model->getprofilepegawai($id);
			$data['title'] = "Profil Pegawai"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('pegawai/profilepegawai', $data);
			$this->load->view('templates/footer', $data);
		}
		else
		{
			show_404();
		}
	}

	public function editpegawai($id) {
		if ( ! file_exists('application/views/pegawai/editpegawai.php')) {
					show_404();
		}
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->user_model->get_my_id() == $id) {
			
			$data['title'] = "Edit Profil"; 
			$this->load->model('pegawai_model');
			$data['row'] = $this->pegawai_model->getprofilepegawai($id);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('pegawai/editpegawai', $data);
			$this->load->view('templates/footer', $data);
		}
		else show_404();
	}
/*
	public function editpegawaisukses($page = 'editpegawaisukses') {
		if ( ! file_exists('application/views/pegawai/'.$page.'.php')) {
					show_404();
		}
		$data['title'] = "Edit Profil"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('pegawai/'.$page, $data);
		$this->load->view('templates/footer', $data);
	} */
	
	public function add_pegawai (){


		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->pegawai_model->firsttime()) {

			$this->load->model('pegawai_model');
			// validasi input liat library CI
			$this->form_validation->set_message('required', '%s wajib diisi');
			$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
			$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
			$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
			$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
			$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, strip, garis miring, dan spasi');
			$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
			$this->form_validation->set_message('is_natural', '%s harus angka');
			$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');
			$this->form_validation->set_message('unique', '%s telah digunakan orang lain');
			
			$this->load->helper(array('form', 'url'));
			$this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required|max_length[32]|callback_char_dash_only');
			$this->form_validation->set_rules('alamat','Alamat', 'trim|required|callback_char_dash_num_only');
			$this->form_validation->set_rules('tempatLahir','Tempat Lahir','trim|required|callback_char_space_only');
			$this->form_validation->set_rules('tglLahir','Tanggal Lahir', 'required');
			$this->form_validation->set_rules('sex','Jenis Kelamin', 'required');
			$this->form_validation->set_rules('agama','Agama', 'required');
			$this->form_validation->set_rules('status','Status Pernikahan', 'required');
			$this->form_validation->set_rules('kewarga','Kewarganegaraan', 'trim|required|callback_char_space_only');
			$this->form_validation->set_rules('golDarah','Golongan Darah');
			$this->form_validation->set_rules ('peran','Peran', 'required');
			$this->form_validation->set_rules ('notelepon','Telepon', 'required|is_natural|min_length[5]');
			$this->form_validation->set_rules ('username','Username', 'required|min_length[4]|callback_char_dash_num_only|callback_unique');
			
			
			
			//cek apakah validasi bernilai true
			if ($this->form_validation->run() == FALSE) 
			{

				if ($this->pegawai_model->firsttime())
				{
					$data['title'] = "Registrasi Pasien"; 
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/loginform', $data);
					$this->load->view('pegawai/firsttime');
					$this->load->view('templates/footer', $data);
				}

				else
				{
					$data['title'] = "Registrasi Pasien"; 
					$this->load->view('templates/header', $data);
					if ($this->pegawai_model->is_firsttime())
					{
						$this->load->view('usermenu/loginform', $data);
					}
					else $this->load->view('usermenu/usermenu', $data);
					$this->load->view('pegawai/registrasipegawai');
					$this->load->view('templates/footer', $data);
				}
				
				
			}
			else
			{  
				$peran = $this->input->post('peran');

				$id = $this->buat_id($peran);
				$last_id = $this->pegawai_model->get_last_id();
				foreach ($last_id->result() as $last):
					$integer_id =$last->last_id_pegawai;
				endforeach;
				$id_pegawai = $id.$integer_id."";
				$integer_id = $integer_id+1;
				$this->pegawai_model->update_id($integer_id);
				
				$datapegawai=array(
					'id'=> $id_pegawai,
					'password_pegawai' => random_string ('alnum',10),
					'username' =>$this->input->post('username'),
					'nama' =>$this->input->post('nama'),
					'jeniskelamin'=>$this->input->post('sex'),
					'goldarah'=>$this->input->post('golDarah'),
					'kewarganegaraan'=>$this->input->post('kewarga'),
					'tempatlahir'=>$this->input->post('tempatLahir'),
					'tanggallahir'=>$this->input->post('tglLahir'),
					'agama'=>$this->input->post('agama'),
					'alamat'=>$this->input->post('alamat'),
					'statuspernikahan'=>$this->input->post('status'),
					'peran'=> $peran,
					'no_telepon'=>$this->input->post('notelepon')
					);
				/*
					//cek duplikasi
				$data['name']= $datapegawai['nama'];
				$nama_data= $this->pegawai_model->getnama($datapegawai['nama']);
				$int = $this->pegawai_model(get_sufix);
				if($nama_data->num_rows()>0){
						$datapegawai['nama'] = $datapegawai['nama']."0"."".$int;
						$this->pegawai_model->update_sufix($int);
					}

				else{ */
				
				$config = array(
				'upload_path' => './foto/',
				'allowed_types' => 'gif|jpg|png',
				'max_size'	=> '200',
				'max_width'  => '500',
				'max_height' => '500',
				'file_name' => $datapegawai['id'].".jpg",
				'overwrite' => TRUE
				);

				$this->load->library('upload', $config);
			
				if ($_FILES && $_FILES ['foto']['name'] !== "") {
					
					if ( $this->upload->do_upload('foto'))
					{
						$data_array = $this->upload->data();
						//$this->resize_image (base_url(),$data_array['file_name'],$data_array['image_width'],$data_array['image_height']);
						$datapegawai['Foto']= $data_array['file_name'];
						
					}
					else
					{ 
						$error = $this->upload->display_errors();
						
					}
				}

				$this->load->model('pegawai_model');
				$data['title'] = "Registrasi Pegawai";
				$this->pegawai_model->insert($datapegawai);
				$this->load->view('templates/header', $data);

				if ($this->pegawai_model->is_firsttime())
				{
					$this->load->view('usermenu/loginform', $data);
				}
				else
				{
					$this->load->view('usermenu/usermenu', $data);
				}
				
				$this->load->view('pegawai/registrasipegawaisuccess', $datapegawai);
				$this->load->view('templates/footer', $data);
			}
		}

		else
		{
			show_404();
		}
	}
	
	function _check_select ($str){
		if ($str == 'Pilih Salah Satu' )
		{
			$this->form_validation->set_message('_check_select', 'Field ini harus diisi');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function updatepegawai($id){
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->user_model->get_my_id() == $id) {


			$this->form_validation->set_message('required', '%s wajib diisi');
			$this->form_validation->set_message('max_length', '%s tidak boleh lebih dari %d karakter');
			$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');
			$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
			$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
			$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
			$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
			$this->form_validation->set_message('is_natural', '%s harus angka');
			$this->form_validation->set_message('is_natural_no_zero', '%s harus angka dan tidak boleh 0');
			$this->form_validation->set_message('only_me', '%s sudah digunakan orang lain');
			
			$this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|max_length[32]|required|callback_char_dash_only');
			$this->form_validation->set_rules('alamat','Alamat', 'trim|required|callback_char_dash_num_only');
			$this->form_validation->set_rules('tempatLahir','Tempat Lahir', 'trim|required|callback_char_space_only');
			$this->form_validation->set_rules('tglLahir','Tanggal Lahir', 'required');
			$this->form_validation->set_rules('sex','Jenis Kelamin', 'required');
			$this->form_validation->set_rules('agama','Agama', 'required');
			$this->form_validation->set_rules('status_pernikahan','Status Pernikahan', 'required');
			$this->form_validation->set_rules('kewarga','Kewarganegaraan', 'required|callback_char_space_only');
			$this->form_validation->set_rules('golDarah','Golongan Darah');
			$this->form_validation->set_rules ('no_telepon','Telepon', 'min_length[5]|required|is_natural');
			$this->form_validation->set_rules ('username','Username', 'required|min_length[4]|callback_char_dash_num_only');
			
			if ($this->form_validation->run() == FALSE){
				$data['title'] = "Edit Pegawai"; 
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$data['row']=$this->pegawai_model->getprofilepegawai($id);
				$this->load->view('pegawai/editpegawai',$data);
				$this->load->view('templates/footer', $data);
			
			}
			elseif ($this->only_me($this->input->post('username'),$id))
			{
				
				$data=array(
				'username' =>$this->input->post('username'),
				'nama' =>$this->input->post('nama'),
				'jeniskelamin'=>$this->input->post('sex'),
				'goldarah'=>$this->input->post('golDarah'),
				'kewarganegaraan'=>$this->input->post('kewarga'),
				'tempatlahir'=>$this->input->post('tempatLahir'),
				'tanggallahir'=>$this->input->post('tglLahir'),
				'agama'=>$this->input->post('agama'),
				'alamat'=>$this->input->post('alamat'),
				'statuspernikahan'=>$this->input->post('status-pernikahan'),
				'no_telepon'=>$this->input->post('no_telepon')
				);
				
				$config = array(
					'upload_path' => './foto/',
					'allowed_types' => 'gif|jpg|png',
					'max_size'	=> '200',
					'max_width'  => '500',
					'max_height' => '500',
					'file_name' => $id.".jpg",
					'overwrite' => TRUE
					);

					$this->load->library('upload',$config);
				
					if ($_FILES && $_FILES ['foto']['name'] !== ""){
						
						if ( $this->upload->do_upload('foto'))
						{
							$data_array = $this->upload->data();
							$data['Foto']= $data_array['file_name'];
						}
						else
						{ 
							$error = $this->upload->display_errors();
							//$this->load->view('pegawai/registrasipegawai', $error);
						}
					}
						//echo 'gamasuk';
					
					$this->load->model('pegawai_model');
					$this->pegawai_model->updatepegawai($id,$data);
					//$databaru['row']=$this->pegawai_model->getprofilepegawai($id);
					//$databaru['row']=$data;
					//$data['title'] = "Edit Pegawai"; 
					//$this->load->view('templates/header', $data);
					//$this->load->view('usermenu/usermenu', $data);
					//$this->load->view('pegawai/editpegawaisukses/',$databaru);
					//$this->load->view('templates/footer', $data);
					$this->profilepegawai($id);
			}
			else {
				$data['title'] = "Edit Pegawai"; 
				$data['msg'] = "
	                  <div class=\"row\">
	                    <div class=\"message message-red\">
	                      <p class=\"p_message\">Username sudah digunakan orang lain</p>
	                    </div>
	                  </div>";
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$data['row']=$this->pegawai_model->getprofilepegawai($id);
				$this->load->view('pegawai/editpegawai',$data);
				$this->load->view('templates/footer', $data);
			}
		}
		else echo show_404();

	}
	
	public function deletepegawai($id){

		if(($this->authentication->is_pelayanan() || $this->authentication->is_pj()) && $this->user_model->get_my_id() != $id) {
			
			$this->load->model('pegawai_model');
			//$data_nama ['nama'] =$this->pegawai_model->getnama($id);
			$this->pegawai_model->deletepegawai($id);
			$data['row'] = $this->pegawai_model->daftarpegawaimodel();
			$data['title'] = "Daftar Pegawai"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('pegawai/deletepegawai',$data);
			$this->load->view('templates/footer', $data);
		}

		else
		{
			show_404();
		}
		
	}
	
	public function buat_id ($string){

		$prefix_id=0;
		if ($string == 'Penanggung Jawab PKM'){
			$prefix_id = '101';
		}
		if ($string == 'Koordinator Pelayanan'){
			$prefix_id = '102';
		}
				if ($string == 'Dokter Umum'){
			$prefix_id = '103';
		}
				if ($string == 'Koordinator Logistik'){
			$prefix_id = '104';
		}
				if ($string == 'Dokter Gigi'){
			$prefix_id = '105';
		}
				if ($string == 'Perawat Gigi'){
			$prefix_id = '106';
		}
				if ($string == 'Perawat Umum'){
			$prefix_id = '107';
		}
				if ($string == 'Kesekretariatan'){
			$prefix_id = '108';
		}
				if ($string == 'Staf Keuangan'){
			$prefix_id = '109';
		}
				if ($string == 'Apoteker'){
			$prefix_id = '100';
		}
				if ($string == 'Asisten Apoteker'){
			$prefix_id = '110';
		}
				if ($string == 'Administrasi Loket'){
			$prefix_id = '111';
		}
				if ($string == 'Koord Administrasi&Keuangan'){
			$prefix_id = '112';
		}
			if ($string == 'Dokter Estetika Medis'){
			$prefix_id = '113';
		}

		return $prefix_id;
	}

	public function editpassword($id) {
	    
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('min_length', '%s tidak boleh kurang dari %d karakter');

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		if ($this->form_validation->run() == TRUE)
		{
			$this->savenewpassword($id);
		}
		else
		{
			$this->formeditpassword($id);
		}	
	       
    }

	public function formeditpassword($id) {
		$page = 'formeditpasswordpegawai';
		if ( ! file_exists('application/views/pegawai/'.$page.'.php')) {
					show_404();
		}

		$this->load->model('pegawai_model');
		
		if ($this->pegawai_model->is_firsttime())
		{
			$data['title'] = "Edit Password"; 
			$data['msg'] = "";
			$data['role'] = $this->pegawai_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/loginform', $data);
			$this->load->view('pegawai/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}

		elseif ($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->pegawai_model->get_my_id() == $id)
		{
		    $data['title'] = "Edit Password"; 
			$data['msg'] = "";
			$data['role'] = $this->pegawai_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('pegawai/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}

		else {
			show_404();
		} 
	}

	public function savenewpassword($id)
	{
		$page = "sukses";
		$this->pegawai_model->updatepassword($id);

		if ($this->pegawai_model->is_firsttime())
		{
			$this->session->sess_destroy();
			$data['title'] = "Edit Password Sukses"; 
			$data['msg'] = "
	              <div class=\"row\">
	                <div class=\"message message-green\">
	                  <p class=\"p_message\">Password untuk ".$id." - ".$this->pegawai_model->get_name($id)." berhasil diperbarui </p>
	                </div>
	              </div>";

			$data['role'] = $this->pegawai_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);

			redirect('site');
		}

		elseif ($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->pegawai_model->get_my_id() == $id)
		{
			$data['title'] = "Edit Password Sukses"; 
			$data['msg'] = "
	              <div class=\"row\">
	                <div class=\"message message-green\">
	                  <p class=\"p_message\">Password untuk ".$id." - ".$this->pegawai_model->get_name($id)." berhasil diperbarui </p>
	                </div>
	              </div>";

			$data['role'] = $this->pegawai_model->get_user_role($id);
			$data['id'] = $id;

			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}

		else {
			show_404();
		} 
		
	}
	
	
	public function char_dash_only($str) {

	    if (! preg_match("/^([-a-z .,'])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_dash_num_only($str) {

	    if (! preg_match("/^([-a-z .,0-9\/])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_space_only($str) {

	    if (! preg_match("/^([a-z ])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_dot_num_only($str) {

	    if (! preg_match("/^([a-z.0-9])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}
	
	public function resize_image($dir, $width, $height){
		$data_array['image_library'] = 'gd2';
		$data_array['source_image'] = "./" . $dir . "/foto";
		$data_array['maintain_ratio'] = TRUE;
		//$data_array['new_image'] = $new_name;
		$data_array['width'] = 200;
		$data_array['height'] = 200;
		$this->load->library('data_array');
		$this->image_lib->initialize($data_array);
		$this->image_lib->resize();
	}

    public function unique($str) 
    {

        if ($this->pegawai_model->is_unique($str))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function only_me($str,$id) 
    {
        return $this->pegawai_model->is_only_me($str,$id);
    }
}
