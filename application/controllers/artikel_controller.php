<?php

class artikel_controller extends CI_Controller {
   
    function __construct()
    {
      parent::__construct();
      $this->load->helper('url');
			$this->load->model('authentication');
			$this->load->model('antrian_model');
			$this->load->model('user_model');
			$this->load->library('pagination');
			$this->load->helper('form');
			$this->load->library('javascript');
			$this->load->library('calendar');
			$this->load->helper('date');
			$this->load->library('session');
			$this->load->model('artikel_model');
			date_default_timezone_set('Asia/Jakarta');
    }

	public function article($page = 'article') {
		if ( ! file_exists('application/views/artikel/'.$page.'.php')) {
					show_404();
		}

		$has_create_access = $this->authentication->is_pelayanan()
												 || $this->authentication->is_pj()
												 || $this->authentication->is_loket()
												 || $this->authentication->is_admin();

		if ( ! $has_create_access) redirect('site');

		$data['title'] = "Buat Artikel"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('artikel/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
	
	
	public function editarticle($id) {

		$has_edit_access = $this->authentication->is_pelayanan()
														 || $this->authentication->is_pj()
														 || $this->authentication->is_loket()
														 || $this->authentication->is_admin();
														 

		if ( ! $has_edit_access) redirect('site');

		$this->load->model('artikel_model');
		$data['title'] = "Buat Artikel"; 
		$dataartikel ['row']= $this->artikel_model->get_artikel($id);
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('artikel/editarticle', $dataartikel);
		$this->load->view('templates/footer', $data);
	}
	
	public function readarticle($id) {
		if ( ! file_exists('application/views/artikel/readarticle.php')) {
					show_404();
		}
		$data['title'] = "Artikel"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else {
		$this->load->view('usermenu/loginform', $data);}
		$this->load->model('artikel_model');
		$data['row']= $this->artikel_model->get_artikel($id);
		$this->load->view('artikel/readarticle',$data);
		$this->load->view('templates/footer', $data);
	}
	
	public function add_article(){
		date_default_timezone_set('Asia/Jakarta');
		
		
		$datestring = "%Y %m %d - %h:%i:%s.%u";
		$time = time();
		
		$date = mdate($datestring, $time);
		
		//validasi
		$this->load->library('form_validation');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('no_script', '%s mengandung karakter yang dilarang');

		$this->form_validation->set_rules('judul', 'Judul', 'required|callback_char_dash_num_only');
		//$this->form_validation->set_rules('tag','Kata Kunci', 'required|callback_char_dash_only');
		$this->form_validation->set_rules('teks','Isi','required|xss_clean|');

		
		if ($this->form_validation->run() == FALSE){
			//echo 'tidak valid'; 
		
			$data['title'] = "Buat Artikel"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('artikel/article', $data);
			$this->load->view('templates/footer', $data);
		} 
		else{ 
		//$status = $this->input->post('status');
		$user_id = $this->authentication->get_my_id();
		$data= array(
			'isi'=> $this->input->post('teks'),
			//'tag'=> $this->input->post('tag'),
			'judul' => $this->input->post('judul'),
			'nomor_penulis' => $user_id,
			'status'=> 'draft');
		//set id foto

			//foto
			$config = array(
				'upload_path' => './foto/artikel/',
				'allowed_types' => 'gif|jpg|png|jpeg',
				'max_size'	=> '200',
				'max_width'  => '500',
				'max_height' => '500',
				'file_name' => $time.".jpg",
				'overwrite' => TRUE
				);

			$this->load->library('upload', $config);
			
			if ($_FILES && $_FILES ['foto']['name'] !== "") {
					
				if ( $this->upload->do_upload('foto')){
					$data['foto'] = $time.".jpg";
					$this->artikel_model->add_artikel($data);
					redirect('artikel_controller/articlelist');
				}
				else{ 
					$error = $this->upload->display_errors();
					$data['error'] = $error;
					$data['title'] = "Buat Artikel"; 
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/usermenu', $data);
					$this->load->view('artikel/article', $data);
					$this->load->view('templates/footer', $data);
				}
			}
			else { $this->artikel_model->add_artikel($data);
			
			redirect('artikel_controller/articlelist');}
		} 
	}
	
	public function articlelist() {
		$page = 'articlelist';
		if ( ! file_exists('application/views/artikel/'.$page.'.php')) {
					show_404();
		}

		$has_read_access = $this->authentication->is_pelayanan()
														 || $this->authentication->is_pj()
														 || $this->authentication->is_loket()
														 || $this->authentication->is_admin();
														 
		
		if ( ! $has_read_access) redirect('site');

		$this->load->model('artikel_model');
    $semuaartikel = $this->artikel_model->daftar_artikel();
		//$data ['all']= $semuapegawai->result();
        // generate pagination
		
    $config['base_url'] = site_url('/artikel_controller/articlelist');
    $config['total_rows'] = $semuaartikel->num_rows();
    $config['per_page'] = 5;
    $config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$config['first_link'] = 'Pertama';
		$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';
		$config['last_link'] = 'Terakhir';
		
		$page_num = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['page_num']= $page_num;
		$this->pagination->initialize($config);
		
		$data['row'] = $this->artikel_model->get_paged_list($config['per_page'], $page_num)->result();
		$data['pagination'] = $this->pagination->create_links();
		$data['title'] = "Daftar Artikel"; 
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('artikel/'.$page, $data);
		$this->load->view('templates/footer', $data);
		
	}

	public function calendar(){
		$prefs = array (
               'start_day'    => 'sunday',
               'month_type'   => 'long',
               'day_type'     => 'short'
             );
		$this->load->library('calendar', $prefs);
		$date  = $this->calendar->generate();
		return $date;
	}
	
	public function update_article($id){
		$this->load->library('form_validation');
		$this->form_validation->set_message('alpha', '%s hanya boleh mengandung huruf');
		$this->form_validation->set_message('char_dash_only', '%s hanya boleh mengandung huruf, koma, titik, setrip, dan spasi');
		$this->form_validation->set_message('char_dash_num_only', '%s hanya boleh mengandung huruf, angka, koma, titik, setrip, garis miring, dan spasi');
		$this->form_validation->set_message('char_space_only', '%s hanya boleh mengandung huruf dan spasi');
		$this->form_validation->set_message('no_script', '%s mengandung karakter yang dilarang');

		$this->form_validation->set_rules('judul', 'Judul', 'required|callback_char_dash_num_only');
		//$this->form_validation->set_rules('tag','Kata Kunci', 'required|callback_char_dash_num_only');
		$this->form_validation->set_rules('teks','Isi','required|xss_clean');
		
		$time = time();

		if ($this->form_validation->run() == FALSE){
			//echo 'tidak valid'; 
		
			$data['title'] = "Buat Artikel"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('artikel/article', $data);
			$this->load->view('templates/footer', $data);
		}
		else {
			$data = array(
				'isi'=>$this->input->post ('teks'),
				//'tag'=>$this->input->post ('tag'),
				'judul' =>$this->input->post ('judul')
			);
				//foto
			$config = array(
					'upload_path' => './foto/artikel/',
					'allowed_types' => 'gif|jpg|png|jpeg',
					'max_size'	=> '200',
					'max_width'  => '500',
					'max_height' => '500',
				'file_name' => $time.".jpg",
					'overwrite' => TRUE);

			$this->load->library('upload', $config);
			
			if ($_FILES && $_FILES ['foto']['name'] !== ""){
					
				if ( $this->upload->do_upload('foto')){
					$data['foto'] = $time.".jpg";
					$this->artikel_model->update_artikel($id,$data);
					redirect('artikel_controller/articlelist');
				}
				else{ 
					$error = $this->upload->display_errors();
					$data['error'] = $error;
					$data['title'] = "Artikel"; 
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/usermenu', $data);
					$this->load->view('artikel/article', $data);
					$this->load->view('templates/footer', $data);
				}
			} 
			else 
			{
				$this->artikel_model->update_artikel($id,$data);
	
				$databaru['row']=$this->artikel_model->get_artikel($id);
				//$databaru['foto'] = $data_array['foto'];
				$data['title'] = "Artikel"; 
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('artikel/readarticle', $databaru);
				$this->load->view('templates/footer', $data);
			}
		}
	}
	
	public function ubah_status($id, $status_sekarang){
		date_default_timezone_set('Asia/Jakarta');
		$date = new DateTime();
		$time = $date->format('Y-m-d H:i:s.u');
		if ($status_sekarang == 'draft'){
			//artikel diset statusnya terbit
			$status_sekarang = 'terbit';
			//manggil set status di model
			$data = array('tgl_naik' => $time);
			$this->artikel_model->update_tgl($id, $data);
			$this->artikel_model->set_status($id, $status_sekarang);
			redirect('artikel_controller/readarticle/'.$id);
		}
		else {
			//artikel diset statusnya terbit
			$status_sekarang = 'draft';
			$data = array('tgl_turun' => $time);
			$this->artikel_model->update_tgl($id, $data);
			$this->artikel_model->set_status($id, $status_sekarang);
			redirect('artikel_controller/articlelist');
		}
		//return $status_sekarang;
			
	}
	
	public function deletearticle($id){
		$message = 'Artikel sudah berhasil dihapus';
		$this->load->model('artikel_model');
		$this->artikel_model->delete_artikel($id);
		redirect('artikel_controller/articlelist');
		/*$data['title'] = "Daftar Pegawai";
		$data['row'] = $this->artikel_model->daftar_artikel();
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('artikel/articlelist',$data);
		$this->load->view('templates/footer', $data);*/
		
	}
	
		public function char_dash_only($str) {

	    if (! preg_match("/^([-a-z .,'])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_dash_num_only($str) {

	    if (! preg_match("/^([-a-z .,0-9\/])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function char_space_only($str) {

	    if (! preg_match("/^([a-z ])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function no_script($str) {

	    if (! preg_match("/^([-!?()\"\'+=#@[]:;%a-z .,0-9\/])+$/i", $str))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}
}