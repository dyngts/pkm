<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class Jadwal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('jadwal_model');
	}

	public function index()
	{
		redirect('jadwal/daftar');
	}

	public function daftar()
	{
		redirect('jadwal/mingguan');
	}

	public function mingguan()
	{
		$page = "jadwal_list";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['doctor_table_umum'] = $this->jadwal_model->get_umum_mingguan();
		$data['doctor_table_gigi'] = $this->jadwal_model->get_gigi_mingguan();
		$data['doctor_table_medis'] = $this->jadwal_model->get_estetika_mingguan();

		$data['is_kosong'] = !$this->jadwal_model->is_harian_set();

		$data['msg'] = NULL;
		$data['title'] = "Jadwal Praktik Dokter"; 

		$this->load->view('templates/header', $data);

		if($this->authentication->is_loggedin()){            
			$this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);

		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function harian()
	{
		redirect('antrian');
	}

	public function create()
	{
		$data['init'] = true;
		$this->form_create_jadwal($data);
	}

	public function delete($id)
	{
		$data1['base_table'] = SCHEMA."dokter_mingguan";
		$data1['const'] = array("id_jadwal_mingguan = ".$id);
		$this->jadwal_model->delete_jadwal($data1);

		$data2['base_table'] = SCHEMA."jadwal_mingguan";
		$data2['const'] = array("id = ".$id);
		$this->jadwal_model->delete_jadwal($data2);

		redirect('jadwal/daftar');
	}

	public function edit($id)
	{
		if($id == NULL) redirect('jadwal/daftar');
		$data['id'] = $id;
		$this->form_edit_jadwal($data);
	}

	public function process(){

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'nama dokter', 'required');
		$this->form_validation->set_rules('hari', 'hari praktik', 'required');
		$this->form_validation->set_rules('jamMulai', 'jam mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'menit mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'jam selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'menit selesai', 'required');
		$this->form_validation->set_rules('poli', 'jenis poli', 'required');
		
		$this->load->model('jadwal_model');

		$data['query'] = null;

		if ($this->form_validation->run() == TRUE)
		{
			$sukses = $this->process_input($data);
			if ($sukses['is_konflik'] == FALSE)
			{
				$data['show_msg_sukses'] = !$sukses['is_konflik'];
			}
			else
			{
				$data['show_msg_overlap'] = $sukses['is_overlap'];
				$data['show_msg_konflik'] = $sukses['is_konflik'];
			}
			$this->form_create_jadwal($data);
		}
		else $this->form_create_jadwal($data);

	}

	private function form_create_jadwal($data) {
		$page = 'form_jadwal';

		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}
		
		$this->load->model('jadwal_model');

		$doctor = $this->user_model->get_all_doctor();

		$data['doctor_name'] = $doctor;
		$data['title'] = "Buat Jadwal Praktik Dokter Mingguan"; 

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);

	}

	private function process_input($data_in)
	{

		$nomor_dokter = $this->input->post('nama');
		$data_save['nomor_dokter'] = $nomor_dokter;

		$hari = $this->input->post('hari');
		$waktu_awal = $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktu_akhir = $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$jenis_poli = $this->input->post('poli');

		$data_save['hari'] = $hari;
		$data_save['waktu_awal'] = $waktu_awal;
		$data_save['waktu_akhir'] = $waktu_akhir;
		$data_save['id_layanan'] = $jenis_poli;

		$data_konflik = $this->periksa_konflik_jadwal($data_save);
		if($data_konflik['is_konflik']) return $data_konflik;

		return $this->save($data_save);
	}

	public function process_edit($id){

		$this->form_validation->set_error_delimiters('<div class="row"><div class="message message-red">', '</div></div>');
		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_rules('nama', 'nama dokter', 'required');
		$this->form_validation->set_rules('hari', 'hari praktik', 'required');
		$this->form_validation->set_rules('jamMulai', 'jam mulai', 'required');
		$this->form_validation->set_rules('menitMulai', 'menit mulai', 'required');
		$this->form_validation->set_rules('jamSelesai', 'jam selesai', 'required');
		$this->form_validation->set_rules('menitSelesai', 'menit selesai', 'required');
		$this->form_validation->set_rules('poli', 'jenis poli', 'required');
		
		$this->load->model('jadwal_model');
		$data['id'] = $id;

		if ($this->form_validation->run() == TRUE)
		{

			$data['jadwal_data'] = $this->jadwal_model->get_by_id($data['id']);
			$data['id_jadwal'] = $data['jadwal_data']['id'];

			$sukses = $this->process_update($data);
			if ($sukses['is_konflik'] == FALSE)
			{
				$data['show_msg_sukses'] = !$sukses['is_konflik'];
			}
			else
			{
				$data['show_msg_overlap'] = $sukses['is_overlap'];
				$data['show_msg_konflik'] = $sukses['is_konflik'];
			}
			$this->form_edit_jadwal($data);
		}
		else $this->form_edit_jadwal($data);

	}

	private function form_edit_jadwal($data) {
		$page = 'form_edit_jadwal';

		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}

		$data['jadwal_data'] = $this->jadwal_model->get_by_id($data['id']);
		$data['id_jadwal'] = $data['jadwal_data']['id'];

		$data['dateparts'] = date_parse($data['jadwal_data']['waktu_awal']);
		$data['datepartsend'] = date_parse($data['jadwal_data']['waktu_akhir']);	

		$doctor = $this->user_model->get_all_doctor();

		$data['doctor_name'] = $doctor;
		$data['title'] = "Buat Jadwal Praktik Dokter Mingguan"; 

		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);

	}

	private function process_update($data_in)
	{

		$hari = $this->input->post('hari');
		$waktu_awal = $this->input->post('jamMulai') . ":" . $this->input->post('menitMulai') . ":00";
		$waktu_akhir = $this->input->post('jamSelesai') . ":" . $this->input->post('menitSelesai') . ":00";
		$jenis_poli = $this->input->post('poli');

		$nomor_dokter = $this->input->post('nama');
		$id_jadwal = $data_in['id_jadwal'];

		$data_update['nomor_dokter'] = $nomor_dokter;
		$data_update['id_jadwal'] = $data_in['id_jadwal'];
		$data_update['hari'] = $hari;
		$data_update['waktu_awal'] = $waktu_awal;
		$data_update['waktu_akhir'] = $waktu_akhir;
		$data_update['id_layanan'] = $jenis_poli;

		$data_konflik = $this->periksa_konflik_jadwal($data_update);
		if($data_konflik['is_konflik']) return $data_konflik;

		return $this->update($data_update);
	}

	public function periksa_konflik_jadwal($data_in)
	{
		$waktu_awal = $data_in['waktu_awal'];
		$waktu_akhir = $data_in['waktu_akhir'];

		if ($waktu_awal >= $waktu_akhir) 
		{
			$data['is_konflik'] = TRUE; 
			$data['is_overlap'] = TRUE;

			return $data;
		}
		else 
		{
			$is_konflik = $this->periksa_konflik_adv($data_in);
			
			$data['is_konflik'] = $is_konflik; 
			$data['is_overlap'] = FALSE; 

			return $data;
		}
	}


	private function periksa_konflik_adv($data)
	{

		$multithread = FALSE;

		if(isset($data['id_jadwal']))
		$id_jadwal = $data['id_jadwal'];
		else $id_jadwal = "";

		$hari = $data['hari'];
		$waktu_awal = $data['waktu_awal'];
		$waktu_akhir = $data['waktu_akhir'];
		$id_layanan = $data['id_layanan'];
		$nomor_dokter = $data['nomor_dokter'];

		$data['base_table'] = SCHEMA."jadwal_mingguan";
		$data['join'] = array(
			array(
				'table' => SCHEMA."dokter_mingguan",
				'const' => 'id = id_jadwal_mingguan',
				),
			);
		$data['const'] = array(
				'hari = '.$hari,
				//'id_layanan = '.$id_layanan,
			); 

		$out = $this->jadwal_model->get_jadwal_konflik($data);

		if ($out == NULL) return $result = false;
		else
		{
			if ($multithread)
			{
				$result = false;
				foreach ($out as $key) {
					if($key['waktu_awal'] <= $waktu_awal && $key['waktu_akhir'] > $waktu_awal) 
					{
						if($key['id']!=$id_jadwal && $key['nomor_dokter'] == $nomor_dokter) $result = $result || true;
					}
					elseif($key['waktu_awal'] >= $waktu_awal && $key['waktu_awal'] < $waktu_akhir)
					{
						if($key['id']!=$id_jadwal && $key['nomor_dokter'] == $nomor_dokter) $result = $result || true;
					}
				}
			}
			else
			{
				$result = false;
				foreach ($out as $key) {
					if($key['waktu_awal'] <= $waktu_awal && $key['waktu_akhir'] > $waktu_awal) 
					{
						if($key['id']!=$id_jadwal) $result = $result || true;
					}
					elseif($key['waktu_awal'] >= $waktu_awal && $key['waktu_awal'] < $waktu_akhir)
					{
						if($key['id']!=$id_jadwal) $result = $result || true;
					}
				}
			}
		}

		return $result;
	}

	private function save($data_in)
	{
		$data['base_table'] = SCHEMA."jadwal_mingguan";
		$data['data_in'] = array(
			'hari' => $data_in['hari'],
			'waktu_awal' => $data_in['waktu_awal'],
			'waktu_akhir' => $data_in['waktu_akhir'],
			'id_layanan' => $data_in['id_layanan'],
			);
		//simpan ke database
		$this->jadwal_model->insert_jadwal($data);

		//ambil last id
		$id_jadwal_mingguan = $this->jadwal_model->get_last_jadwal_mingguan_id();

		//save lagi
		$data2['base_table'] = SCHEMA."dokter_mingguan";
		$data2['data_in'] = array(
			'nomor_dokter' => $data_in['nomor_dokter'],
			'id_jadwal_mingguan' => $id_jadwal_mingguan,
			);
		//simpan ke database
		return $this->jadwal_model->insert_jadwal($data2);
	}

	private function update($data_in)
	{
		$data['base_table'] = SCHEMA."jadwal_mingguan";
		$data['data_in'] = array(
			'hari' => $data_in['hari'],
			'waktu_awal' => $data_in['waktu_awal'],
			'waktu_akhir' => $data_in['waktu_akhir'],
			'id_layanan' => $data_in['id_layanan'],
			);
		$data['const'] = array(
			"id = ".$data_in['id_jadwal'],
			);
		//simpan ke database
		$this->jadwal_model->update_jadwal($data);

		//save lagi
		$data2['base_table'] = SCHEMA."dokter_mingguan";
		$data2['data_in'] = array(
			'nomor_dokter' => $data_in['nomor_dokter'],
			);
		$data['const'] = array(
			"id_jadwal_mingguan = ".$data_in['id_jadwal'],
			);
		//simpan ke database
		return $this->jadwal_model->update_jadwal($data2);
	}

	public function get_jadwal_mingguan()
	{
		return $this->jadwal_model->get_jadwal_mingguan();
	}

	public function get_jadwal_harian()
	{
		return $this->jadwal_model->get_jadwal_harian();
	}

	public function generate()
	{
		$this->jadwal_model->generate_jadwal_harian();
		redirect('jadwal/mingguan');
	}
}

/* End of file  */
/* Location: ./application/controllers/ */
