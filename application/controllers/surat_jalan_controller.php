<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surat_jalan_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('surat_jalan_model');
		$this->get_model('obat_model');
	}

	public function index() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		//$this->load->view('surat_jalan/_surat_jalan');
		$this->load->view('home');
	}

	public function data_dropdown_nama($jenis) {
		$this->obat_model->_daftar_pilihan_nama($jenis);
	}

	public function data_jenis() {
		$this->obat_model->_data_jenis();
	}

	public function tambah() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));
		// $this->load->library('form_validation');

		// $aturan_tambah = array(
		// 	   array(
  //                    'field'   => 'penerima',
  //                    'label'   => 'Penerima',
  //                    'rules'   => 'trim|required|xss_clean'
  //                 ),
		// 	   array(
  //                    'field'   => 'jenis[]',
  //                    'label'   => 'Jenis Obat',
  //                    'rules'   => 'trim|required'
  //                 ),
  //              array(
  //                    'field'   => 'nama_obat[]',
  //                    'label'   => 'Nama Obat',
  //                    'rules'   => 'trim|required'
  //                 ),
  //              array(
  //                    'field'   => 'sediaan[]',
  //                    'label'   => 'Sediaan',
  //                    'rules'   => 'trim|required'
  //                 ),
  //              array(
  //                    'field'   => 'jumlah_keluar[]',
  //                    'label'   => 'Jumlah Unit Obat Keluar',
  //                    'rules'   => 'trim|required|numeric|greater_than[0]|callback__cek_stok_obat'
  //                 ),
  //           );

		$this->load->library('form_validation');

		$this->form_validation->set_rules('no_surat_jalan', 'Nomor Surat Jalan', 'trim|callback__cek_no_surat_jalan');

		if($this->input->post('kembali', TRUE)) {
			redirect(site_url('/surat_jalan_controller/tambah'));
		}

		//$this->form_validation->set_rules($aturan_tambah);

		if($this->form_validation->run() == TRUE) {
			if($_POST) {
				
				$penerima = $this->security->xss_clean($this->input->post('penerima', TRUE));
				$nama_obat = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
				$sediaan = $this->security->xss_clean($this->input->post('sediaan', TRUE));
				$jumlah_keluar = $this->security->xss_clean($this->input->post('jumlah_keluar', TRUE));
				$deskripsi = $this->security->xss_clean($this->input->post('deskripsi', TRUE));
				$jenis_penerima = $this->security->xss_clean($this->input->post('jenis_penerima', TRUE));
				$no_surat_jalan = $this->security->xss_clean($this->input->post('no_surat_jalan', TRUE));
			
				$total_input = count($nama_obat);

				$nama_habis = array();
				$stok_gudang = array();
				$stok_butuh = array();

				for($i = 0;$i<$total_input;++$i) {
					$total_stok_temp = $this->surat_jalan_model->_cek_total_obat($nama_obat[$i]);
					
					if($total_stok_temp < $jumlah_keluar[$i]) {
						$nama_habis[$i] = $nama_obat[$i];
						$stok_gudang[$i] = $total_stok_temp;
						$stok_butuh[$i] = $jumlah_keluar[$i];
					}
				}

				$data_habis = array();
				$data_habis['nama_habis'] = $nama_habis;
				$data_habis['stok_gudang'] = $stok_gudang;
				$data_habis['stok_butuh'] = $stok_butuh;

				if(count($nama_habis) > 0) {
					return $this->cetak_halaman('surat_jalan/_notifikasi_stockout', $data_habis);
					die();
				}

				date_default_timezone_set('Asia/Jakarta');
				$data_log_keluar = array();
				$data_log_keluar['deskripsi'] = $deskripsi;
				$data_log_keluar['tgl'] = date('d-M-Y');
				$data_log_keluar['jam'] = date("H:i:s");
				$data_log_keluar['jenis'] = $jenis_penerima;
				$data_log_keluar['username_pemberi'] = $this->session->userdata('username');
				if($jenis_penerima == 'Event') {
					$data_log_keluar['no_surat_jalan'] = null;
				}
				else {
					$data_log_keluar['no_surat_jalan'] = $no_surat_jalan;
				}
				
				$data_log_keluar['penerima'] = $penerima;
				$data_log_keluar['tgl_penerimaan'] = date('d-M-Y');
				$data_log_keluar['id_resep'] = NULL;

				$this->surat_jalan_model->_tambah($data_log_keluar, $this->t_log_obat_keluar); 
				
				for($i = 0; $i < $total_input; ++$i) {

					$array_nama_obat[$i] = $nama_obat[$i];
					$array_jumlah_keluar[$i] = $jumlah_keluar[$i];
					$array_sediaan[$i] = $sediaan[$i];

					$jumlah_stok_keluar = $jumlah_keluar[$i];

					$kad_obats = $this->surat_jalan_model->_ambil_baris_tertentus($nama_obat[$i]); 

					for($j = 0;($jumlah_stok_keluar != 0); $j++) {
					
						if($jumlah_stok_keluar <= $kad_obats[$j]->jml_satuan) {

							$total_stok_baru = array();
							$stok_baru = array();
							$stok_baru['jml_satuan'] = $kad_obats[$j]->jml_satuan - $jumlah_stok_keluar; 
							$this->surat_jalan_model->_ubah(array('nama_obat'=>$kad_obats[$j]->nama_obat, 'tgl_kadaluarsa'=>$kad_obats[$j]->tgl_kadaluarsa), $stok_baru, $this->t_stok_obat );
						
							$jumlah_stok_keluar = 0;
						}
						else {
							$total_stok_baru = array();
							$stok_baru = array();							
							$stok_baru['jml_satuan'] = 0;
							
							$jumlah_stok_keluar = $jumlah_stok_keluar - $kad_obats[$j]->jml_satuan;
							$this->surat_jalan_model->_ubah(array('nama_obat'=>$kad_obats[$j]->nama_obat, 'tgl_kadaluarsa'=>$kad_obats[$j]->tgl_kadaluarsa), $stok_baru, $this->t_stok_obat );
						}

						
					}

					$obat = $this->surat_jalan_model->_ambil_baris_tertentu(array('nama'=>$nama_obat[$i]), $this->t_obat, 'row');

					$obat_stok_baru = array();
					$obat_stok_baru['total_satuan'] = ($obat->total_satuan - $jumlah_keluar[$i]);
					$this->surat_jalan_model->_ubah(array('nama'=>$nama_obat[$i]), $obat_stok_baru, $this->t_obat); 

					$log_obat_keluar = $this->surat_jalan_model->_ambil_log_keluar_terbaru();
					$total_sisa = $this->surat_jalan_model->_cek_total_obat($nama_obat[$i]);

					$pengeluaran_obat = array();
					$pengeluaran_obat['id_log_obat_keluar'] = $log_obat_keluar->id;
					$pengeluaran_obat['nama_obat'] = $kad_obats[$j]->nama_obat;
					$pengeluaran_obat['jumlah'] = $jumlah_keluar[$i];
					$pengeluaran_obat['sisa_obat'] = $obat_stok_baru['total_satuan'];

					$this->surat_jalan_model->_tambah($pengeluaran_obat, $this->t_pengeluaran_obat);
				}

				date_default_timezone_set('Asia/Jakarta');
				
				$data['tgl'] = date('dMY');
				$data['jam'] = date('H:i:s');
				$data['jenis'] = $jenis_penerima;

				$jam = date('His');
				
				$temp = explode(' ', $penerima);
				$penerima_asli = '';

				for($i = 0; $i < count($temp); ++$i) {
					if($i > 0) {
						$penerima_asli = $penerima_asli.'_'.$temp[$i];
					}
					else {
						$penerima_asli = $temp[$i];
					}	
				}
				
				$data['nama_surat_jalan'] = $jam.'_'.$data['tgl'].'_'.$penerima_asli;

				$datas = array();

				//Format Tanggal
				$tanggal = date('d');
 
				//Array Bulan
				$array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
				$bulan = $array_bulan[date('n')];
 
				//Format Tahun
				$tahun = date('Y');

				$datas['nama_surat_jalan'] = $data['nama_surat_jalan'];
				$datas['tgl_pembuatan'] = $tanggal.' '.$bulan.' '.$tahun;
				$datas['penerima'] = $penerima;
				$datas['pembuat_surat_jalan'] = $this->session->userdata('username');
				$datas['nama_obats'] = $array_nama_obat;
				$datas['sediaans'] = $array_sediaan;
				$datas['jumlah_keluars'] = $array_jumlah_keluar;
				$datas['jumlah_input'] = $total_input;
				$datas['no_surat_jalan'] = $no_surat_jalan;

				
				/*
				* Sesi generate dokumen dalam bentuk .pdf menggunakan
				* library html2pdf
				*/

				//Load the library
	    		$this->load->library('html2pdf');
	    
	    		//Set folder to save PDF to
	    		$this->html2pdf->folder('./surat_jalan/');
	    
	    		//Set the filename to save/download as
	    		$this->html2pdf->filename($datas['nama_surat_jalan'].'.pdf');
	    
	    		//Set the paper defaults
	   			$this->html2pdf->paper('a3', 'landscape');

	   			$this->html2pdf->html($this->load->view('surat_jalan/_surat_jalan', $datas, true));

	   			$this->html2pdf->create('download');

	   			 // if($this->html2pdf->create('save')) {
	    			// //PDF was successfully saved or downloaded
	    			// //echo 'PDF saved';
	    			// chmod("./surat_jalan/".$data['nama_surat_jalan'].'.pdf', 0755);
	    			// $this->view_surat_jalan($datas['nama_surat_jalan'].'.pdf');
	    		 //  }
			}
		}

		if($_POST) {

			$penerima = $this->security->xss_clean($this->input->post('penerima', TRUE));
			$jenis_penerima = $this->security->xss_clean($this->input->post('jenis_penerima', TRUE));
			$no_surat_jalan = $this->security->xss_clean($this->input->post('no_surat_jalan', TRUE));
			$deskripsi = $this->security->xss_clean($this->input->post('deskripsi', TRUE));
			$nama_obat = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
			$sediaan = $this->security->xss_clean($this->input->post('sediaan', TRUE));
			$jumlah_keluar = $this->security->xss_clean($this->input->post('jumlah_keluar', TRUE));
			
			$total_input = count($nama_obat);

			for($i = 0; $i < $total_input; $i++) {
				$array_nama_obat[$i] = $nama_obat[$i];
				$array_jumlah_keluar[$i] = $jumlah_keluar[$i];
				$array_sediaan[$i] = $sediaan[$i];
			}

			$data = array();
			$data['penerima'] = $penerima;
			$data['jenis_penerima'] = $jenis_penerima;
			$data['no_surat_jalan'] = $no_surat_jalan;
			$data['deskripsi'] = $deskripsi;
			$data['array_nama_obat'] = $array_nama_obat;
			$data['array_jumlah_keluar'] = $array_jumlah_keluar;
			$data['array_sediaan'] = $array_sediaan;
			$data['username'] = $this->session->userdata('username');
			$data['jumlah_input'] = $total_input;
			$data['mulai'] = 0;
			$data['title'] = 'Daftar Surat Jalan';

			return $this->cetak_halaman('surat_jalan/_daftar_surat_jalan', $data);
		}
		
		
		$data = array();

		$data['data_nama_obat'] = $this->surat_jalan_model->_ambil_daftar_nama_obat();
		$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat_modifikasi();
		$data['data_supplier'] = $this->obat_model->_ambil_daftar_supplier();
		$data['data_sediaan_obat'] = $this->obat_model->_ambil_daftar_sediaan();
		$data['title'] = 'Buat Surat Jalan';
		$this->cetak_halaman('surat_jalan/_buat_surat_jalan', $data);
		
	}

	public function hasil_surat_jalan() {

		if($_POST) {
			$penerima = $this->security->xss_clean($this->input->post('penerima', TRUE));
			$jenis_penerima = $this->security->xss_clean($this->input->post('jenis_penerima', TRUE));
			$no_surat_jalan = $this->security->xss_clean($this->input->post('no_surat_jalan', TRUE));
			$deskripsi = $this->security->xss_clean($this->input->post('deskripsi', TRUE));
			$nama_obat = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
			$sediaan = $this->security->xss_clean($this->input->post('sediaan', TRUE));
			$jumlah_keluar = $this->security->xss_clean($this->input->post('jumlah_keluar', TRUE));
			
			$total_input = count($nama_obat);

			for($i = 0; $i < $total_input; $i++) {
				$array_nama_obat[$i] = $nama_obat[$i];
				$array_jumlah_keluar[$i] = $jumlah_keluar[$i];
				$array_sediaan[$i] = $sediaan[$i];
			}

			$data = array();
			$data['penerima'] = $penerima;
			$data['jenis_penerima'] = $jenis_penerima;
			$data['no_surat_jalan'] = $no_surat_jalan;
			$data['deskripsi'] = $deskripsi;
			$data['array_nama_obat'] = $array_nama_obat;
			$data['array_jumlah_keluar'] = $array_jumlah_keluar;
			$data['array_sediaan'] = $array_sediaan;
			$data['username'] = $this->session->userdata('username');
			$data['jumlah_input'] = $total_input;
			$data['mulai'] = 0;
			$data['title'] = 'Daftar Surat Jalan';

			return $this->cetak_halaman('surat_jalan/_daftar_surat_jalan', $data);
		}

		redirect('surat_jalan_controller/tambah');

	}

	public function _cek_input_teks($str) {

		if($str != null) {
			if(!preg_match('/^[a-zA-Z\s]+$/', $str)) {
				$this->form_validation->set_message('_cek_input_teks', 'Format masukkan mengandung karakter yang tidak diperbolehkan');
				return FALSE;
			}
		}
		return TRUE;
	}

	public function _cek_input_teks2($str) {

		if($str != null) {
			if(!preg_match('/^[0-9a-zA-Z,.-\s]+$/', $str)) {
				$this->form_validation->set_message('_cek_input_teks2', 'Format masukkan mengandung karakter yang tidak diperbolehkan');
				return FALSE;
			}
		}
		return TRUE;
	}

	public function _cek_no_surat_jalan() {
		$no_surat_jalan = $this->security->xss_clean($this->input->post('no_surat_jalan'));

		$nilai = $this->surat_jalan_model->_cek_no_surat_jalan($no_surat_jalan);

		if($nilai == 1) {
			$this->form_validation->set_message('_cek_no_surat_jalan', 'Nomor surat jalan sudah pernah dipakai');
			return FALSE;
		}

		return TRUE;
	}

	public function ajax_cek_no_surat_jalan() {
		$no_surat_jalan = $this->security->xss_clean($this->input->post('no_surat_jalan'));
		$nilai = $this->surat_jalan_model->_cek_no_surat_jalan($no_surat_jalan);

		$json = array();

		if($nilai == 1) {
			$json['nilai'] = 1;
		} else {
			$json['nilai'] = 0;
		}

		echo json_encode($json);
	}

	public function _cek_stok_obat($str) {
		$nama_obat = $this->input->post('nama_obat');
		
		if($nama_obat[0] == '') {
			$this->form_validation->set_message('_cek_stok_obat', 'Nama obat harus dipilih');
			return FALSE;
		}

		$jumlah_keluar = $this->input->post('jumlah_keluar', TRUE);
		$total_input = count($nama_obat);

		$jumlah_nama_obat = array();

		for($i=0;$i<$total_input;++$i) $jumlah_nama_obat[$nama_obat[$i]] = 0; 
 
		for($i = 0;$i<$total_input;++$i) {
			$jumlah_nama_obat[$nama_obat[$i]]++;
			if($jumlah_nama_obat[$nama_obat[$i]]>1) {
				$this->form_validation->set_message('_cek_stok_obat', 'Anda tidak boleh mengeluarkan obat lebih dari satu kali dengan nama obat yang sama');
				return FALSE;
			}
		}

		for($i = 0;$i<$total_input;++$i) {
			$obat_temp = $this->surat_jalan_model->_ambil_baris_tertentu(array('id_obat'=>$nama_obat[$i]), 'propensi.obat', 'row');

			if($obat_temp->total_obat_satuan < $jumlah_keluar[$i]) {
				$this->form_validation->set_message('_cek_stok_obat', 'Stok obat yang ada di gudang tidak mencukupi ');
				return FALSE;
			}
		}
		return TRUE;
	}

	public function kumpulan($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		$hasil = $this->surat_jalan_model->_ambil_semua_surat_jalan($mulai);

		$data['surat_jalans'] = $hasil['daftar_surat_jalan'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/surat_jalan_controller/kumpulan';
		$config['total_rows'] = $hasil['jumlah_baris'];
    	$this->pagination->initialize($config);
    	$data['pagination_links'] = $this->pagination->create_links();
    	$data['mulai'] = $mulai;		
    	$data['title'] = 'surat_jalan';
		$this->cetak_halaman('surat_jalan/_daftar_surat_jalan', $data);
	}

	public function detil($id_surat_jalan) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		$data['surat_jalan'] = $this->surat_jalan_model->_ambil_baris_tertentu(array('id_surat_jalan'=>$id_surat_jalan), 'propensi.surat_jalan', 'row');
		$data['title'] = 'surat_jalan';
		$this->cetak_halaman('surat_jalan/_detil_data_surat_jalan', $data);
	}

	public function hapus($id_surat_jalan) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		$where = array('id_surat_jalan'=>$id_surat_jalan);
		$surat_jalan = $this->surat_jalan_model->_ambil_baris_tertentu($where, 'propensi.surat_jalan', 'row');

		$path = PUBPATH.'/assets/pdfs/'.$surat_jalan->nama_surat_jalan.'.pdf';

		unlink($path);
		$this->surat_jalan_model->_hapus($where, 'propensi.surat_jalan');
		redirect('/surat_jalan_controller/kumpulan');
	}

	public function surat() {
		$this->load->view('surat_jalan/_surat_jalan');
	}



}

/* End of file surat_jalan_controller.php */
/* Location: ./application/controllers/surat_jalan_controller.php */