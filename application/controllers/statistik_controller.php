<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('statistik_model_sipotek');
	}

	public function index()
	{

	}


	public function statistik_umum($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data = array();
		$search_keywords_array = array();

		$hasil = $this->statistik_model_sipotek->_ambil_statistik_umum($mulai, $search_keywords_array);
		$data['statistik_umum'] = $hasil['daftar_statistik'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/statistik_umum';
		$config['total_rows'] = $hasil['jumlah_baris'];
     	$this->pagination->initialize($config);
     	$data['pagination_links'] = $this->pagination->create_links();
     	$data['mulai'] = $mulai;
     	$data['title'] = 'Statistik Umum';
		$this->cetak_halaman('statistik/_statistik_obat_umum', $data);
	}

	public function search_stat_umum($mulai = 0) {
		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

			if(!empty($tanggal_awal)) { 
				$search_keywords_array['tanggal_awal_umum'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_awal_umum',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('tanggal_awal_umum');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['tanggal_akhir_umum'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_akhir_umum',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('tanggal_akhir_umum');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('tanggal_awal_umum');
				$this->session->unset_userdata('tanggal_akhir_umum');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_umum');
			}

		}
		else {
			if($this->session->userdata('tanggal_awal_umum')) {
				$search_keywords_array['tanggal_awal_umum'] = $this->session->userdata('tanggal_awal_umum');
			}

			if($this->session->userdata('tanggal_akhir_umum')) {
				$search_keywords_array['tanggal_akhir_umum'] = $this->session->userdata('tanggal_akhir_umum');
			}
		}

		$data = array();
		$hasil = $this->statistik_model_sipotek->_ambil_statistik_umum($mulai, $search_keywords_array);
		$data['statistik_umum'] = $hasil['daftar_statistik'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/search_stat_umum';
		$config['total_rows'] = $hasil['jumlah_baris'];
		$this->pagination->initialize($config);
		$data['pagination_links'] = $this->pagination->create_links();
		$data['mulai'] = $mulai;
		$data['title'] = 'Statistik Umum';
		$this->cetak_halaman('statistik/_statistik_obat_umum_query', $data);


	}

	
	public function statistik_umum_grafik() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));
		$data['title'] = 'statistik';
		$this->cetak_halaman('statistik/_statistik_obat_umum_grafik', $data);
	}

	public function search_grafik_umum() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));
		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {

			if(!empty($tanggal_awal)) { 
				$search_keywords_array['awal_grafik_umum'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('awal_grafik_umum',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('awal_grafik_umum');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['akhir_grafik_umum'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('akhir_grafik_umum',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('akhir_grafik_umum');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('awal_grafik_umum');
				$this->session->unset_userdata('akhir_grafik_umum');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_umum_grafik');
			}

		}
		else {
			if($this->session->userdata('awal_grafik_umum')) {
				$search_keywords_array['awal_grafik_umum'] = $this->session->userdata('awal_grafik_umum');
			}

			if($this->session->userdata('akhir_grafik_umum')) {
				$search_keywords_array['akhir_grafik_umum'] = $this->session->userdata('akhir_grafik_umum');
			}
		}

		$data['title'] = 'Statistik Grafik Umum';

 		$this->cetak_halaman('statistik/_statistik_obat_umum_grafik_query', $data);
	}

	public function get_data_obat_umum_json($tgl_mulai = '' , $tgl_akhir = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));
		$this->statistik_model_sipotek->_ambil_top_10_umum_json($tgl_mulai, $tgl_akhir);
	}

	public function statistik_pasien($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data = array();
		$hasil = $this->statistik_model_sipotek->_ambil_statistik_pasien($mulai);
		$data['daftar_pasiens'] = $hasil['list_namas'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/statistik_pasien';
		$config['total_rows'] = $hasil['jumlah_baris'];
     	$this->pagination->initialize($config);
     	$data['pagination_links'] = $this->pagination->create_links();
     	$data['mulai'] = $mulai;
     	$data['title'] = 'Statistik Pasien';
     	$data['jumlah_baris'] = $hasil['jumlah_baris'];
		$this->cetak_halaman('statistik/_statistik_obat_pasien', $data);
	}

	public function statistik_detil_pasien($id_pasien, $mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data = array();
		$search_keywords_array = array();
		$hasil = $this->statistik_model_sipotek->_detil_statistik_pasien($id_pasien, $search_keywords_array, $mulai);

		if($hasil['jumlah_baris'] > 0) {
			$nama_pasien = $this->statistik_model_sipotek->_ambil_nama_pasien($id_pasien);
			$data['daftar_obats'] = $hasil['daftar_obats'];
			$this->load->library('pagination');
			$config['base_url'] = base_url().'index.php/statistik_controller/statistik_detil_pasien/'.$id_pasien;
			$config['total_rows'] = $hasil['jumlah_baris'];
			$config['uri_segment'] = 4;
			$config['num_links'] = 4;
			$this->pagination->initialize($config);
			$data['pagination_links'] = $this->pagination->create_links();
			$data['mulai'] = $mulai;
			$data['id_pasien'] = $id_pasien;
			$data['nama_pasien'] = $nama_pasien;
			$data['title'] = 'Statistik Detil Pasien - '.$id_pasien;
			$data['jumlah_baris'] = $hasil['jumlah_baris'];
			$this->cetak_halaman('statistik/_statistik_detil_obat_pasien', $data);
		}
		else {
			show_404();
		}

		
	}

	public function search_stat_pasien($id_pasien = '', $mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

			if(!empty($tanggal_awal)) {  
				$search_keywords_array['tanggal_awal_pasien'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_awal_pasien',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('tanggal_awal_pasien');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['tanggal_akhir_pasien'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_akhir_pasien',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('tanggal_akhir_pasien');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('tanggal_awal_pasien');
				$this->session->unset_userdata('tanggal_akhir_pasien');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_detil_pasien/'.$id_pasien);
			}

		}
		else {
			if($this->session->userdata('tanggal_awal_pasien')) {
				$search_keywords_array['tanggal_awal_pasien'] = $this->session->userdata('tanggal_awal_pasien');
			}

			if($this->session->userdata('tanggal_akhir_pasien')) {
				$search_keywords_array['tanggal_akhir_pasien'] = $this->session->userdata('tanggal_akhir_pasien');
			}
		}


		$data = array();

		$hasil = $this->statistik_model_sipotek->_detil_statistik_pasien($id_pasien, $search_keywords_array, $mulai);

		$nama_asli = $this->statistik_model_sipotek->_ambil_nama_pasien($id_pasien);

		$data['daftar_obats'] = $hasil['daftar_obats'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/statistik_detil_pasien/'.$id_pasien;
		$config['total_rows'] = $hasil['jumlah_baris'];
		$config['uri_segment'] = 4;
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		$data['pagination_links'] = $this->pagination->create_links();
		$data['mulai'] = $mulai;
		$data['nama_pasien'] = $nama_asli;
		$data['id_pasien'] = $id_pasien;
		$data['title'] = 'Statistik Detil Pasien - '.$id_pasien;
		$data['jumlah_baris'] = $hasil['jumlah_baris'];
		$this->cetak_halaman('statistik/_statistik_detil_obat_pasien_query', $data);
	}

	
	public function search_grafik_pasien($id_pasien = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {

			if(!empty($tanggal_awal)) { 
				$search_keywords_array['awal_grafik_pasien'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('awal_grafik_pasien',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('awal_grafik_pasien');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['akhir_grafik_pasien'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('akhir_grafik_pasien',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('akhir_grafik_pasien');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('awal_grafik_pasien');
				$this->session->unset_userdata('akhir_grafik_pasien');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_detil_pasien_grafik/'.$id_pasien);
			}

		}
		else {
			if($this->session->userdata('awal_grafik_pasien')) {
				$search_keywords_array['awal_grafik_pasien'] = $this->session->userdata('awal_grafik_pasien');
			}

			if($this->session->userdata('akhir_grafik_pasien')) {
				$search_keywords_array['akhir_grafik_pasien'] = $this->session->userdata('akhir_grafik_pasien');
			}
		}

		$data = array();

		$data['nama_pasien'] = $this->statistik_model_sipotek->_ambil_nama_pasien($id_pasien);
		$data['id_pasien'] = $id_pasien;
		$data['title'] = 'Statistik Detil Grafik Pasien - '.$id_pasien;
		$this->cetak_halaman('statistik/_statistik_detil_obat_pasien_grafik_query', $data);
	}

	public function statistik_detil_pasien_grafik($id_pasien = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$nama_asli = $this->statistik_model_sipotek->_ambil_nama_pasien($id_pasien);

		$data['nama_pasien'] = $nama_asli;
		$data['id_pasien'] = $id_pasien;
		$data['title'] = 'Statistik Detil Grafik Pasien - '.$id_pasien;
		$this->cetak_halaman('statistik/_statistik_detil_obat_pasien_grafik', $data);
	}

	public function get_data_obat_pasien_json($id_pasien, $tgl_mulai = '', $tgl_akhir = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$nama_asli = $this->statistik_model_sipotek->_ambil_nama_pasien($id_pasien);

		$this->statistik_model_sipotek->_ambil_top_10_pasien_json($id_pasien, $tgl_mulai, $tgl_akhir);
	}
	
	public function statistik_dokter($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data = array();
		
		$hasil = $this->statistik_model_sipotek->_ambil_statistik_dokter($mulai);
		$data['daftar_dokter'] = $hasil['list_namas'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/statistik_dokter';
		$config['total_rows'] = $hasil['jumlah_baris'];
     	$this->pagination->initialize($config);
     	$data['pagination_links'] = $this->pagination->create_links();
     	$data['mulai'] = $mulai;
     	$data['title'] = 'Statistik Dokter';
		$this->cetak_halaman('statistik/_statistik_obat_dokter', $data);
	}

	public function statistik_detil_dokter($username = '', $mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data = array();
		$search_keywords_array = array();
		$hasil = $this->statistik_model_sipotek->_detil_statistik_dokter($username, $search_keywords_array, $mulai);

		if($hasil['jumlah_baris'] > 0) {
			$data['daftar_obats'] = $hasil['daftar_obats'];
			$this->load->library('pagination');
			$config['base_url'] = base_url().'index.php/statistik_controller/statistik_detil_dokter/'.$username;
			$config['total_rows'] = $hasil['jumlah_baris'];
			$config['uri_segment'] = 4;
			$config['num_links'] = 4;
			$this->pagination->initialize($config);
			$data['pagination_links'] = $this->pagination->create_links();
			$data['mulai'] = $mulai;
			$data['username'] = $username;
			$data['title'] = 'Statistik Detil Dokter - '.$username;
			$data['jumlah_baris'] = $hasil['jumlah_baris'];
			$this->cetak_halaman('statistik/_statistik_detil_obat_dokter', $data);
		}
		else {
			show_404();
		}

		
	}

	public function search_stat_dokter($username = '', $mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

			if(!empty($tanggal_awal)) { 
				$search_keywords_array['tanggal_awal_dokter'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_awal_dokter',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('tanggal_awal_dokter');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['tanggal_akhir_dokter'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('tanggal_akhir_dokter',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('tanggal_akhir_dokter');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('tanggal_awal_dokter');
				$this->session->unset_userdata('tanggal_akhir_dokter');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_detil_dokter/'.$username);
			}

		}
		else {
			if($this->session->userdata('tanggal_awal_dokter')) {
				$search_keywords_array['tanggal_awal_dokter'] = $this->session->userdata('tanggal_awal_dokter');
			}

			if($this->session->userdata('tanggal_akhir_dokter')) {
				$search_keywords_array['tanggal_akhir_dokter'] = $this->session->userdata('tanggal_akhir_dokter');
			}
		}

		$data = array();
		$hasil = $this->statistik_model_sipotek->_detil_statistik_dokter($username, $search_keywords_array, $mulai);

		$data['daftar_obats'] = $hasil['daftar_obats'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/statistik_controller/search_stat_dokter/'.$username;
		$config['total_rows'] = $hasil['jumlah_baris'];
		$config['uri_segment'] = 4;
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		$data['pagination_links'] = $this->pagination->create_links();
		$data['mulai'] = $mulai;
		$data['username'] = $username;
		$data['title'] = 'Statistik Detil Dokter - '.$username;
		$data['jumlah_baris'] = $hasil['jumlah_baris'];
		$this->cetak_halaman('statistik/_statistik_detil_obat_dokter_query', $data);
		
	}

	public function statistik_detil_dokter_grafik($username = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$data['username'] = $username;
		$data['title'] = 'Statistik Detil Grafik Dokter - '.$username;
		$this->cetak_halaman('statistik/_statistik_detil_obat_dokter_grafik', $data);
	}

	public function search_grafik_dokter($username = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));
		$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
		$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
		$search_keywords_array = array();

		if($_POST) {

			if(!empty($tanggal_awal)) { 
				$search_keywords_array['awal_grafik_dokter'] =  $tanggal_awal;
				$this->statistik_model_sipotek->searchterm_handler('awal_grafik_dokter',$tanggal_awal); 
			} else {
				$this->session->unset_userdata('awal_grafik_dokter');	
			}

			if(!empty($tanggal_akhir)) { 
				$search_keywords_array['akhir_grafik_dokter'] =  $tanggal_akhir;
				$this->statistik_model_sipotek->searchterm_handler('akhir_grafik_dokter',$tanggal_akhir); 
			} else {
				$this->session->unset_userdata('akhir_grafik_dokter');	
			}

			if($this->input->post("hapus", TRUE)) {
				$this->session->unset_userdata('awal_grafik_dokter');
				$this->session->unset_userdata('akhir_grafik_dokter');
				$search_keywords_array = array();
				redirect('statistik_controller/statistik_detil_dokter_grafik/'.$username);
			}

		}
		else {
			if($this->session->userdata('awal_grafik_dokter')) {
				$search_keywords_array['awal_grafik_dokter'] = $this->session->userdata('awal_grafik_dokter');
			}

			if($this->session->userdata('akhir_grafik_dokter')) {
				$search_keywords_array['akhir_grafik_dokter'] = $this->session->userdata('akhir_grafik_dokter');
			}
		}

		$data = array();

		$data['username'] = $username;
		$data['title'] = 'Statistik Detil Grafik Dokter - '.$username;
		$this->cetak_halaman('statistik/_statistik_detil_obat_dokter_grafik_query', $data);
	}

	public function get_data_obat_dokter_json($username = '', $tgl_mulai = '', $tgl_akhir = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('index.php/home_controller'));

		$this->statistik_model_sipotek->_ambil_top_10_dokter_json($username, $tgl_mulai, $tgl_akhir);
	}

}

/* End of file statistik_controller.php */
/* Location: ./application/controllers/statistik_controller.php */