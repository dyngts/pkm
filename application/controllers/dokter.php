<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dokter extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('doktermod');
		$this->load->model('koorpelmod');
		$this->load->model('user_model');
		$this->load->library('pagination');
		$this->load->helper('html');
		$this->load->library('upload');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		date_default_timezone_set("Asia/Jakarta");
	}

	function rolechecker(){
    	$role = $this->session->userdata('role');
		$access = 0;
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->authentication->is_adm_keu()|| $this->authentication->is_dokter() || $this->authentication->is_perawat()){
			$access = 1;
		}
		if($access == 0) {
            show_404();
        }
        return $access;
    }

	public function pasien() {
		$this->rolechecker();
    	$data['title'] = 'Masukkan ID Pasien';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('dokter/data_p1');
		$this->load->view('templates/footer', $data);
	}


	public function rekam_medis_success() {
		$this->rolechecker();
		$data['title'] = 'Rekam Medis';
		$data['idpasien'] = $this->session->userdata("id_pasien");
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('dokter/rekmed_notif');
		$this->load->view('templates/footer', $data);

	}

	public function lihatrekam() {
		$this->rolechecker();
		$data['title'] = 'Masukkan ID Pasien';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('dokter/data_p2');
		$this->load->view('templates/footer', $data);
	}


	function cekPasien($idpasien) {
		$temp = $this->doktermod->search_rekmed($idpasien);
		if($temp) {
			return true;
		}

		else {
			$this->form_validation->set_message('cekPasien', 'ID yang anda masukkan tidak ada dalam database');
			return false;
		}
	}

	public function rekam_medis_isi(){
		$this->rolechecker();
		$datapasien = $this->doktermod->getIdPasien();
		$perawat = $this->doktermod->getPerawat();
		$datadiagnosa = $this->doktermod->getDiagnosa();
		$dataobat = $this->doktermod->getDataObat();
		$dokter = $this->doktermod->getDokter();
		$datatindakan = $this->doktermod-> retrieveTindakan();
		$data['diagnosa'] = $datadiagnosa;
		$data['perawat'] = $perawat;
		$data['dokter'] = $dokter;
		$data['idpasien'] = $datapasien;
		$data['obat'] = $dataobat;
		$data['tindakan'] = $datatindakan->result_array();
		$data['nama'] = $this->session->userdata('name');
		$data['title'] = 'Buat Rekam Medis';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('dokter/rekam_medis1');
		$this->load->view('templates/footer', $data);
	}


	public function searchrekam_2(){
		$this->rolechecker();
		$this->form_validation->set_rules('idpasien', 'Idpasien', 'strip_tags|max_length[6]');
		$this->form_validation->set_rules('namapasien', 'NamaPasien', 'strip_tags|max_length[50]');
    	if ($this->form_validation->run() == TRUE) {
			$idpasien =  $this->input->post("idpasien",true);
			if($idpasien == null){
				$idpasien = "ADSSFSDSD";
			}
			$namapasien =  $this->input->post("namapasien",true);
			if($namapasien == null){
				$namapasien = "ADSSFSDSD";
			}
			$array =  $this->doktermod->retrieve_rekam_medis($idpasien,$namapasien);
			$data['rekmed'] = $array;
			$data['title'] = 'Hasil Pencarian Rekam Medis';
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('dokter/search_rekmed_results');
			$this->load->view('templates/footer', $data);
		}	
		else {
			echo ("<script language ='JavaScript'>
			window.alert('ID Pasien yang anda masukkan belum benar!') 
			location = '../dokter/lihatrekam';
			</script>");
		}
	}

	function hasil_rekam_medis($idpasien){
		$query =  $this->doktermod->search_rekmed_retrieve($idpasien);
		if ($query != null && gettype($query) != "string") {
			$data['query'] = $query->result_array();
			$data['nama'] = $this->session->userdata("namapasien");
			$data['tempatlahir'] = $this->session->userdata("tempatlahir");
			$data['tanggallahir'] = $this->session->userdata("tanggallahir");
			$data['jeniskelamin'] = $this->session->userdata("jeniskelamin");
			$data['bangsa'] = $this->session->userdata("bangsa");
			$data['lembaga'] = $this->session->userdata("lembaga");
			$data['kawin'] = $this->session->userdata("kawin");
			$data['agama'] = $this->session->userdata("agama");
			$data['alamat'] = $this->session->userdata("alamat");
			$data['gol_darah'] = $this->session->userdata("gol_darah");
			$data['idrekam'] = $this->session->userdata("idrekam");
			$data['title'] = 'Rekam Medis';
			$data['idpasien'] = $idpasien;
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('dokter/rekam_medis2');
			$this->load->view('templates/footer', $data);
		}
		else {
			$data['query'] = $query;
			$data['nama'] = $this->session->userdata("namapasien");
			$data['tempatlahir'] = $this->session->userdata("tempatlahir");
			$data['tanggallahir'] = $this->session->userdata("tanggallahir");
			$data['jeniskelamin'] = $this->session->userdata("jeniskelamin");
			$data['bangsa'] = $this->session->userdata("bangsa");
			$data['lembaga'] = $this->session->userdata("lembaga");
			$data['kawin'] = $this->session->userdata("kawin");
			$data['agama'] = $this->session->userdata("agama");
			$data['alamat'] = $this->session->userdata("alamat");
			$data['gol_darah'] = $this->session->userdata("gol_darah");
			$data['idrekam'] = $this->session->userdata("idrekam");
			$data['title'] = 'Rekam Medis';
			$data['idpasien'] = $idpasien;
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('dokter/rekam_medis2');
			$this->load->view('templates/footer', $data);
		}
	}

	public function form_rekmed(){
		/*if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		echo "<code><pre>";
		$coty = ($_POST);
		print_r ($this->session->userdata("idpasien"));
		print_r ($this->session->userdata("idrekam"));
		print_r($coty);
		echo "</pre></code>";
		exit();
		}
		echo "<code><pre>";
		print_r($_POST);
		echo "</pre></code>";
		exit();*/
		$uploadrontgen = null;
		$uploadlab = null;
		$date = $this->input->post("tanggal", true);
	  	$idrekmed =  $this->input->post("idpasien");
	  	$config['upload_path'] = null;
		$config['allowed_types'] = 'gif|jpg|png|pdf';
		$config['max_size']	= '2048000';
		$config['max_width']  = '4960';
		$config['max_height']  = '4960';
		$config['overwrite'] = true;
		$this->load->model('kasirmod');
		
		
		$postrekam = array (
			'id_rekam_medis' => $idrekmed,
			'waktu' => date("H:i"),
		    'tanggal' => $this->input->post("tanggal", true),
		    'keluhan_utama' => $this->input->post("keluhanutama", true),
		    'anamnesa' => $this->input->post("anamnesa", true),
		    'keterangan_alergi' => $this->input->post("alergi",true),
		    'username_dokter' => $this->input->post("usernamedokter", true),
		    'username_perawat' => $this->input->post("usernameperawat", true)
		);
		$rontgendata = array();
		if($_FILES['rontgen']!=null){
			$files = $_FILES['rontgen'];
			$cpt = count($_FILES['rontgen']['name']);
			$config['upload_path'] = './uploads/rontgen';
			for($i=0; $i<$cpt; $i++) {
				$_FILES['rontgen']['name']= $files['name'][$i];
		        $_FILES['rontgen']['type']= $files['type'][$i];
		        $_FILES['rontgen']['tmp_name']= $files['tmp_name'][$i];
		        $_FILES['rontgen']['error']= $files['error'][$i];
		        $_FILES['rontgen']['size']= $files['size'][$i]; 
		        $config['file_name']  ='rontgen_'.$date.'_'.$idrekmed.'_'.$i; 
		        $this->upload->initialize($config);
		        if ($this->upload->do_upload('rontgen')) {
					$up = array('upload_data' => $this->upload->data());
					$uploadrontgen.= $up['upload_data']['file_name']."_";
					array_push($rontgendata,$up['upload_data']['full_path']);
				}
				else {
					$uploadrontgen = null;
					$error = array('error' => $this->upload->display_errors());
				} 	
			}			
		}
		

		if($_FILES['hasillab'] != null) {
			$config['upload_path'] = './uploads/hasil_lab';
			$config['file_name']  ='hasillab_'.$date.'_'.$idrekmed;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('hasillab')) {
				$up = array('upload_data' => $this->upload->data());
				$uploadlab = $up['upload_data']['file_name'];
			}
			else {
				$uploadlab = null;
				$error = array('error' => $this->upload->display_errors());
			} 	
		}

		
		
		/*============================Mulai Validasi================================*/	
		// Validasi untuk cek rekam medis sudah apa belum
		$temp = $this->doktermod->getRekMed($idrekmed,$this->input->post("tanggal", true), date("H:i:s"));

		if($temp) {
			echo ("<script language ='JavaScript'>
				window.alert('Sudah ada di dalam data base') 
				</script>");
			//redirect('dokter/rekam_medis_isi');
		}
		else {
			//Validasi untuk form yang dibutuhkan untuk diisi
			$this->form_validation->set_error_delimiters('<div class="errors">', '</div>');
			$this->form_validation->set_rules('tanggal','Tanggal','required');
			$this->form_validation->set_rules('keluhanutama','KeluhanUtama','required|strip_tags');
			$this->form_validation->set_rules('anamnesa','Anamnesa','strip_tags');
			$this->form_validation->set_rules('tekanandarah','Tekanandarah','string|required|max_length[7]');
			$this->form_validation->set_rules('suhu','Suhu','alpha_numeric|strip_tags|required');
			$this->form_validation->set_rules('nadi','Nadi','alpha_numeric|strip_tags|required');
			$this->form_validation->set_rules('napas','Napas','alpha_numeric|strip_tags|required');
			$this->form_validation->set_rules('keterangand','Keterangan Diagnosis','strip_tags');
			$this->form_validation->set_rules('keterangant','Keterangan Tindakan','strip_tags');
			

			$this->form_validation->set_message('required', 'Field ini harus diisi');
			
			if($this->form_validation->run()==true) {
				$rekamcheck = $this->doktermod->rekmed_entry($postrekam);
				$no=0;					
				$postpemeriksaan = array (
					'id_rekam_medis' => $idrekmed,
					'tekanan_darah' =>  $this->input->post("tekanandarah", true),
					'frekuensi_suhu' => $this->input->post("suhu",true),
					'frekuensi_nadi' => $this->input->post("nadi", true),
					'frekuensi_pernapasan' => $this->input->post("napas"),
					'tanggal' => $this->input->post("tanggal", true),
					'waktu' => date("H:i"),
					'hasil_lab' => $uploadlab
				);

				$pemeriksaancheck = $this->doktermod->pemeriksaan_insert($postpemeriksaan);
				
				foreach ($rontgendata as $rontgen => $ron) {
					$rontgen = array(
						'id_rekam_medis' => $idrekmed,
						'tanggal' => $this->input->post("tanggal", true),
						'waktu' => date("H:i"),	
						'rontgen'=> $ron
					);
					$rontgentes = $this->doktermod->rontgen_insert($rontgen);
				}
				
				$kodediag = $_POST['kodediag'];
				$statkasus = $_POST['status_kasus'];
				foreach ($kodediag as $kode => $kod) {
					$postdiagnosa = array (
					'kode_diagnosis' => $kod,
					'id_rekam_medis' => $idrekmed,		
					'tanggal' => $this->input->post("tanggal", true),
					'waktu' => date("H:i"),
					'status_diagnosa'=> $statkasus[$no],
					'keterangan_diagnosa' =>$this->input->post("keterangand",true)
					); 
					$this->doktermod->diagnosa_insert($postdiagnosa);
				}
				$diagcheck=true;
			
				$idtindakan = $_POST['idtindakan'];
				foreach ($idtindakan as $idt => $id) {
					$posttindakan = array (
					'id_rekam_medis' => $idrekmed,
					'waktu' => date("H:i"),
					'tanggal' => $this->input->post("tanggal", true),
					'id_tindakan' => $id,
					'keterangan_tindakan' =>$this->input->post("keterangant",true)
					);
					$this->doktermod->tindakan_insert($posttindakan);
				}
				$tindakancheck = true;

				$postresep = array (
					'id_rekam_medis' => $idrekmed,
					'waktu' => date("H:i"),
					'tanggal' => $this->input->post("tanggal", true),
					'status_proses'=> 0
					);	
				$this->doktermod->resep_insert($postresep);
				$resepcheck = true;
				
				if($rekamcheck && $diagcheck && $pemeriksaancheck && $tindakancheck && $resepcheck){
					$waktu = date("H:i");
					$tanggal = $this->input->post("tanggal", true);
					$idresep = $this->doktermod->getIDResep($idrekmed,$tanggal,$waktu);
					$datatebus = array('id_resep' => $idresep,'status_tebus' => 'FALSE');
					$this->doktermod->resep_tebus($datatebus);
					
					$i=0;
					$idobat = $_POST['idobat'];
					$dosisresep = $_POST['dosisresep'];
					$jmlresep = $_POST['jmlresep'];
					foreach ($idobat as $ido => $id) {
						$namaobat = $this->doktermod->getNamaObat($id);
						$detailresep = array(
							'id_resep' => $idresep,
							'nama_obat' => $namaobat,
							'dosis' => $dosisresep[$i],
							'jumlah' => $jmlresep[$i]
						);
						$this->doktermod->detailresep($detailresep);
						$i++;
					}
					
					$datatagihan = array (
						'id_rekam_medis' => $idrekmed,
						'waktu' => date("H:i"),
						'status' => 'FALSE',
						'username_dokter' => $this->input->post("usernamedokter", true),
						'tanggal' => $this->input->post("tanggal", true)
					);
					$bayar = $this->kasirmod->buat_tagihan($datatagihan);
					if ($bayar) {
						redirect("dokter/rekam_medis_success");
					}		
				}
				else {
					echo "<script language ='JavaScript'>
							window.alert('Data yang anda masukkan belum valid!') 
							</script>";
				}
			}

			else {
				$this->rekam_medis_isi();
			}	
		
	  }
	  /*============================Selesai Validasi================================*/	
	}	
	
}
