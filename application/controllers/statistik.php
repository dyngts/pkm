<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class statistik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
    $this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('antrian_model');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('javascript');
		$this->load->model('authentication');
		$this->load->model('statistik_model');

		if ( ! ENABLE_STATISTIK_KUNJUNGAN) 
		{
			redirect('site');
		}
	}

	/**
	 * Menampilkan halaman awal menu statistik
	 */
	public function index()
	{
		// Halaman yang di load
		$page = "stats";
		// Periksa apakah ada dalam folder view
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			// Jika tidak, tampilkan halaman 404
			show_404();
		}

		// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
		$data['firstyear'] = (int)$this->statistik_model->get_first_year();
		$data['lastyear'] = (int)$this->statistik_model->get_last_year();

		// Judul halaman
		$data['title'] = "Statistik";
		// Pesan error (jika ada)
		$data['msg'] = "";

		// load view
		$this->load->view('templates/header', $data); // Header
		// Periksa apakah user telah login
		if($this->authentication->is_loggedin()){
			// Jika ya, tampilan usermenu
            $this->load->view('usermenu/usermenu', $data);
        }
        // Jika tidak, tapilkan form login
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data); // Main page
		$this->load->view('templates/footer', $data); // Footer
	}

	/**
	 * Menampilkan statistik kunjungan pada periode tertentu berdasarkan bulan
	 */
	public function periode() {
		// Halaman yang di load
		$page = "statistik_periode";
		// Periksa apakah ada dalam folder view
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			// Jika tidak, tampilkan halaman 404
			show_404();
		}

		// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
		$data['firstyear'] = $this->statistik_model->get_first_year();
		$data['lastyear'] = $this->statistik_model->get_last_year();

		// Ambil tahun yang dipilih user
		$tahun = $this->input->post('p_tahun');

		// Jika tahun dipilih, tampilkan grafik
		if ($tahun != "") {
			/* Tampilkan halaman statistik disertai grafik */
			$data['title'] = "Statistik"; // Judul halaman
			//$data['query'] = $this->statistik_model->get_statistik_periode($tahun); // Ambil data statistik
			$data['tahun'] = $tahun; // Tahun
			$data['data_year'] = $this->statistik_model->get_visit_on_year($tahun);

			$this->load->view('templates/header', $data); // Header
			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jika tidak, tapilkan form login
			else $this->load->view('usermenu/loginform', $data);

			$this->load->view('site/'.$page, $data); // Main page 
			$this->load->view('templates/footer', $data); // Footer

		}
		// Jika tahun tidak dipilih, tampilkan pesan error
		else 
		{

			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = $this->statistik_model->get_first_year();
			$data['lastyear'] = $this->statistik_model->get_last_year();

			$data['title'] = "Statistik"; // judul halaman
			// Pesan error
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";

			$this->load->view('templates/header', $data); // Header

			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jika tidak, tapilkan form login
			else $this->load->view('usermenu/loginform', $data);

			$this->load->view('site/stats', $data); // Main page
			$this->load->view('templates/footer', $data); // Footer
		}
	}

	/**
	 * Menampilkan statistik kunjungan pada periode tertentu berdasarkan fakultas
	 */
	public function fakultas() {
		// Halaman yang di load
		$page = "statistik_fakultas";
		// Periksa apakah ada dalam folder view
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			// Jika tidak, tampilkan halaman 404
			show_404();
		}

		// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
		$data['firstyear'] = $this->statistik_model->get_first_year();
		$data['lastyear'] = $this->statistik_model->get_last_year();

		// Ambil tahun yang dipilih user
		$tahun = $this->input->post('f_tahun');
		$bulan = $this->input->post('f_bulan');

		// Periksa apakah user memilih tahun.
		// Jika user memilih tahun dan bulan, tampilkan grafik
		if ($tahun != "" && $bulan != "") {

			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = $this->statistik_model->get_first_year();
			$data['lastyear'] = $this->statistik_model->get_last_year();

			$data['title'] = "Statistik"; // Judul halaman
			$data['data_fakultas'] = $this->statistik_model->get_fakultas_visit($bulan, $tahun); // Ambil data statsitik
			$data['tahun'] = $tahun; // Tahun
			$data['bulan'] = $bulan; // Bulan

			$this->load->view('templates/header', $data); // Header

			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jika tidak, tapilkan form login
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data); // Main page
			$this->load->view('templates/footer', $data); // Footer
		}
		// Jika tidak, tampilkan pesan error
		else 
		{

			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = $this->statistik_model->get_first_year();
			$data['lastyear'] = $this->statistik_model->get_last_year();
			
			$data['title'] = "Statistik"; // Judul halaman
			// Pesan error
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";

			$this->load->view('templates/header', $data); // Header

			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jika tidak, tapilkan form login
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/stats', $data); // Main page, berisi grafik
			$this->load->view('templates/footer', $data); // Footer
		}
	}

	/**
	 * Memeriksa input user saat memilih menu statistik kepuasan
	 */
	public function statistikkepuasan()
	{
		// Halaman yang di load
		$page = "statistik_kepuasan";
		// Periksa apakah ada dalam folder view

		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			// Jika tidak, tampilkan halaman 404
			show_404();
		}

		// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
		$data['firstyear'] = $this->statistik_model->get_first_year();
		$data['lastyear'] = $this->statistik_model->get_last_year();

		// Ambil pilihan user
		$tahun = $this->input->post('k_tahun');

		// Jika user memilih tahun yang benar
		if ($tahun != "") {
			// Tampilkan grafik
			//$this->get_statistikkepuasan($tahun); DEFAULT - ORIGINAL
			//
			
			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = $this->statistik_model->get_first_year();
			$data['lastyear'] = $this->statistik_model->get_last_year();
			$data['tahun'] = $tahun;
			$data['title'] = "Statistik"; // Judul halaman

			$data['data_kepuasan'] = $this->statistik_model->get_kepuasan_on_year($tahun);

			$this->load->view('templates/header', $data); // Header

			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jikat tidak, tampilkan form login
			else $this->load->view('usermenu/loginform', $data);

			$this->load->view('site/'.$page, $data); // Main page, berisi grafik
			$this->load->view('templates/footer', $data); // Footer
		}
		// Jika tidak, tampilkan pesan error
		else 
		{
			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = $this->statistik_model->get_first_year();
			$data['lastyear'] = $this->statistik_model->get_last_year();

			$data['title'] = "Statistik"; // Judul halaman
			// Pesan error
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";

			$this->load->view('templates/header', $data); // Header

			// Periksa apakah user telah login
			if($this->authentication->is_loggedin()){
				// Jika ya, tampilan usermenu
	            $this->load->view('usermenu/usermenu', $data);
	        }
	        // Jikat tidak, tampilkan form login
			else $this->load->view('usermenu/loginform', $data);

			$this->load->view('site/stats', $data); // Main page, berisi grafik
			$this->load->view('templates/footer', $data); // Footer
		}
	}

	/**
	 * Menampilkan grafik statistik kepusasan pasien pada periode tertentu berdasarkan level kepuasan
	 */
	public function get_statistikkepuasan($tahun)
	{
		// Halaman yang di load
		$page = 'statsk';
		// Periksa apakah ada dalam folder view
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			// Jika tidak, tampilkan halaman 404
			show_404();
		}

		// Periksa apakah user memiliki hak untuk melihat grafik statistik kepuasan
        if($this->authentication->is_pelayanan() || $this->authentication->is_loket()) {

        	// Ambil data untuk masing-masing level kepuasan
        	$data['query_tidakpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun, "tidakpuas", 1);
        	$data['query_kurangpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"kurangpuas", 2);
        	$data['query_puas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"puas", 3);
        	$data['query_sangatpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"sangatpuas", 4);

			// Ambil tahun pertama dan tahun terbaru dimana kunjungan dibuat
			$data['firstyear'] = (int)$this->statistik_model->get_first_year();
			$data['lastyear'] = (int)$this->statistik_model->get_last_year();

			$data['title'] = "Statistik Kepuasan"; // Judul halaman
			$data['tahun'] = $tahun; // Tahun
			$this->load->view('templates/header', $data); // Header
			$this->load->view('usermenu/usermenu', $data); // Usermenu
			$this->load->view('site/'.$page, $data); // Main page, berisi grafik
			$this->load->view('templates/footer', $data); // Footer
        }
        // Jika tidak memiliki hak, lempar ke halaman utama menu statistik
		else {
			$this->index();
		}
	}

}

/* End of file  */
/* Location: ./application/controllers/ */
