<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class antrian extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
    parent::__construct();
    $this->load->model('antrian_model');
    $this->load->model('jadwal_model');
    if ( ! ENABLE_ANTREAN)
    {
    	redirect('site');
    }
  }
	public function index()
	{

		if(!$this->authentication->is_loket() && !$this->authentication->is_perawat() && !$this->authentication->is_dokter()){
			show_404();
		}

		
		$this->load->model('antrian_model');
		$this->load->model('klinik_model');
		$this->load->model('jadwal_model');
		$this->load->model('pegawai_model');
		$query=$this->user_model->get_all_doctor();
		$doctor_name = array();
			if ($query != NULL){
				foreach ($query as $entry){ 
					$temp = array("id"=>$entry['nomor'], "nama"=>$entry['nama']);
					$doctor_name[] = $temp;
				}
			} 
			else
			{
				$temp = array("id"=>"", "nama"=>"Tidak ada dokter");
				$doctor_name[] = $temp;
			}
		$data['hideselesai'] = $this->get_hide_selesai();
		$data['doctor_name']=$doctor_name;
		$data['doctor'] = $query;
		$data['query'] = $this->antrian_model->get_all_antrian();
		$data['status_registrasi'] = $this->klinik_model->is_tutup();
		$data['buka_umum'] = $this->klinik_model->get_status('1');
		$data['buka_gigi'] = $this->klinik_model->get_status('2');
		$data['doctor_table_umum'] = $this->jadwal_model->get_umum_harian();
		$data['doctor_table_gigi'] = $this->jadwal_model->get_gigi_harian();

		$data['msg'] = "";
		$data['title'] = "Verifikasi Status Antrean Pasien"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/verifikasi', $data);
		$this->load->view('templates/footer', $data);

	}

	function toggle()
	{
		$hideselesai = $this->get_hide_selesai();
		$data['hideselesai'] = !$hideselesai;
		$this->session->set_userdata($data);

		redirect('antrian');
	}

	private function get_hide_selesai()
	{
		$hideselesai = $this->session->userdata('hideselesai') && TRUE;
		return $hideselesai;
	}

	function tambah_antrian()
	{
		date_default_timezone_set('Asia/Jakarta');
		$datestring = "%Y-%m-%d";
  	$time = time();
  	$tanggal = mdate($datestring, $time);
		$this->load->model('antrian_model');
		$this->load->model('jadwal_model');

		$jharian = $this->jadwal_model->get_jadwal_harian_by_id($this->input->post('jadwal'), $tanggal);
		$msg = "";

		$default_status = '2';
		if($this->authentication->is_loket())
		{
			//set no urut
			if ($jharian['id_layanan'] == '1')
			$no_urutan = $this->antrian_model->get_urutan_umum();
			else $no_urutan = $this->antrian_model->get_urutan_gigi();

			//set hadir
			$default_status = '3';
		}
		else
		{
			$no_urutan = "";	
		}

				$data_user = array(
						'nomor_petugas' => $jharian['nomor'],
						'id_pasien' => $this->input->post('id_pasien'),		
						'jadwal_harian' => $this->input->post('jadwal'),
						'tanggal' => $tanggal,
						'status' => $default_status,
						'no_urutan' => $no_urutan,
				);

		$msg = $this->antrian_model->add_antrian($data_user);		

		$data['msg'] = $msg;
		//redirect('antrian', 'refresh');
		
		$this->sukses($data);
	}

	public function sukses($datamsg)
	{
		if(!$this->authentication->is_loket()) {
			show_404();
		}
		$this->load->model('antrian_model');
		$this->load->model('klinik_model');
		$this->load->model('pegawai_model');
		$this->load->model('jadwal_model');
		$query=$this->user_model->get_all_doctor();
		$doctor_name = array();
			if ($query != NULL){
				foreach ($query as $entry){ 
					$temp = array("id"=>$entry['nomor'], "nama"=>$entry['nama']);
					$doctor_name[] = $temp;
				}
			} 
			else
			{
				$temp = array("id"=>"", "nama"=>"Tidak ada dokter");
				$doctor_name[] = $temp;
			}
		$data['hideselesai'] = $this->get_hide_selesai();
		$data['doctor_name']=$doctor_name;
		$data['query'] = $this->antrian_model->get_all_antrian();
		$data['status_registrasi'] = $this->klinik_model->is_tutup();
		$data['buka_umum'] = $this->klinik_model->get_status('1');
		$data['buka_gigi'] = $this->klinik_model->get_status('2');
		$data['doctor_table_umum'] = $this->jadwal_model->get_umum_harian();
		$data['doctor_table_gigi'] = $this->jadwal_model->get_gigi_harian();

		$data['msg'] = $datamsg['msg'];
		$data['title'] = "Verifikasi Status Antrean Pasien"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/verifikasi', $data);
		$this->load->view('templates/footer', $data);

	}
	function antri($id)
	{
		if(!$this->authentication->is_pasien()) {
			show_error('Anda tidak memiliki hak akses');
		} else {
			date_default_timezone_set('Asia/Jakarta');
			$datestring = "%Y-%m-%d";
	      	$time = time();
	      	$tanggal = mdate($datestring, $time);


		$jharian = $this->jadwal_model->get_jadwal_harian_by_id($id, $tanggal);


			$this->load->model('antrian_model');
			$this->load->model('user_model');
			$data_user = array(
						'nomor_petugas' => $jharian['nomor'],
						'id_pasien' => $this->user_model->get_my_id(),		
						'jadwal_harian' => $id,
						'tanggal' => $tanggal,
						'status' => '2',

			);

			$this->antrian_model->add_antrian($data_user);
			redirect('site/antre');
		}
	}
	function batalkan_antrian($id)
	{
		if(!$this->authentication->is_pasien()) {
			show_error('Anda tidak memiliki hak akses');
		} else {
			$this->load->model('antrian_model');
			$this->load->model('user_model');
			date_default_timezone_set('Asia/Jakarta');
			$datestring = "%Y-%m-%d";
	      	$time = time();
	      	$tanggal = mdate($datestring, $time);
			$data_user = array(
						'id_pasien' => $this->user_model->get_my_id(),		
						'id' => $id,
						'tanggal' => $tanggal,
						'status' => '2',
			);
			$this->antrian_model->delete_antrian($data_user);
			redirect('site/antre');
		}
	}
	function batalkan_antrian_gigi()
	{
		$this->load->model('antrian_model');
		$this->load->model('user_model');
		$data_user = array(
					'id_pasien' => $this->user_model->get_my_id(),		
					'poli' => '2',
		);
		$this->antrian_model->delete_antrian($data_user);
		redirect('site/antre');
	}
	
	
	function buka_registrasi_umum()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->set_open('1');
		redirect('antrian', 'refresh');
	}
	function tutup_registrasi_umum()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->set_close('1');
		redirect('antrian', 'refresh');
	}
	function buka_registrasi_gigi()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->set_open('2');
		redirect('antrian', 'refresh');
	}
	function tutup_registrasi_gigi()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->set_close('2');
		redirect('antrian', 'refresh');
	}
	function ubah_dokter_periksa($idreg) {
		if(!$this->authentication->is_loket()) {
			show_error('Anda tidak memiliki hak akses');
		} else {
			date_default_timezone_set('Asia/Jakarta');
			$string = "%Y-%m-%d";
	        $time = time();
	        $tanggal = mdate($string, $time);
			$this->load->model('antrian_model');
			$data = array(
				'jadwal_harian' => $this->input->post('jadwal'),
				'tanggal' => $tanggal 
				);
			$aaa = $this->antrian_model->update_antrian($data,$idreg);
			if ($aaa == true) {
				$this->ubah_status(6,$idreg);
				redirect('antrian', 'refresh');
			} else {
				echo "gagal";
			}
		}
	}
	function ubah_status($stat,$id)
	{
		if(!$this->authentication->is_loket() && !$this->authentication->is_perawat()) {
			redirect('site');
		} else {
			$this->load->model('antrian_model');
			$this->load->model('respons');
			date_default_timezone_set('Asia/Jakarta');
	        $datestring = "%h:%i:%s %a";
	        $string = "%Y-%m-%d";
	        $time = time();
	        $waktu = mdate($datestring, $time);
	        if ($stat == 3)
	        {
	        	$no_urut = $this->antrian_model->get_urutan_umum();
            $data = array(
                'status' => $stat,
                'waktu_reg_awal' => $waktu,
                'no_urutan' => $no_urut,
                );
	        }
	        elseif($stat == 5) {
	            $data = array(
	                'status' => $stat,
	                'waktu_reg_awal' => $waktu,
	                );
	        } elseif ($stat == 6) {
	            $data = array(
	                'status' => $stat,
	                'waktu_reg_akhir' => $waktu,
	                );

	        } else {
	            $data = array('status' => $stat);
	        }
			$aaa = $this->antrian_model->update_antrian($data,$id);
			if ($aaa == true) {
				redirect('antrian', 'refresh');
			} else {
				echo "gagal";
			}
		}
	}
	function tutup_antrian_umum()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->update_status('Umum');
		redirect('antrian', 'refresh');
	}
	function tutup_antrian_gigi()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->update_status('Gigi');
		redirect('antrian', 'refresh');
	}

	function batalkan_tutup_umum()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->batal_tutup('Umum');
		redirect('antrian', 'refresh');
	}

	function batalkan_tutup_gigi()
	{
		$this->load->model('klinik_model');
		$this->klinik_model->batal_tutup('Gigi');
		redirect('antrian', 'refresh');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
