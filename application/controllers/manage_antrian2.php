<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_antrian extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->model('authentication');
		$this->load->library('pagination');
    }

/**
* Index Page for this controller.
*
* Maps to the following URL
* http://example.com/index.php/welcome
* - or -
* http://example.com/index.php/welcome/index
* - or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
*/
public function index()
{
$this->load->model('antrian_model');
$data['query'] = $this->antrian_model->get_all_antrian();

$data['title'] = "Verifikasi Status Antrean Pasien";
$this->load->view('templates/header', $data);
$this->load->model('authentication');
if($this->authentication->is_loggedin()){
$this->load->view('usermenu/usermenu', $data);
}
else $this->load->view('usermenu/loginform', $data);
$this->load->view('site/verifikasi', $data);
$this->load->view('templates/footer', $data);


}

function update_antrian()
{

}
function ubah_status($status,$id)
{
$this->load->model('antrian_model');
$aaa = $this->antrian_model->update_status($status,$id);
if ($aaa == true) {
redirect('manage_antrian', 'refresh');
} else {
echo "gagal";
}
}
function tambah_antrian()
{
$this->load->model('antrian_model');
$data_user = array(
'id_pasien' => $this->input->post('id_pasien'),
'poli' => $this->input->post('poli'),
'status' => '2'
);
$this->antrian_model->add_antrian($data_user);
}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */