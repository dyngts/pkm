<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Obat_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('obat_model');
		$this->get_model('resep_model');
		$this->get_model('model_laporan');
		$this->skema = SCHEMA;

	}

	public function index() {
		

		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));
		// $atribut = "(id, jenis,tgl,deskripsi,username_pegawai,jam)";
		// $data = "(DEFAULT, 't','1992-08-12','test','mahasiswa01','12:12:12')";
		// $id = 'id';
		// $nama_tabel = 'transaksi_obat_salemba';

		// $res = $this->obat_model->_create($atribut, $data, $nama_tabel, $id);

		// echo var_dump($res);
		// die();
	}

	public function tambah() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

		$this->load->library('form_validation');

		$aturan_tambah = array(		   
			array(
				'field'   => 'nama_obat',
				'label'   => 'Nama Obat',
				'rules'   => 'trim|required|callback__cek_nama_obat|callback__cek_input_teks'
				),
			array(
				'field'   => 'kategori',
				'label'   => 'Kategori',
				'rules'   => 'trim|required'
				),
			array(
				'field'   => 'jenis',
				'label'   => 'Jenis Obat',
				'rules'   => 'trim|required'
				),
			array(
				'field'   => 'komposisi',
				'label'   => 'Komposisi',
				'rules'   => 'trim|xss_clean|callback__cek_input_teks2'
				),   
			array(
				'field'   => 'sediaan',
				'label'   => 'Sediaan',
				'rules'   => 'trim|required'
				),
			array(
				'field'   => 'harga_satuan',
				'label'   => 'Harga Satuan',
				'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
				),
			array(
				'field'   => 'harga_per_kemasan',
				'label'   => 'Harga per Kemasan',
				'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
				),
			array(
				'field'   => 'kemasan',
				'label'   => 'Kemasan',
				'rules'   => 'trim|required|callback__cek_input_teks2'
				),
			array(
				'field'   => 'satuan_per_kemasan',
				'label'   => 'Satuan per Kemasan',
				'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
				),
			array(
				'field'   => 'supplier',
				'label'   => 'Supplier',
				'rules'   => 'trim'
				),
			);



if($this->input->post('batal') == "Batal") {
	redirect(site_url('obat_controller/kumpulan_obat_detil'));
}

$this->form_validation->set_rules($aturan_tambah);

$this->form_validation->set_message('required', '%s Harus Diisi'); 
$this->form_validation->set_message('greater_than', 'Jumlah Obat Masuk Harus Lebih dari 0');
$this->form_validation->set_message('is_natural', 'Jumlah Obat Masuk Harus Bilangan Bulat');



if($this->form_validation->run() == TRUE) {
	if($_POST) {
				// $jenis_temp = ;
				// $temp = explode('-', $jenis_temp);
				// $jenis_asli = '';
				// for($i = 0; $i < count($temp); ++$i) {
				// 	if($i > 0) {
				// 		$jenis_asli = $jenis_asli.' '.$temp[$i];
				// 	}
				// 	else {
				// 		$jenis_asli = $temp[$i];
				// 	}
				// }

		$data['nama'] = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
		$data['kategori'] = str_replace("-", " ", $this->security->xss_clean($this->input->post('kategori', TRUE)));
		$data['jenis'] = str_replace("-", " ", $this->security->xss_clean($this->input->post('jenis', TRUE)));
		$data['status_pemakaian'] = '1';
		$data['sediaan'] = $this->security->xss_clean($this->input->post('sediaan', TRUE));
		$data['kemasan'] = $this->security->xss_clean($this->input->post('kemasan', TRUE));
		$data['satuan_per_kemasan'] = (int) $this->input->post('satuan_per_kemasan', TRUE);
		$data['total_satuan'] = 0;
		$data['komposisi'] = $this->security->xss_clean($this->input->post('komposisi', TRUE));
		$data['harga_satuan'] = (int) $this->input->post('harga_satuan', TRUE);
		$data['harga_per_kemasan'] = (int) $this->input->post('harga_per_kemasan', TRUE);
		$data['id_supplier'] = ($this->security->xss_clean($this->input->post('supplier', TRUE)) != NULL) ? $this->security->xss_clean($this->input->post('supplier', TRUE)) : NULL;

		$this->obat_model->_tambah($data, $this->t_obat);

		$where = array('nama'=>'test');
		$obat = $this->obat_model->_ambil_baris_tertentu($where, $this->t_obat, 'row', 'id');

		date_default_timezone_set('Asia/Jakarta');
		$data3 = array();
		$data3['tgl'] = date('d-M-Y');
		$data3['deskripsi'] = 'Supplier';
		$data3['jam'] = date('H:i:s');
		$data3['username_penerima'] = $this->session->userdata('username');
		$data3['flag_faktur'] = '0';
		$data3['no_faktur'] = null;
		$data3['scanned_faktur'] = null;

		$this->obat_model->_tambah($data3, $this->t_log_obat_masuk); 
		$this->session->sess_expiration = '5';
		$this->session->set_userdata('notif_mess', 'Data obat telah berhasil ditambahkan!');
		redirect(site_url('obat_controller/kumpulan_obat_detil'));
	}
}
$data = array();
$data['data_supplier'] = $this->obat_model->_ambil_daftar_supplier();

$data['data_kategori'] = array(
	''=>'Pilih Kategori Obat',
	'Obat-Umum'=>'Obat Umum',
	'Obat-Alkes-dan-BMHP-Gigi'=>'Obat, Alkes, dan BMHP Gigi',
	'Alkes-dan-BMHP-Umum'=>'Alkes dan BMHP Umum',
	'Obat-Emergency'=>'Obat Emergency');

$data['data_sediaan_obat'] = $this->obat_model->_ambil_daftar_sediaan();
$data['title'] = 'Tambah Obat';


$data['nama_obat'] = $this->input->post('nama_obat', TRUE);
$data['sediaan'] = $this->input->post('sediaan', TRUE);
$data['kategori'] = $this->input->post('kategori', TRUE);
$data['jenis'] = $this->input->post('jenis', TRUE);

if($_POST) {
	$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat_obj(str_replace('-', ' ', $data['kategori']));
}
else {
	$data['data_jenis_obat'] = array();
}
$data['kemasan'] = $this->input->post('kemasan', TRUE);
$data['satuan_per_kemasan'] = $this->input->post('satuan_per_kemasan', TRUE);
$data['komposisi'] = $this->input->post('komposisi', TRUE);
$data['harga_satuan'] = $this->input->post('harga_satuan', TRUE);
$data['harga_per_kemasan'] = $this->input->post('harga_per_kemasan', TRUE);
$data['supplier'] = ($this->input->post('supplier', TRUE) == NULL) ? NULL : $this->input->post('supplier', TRUE);


$this->cetak_halaman('obat/_tambah_obat', $data);
}

public function _cek_angka($str) {
	$batas = 2147483647;
	$in = (int) $str;

	if($in < $batas) {
		return TRUE;
	}
	else {
		$this->form_validation->set_message('_cek_angka', 'Nilai masukan melebihi batas maksimal');
		return FALSE;
	}

}

public function _cek_input_angka($str) {

	if($str != null) {
		if(!preg_match('/^[0-9]+$/', $str)) {
			$this->form_validation->set_message('_cek_input_angka', 'Nilai masukan harus berupa bilangan bulat positif');
			return FALSE;
		}
	}
	return TRUE;
}

public function _cek_input_teks($str) {

	if($str != null) {
		if(!preg_match('/^[0-9a-zA-Z,.-\s]+$/', $str)) {
			$this->form_validation->set_message('_cek_input_teks', 'Format masukkan mengandung karakter yang tidak diperbolehkan');
			return FALSE;
		}
	}
	return TRUE;
}

public function _cek_input_teks2($str) {

	if($str != null) {
		if(!preg_match('/^[a-zA-Z,.-\s]+$/', $str)) {
			$this->form_validation->set_message('_cek_input_teks2', 'Format masukkan mengandung karakter yang tidak diperbolehkan');
			return FALSE;
		}
	}
	return TRUE;
}

public function _cek_nama_obat($str) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$nama_obat = $this->input->post('nama_obat', TRUE);
	$where = array('nama'=>$nama_obat);

	$ada_obat = $this->obat_model->_cek_ada_atribut($where, $this->t_obat);

	if($ada_obat) {
		$this->form_validation->set_message('_cek_nama_obat', '%s sudah terpakai, silakan isi nama obat yang belum terpakai ');
		return FALSE;
	}
	return TRUE;
}

public function lihat_detil($id = '', $mulai = 0) {

	$data = array();
	$obats = $this->obat_model->_ambil_detil_obat($id, $mulai);
	$data['obats'] = $obats['obats'];
	$data['datas'] = $this->obat_model->_ambil_data_obat_umum($id, $mulai);
	$this->load->library('pagination');
	$config['base_url'] = base_url().'/obat_controller/lihat_detil/'.$id;
	$config['total_rows'] = $obats['jumlah_baris'];
		// $config['uri_segment'] = 4;
		// $config['num_links'] = 4;
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Detil Data Obat - '.$id;
	$this->cetak_halaman('obat/_detil_obat_tertentu', $data);
}


public function tambah_masuk() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));
		// $this->load->library('form_validation');

		// $aturan_tambah = array(
		// 	array(
		// 		'field'   => 'sumber_obat',
		// 		'label'   => 'Sumber Obat',
		// 		'rules'   => 'trim|required'
		// 		),
		// 	array(
		// 		'field'   => 'nama_obat[]',
		// 		'label'   => 'Nama Obat',
		// 		'rules'   => 'trim|required'
		// 		),
		// 	array(
		// 		'field'   => 'kategori',
		// 		'label'   => 'Kategori',
		// 		'rules'   => 'trim|required'
		// 		),
		// 	array(
		// 		'field'   => 'jenis',
		// 		'label'   => 'Jenis Obat',
		// 		'rules'   => 'trim|required'
		// 		),
		// 	array(
		// 		'field'   => 'tanggal[]',
		// 		'label'   => 'Tanggal Kadaluarsa',
		// 		'rules'   => 'trim|required'
		// 		),
		// 	array(
		// 		'field'   => 'jml_obat_masuk[]',
		// 		'label'   => 'Jumlah Obat Masuk',
		// 		'rules'   => 'trim|required|callback__cek_input_angka|callback__cek_angka'
		// 		),
		// 	);

	if($this->input->post('batal') == "Batal") {
		redirect(site_url('obat_controller/kumpulan_obat_detil'));
	}

		// $this->form_validation->set_rules($aturan_tambah);

		// $this->form_validation->set_message('required', '%s Harus Diisi'); 
		// $this->form_validation->set_message('greater_than', 'Jumlah Obat Masuk Harus Lebih dari 0');
		// $this->form_validation->set_message('is_natural', 'Jumlah Obat Masuk Harus Bilangan Bulat');

		//if($this->form_validation->run() == TRUE) {
	if($_POST) {

				// $nama_obat = 

				// $temp = explode('-', $nama_obat);
				// $nama_asli = '';
				// for($i = 0; $i < count($temp); ++$i) {
				// 	if($i > 0) {
				// 		$jenis_asli = $jenis_asli.' '.$temp[$i];
				// 	}
				// 	else {
				// 		$jenis_asli = $temp[$i];
				// 	}
				// }

		$data = array();

		$nama_obat = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
		$tgl_kadaluarsa = $this->security->xss_clean($this->input->post('tanggal', TRUE));
		$jml_obat_masuk = $this->security->xss_clean($this->input->post('jml_obat_masuk', TRUE));
		$deskripsi = $this->security->xss_clean($this->input->post('sumber_obat', TRUE));
		$no_faktur =  $this->security->xss_clean($this->input->post("no_faktur", TRUE));

		$total_input = count($nama_obat);

		date_default_timezone_set('Asia/Jakarta');
		$data2 = array();
		$data2['tgl'] = date('d-M-Y');
		$data2['jam'] = date('H:i:s');
		$data2['deskripsi'] = $deskripsi[0];

		$jam = str_replace(':', '', $data2['jam']);


		if($_FILES['file']['name'][0] != '') {
			$data2['flag_faktur'] = '1';
			$data2['no_faktur'] = $no_faktur[0];
			$ext_gambar = str_replace('image/', '', $_FILES['file']['type'][0]);

			if($ext_gambar == 'jpeg') {
				$ext_gambar = 'jpg';
			}

			$data2['scanned_faktur'] = $data2['tgl'].'_'.$jam.'.'.$ext_gambar;


			$config['upload_path'] = './faktur/';
			$config['allowed_types'] = 'gif|jpg|png';

			$ext_gambar = str_replace('image/', '', $_FILES['file']['type'][0]);
			$config['file_name'] = $data2['tgl'].'_'.$jam; 

			$this->load->library('upload', $config);

			foreach($_FILES['file'] as $key=>$val)
			{
				$i = 1;
				foreach($val as $v)
				{
					$field_name = "file_".$i;
					$_FILES[$field_name][$key] = $v;
					$i++;
				}
			}

					// hapus array awal, karena kita sudah memiliki array baru
			unset($_FILES['file']);

      				// variabel error diubah, dari string menjadi array
			$error = array();
			$success = array();
			foreach($_FILES as $field_name => $file)
			{
				$this->upload->do_upload($field_name);
			}




					// $files = $_FILES;
					// $n = count($_FILES['file']['name']);

					// for($i=0 ; $i <$n ; $i++)
					// {

					// 	$_FILES['file']['name']= $files['file']['name'][$i];
					// 	$_FILES['file']['type']= $files['file']['type'][$i];
					// 	$_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
					// 	$_FILES['file']['error']= $files['file']['error'][$i];
					// 	$_FILES['file']['size']= $files['file']['size'][$i];

					// 	$config['upload_path'] = './faktur/';
					// 	$config['allowed_types'] = 'gif|jpg|png';
					//     $config['file_name'] = $data2['scanned_faktur'];   

					// 	$this->upload->initialize($config);

					// 	var_dump(is_dir($config['upload_path']));
					// 	var_dump(is_writable($config['upload_path']));

					// 	var_dump($this->upload->do_upload('file'));
					// 	var_dump($this->upload->display_errors());
					// 	die();
					// }

					// $gambar = $this->upload->data();
					// chmod("./faktur/".$gambar['file_name'], 0755);
		}
		else {
			$data2['flag_faktur'] = '0';
			$data2['no_faktur'] = NULL;
			$data2['scanned_faktur'] = NULL;
		}

		$data2['username_penerima'] = $this->session->userdata('username');
		$this->obat_model->_tambah($data2, $this->t_log_obat_masuk);

		for($i = 0; $i < $total_input; ++$i) {
			$where = array(
				'nama_obat'=>$nama_obat[$i], 'tgl_kadaluarsa'=>$tgl_kadaluarsa[$i]
				);

			$total_stok_lama = $this->obat_model->_ambil_baris_tertentu(array('nama'=>$nama_obat[$i]), $this->t_obat, 'row', 'total_satuan');
			$total_stok_baru['total_satuan'] = $total_stok_lama->total_satuan + $jml_obat_masuk[$i];
			$this->obat_model->_ubah(array('nama'=>$nama_obat[$i]), $total_stok_baru , $this->t_obat);

			if($this->obat_model->_cek_ada_atribut($where, $this->t_stok_obat)) {
				$jml_satuan_lama = $this->obat_model->_ambil_baris_tertentu($where, $this->t_stok_obat, 'row', 'jml_satuan');

				$stok_baru = array();
				$stok_baru['jml_satuan'] = $jml_satuan_lama->jml_satuan + $jml_obat_masuk[$i];

				$this->obat_model->_ubah($where, $stok_baru , $this->t_stok_obat);

				$log_obat_masuk = $this->obat_model->_ambil_log_masuk_terbaru();

				$stok = $this->obat_model->_ambil_total_terbaru($nama_obat[$i]);

				$data3 = array();
				$data3['id_log_obat_masuk'] = $log_obat_masuk->id;
				$data3['nama_obat'] = $nama_obat[$i];
				$data3['jumlah'] = $jml_obat_masuk[$i];
				$data3['total_stok'] = (int) $total_stok_baru['total_satuan'];

				$this->obat_model->_tambah($data3, $this->t_penerimaan_obat);
			}
			else {
				$obat_tgl_baru = array();
				$obat_tgl_baru['nama_obat'] = $nama_obat[$i];
				$obat_tgl_baru['tgl_kadaluarsa'] = $tgl_kadaluarsa[$i];
				$obat_tgl_baru['jml_satuan'] = $jml_obat_masuk[$i];

				$this->obat_model->_tambah($obat_tgl_baru, $this->t_stok_obat);

				$log_obat_masuk = $this->obat_model->_ambil_log_masuk_terbaru();

				$stok = $this->obat_model->_ambil_total_terbaru($nama_obat[$i]);

				$data3 = array();
				$data3['id_log_obat_masuk'] = $log_obat_masuk->id;
				$data3['nama_obat'] = $nama_obat[$i];
				$data3['jumlah'] = $jml_obat_masuk[$i];
				$data3['total_stok'] = (int) $total_stok_baru['total_satuan'];

				$this->obat_model->_tambah($data3, $this->t_penerimaan_obat);
			}
		}
		$this->session->sess_expiration = '5';
		$this->session->set_userdata('notif_mess', 'Entri obat masuk telah berhasil ditambahkan!');				
		redirect(site_url('obat_controller/kumpulan_obat_detil'));
	}
		//}

	$data['data_kategori'] = array(''=>'Pilih Kategori Obat', 'Obat-Umum'=>'Obat Umum','Obat-Alkes-dan-BMHP-Gigi'=>'Obat, Alkes, dan BMHP Gigi','Alkes-dan-BMHP-Umum'=>'Alkes dan BMHP Umum','Obat-Emergency'=>'Obat Emergency');
	$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat();
		//$data['data_nama_obat'] = $this->obat_model->_ambil_daftar_nama_obat();
	$data['nama_obat'] = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
	$data['tgl_kadaluarsa'] = $this->security->xss_clean($this->input->post('tanggal', TRUE));
	$data['kategori'] = $this->input->post('kategori', TRUE);
	$data['jenis'] = $this->input->post('jenis', TRUE);
	if($_POST) {
		$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat($data['kategori']);
		$data['data_nama_obat'] = $this->obat_model->_ambil_daftar_nama_obat_obj($data['nama_obat']);
		$data['daftar_jenis'] = $this->data_dropdown_jenis($data['kategori']);
	}

	$data['jml_satuan'] =  $this->security->xss_clean($this->input->post('jml_obat_masuk', TRUE));
	$data['title'] = 'Entri Obat Masuk';
	$this->cetak_halaman('obat/_entri_obat_masuk', $data);
}

public function tambah_faktur($id = '') {
	$no_faktur = $this->security->xss_clean($this->input->post('no_faktur', TRUE));

	$log_obat_masuk = $this->obat_model->_ambil_baris_tertentu(array('id'=>$id), $this->t_log_obat_masuk, 'row');

	$ext_gambar = str_replace('image/', '', $_FILES['file']['type']);

	if($ext_gambar == 'jpeg') {
		$ext_gambar = 'jpg';
	}

	$data = array();
	$data['flag_faktur'] = '1';
	$data['no_faktur'] = $no_faktur;
	$jam = str_replace(':', '', $log_obat_masuk->jam);
	$data['scanned_faktur'] = $log_obat_masuk->tgl.'_'.$jam.'.'.$ext_gambar;

	$this->obat_model->_ubah(array('id'=>$id), $data, $this->t_log_obat_masuk);
	$config['allowed_types'] = 'gif|jpg|png';
	$config['upload_path'] = './faktur/';
	$config['file_name'] = $log_obat_masuk->tgl.'_'.$jam;
	$this->load->library('upload', $config);
	$this->upload->do_upload('file');

	redirect('obat_controller/detil_log_masuk/'.$id);
}

public function update_faktur($id) {
	$no_faktur = $this->security->xss_clean($this->input->post('no_faktur', TRUE));

	$log_obat_masuk = $this->obat_model->_ambil_baris_tertentu(array('id'=>$id), $this->t_log_obat_masuk, 'row');

	$ext_gambar = str_replace('image/', '', $_FILES['file']['type']);

	if($ext_gambar == 'jpeg') {
		$ext_gambar = 'jpg';
	}

		// $path = PUBPATH.'/faktur/'.$log_obat_masuk->scanned_faktur;
		// unlink($path);

	$data = array();
	$data['flag_faktur'] = '1';
	$data['no_faktur'] = $no_faktur;
	$jam = str_replace(':', '', $log_obat_masuk->jam);
	$data['scanned_faktur'] = $log_obat_masuk->tgl.'_'.$jam.'.'.$ext_gambar;

	$this->obat_model->_ubah(array('id'=>$id), $data, $this->t_log_obat_masuk);
	$config['allowed_types'] = 'gif|jpg|png';
	$config['upload_path'] = './faktur/';
	$config['file_name'] = $log_obat_masuk->tgl.'_'.$jam;
	$config['overwrite'] = TRUE;
	$this->load->library('upload', $config);
	$this->upload->do_upload('file');

	redirect('obat_controller/detil_log_masuk/'.$id);
}

public function kumpulan_obat_detil($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));
	$data = array();
	$search_keywords_array = array();

	$hasil = $this->obat_model->_join_id_kadaluarsa_obat($mulai, $search_keywords_array);

	$data['obats'] = $hasil['obats'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/kumpulan_obat_detil';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Daftar Obat Aktif';		
	$this->cetak_halaman('obat/_daftar_obat_detil', $data);
}

public function cari_sesuatu($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$nilai_cari = $this->security->xss_clean($this->input->post('search'));
			//$nilai_cari = preg_replace('/[^a-zA-Z0-9,.-_ ]/', ' ', $nilai);

	$search_keywords_array = array();

	if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();
		if(!empty($nilai_cari)) { 
			$search_keywords_array['nilai_obat_cari'] =  $nilai_cari;
			$this->obat_model->searchterm_handler('nilai_obat_cari', $nilai_cari); 
		} else {
			$this->session->unset_userdata('nilai_obat_cari');	
		}

		if($this->input->post("hapus", TRUE)) {
			$this->session->unset_userdata('nilai_obat_cari');
			$search_keywords_array = array();
			redirect('obat_controller/kumpulan_obat_detil');
		}

	}
	else {
		if($this->session->userdata('nilai_obat_cari')) {
			$search_keywords_array['nilai_obat_cari'] = $this->session->userdata('nilai_obat_cari');
		}
	}

	$hasil = $this->obat_model->_join_id_kadaluarsa_obat($mulai, $search_keywords_array);

	$data['obats'] = $hasil['obats'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/cari_sesuatu';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$this->cetak_halaman('obat/_daftar_obat_cari', $data);
}

public function kumpulan_obat_notifikasi($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$data = array();

	$hasil = $this->obat_model->_join_id_kadaluarsa_obat_notif($mulai);

	$data['obats'] = $hasil['obats'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/kumpulan_obat_notifikasi';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'obat';		
	$this->cetak_halaman('obat/_daftar_obat_detil', $data);

}

public function kumpulan_obat_menjelang_kadaluarsa($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$data = array();

	$hasil = $this->obat_model->_ambil_daftar_menjelang_kadaluarsa($mulai); 
	$data['obats'] = $hasil['kumpulan'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/kumpulan_obat_menjelang_kadaluarsa/';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Daftar Obat Menjelang Kadaluarsa';
	$this->cetak_halaman('obat/_daftar_obat_menjelang_kadaluarsa', $data);
}

public function kumpulan_obat_sudah_kadaluarsa($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$data = array();

	$hasil = $this->obat_model->_ambil_daftar_sudah_kadaluarsa($mulai); 

	$data['obats'] = $hasil['kumpulan'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'/obat_controller/kumpulan_obat_sudah_kadaluarsa/';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['jumlah_baris'] = $hasil['jumlah_baris'];
	$data['title'] = 'obat';
	$this->cetak_halaman('obat/_daftar_obat_kadaluarsa', $data);
}


public function kumpulan_log_obat_keluar($mulai =0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller')); 
	$search_keywords_array = array();

	$data_log = $this->obat_model->_ambil_daftar_log_keluar($mulai, $search_keywords_array);
	$data = array();
	$data['keluar'] = $data_log['daftar_log']; 
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/kumpulan_log_obat_keluar/';
	$config['total_rows'] = $data_log['jumlah_log'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Daftar Log Obat Keluar';

	$this->cetak_halaman('obat/_daftar_log_obat_keluar', $data);
}

public function search_log_keluar($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
	$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
	$search_keywords_array = array();

	if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

		if(!empty($tanggal_awal)) { 
			$search_keywords_array['tanggal_awal_keluar'] =  $tanggal_awal;
			$this->obat_model->searchterm_handler('tanggal_awal_keluar',$tanggal_awal); 
		} else {
			$this->session->unset_userdata('tanggal_awal_keluar');	
		}

		if(!empty($tanggal_akhir)) { 
			$search_keywords_array['tanggal_akhir_keluar'] =  $tanggal_akhir;
			$this->obat_model->searchterm_handler('tanggal_akhir_keluar',$tanggal_akhir); 
		} else {
			$this->session->unset_userdata('tanggal_akhir_keluar');	
		}

		if($this->input->post("hapus", TRUE)) {
			$this->session->unset_userdata('tanggal_awal_keluar');
			$this->session->unset_userdata('tanggal_akhir_keluar');
			$search_keywords_array = array();
			redirect('obat_controller/kumpulan_log_obat_keluar');
		}

	}
	else {
		if($this->session->userdata('tanggal_awal_keluar')) {
			$search_keywords_array['tanggal_awal_keluar'] = $this->session->userdata('tanggal_awal_keluar');
		}

		if($this->session->userdata('tanggal_akhir_keluar')) {
			$search_keywords_array['tanggal_akhir_keluar'] = $this->session->userdata('tanggal_akhir_keluar');
		}
	}

	$data_log = $this->obat_model->_ambil_daftar_log_keluar($mulai, $search_keywords_array);
	$data = array();
	$data['keluar'] = $data_log['daftar_log']; 
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/search_log_keluar/';
	$config['total_rows'] = $data_log['jumlah_log'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Daftar Log Obat Keluar';

	$this->cetak_halaman('obat/_daftar_log_obat_keluar_query', $data);
}

public function detil_log_keluar($id) {
	if($this->cek_int_only($id)) {
		$hasil = $this->obat_model->_ambil_detil_log($id, 'keluar');

		if($hasil['data_obat']->num_rows() > 0) {
			$data['datas'] = $hasil['data_log'];
			$data['obats'] = $hasil['data_obat'];
			$data['title'] = 'Detil Log Keluar - '.$id;
			$this->cetak_halaman('obat/_detil_log_keluar', $data);
		}
		else {
			show_404();
		}
	}
	else {
		show_404();
	}	
}

public function generate_surat_jalan($id) {

	$hasil = $this->obat_model->_detil_log_surat_jalan($id);
	$data = array();
	$data_log = $hasil['data_log'];

	$array_nama_obat = array();
	$array_jumlah_keluar = array();
	$array_sediaan = array();

	$i = 0;

	$total_input = $hasil['data_obat']->num_rows();

	foreach ($hasil['data_obat']->result() as $obat) {
		$array_nama_obat[$i] = $obat->nama;
		$array_sediaan[$i] = $obat->sediaan;
		$array_jumlah_keluar[$i] = $obat->jumlah;
	}

	$data['nama_surat_jalan'] = $data_log->jam.'_'.$data_log->tgl.'_'.$data_log->penerima;

	$datas = array();

			//Format Tanggal

	$datas['tgl_pembuatan'] = $data_log->tgl;
	$datas['penerima'] = $data_log->penerima;
			$datas['pembuat_surat_jalan'] = $data_log->username_pemberi; //$this->session->userdata('username');
			$datas['nama_obats'] = $array_nama_obat;
			$datas['sediaans'] = $array_sediaan;
			$datas['jumlah_keluars'] = $array_jumlah_keluar;
			$datas['jumlah_input'] = $total_input;
			$datas['no_surat_jalan'] = $data_log->no_surat_jalan;

				/*
				* Sesi generate dokumen dalam bentuk .pdf menggunakan
				* library html2pdf
				*/

				//Load the library
				$this->load->library('html2pdf');

	    		//Set folder to save PDF to
				$this->html2pdf->folder('./surat_jalan/');

	    		//Set the filename to save/download as
				$this->html2pdf->filename($data['nama_surat_jalan'].'.pdf');

	    		//Set the paper defaults
				$this->html2pdf->paper('a3', 'landscape');

				$this->html2pdf->html($this->load->view('surat_jalan/_surat_jalan', $datas, true));

				if($this->html2pdf->create('download')) {
	    			//PDF was successfully saved or downloaded
	    			//echo 'PDF saved';
	    			//chmod("./assets/pdfs/".$data['nama_surat_jalan'].'.pdf', 0755);
					//redirect(site_url('/surat_jalan_controller/kumpulan'));
				}



			}

			public function kumpulan_log_obat_masuk($mulai =0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

				$search_keywords_array = array();

				$data_log = $this->obat_model->_ambil_daftar_log_masuk($mulai, $search_keywords_array);
				$data = array();
				$data['masuk'] = $data_log['daftar_log']; 
				$this->load->library('pagination');
				$config['base_url'] = base_url().'index.php/obat_controller/kumpulan_log_obat_masuk/';
				$config['total_rows'] = $data_log['jumlah_log'];
				$this->pagination->initialize($config);
				$data['pagination_links'] = $this->pagination->create_links();
				$data['mulai'] = $mulai;
				$data['title'] = 'Daftar Log Obat Masuk';

				$this->cetak_halaman('obat/_daftar_log_obat_masuk', $data);
			}

			public function search_log_masuk($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

				$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
				$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
				$search_keywords_array = array();

				if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

					if(!empty($tanggal_awal)) { 
						$search_keywords_array['tanggal_awal_masuk'] =  $tanggal_awal;
						$this->obat_model->searchterm_handler('tanggal_awal_masuk',$tanggal_awal); 
					} else {
						$this->session->unset_userdata('tanggal_awal_masuk');	
					}

					if(!empty($tanggal_akhir)) { 
						$search_keywords_array['tanggal_akhir_masuk'] =  $tanggal_akhir;
						$this->obat_model->searchterm_handler('tanggal_akhir_masuk',$tanggal_akhir); 
					} else {
						$this->session->unset_userdata('tanggal_akhir_masuk');	
					}

					if($this->input->post("hapus", TRUE)) {
						$this->session->unset_userdata('tanggal_awal_masuk');
						$this->session->unset_userdata('tanggal_akhir_masuk');
						$search_keywords_array = array();
						redirect('obat_controller/kumpulan_log_obat_masuk');
					}

				}
				else {
					if($this->session->userdata('tanggal_awal_masuk')) {
						$search_keywords_array['tanggal_awal_masuk'] = $this->session->userdata('tanggal_awal_masuk');
					}

					if($this->session->userdata('tanggal_akhir_masuk')) {
						$search_keywords_array['tanggal_akhir_masuk'] = $this->session->userdata('tanggal_akhir_masuk');
					}
				}

				$data_log = $this->obat_model->_ambil_daftar_log_masuk($mulai, $search_keywords_array);
				$data = array();
				$data['masuk'] = $data_log['daftar_log']; 
				$this->load->library('pagination');
				$config['base_url'] = base_url().'index.php/obat_controller/search_log_masuk/';
				$config['total_rows'] = $data_log['jumlah_log'];
				$this->pagination->initialize($config);
				$data['pagination_links'] = $this->pagination->create_links();
				$data['mulai'] = $mulai;
				$data['title'] = 'Daftar Log Obat Masuk';

				$this->cetak_halaman('obat/_daftar_log_obat_masuk_query', $data);
			}

			public function detil_log_masuk($id) {
				if($this->cek_int_only($id)) {
					$hasil = $this->obat_model->_ambil_detil_log($id, 'masuk');

					if($hasil['data_obat']->num_rows() > 0) {
						$data['datas'] = $hasil['data_log'];
						$data['obats'] = $hasil['data_obat'];
						$data['title'] = 'Detil Log Obat Masuk - '.$id;
						$this->cetak_halaman('obat/_detil_log_masuk', $data);
					}
					else {
						show_404();
					}
				}
				else {
					show_404();
				}	
			}

			public function join_id_kadaluarsa() {
				$data['obat'] = $this->obat_model->__join_id_kadaluarsa();
				$data['title'] = 'obat'; 

				$this->cetak_halaman('obat/_daftar_obat', $data);
			}

			public function ubah_detil($id_obat) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

				$this->load->library('form_validation');

				$aturan_ubah = array(		   
					array(
						'field'   => 'nama_obat',
						'label'   => 'Nama Obat',
						'rules'   => 'trim|required|callback__cek_input_teks|callback__cek_nama_obat_ubah'
						),
					array(
						'field'   => 'kategori',
						'label'   => 'Kategori',
						'rules'   => 'trim|required'
						),
					array(
						'field'   => 'jenis',
						'label'   => 'Jenis Obat',
						'rules'   => 'trim|required'
						),
					array(
						'field'   => 'komposisi',
						'label'   => 'Komposisi',
						'rules'   => 'callback__cek_input_teks|trim|xss_clean'
						),   
					array(
						'field'   => 'sediaan',
						'label'   => 'Sediaan',
						'rules'   => 'trim|required'
						),
					array(
						'field'   => 'harga_satuan',
						'label'   => 'Harga Satuan',
						'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
						),
					array(
						'field'   => 'harga_per_kemasan',
						'label'   => 'Harga per Kemasan',
						'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
						),
					array(
						'field'   => 'kemasan',
						'label'   => 'Kemasan',
						'rules'   => 'trim|required|callback__cek_input_teks'
						),
					array(
						'field'   => 'satuan_per_kemasan',
						'label'   => 'Satuan per Kemasan',
						'rules'   => 'trim|callback__cek_input_angka|callback__cek_angka'
						),

					array(
						'field'   => 'status_pemakaian',
						'label'   => 'Status Pemakaian',
						'rules'   => 'trim|required'
						),
					array(
						'field'   => 'supplier',
						'label'   => 'Supplier',
						'rules'   => 'trim'
						),
					);

if($this->input->post('batal') == "Batal") {
	redirect(site_url('obat_controller/kumpulan_obat_detil'));
}

$this->form_validation->set_rules($aturan_ubah);

$this->form_validation->set_message('required', '%s Harus Diisi'); 
$this->form_validation->set_message('greater_than', 'Jumlah Obat Masuk Harus Lebih dari 0');
$this->form_validation->set_message('is_natural', 'Jumlah Obat Masuk Harus Bilangan Bulat');



if($this->form_validation->run() == TRUE) {
	if($_POST) {
		$data = array();
		$data['nama'] = $this->security->xss_clean($this->input->post('nama_obat', TRUE));
		$data['kategori'] = str_replace("-", " ", $this->security->xss_clean($this->input->post('kategori', TRUE)));
		$data['jenis'] = str_replace("-", " ", $this->security->xss_clean($this->input->post('jenis', TRUE)));
		$data['status_pemakaian'] = ($this->input->post('status_pemakaian', TRUE) == 'aktif') ? '1' : '0';
		$data['sediaan'] = $this->security->xss_clean($this->input->post('sediaan', TRUE));
		$data['kemasan'] = $this->security->xss_clean($this->input->post('kemasan', TRUE));
		$data['satuan_per_kemasan'] = (int) $this->input->post('satuan_per_kemasan', TRUE);
		$data['komposisi'] = $this->security->xss_clean($this->input->post('komposisi', TRUE));
		$data['harga_satuan'] = (int) $this->input->post('harga_satuan', TRUE);
		$data['harga_per_kemasan'] = (int) $this->input->post('harga_per_kemasan', TRUE);
		$data['id_supplier'] = ($this->input->post('supplier', TRUE) == NULL) ? NULL : $this->input->post('supplier', TRUE);

		$where = array('id'=>$id_obat);

		$this->obat_model->_ubah($where, $data, $this->t_obat);

		date_default_timezone_set('Asia/Jakarta');
		$data2 = array();
		$data2['waktu'] = date('d-M-Y').' '.date('H:i:s');
		$data2['deskripsi'] = 'Update Data Obat';
		$data2['nama_obat'] = $data['nama'];
		$data2['username_updater'] =  $this->session->userdata('username');

		$this->obat_model->_tambah($data2, $this->t_log_update_obat);
		$this->session->sess_expiration = '5';
		$this->session->set_userdata('notif_mess', 'Data obat telah berhasil diubah!');
		redirect(site_url('obat_controller/kumpulan_obat_detil'));
	}		
}

if($this->cek_int_only($id_obat)) {
	$data['obat'] = $this->obat_model->_ambil_baris_tertentu_kadaluarsa_obat($id_obat);
	$obat = $data['obat'];

	if($obat != null) {
		
		$data['data_kategori'] = array('Obat-Umum'=>'Obat Umum','Obat-Alkes-dan-BMHP-Gigi'=>'Obat, Alkes, dan BMHP Gigi','Alkes-dan-BMHP-Umum'=>'Alkes dan BMHP Umum','Obat-Emergency'=>'Obat Emergency');
		$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat($obat->kategori);
		$data['data_supplier'] = $this->obat_model->_ambil_daftar_supplier();
		$data['data_sediaan_obat'] = $this->obat_model->_ambil_daftar_sediaan();
		$data['title'] = 'Ubah Detil Obat - '.$id_obat;
		$this->cetak_halaman('obat/_ubah_obat_detil', $data);
	}
	else {
		show_404();
	}
	
}
else {
	show_404();
}


}

public function _cek_nama_obat_ubah($str) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$nama_baru = $this->input->post('nama_obat', TRUE);
	$id = $this->uri->segment(3);



	$obat = $this->obat_model->_ambil_baris_tertentu(array('nama'=>$nama_baru), $this->t_obat, 'row', 'id, nama');

	if(count($obat) > 0 && ($obat->id != $id)) {
		$this->form_validation->set_message('_cek_nama_obat_ubah', '%s sudah digunakan di obat lain ');
		return FALSE;
	}

	return TRUE; 
}

public function data_dropdown_jenis($kategori) {
	$this->obat_model->_daftar_pilihan_jenis($kategori);
}

public function data_dropdown_nama($jenis) {
	$this->obat_model->_daftar_pilihan_nama($jenis);
}


public function tambah_log_salemba() {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));
	

	if($this->input->post('batal') == "Batal") {
		redirect(site_url('obat_controller/kumpulan_log_salemba'));
	}

	if($_POST) {
		$nama_obats = $this->security->xss_clean($this->input->post('nama_obat',TRUE));
		$jumlahs = $this->security->xss_clean($this->input->post('jml_obat_masuk',TRUE));
		$jenis = $this->security->xss_clean($this->input->post('jenis_obat',TRUE));
		$deskripsi = $this->security->xss_clean($this->input->post('deskripsi',TRUE));

		$jumlah = count($nama_obats);

		date_default_timezone_set('Asia/Jakarta');

		$transaksi_salemba = array();

		$transaksi_salemba['jenis'] = $jenis;
		$transaksi_salemba['tgl'] = date('d-M-Y');
		$transaksi_salemba['jam'] = date('H:i:s');
		$transaksi_salemba['deskripsi'] = $deskripsi;
		$transaksi_salemba['username_pegawai'] = $this->session->userdata('username');

		$this->obat_model->_tambah($transaksi_salemba, $this->t_transaksi_obat_salemba);

		$id_log_baru = (int) $this->obat_model->_ambil_log_salemba()->id;

		for($i = 0; $i < $jumlah; ++$i) {

			$detil_transaksi = array();
			$detil_transaksi['id_transaksi_salemba'] = $id_log_baru;
			$detil_transaksi['nama_obat'] = $nama_obats[$i];
			$detil_transaksi['jml_satuan'] = $jumlahs[$i];

			$this->obat_model->_tambah($detil_transaksi, $this->t_detail_transaksi_salemba);

		}
		redirect(site_url('obat_controller/kumpulan_log_salemba'));
	}

	$data['data_kategori'] = array(''=>'Pilih Kategori','Obat-Umum'=>'Obat Umum','Obat-Alkes-dan-BMHP-Gigi'=>'Obat, Alkes, dan BMHP Gigi','Alkes-dan-BMHP-Umum'=>'Alkes dan BMHP Umum','Obat-Emergency'=>'Obat Emergency');
	$data['data_jenis_obat'] = $this->obat_model->_ambil_daftar_jenis_obat();
		//$data['data_nama_obat'] = $this->obat_model->_ambil_daftar_nama_obat();
	$data['title'] = 'Tambah Log Obat Salemba';
	$this->cetak_halaman('obat/_tambah_log_salemba', $data);

}

public function kumpulan_log_salemba($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('home_controller'));

	$data = array();
	$search_keywords_array = array();

	$hasil = $this->obat_model->_ambil_daftar_log_salemba($mulai, $search_keywords_array); 

	$data['daftar_log'] = $hasil['daftar_log'];
	$this->load->library('pagination');
	$config['base_url'] = base_url().'/obat_controller/kumpulan_log_salemba/';
	$config['total_rows'] = $hasil['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['jumlah_baris'] = $hasil['jumlah_baris'];
	$data['title'] = 'Daftar Log Salemba';
	$this->cetak_halaman('obat/_daftar_log_salemba', $data);
}

public function search_log_salemba($mulai = 0) {
	$tanggal_awal = $this->security->xss_clean($this->input->post('tanggal_awal', TRUE));
	$tanggal_akhir = $this->security->xss_clean($this->input->post('tanggal_akhir', TRUE));
	$search_keywords_array = array();

	if($_POST) {
			// echo var_dump($this->input->post('cari'));
			// die();

		if(!empty($tanggal_awal)) { 
			$search_keywords_array['tanggal_awal_salemba'] =  $tanggal_awal;
			$this->obat_model->searchterm_handler('tanggal_awal_salemba',$tanggal_awal); 
		} else {
			$this->session->unset_userdata('tanggal_awal_salemba');	
		}

		if(!empty($tanggal_akhir)) { 
			$search_keywords_array['tanggal_akhir_salemba'] =  $tanggal_akhir;
			$this->obat_model->searchterm_handler('tanggal_akhir_salemba',$tanggal_akhir); 
		} else {
			$this->session->unset_userdata('tanggal_akhir_salemba');	
		}

		if($this->input->post("hapus", TRUE)) {
			$this->session->unset_userdata('tanggal_awal_salemba');
			$this->session->unset_userdata('tanggal_akhir_salemba');
			$search_keywords_array = array();
			redirect('obat_controller/kumpulan_log_salemba');
		}

	}
	else {
		if($this->session->userdata('tanggal_awal_salemba')) {
			$search_keywords_array['tanggal_awal_salemba'] = $this->session->userdata('tanggal_awal_salemba');
		}

		if($this->session->userdata('tanggal_akhir_keluar')) {
			$search_keywords_array['tanggal_akhir_salemba'] = $this->session->userdata('tanggal_akhir_salemba');
		}
	}

	$data_log = $this->obat_model->_ambil_daftar_log_salemba($mulai, $search_keywords_array);
	$data = array();
	$data['daftar_log'] = $data_log['daftar_log']; 
	$this->load->library('pagination');
	$config['base_url'] = base_url().'index.php/obat_controller/search_log_salemba/';
	$config['total_rows'] = $data_log['jumlah_baris'];
	$this->pagination->initialize($config);
	$data['pagination_links'] = $this->pagination->create_links();
	$data['mulai'] = $mulai;
	$data['title'] = 'Daftar Log Salemba';

	$this->cetak_halaman('obat/_daftar_salemba_cari', $data);
}

public function detil_log_salemba($id) {
	if($this->cek_int_only($id)) {
		$hasil = $this->obat_model->_ambil_detil_salemba($id);

		if($hasil['data_obat']->num_rows() > 0) {
			$data['datas'] = $hasil['data_log'];
			$data['obats'] = $hasil['data_obat'];
			$this->cetak_halaman('obat/_detil_log_salemba', $data);
		}
		else {
			show_404();
		}
	}
	else {
		show_404();
	}
}

public function hapus_kadaluarsa() {
	//if(!$this->cek_login()) redirect(site_url('index.php/home_controller'));

	$obat_kadaluarsas = $this->obat_model->_ambil_semua_obat_kadaluarsa();

	if(count($obat_kadaluarsas) > 0) {

		date_default_timezone_set('Asia/Jakarta');
		$data_log_keluar = array();
		$data_log_keluar['deskripsi'] = 'Keluaran Obat Kadaluarsa';
		$data_log_keluar['tgl'] = date('d-M-Y');
		$data_log_keluar['jam'] = date('H:i:s');
		$data_log_keluar['jenis'] = 'Obat Kadaluarsa';
		$data_log_keluar['username_pemberi'] = $this->session->userdata('username');
		$data_log_keluar['no_surat_jalan'] = null;
		$data_log_keluar['penerima'] = null;
		$data_log_keluar['tgl_penerimaan'] = null;
		$data_log_keluar['id_resep'] = null;


		$this->db->trans_start();
		$this->resep_model->_tambah($data_log_keluar, $this->t_log_obat_keluar);

		foreach ($obat_kadaluarsas as $obat_kadaluarsa) {

			$stok_dibuang = $obat_kadaluarsa->jml_satuan;

			$nama_obat = $obat_kadaluarsa->nama_obat;

			$stok_baru = array();
			$stok_baru['jml_satuan'] = 0;
			$stok_baru['status_kadaluarsa'] = '1';

			$this->obat_model->_ubah(array('nama_obat'=>$nama_obat, 'tgl_kadaluarsa'=>$obat_kadaluarsa->tgl_kadaluarsa), $stok_baru, $this->t_stok_obat);

			$obat_global = $this->obat_model->_ambil_baris_tertentu(array('nama'=>$nama_obat), $this->t_obat, 'row');

			$stok_total_baru = array();
			$stok_total_baru['total_satuan'] = $obat_global->total_satuan - $stok_dibuang;

			$this->obat_model->_ubah(array('nama'=>$nama_obat), $stok_total_baru, $this->t_obat);

			$log_obat_keluar = $this->resep_model->_ambil_log_keluar();

			$pengeluaran = array();
			$pengeluaran['id_log_obat_keluar'] = $log_obat_keluar->id;
			$pengeluaran['nama_obat'] = $obat_kadaluarsa->nama_obat;
			$pengeluaran['jumlah'] = $stok_dibuang;
			$pengeluaran['sisa_obat'] = $stok_total_baru['total_satuan'];

			$this->obat_model->_tambah($pengeluaran, $this->t_pengeluaran_obat);
		}

		$this->db->trans_complete();

		redirect(base_url('index.php/obat_controller/kumpulan_obat_detil'));
	}
	else {
		// $data = array();
		// $data['title'] = 'obat';
		// return $this->cetak_halaman('obat/_notifikasi_buang', $data);
	}
}


public function catat_jumlah_pemakaian_rutin() {

	date_default_timezone_set('Asia/Jakarta');

	$waktu_skrg = date('H:i');

		//if($waktu_skrg >= '21:00' && $waktu_skrg <= '22:00') {

	$bulan_skrg = date('m');
	$tahun_skrg = date('Y');

	$obat_penerimaan = $this->model_laporan->get_delivered_stok_this_month($bulan_skrg, $tahun_skrg);
	$obat_pemakaian = $this->model_laporan->get_pemakaian_depok($bulan_skrg, $tahun_skrg);

	$data_obats;


	foreach ($obat_penerimaan as $obat) {
		$data_obats[$obat->nama_obat]['jumlah_penerimaan'] = $obat->jumlah_penerimaan_stok;
	}

	foreach ($obat_pemakaian as $obat) {
		$data_obats[$obat->nama_obat]['jumlah_pemakaian'] = $obat->jumlah_pemakaian;
	}



	foreach ($data_obats as $nama_obat => $value) {
		$where = array('nama_obat'=>$nama_obat, 'bulan'=>$bulan_skrg, 'tahun'=>$tahun_skrg);


		if($this->obat_model->_cek_ada_atribut($where, $this->t_stok_awal_bulan)) {

			$data = array();

			if(isset($data_obats[$nama_obat]['jumlah_penerimaan'])) {
				$data['jumlah_penerimaan'] = $data_obats[$nama_obat]['jumlah_penerimaan'];
			}
			else {
				$data['jumlah_penerimaan'] = 0;
			}

			if(isset($data_obats[$nama_obat]['jumlah_pemakaian'])) {
				$data['jumlah_pemakaian'] = $data_obats[$nama_obat]['jumlah_pemakaian'];
			}
			else {
				$data['jumlah_pemakaian'] = 0;
			}

			$this->obat_model->_ubah($where ,$data, $this->t_stok_awal_bulan);

		}
		else {

			$data = array();
			$data['nama_obat'] = $nama_obat;
			$data['bulan'] = $bulan_skrg;
			$data['tahun'] = $tahun_skrg;
			$data['jumlah_stok'] = 0;

			if(isset($data_obats[$nama_obat]['jumlah_penerimaan'])) {
				$data['jumlah_penerimaan'] = $data_obats[$nama_obat]['jumlah_penerimaan'];
			}
			else {
				$data['jumlah_penerimaan'] = 0;
			}

			if(isset($data_obats[$nama_obat]['jumlah_pemakaian'])) {
				$data['jumlah_pemakaian'] = $data_obats[$nama_obat]['jumlah_pemakaian'];
			}
			else {
				$data['jumlah_pemakaian'] = 0;
			}

			$data['jumlah_sisa'] = ($data['jumlah_penerimaan'] + $data['jumlah_stok']) - $data['jumlah_pemakaian'];

			$this->obat_model->_tambah($data, $this->t_stok_awal_bulan);

		}
	}
		//}
}

public function catat_jumlah_obat_datang() {

	date_default_timezone_set('Asia/Jakarta');
	$tgl_skrg = date('d');


	if($tgl_skrg == '01') {
		$bulan_skrg = date('m');
		$tahun_skrg = date('Y');

		$bulan_huruf = date('M');


		if($this->obat_model->_cek_ada_atribut(array('bulan'=>$bulan_skrg, 'tahun'=>$tahun_skrg), $this->t_stok_awal_bulan)) {
			echo "Daftar jumlah stok, jumlah pemakaian, jumlah penerimaan, dan jumlah sisa dari setiap obat sudah dicatat pada tanggal 1 $bulan_huruf $tahun_skrg";
			die();
		}



		$obat_stok = $this->model_laporan->get_stok_pemakaian_sisa($bulan_skrg, $tahun_skrg, 'CATAT');
		$obat_penerimaan = $this->model_laporan->get_delivered_stok_this_month($bulan_skrg, $tahun_skrg);
		$obat_pemakaian = $this->model_laporan->get_pemakaian_depok($bulan_skrg, $tahun_skrg, 'CATAT');

		$data_obats;

		foreach ($obat_stok as $obat) {
			$data_obats[$obat->nama_obat]['jumlah_stok'] = $obat->total_stok; 
		}

		foreach ($obat_penerimaan as $obat) {
			$data_obats[$obat->nama_obat]['jumlah_penerimaan'] = $obat->jumlah_penerimaan_stok;
		}

		foreach ($obat_pemakaian as $obat) {
			$data_obats[$obat->nama_obat]['jumlah_pemakaian'] = $obat->jumlah_pemakaian_depok;
		}

		foreach ($data_obats as $nama_obat => $value) {

			$temp_jml_stok = 0;
			$temp_jml_penerimaan = 0;
			$temp_jml_pemakaian = 0;
			$temp_jml_sisa = 0;

			if(isset($data_obats[$nama_obat]['jumlah_stok'])) {
				$temp_jml_stok = $data_obats[$nama_obat]['jumlah_stok'];
			}

			if(isset($data_obats[$nama_obat]['jumlah_penerimaan'])) {
				$temp_jml_penerimaan = $data_obats[$nama_obat]['jumlah_penerimaan'];
			}

			if(isset($data_obats[$nama_obat]['jumlah_pemakaian'])) {
				$temp_jml_pemakaian = $data_obats[$nama_obat]['jumlah_pemakaian'];
			}

			$temp_jml_sisa = ($temp_jml_stok + $temp_jml_penerimaan) - $temp_jml_pemakaian;

			$data = array();
			$data['nama_obat'] = $nama_obat;
			$data['bulan'] = $bulan_skrg;
			$data['tahun'] = $tahun_skrg;
			$data['jumlah_stok'] = $temp_jml_stok;
			$data['jumlah_penerimaan'] = $temp_jml_penerimaan;
			$data['jumlah_pemakaian'] = $temp_jml_pemakaian;
			$data['jumlah_sisa'] = $temp_jml_sisa;

			$this->obat_model->_tambah($data, $this->t_stok_awal_bulan);
		}

	}
	else {
		echo "Masih belum masuk periode pencatatan rutin per tanggal 1 di awal bulan";
	}

}


}

/* End of file obat_controller.php */
/* Location: ./application/controllers/obat_controller.php */