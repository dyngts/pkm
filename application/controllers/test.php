<?php

class Test extends CI_Controller {
    
  function __construct(){
    parent::__construct();
  }

	public function index() {
		$this->load->model('laporan_model');
		// $jenispoli, $jenispasien, $bulan, $tahun, $jammulai, $jamselesai
		$wa =$this->laporan_model->getpoli('1', '3', '9', '2013', "'".date('H:i:s', strtotime('00:00'))."'",  "'".date('H:i:s', strtotime('23:59'))."'", 'Lain-lain');
		
		$wawa = $this->laporan_model->getlaporanpoli(1, 9, 2013);
		$wawawa = $this->laporan_model->get_umur_range('1', '9', '2013', '0', '1000');
		$wawawawa = $this->laporan_model->get_umur('9', '2013');

		print_r($wawawawa);



	}

	public function p() {
		$id = $this->authentication->get_my_id();
		$str = $this->user_model->get_user_password_by_id($id);
		print_r($str);
	}

	public function create_pegawai() {
		$this->load->model('user_model');

		$tablepegawai = "pegawai";

		$nama = "Mozilla Firefox"; 
		$jenis_kelamin = "L";
		$agama = "Islam";  
		$gol_darah = "A";
		$tempat_lahir = "Jakarta"; 
		$tanggal_lahir = "01-01-1981"; 
		$status_pernikahan = "sudah_menikah";
		$alamat = "Jalan Sawo 1";
		$jenis_pasien = "5";
		$lembaga_fakultas = "-";
		$kewarganegaraan = "Indonesia";
		$foto = "-";
		$no_telepon = "568455";
		$no_identitas = "568455";
		$username = $this->random_string(6);
		$password = md5("mozilla");

		$email = "emailku@provider.com";
		$id_otorisasi = 5;
		$status_aktif = "false";

		$datapegawai_gen = array(
			'nama' => $nama, 
			'jenis_kelamin' => $jenis_kelamin,
			'email' => $email,
			'alamat' => $alamat,
			'no_telepon' => $no_telepon,
			'foto' => $foto,
			'username' => $username,
			'id_otorisasi' => $id_otorisasi,
			'status_aktif' => $status_aktif,
			);

		$datapegawai_auth = array(
				'username' => $username, 
				'password' => $password,
				'id_otorisasi' => $id_otorisasi,
			);
		$datapegawai = array(
				'data_auth' => $datapegawai_auth,
				'data_general' => $datapegawai_gen,
			);
		$this->user_model->insert_pegawai($datapegawai);
	}

	public function update_pegawai($id) {
		$this->load->model('user_model');

		$tablepegawai = "pegawai";

		$nama = "Google Chrome"; 
		$jenis_kelamin = "L";
		$agama = "Islam";  
		$gol_darah = "A";
		$tempat_lahir = "Jakarta"; 
		$tanggal_lahir = "01-01-1981"; 
		$status_pernikahan = "sudah_menikah";
		$alamat = "Jalan Sawo 1";
		$jenis_pasien = "7";
		$lembaga_fakultas = "-";
		$kewarganegaraan = "Indonesia";
		$foto = "-";
		$no_telepon = "568455";
		$no_identitas = "568455";
		$username = 'nanana';
		$password = md5("nanana");

		$old_username = 'wisuhi';

		$email = "emailku@provider.com";
		$id_otorisasi = 5;
		$status_aktif = "false";

		$datapegawai_gen = array(
			'nama' => $nama, 
			'jenis_kelamin' => $jenis_kelamin,
			'email' => $email,
			'alamat' => $alamat,
			'no_telepon' => $no_telepon,
			'foto' => $foto,
			'username' => $username,
			'id_otorisasi' => $id_otorisasi,
			);

		$datapegawai_auth = array(
			'username' => $username, 
			'password' => $password,
			'id_otorisasi' => $id_otorisasi,
			);

		$data_id = array(
			'id' => $id,
			'username' => $old_username,
			);

		$datapegawai = array(
			'data_id' => $data_id,
			'data_auth' => $datapegawai_auth,
			'data_general' => $datapegawai_gen,
			);
		$this->user_model->update_pegawai($datapegawai);
	}

	public function create_pasien() {
		$this->load->model('user_model');

		$tablepasien = "pasien";

		$nama = "Mozilla Firefox"; 
		$jenis_kelamin = "L";
		$agama = "Islam";  
		$gol_darah = "A";
		$tempat_lahir = "Jakarta"; 
		$tanggal_lahir = "01-01-1981"; 
		$status_pernikahan = "sudah_menikah";
		$alamat = "Jalan Sawo 1";
		$jenis_pasien = "1";
		$lembaga_fakultas = "-";
		$kewarganegaraan = "Indonesia";
		$foto = "-";
		$no_telepon = "568455";
		$no_identitas = "568455";
		$username = $this->random_string(6);
		$password = md5("mozilla");

		$email = "emailku@provider.com";
		$id_otorisasi = 1;

		$datapasien_gen = array(
			'id' => $id,
			'nama' => $nama, 
			'jenis_kelamin' => $jenis_kelamin,
			'agama' => $agama,  
			'gol_darah' => $gol_darah,
			'tempat_lahir' => $tempat_lahir, 
			'tanggal_lahir' => $tanggal_lahir, 
			'status_pernikahan' => $status_pernikahan,
			'alamat' => $alamat,
			'jenis_pasien' => $jenis_pasien,
			'lembaga_fakultas' => $lembaga_fakultas,
			'kewarganegaraan' => $kewarganegaraan,
			'foto' => "-",
			'no_telepon' => $no_telepon,
			'no_identitas' => $no_identitas,
			'username' => $username,
			'password' => $password,
			);

		$datapasien_auth = array(
				'username' => $username, 
				'password' => $password,
				'id_otorisasi' => $id_otorisasi,
			);
		$datapasien = array(
				'data_auth' => $datapasien_auth,
				'data_general' => $datapasien_gen,
			);
		$this->user_model->insert_pasien($datapasien);
	}

	public function update_pasien($id) {
		$this->load->model('user_model');

		$tablepasien = "pasien";

		$nama = "Google Chrome"; 
		$jenis_kelamin = "L";
		$agama = "Islam";  
		$gol_darah = "A";
		$tempat_lahir = "Jakarta"; 
		$tanggal_lahir = "01-01-1981"; 
		$status_pernikahan = "sudah_menikah";
		$alamat = "Jalan Sawo 1";
		$jenis_pasien = "1";
		$lembaga_fakultas = "-";
		$kewarganegaraan = "Indonesia";
		$foto = "-";
		$no_telepon = "568455";
		$no_identitas = "568455";
		$username = 'M10003';
		$password = md5("google");

		$old_username = $username;

		$email = "emailku@provider.com";
		$id_otorisasi = 1;

		$datapasien_gen = array(
			'nama' => $nama, 
			'jenis_kelamin' => $jenis_kelamin,
			'agama' => $agama,  
			'gol_darah' => $gol_darah,
			'tempat_lahir' => $tempat_lahir, 
			'tanggal_lahir' => $tanggal_lahir, 
			'status_pernikahan' => $status_pernikahan,
			'alamat' => $alamat,
			'jenis_pasien' => $jenis_pasien,
			'lembaga_fakultas' => $lembaga_fakultas,
			'kewarganegaraan' => $kewarganegaraan,
			'foto' => "-",
			'no_telepon' => $no_telepon,
			'no_identitas' => $no_identitas,
			'username' => $username,
			'password' => $password,
			);

		$datapasien_auth = array(
				'username' => $username, 
				'password' => $password,
				'id_otorisasi' => $id_otorisasi,
			);

		$data_id = array(
			'id' => $id,
			'username' => $old_username,
			);

		$datapasien = array(
				'data_id' => $data_id,
				'data_auth' => $datapasien_auth,
				'data_general' => $datapasien_gen,
			);
		$this->user_model->update_pasien($datapasien);
	}

	public function verify($user_id) {
		$this->load->model('user_model');
		$table = "pasien";
		$username = $this->user_model->get_username_by_id($user_id);	
		$data = $this->user_model->get_user_by_username($username);
		print_r($data);
		$this->user_model->verify($table,$data);
	}

  public function phpinfo()
  {
  	echo phpinfo();
  }

  public function md5($term)
  {
  	echo md5($term);
  }

  public function random_string($length) 
  {
  	$key = "";
  	$keys = range('a', 'z');
    $keys0 = array('c','q','f','x','v','z');
    $keys1 = array('a','i','u','e','o');
  	$keys2 = array_diff($keys, array_merge($keys0,$keys1));
    
    for($i=0; $i < $length; $i++) {
    		if ($i%2 == 1) {
    			$key = $key.$keys1[array_rand($keys1)];
    		}
    		else {
    			$key = $key.$keys2[array_rand($keys2)];
    		}
    }
    return $key;
	}

  public function random_string2($length) 
  {
  	$key = "";
    $keys = array_merge(range(0,9), range('a', 'z'));
    for($i=0; $i < $length; $i++) {
        $key = $key.$keys[array_rand($keys)];
    }
    return $key;
	}

	public function is_unique_username($username) {
		if ($this->user_model->is_unique_username($username)) return TRUE;
		else return FALSE;
	}

	public function authenticate($username,$password) {
		$this->authentication->is_umum($username,$password);
	}
}
