<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class statistik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('antrian_model');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('javascript');
		$this->load->model('authentication');
		$this->load->model('statistik_model');
	}

	public function index()
	{
		$page = "stats";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		
		$data['title'] = "Statistik"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function periode() {
		$this->load->model('statistik_model');
		$page = "statsp";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$tahun = $this->input->post('tahun');
		if ($tahun != "") {
			$data['title'] = "Statistik"; 
			$data['query'] = $this->statistik_model->get_statistik_periode($tahun);
			$data['tahun'] = $tahun;
			$this->load->view('templates/header', $data);
	        $this->load->model('authentication');
			if($this->authentication->is_loggedin()){
	            $this->load->view('usermenu/usermenu', $data);
	        }
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);

		}
		else 
		{
			
			$data['title'] = "Statistik";
			$data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";
			$this->load->view('templates/header', $data);
	        $this->load->model('authentication');
			if($this->authentication->is_loggedin()){
	            $this->load->view('usermenu/usermenu', $data);
	        }
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/stats', $data);
			$this->load->view('templates/footer', $data);
		}
	}

	public function fakultas() {
		$this->load->model('statistik_model');
		$page = "statsf";
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
		$tahun = $this->input->post('f_tahun');
		$bulan = $this->input->post('f_bulan');

		if ($tahun != "" && $bulan != "") {
			$data['title'] = "Statistik"; 
			$data['query'] = $this->statistik_model->get_statistik_fakultas($tahun, $bulan);
			$data['tahun'] = $tahun;
			$data['bulan'] = $bulan;
			$this->load->view('templates/header', $data);
	        $this->load->model('authentication');
			if($this->authentication->is_loggedin()){
	            $this->load->view('usermenu/usermenu', $data);
	        }
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
		else $this->index();
	}


	public function statistikkepuasan()
	{
		$page = 'statsk';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
        $this->load->model('authentication');
        if($this->authentication->is_pelayanan() || $this->authentication->is_loket()) {

        	$this->load->model('statistik_model');
        	$data['query'] = $this->statistik_model->get_statistik_kepuasan();
           
			$data['title'] = "Statistik Kepuasan"; 
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
        }
		else {
			redirect ('site/index');
		}
	}

	public function get_statistikkepuasan($tahun)
	{
		$page = 'statsk';
		if ( ! file_exists('application/views/site/'.$page.'.php')) {
					show_404();
		}
        $this->load->model('authentication');
        if($this->authentication->is_pelayanan() || $this->authentication->is_loket()) {

        	$this->load->model('statistik_model');
        	$data['query_tidakpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun, "tidakpuas", 1);
        	$data['query_kurangpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"kurangpuas", 2);
        	$data['query_puas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"puas", 3);
        	$data['query_sangatpuas'] = $this->statistik_model->get_statistik_kepuasan($tahun,"sangatpuas", 4);
           
			$data['title'] = "Statistik Kepuasan";
			$data['tahun'] = $tahun;
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
        }
		else {
			redirect ('site/index');
		}
	}

}

/* End of file  */
/* Location: ./application/controllers/ */