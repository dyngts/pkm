<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Koorpel extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('koorpelmod');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->load->model('authentication');
    }

    function rolechecker(){
    	$role = $this->session->userdata('role');
		$access = 0;
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->authentication->is_adm_keu()){
			$access = 1;
		}
        if ($access == 0) {
            show_404();
        }
        return $access;
    }

	function addtindakan() {
		$this->rolechecker();
		$data['title'] = 'Tambah Tindakan';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/tambah_tindakan');
		$this->load->view('templates/footer', $data);
	}

	function addDiagnosis() {
		$this->rolechecker();
		$data['title'] = 'Tambah Diagnosis';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/tambah_diagnosis');
		$this->load->view('templates/footer', $data);
	}

	function form_adddiagnosis(){
		$this->form_validation->set_rules('namadiagnosis', 'Nama Diagnosis', 'required|max_length[50]|strip_tags|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'strip_tags|xss_clean|trim|');
		$datadiagnosis = array(
			'kode' => $this->input->post('kodediagnosis', true),
			'jenis_penyakit'=> $this->input->post('namadiagnosis',true),
			'keterangan'=> $this->input->post('keterangan',true)		
		);

		if($this->form_validation->run() == TRUE) {
			$checkmasukga = $this->koorpelmod->create_diagnosis($datadiagnosis);
			if($checkmasukga) {
				redirect('koorpel/notifadd_d');
			}
			else echo ("<script language ='JavaScript'>
					window.alert('Data yang anda masukkan salah!') 
					location = '';
					</script>");
		}
		else {
			echo ("<script language ='JavaScript'>
						window.alert('Data yang anda masukkan salah!!') 
						</script>");
			$this->form_validation->set_message('required','Field ini harus diisi');
			$this->addDiagnosis();
			
		}
	}

	function form_addtindakan() {
		$this->form_validation->set_rules('idtindakan', 'ID Tindakan', 'required|max_length[5]|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('namatindakan', 'Nama Tindakan', 'required|max_length[50]|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('pilihpoli', 'Pilihan poli', 'required|max_length[20]|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('hargaumum', 'Harga tindakan untuk umum', 'required|xss_clean|trim|numeric');
		$this->form_validation->set_rules('hargakaryawan', 'Harga tindakan karyawan', 'required|xss_clean|trim|numeric');
		$this->form_validation->set_rules('hargamahasiswa', 'Harga tindakan mahasiswa', 'required|xss_clean|trim|numeric');
		$data = array (
				'id'=> $this->input->post('idtindakan', true),
				'jenis_pelayanan' => $this->input->post('pilihpoli',true),
				'nama'=> $this->input->post('namatindakan', true),
				'tarif_umum'=> $this->input->post('hargaumum', true),
				'tarif_karyawan'=> $this->input->post('hargakaryawan', true),
				'tarif_mahasiswa'=> $this->input->post('hargamahasiswa', true),
				'ketentuan'=> $this->input->post('hargamahasiswa', true)
			);
		if($this->form_validation->run()== FALSE){
			echo ("<script language ='JavaScript'>
						window.alert('Data yang anda masukkan salah!') 
						</script>");
			$this->form_validation->set_message('required','');
			
			$data['title'] = 'Tambah Tindakan';
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('koorpel/tambah_tindakan');
			$this->load->view('templates/footer', $data);

		}
		else{
			$checkmasukga = $this->koorpelmod->create_tindakan($data);
			if($checkmasukga) {
				redirect('koorpel/notifadd');
			}
			else echo ("<script language ='JavaScript'>
						window.alert('Data yang anda masukkan salah!') 
						location = '';
						</script>");
		}
	}

	function updatetindakan($idtindakan) {
		$this->rolechecker();
		$getdata = $this->koorpelmod->searchbyId($idtindakan);
		$data['row'] = $getdata->row();
		$data['title'] = 'Mengubah Tindakan';
		$data['role'] = $this->session->userdata('role');
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/update_tindakan');
		$this->load->view('templates/footer', $data);
	}

	function updatediagnosis($kodediagnosis){
		$this->rolechecker();
		$getdata = $this->koorpelmod->searchbyKode($kodediagnosis);
		$data['row'] = $getdata->row();
		$data['title'] = 'Mengubah Diagnosis';
		$data['role'] = $this->session->userdata('role');
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/update_diagnosis');
		$this->load->view('templates/footer', $data);
	}

	function form_updatediagnosis() {
		$this->form_validation->set_rules('namadiagnosis', 'Nama Diagnosis', 'required|max_length[50]|strip_tags|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'strip_tags|trim');
		$datadiagnosis = array(
			'kode' => $this->input->post('kodediagnosis', true),
			'jenis_penyakit'=> $this->input->post('namadiagnosis',true),
			'keterangan'=> $this->input->post('keterangan',true)		
		);
		
		if($this->form_validation->run() == TRUE) {
			$checkmasukga = $this->koorpelmod->updatediagnosis($datadiagnosis);
			if($checkmasukga) {
				redirect('koorpel/notifupdate_d');
			}
			else echo ("<script language ='JavaScript'>
					window.alert('Data yang anda masukkan salah!') 
					location = '';
					</script>");
		}
		else {
			echo ("<script language ='JavaScript'>
						window.alert('Data yang anda masukkan salah!!') 
						</script>");
			$this->form_validation->set_message('required','');
			$this->updatediagnosis($datadiagnosis['kode']);
			
		}
		
	}

	function form_updatetindakan() {
		$this->form_validation->set_rules('namatindakan', 'Nama Tindakan', 'required|max_length[50]|xss_clean|trim|alpha_numeric');
		$this->form_validation->set_rules('poli', 'Pilihan poli', 'required|max_length[20]');
		$this->form_validation->set_rules('hargatindakanu', 'Harga tindakan untuk umum', 'required|xss_clean|trim|numeric');
		$this->form_validation->set_rules('hargatindakank', 'Harga tindakan karyawan', 'required|xss_clean|trim|numeric');
		$this->form_validation->set_rules('hargatindakanm', 'Harga tindakan mahasiswa', 'required|xss_clean|trim|numeric');

		$datatindakan = array(
			'id' => $this->input->post('idtindakan', true),
			'nama'=> $this->input->post('namatindakan',true),
			'jenis_pelayanan'=> $this->input->post('poli',true),
			'tarif_umum' =>$this->input->post('hargatindakanu',true),
			'tarif_karyawan' =>$this->input->post('hargatindakank',true),
			'tarif_mahasiswa' =>$this->input->post('hargatindakanm',true),
			'ketentuan'=>$this->input->post('ketentuan',true),			
		);
		
		if($this->form_validation->run() == TRUE) {
			$checkmasukga = $this->koorpelmod->updatetindakan($datatindakan);
			if($checkmasukga) {
				redirect('koorpel/notifupdate_d');
			}
			else echo ("<script language ='JavaScript'>
					window.alert('Data yang anda masukkan salah!') 
					location = '';
					</script>");
		}
		else {
			echo ("<script language ='JavaScript'>
						window.alert('Data yang anda masukkan salah!') 
						</script>");
			$this->form_validation->set_message('required','');
			$this->updatetindakan($datatindakan['id']);
			
		}
		
	}

	function deletetindakan($idtindakan){
		$this->rolechecker();
		$getdata = $this->koorpelmod->searchbyId($idtindakan);
		$data['row'] = $getdata->row();
		$data['title'] = 'Hapus Tindakan';
		$data['role'] = $this->session->userdata('role');
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/hapus_tindakan');
		$this->load->view('templates/footer', $data);
	}

	function form_deletetindakan($idtindakan) {
		$getdata = $this->koorpelmod->searchbyId($idtindakan);
		$arraydata = $getdata->row();
		$datatindakan = array(
			'id' => $arraydata->id,
			'nama'=> $arraydata->nama,
			'jenis_pelayanan'=> $arraydata->jenis_pelayanan,
			'tarif_umum' =>$arraydata->tarif_umum,
			'tarif_karyawan' =>$arraydata->tarif_karyawan,
			'tarif_mahasiswa' =>$arraydata->tarif_mahasiswa,
			'ketentuan'=>$arraydata->ketentuan
			);
		$checkdelete = $this->koorpelmod->deletetindakan($datatindakan);
		if($checkdelete) {
			redirect('koorpel/notifdelete');
		}
		
	}

	function deletediagnosis($kodediagnosis){
		$this->rolechecker();
		$getdata = $this->koorpelmod->searchbyKode($kodediagnosis);
		$data['row'] = $getdata->row();
		$data['title'] = 'Mengubah Diagnosis';
		$data['role'] = $this->session->userdata('role');
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/hapus_diagnosis');
		$this->load->view('templates/footer', $data);
	}

	function form_deletediagnosis($kodediagnosis) {
		$getdata = $this->koorpelmod->searchbyKode($kodediagnosis);
		$arraydata = $getdata->row();
		$datadiagnosis = array(
			'kode' => $arraydata->kode,
			'jenis_penyakit'=> $arraydata->jenis_penyakit,
			'keterangan'=>$arraydata->keterangan
			);
		$checkdelete = $this->koorpelmod->deletediagnosis($datadiagnosis);
		if($checkdelete) {
			redirect('koorpel/notifdelete_d');
		}
	}


	function searchtindakan(){
		$this->rolechecker();
		$data['title'] = 'Hasil pencarian tindakan';
		$search= $this->input->post('searchquery', true);
		$result = $this->koorpelmod->search_tindakan($search);
		$data['query'] =$result->result_array();
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/search_tindakan_result');
		$this->load->view('templates/footer', $data);
	}

	function searchdiagnosis(){
		$this->rolechecker();
		$data['title'] = 'Hasil pencarian diagnosis';
		$search= $this->input->post('searchquery', true);
		$result = $this->koorpelmod->search_diagnosis($search);
		$data['query'] =$result->result_array();
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/search_diagnosis_result');
		$this->load->view('templates/footer', $data);
	}

	function notifadd(){
		$data['title'] = 'Tindakan Berhasil Ditambah';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifadd');
		$this->load->view('templates/footer', $data);
	}

	function notifupdate(){
		$data['title'] = 'Tindakan Berhasil Diubah';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifupdate');
		$this->load->view('templates/footer', $data);
	}

	function notifdelete(){
		$data['title'] = 'Tindakan Berhasil Dihapus';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifdelete');
		$this->load->view('templates/footer', $data);
	}

	function notifadd_d(){
		$data['title'] = 'Tindakan Berhasil Ditambah';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifadd_d');
		$this->load->view('templates/footer', $data);
	}

	function notifupdate_d(){
		$data['title'] = 'Tindakan Berhasil Diubah';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifupdate_d');
		$this->load->view('templates/footer', $data);
	}

	function notifdelete_d(){
		$data['title'] = 'Tindakan Berhasil Dihapus';
		$this->load->view('templates/header', $data);
		$this->load->view('usermenu/usermenu', $data);
		$this->load->view('koorpel/notifdelete_d');
		$this->load->view('templates/footer', $data);
	}
}