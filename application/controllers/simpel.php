<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simpel extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('authentication');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

	}

	public function index() {	
		redirect('site/index');
	}

	function rolechecker(){
    	$role = $this->session->userdata('role');
		$access = 0;
		if($this->authentication->is_pelayanan() || $this->authentication->is_pj() || $this->authentication->is_adm_keu()){
			$access = 1;
		}
        if ($access == 0) {
            show_404();
        }
        return $access;
    }

	public function tindakan($index=null) {
		$this->rolechecker();
		$this->load->library('pagination');	
		$jml = $this->db->get(SCHEMA.'tindakan');
		$this->load->model('koorpelmod');
		$data['title'] = "Daftar Tindakan"; 
		$config['base_url'] = base_url().'index.php/simpel/tindakan';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '15';
		$config['first_page'] = 'Awal';
		$config['last_page'] = 'Akhir';
		$this->pagination->initialize($config);
		$query = $this->koorpelmod->retrieveTindakan($config['per_page'], $index);
		$data['halaman'] = $this->pagination->create_links();
		$data['query'] = $query->result_array();
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        } 
		else {
			$this->load->view('usermenu/loginform', $data);
		}
		$this->load->view('tindakan',$data);
		$this->load->view('templates/footer');
	}
	
	public function diagnosis($index=null){
		$this->rolechecker();
		$this->load->model('koorpelmod');
		$this->load->library('pagination');	
		$jml = $this->db->get(SCHEMA.'diagnosis');
		$this->load->model('koorpelmod');
		$data['title'] = "Daftar Tindakan"; 
		$config['base_url'] = base_url().'index.php/simpel/diagnosis';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '15';
		$config['first_page'] = 'Awal';
		$config['last_page'] = 'Akhir';
		$this->pagination->initialize($config);
		$query = $this->koorpelmod->retrieveDiagnosis($config['per_page'], $index);
		$data['halaman'] = $this->pagination->create_links();
		$data['query'] = $query->result_array();
		$data['title'] = " Daftar Diagnosis"; 
		$this->load->view('templates/header', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        } 
		else {
			$this->load->view('usermenu/loginform', $data);
		}
		$this->load->view('diagnosis',$data);
		$this->load->view('templates/footer');
	}
	
}
