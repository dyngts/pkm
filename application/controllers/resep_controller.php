<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resep_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('resep_model');
		$this->get_model('surat_jalan_model');
	}

	public function index()
	{

	}

	public function kumpulan($mulai = 0) {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		$data = array();

		$hasil = $this->resep_model->_ambil_daftar_resep($mulai);

		$data['reseps'] = $hasil['reseps'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/resep_controller/kumpulan';
		$config['total_rows'] = $hasil['jumlah_baris'];
    	$this->pagination->initialize($config);
    	$data['pagination_links'] = $this->pagination->create_links();
    	$data['mulai'] = $mulai;
    	$data['title'] = 'Daftar Resep';
		$this->cetak_halaman('resep/_daftar_resep', $data);
	}

	public function detil($id = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		if($this->cek_int_only($id)) {
			$hasil = $this->resep_model->_ambil_dokter_pasien($id);

			if($hasil['daftar_obats']->num_rows() > 0) {
				$data['daftar_obats'] = $hasil['daftar_obats'];
				$data['data_resep'] = $hasil['data_resep'];
				$data['title'] = 'Detil Resep - '.$id;
				$this->cetak_halaman('resep/_detil_resep', $data);
			}
			else {
				show_404();
			}
		}
		else {
			show_404();
		}	
	}

	public function proses_resep($id = '') {
		//if(!$this->authentication->is_loggedin()) redirect(site_url('/home_controller'));

		if($this->input->post('batal') == "Batal") {
			redirect(site_url('/resep_controller/kumpulan'));
		}

		$this->load->library('form_validation');

		$hasil = $this->resep_model->_ambil_dokter_pasien($id);

		$daftar_obats = $hasil['daftar_obats'];
		$jumlah_baris = count($daftar_obats);

		$tidak_cukup = FALSE;
		$daftar_tidak_cukup = array();
		$jumlah_dibutuhkan = array();
		$jumlah_tersedia = array();

		foreach ($daftar_obats->result() as $obat) {
			$obat_temp = $this->resep_model->_ambil_obat_tertentu_total($obat->nama_obat);
			if($obat_temp->total < $obat->jumlah) {
				$tidak_cukup = TRUE;
				$daftar_tidak_cukup[] = $obat_temp->nama_obat;
				$jumlah_dibutuhkan[] = $obat->jumlah;
				$jumlah_tersedia[] = $obat_temp->total;
			}
			
		}

		if($tidak_cukup == TRUE) {
			$list_obat['daftar_tidak_cukup'] = $daftar_tidak_cukup;
			$list_obat['jumlah_dibutuhkan'] = $jumlah_dibutuhkan;
			$list_obat['jumlah_tersedia'] = $jumlah_tersedia;
			$list_obat['tanggal'] = $tanggal;
			$list_obat['jam'] = $jam;
			$list_obat['id_rekam_medis'] = $obat->id_rekam_medis;
			$list_obat['tanggal'] = $obat->tanggal;
			$list_obat['jam'] = $obat->jam;
			$list_obat['nama_aktif'] = 'resep';
			return $this->cetak_halaman('resep/_notifikasi', $list_obat);
		}

		date_default_timezone_set('Asia/Jakarta');
		$data_log_keluar = array();
		$data_log_keluar['deskripsi'] = 'Keluaran Resep';
 		$data_log_keluar['tgl'] = date('d-M-Y');
 		$data_log_keluar['jam'] = date('H:i:s');
 		$data_log_keluar['jenis'] = 'Resep';
		$data_log_keluar['username_pemberi'] = 'mahasiswa01'; //$this->session->userdata('username');
		$data_log_keluar['no_surat_jalan'] = null;
		$data_log_keluar['penerima'] = $obat->nama_pasien;
		$data_log_keluar['tgl_penerimaan'] = null;
		$data_log_keluar['id_resep'] = (int) $id;


		$this->db->trans_start();
		$this->resep_model->_tambah($data_log_keluar, $this->t_log_obat_keluar);
		



		foreach ($daftar_obats->result() as $data_resep_obat) {
			$nama_obat = $data_resep_obat->nama_obat;
			$jumlah_obat = $data_resep_obat->jumlah;
			$jumlah_stok_keluar = $jumlah_obat;

			$kad_obats = $this->resep_model->_ambil_baris_tertentus($nama_obat);

			for($j = 0;($jumlah_stok_keluar != 0); $j++) {
				$stok_baru = array();
				if($jumlah_stok_keluar <= $kad_obats[$j]->jml_satuan) {
					$stok_baru['jml_satuan'] = $kad_obats[$j]->jml_satuan - $jumlah_stok_keluar;  
					$this->resep_model->_ubah(array('nama_obat'=>$kad_obats[$j]->nama_obat, 'tgl_kadaluarsa'=>$kad_obats[$j]->tgl_kadaluarsa), $stok_baru, $this->t_stok_obat);
					$jumlah_stok_keluar = 0;
				}
				else {					
					$stok_baru['jml_satuan'] = 0;
					$jumlah_stok_keluar = $jumlah_stok_keluar - $kad_obats[$j]->jml_satuan;
					$this->surat_jalan_model->_ubah(array('nama_obat'=>$kad_obats[$j]->nama_obat, 'tgl_kadaluarsa'=>$kad_obats[$j]->tgl_kadaluarsa), $stok_baru, $this->t_stok_obat);
				}
			}

			$obat = $this->resep_model->_ambil_baris_tertentu(array('nama'=>$nama_obat), $this->t_obat, 'row'); 
			$total_stok_baru = array();
			$total_stok_baru['total_satuan'] = $obat->total_satuan - $jumlah_obat; 
			$this->resep_model->_ubah(array('nama'=>$nama_obat), $total_stok_baru, $this->t_obat);


			$log_obat_keluar = $this->resep_model->_ambil_log_keluar();

			$pengeluaran = array();
			$pengeluaran['id_log_obat_keluar'] = $log_obat_keluar->id;
			$pengeluaran['nama_obat'] = $kad_obats[$j]->nama_obat;
			$pengeluaran['jumlah'] = $jumlah_obat;
			$pengeluaran['sisa_obat'] = $total_stok_baru['total_satuan'];

			$this->surat_jalan_model->_tambah($pengeluaran, $this->t_pengeluaran_obat);
		}

		$data_ganti_resep = array();
		$data_ganti_resep['status_proses'] = '1';

		$id_resep = $this->uri->segment(3);

		$this->resep_model->_ubah(array('id'=>$id_resep), $data_ganti_resep, $this->t_resep);

		$this->db->trans_complete();

		redirect(site_url('/resep_controller/kumpulan'));
	}
 
}

/* End of file resep_controller.php */
/* Location: ./application/controllers/resep_controller.php */