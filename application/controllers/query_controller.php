<?php

class Query_controller extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
		$this->load->model('authentication');
		$this->load->model('antrian_model');
		$this->load->model('pegawai_model');
		$this->load->model('user_model');
		$this->load->model('artikel_model');
		$this->load->model('query_model');

		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('javascript');
		$this->load->library('calendar');

		$this->load->helper('date');
		$this->load->helper('date');
		$this->load->helper('security');
		$this->load->helper('captcha');
		$this->load->helper('form');
		$this->load->helper('text');

    }

	public function index() {
		echo "<h1>It Works!</h1>";
	}

	public function akun() {
		$data['query_akun'] = $this->query_model->get_akun();
		$data['akun'] = true;
		$data['pasien_simple'] = false;
		$this->load->view('query',$data);
	}

	public function pasien_simple() {
		$data['query_pasien_simple'] = $this->query_model->get_pasien_simple();
		$data['pasien_simple'] = true;
		$data['akun'] = false;
		$this->load->view('query',$data);
	}
	
	
}
