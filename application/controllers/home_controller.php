<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('pengguna_model');
		
	}

	public function index()
	{
		
		$data['error'] = 0;
		
		
		
		

		if(!$this->session->userdata('username')) {
			$this->load->view('home/index', $data);
		}
		else {
			redirect(site_url('index.php/home_controller/welcome'));
		}
	}


	public function login() {
		$data['error'] = 0;
		// Cek jika ada data username dan password yang masuk
		if($_POST) {
			// Mengambil data username dan password dari form login
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);


			$pengguna = $this->pengguna_model->login($username,$password);
			
			// Jika tidak ada apoteker
			if(!$pengguna) $data['error'] = 1;
			else {
				// jika ada maka buat session untuk username dan role
				$this->session->set_userdata('username', $pengguna['username']);
				$this->session->set_userdata('nip', $pengguna['nip']);
				$this->session->set_userdata('flag_admin', $pengguna['flag_admin']);

				redirect(site_url('index.php/home_controller/welcome'));
			}
		}

		if(!$this->session->userdata('username')) {
			$this->load->view('home/index', $data);
		}
		else {
			redirect(base_url().'index.php/home_controller/welcome');
		}
	}

	public function logout() {
		if(!$this->cek_login()) redirect(site_url('index.php/home_controller'));
		$this->session->sess_destroy();
		redirect(site_url('index.php/home_controller/index'));
	}

	public function welcome() {
		if(!$this->cek_login()) redirect(site_url('index.php/home_controller'));
		else$this->load->view('home/welcome');
	}

}

/* End of file home_controller.php */
/* Location: ./application/controllers/home_controller.php */