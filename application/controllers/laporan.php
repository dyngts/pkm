<?php

class Laporan extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('authentication');
        $this->load->model('antrian_model');
        $this->load->model('laporan_model');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('javascript');
        $this->load->library('calendar');       
        $this->load->library('excel');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->helper('captcha');
		$this->load->model('perawatmod');
		$this->load->model('kasirmod');
		$this->load->model('doktermod');
    }


    function rolechecker(){
    	$role = $this->session->userdata('role');
		$access = 0;
		switch($role) {
			case 'Koordinator Pelayanan': $access = 1;break;
			case 'Koord. Adm. dan Keuangan': $access = 2;break;
			case 'Staf Keuangan': $access = 2;break;
			case 'Perawat Gigi': $access = 1;break;
			case 'Perawat Umum': $access = 1;break;
			case 'Penanggung Jawab':$access = 3; break;
			case 'Apoteker' : $access = 1; break;
			case 'Asisten Apoteker' : $access = 1; break;
		}
        if ($access == 0) {
            show_404();
        }
    }

    public function getHuruf($index) {
		$array = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I');

		return $array[$index];
	}

	public function getBulan($index) {
		$array = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei',
					  '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September','10'=>'Oktober',
					  '11'=>'November', '12'=>'Desember');

		return $array[$index];
	}

    public function getDaftarBulan() {
		$array = array(''=>'-- Pilih Bulan --','01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei',
					  '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September','10'=>'Oktober',
					  '11'=>'November', '12'=>'Desember');

		return $array;
	}

	public function getDaftarTahun() {
		date_default_timezone_set('Asia/Jakarta');
		$year_start = '2013';
		$year_now = date('Y');

		$year = array();

		$year[''] = '-- Pilih Tahun --';

		for($i = $year_start; $i <= $year_now; $i++) {
			$year[$i] = $i;
		}

		return $year;

	}

    public function index() {
        $page = "daftarlaporan";
        if ( ! file_exists('application/views/site/'.$page.'.php')) {
                    show_404();
        }
        
        if (!$this->authentication->is_pelayanan() && 
            !$this->authentication->is_pj() && 
            !$this->authentication->is_kasir() && 
            !$this->authentication->is_perawat() &&
			!$this->authentication->is_adm_keu() &&
			!$this->authentication->is_apoteker()
			) {
            show_error('Anda tidak memiliki hak akses');
        }

        $data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
									   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
									   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
									   ,'laporan_stok_obat' => 'Laporan Stok Obat'
									   );

		$data['kategori'] = array(
			''=>'-- Pilih Kategori Obat --',
			'Obat Umum'=>'Obat Umum',
			'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
			'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
			'Obat Emergency'=>'Obat Emergency');
		
		$data['month'] = $this->getDaftarBulan();
		$data['year'] = $this->getDaftarTahun();
        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
        $data['title'] = "Laporan";
        $data['msg'] = "";
        $this->load->view('templates/header', $data);
        $this->load->model('authentication');
        if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
        else $this->load->view('usermenu/loginform', $data);
        $this->load->view('site/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
	
	function lihat_laporan_kinerja(){
		$this->rolechecker();
		$bulan = $this->input->post('pilihbulan');
		$dokter = $this->perawatmod->getDokter();
		$tahun = $this->input->post('pilihtahun');
		$data['dokter'] = $dokter;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['title'] = 'Lihat Laporan Kinerja';
		$data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
									   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
									   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
									   ,'laporan_stok_obat' => 'Laporan Stok Obat'
									   );

		$data['kategori'] = array(
			''=>'-- Pilih Kategori Obat --',
			'Obat Umum'=>'Obat Umum',
			'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
			'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
			'Obat Emergency'=>'Obat Emergency');
		
		$data['month'] = $this->getDaftarBulan();
		$data['year'] = $this->getDaftarTahun();
        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
		if ($bulan == "" || $tahun =="") {
			$page = "daftarlaporan";
			$data['title'] = "Laporan";
			$data['msg'] = "
				  <div class=\"row\">
					<div class=\"message message-yellow\">
					  <p class=\"p_message\">Masukkan pilihan dengan benar</p>
					</div>
				  </div>";
			$this->load->view('templates/header', $data);
			$this->load->model('authentication');
			if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
		else {
			$this->session->set_userdata('bulan', $bulan);
			$this->session->set_userdata('tahun', $tahun);
			$this->load->view('templates/header', $data);
			$this->load->view('usermenu/usermenu', $data);
			$this->load->view('perawat/laporan_kinerja');
			$this->load->view('templates/footer', $data);
		}
	}	
	
	public function laporan_kesakitan() {
		$this->rolechecker();
		$jenislap = $this->input->post("jenislaporan", true);
		$bulan = $this->input->post("pilihbulan", true);
		$tahun = $this->input->post("pilihtahun", true);
		$data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
									   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
									   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
									   ,'laporan_stok_obat' => 'Laporan Stok Obat'
									   );

		$data['kategori'] = array(
			''=>'-- Pilih Kategori Obat --',
			'Obat Umum'=>'Obat Umum',
			'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
			'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
			'Obat Emergency'=>'Obat Emergency');
		
		$data['month'] = $this->getDaftarBulan();
		$data['year'] = $this->getDaftarTahun();
        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
		if ($bulan == "" || $tahun =="") {
			$page = "daftarlaporan";
			$data['title'] = "Laporan";
			$data['msg'] = "
				  <div class=\"row\">
					<div class=\"message message-yellow\">
					  <p class=\"p_message\">Masukkan pilihan dengan benar</p>
					</div>
				  </div>";
			$this->load->view('templates/header', $data);
			$this->load->model('authentication');
			if($this->authentication->is_loggedin()){
				$this->load->view('usermenu/usermenu', $data);
			}
			else $this->load->view('usermenu/loginform', $data);
			$this->load->view('site/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
		else{
			$this->session->set_userdata('jenislap', $jenislap);
			$this->session->set_userdata('bulan', $bulan);
			$this->session->set_userdata('tahun', $tahun);

			if ($jenislap == 'data-kesakitan') {
				$query = $this->perawatmod->getLaporanPenyakitUmur($bulan, $tahun);
				$query2 = $this->perawatmod->getLaporanPenyakitKasus($bulan, $tahun);
				$datakesakitan = $query->result_array();
				$datakesakitanstatus = $query2->result_array();
				$data['datakesakitan'] = $datakesakitan;
				$data['datakesakitanstatus'] = $datakesakitanstatus;
				$data['title'] = 'Daftar Pasien';
				$data['bulan']= $bulan;
				$data['tahun'] = $tahun;
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('perawat/laporan_data_kesakitan');
				$this->load->view('templates/footer', $data);
			}
			else if ($jenislap == 'pola-penyakit') {
				$laporanPolaPenyakit = $this->perawatmod->getPolaPenyakit($bulan, $tahun);
				$data['laporanPolaPenyakit'] = $laporanPolaPenyakit->result_array();
				$this->session->set_userdata('laporanPolaPenyakit', $laporanPolaPenyakit->result_array());

				$data['bulan'] = $bulan;
				$data['tahun'] = $tahun;
				$data['title'] = 'Daftar Pasien';
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('perawat/laporan_pola_penyakit');
				$this->load->view('templates/footer', $data);
			}
			else if ($jenislap == 'tindakan-poli-gigi') {
				$laporanTindakanGigi = $this->perawatmod->getJenisTindakan($bulan,$tahun);
				$data['laporanTindakanGigi'] = $laporanTindakanGigi->result_array();
				$this->session->set_userdata('laporanTindakanGigi',  $laporanTindakanGigi->result_array());
				$data['bulan'] = $bulan;
				$data['tahun'] = $tahun;
				$data['title'] = 'Daftar Pasien';
				$this->load->view('templates/header', $data);
				$this->load->view('usermenu/usermenu', $data);
				$this->load->view('perawat/laporan_tindakan_poli_gigi');
				$this->load->view('templates/footer', $data);
			}
			else redirect('laporan');
			
		}
		
	}
	
	function countPenyakit($kodediagnosis) {
		$count = $this->perawatmod->getCountPenyakit($kodediagnosis);
		return $count;
	}

	public function laporan_keuangan(){
		$this->rolechecker();
		$jenis = $this->input->post("jenis", true);
		$bulan = $this->input->post("pilihbulan", true);
		$tanggal = $this ->input->post("tanggal", true);
		$jenispoli = $this->input->post('pilihpoli',true);
		$tahun = $this->input->post('pilihtahun',true);
		/*==============Set Userdata buat download pdf =================*/
		$this->session->set_userdata('tanggal', $tanggal);
		$this->session->set_userdata('poli', $jenispoli);
		$this->session->set_userdata('bulan',$bulan);
		$this->session->set_userdata('tahun',$tahun);
		/*==============================================================*/
		$data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
									   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
									   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
									   ,'laporan_stok_obat' => 'Laporan Stok Obat'
									   );

		$data['kategori'] = array(
			''=>'-- Pilih Kategori Obat --',
			'Obat Umum'=>'Obat Umum',
			'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
			'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
			'Obat Emergency'=>'Obat Emergency');
		
		$data['month'] = $this->getDaftarBulan();
		$data['year'] = $this->getDaftarTahun();
        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
			if ($jenis == 'harian') {
				if ($tanggal =="") {
					$page = "daftarlaporan";
					$data['title'] = "Laporan";
					$data['msg'] = "
					  <div class=\"row\">
						<div class=\"message message-yellow\">
						  <p class=\"p_message\">Masukkan pilihan dengan benar</p>
						</div>
					</div>";
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/usermenu', $data);
					$this->load->view('site/'.$page, $data);
					$this->load->view('templates/footer', $data);
				}
				else {
					$laporanharian = $this->kasirmod->getLaporanHarian($tanggal);	
					$data['laporanharian'] = $laporanharian->result_array();
					$data['tanggal'] = $tanggal;
					$data['title'] = 'Laporan Harian PKM UI Depok';
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/usermenu', $data);
					$this->load->view('kasir/harian');
					$this->load->view('templates/footer', $data);
				}
			}
			else if ($jenis == 'bulanan') {
				if ($bulan == "" || $tahun =="") {
					$page = "daftarlaporan";
					$data['title'] = "Laporan";
					$data['msg'] = "
					  <div class=\"row\">
						<div class=\"message message-yellow\">
						  <p class=\"p_message\">Masukkan pilihan dengan benar</p>
						</div>
					</div>";
					$this->load->view('templates/header', $data);
					$this->load->view('usermenu/usermenu', $data);
					$this->load->view('site/'.$page, $data);
					$this->load->view('templates/footer', $data);
				}
				else {
					if($jenispoli == 'Semua') {
						$laporanbulanan = $this->kasirmod->getLaporanBulanan($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['tahun'] = $tahun;
						$data['poli'] = $jenispoli;
						$data['title'] = 'Laporan Bulanan PKM UI Depok';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Umum') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananUmum($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['tahun'] = $tahun;
						$data['poli'] = $jenispoli;
						$data['title'] = 'Laporan Bulanan Poli Umum';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_umum', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Gigi') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananGigi($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['poli'] = $jenispoli;
						$data['title'] = 'Laporan Bulanan Poli Gigi';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_gigi', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Farmasi') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananFarmasi($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['poli'] = $jenispoli;
						$data['title'] = 'Laporan Bulanan Farmasi';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_farmasi', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Lab') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananLab($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['title'] = 'Laporan Bulanan Laboratorium';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_lab', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Radiologi') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananRadio($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['title'] = 'Laporan Bulanan Radiologi';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_radiologi', $data);
						$this->load->view('templates/footer', $data);
					}

					else if($jenispoli == 'Estetika') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananEstetika($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['title'] = 'Laporan Bulanan Estetika';					
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_estetika', $data);
						$this->load->view('templates/footer', $data);
					}

					else if($jenispoli == 'Ambulans') {
						$laporanbulanan = $this->kasirmod->getLaporanBulananAmbulans($bulan, $tahun);
						$data['laporanbulanan'] = $laporanbulanan->result_array();
						$this->session->set_userdata('laporanbulanan', $laporanbulanan->result_array());
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['title'] = 'Laporan Bulanan Ambulans';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_ambulans', $data);
						$this->load->view('templates/footer', $data);
					}
					else if($jenispoli == 'Salemba'){
						$laporan_salemba = $this->kasirmod->get_salemba($bulan, $tahun);
						$data['laporan_salemba'] = $laporan_salemba->result_array();
						$data['bulan'] = $bulan;
						$data['tahun'] = $tahun;
						$data['title'] = 'Laporan Bulanan PKM Salemba';
						$this->load->view('templates/header', $data);
						$this->load->view('usermenu/usermenu', $data);
						$this->load->view('kasir/bulanan_salemba', $data);
						$this->load->view('templates/footer', $data);
					}
				}
			}	
		}
	
    function poli($jenispoli)
    {
		$role = $this->session->userdata('role');
		$access = 0;
		switch($role) {
			case 'Koordinator Pelayanan': $access = 1;break;
			case 'Koord Administrasi&Keuangan': $access = 2;break;
			case 'Penanggung Jawab':$access = 3; break;
		}
        if ($access == 0) {
            show_404();
        }

		$this->rolechecker();
        $valid = false;
        $umum = false;
        $gigi = false;
        $polistr = "";
        $bulan_array = array("","JANUARI","FEBRUARI","MARET","APRIL","MEI","JUNI",
            "JULI","AGUSTUS","SEPTEMBER","OKTOBER","NOVEMBER","DESEMBER");

        if ($jenispoli == "umum") {

            $lokasi = $this->input->post('u_lokasi');
            $bulan = $this->input->post('u_bulan');
            $tahun = $this->input->post('u_tahun');

            if ($bulan != "")
            {
                $bulan = $bulan_array[$this->input->post('u_bulan')];
            }

            if ($lokasi == "" || $bulan == "" || $tahun =="") {
                //do nothing
            }

            else
            {
                $valid = true;
                $umum = true;
                $polistr = "UMUM";
            }
        }
        else if ($jenispoli == "gigi") {

            $lokasi = "";
            $bulan = $this->input->post('g_bulan');
            $tahun = $this->input->post('g_tahun');

            if ($bulan == "" || $tahun =="")
            {
                //do nothing
            }
            else
            {
                $valid = true;
                $gigi = true;
                $polistr = "GIGI";
            }
        }

        if (!$valid)
        {
			
            $page = "daftarlaporan";
            $data['title'] = "Laporan";
            $data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";
            $data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
									   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
									   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
									   ,'laporan_stok_obat' => 'Laporan Stok Obat'
									   );

		$data['kategori'] = array(
			''=>'-- Pilih Kategori Obat --',
			'Obat Umum'=>'Obat Umum',
			'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
			'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
			'Obat Emergency'=>'Obat Emergency');
		
		$data['month'] = $this->getDaftarBulan();
		$data['year'] = $this->getDaftarTahun();
        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
            $this->load->view('templates/header', $data);
            $this->load->model('authentication');
            if($this->authentication->is_loggedin()){
                $this->load->view('usermenu/usermenu', $data);
            }
            else $this->load->view('usermenu/loginform', $data);
            $this->load->view('site/'.$page, $data);
            $this->load->view('templates/footer', $data);
        }
        else 
        {

            if ($umum) {
                //query umum
                //$query = $this->db->get('sitrasi.pasien'/*$table_name*/);
                $query = $this->laporan_model->get_umum($this->input->post('u_bulan'), $this->input->post('u_tahun'));
            }

            else /*$gigi*/ {
                //query gigi
                $query = $this->laporan_model->get_gigi($this->input->post('g_bulan'), $this->input->post('g_tahun'));
            }
            
     
            if(!$query)
                return false;
     
            // Starting the PHPExcel library
            $this->load->library('excel');
     
            $this->excel->getProperties()->setTitle("Laporan Kunjungan Pasien Poli ".$polistr." PKM UI ".$bulan." ".$tahun)->setDescription("none");
     
            $this->excel->setActiveSheetIndex(0);

            // Head
            $title = "LAPORAN KUNJUNGAN PASIEN";
            $subtitle1 = "POLI ".$polistr." PKM UI ".$lokasi;
            $subtitle2 = "BULAN: ".$bulan."    TAHUN: ".$tahun;

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $title);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $subtitle1);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $subtitle2);

            // Field Head
            $start_field = 7;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $start_field, "NO");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $start_field, "FAKULTAS/UNIT KERJA");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $start_field, "MAHASISWA");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $start_field, "DOSEN/KARYAWAN");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $start_field, "TAMU");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $start_field, "JUMLAH");

            $start_field2 = $start_field + 1;
            $col = 2;
            for ($i=0; $i < 3; $i++) { 
                
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, "PAGI");
                $col += 2;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, "SORE");
                $col += 2;
            }

            $start_field3 = $start_field2 + 1;
            $col = 2;
            for ($i=0; $i < 6; $i++) { 
                
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field3, "BARU");
                $col++;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field3, "LAMA");
                $col++;
            }


            // Field names in the first row
            $fields = $query->list_fields();
            // $col = 0;
            // $start_field2 = $start_field + 1;
            // foreach ($fields as $field)
            // {
            //     $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, $field);
            //     $col++;
            // }
     
            // Fetching the table data
            $row = $start_field + 3;
            foreach($query->result() as $data)
            {
                $col = -1;
                ++$col;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row-9);
                foreach ($fields as $field)
                {
                    ++$col;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                }
                
                ++$col;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, '=SUM(C'.$row.':N'.$row.')');

                $row++;
            }

            $col = 0;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "JUMLAH");

            $rowbefore = $row - 1;
            $col = 2;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(C10:C'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(D10:D'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(E10:E'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(F10:F'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(G10:G'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(H10:H'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(I10:I'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(J10:J'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(K10:K'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(L10:L'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(M10:M'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(N10:N'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(O10:O'.$rowbefore.')');
        


            // table merge
            $this->excel->getActiveSheet()->mergeCells('A2:P2');
            $this->excel->getActiveSheet()->mergeCells('A3:P3');
            $this->excel->getActiveSheet()->mergeCells('A4:P4');

            $this->excel->getActiveSheet()->mergeCells('A7:A9');
            $this->excel->getActiveSheet()->mergeCells('B7:B9');
            $this->excel->getActiveSheet()->mergeCells('C7:F7');
            $this->excel->getActiveSheet()->mergeCells('G7:J7');
            $this->excel->getActiveSheet()->mergeCells('K7:N7');

            $this->excel->getActiveSheet()->mergeCells('C8:D8');
            $this->excel->getActiveSheet()->mergeCells('E8:F8');
            $this->excel->getActiveSheet()->mergeCells('G8:H8');
            $this->excel->getActiveSheet()->mergeCells('I8:J8');
            $this->excel->getActiveSheet()->mergeCells('K8:L8');
            $this->excel->getActiveSheet()->mergeCells('M8:N8');

            $this->excel->getActiveSheet()->mergeCells('O7:O9');

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);

            // Styling
            $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
            $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
            $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

            $bodyStyleArray = array(
                'font' => array(
                    'bold' => false,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $headerStyleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'FF000000'),
                    ),
                    'inside' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $outlineStyleArray = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $titleStyleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 14,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
            );

            $this->excel->getActiveSheet()->getStyle('A2:O4')->applyFromArray($titleStyleArray);
            $this->excel->getActiveSheet()->getStyle('A7:O9')->applyFromArray($headerStyleArray);
            $this->excel->getActiveSheet()->getStyle('A10:O'.$row)->applyFromArray($bodyStyleArray);
            $this->excel->getActiveSheet()->getStyle('A7:O'.$row)->applyFromArray($outlineStyleArray);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($outlineStyleArray);
            $this->excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headerStyleArray);

            $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A7:O9')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A1:O9')->getAlignment()->sethorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A7:N9')->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getDefaultColumnDimension()->setWidth(7);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);

            // signature
            $col = 10;
            $row+=2;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "Depok,");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "Koordinator Administrasi dan Keuangan");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+=3, "PKM UI");
            $this->excel->getActiveSheet()->getStyle('K'.$row)->getFont()->setUnderline(true);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "NIP");

            // header and footer
            //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
            $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

            $this->excel->setActiveSheetIndex(0);
     
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
     
            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan_DaftarPasien_'.date('dMy').'.xls"');
            header('Cache-Control: max-age=0');
     				
            ob_clean();
        $objWriter->save('php://output');
        }
    }
	
    function laporanpoli($jenispoli)
    {
		$role = $this->session->userdata('role');
		$access = 0;
		switch($role) {
			case 'Koordinator Pelayanan': $access = 1;break;
			case 'Koord Administrasi&Keuangan': $access = 2;break;
			case 'Penanggung Jawab':$access = 3; break;
		}
        if ($access == 0) {
            show_404();
        }

        $valid = false;
        $umum = false;
        $gigi = false;
        $polistr = "";
        $bulan_array = array("","JANUARI","FEBRUARI","MARET","APRIL","MEI","JUNI",
            "JULI","AGUSTUS","SEPTEMBER","OKTOBER","NOVEMBER","DESEMBER");

        if ($jenispoli == "umum") {

            // $lokasi = $this->input->post('u_lokasi');
            $lokasi = " ";
            $bulan = $this->input->post('u_bulan');
            $tahun = $this->input->post('u_tahun');

            if ($bulan != "")
            {
                $bulan_str = $bulan_array[$this->input->post('u_bulan')];
            }

            if ($lokasi == "" || $bulan == "" || $tahun =="") {
                //do nothing
            }

            else
            {
                $valid = true;
                $umum = true;
                $polistr = "UMUM";
            }
        }
        else if ($jenispoli == "gigi") {

            $lokasi = "";
            $bulan = $this->input->post('g_bulan');
            $tahun = $this->input->post('g_tahun');

            if ($bulan == "" || $tahun =="")
            {
                //do nothing
            }
            else
            {
                $valid = true;
                $gigi = true;
                $polistr = "GIGI";
            }
        }

        if (!$valid)
        {
			
            /*$page = "daftarlaporan";
            $data['title'] = "Laporan";
            $data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";
            $this->load->view('templates/header', $data);
            $this->load->model('authentication');
            if($this->authentication->is_loggedin()){
                $this->load->view('usermenu/usermenu', $data);
            }
            else $this->load->view('usermenu/loginform', $data);
            $this->load->view('site/'.$page, $data);
            $this->load->view('templates/footer', $data);*/

		        $page = "daftarlaporan";
		        if ( ! file_exists('application/views/site/'.$page.'.php')) {
		                    show_404();
		        }
		        
		        if (!$this->authentication->is_pelayanan() && 
		            !$this->authentication->is_pj() && 
		            !$this->authentication->is_kasir() && 
		            !$this->authentication->is_perawat() &&
					!$this->authentication->is_adm_keu()) {
		            show_error('Anda tidak memiliki hak akses');
		        }

		        $data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
											   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
											   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
											   ,'laporan_stok_obat' => 'Laporan Stok Obat'
											   );

				$data['kategori'] = array(
					''=>'-- Pilih Kategori Obat --',
					'Obat Umum'=>'Obat Umum',
					'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
					'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
					'Obat Emergency'=>'Obat Emergency');
				
            $data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";
				$data['month'] = $this->getDaftarBulan();
				$data['year'] = $this->getDaftarTahun();
		        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
		        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
		        $data['title'] = "Laporan";
		        $this->load->view('templates/header', $data);
		        $this->load->model('authentication');
		        if($this->authentication->is_loggedin()){
		            $this->load->view('usermenu/usermenu', $data);
		        }
		        else $this->load->view('usermenu/loginform', $data);
		        $this->load->view('site/'.$page, $data);
		        $this->load->view('templates/footer', $data);
        }
        else 
        {

            if ($umum) {
            	$jenispoli = '1';
            }

            else /*$gigi*/ {
            	$jenispoli = '2';
            }

																								// $jenispoli, $jenispasien, $bulan, $tahun, $jammulai, $jamselesai
            $query = $this->laporan_model->getlaporanpoli($jenispoli, $bulan, $tahun);
            
     
            if(!$query)
                return false;
     
            // Starting the PHPExcel library
            $this->load->library('excel');
     
            $this->excel->getProperties()->setTitle("Laporan Kunjungan Pasien Poli ".$polistr." PKM UI ".$bulan_str." ".$tahun)->setDescription("none");
     
            $this->excel->setActiveSheetIndex(0);

            // Head
            $title = "LAPORAN KUNJUNGAN PASIEN";
            $subtitle1 = "POLI ".$polistr." PKM UI ".$lokasi;
            $subtitle2 = "BULAN: ".$bulan_str."    TAHUN: ".$tahun;

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $title);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, $subtitle1);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, $subtitle2);

            // Field Head
            $start_field = 7;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $start_field, "NO");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $start_field, "FAKULTAS/UNIT KERJA");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $start_field, "MAHASISWA");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $start_field, "DOSEN/KARYAWAN");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $start_field, "TAMU");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $start_field, "JUMLAH");

            $start_field2 = $start_field + 1;
            $col = 2;
            for ($i=0; $i < 3; $i++) { 
                
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, "PAGI");
                $col += 2;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, "SORE");
                $col += 2;
            }

            $start_field3 = $start_field2 + 1;
            $col = 2;
            for ($i=0; $i < 6; $i++) { 
                
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field3, "BARU");
                $col++;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field3, "LAMA");
                $col++;
            }


            // Field names in the first row
            /*$fields = $query->list_fields();*/
            // $col = 0;
            // $start_field2 = $start_field + 1;
            // foreach ($fields as $field)
            // {
            //     $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $start_field2, $field);
            //     $col++;
            // }
     
            // Fetching the table data
            $row = $start_field + 3;
            for ($qq=0; $qq < count($query) ; $qq++)
            {
                $col = -1;
                ++$col;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $row-9);

                ++$col;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $query[$qq]['lembaga']);

                for ($jp=1; $jp <= 3 ; $jp++) { 
	                ++$col;
	                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $query[$qq][$jp]['pagi']['baru']);
	                ++$col;
	                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $query[$qq][$jp]['pagi']['lama']);
	                ++$col;
	                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $query[$qq][$jp]['sore']['baru']);
	                ++$col;
	                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $query[$qq][$jp]['sore']['lama']);
                }
                
                ++$col;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, '=SUM(C'.$row.':N'.$row.')');

                $row++;
            }

            $col = 0;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "JUMLAH");

            $rowbefore = $row - 1;
            $col = 2;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(C10:C'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(D10:D'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(E10:E'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(F10:F'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(G10:G'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(H10:H'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(I10:I'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(J10:J'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(K10:K'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(L10:L'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(M10:M'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(N10:N'.$rowbefore.')');
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, '=SUM(O10:O'.$rowbefore.')');
        


            // table merge
            $this->excel->getActiveSheet()->mergeCells('A2:P2');
            $this->excel->getActiveSheet()->mergeCells('A3:P3');
            $this->excel->getActiveSheet()->mergeCells('A4:P4');

            $this->excel->getActiveSheet()->mergeCells('A7:A9');
            $this->excel->getActiveSheet()->mergeCells('B7:B9');
            $this->excel->getActiveSheet()->mergeCells('C7:F7');
            $this->excel->getActiveSheet()->mergeCells('G7:J7');
            $this->excel->getActiveSheet()->mergeCells('K7:N7');

            $this->excel->getActiveSheet()->mergeCells('C8:D8');
            $this->excel->getActiveSheet()->mergeCells('E8:F8');
            $this->excel->getActiveSheet()->mergeCells('G8:H8');
            $this->excel->getActiveSheet()->mergeCells('I8:J8');
            $this->excel->getActiveSheet()->mergeCells('K8:L8');
            $this->excel->getActiveSheet()->mergeCells('M8:N8');

            $this->excel->getActiveSheet()->mergeCells('O7:O9');

            $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);

            // Styling
            $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->excel->getActiveSheet()->getPageMargins()->setTop(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setRight(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setLeft(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
            $this->excel->getActiveSheet()->getPageMargins()->setHeader(0.5);
            $this->excel->getActiveSheet()->getPageMargins()->setFooter(0.5);
            $this->excel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

            $bodyStyleArray = array(
                'font' => array(
                    'bold' => false,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $headerStyleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'FF000000'),
                    ),
                    'inside' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                    ),
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $outlineStyleArray = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THICK,
                        'color' => array('argb' => 'FF000000'),
                    ),
                ),
            );

            $titleStyleArray = array(
                'font' => array(
                    'bold' => true,
                    'size' => 14,
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
            );

            $this->excel->getActiveSheet()->getStyle('A2:O4')->applyFromArray($titleStyleArray);
            $this->excel->getActiveSheet()->getStyle('A7:O9')->applyFromArray($headerStyleArray);
            $this->excel->getActiveSheet()->getStyle('A10:O'.$row)->applyFromArray($bodyStyleArray);
            $this->excel->getActiveSheet()->getStyle('A7:O'.$row)->applyFromArray($outlineStyleArray);
            $this->excel->getActiveSheet()->getStyle('A'.$row.':O'.$row)->applyFromArray($outlineStyleArray);
            $this->excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($headerStyleArray);

            $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

            $this->excel->getActiveSheet()->getStyle('A7:O9')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A1:O9')->getAlignment()->sethorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A7:N9')->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getDefaultColumnDimension()->setWidth(7);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);

            // signature
            $col = 10;
            $row+=2;
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "Depok,");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "Koordinator Administrasi dan Keuangan");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+=3, "PKM UI");
            $this->excel->getActiveSheet()->getStyle('K'.$row)->getFont()->setUnderline(true);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row++, "");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, "NIP");

            // header and footer
            //$this->excel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
            $this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

            $this->excel->setActiveSheetIndex(0);
     
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
     
            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan_DaftarPasien_'.date('dMy').'.xls"');
            header('Cache-Control: max-age=0');
     
            ob_clean();
        		$objWriter->save('php://output');
        }
    }


 
    function umur1()
    {
        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'mPDF5.4';
        //$rendererLibraryPath = dirname(__FILE__).'/../../../libraries/PDF/' . $rendererLibrary;
        $rendererLibraryPath = dirname(__FILE__).'/../third_party/PHPExcel/'.$rendererLibrary;
        if (!PHPExcel_Settings::setPdfRenderer(
                $rendererName,
                $rendererLibraryPath
            )) {
            die(
                'Please set the $rendererName and $rendererLibraryPath values' .
                PHP_EOL .
                ' as appropriate for your directory structure'.$rendererLibraryPath
            );
        }

        $objPHPexcel = PHPExcel_IOFactory::load('template/laporan_umur.xls');
        $objPHPexcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_PDF($objPHPexcel);

        // Sending headers to force the user to download the file
        //header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_Umur_'.date('dMy').'.pdf"');

        ob_clean();
        $objWriter->save('php://output');
        //$objWriter->save("05featuredemo.pdf");

    }
 
    function umur()
    {

        if (!$this->authentication->is_pelayanan()&&!$this->authentication->is_pj()) {
            show_404();
        }

        $valid = false;

        $bulan_array = array("","JANUARI","FEBRUARI","MARET","APRIL","MEI","JUNI",
            "JULI","AGUSTUS","SEPTEMBER","OKTOBER","NOVEMBER","DESEMBER");

        $bulan = $this->input->post('a_bulan');
        $tahun = $this->input->post('a_tahun');

        if ($bulan != "")
        {
            $bulan_str = $bulan_array[$bulan];
        }

        if ($bulan == "" || $tahun == "") {
            //do nothing
        }

        else
        {
            $valid = true;
            $umum = true;
        }

        if (!$valid)
        {

		        $page = "daftarlaporan";
		        if ( ! file_exists('application/views/site/'.$page.'.php')) {
		                    show_404();
		        }
		        
		        if (!$this->authentication->is_pelayanan() && 
		            !$this->authentication->is_pj() && 
		            !$this->authentication->is_kasir() && 
		            !$this->authentication->is_perawat() &&
					!$this->authentication->is_adm_keu() &&
					!$this->authentication->is_apoteker()) {
		            show_error('Anda tidak memiliki hak akses');
		        }

		        $data['jenis_laporan'] = array( '' => '-- Pilih Jenis Laporan --'
											   ,'laporan_pakai'=>'Laporan Pemakaian Obat'
											   , 'laporan_keluar' => 'Laporan Pengeluaran Obat'
											   ,'laporan_stok_obat' => 'Laporan Stok Obat'
											   );

				$data['kategori'] = array(
					''=>'-- Pilih Kategori Obat --',
					'Obat Umum'=>'Obat Umum',
					'Obat Alkes dan BMHP Gigi'=>'Obat, Alkes, dan BMHP Gigi',
					'Alkes dan BMHP Umum'=>'Alkes dan BMHP Umum',
					'Obat Emergency'=>'Obat Emergency');
				
            $data['msg'] = "
                  <div class=\"row\">
                    <div class=\"message message-yellow\">
                      <p class=\"p_message\">Masukkan pilihan dengan benar</p>
                    </div>
                  </div>";
				$data['month'] = $this->getDaftarBulan();
				$data['year'] = $this->getDaftarTahun();
		        $data['firstyear'] = (int)$this->laporan_model->get_first_year();
		        $data['lastyear'] = (int)$this->laporan_model->get_last_year();
		        $data['title'] = "Laporan";
		        $this->load->view('templates/header', $data);
		        $this->load->model('authentication');
		        if($this->authentication->is_loggedin()){
		            $this->load->view('usermenu/usermenu', $data);
		        }
		        else $this->load->view('usermenu/loginform', $data);
		        $this->load->view('site/'.$page, $data);
		        $this->load->view('templates/footer', $data);
        }

        else 
        {
            //query umur
            $query = $this->laporan_model->get_umur($bulan,$tahun);
     
            if(!$query)
                return false;
     
            // Starting the PHPExcel library
            $this->load->library('excel');
     
            $this->excel->getProperties()->setTitle("Laporan Kunjungan Pasien PKM UI Berdasarkan Umur ".$bulan_str." ".$tahun)->setDescription("none");
     
            $baseurl = base_url();
            $objPHPexcel = PHPExcel_IOFactory::load('template/laporan_umur.xls');
            $objPHPexcel->setActiveSheetIndex(0);

            // Head
            $subtitle = "BULAN: ".$bulan_str."    TAHUN: ".$tahun;

            $objPHPexcel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $subtitle);
     
            // Fetching the table data
            $col = 3;
            $row = 9;


            foreach($query as $data)
            {

              $objPHPexcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data[0]);
              $objPHPexcel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $data[1]);

              $row += 2;
            }

            // Styling
            $objPHPexcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
            $objPHPexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setTop(0.5);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setRight(0.5);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setLeft(0.5);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setBottom(0.75);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setHeader(0.5);
            $objPHPexcel->getActiveSheet()->getPageMargins()->setFooter(0.5);
            $objPHPexcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

            // header and footer
            //$objPHPexcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HPlease treat this document as confidential!');
            $objPHPexcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L' . $objPHPexcel->getProperties()->getTitle() . '&RPage &P of &N');

            $objPHPexcel->setActiveSheetIndex(0);
     
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');
            
            // Sending headers to force the user to download the file
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Laporan_Umur_'.date('dMy').'.xls"');
            header('Cache-Control: max-age=0');
     
            ob_clean();
        $objWriter->save('php://output');
        }
    }
}
