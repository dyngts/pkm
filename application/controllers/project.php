<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Class untuk keperluan statistik
 */
class project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
    $page = "project";

		if ( ! file_exists('application/views/site/'.$page.'.php')) {
			show_404();
		}

		$data['title'] = "TENTANG PROYEK"; 
		$this->load->view('templates/header', $data);
		$this->load->view('site/'.$page, $data);
		$this->load->view('templates/footer');
	}
}

/* End of file  */
/* Location: ./application/controllers/ */
