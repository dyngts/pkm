<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('model_laporan');
	}

	public function proses_laporan() {

		if($_POST) {
			$month = $this->security->xss_clean($this->input->post('month'));
			$year = $this->security->xss_clean($this->input->post('year'));
			$jenis_laporan = $this->security->xss_clean($this->input->post('jenis_laporan'));

			if($jenis_laporan == 'laporan_pakai') {
				$this->laporan_pakai_obat($month, $year);
			}
			else if($jenis_laporan == 'laporan_keluar') {
				$this->laporan_pengeluaran_obat($month, $year);
			}
			else if($jenis_laporan == 'laporan_stok_obat') {
				$kategori = $this->security->xss_clean($this->input->post('kategori'));
				$this->laporan_stok_obat($kategori, $month, $year);
			}
		}

	}

	public function laporan_pakai_obat($bulan = '' , $tahun = '') {
		$this->load->library('excel');
		//$this->load->library('PHPExcel/iofactory');

		$month = $bulan;
		$year = $tahun;

		$judul1 = 'LAPORAN PEMAKAIAN OBAT,ALKES, BMHP TERBANYAK';
		$judul2 = 'PUSAT KESEHATAN MAHASISWA';
		$judul3 = 'UNIVERSITAS INDONESIA';
		$judul4 = 'BULAN : '. $this->getBulan($month).' '.$year;

		$this->excel->setActiveSheetIndex(0);

		for($k = 'B'; $k != 'J'; $k++) {
			$this->excel->getActiveSheet()->getColumnDimension($k)->setWidth(25);
		}
		

		// bagian judul
		$this->excel->getActiveSheet()->setCellValue('A2', $judul1);
		$this->excel->getActiveSheet()->setCellValue('A3', $judul2);
		$this->excel->getActiveSheet()->setCellValue('A4', $judul3);
		$this->excel->getActiveSheet()->setCellValue('A5', $judul4);
		$this->excel->getActiveSheet()->mergeCells('A2:I2');
		$this->excel->getActiveSheet()->mergeCells('A3:I3');
		$this->excel->getActiveSheet()->mergeCells('A4:I4');
		$this->excel->getActiveSheet()->mergeCells('A5:I5');
		$this->excel->getActiveSheet()->getStyle('A2:N19')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// Bagian table header
		$this->excel->getActiveSheet()->getStyle('A7:A8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		$this->excel->getActiveSheet()->mergeCells('A7:A8')->setCellValue('A7', 'No');
		$this->excel->getActiveSheet()->getStyle('A2:I8')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('B7:I7');
		$this->excel->getActiveSheet()->setCellValue('B7', 'Klasifikasi Pemakaian');
		$this->excel->getActiveSheet()->getStyle('B7:I7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		
		$this->excel->getActiveSheet()->setCellValue('B8', 'Obat Umum');
		$this->excel->getActiveSheet()->setCellValue('C8', 'Jumlah Pemakaian');
		$this->excel->getActiveSheet()->setCellValue('D8', 'Obat Emergency');
		$this->excel->getActiveSheet()->setCellValue('E8', 'Jumlah Pemakaian');
		$this->excel->getActiveSheet()->setCellValue('F8', 'Obat/Alkes/BMHP Gigi');
		$this->excel->getActiveSheet()->setCellValue('G8', 'Jumlah Pemakaian');
		$this->excel->getActiveSheet()->setCellValue('H8', 'Alkes & BMHP Umum');
		$this->excel->getActiveSheet()->setCellValue('I8', 'Jumlah Pemakaian');

		$mulai = 1;
		for($k = 9; $k <= 18; $k++) {
			$temp = 'A'.$k;
			$this->excel->getActiveSheet()->setCellValue($temp, $mulai++);
		}


		$this->excel->getActiveSheet()->setCellValue('H24', 'Depok,');
		$this->excel->getActiveSheet()->setCellValue('H25', 'Koordinator,');
		$this->excel->getActiveSheet()->setCellValue('H28', 'Nama');
		$this->excel->getActiveSheet()->setCellValue('H29', 'NIP/NUP');




		$condition = array(0=>'Obat Umum'
			, 1=>'Obat, Alkes, dan BMHP Gigi'
			,2=>'Alkes dan BMHP Umum'
			,3=>'Obat Emergency');

		// ambil pemakaian "Obat Umum"
		$obat_umum = $this->model_laporan->get_pemakaian_obat($condition[0], $month, $year);
		$start = 9;
		foreach ($obat_umum as $obat) {
			$this->excel->getActiveSheet()->setCellValue('B'.$start, $obat->nama_obat);
			$this->excel->getActiveSheet()->setCellValue('C'.$start, $obat->jumlah);
			$start++;
		}


		// ambil pemakaian "Obat, Alkes, dan BMHP Gigi"
		$obat_gigi = $this->model_laporan->get_pemakaian_obat($condition[1] , $month, $year);
		$start = 9;
		foreach ($obat_gigi as $obat) {
			$this->excel->getActiveSheet()->setCellValue('D'.$start, $obat->nama_obat);
			$this->excel->getActiveSheet()->setCellValue('E'.$start, $obat->jumlah);
			$start++;
		}

		// ambil pemakaian "Alkes dan BMHP Umum"
		$obat_alkes = $this->model_laporan->get_pemakaian_obat($condition[2] , $month, $year);
		$start = 9;
		foreach ($obat_alkes as $obat) {
			$this->excel->getActiveSheet()->setCellValue('F'.$start, $obat->nama_obat);
			$this->excel->getActiveSheet()->setCellValue('G'.$start, $obat->jumlah);
			$start++;
		}

		// ambil pemakaian "Obat Emergency"
		$obat_emergency = $this->model_laporan->get_pemakaian_obat($condition[3] , $month, $year);
		$start = 9;
		foreach ($obat_emergency as $obat) {
			$this->excel->getActiveSheet()->setCellValue('H'.$start, $obat->nama_obat);
			$this->excel->getActiveSheet()->setCellValue('I'.$start, $obat->jumlah);
			$start++;
		}

		$filename='laporan_pemakaian_obat.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD

		ob_clean();
		$objWriter->save('php://output');

	}

	public function laporan_pengeluaran_obat($bulan = '' , $tahun = '') {
		$this->load->library('excel');

		$month = $bulan;
		$year = $tahun;

		$judul1 = 'LAPORAN PENGELUARAN OBAT';
		$judul2 = 'PUSAT KESEHATAN MAHASISWA';
		$judul3 = 'UNIVERSITAS INDONESIA';
		$judul4 = 'BULAN : '.$this->getBulan($bulan).' '.$tahun ;

		$this->excel->setActiveSheetIndex(0);

		for($k = 'A'; $k != 'F'; $k++) {
			$this->excel->getActiveSheet()->getColumnDimension($k)->setWidth(25);
		}
		
		// bagian judul
		$this->excel->getActiveSheet()->setCellValue('A2', $judul1);
		$this->excel->getActiveSheet()->setCellValue('A3', $judul2);
		$this->excel->getActiveSheet()->setCellValue('A4', $judul3);
		$this->excel->getActiveSheet()->setCellValue('A5', $judul4);
		$this->excel->getActiveSheet()->mergeCells('A2:E2');
		$this->excel->getActiveSheet()->mergeCells('A3:E3');
		$this->excel->getActiveSheet()->mergeCells('A4:E4');
		$this->excel->getActiveSheet()->mergeCells('A5:E5');
		$this->excel->getActiveSheet()->getStyle('A2:E19')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A2:E8')->getFont()->setBold(true);

		$this->excel->getActiveSheet()->setCellValue('A7', 'NO');
		$this->excel->getActiveSheet()->mergeCells('A7:A8');

		$this->excel->getActiveSheet()->setCellValue('B7', 'NAMA OBAT');
		$this->excel->getActiveSheet()->mergeCells('B7:B8');

		$this->excel->getActiveSheet()->getStyle('A7:B8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$this->excel->getActiveSheet()->setCellValue('C7', 'JENIS PENGELUARAN');
		$this->excel->getActiveSheet()->mergeCells('C7:E7');

		$jenis = array('C'=>'RESEP PASIEN', 'D'=>'STOK PKM UI SALEMBA', 'E'=>'KEGIATAN UI');

		for($i = 'C'; $i != 'F'; $i++) {
			$this->excel->getActiveSheet()->setCellValue($i.'8', $jenis[$i]);
		}


		// ambil daftar obat

		$obats = $this->model_laporan->get_pengeluaran_obat($month, $year);

		
		$mulai = 9;
		$nomor = 1;
		foreach ($obats as $obat) {
			$this->excel->getActiveSheet()->setCellValue('A'.$mulai, $nomor);
			$this->excel->getActiveSheet()->setCellValue('B'.$mulai, $obat->nama_obat);
			$this->excel->getActiveSheet()->setCellValue('C'.$mulai, $obat->resep);
			$this->excel->getActiveSheet()->setCellValue('D'.$mulai, $obat->salemba);
			$this->excel->getActiveSheet()->setCellValue('E'.$mulai, $obat->event);
			$nomor++;
			$mulai++;
		}


		$filename='laporan_pengeluaran_obat.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD

		ob_clean();
		$objWriter->save('php://output');

	}

	public function laporan_stok_obat($kategori, $bulan, $tahun) {
		$this->load->library('excel');

		$category = $kategori;
		$month = $bulan;
		$year = $tahun;

		$juduls = array('Obat Umum'=>'LAPORAN STOK OBAT UMUM'
			, 'Obat, Alkes, dan BMHP Gigi'=>'LAPORAN STOK OBAT GIGI, ALKES GIGI & BAHAN MEDIS GIGI HABIS PAKAI'
			, 'Alkes dan BMHP Umum'=>'LAPORAN STOK ALAT KESEHATAN & BAHAN MEDIS UMUM HABIS PAKAI'
			, 'Obat Emergency'=>'LAPORAN STOK OBAT EMERGENCY'
			);

		$judul1 = $juduls[$category];
		$judul2 = 'PUSAT KESEHATAN MAHASISWA';
		$judul3 = 'UNIVERSITAS INDONESIA';
		$judul4 = 'BULAN : '.$this->getBulan($bulan).' '.$tahun ;

		$this->excel->setActiveSheetIndex(0);

		for($k = 'A'; $k != 'I'; $k++) {
			$this->excel->getActiveSheet()->getColumnDimension($k)->setWidth(25);
		}

		$this->excel->getActiveSheet()->setCellValue('A2', $judul1);
		$this->excel->getActiveSheet()->setCellValue('A3', $judul2);
		$this->excel->getActiveSheet()->setCellValue('A4', $judul3);
		$this->excel->getActiveSheet()->setCellValue('A5', $judul4);


		$stok_depok = $this->model_laporan->get_stok_pemakaian_sisa_by_category($category, $month, $year);
		$delivery_stock = $this->model_laporan->get_delivered_stok_this_month_by_category($category, $month, $year);
		$stok_salemba = array();

		$nama_koloms = array('A'=>'No', 'B'=>'Nama Obat', 'C'=>'Sediaan', 'D'=>'Jumlah Stok', 'E'=>'Jumlah Obat Datang','F'=>'Jumlah Pemakaian','G'=>'Jumlah Sisa','F7'=>'Jumlah Pemakaian','F8'=>'PKM UI Depok', 'G8'=>'PKM UI Salemba','H7'=>'Jumlah Sisa');

        // Mengambil sediaan dan jumlah stok dari nama obat tertentu
		foreach ($stok_depok as $obat) {
			$datas[$obat->nama]['sediaan'] = $obat->sediaan;
			$datas[$obat->nama]['jumlah_stok'] = $obat->jumlah_stok;
			$datas[$obat->nama]['jumlah_depok'] = $obat->jumlah_pemakaian;
			$datas[$obat->nama]['jumlah_sisa'] = $obat->jumlah_sisa;
		}

		foreach ($delivery_stock as $obat) {
			$datas[$obat->nama]['jumlah_obat_datang'] = $obat->jumlah_penerimaan_stok_category;
		}



		$mulai = 10;
		$nomor = 1;


		if($category != 'Obat, Alkes, dan BMHP Gigi') {
            $stok_salemba = $this->model_laporan->get_pemakaian_salemba_by_category($category, $month, $year);

			for($i = 'A'; $i != 'F'; $i++) {
				$this->excel->getActiveSheet()->setCellValue($i.'7', $nama_koloms[$i]);
				$this->excel->getActiveSheet()->mergeCells($i.'7:'.$i.'8');
			}

			$this->excel->getActiveSheet()->setCellValue('F7', $nama_koloms['F7']);
			$this->excel->getActiveSheet()->mergeCells('F7:G7');

			$this->excel->getActiveSheet()->setCellValue('F8', $nama_koloms['F8']);
			$this->excel->getActiveSheet()->setCellValue('G8', $nama_koloms['G8']);

			$this->excel->getActiveSheet()->setCellValue('H7', $nama_koloms['H7']);
			$this->excel->getActiveSheet()->mergeCells('H7:H8');

			$this->excel->getActiveSheet()->getStyle('A2:H8')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('A7:H8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

			$this->excel->getActiveSheet()->mergeCells('A2:H2');
			$this->excel->getActiveSheet()->mergeCells('A3:H3');
			$this->excel->getActiveSheet()->mergeCells('A4:H4');
			$this->excel->getActiveSheet()->mergeCells('A5:H5');

            // Mengambil jumlah pemakaian di PKM UI Salemba
			foreach ($stok_salemba as $obat) {
				$datas[$obat->nama]['jumlah_salemba'] = $obat->jumlah_pemakaian_salemba_category;
			}

			foreach ($datas as $nama_obat => $value) {

                // isi nomor
				$this->excel->getActiveSheet()->setCellValue('A'.$mulai, $nomor);

                // isi nama
				$this->excel->getActiveSheet()->setCellValue('B'.$mulai, $nama_obat);

                // isi sediaan
				$this->excel->getActiveSheet()->setCellValue('C'.$mulai, $datas[$nama_obat]['sediaan']);

                // isi jumlah stok
				if(isset($datas[$nama_obat]['jumlah_stok'])) {
					$this->excel->getActiveSheet()->setCellValue('D'.$mulai, $datas[$nama_obat]['jumlah_stok']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('D'.$mulai, '0');
				}

                // isi jumlah obat datang
				if(isset($datas[$nama_obat]['jumlah_obat_datang'])) {
					$this->excel->getActiveSheet()->setCellValue('E'.$mulai, $datas[$nama_obat]['jumlah_obat_datang']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('E'.$mulai, '0');
				}

                // isi jumlah pemakaian di PKM UI Depok
				if(isset($datas[$nama_obat]['jumlah_depok'])) {
					$this->excel->getActiveSheet()->setCellValue('F'.$mulai, $datas[$nama_obat]['jumlah_depok']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('F'.$mulai, $datas[$nama_obat]['jumlah_depok']);
				}

                // isi jumlah pemakaian di PKM UI Salemba
				if(isset($datas[$nama_obat]['jumlah_salemba'])) {
					$this->excel->getActiveSheet()->setCellValue('G'.$mulai, $datas[$nama_obat]['jumlah_salemba']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('G'.$mulai, '0');
				}

                // isi jumlah sisa
				if(isset($datas[$nama_obat]['jumlah_sisa'])) {
					$this->excel->getActiveSheet()->setCellValue('H'.$mulai, $datas[$nama_obat]['jumlah_sisa']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('H'.$mulai, $datas[$nama_obat]['jumlah_sisa']);
				}

				$mulai++;
				$nomor++;
			}

			$this->excel->getActiveSheet()->getStyle('A2:H'.$mulai)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		}
		else {
			for($i = 'A'; $i != 'H'; $i++) {
				$this->excel->getActiveSheet()->setCellValue($i.'7', $nama_koloms[$i]);
				$this->excel->getActiveSheet()->getStyle('A2:H7')->getFont()->setBold(true);
			}

			$this->excel->getActiveSheet()->mergeCells('A2:G2');
			$this->excel->getActiveSheet()->mergeCells('A3:G3');
			$this->excel->getActiveSheet()->mergeCells('A4:G4');
			$this->excel->getActiveSheet()->mergeCells('A5:G5');

			foreach ($datas as $nama_obat => $value) {

                // isi nomor
				$this->excel->getActiveSheet()->setCellValue('A'.$mulai, $nomor);

                // isi nama
				$this->excel->getActiveSheet()->setCellValue('B'.$mulai, $nama_obat);

                // isi sediaan
				$this->excel->getActiveSheet()->setCellValue('C'.$mulai, $datas[$nama_obat]['sediaan']);

                // isi jumlah stok
				if(isset($datas[$nama_obat]['jumlah_stok'])) {
					$this->excel->getActiveSheet()->setCellValue('D'.$mulai, $datas[$nama_obat]['jumlah_stok']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('D'.$mulai, '0');
				}

                // isi jumlah obat datang
				if(isset($datas[$nama_obat]['jumlah_obat_datang'])) {
					$this->excel->getActiveSheet()->setCellValue('E'.$mulai, $datas[$nama_obat]['jumlah_obat_datang']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('E'.$mulai, '0');
				}

                // isi jumlah pemakaian di PKM UI Depok
				if(isset($datas[$nama_obat]['jumlah_depok'])) {
					$this->excel->getActiveSheet()->setCellValue('F'.$mulai, $datas[$nama_obat]['jumlah_depok']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('F'.$mulai, '0');
				}

                // isi jumlah sisa
				if(isset($datas[$nama_obat]['jumlah_sisa'])) {
					$this->excel->getActiveSheet()->setCellValue('G'.$mulai, $datas[$nama_obat]['jumlah_sisa']);
				}
				else {
					$this->excel->getActiveSheet()->setCellValue('G'.$mulai, '0');
				}

				$mulai++;
				$nomor++;
			}

			$this->excel->getActiveSheet()->getStyle('A2:G'.$mulai)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		$mulai += 2;

		$this->excel->getActiveSheet()->setCellValue('F'.$mulai++, 'Depok,');
		$this->excel->getActiveSheet()->setCellValue('F'.$mulai++, 'Koordinator,');

		$mulai += 2;

		$this->excel->getActiveSheet()->setCellValue('F'.$mulai++, 'Nama');
		$this->excel->getActiveSheet()->setCellValue('F'.$mulai++, 'NIP/NUP');


                $filename='laporan_stok_obat.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD

                ob_clean();
                $objWriter->save('php://output');
            }

            public function getHuruf($index) {
            	$array = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I');

            	return $array[$index];
            }

            public function getBulan($index) {
            	$array = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei',
            		'06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September','10'=>'Oktober',
            		'11'=>'November', '12'=>'Desember');

            	return $array[$index];
            }

            public function getDaftarBulan() {
            	$array = array(''=>'-- Pilih Bulan --','01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei',
            		'06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September','10'=>'Oktober',
            		'11'=>'November', '12'=>'Desember');

            	return $array;
            }

            public function getDaftarTahun() {
            	date_default_timezone_set('Asia/Jakarta');
            	$year_start = '2013';
            	$year_now = date('Y');

            	$year = array();

            	$year[''] = '-- Pilih Tahun --';

            	for($i = $year_start; $i <= $year_now; $i++) {
            		$year[$i] = $i;
            	}

            	return $year;

            }

        }

        /* End of file laporan_controller.php */
/* Location: ./application/controllers/laporan_controller.php */