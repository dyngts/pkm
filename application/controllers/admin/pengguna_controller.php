<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna_controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_model('pengguna_model');
	}

	public function tambah() {
		if(!$this->cek_login() || $this->session->userdata('flag_admin') != TRUE) redirect(site_url('index.php/home_controller'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|callback__cek_ada_username');

		if($this->input->post('batal') == "Batal") {
			redirect(site_url('index.php/admin/pengguna_controller/kumpulan'));
		}
		

		if($this->form_validation->run() == TRUE) {

			if($_POST) {
				$data = array();
				$data['username'] = $this->input->post('username', TRUE);
				//$data['flag_admin'] = ($this->input->post('peran', TRUE) == 'apoteker') ? 0 : 1;
				
				$this->pengguna_model->_tambah_pengguna($data);

				redirect(site_url('admin/pengguna_controller/kumpulan'));
			}
		}
		$data['nama_aktif'] = 'pengguna';
		$this->cetak_halaman('pengguna/_tambahkan_pengguna', $data);
	}

	public function _cek_ada_username($str) {
		if(!$this->cek_login() || $this->session->userdata('flag_admin') != TRUE) redirect(site_url('index.php/home_controller'));
		$username = $this->input->post('username');

		$cek = $this->pengguna_model->_cek_pengguna($username);

			if($cek) {
				$cek_username = $this->pengguna_model->_cek_ada_pengguna($username);
				if($cek_username) {
					$this->form_validation->set_message('_cek_ada_username', '%s sudah ditambahkan ke dalam sistem');
					return FALSE;
				}
				return TRUE;
			}
			else {
				$this->form_validation->set_message('_cek_ada_username', '%s tidak terdaftar di dalam sistem registrasi');
				return FALSE;
			}
		return TRUE;
	}


	public function _cek_ubah_username($str) {
		if(!$this->cek_login() || $this->session->userdata('flag_admin') != TRUE) redirect(site_url('index.php/home_controller'));
		$username_baru = $this->input->post('username');
		$username_lama = $this->uri->segment(4);
		
		$cek = $this->pengguna_model->_cek_ada_pengguna($username_baru);

			if($username_baru != $username_lama && $cek) {
				$this->form_validation->set_message('_cek_username', '%s sudah terpakai, silakan cari username yang lain :)');
			}
			else if($username_baru == $username_lama) {
				return TRUE;
			}
		return TRUE;
	}

	public function ubah($username) {
		if(!$this->cek_login() || $this->session->userdata('flag_admin') != TRUE) redirect(site_url('index.php/home_controller'));
		$this->load->library('form_validation');
		//$username = $this->uri->segment(3);
		$this->form_validation->set_rules('username', 'Username', 'trim|required|callback__cek_ubah_username');
		$this->form_validation->set_rules('nip', 'NIP', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[konfirmasi]|callback__cek_password');
		$this->form_validation->set_rules('konfirmasi', 'Password Confirmation', 'trim|matches[password]');

		

		if($this->form_validation->run() == TRUE) {
			if($_POST) {
				$pengguna = $this->pengguna_model->_ambil_pengguna($username);
				$data = array();
				$data['username'] = $this->input->post('username');
				$data['password'] = ($this->input->post('password') == '') ? $pengguna->password : sha1($this->input->post('password'));
				$data['nip'] = $this->input->post('nip');
				$data['flag_admin'] = ($this->input->post('peran', TRUE) == 'apoteker') ? 0 : 1;
				$this->pengguna_model->_ubah_pengguna($data, $username);

				redirect(site_url('admin/pengguna_controller/kumpulan'));
			}		
		}
		
		$data['pengguna'] = $this->pengguna_model->_ambil_pengguna($username);
		
		$data['flag_admins'] = array(
			'kepala'=>'Kepala Apoteker',
			'apoteker'=>'Apoteker');
		$data['nama_aktif'] = 'pengguna';
		$this->cetak_halaman('pengguna/_ubah_pengguna', $data);
	}

	public function kumpulan($mulai = 0) {
		if(!$this->cek_login() || $this->session->userdata('flag_admin') != TRUE) redirect(site_url('index.php/home_controller'));
		$data = array();

		$hasil = $this->pengguna_model->_ambil_semua_pengguna($mulai);

		$data['apotekers'] = $hasil['apotekers'];
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/admin/pengguna_controller/kumpulan';
		$config['total_rows'] = $hasil['jumlah_baris'];
		$config['uri_segment'] = 4;
    	$this->pagination->initialize($config);
    	$data['pagination_links'] = $this->pagination->create_links();
    	$data['mulai'] = $mulai;
    	$data['nama_aktif'] = 'pengguna';		
		$this->cetak_halaman('pengguna/_daftar_pengguna', $data);
	}

}

/* End of file pengguna_controller.php */
/* Location: ./application/controllers/pengguna_controller.php */