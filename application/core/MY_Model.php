<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$s = SCHEMA;
		$this->t_obat = $s.'obat';
		$this->t_log_obat_keluar = $s.'log_obat_keluar';
		$this->t_pengeluaran_obat = $s.'pengeluaran_obat';
		$this->t_stok_obat = $s.'stok_obat';
		$this->t_pasien = $s.'pasien';
		$this->t_rekam_medis = $s.'rekam_medis';
		$this->t_resep = $s.'resep';
		$this->t_detail_resep = $s.'detail_resep';
		$this->t_log_update_obat = $s.'log_update_obat';
		$this->t_penerimaan_obat = $s.'penerimaan_obat';
		$this->t_pengeluaran_obat = $s.'pengeluaran_obat';
		$this->t_log_obat_masuk  = $s.'log_obat_masuk';
		$this->t_supplier  = $s.'supplier';
		$this->t_pasien = $s.'pasien';
		$this->t_transaksi_obat_salemba = $s.'transaksi_obat_salemba';
		$this->t_detail_transaksi_salemba = $s.'detail_transaksi_salemba';
		$this->t_stok_awal_bulan = $s.'stok_awal_bulan';

	}

	public function index()
	{
		
	}

	public function _cek_ada_atribut($where, $nama_tabel) {

		$this->db->select()->from($nama_tabel)->where($where);
		$query = $this->db->get();
		$count = $query->num_rows();

		if($count > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function searchterm_handler($field,$searchterm)
	{
		if($searchterm)
		{
			$this->session->set_userdata($field, $searchterm);
			return $searchterm;
		}
		elseif($this->session->userdata($field))
		{
			$searchterm = $this->session->userdata($field);
			return $searchterm;
		}
		else
		{
			$searchterm ="";
			return $searchterm;
		}
	}

	public function _create($atribut, $data, $nama_tabel, $id) {
		return $this->db->query("INSERT INTO ".SCHEMA.$nama_tabel." $atribut VALUES $data RETURNING $id ");
	}

	public function _tambah($data, $nama_tabel) {
		$this->db->set($data);
		$this->db->insert($nama_tabel);
	}

	public function _ubah($where, $data, $nama_tabel) {
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($nama_tabel);
	}

	public function _hapus($where, $nama_tabel) {

		$this->db->where($where);
		$this->db->delete($nama_tabel);
	}

	public function _ambil_semua_baris($nama_tabel, $limit = 10, $mulai = 0) {

		$this->db->select('*')->from($nama_tabel)->limit($limit, $mulai);
		return $this->db->get();
	}

	public function _ambil_baris_tertentu($where, $nama_tabel,  $method, $atribut = '*') {
		$this->db->select($atribut)->from($nama_tabel)->where($where);

		if($method == 'row') {
			return $this->db->get()->row();
		}
		else if($method == 'result') {
			return $this->db->get()->result();
		}
	}

	public function _hitung_jumlah_data($nama_tabel) {
		$query = $this->db->get($nama_tabel)->num_rows();
		return $query;
	}

	protected function insert($data) 
	{
		$data['query_type'] = "C";
		return $this->run_query($data);
	}

	protected function update($data) 
	{
		$data['query_type'] = "U";
		return $this->run_query($data);
	}

	protected function delete($data) 
	{
		$data['query_type'] = "D";
		return $this->run_query($data);
	}

	protected function get($data)
	{
		return $this->get_data($data);
	}

	protected function get_data($data)
	{
		$data['single'] = true;
		return $this->get_datas($data);
	}

	protected function get_datas($data) 
	{
		$data['query_type'] = "R";
		return $this->run_query($data);
	}

	private function run_query($data)
	{
		if(!isset($data['base_table'])) return FALSE;

		if(isset($data['data_in']))
		{
			$this->db->set($data['data_in']);
		}

		if(isset($data['select']))
		{
			$this->db->select($data['select']);
		}

		if(isset($data['join']))
		{
			foreach ($data['join'] as $join) {
				if(!isset($join['type'])) $join['type'] = "";
				$this->db->join($join['table'],$join['const'],$join['type']);
			}
		}

		if(isset($data['const']))
		{
			foreach ($data['const'] as $where) {
				$this->db->where($where);
			}
		}

		if(isset($data['order_by']))
		{
			foreach ($data['order_by'] as $order) {
				if(!isset($order['type'])) $order['type'] = "asc";
				$this->db->order_by($order['attrb'],$order['type']);
			}
		}

		if(!isset($data['query_type']))
		{
			$data['query_type'] = "R";
		}
		
		if($data['query_type'] == "R")
		{
			$query = $this->db->get($data['base_table']);

			if ($query->num_rows() > 0) {
				if (isset($data['single'])) {
					if (isset($data['obj'])) return $query->row();
					else return $query->row_array();
				}
				elseif (isset($data['obj'])) return $query->result();
				else return $query->result_array();
			}
			else return NULL;
		}
		elseif($data['query_type'] == "C")
		{
			$this->db->insert($data['base_table']);
			return TRUE;
		}
		elseif($data['query_type'] == "U")
		{
			$this->db->update($data['base_table']);
			return TRUE;
		}
		elseif($data['query_type'] == "D")
		{
			$this->db->delete($data['base_table']);
			return TRUE;
		}
	}

}

/* End of file MY_Model.php */
/* Location: ./application/controllers/MY_Model.php */