<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$s = SCHEMA;
		$this->t_obat = $s.'obat';
		$this->t_log_obat_keluar = $s.'log_obat_keluar';
		$this->t_pengeluaran_obat = $s.'pengeluaran_obat';
		$this->t_stok_obat = $s.'stok_obat';
		$this->t_pasien = $s.'pasien';
		$this->t_rekam_medis = $s.'rekam_medis';
		$this->t_resep = $s.'resep';
		$this->t_detail_resep = $s.'detail_resep';
		$this->t_log_update_obat = $s.'log_update_obat';
		$this->t_penerimaan_obat = $s.'penerimaan_obat';
		$this->t_pengeluaran_obat = $s.'pengeluaran_obat';
		$this->t_log_obat_masuk  = $s.'log_obat_masuk';
		$this->t_supplier  = $s.'supplier';
		$this->t_pasien = $s.'pasien';
		$this->t_transaksi_obat_salemba = $s.'transaksi_obat_salemba';
		$this->t_detail_transaksi_salemba = $s.'detail_transaksi_salemba';
		$this->t_stok_awal_bulan = $s.'stok_awal_bulan';

		$this->load->model('authentication');
	}

	/*
	Fungsi ini mencetak halaman UI yang diinginkan.
	parameter (nama view, $data yang diperlukan (opsional))
	*/
	public function cetak_halaman($view, $data = array()) {

		// $this->load->view('komponen/_header', $data);
		// $this->load->view($view, $data);
		// $this->load->view('komponen/_footer');


		$this->load->view('templates/header', $data);
		// $this->load->view('komponen/_loader', $data);
        $this->load->model('authentication');
		if($this->authentication->is_loggedin()){
            $this->load->view('usermenu/usermenu', $data);
        }
		else $this->load->view('usermenu/loginform', $data);
		// $this->load->view('site/'.$page, $data);
		$this->load->view($view, $data);
		$this->load->view('templates/footer', $data);
		$this->_set_last_url();
	}


	/*
	Fungsi ini memanggil model yang diinginkan.
	parameter (nama model)
	*/
	public function get_model($model) {
		$this->load->model($model);
	}

	/*
	Fungsi ini melakukan pengecekan apakah sudah login atau belum.
	mengembalikan nilai apakah sudah login atau belum
	*/
	public function cek_login() {
		return $this->authentication->is_loggedin();
	}

	public function enkripsi($str) {
		return $this->encrypt->encode($str);
	}

	public function dekripsi($str) {
		return $this->encrypt->decode($str);
	}

	public function cek_int_only($input) {
		if($input != null) {
			if(!preg_match('/^[0-9]+$/', $input)) {
				//$this->form_validation->set_message('_cek_input_angka', 'Nilai masukan harus berupa bilangan bulat positif');
				return FALSE;
			}
			else if(strlen($input) > 12) {
				return FALSE;
			}
		}
		return TRUE;
	}

  public function get_last_url()
  {
  	if ($this->session->userdata('last_url') != NULL)
  		$last_url = $this->session->userdata('last_url');
  	else $last_url = "";
    return $last_url;
  }

  private function _set_last_url()
  { 
  	$url = current_url();
  	$this->_updt_last_url($url);
  }

  private function _updt_last_url($url)
  {
  	$last_url_new = $this->_get_last_url_old();
    $data = array('last_url' => $last_url_new, 'last_url_old' => $url);
    $this->session->set_userdata($data);
  }

  private function _get_last_url_old()
  {
    return $this->session->userdata('last_url_old');
  }



}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */